<?php

if (preg_match("/^[-_a-z0-9]+$/i", $_GET['key']))
{
    $user = $DB->GetRow("SELECT id, title, email, login FROM users WHERE status=1 AND valid_key=?", array($_GET['key']));
    
    if ($user)
    {
        // подключение к пользовательской БД
        $DB2 = ADONewConnection(DB_USERS_DSN);
        $DB2->SetFetchMode(ADODB_FETCH_ASSOC);
        $DB2->Execute("SET NAMES utf8");
        $DB2->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
        $DB2->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
        $DB2->cacheSecs = DB_CACHE_TIME;
        //$DB2->debug = true;

		// пользователь
		include_once (PATH_BACKEND . "/php/classes/class.User.php");
		$user_upd = new User($DB, $DB2);

        // генерация пароля и запись его в БД
        $new_password = mb_substr(md5(uniqid()), 0, 8);
        $upd['password'] = md5(iconv('utf-8', 'windows-1251', $new_password));
        $upd['valid_key'] = "";
        $user_upd->Edit($user['id'], $upd);

        // отправка письма пользователю с новым паролем
        $mail = new PHPMailer();
        $mail->SetFrom("admin@einfo.ru", "Einfo");
        $mail->Subject = "[einfo.ru] Новый пароль";
        $smarty->assign("new_password", $new_password);
        $smarty->assign("user", $user);
        $mail->Body = $smarty->fetch("_remember/mail_newpass.tpl");

        $email_to = preg_split("/[,;\s]/", $user['email'], -1, PREG_SPLIT_NO_EMPTY);
        foreach ($email_to as $value)
        {
            $mail->AddAddress(trim($value));
        }
		$mail->AddAddress("admin@einfo.ru");
		$mail->AddAddress("admin@efind.ru");

        $mail->Send();

        print "На ваш email выслано письмо с новым паролем.";
    }
    else
    {
        print "Код валидации неверен!";
    }
}

?>
