<?php

if (isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] != "" && $_SESSION['captcha_keystring'] == $_POST['captcha'])
{
    $_email = trim($_POST['email']);

    if (PHPMailer::ValidateAddress($_email))
    {
        $email = "%" . $_email . "%";
        $user = $DB->GetRow("SELECT id, title, email, price_file_use FROM users WHERE status = 1 AND email LIKE ?", array($email));

        if ($user)
        {
            // отправка письма с ссылкой для верификации (смена пароля)
            $valid_key = md5(uniqid());

            $mail = new PHPMailer();
            $mail->SetFrom("admin@einfo.ru", "Einfo");
            $mail->Subject = "[einfo.ru] Смена пароля";
            $smarty->assign("valid_key", $valid_key);
            $smarty->assign("user", $user);
            $mail->Body = $smarty->fetch("_remember/mail_valid.tpl");

            $email_to = preg_split("/[,;\s]/", $user['email'], -1, PREG_SPLIT_NO_EMPTY);
            
            foreach ($email_to as $value)
            {
                $mail->AddAddress(trim($value));
            }
            $mail->AddAddress("admin@einfo.ru");

            $mail->Send();

            $DB->Execute("UPDATE users SET valid_key = ? WHERE id = ?", array($valid_key, $user['id']));

            print "OK";
        }
        else
        {
            print "NONE";
        }
    }
    else
    {
        print "NOT_VALID";
    }
}
else
{
    print "CAPTCHA";
}
