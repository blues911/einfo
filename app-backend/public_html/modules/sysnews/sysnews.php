<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        if ($_GET['action'] == "view")
        {
            $all_items = $DB->GetOne("SELECT COUNT(*) FROM sysnews");

            $pager = new Pager($all_items, $SETTINGS['sysnews']['max_on_page'], "dynamic");

            $sysnews = $DB->GetAll("SELECT * FROM sysnews ORDER BY date DESC LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());
            foreach ($sysnews as $k => $value)
            {
                $sysnews[$k]['date'] = $date->GetRusDate($value['date'], 4);
            }

            $smarty->assign("sysnews", $sysnews);
        	$smarty->assign("pager", $pager->GetPages());
            $smarty->display("sysnews/view.tpl");
            $smarty->clear_all_assign();
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>