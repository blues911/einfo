<?php

include_once (PATH_BACKEND . "/php/classes/class.Billing.php");

$response = array('success' => 0);

if (!empty($_POST['id']))
{
    $id = (int)$_POST['id'];

    $billing = new Billing($DB);

    $billing->change_status_notify($id);

    $response['status'] = $billing->change_status($id);
    $response['success'] = 1;
}

echo(json_encode($response));