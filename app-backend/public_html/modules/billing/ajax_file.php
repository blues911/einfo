<?php

include_once(PATH_BACKEND . '/php/classes/class.Billing.php');

if (!empty($_GET['id']))
{
    $id = (int)$_GET['id'];

    $billing = new Billing($DB);

    $file = $billing->get_file($id);

    if (!empty($file))
    {
        header('Content-Type: ' . $file['file_type']);
        echo($file['file']);
    }
}