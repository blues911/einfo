<?php

defined("ADMIN") or die("FAIL");

define('ITEMS_ON_PAGE', 200);

include_once (PATH_BACKEND . "/php/classes/class.Billing.php");
include_once (PATH_BACKEND . "/php/classes/class.User.php");

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {
        // Список предустановленных периодов
        $periods = array(
            array(
                'title' => 'текущий месяц',
                'from' => date('d.m.Y', strtotime('first day of this month')),
                'to' => date('d.m.Y', strtotime('last day of this month'))
            ),
            array(
                'title' => 'следующий месяц',
                'from' => date('d.m.Y', strtotime('first day of next month')),
                'to' => date('d.m.Y', strtotime('last day of next month'))
            ),
            array(
                'title' => '6 месяцев с текущего',
                'from' => date('d.m.Y', strtotime('first day of this month')),
                'to' => date('d.m.Y', strtotime('last day of +5 month'))
            ),
            array(
                'title' => '6 месяцев со следующего',
                'from' => date('d.m.Y', strtotime('first day of next month')),
                'to' => date('d.m.Y', strtotime('last day of +6 month'))
            )
        );

        // Список платежей
        if ($_GET['action'] == 'list')
        {
            $filter = array(
                'user_id' => !empty($_GET['user_id']) ? (int)$_GET['user_id'] : 0,
                'status' => isset($_GET['status']) ? (int)$_GET['status'] : -1,
                'active_from' => isset($_GET['active_from']) ? date('d.m.Y', strtotime($_GET['active_from'])) : date('d.m.Y', strtotime('first day of this month')),
                'active_to' => isset($_GET['active_to']) ? date('d.m.Y', strtotime($_GET['active_to'])) : date('d.m.Y', strtotime('last day of this month')),
                'one_page' => !empty($_GET['one_page']) ? (int)$_GET['one_page'] : 0
            );

            foreach (array_keys($periods) as $k)
            {
                $periods[$k]['selected'] = ($filter['active_from'] == $periods[$k]['from']) && ($filter['active_to'] == $periods[$k]['to']) ? true : false;
            }

            $billing = new Billing($DB);
            $user = new User($DB);

            if (empty($filter['one_page']))
            {
                $cnt = $billing->get_count();
                $pager = new Pager($cnt, ITEMS_ON_PAGE, 'dynamic');

                $filter = array_merge($filter, array('limit_offset' => $pager->GetLimitStart(), 'limit' => $pager->GetLimitEnd()));

                $smarty->assign('pager', $pager->GetPages());
            }

            $bills = $billing->get_list($filter);
            $user_list = $user->get_list(array('without_admins' => true, 'order' => 'title_asc'));

            $sum = 0.00;
            foreach (array_keys($bills) as $k)
            {
                if (strtotime($bills[$k]['active_from']) >= strtotime($filter['active_from']))
                {
                    $sum += $bills[$k]['amount'];
                }
            }

            $smarty->assign('list', $bills);
            $smarty->assign('sum', $sum);
            $smarty->assign('user_list', $user_list);
            $smarty->assign('filter', $filter);
            $smarty->assign('periods', $periods);
            $smarty->display('billing/list.tpl');
        }

        // Форма добавления
        elseif ($_GET['action'] == 'add-form')
        {
            $user = new User($DB);
            $user_list = $user->get_list(array('without_admins' => true, 'order' => 'title_asc'));

            $smarty->assign('user_list', $user_list);
            $smarty->assign('periods', $periods);
            $smarty->assign('today', date('d.m.Y'));
            $smarty->display('billing/add-form.tpl');
        }

        // Добавление
        elseif ($_GET['action'] == 'add')
        {
            $billing = new Billing($DB);

            $ins = array(
                'user_id' => (int)$_POST['user_id'],
                'payment_number' => $_POST['payment_number'],
                'payment_date' => $_POST['payment_date'],
                'amount' => (float)$_POST['amount'],
                'active_from' => $_POST['active_from'],
                'active_to' => $_POST['active_to'],
                'status' => (int)$_POST['status']
            );

            $id = $billing->add($ins);

            if (!empty($id) && !empty($_FILES['file']['name']) && $_FILES['file']['error'] == 0)
            {
                $file = file_get_contents($_FILES['file']['tmp_name']);
                $file_type = $_FILES['file']['type'];

                $billing->set_file($id, $file, $file_type);
            }

            adm_redirect('?module=billing&action=list');
        }

        // Форма редактирования
        elseif ($_GET['action'] == 'edit-form')
        {
            if (!empty($_GET['id']))
            {
                $id = (int)$_GET['id'];

                $billing = new Billing($DB);
                $user = new User($DB);

                $bill = $billing->get_item(array('id' => $id));
                $user_list = $user->get_list(array('without_admins' => true, 'order' => 'title_asc'));

                foreach (array_keys($periods) as $k)
                {
                    $periods[$k]['selected'] = ($bill['active_from'] == $periods[$k]['from']) && ($bill['active_to'] == $periods[$k]['to']) ? true : false;
                }

                $smarty->assign('bill', $bill);
                $smarty->assign('user_list', $user_list);
                $smarty->assign('periods', $periods);
                $smarty->display('billing/edit-form.tpl');
            }
            else
            {
                adm_redirect('?module=billing&action=list');
            }
        }

        // Редактирование
        elseif ($_GET['action'] == 'edit')
        {
            if (!empty($_GET['id']))
            {
                $id = (int)$_GET['id'];

                $billing = new Billing($DB);

                $upd = array(
                    'user_id' => (int)$_POST['user_id'],
                    'payment_number' => $_POST['payment_number'],
                    'payment_date' => $_POST['payment_date'],
                    'amount' => (float)$_POST['amount'],
                    'active_from' => $_POST['active_from'],
                    'active_to' => $_POST['active_to'],
                    'status' => (int)$_POST['status']
                );

                $billing->change_status_notify($id, $upd['status']);
                $result = $billing->edit($id, $upd);

                if (!empty($result) && !empty($_FILES['file']['name']) && $_FILES['file']['error'] == 0)
                {
                    $file = file_get_contents($_FILES['file']['tmp_name']);
                    $file_type = $_FILES['file']['type'];

                    $billing->set_file($id, $file, $file_type);
                }
            }

            adm_redirect('?module=billing&action=list');
        }
    }
}
else
{
    adm_message($control->GetMessage());
}