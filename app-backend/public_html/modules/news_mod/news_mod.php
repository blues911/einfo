<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_NEWS_ONPAGE", 30);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    include_once (PATH_BACKEND . "/php/classes/class.News.php");
	    $news = new News($DB, $SETTINGS['news'], $_SESSION['user_id']);

	    // ------------------------------------
	    if ($_GET['action'] == "mod-list")
        {
            $all_items = $DB->GetOne("SELECT COUNT(*) FROM news");

            $pager = new Pager($all_items, MAX_NEWS_ONPAGE, "dynamic");

            $list = $DB->GetAll("SELECT news.id, news.date, news.status, news.title, news.user_id, user.title as user_title FROM news
            						INNER JOIN users user ON news.user_id=user.id
            					 ORDER BY IF(news.status=0, 0, 1) ASC, date DESC LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());
            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $list[$k] = $value;
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
        	$smarty->assign("pager", $pager->GetPages());
            $smarty->display("news_mod/mod-list.tpl");
            $smarty->clear_all_assign();
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "changestatus")
        {
            $news->ChangeStatus((int)$_GET['id'], (int)$_GET['status']);
            adm_redirect("?module=news_mod&action=mod-list");
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "delete")
        {
            $id = (int)$_GET['id'];
            $news->Delete($id);
            adm_redirect("?module=news_mod&action=mod-list");
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "view")
        {
            $id = (int)$_GET['id'];
            $content = $DB->GetRow("SELECT news.*, user.title as user_title FROM news
            							INNER JOIN users user ON news.user_id=user.id
            						WHERE news.id=?", array($id));
            if ($content['img'] != "")
            {
                $content['img'] = 'http://' . NEWSIMG_URL . '/' . $content['img'];
            }

            $smarty->assign("content", $content);
            $smarty->display("news_mod/view.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit-form")
        {
            $id = (int)$_GET['id'];
	        foreach ($SETTINGS['news'] as $k => $value)
	        {
	            $smarty->assign($k, $value);
	        }

            $content = $DB->GetRow("SELECT * FROM news WHERE id=?", array($id));
            if ($content['img'] != "")
            {
                $content['img'] = 'http://' . NEWSIMG_URL . '/' . $content['img'];
            }

    	    $smarty->assign("content", $content);
    	    $smarty->display("news_mod/edit-form.tpl");
    	    $smarty->clear_all_assign();

        }
        // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $data['id'] = (int)$_GET['id'];
                $data['status'] = (int)$_POST['status'];
                $data['title'] = $_POST['title'];
                $data['text'] = $_POST['text'];
                $data['img'] = $_FILES['img'];
                $news->Edit($data);

                adm_redirect("?module=news_mod&action=mod-list");
            }
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>