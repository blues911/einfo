<?php

include_once(PATH_FRONTEND . '/php/libs/sphinxapi.php');

defined("ADMIN") or die("FAIL");

define('ITEMS_ON_PAGE', 50);

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {

        $category_list = $DB->GetAll("SELECT id, c_level, title FROM categories WHERE c_level!=0 ORDER BY c_left ASC");
        $smarty->assign('category_list', $category_list);

        // Список компонентов
        if ($_GET['action'] == 'list')
        {
            $like = !empty($_GET['like']) ? trim($_GET['like']) : '';
            $view_all = !empty($_GET['view_all']) ? true : false;

            $category_id = !empty($_GET['category_id']) ? (int)$_GET['category_id'] : 1;
            $category = $DB->GetRow('SELECT c_left, c_right FROM categories WHERE id = ?', array($category_id));

            $_adv_yandex = isset($_GET['adv_yandex']) ? $_GET['adv_yandex'] : -1;
            if ($_adv_yandex == 0)
            {
                $adv_yandex = ' AND comp.adv_show = 0 ';
            }
            else if ($_adv_yandex == 1)
            {
                $adv_yandex = ' AND comp.adv_show = 1 ';
            }
            else
            {
                $adv_yandex = '';
            }

            $_type = isset($_GET['type']) ? $_GET['type'] : -1;
            if ($_type == 0)
            {
                $type = ' AND comp.regexp_id = 0 ';
            }
            else if ($_type == 1)
            {
                $type = ' AND comp.regexp_id > 0 ';
            }
            else
            {
                $type = '';
            }

            if (!$view_all)
            {
                $counter = $DB->GetOne('
                    SELECT
                      COUNT(*)
                    FROM
                      catalog_comp_canon AS comp
                      INNER JOIN categories AS cat ON comp.category_id = cat.id
                      LEFT OUTER JOIN datasheet ON datasheet.comp_canon = comp.comp_canon
                    WHERE
                      cat.c_left BETWEEN ? AND ?
                      AND comp.comp_canon LIKE ?
                      ' . $adv_yandex . '
                      ' . $type
                , array($category['c_left'], $category['c_right'], $like . '%'));

                $pager = new Pager($counter, ITEMS_ON_PAGE, 'dynamic');

                $limit = ' LIMIT ' . $pager->GetLimitStart() . ', ' . $pager->GetLimitEnd();
                $num = $pager->GetLimitStart() + 1;

                $smarty->assign('pager', $pager->GetPages());
            }
            else
            {
                $limit = '';
                $num = 1;
            }

            $list = $DB->GetAll('
                SELECT
                  comp.id,
                  IF(comp.regexp_id > 0, 0, 1) AS manual,
                  cat.full_title AS category_title,
                  comp.comp_canon,
                  datasheet.`remote` AS datasheet_remote,
                  comp.adv_show
                FROM
                  catalog_comp_canon AS comp
                  INNER JOIN categories AS cat ON comp.category_id = cat.id
                  LEFT OUTER JOIN datasheet ON datasheet.comp_canon = comp.comp_canon
                WHERE
                  cat.c_left BETWEEN ? AND ?
                  AND comp.comp_canon LIKE ?
                  ' . $adv_yandex . '
                  ' . $type . '
                ORDER BY cat.c_left, comp.comp_canon ASC
                ' . $limit
            , array($category['c_left'], $category['c_right'], $like . '%'));

            foreach (array_keys($list) as $k)
            {
                $list[$k]['num'] = $num;
                $num++;
            }

            $smarty->assign('adv_yandex', $_adv_yandex);
            $smarty->assign('type', $_type);
            $smarty->assign('like', $like);
            $smarty->assign('view_all', $view_all);
            $smarty->assign('list', $list);
            $smarty->assign('category_id', $category_id);
            $smarty->display('comp_canon/list.tpl');
        }

        // Форма редактирования компонентаа
        if ($_GET['action'] == 'edit_form')
        {
            $comp = $DB->GetRow('
                SELECT
                  comp.id,
                  comp.category_id,
                  comp.comp_canon,
                  datasheet.`remote` AS datasheet_remote
                FROM
                  catalog_comp_canon AS comp
                  LEFT OUTER JOIN datasheet ON datasheet.comp_canon = comp.comp_canon
                WHERE
                  comp.id = ?
            ', array((int)$_GET['id']));

            $smarty->assign('comp', $comp);
            $smarty->display('comp_canon/edit_form.tpl');
        }

        // Редактирование компонентов
        if ($_GET['action'] == 'edit')
        {
            $comp = $DB->GetRow('
                SELECT
                  comp.id,
                  comp.category_id,
                  comp.comp_canon,
                  datasheet.`remote` AS datasheet_remote
                FROM
                  catalog_comp_canon AS comp
                  LEFT OUTER JOIN datasheet ON datasheet.comp_canon = comp.comp_canon
                WHERE
                  comp.id = ?
            ', array((int)$_POST['id']));

            $upd = array();
            $upd['comp_canon'] = $_POST['comp_canon'];
            $upd['category_id'] = $_POST['category_id'];

            // Если изменилось каноническое имя, то пересчитываем цену и кол-во предложений
            if ($upd['comp_canon'] != $comp['comp_canon'])
            {
                $upd = array_merge($upd, comp_canon_count($upd['comp_canon'], $DB, $SETTINGS));
            }

            // Если изменилась категория или каноническое имя, то ставим признак ручной привязки
            if ($upd['category_id'] != $comp['category_id'] || $upd['comp_canon'] != $comp['comp_canon'])
            {
                $upd['regexp_id'] = 0;
            }

            $datasheet = array();
            $datasheet['remote'] = $_POST['datasheet_remote'];
            $datasheet['comp_canon'] = $_POST['comp_canon'];

            // Если сменилось каноническое имя или ссылка на даташит, то вносим новую информацию
            if ($datasheet['remote'] != $comp['datasheet_remote'] || $datasheet['comp_canon'] != $comp['comp_canon'])
            {
                $DB->Execute('
                    INSERT INTO
                      datasheet(`comp_canon`,`remote`,`local`,`img`,`size`,`update`)
                    VALUES
                      (?, ?, "", "", 0, "0000-00-00 00:00:00")
                    ON DUPLICATE KEY UPDATE
                      `remote` = ?,
                      `update` = "0000-00-00 00:00:00"
                ', array($datasheet['comp_canon'], $datasheet['remote'], $datasheet['remote']));
            }
            // Обновляем компонент
            $DB->AutoExecute('catalog_comp_canon', $upd, 'UPDATE', 'id=' . (int)$_POST['id']);
            adm_redirect('?module=comp_canon&action=list');
        }

        // Форма добавления компонента
        if ($_GET['action'] == 'add_form')
        {
            $smarty->display('comp_canon/add_form.tpl');
        }

        // Добавление компонента
        if ($_GET['action'] == 'add')
        {
            $ins = array();
            $ins['comp_canon'] = $_POST['comp_canon'];
            $ins['category_id'] = $_POST['category_id'];
            $ins['regexp_id'] = 0;

            // Считаем цену и кол-во предложений
            $ins = array_merge($ins, comp_canon_count($ins['comp_canon'], $DB, $SETTINGS));

            $datasheet = array();
            $datasheet['comp_canon'] = $_POST['comp_canon'];
            $datasheet['remote'] = $_POST['datasheet_remote'];

            // Если заполнен адрес документации, то записываем в datasheet
            if (!empty($datasheet['remote']))
            {
                $DB->Execute('
                    INSERT INTO
                      datasheet(`comp_canon`,`remote`,`local`,`img`,`size`,`update`)
                    VALUES
                      (?, ?, "", "", 0, "0000-00-00 00:00:00")
                    ON DUPLICATE KEY UPDATE
                      `remote` = ?,
                      `update` = "0000-00-00 00:00:00"
                ', array($datasheet['comp_canon'], $datasheet['remote'], $datasheet['remote']));
            }

            // Добавляем компонент
            $DB->AutoExecute('catalog_comp_canon', $ins, 'INSERT');
            adm_redirect('?module=comp_canon&action=list');
        }

        // Удаление компонента
        if ($_GET['action'] == 'delete')
        {
            $DB->Execute('DELETE FROM catalog_comp_canon WHERE id = ?', array((int)$_GET['id']));
            adm_redirect('?module=comp_canon&action=list');
        }

        // Групповые действия
        if ($_GET['action'] == 'group_action')
        {
            $ids = !empty($_POST['comp']) ? array_keys($_POST['comp']) : array();
            if(!empty($ids))
            {
                if ($_POST['do'] == 'adv_show')
                {
                    $DB->Execute('
                        UPDATE
                          catalog_comp_canon
                        SET
                          adv_show = 1
                        WHERE
                          id IN (' . implode(',', $ids) . ')
                    ');
                }
                else if ($_POST['do'] == 'adv_hide')
                {
                    $DB->Execute('
                        UPDATE
                          catalog_comp_canon
                        SET
                          adv_show = 0
                        WHERE
                          id IN (' . implode(',', $ids) . ')
                    ');
                }
                else if ($_POST['do'] == 'delete')
                {
                    $DB->Execute('
                        DELETE FROM
                          catalog_comp_canon
                        WHERE
                          id IN (' . implode(',', $ids) . ')
                    ');
                }
            }

            adm_redirect('?module=comp_canon&action=list');
        }

    }
}

function comp_canon_count($comp_canon, &$DB, &$SETTINGS)
{
    $upd = array(
        'offer_cnt' => 0,
        'price_median' => 0
    );

    // Курсы валют
    $usd = $SETTINGS['cur_rates']['usd'];
    $eur = $SETTINGS['cur_rates']['eur'];

    // Соединение со Sphinx Engine
    $sphinx = new SphinxClient();
    $sphinx->SetServer(SPHINX_HOST, SPHINX_PORT);
    $sphinx->SetMatchMode(SPH_MATCH_EXTENDED);
    $sphinx->SetLimits(0, 3000, 3000, 3000);

    // Первоначальный поиск с помощью LIKE
    $canon_exist = $DB->GetOne('SELECT id FROM base_main WHERE comp_title LIKE ? LIMIT 0, 1', array($comp_canon . '%'));

    // Если нашлась хотя бы одна запись, запускаем поиск через sphinx
    if (!empty($canon_exist))
    {
        $atoms = implode(' ', get_request_atoms($comp_canon));

        $sphinx_query = '"^' . $atoms . '"';
        $sphinx_result = $sphinx->Query($sphinx_query, SPHINX_INDEX);

        // Если sphinx вернул не пустой результат, то делаем дополнительный запрос в БД для найденных ID
        if (!empty($sphinx_result['matches']))
        {
            $comp_ids = array_keys($sphinx_result['matches']);

            $list_rs = $DB->Execute('
                        SELECT
                          main.id AS id,
                          main.prices AS prices
                        FROM
                          base_main AS main
                          INNER JOIN users ON main.user_id = users.id
                        WHERE
                          users.status = 1
                          AND main.id IN (' . implode(',', $comp_ids) . ')
                          AND main.atoms LIKE ?
                    ', array($atoms . '%'));

            $list = array('id' => array(), 'price' => array());
            while(!$list_rs->EOF)
            {
                $list['id'][] = $list_rs->fields('id');

                $prices = json_decode($list_rs->fields('prices'), true);

                if (!empty($prices))
                {
                    $price = array_shift($prices);
        
                    if ($price['c'] == 'rur')
                    {
                        $list['price'][] = $price['p'];
                    }
                    else if ($price['c'] == 'usd')
                    {
                        $list['price'][] = $price['p'] * $usd;
                    }
                    else
                    {
                        $list['price'][] = $price['p'] * $eur;
                    }
                }

                $list_rs->MoveNext();
            }

            if (!empty($list['id']))
            {
                $canon = array(
                    'offer_cnt' => count($list['id']),
                    'price_median' => median($list['price'])
                );

                $upd['offer_cnt'] = $canon['offer_cnt'];
                $upd['price_median'] = $canon['price_median'];
            }
        }
    }

    return $upd;
}