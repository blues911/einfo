<?php

if (preg_match("/^[-_a-z0-9]+$/i", $_GET['key']))
{
    $user_id = $DB->GetOne("SELECT id FROM users WHERE status=-2 AND valid_key=?", array($_GET['key']));
    
    if ($user_id)
    {
        $moderated = preg_match("/^m_/i", $_GET['key']);
        $DB->Execute("UPDATE users SET valid_key='', status=".($moderated ? -1 : 1)." WHERE id=?", array($user_id));

        print "Ваш аккаунт успешно прошел валидацию. ";
        if ($moderated)
        {
            print "После одобрения его модератором он станет активным.";
        }
        else
        {
            print 'Для доступа к аккаунту используйте адрес <a href="http://www.einfo.su">www.einfo.su</a> и свой логин с паролем.';
        }

        $user = $DB->GetRow("SELECT users.*, city.city, country.country
                             FROM users
                                LEFT JOIN ip2ruscity_cities city ON city.city_id=users.city_id
                                LEFT JOIN ip2ruscity_countries country ON country.country_id=users.country_id
                             WHERE users.id=?", array($user_id));

        // отправка письма админам с информацией о новом участнике
        $mail = new PHPMailer();
        $mail->SetFrom("admin@einfo.ru", "Einfo");
        $mail->Subject = "[einfo.ru] Новый участник в системе";
        $smarty->assign("user", $user);
        $mail->Body = $smarty->fetch("_register/mail_newuser.tpl");
        $mail->AddAddress("admin@einfo.ru");
		$mail->AddAddress("alexey@efind.ru");
		$mail->AddAddress("admin@efind.ru");
        $mail->Send();

    }
    else
    {
        print "Код валидации неверен!";
    }
}

?>
