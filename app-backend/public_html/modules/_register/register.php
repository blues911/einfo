<?php

if ($_GET['register'])
{
    $id = (int)$_GET['register'];
    $account = $DB->GetRow("SELECT * FROM accounts WHERE id=?", array($id));
    if ($account)
    {
        $fields = $DB->GetAll("SELECT ufield.title, ufield.field, levels_fields.fill
                               FROM users_fields ufield
                                  INNER JOIN access_levels_fields levels_fields ON levels_fields.field_id=ufield.id
                               WHERE levels_fields.level_id=?
                               ORDER BY ufield.position ASC", $account['level_id']);
        $fill = array();
        $arr_fields = array();
        foreach ($fields as $value)
        {
            $arr_fields[] = $value['field'];
            if ($value['fill'])
            {
                $fill[] = $value['field'];
            }
        }

        $countries = $DB->GetAll("SELECT * FROM ip2ruscity_countries WHERE country_id!=0 ORDER BY country ASC");
        $smarty->assign("countries", $countries);

        $cities = $DB->GetAll("SELECT * FROM ip2ruscity_cities WHERE city_id!=0 ORDER BY city ASC");
        $smarty->assign("cities", $cities);

        $smarty->assign("account", $account);
        $smarty->assign("fields", $fields);
        $smarty->assign("fill", $fill);
        $smarty->assign("arr_fields", $arr_fields);
        $smarty->display("_register/regform.tpl");
        $smarty->clear_all_assign();

        $container['page_title'] = "Регистрация аккаунта - ".$account['title'];
    }    
}
else
{
    $accounts = $DB->GetAll("SELECT * FROM accounts ORDER BY title ASC");

    $smarty->assign("accounts", $accounts);
    $smarty->display("_register/accounts.tpl");
    $smarty->clear_all_assign();
}



?>