<?php

header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
exit;

if (isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] != "" && $_SESSION['captcha_keystring'] == $_POST['captcha'])
{
    $account = $DB->GetRow("SELECT id, level_id, moderate FROM accounts WHERE id=?", array((int)$_POST['account_type']));

    if ($account !== false)
    {
        $fields = $DB->GetCol("SELECT ufield.field
                               FROM users_fields ufield
                                  INNER JOIN access_levels_fields levels_fields ON levels_fields.field_id=ufield.id
                               WHERE levels_fields.level_id=?
                               ORDER BY ufield.position ASC", $account['level_id']);

        $ins['level_id'] = $account['level_id'];
        $ins['status'] = -2;
        $ins['valid_key'] = ($account['moderate'] ? "m_" : "a_").md5(uniqid());
        $ins['register_date'] = date("Y-m-d H:i:s");
        $ins['password'] = md5(iconv('utf-8', 'windows-1251', $_POST['password']));

        foreach ($fields as $value)
        {
            if ($value != "password")
            {
                $ins[$value] = $_POST[$value];
            }
        }

        // добавление в БД
        $DB->AutoExecute("users", $ins, "INSERT");

        // отправка письма с ссылкой для верификации
        if ($ins['email'] != "")
        {
            $mail = new PHPMailer();
            $mail->SetFrom("admin@einfo.ru", "Einfo");
            $mail->Subject = "[einfo.ru] Валидация аккаунта";

            $smarty->assign("valid_key", $ins['valid_key']);
            $mail->Body = $smarty->fetch("_register/mail_valid.tpl");

            $email_to = preg_split("/[,;\s]/", $ins['email'], -1, PREG_SPLIT_NO_EMPTY);
            foreach ($email_to as $value)
            {
                $mail->AddAddress(trim($value));
            }

            $mail->Send();
        }

        print "OK";
    }
}
else
{
    print "CAPTCHA";
}