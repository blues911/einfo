<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_SYSNEWS_ONPAGE", 30);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    // ------------------------------------
	    if ($_GET['action'] == "add-form")
	    {
	        $smarty->display("sysnews_mod/add-form.tpl");
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "add")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $ins['date'] = date("Y-m-d H:i:s");
                $ins['title'] = $_POST['title'];
                $ins['text'] = $_POST['text'];

                $DB->AutoExecute("sysnews", $ins, "INSERT");

                adm_redirect("?module=sysnews_mod&action=list");
            }
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "list")
        {
            $sql_order = (!isset($_GET['order']) || $_GET['order'] == "desc") ? "desc" : "asc";

            $all_items = $DB->GetOne("SELECT COUNT(*) FROM sysnews");

            $pager = new Pager($all_items, MAX_SYSNEWS_ONPAGE, "dynamic");

            $list = $DB->GetAll("SELECT id, date, title FROM sysnews ORDER BY date ".$sql_order." LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());
            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $list[$k] = $value;
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
        	$smarty->assign("list_order", $sql_order);
        	$smarty->assign("pager", $pager->GetPages());
            $smarty->display("sysnews_mod/list.tpl");
            $smarty->clear_all_assign();
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "delete" && isset($_GET['id']))
        {
            $id = (int)$_GET['id'];
            $DB->Execute("DELETE FROM sysnews WHERE id=?", array($id));

            adm_redirect("?module=sysnews_mod&action=list");
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit-form")
        {
            $id = (int)$_GET['id'];
            $content = $DB->GetRow("SELECT * FROM sysnews WHERE id=?", array($id));

    	    $smarty->assign("content", $content);
    	    $smarty->display("sysnews_mod/edit-form.tpl");
    	    $smarty->clear_all_assign();
        }
        // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit" && isset($_GET['id']))
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $id = (int)$_GET['id'];
                $upd['title'] = $_POST['title'];
                $upd['text'] = $_POST['text'];
                $DB->AutoExecute("sysnews", $upd, "UPDATE", "id=".$id);

                adm_redirect("?module=sysnews_mod&action=list");
            }
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>