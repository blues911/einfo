<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        // ------------------------------------
        if ($_GET['action'] == 'search')
        {
            // Значения по умолчанию
            $to = empty($_GET['date_to']) ? time() : $_GET['date_to'] . ' 23:59:59';
            $from = empty($_GET['date_from']) ? strtotime('-1 month', time()) : $_GET['date_from'] . ' 00:00:00';
            $query = empty($_GET['query']) ? '' : $_GET['query'];

            // Приведение к timestamp
            $from = is_int($from) ? (int)$from : strtotime($from);
            $to = is_int($to) ? (int)$to : strtotime($to);

            if ($from > $to)
            {
                $_to = $to;
                $to = $from;
                $from = $_to;
            }

            // Форматирование для вывода в датапикере
            $date_from = date('d.m.Y', $from);
            $date_to = date('d.m.Y', $to);

            // Форматирование в представление Y-m-d H:i:s (для БД)
            $from_date = date('Y-m-d H:i:s', $from);
            $to_date = date('Y-m-d H:i:s', $to);

            // Запрос к БД
            $top = $DB->GetAll('
                SELECT
                  item.title,
                  MAX(orders.`date`) AS last_date,
                  COUNT(DISTINCT orders.id, item.title) AS cnt
                FROM
                  orders_items AS item
                  INNER JOIN orders ON orders.id = item.order_id
                WHERE
                  orders.`date` BETWEEN ? AND ?
                  AND item.title LIKE ?
                GROUP BY item.title
                ORDER BY cnt DESC, item.title ASC
                LIMIT ?
            ', array($from_date, $to_date, '%' . $query . '%', 100));

            // Добавляем нумерацию и форматируем дату
            $i = 1;
            foreach ($top as $key => $value)
            {
                $date = new MyDate();
                $top[$key]['num'] = $i;
                $top[$key]['last_date'] = $date->GetRusDate($value['last_date'], 4);
                $i++;
            }

            $smarty->assign('date_from', $date_from);
            $smarty->assign('date_to', $date_to);
            $smarty->assign('query', $query);
            $smarty->assign('top', $top);
            $smarty->display('stats_order/search.tpl');
            $smarty->clear_all_assign();
        }
        // ------------------------------------

	}
}
else
{
    adm_message($control->GetMessage());
}