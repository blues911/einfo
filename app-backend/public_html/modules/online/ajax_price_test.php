<?php

include_once(PATH_BACKEND . '/php/classes/class.XmlPrice.php');
include_once(PATH_BACKEND . '/php/xml_string_streamer/autoload.php');

ini_set('max_execution_time', 0);

$xml_price = new XmlPrice();

// ---------------------------------------------------------------

$user = $DB->GetRow('SELECT * FROM users WHERE id = ?', array($_SESSION['user_id']));

$user_file_type = ($_GET['type'] == 'einfo_json') ? 'json' : 'xml';

$file_path = PATH_PRICES . '/0_tmp/' . md5(uniqid()) . '.' . $user_file_type;

// Скачиваем файл
$file_handle = fopen($file_path, 'wb');
$curl = curl_init($_GET['url']);
curl_setopt($curl, CURLOPT_FILE, $file_handle);

if (!empty($_GET['login']) && !empty($_GET['pass']))
{
    curl_setopt($curl, CURLOPT_USERPWD, $_GET['login'] . ':' . $_GET['pass']);
}

curl_exec($curl);
$curl_info = curl_getinfo($curl);
curl_close($curl);
fclose($file_handle);

if ($curl_info['http_code'] == 200 && is_file($file_path) && filesize($file_path))
{
    $file_type = 'xml';

    if ($user_file_type == 'json')
    {
        $file_type = 'json';
        $prepare = $xml_price->prepareXmlFileFromJson($file_path, $user, true);
    }
    else
    {
        $prepare = $xml_price->prepareXmlFileFromXml($file_path, $_GET['type'], $user, null, true, $xml_hash);
    }

    unlink($file_path);

    if (!empty($_SESSION['user_id']))
    {
        $user_hash = md5(iconv('utf-8', 'windows-1251', $user['login'] . $user['password']));

        $smarty->assign('file_type', $file_type);
        $smarty->assign('rows', $prepare['cnt']);
        $smarty->assign('tags_info', $prepare['tags']);
        $smarty->assign('type', $_GET['type']);
    }
    else
    {
        $smarty->assign('error', 'Ошибка авторизации.');
    }
}
elseif ($curl_info['http_code'] == 401)
{
    $smarty->assign('error', 'Ошибка авторизации на удаленном сервере.');
}
else
{
    $smarty->assign('error', 'Не удается загрузить файл по указанному адресу.');
}

$smarty->display("online/ajax_price_test.tpl");