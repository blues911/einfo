<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    // ------------------------------------
	    if ($_GET['action'] == "price_file")
	    {
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['price_file']))
            {
                $date = new MyDate();

                // Отправляем сохраняемые данные на почту админа
                $mail = new PHPMailer();
                $mail->SetFrom('admin@einfo.ru', 'Einfo');
                $mail->Subject = '[einfo.ru] Изменение параметров XML импорта';

                $smarty->assign('id', $_SESSION['user_id']);
                $smarty->assign('ip', $_SERVER['REMOTE_ADDR']);
                $smarty->assign('date', $date->GetRusDate(date('Y-m-d H:i:s'), 4));
                $smarty->assign('post', $_POST);
                $mail->Body = $smarty->fetch('online/mail_settings_change.tpl');

                $email_to = array('admin@einfo.ru');
                foreach ($email_to as $value)
                {
                    $mail->AddAddress(trim($value));
                }
                $mail->Send();


                $upd['price_file_url'] = $_POST['price_file'];
                $upd['price_file_type'] = $_POST['price_file_type'];
                $upd['price_file_use'] = isset($_POST['price_file_use']) ? 1 : 0;
                $upd['price_file_login'] = $_POST['price_login'];
                $upd['price_file_pass'] = $_POST['price_password'];

                $DB->AutoExecute('users', $upd, 'UPDATE', 'id='.$_SESSION['user_id']);

                $smarty->assign('post', true);
            }

            $user = $DB->GetRow('SELECT price_file_url, price_file_type, price_file_use, price_file_login, price_file_pass FROM users WHERE id=?', array($_SESSION['user_id']));
            $smarty->assign('user', $user);

	        $smarty->display("online/price_file.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	}
}
else
{
    adm_message($control->GetMessage());
}




?>