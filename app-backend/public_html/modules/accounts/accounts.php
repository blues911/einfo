<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_ACCOUNTS_ONPAGE", 50);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    // ------------------------------------
	    if ($_GET['action'] == "add-form")
	    {
	        $levels = $DB->GetAll("SELECT * FROM access_levels ORDER BY title ASC");

	        $smarty->assign("levels", $levels);
	        $smarty->display("accounts/add-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "add")
	    {
	        $ins['title'] = $_POST['title'];
	        $ins['description'] = $_POST['description'];
	        $ins['level_id'] = (int)$_POST['access_levels'];
	        $ins['moderate'] = (int)$_POST['moderate'];
	        $ins['disclaimer'] = $_POST['disclaimer'];

	        $DB->AutoExecute("accounts", $ins, "INSERT");

	        adm_redirect("?module=accounts&action=list");
	    }
	    // ------------------------------------


        // ------------------------------------
	    else if ($_GET['action'] == "list")
	    {
	        $all_items = $DB->GetOne("SELECT COUNT(*) FROM accounts");

	        $pager = new Pager($all_items, MAX_ACCOUNTS_ONPAGE, "dynamic");

	        $list = $DB->GetAll("SELECT * FROM accounts LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());

	        $i = $pager->GetLimitStart() + 1;
	        foreach ($list as $k => $value)
	        {
	            $list[$k]['num'] = $i;
	            $i++;
	        }

	        $smarty->assign("list", $list);
	        $smarty->assign("pager", $pager->GetPages());

	        $smarty->display("accounts/list.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit-form")
	    {
	        $content = $DB->GetRow("SELECT * FROM accounts WHERE id=?", array((int)$_GET['id']));
	        $levels = $DB->GetAll("SELECT * FROM access_levels ORDER BY title ASC");

            $smarty->assign("content", $content);
            $smarty->assign("levels", $levels);
	        $smarty->display("accounts/edit-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit" && isset($_GET['id']))
	    {
	        $upd['title'] = $_POST['title'];
	        $upd['description'] = $_POST['description'];
	        $upd['level_id'] = (int)$_POST['access_levels'];
	        $upd['moderate'] = (int)$_POST['moderate'];
	        $upd['disclaimer'] = $_POST['disclaimer'];

	        $DB->AutoExecute("accounts", $upd, "UPDATE", "id=".(int)$_GET['id']);

	        if (isset($_POST['disclaimer_change']))
	        {
	            $DB->Execute("UPDATE users SET disclaimer=0 WHERE level_id=?", $upd['level_id']);
	        }

	        adm_redirect("?module=accounts&action=list");
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "delete" && isset($_GET['id']))
	    {
	        $id = (int)$_GET['id'];
	        $DB->Execute("DELETE FROM accounts WHERE id=?", $id);

	        adm_redirect("?module=accounts&action=list");
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>