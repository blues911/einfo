<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_BANNERS_ONPAGE", 30);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    // ------------------------------------
	    if ($_GET['action'] == "upload-form")
	    {
            // вывод загруженных баннеров
	        $all_items = $DB->GetOne("SELECT COUNT(*) FROM users_banners WHERE user_id=?", array($_SESSION['user_id']));
	        $pager = new Pager($all_items, MAX_BANNERS_ONPAGE, "dynamic");

	        $list = $DB->GetAll("SELECT * FROM users_banners WHERE user_id=? LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd(), array($_SESSION['user_id']));
            $i = $pager->GetLimitStart() + 1;
	        foreach ($list as $k => $value)
	        {
	            $list[$k]['banner'] = BANNERIMG_URL.'/'.$value['banner'];
                $list[$k]['num'] = $i;
                $i++;
	        }

	        $smarty->assign("list", $list);
	        $smarty->assign("pager", $pager->GetPages());
            $smarty->assign("allow_formats", $SETTINGS['users_banners']['allow_formats']);
            $smarty->assign("allow_sizes", $SETTINGS['users_banners']['allow_sizes']);

	        $smarty->display("users_banners/upload-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "upload")
	    {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                // выборка допустимых значений
                $allow_formats = explode(",", $SETTINGS['users_banners']['allow_formats']);
                $allow_sizes_arr = explode(",", $SETTINGS['users_banners']['allow_sizes']);
                $allow_sizes = array();
                foreach ($allow_sizes_arr as $k => $value)
                {
                    $allow_sizes[$k] = explode("x", $value);
                }


                if ($_FILES['upload_banner']['name'] != "")
                {
                    if (is_valid_url(trim($_POST['upload_url'])))
                    {
                        $banner_ext = pathinfo($_FILES['upload_banner']['name'], PATHINFO_EXTENSION);
                        if (in_array($banner_ext, $allow_formats))
                        {
                            $sizes_check = false;
                            $banner_info = getimagesize($_FILES['upload_banner']['tmp_name']);
                            foreach ($allow_sizes as $k => $size)
                            {
                                if ($size[0] == $banner_info[0] && $size[1] == $banner_info[1])
                                {
                                    $sizes_check = true;
                                }
                            }

                            if ($sizes_check)
                            {
                                $banner_name = "banner_".md5(uniqid()).".".$banner_ext;

                                $ins['user_id'] = $_SESSION['user_id'];
                                $ins['banner'] = $banner_name;
                                $ins['banner_blob'] = file_get_contents($_FILES['upload_banner']['tmp_name']);
                                $ins['url'] = trim($_POST['upload_url']);
                                $ins['size_x'] = $banner_info[0];
                                $ins['size_y'] = $banner_info[1];
                                $DB->AutoExecute("users_banners", $ins, "INSERT");

                                adm_redirect("?module=users_banners&action=upload-form");
                            }
                            else
                            {
                                adm_message("<strong>Вы пытаетесь загрузить баннер с недопустимыми размером &mdash; ".$banner_info[0]."x".$banner_info[1].".</strong><br />Разрешенные размеры: ".$SETTINGS['users_banners']['allow_sizes']);
                            }
                        }
                        else
                        {
                            adm_message("<strong>Вы пытаетесь загрузить недопустимый формат баннера &mdash; ".$banner_ext.".</strong><br />Разрешенные форматы: ".$SETTINGS['users_banners']['allow_formats']);
                        }
                    }
                    else
                    {
                        adm_message("<strong>Вы ввели некорректный URL баннера</strong>");
                    }
                }
                else
                {
                    adm_message("<strong>Вы не выбрали баннер для загрузки</strong>");
                }
            }
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}