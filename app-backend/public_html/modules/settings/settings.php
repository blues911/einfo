<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        // ------------------------------------
        if ($_GET['action'] == "settings-form")
        {
            $smarty->assign("settings", $SETTINGS);
            $smarty->display("settings/settings-form.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "settings-save")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                foreach($_POST as $k => $value)
                {
                    $value = $value;
                    $DB->Execute("UPDATE settings SET value=? WHERE var=?", array($value, $k));
                }

                adm_redirect("?module=settings&action=settings-form");
            }
        }
        // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>