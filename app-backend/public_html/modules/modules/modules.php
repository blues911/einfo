<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_MODULES_ONPAGE", 50);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    // ------------------------------------
	    if ($_GET['action'] == "add-form")
	    {
	        $smarty->display("modules/add-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "add")
	    {
	        $ins['title'] = $_POST['title'];
	        $ins['name'] = $_POST['name'];
	        $ins['position'] = (int)$_POST['position'];
	        $ins['default_action'] = $_POST['default_action'];
	        $ins['admin'] = (isset($_POST['admin'])) ? 1 : 0;
	        $ins['showmenu'] = (isset($_POST['showmenu'])) ? 1 : 0;

            if (preg_match('/\b[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\b/', $_POST['ip_filter']))
            {
                $ins['ip_filter'] = $_POST['ip_filter'];
            }
            else
            {
                $ins['ip_filter'] = '';
            }

	        $DB->AutoExecute("modules", $ins, "INSERT");

	        adm_redirect("?module=modules&action=list");
	    }
	    // ------------------------------------


        // ------------------------------------
	    else if ($_GET['action'] == "list")
	    {
	        $all_items = $DB->GetOne("SELECT COUNT(*) FROM modules");

	        $pager = new Pager($all_items, MAX_MODULES_ONPAGE, "dynamic");

	        $list = $DB->GetAll("SELECT * FROM modules ORDER BY position ASC LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());

	        $i = $pager->GetLimitStart() + 1;
	        foreach ($list as $k => $value)
	        {
	            $list[$k]['num'] = $i;
	            $i++;
	        }

	        $smarty->assign("list", $list);
	        $smarty->assign("pager", $pager->GetPages());

	        $smarty->display("modules/list.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit-form")
	    {
	        $content = $DB->GetRow("SELECT * FROM modules WHERE id=?", array((int)$_GET['id']));

            $smarty->assign("content", $content);
	        $smarty->display("modules/edit-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit" && isset($_GET['id']))
	    {
	        $upd['title'] = $_POST['title'];
	        $upd['name'] = $_POST['name'];
	        $upd['position'] = (int)$_POST['position'];
	        $upd['default_action'] = $_POST['default_action'];
	        $upd['admin'] = (isset($_POST['admin'])) ? 1 : 0;
	        $upd['showmenu'] = (isset($_POST['showmenu'])) ? 1 : 0;

            if (preg_match('/\b[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\b/', $_POST['ip_filter']))
            {
                $upd['ip_filter'] = $_POST['ip_filter'];
            }
            else
            {
                $upd['ip_filter'] = '';
            }

	        $DB->AutoExecute("modules", $upd, "UPDATE", "id=".(int)$_GET['id']);

	        adm_redirect("?module=modules&action=list");
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "delete" && isset($_GET['id']))
	    {
	        $id = (int)$_GET['id'];

	        $DB->Execute("DELETE FROM access WHERE module_id=?", $id);
	        $DB->Execute("DELETE FROM modules WHERE id=?", $id);

	        adm_redirect("?module=modules&action=list");
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>