<?php

include_once(PATH_BACKEND . '/php/classes/class.SocialPolls.php');

$response = array();
$response['success'] = false;

if (!empty($_POST['id']))
{
    $social_polls = new SocialPolls($DB);
    
    $response['item']['id'] = $_POST['id'];
    $response['item']['status'] = $social_polls->ChangeStatus($_POST['id']);
    $response['success'] = true;
}

echo(json_encode($response));