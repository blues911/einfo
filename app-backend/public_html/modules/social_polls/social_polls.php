<?php

defined("ADMIN") or die("FAIL");
define("MAX_SOCIAL_POLLS_ONPAGE", 50);

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
	  {
        include_once (PATH_BACKEND . "/php/classes/class.SocialPolls.php");
        $social_polls = new SocialPolls($DB);

        if ($_GET['action'] == "list")
	      {
            $all_items = $DB->GetOne("SELECT COUNT(*) FROM social_polls");

            $pager = new Pager($all_items, MAX_SOCIAL_POLLS_ONPAGE, "dynamic");

            $list = $DB->GetAll("SELECT * FROM social_polls LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());
            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $v)
            {
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
            $smarty->assign("pager", $pager->GetPages());
            $smarty->display("social_polls/list.tpl");
            $smarty->clear_all_assign();
        }

        // ------------------------------------

        else if ($_GET['action'] == "add-form")
        {
            $smarty->display("social_polls/add-form.tpl");
            $smarty->clear_all_assign();
        }

        // ------------------------------------

        else if ($_GET['action'] == "add")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $data['status'] = isset($_POST['status']) ? 1 : 0;
                $data['views_min'] = (int)$_POST['views_min'];
                $data['views_max'] = (int)$_POST['views_max'];
                $data['title'] = trim($_POST['title']);
                $data['questions'] = array();
              
                foreach ($_POST['question_type'] as $k => $v)
                {
                    $data['questions'][$k]['type'] = $v;
                }

                foreach ($_POST['question_text'] as $k => $v)
                {
                    $data['questions'][$k]['text'] = $v;
                }

                foreach ($_POST['question_answers'] as $k => $v)
                {
                    $data['questions'][$k]['answers'] = $v;
                }

                $data['questions'] = json_encode($data['questions']);

                if ($data['status'] == 1)
                {
                    $DB->Execute('UPDATE social_polls SET status=0');
                }

                $social_polls->Add($data);

                adm_redirect("?module=social_polls&action=list");
            }
        }

        // ------------------------------------

        else if ($_GET['action'] == "edit-form" && isset($_GET['id']))
        {
            $id = (int)$_GET['id'];
            $content = $DB->GetRow("SELECT * FROM social_polls WHERE id=?", array($id));
            $content['questions'] = json_decode($content['questions'], true);

            $smarty->assign("content", $content);
            $smarty->display("social_polls/edit-form.tpl");
            $smarty->clear_all_assign();
        }

        // ------------------------------------

        else if ($_GET['action'] == "edit" && isset($_GET['id']))
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $id = (int)$_GET['id'];

                $data['id'] = $id;
                $data['status'] = isset($_POST['status']) ? 1 : 0;
                $data['views_min'] = (int)$_POST['views_min'];
                $data['views_max'] = (int)$_POST['views_max'];
                $data['title'] = trim($_POST['title']);
                $data['questions'] = array();

                foreach ($_POST['question_type'] as $k => $v)
                {
                    $data['questions'][$k]['type'] = $v;
                }

                foreach ($_POST['question_text'] as $k => $v)
                {
                    $data['questions'][$k]['text'] = $v;
                }

                foreach ($_POST['question_answers'] as $k => $v)
                {
                    $data['questions'][$k]['answers'] = $v;
                }


                $data['questions'] = json_encode($data['questions']);

                if ($data['status'] == 1)
                {
                    $DB->Execute('UPDATE social_polls SET status=0');
                }

                $social_polls->Edit($data);

                adm_redirect("?module=social_polls&action=list");
            }
        }

        // ------------------------------------

        else if ($_GET['action'] == "delete" && isset($_GET['id']))
        {
            $id = (int)$_GET['id'];
            $social_polls->Delete($id);

            adm_redirect("?module=social_polls&action=list");
        }
    }
}
else
{
    adm_message($control->GetMessage());
}