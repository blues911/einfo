<?php

include_once(PATH_BACKEND . '/php/classes/class.User.php');

defined("ADMIN") or die("FAIL");

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {

        if ($_GET['action'] == 'list')
        {
            // Приводим даты к метке Unix
            $date = array();

            $log_start = strtotime($DB->GetOne('SELECT MIN(`date`) FROM users_status WHERE user_id NOT IN(?, ?)', array(USER_ADMIN_ID, USER_PARTNER_ID)));
            $month_start = strtotime('first day of previous month');

            $default_start = $month_start > $log_start ? $month_start : $log_start;

            $date['from_ts'] = empty($_POST['date_from']) ? $default_start : strtotime($_POST['date_from'] . ' 00:00:00');
            $date['to_ts'] = empty($_POST['date_to']) ? time() : strtotime($_POST['date_to'] . ' 23:59:59');
            $date['from_ts'] = $date['from_ts'] < $log_start ? $log_start : $date['from_ts'];
            $date['to_ts'] = $date['to_ts'] > time() ? time() : $date['to_ts'];
            $date['period'] = $date['to_ts'] - $date['from_ts'];
            $date['from'] = date('d.m.Y', $date['from_ts']);
            $date['to'] = date('d.m.Y', $date['to_ts']);
            $date['from_search'] = date('Y-m-d', $date['from_ts']) . ' 00:00:00';
            $date['to_search'] = date('Y-m-d', $date['to_ts']) . ' 23:59:59';

            $_user = new User($DB);
            $user_title = isset($_POST['user_title']) ? htmlspecialchars($_POST['user_title']) : '';

            $status_check = isset($_POST['status_check']) ? (bool)$_POST['status_check'] : false;
            $access_level = !empty($_POST['access_level']) ? (int)$_POST['access_level'] : null;

            $list = $_user->get_list(array('title_or_id_like' => $user_title, 'without_admins' => true));

            // Ид пользователей
            $users = array();
            foreach ($list as $key => $user)
            {
                $users[$user['id']] = $user;
            }
            unset($list);

            // Загрузка таймлайна
            $timeline = $DB->GetAll('
                SELECT *
                FROM users_status
                WHERE date BETWEEN ? AND ?
                    AND user_id NOT IN(?, ?)
                ORDER BY date ASC
            ', array(
                    $date['from_search'],
                    $date['to_search'],
                    USER_ADMIN_ID,
                    USER_PARTNER_ID
                )
            );

            // Загрузка последних статусов до периода поиска
            $_previous = $DB->GetAll('
                SELECT
                  a.user_id,
                  a.status,
                  a.access_level,
                  a.date
                FROM
                  users_status AS a
                  INNER JOIN (
                    SELECT user_id, MAX(`date`) AS max_date
                    FROM users_status
                    WHERE `date` < ?
                    GROUP BY user_id
                  ) AS b ON b.user_id = a.user_id AND b.max_date = a.date
            ', array($date['from_search']));

            $previous_status = array();
            foreach(array_keys($_previous) as $k)
            {
                $previous_status[$_previous[$k]['user_id']] = $_previous[$k];
                unset($_previous[$k]);
            }

            // Сортируем таймлайны по пользователям
            foreach ($timeline as $k => $tl)
            {
                $_id = (int)$tl['user_id'];
                if (isset($users[$_id]))
                {
                    $users[$_id]['timeline'][] = $tl;
                }
            }

            $i = 1;
            foreach ($users as $key => $user)
            {
                // Условие активности в указанный период
                $_add = !(bool)$status_check;

                // Строим таймлайн
                $tm = array();
                if (!empty($user['timeline']))
                {
                    $_last = count($user['timeline']) - 1;
                    for ($k = 0; $k <= $_last; $k++)
                    {
                        $v = $user['timeline'][$k];

                        $_period = 0;

                        $time_prev = isset($user['timeline'][$k-1]['date']) ? strtotime($user['timeline'][$k-1]['date']) : 0;
                        $time = strtotime($v['date']);
                        $time_next = isset($user['timeline'][$k+1]['date']) ? strtotime($user['timeline'][$k+1]['date']) : time();

                        // Если начинается до периода и заканчивается после него
                        if ($time <= $date['from_ts'] && $time_next >= $date['to_ts'])
                        {
                            $_period = $date['period'];
                        }
                        // Если начинается до периода и заканчивается внутри него
                        elseif ($time <= $date['from_ts'] && $time_next < $date['to_ts'])
                        {
                            $_period = $time_next - $date['from_ts'];
                        }
                        // Если начинается и заканчивается внутри периода
                        elseif ($time > $date['from_ts'] && $time < $date['to_ts'] && $time_next < $date['to_ts'])
                        {
                            $_period = $time_next - $time;
                        }
                        // Если начинается в периоде и заканчивается за пределами периода
                        elseif ($time > $date['from_ts'] && $time < $date['to_ts'] && $time_next >= $date['to_ts'])
                        {
                            $_period = $date['to_ts'] - $time;
                        }

                        // Если предыдущего нет, то делаем отступ
                        if ($time_prev == 0 && $time > $date['from_ts'])
                        {
                            $_tm = array();
                            $_tm['width'] = (($time - $date['from_ts']) / $date['period'] * 100);

                            $_tm['status'] = isset($previous_status[$user['id']]) ? $previous_status[$user['id']]['status'] : -2;
                            $_tm['access_level'] = isset($previous_status[$user['id']]) ? $previous_status[$user['id']]['access_level'] : null;

                            $_tm['title'] = 'до ' . date('d.m.Y', $time);
                            $tm[] = $_tm;
                        }

                        // Добавляем спан в таймлайн
                        if ($_period)
                        {
                            $_tm = array();
                            $_tm['width'] = ($_period / $date['period'] * 100);
                            $_tm['status'] = $v['status'];
                            $_tm['access_level'] = $v['access_level'];
                            $_tm['title'] = 'с ' . date('d.m.Y', $time);

                            if ($k == $_last)
                            {
                                $_tm['title'] .= ' по ' . $date['to'];
                            }
                            else
                            {
                                $_tm['title'] .= ' по ' . date('d.m.Y', $time_next);
                            }

                            $tm[] = $_tm;
                        }
                    }

                    // Проверка признака активности в указанный период
                    foreach ($tm as $_tm)
                    {
                        if ($_tm['status'] == 1)
                        {
                            $_add = true;
                        }
                    }

                    // Проверка уровня доступа
                    if (isset($access_level))
                    {
                        $_add = false;

                        foreach ($tm as $_tm)
                        {
                            if ($_tm['access_level'] == $access_level)
                            {
                                $_add = true;
                            }
                        }
                    }
                }
                else
                {
                    $_tm = array();
                    $_tm['width'] = 100;

                    // Поиск
                    $_status = $DB->GetRow('
                        SELECT
                          `status`,
                          access_level
                        FROM
                          users_status
                        WHERE
                          user_id = ?
                          AND date < ?
                        ORDER BY
                          date DESC
                        LIMIT 1
                    ', array($user['id'], $date['from_search']));

                    if (!empty($_status))
                    {
                        $_tm['status'] = $_status['status'] === false ? -2 : $_status['status'];
                        $_tm['access_level'] = isset($_status['access_level']) ? $_status['access_level'] : null;
                    }
                    else
                    {
                        $_tm['status'] = -2;
                        $_tm['access_level'] = null;
                    }

                    if ($status_check)
                    {
                        $_add = $_tm['status'] == 1 ? true : false;
                    }
                    else
                    {
                        $_add = true;
                    }

                    // Проверка уровня доступа
                    if (isset($access_level))
                    {
                        $_add = false;

                        foreach ($tm as $_tm)
                        {
                            if ($_tm['access_level'] == $access_level)
                            {
                                $_add = true;
                            }
                        }
                    }

                    $_tm['title'] = 'Весь период';
                    $tm[] = $_tm;
                }

                if ($_add)
                {
                    $users[$key]['tm'] = $tm;
                    $users[$key]['num'] = $i;
                    $i++;
                }
                else
                {
                    unset($users[$key]);
                }

            }

            $access_levels = $DB->GetAll('SELECT * FROM access_levels');

            $smarty->assign('users', $users);
            $smarty->assign('date', $date);
            $smarty->assign('user_title', $user_title);
            $smarty->assign('status_check', $status_check);
            $smarty->assign('access_levels', $access_levels);
            $smarty->assign('access_level', $access_level);
            $smarty->display('users_status/list.tpl');
            $smarty->clear_all_assign();
        }

    }
}