<?php

defined("ADMIN") or die("FAIL");

define('ITEMS_ON_PAGE', 50);

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {
        // категории
        $category_list = $DB->GetAll("SELECT id, c_level, title FROM categories ORDER BY c_left ASC");
        $smarty->assign('category_list', $category_list);

        // Шаблоны объявлений
        if ($_GET['action'] == 'list')
        {
            $category_id = !empty($_GET['category_id']) ? (int)$_GET['category_id'] : 1;
            $category = $DB->GetRow('SELECT c_left, c_right FROM categories WHERE id = ?', array($category_id));

            $counter = $DB->GetOne('
                SELECT
                  COUNT(*)
                FROM
                  adv_template AS adv
                  INNER JOIN categories ON categories.id = adv.category_id
                WHERE
                  categories.c_left BETWEEN ? AND ?
            ', array($category['c_left'], $category['c_right']));
            $pager = new Pager($counter, ITEMS_ON_PAGE, 'dynamic');

            // выбор элементов
            $ads = $DB->GetAll('
                SELECT
                  adv.id,
                  categories.full_title AS category_title,
                  adv.comp_like,
                  adv.yandex_title,
                  adv.yandex_descr
                FROM
                  adv_template AS adv
                  INNER JOIN categories ON categories.id = adv.category_id
                WHERE
                  categories.c_left BETWEEN ? AND ?
                ORDER BY adv.id DESC
                LIMIT ' . $pager->GetLimitStart() . ', ' . $pager->GetLimitEnd()
            , array($category['c_left'], $category['c_right']));

            $num = $pager->GetLimitStart() + 1;
            foreach(array_keys($ads) as $k)
            {
                $ads[$k]['num'] = $num;
                $num++;
            }

            $smarty->assign('ads', $ads);
            $smarty->assign('pager', $pager->GetPages());
            $smarty->assign('category_id', $category_id);
            $smarty->display('advertising/list.tpl');
        }

        // Форма добавления
        if ($_GET['action'] == 'add_form')
        {
            $smarty->display('advertising/add_form.tpl');
        }

        // Добавление шаблона объявления
        if ($_GET['action'] == 'add')
        {
            $ins = array();
            $ins['category_id'] = (int)$_POST['category_id'];
            $ins['comp_like'] = $_POST['comp_like'];
            $ins['yandex_title'] = $_POST['yandex_title'];
            $ins['yandex_descr'] = $_POST['yandex_descr'];
            $ins['yandex_adtitle_1'] = $_POST['yandex_adtitle_1'];
            $ins['yandex_adtitle_2'] = $_POST['yandex_adtitle_2'];
            $ins['yandex_adtitle_3'] = $_POST['yandex_adtitle_3'];
            $ins['yandex_adtitle_4'] = $_POST['yandex_adtitle_4'];
            $ins['yandex_adlink_1'] = $_POST['yandex_adlink_1'];
            $ins['yandex_adlink_2'] = $_POST['yandex_adlink_2'];
            $ins['yandex_adlink_3'] = $_POST['yandex_adlink_3'];
            $ins['yandex_adlink_4'] = $_POST['yandex_adlink_4'];

            $DB->AutoExecute('adv_template', $ins, 'INSERT');
            adm_redirect('?module=advertising&action=list');
        }

        // Форма редактирования
        if ($_GET['action'] == 'edit_form')
        {
            $ad = $DB->GetRow('SELECT * FROM adv_template WHERE id = ?', array((int)$_GET['id']));

            $smarty->assign('ad', $ad);
            $smarty->display('advertising/edit_form.tpl');
        }

        // Редактирование шаблона объявления
        if ($_GET['action'] == 'edit')
        {
            $upd = array();
            $upd['category_id'] = (int)$_POST['category_id'];
            $upd['comp_like'] = $_POST['comp_like'];
            $upd['yandex_title'] = $_POST['yandex_title'];
            $upd['yandex_descr'] = $_POST['yandex_descr'];
            $upd['yandex_adtitle_1'] = $_POST['yandex_adtitle_1'];
            $upd['yandex_adtitle_2'] = $_POST['yandex_adtitle_2'];
            $upd['yandex_adtitle_3'] = $_POST['yandex_adtitle_3'];
            $upd['yandex_adtitle_4'] = $_POST['yandex_adtitle_4'];
            $upd['yandex_adlink_1'] = $_POST['yandex_adlink_1'];
            $upd['yandex_adlink_2'] = $_POST['yandex_adlink_2'];
            $upd['yandex_adlink_3'] = $_POST['yandex_adlink_3'];
            $upd['yandex_adlink_4'] = $_POST['yandex_adlink_4'];

            $DB->AutoExecute('adv_template', $upd, 'UPDATE', 'id=' . (int)$_POST['id']);
            adm_redirect('?module=advertising&action=list');
        }

        // Удаление шаблона объявления
        if ($_GET['action'] == 'delete')
        {
            if (!empty($_GET['id']))
            {
                $DB->Execute('DELETE FROM adv_template WHERE id = ?', array((int)$_GET['id']));
            }

            adm_redirect('?module=advertising&action=list');
        }

        // Настройки контекстной рекламы
        if ($_GET['action'] == 'settings_form')
        {
            // Статус обновления токена
            $token_error = '';
            $token_info = '';

            if (!empty($_GET['yandex_auth']))
            {
                if ($_GET['yandex_auth'] == 1)
                {
                    $token_info = 'Токен успешно обновлен';
                }
                else if ($_GET['yandex_auth'] == -1)
                {
                    $token_error = 'Ошибка обновления токена';
                }
            }

            $settings = array('yandex' => array());

            $_settings = $DB->GetAll('SELECT `var`, `value` FROM adv_system WHERE `system` = "yandex"');
            foreach ($_settings as $_set)
            {
                $settings['yandex'][$_set['var']] = $_set['value'];
            }

            $smarty->assign('token_error', $token_error);
            $smarty->assign('token_info', $token_info);
            $smarty->assign('settings', $settings);
            $smarty->display('advertising/settings_form.tpl');
        }

        // Сохранение настроек контекстной рекламы
        if ($_GET['action'] == 'settings_edit')
        {
            $yandex = array(
                'banner_descr'          => $_POST['yandex_banner_descr'],
                'banner_price'          => $_POST['yandex_banner_price'],
                'banner_title'          => $_POST['yandex_banner_title'],
                'campaign_prefix'       => $_POST['yandex_campaign_prefix'],
                'catalog_path'          => $_POST['yandex_catalog_path'],
                'yandex_application_id' => $_POST['yandex_application_id'],
                'yandex_application_pwd'=> $_POST['yandex_application_pwd'],
                'yandex_login'          => $_POST['yandex_login'],
                'yandex_token'          => $_POST['yandex_token'],
                'yandex_adtitle_1'      => $_POST['yandex_adtitle_1'],
                'yandex_adlink_1'       => $_POST['yandex_adlink_1'],
                'yandex_adtitle_2'      => $_POST['yandex_adtitle_2'],
                'yandex_adlink_2'       => $_POST['yandex_adlink_2'],
                'yandex_adtitle_3'      => $_POST['yandex_adtitle_3'],
                'yandex_adlink_3'       => $_POST['yandex_adlink_3'],
                'yandex_adtitle_4'      => $_POST['yandex_adtitle_4'],
                'yandex_adlink_4'       => $_POST['yandex_adlink_4']
            );

            foreach ($yandex as $var => $value)
            {
                $DB->Execute('UPDATE adv_system SET `value` = ? WHERE `system` = ? AND `var` = ?', array($value, 'yandex', $var));
            }

            adm_redirect('?module=advertising&action=settings_form');
        }

    }
}