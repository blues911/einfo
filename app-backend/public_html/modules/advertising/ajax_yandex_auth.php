<?php

require_once(PATH_BACKEND . '/php/classes/class.AdvYandex.php');

$success = -1;

if(!empty($_GET['code']))
{

    $yandex = new AdvYandex($DB);
    $postvars = $yandex->get_authorization_param($_GET['code']);

    $postdata = http_build_query($postvars);

    $curl = curl_init();
    curl_setopt ($curl, CURLOPT_URL, 'https://oauth.yandex.ru/token');
    curl_setopt ($curl, CURLOPT_POST, 1);
    curl_setopt ($curl, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
    $response = json_decode(curl_exec($curl), true);
    curl_close($curl);

    if (!empty($response['access_token']))
    {
        $yandex->update_token($response['access_token']);

        $success = 1;
    }
    else
    {
        $success = -1;
    }
}
else
{
    $success = -1;
}

adm_redirect('?module=advertising&action=settings_form&yandex_auth=' . $success);
//header('Location: http://dev.einfo.su:8080/?module=advertising&action=settings_form&yandex_auth=' . $success);
