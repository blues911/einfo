<?php

include_once (PATH_BACKEND . '/php/classes/class.Price.php');
include_once (PATH_BACKEND . '/php/classes/class.Spreadsheet_Excel_Reader.php');
include_once (PATH_BACKEND . '/php/classes/class.PdfUpdate.php');
include_once (PATH_BACKEND . '/php/classes/class.PdfParse.php');

defined("ADMIN") or die("FAIL");

define('ITEMS_ON_PAGE', 50);

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {
        $pdf_update = new PdfUpdate($DB);
        $pdf_parse = new PdfParse($DB);

        // Список документации
        if ($_GET['action'] == 'list')
        {
            $like = !empty($_GET['like']) ? trim($_GET['like']) : '';
            $view_all = !empty($_GET['view_all']) ? true : false;

            if (!$view_all)
            {
                $counter = $DB->GetOne('SELECT COUNT(*) FROM datasheet WHERE comp_canon LIKE ?', array($like . '%'));
                $pager = new Pager($counter, ITEMS_ON_PAGE, 'dynamic');

                $limit = ' LIMIT ' . $pager->GetLimitStart() . ', ' . $pager->GetLimitEnd();
                $num = $pager->GetLimitStart() + 1;

                $smarty->assign('pager', $pager->GetPages());
            }
            else
            {
                $limit = '';
                $num = 1;
            }

            $list = $DB->GetAll('
                SELECT
                  *
                FROM
                  datasheet
                WHERE
                  comp_canon LIKE ?
                ORDER BY comp_canon ASC
                ' . $limit
            , array($like . '%'));

            foreach (array_keys($list) as $k)
            {
                $list[$k]['num'] = $num;
                $list[$k]['local_href'] = 'http://' . HOST_FRONTEND . '/datasheet/' . $list[$k]['comp_canon'] . '.pdf';
                $num++;
            }

            $smarty->assign('like', $like);
            $smarty->assign('view_all', $view_all);
            $smarty->assign('datasheets', $list);
            $smarty->display('datasheet/list.tpl');
        }

        // Удаление
        if ($_GET['action'] == 'delete')
        {
            $datasheet = $DB->GetRow('SELECT `comp_canon`,`local`,`img` FROM datasheet WHERE `comp_canon` = ?', array($_GET['comp_canon']));

            $pdf_update->delete_pdf($datasheet['local']);
            $pdf_parse->delete_img($datasheet['img']);
            $DB->Execute('DELETE FROM datasheet WHERE comp_canon = ?', array($datasheet['comp_canon']));
            adm_redirect('?module=datasheet&action=list');
        }

        // Групповые действия
        if ($_GET['action'] == 'group_action')
        {
            $list = !empty($_POST['datasheet']) ? array_keys($_POST['datasheet']) : array();
            if (!empty($list))
            {
                if ($_POST['do'] == 'delete')
                {
                    $datasheets = $DB->GetAll('
                        SELECT
                          `comp_canon`,
                          `local`,
                          `img`
                        FROM
                          datasheet
                        WHERE
                          `comp_canon` IN (' . implode(',', $list) . ')
                    ');

                    foreach ($datasheets as $ds)
                    {
                        $pdf_update->delete_pdf($ds['local']);
                        $pdf_parse->delete_img($ds['img']);
                    }

                    $DB->Execute('
                        DELETE FROM
                          datasheet
                        WHERE
                          comp_canon IN (' . implode(',', $list) . ')
                    ');
                }
            }

            adm_redirect('?module=datasheet&action=list');
        }

        // Форма добавления
        if ($_GET['action'] == 'add_form')
        {
            $smarty->display('datasheet/add_form.tpl');
        }

        // Добавление
        if ($_GET['action'] == 'add')
        {
            $DB->Execute('
                INSERT INTO
                  datasheet(`comp_canon`,`remote`,`local`,`img`,`size`,`update`)
                VALUES
                  (?, ?, "", "", 0, "0000-00-00 00:00:00")
                ON DUPLICATE KEY UPDATE
                  `remote` = ?,
                  `update` = "0000-00-00 00:00:00"
            ', array(
                $_POST['comp_canon'],
                $_POST['remote'],
                $_POST['remote']
            ));

            adm_redirect('?module=datasheet&action=list');
        }

        // Форма редактирования
        if ($_GET['action'] == 'edit_form')
        {
            $datasheet = $DB->GetRow('SELECT * FROM datasheet WHERE comp_canon = ?', $_GET['comp_canon']);

            $smarty->assign('datasheet', $datasheet);
            $smarty->display('datasheet/edit_form.tpl');
        }

        // Редактирование
        if ($_GET['action'] == 'edit')
        {
            if ($comp = $DB->GetRow('SELECT comp_canon, `remote` FROM datasheet WHERE comp_canon = ?', $_POST['comp_canon_old']))
            {
                $upd = array();
                $upd['comp_canon'] = $_POST['comp_canon'];
                $upd['remote'] = $_POST['remote'];
                if ($upd['remote'] != $comp['remote'])
                {
                    $upd['update'] = '0000-00-00 00:00:00';
                    $DB->Execute('
                        UPDATE
                          datasheet
                        SET
                          comp_canon = ?,
                          `remote` = ?,
                          `update` = ?
                        WHERE
                          comp_canon = ?
                    ', array(
                        $upd['comp_canon'],
                        $upd['remote'],
                        $upd['update'],
                        $_POST['comp_canon_old']
                    ));
                }
                else
                {
                    $DB->Execute('UPDATE datasheet SET comp_canon = ? WHERE comp_canon = ?', array($upd['comp_canon'], $_POST['comp_canon_old']));
                }
            }

            adm_redirect('?module=datasheet&action=list');
        }

        // Импорт
        if ($_GET['action'] == 'import')
        {
            $import_result = false;

            // Импорт прайс-листа
            if (!empty($_POST['action_import']))
            {
                if (!empty($_FILES['import_file']['name']))
                {
                    $file = $_FILES['import_file']['tmp_name'];

                    // import pdf url
                    $pdf_list = new Price($file);

                    while (($item = $pdf_list->get_next_row()) !== false)
                    {
                        if (isset($item[0]) && isset($item[1]))
                        {
                            $DB->Execute('
                                INSERT INTO
                                  datasheet(`comp_canon`,`remote`,`local`,`img`,`size`,`update`)
                                VALUES
                                  (?, ?, "", "", 0, "0000-00-00 00:00:00")
                                ON DUPLICATE KEY UPDATE
                                  `remote` = ?,
                                  `update` = "0000-00-00 00:00:00"
                            ', array(
                                trim($item[0]),
                                trim($item[1]),
                                trim($item[1])
                            ));
                        }
                    }

                    $import_result['success'] = true;
                }
                else
                {
                    $import_result['error'] = true;
                }
            }

            $smarty->assign('import_result', $import_result);
            $smarty->display('datasheet/import.tpl');
        }

        // Экспорт
        if ($_GET['action'] == 'export')
        {
            // Список категорий
            $category_list = $DB->GetAll("SELECT id, c_level, title FROM categories WHERE c_level!=0 ORDER BY c_left ASC");
            $smarty->assign('category_list', $category_list);

            $smarty->display('datasheet/export.tpl');
        }

    }
}