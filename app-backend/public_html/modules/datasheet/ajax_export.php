<?php

$file_data = '';

// ---

$category_id = !empty($_POST['category_id']) ? (int)$_POST['category_id'] : 1;
$without_pdf = !empty($_POST['without_pdf']) ? true : false;

$category = $DB->GetRow('SELECT c_left, c_right FROM categories WHERE id = ?', array($category_id));

$list = $DB->GetAll('
    SELECT
      canon.comp_canon AS title,
      datasheet.`remote` AS pdf_remote
    FROM
      catalog_comp_canon AS canon
      INNER JOIN categories ON canon.category_id = categories.id
      LEFT OUTER JOIN datasheet ON canon.comp_canon = datasheet.comp_canon
    WHERE
      categories.c_left BETWEEN ? AND ?
', array($category['c_left'], $category['c_right']));

if ($without_pdf)
{
    foreach ($list as $item)
    {
        $file_data .= $item['title'] . "\r\n";
    }
}
else
{
    foreach ($list as $item)
    {
        $file_data .= $item['title'] . "\t" . $item['pdf_remote'] . "\r\n";
    }
}

// Отдача в браузер на скачивание
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Cache-Control: private', false);
header('Content-Type: text/plain');
header('Content-Disposition: attachment; filename=Export_components_' . date('d-m-Y') . '.txt');
header('Content-Length: ' . mb_strlen($file_data));

echo $file_data;