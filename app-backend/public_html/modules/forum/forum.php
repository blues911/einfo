<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

if ($control->CheckModuleAccess($_GET['module']))
{
    // ------------------------------------

	if (isset($_GET['action']))
	{
	    $level = $DB->GetOne("SELECT level_id FROM users WHERE id=?", array($_SESSION['user_id']));
	    $smarty->assign("admin_level", $level == 0);


	    // ------------------------------------
	    if ($_GET['action'] == "add-topic-form")
	    {
	        $smarty->display("forum/add-topic.tpl");
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "add-topic")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $ins['date'] = date("Y-m-d H:i:s");
                $ins['title'] = $_POST['title'];
                $ins['text'] = $_POST['text'];
                $ins['user_id'] = $_SESSION['user_id'];

                $DB->AutoExecute("forum_topics", $ins, "INSERT");

                adm_redirect("?module=forum&action=list");
            }
	    }
	    // ------------------------------------


	    // ------------------------------------
	    if ($_GET['action'] == "edit-topic-form" && isset($_GET['id']))
	    {
	        $topic_id = (int)$_GET['id'];
	        $allow_user_id = $DB->GetOne("SELECT user_id FROM forum_topics WHERE id=?", array($topic_id));

	        if ($level == 0 || ($allow_user_id && $allow_user_id == $_SESSION['user_id']))
	        {
    	        $content = $DB->GetRow("SELECT * FROM forum_topics WHERE id=?", array($topic_id));
    	        $smarty->assign("content", $content);
    	        $smarty->display("forum/edit-topic.tpl");
    	    }
    	    else
    	    {
    	        adm_message("Доступ запрещен.");
    	    }
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit-topic")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_GET['id']))
            {
                $topic_id = (int)$_GET['id'];
    	        $allow_user_id = $DB->GetOne("SELECT user_id FROM forum_topics WHERE id=?", array($topic_id));

    	        if ($level == 0 || ($allow_user_id && $allow_user_id == $_SESSION['user_id']))
    	        {
                    $upd['title'] = $_POST['title'];
                    $upd['text'] = $_POST['text'];

                    $DB->AutoExecute("forum_topics", $upd, "UPDATE", "id=".$topic_id);

                    adm_redirect("?module=forum&action=view-topic&id=".$topic_id);
                }
        	    else
        	    {
        	        adm_message("Доступ запрещен.");
        	    }
            }
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "add-message")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $ins['date'] = date("Y-m-d H:i:s");
                $ins['text'] = $_POST['text'];
                $ins['topic_id'] = (int)$_POST['topic_id'];
                $ins['user_id'] = $_SESSION['user_id'];

                $DB->AutoExecute("forum_messages", $ins, "INSERT");

                adm_redirect("?module=forum&action=view-topic&id=".$ins['topic_id']);
            }
	    }
	    // ------------------------------------


	    // ------------------------------------
	    if ($_GET['action'] == "edit-message-form" && isset($_GET['id']))
	    {
	        $message_id = (int)$_GET['id'];
	        $allow_user_id = $DB->GetOne("SELECT user_id FROM forum_messages WHERE id=?", array($message_id));

	        if ($level == 0 || ($allow_user_id && $allow_user_id == $_SESSION['user_id']))
	        {
    	        $content = $DB->GetRow("SELECT message.*, topic.title AS topic_title, topic.id AS topic_id
    	                                FROM forum_messages message
    	                                    INNER JOIN forum_topics topic ON topic.id=message.topic_id
    	                                WHERE message.id=?", array($message_id));

    	        $smarty->assign("content", $content);
    	        $smarty->display("forum/edit-message.tpl");
    	    }
    	    else
    	    {
    	        adm_message("Доступ запрещен.");
    	    }
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit-message")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_GET['id']))
            {
                $message_id = (int)$_GET['id'];
    	        $message = $DB->GetRow("SELECT user_id, topic_id FROM forum_messages WHERE id=?", array($message_id));

    	        if ($level == 0 || ($message['user_id'] && $message['user_id'] == $_SESSION['user_id']))
    	        {
                    $upd['text'] = $_POST['text'];
                    $DB->AutoExecute("forum_messages", $upd, "UPDATE", "id=".$message_id);

                    adm_redirect("?module=forum&action=view-topic&id=".$message['topic_id']);
                }
        	    else
        	    {
        	        adm_message("Доступ запрещен.");
        	    }
            }
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "list")
        {
            $all_items = $DB->GetOne("SELECT COUNT(*) FROM forum_topics");
            $pager = new Pager($all_items, $SETTINGS['forum']['topics_on_page'], "dynamic");

            // информации о просмотре топика (для проставление метки "new")
            if ($level == 0)
            {
                if (!empty($_COOKIE['forum_update']))
                {
                    $forum_cookie = unserialize($_COOKIE['forum_update']);
                }
            }

            $list = $DB->GetAll("SELECT topic.*, user.title AS user_title, MAX(message.date) AS message_date, COUNT(message.topic_id) AS nmess
                                 FROM forum_topics topic
                                    LEFT JOIN users user ON user.id=topic.user_id
                                	LEFT JOIN forum_messages message ON message.topic_id=topic.id
                                 GROUP BY topic.id
                                 ORDER BY (pinned = 1) DESC, IFNULL(MAX(message.date), topic.date) DESC
                                 LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());
            foreach ($list as $k => $value)
            {
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $list[$k]['message_date'] = $value['message_date'] ? $date->GetRusDate($value['message_date'], 4) : null;
                $list[$k]['topic_update'] = !isset($forum_cookie[$value['id']]) || strtotime($value['message_date']) > $forum_cookie[$value['id']];
            }

            $smarty->assign("list", $list);
        	$smarty->assign("pager", $pager->GetPages());
            $smarty->display("forum/list.tpl");
            $smarty->clear_all_assign();
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "search-form")
        {
            $smarty->display("forum/search-form.tpl");
            $smarty->clear_all_assign();
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "view-topic" && isset($_GET['id']))
        {
            $topic_id = (int)$_GET['id'];

            $topic = $DB->GetRow("SELECT topic.*, user.id AS user_id, user.title AS user_title
                                  FROM forum_topics topic
                                      LEFT JOIN users user ON user.id=topic.user_id
                                  WHERE topic.id=?", array($topic_id));
            $topic['date'] = $date->GetRusDate($topic['date'], 4);
            $topic['allow_edit'] = $topic['user_id'] == $_SESSION['user_id'];


            $all_items = $DB->GetOne("SELECT COUNT(*) FROM forum_messages WHERE topic_id=?", array($topic_id));
            $pager = new Pager($all_items, $SETTINGS['forum']['messages_on_page'], "dynamic");

            $messages = $DB->GetAll("SELECT message.*, user.id AS user_id, user.title AS user_title
                                     FROM forum_messages message
                                         LEFT JOIN users user ON user.id=message.user_id
                                     WHERE message.topic_id=?
                                     ORDER BY message.date ASC
                                     LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd(), array($topic_id));
            foreach ($messages as $k => $value)
            {
                $messages[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $messages[$k]['allow_edit'] = $value['user_id'] == $_SESSION['user_id'];
            }

            // обновление информации о просмотре топика
            if ($level == 0)
            {
                $forum_cookie = array();
                if (!empty($_COOKIE['forum_update']))
                {
                    $forum_cookie = unserialize($_COOKIE['forum_update']);
                }

                $forum_cookie[$topic_id] = time();
                setcookie("forum_update", serialize($forum_cookie), time() + 3600 * 24 * 36);
            }

    	    $smarty->assign("topic", $topic);
    	    $smarty->assign("messages", $messages);
    	    $smarty->assign("pager", $pager->GetPages());
    	    $smarty->display("forum/view-topic.tpl");
    	    $smarty->clear_all_assign();
        }
        // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>