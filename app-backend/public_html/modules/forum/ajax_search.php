<?php

if (isset($_GET['q']) && mb_strlen($_GET['q']) >= 3)
{
    $q = stripslashes(strip_tags(trim($_GET['q'])));
    $atoms = get_request_atoms($q);
    $qlike = "%".implode("%", $atoms)."%";

    $list = $DB->GetAll("SELECT topic.*, user.title AS user_title, MAX(message.date) AS message_date, COUNT(message.topic_id) AS nmess
                         FROM forum_topics topic
                            LEFT JOIN users user ON user.id=topic.user_id
                            LEFT JOIN forum_messages message ON message.topic_id=topic.id
                         WHERE topic.title LIKE '".$qlike."'
                               OR topic.text LIKE '".$qlike."'
                               OR message.text LIKE '".$qlike."'
                         GROUP BY topic.id
                         ORDER BY (pinned = 1) DESC, IFNULL(MAX(message.date), topic.date) DESC");

    foreach ($list as $k => $value)
    {
        $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
        $list[$k]['message_date'] = $value['message_date'] ? $date->GetRusDate($value['message_date'], 4) : null;
    }

    $smarty->assign("q", $q);
    $smarty->assign("list", $list);
    $smarty->display("forum/search-result.tpl");
}

?>