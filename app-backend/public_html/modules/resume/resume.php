<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_RESUME_ONPAGE", 50);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    include_once (PATH_BACKEND . "/php/classes/class.Resume.php");
	    $resume = new resume($DB, $_SESSION['user_id']);


	    // ------------------------------------
	    if ($_GET['action'] == "add-form")
	    {
	        $smarty->display("resume/add-form.tpl");
	    }
	    // ------------------------------------

      	// ------------------------------------
        else if ($_GET['action'] == "add")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $ins['date'] = date("Y-m-d H:i:s");
                $ins['user_id'] = $_SESSION['user_id'];
                $ins['fio'] = $_POST['fio'];
                $ins['sex'] = $_POST['sex'];
                $ins['speciality'] = $_POST['speciality'];
                $ins['experience'] = (int)$_POST['experience'];
                $ins['age'] = (int)$_POST['age'];
                $ins['education'] = (int)$_POST['education'];
                $ins['city'] = $_POST['city'];
                $ins['workplace'] = (int)$_POST['workplace'];
                $ins['worktype'] = (int)$_POST['worktype'];
                $ins['workgraphic'] = (int)$_POST['workgraphic'];
                $ins['language'] = $_POST['language'];
                $ins['pay'] = (int)$_POST['pay'];
                $ins['contact_phone'] = $_POST['contact_phone'];
                $ins['contact_email'] = $_POST['contact_email'];
                $ins['text'] = $_POST['text'];

                $resume->Add($ins);

                adm_redirect("?module=resume&action=list");
            }
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "list")
        {
            $sql_order = (!isset($_GET['order']) || $_GET['order'] == "desc") ? "desc" : "asc";

            $all_items = $DB->GetOne("SELECT COUNT(*) FROM resume");

            $pager = new Pager($all_items, MAX_RESUME_ONPAGE, "dynamic");

            $list = $DB->GetAll("SELECT id, speciality, date FROM resume WHERE user_id=? ORDER BY date ".$sql_order." LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd(), array($_SESSION['user_id']));
            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $list[$k] = $value;
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
            $smarty->assign("list_order", $sql_order);
        	$smarty->assign("pager", $pager->GetPages());
            $smarty->display("resume/list.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "edit-form" && isset($_GET['id']))
        {
            $id = (int)$_GET['id'];

            if ($resume->CheckAccess($id))
            {
                $content = $DB->GetRow("SELECT * FROM resume WHERE id=?", array((int)$_GET['id']));
    	        $smarty->assign("content", $content);
        	    $smarty->display("resume/edit-form.tpl");
        	    $smarty->clear_all_assign();
            }
            else adm_message("Действие запрещено");
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "edit" && isset($_GET['id']))
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $id = (int)$_GET['id'];

                if ($resume->CheckAccess($id))
                {
            		$upd['fio'] = $_POST['fio'];
                    $upd['sex'] = $_POST['sex'];
                    $upd['speciality'] = $_POST['speciality'];
                    $upd['experience'] = (int)$_POST['experience'];
                    $upd['age'] = (int)$_POST['age'];
                    $upd['education'] = (int)$_POST['education'];
                    $upd['city'] = $_POST['city'];
                    $upd['workplace'] = (int)$_POST['workplace'];
                    $upd['worktype'] = (int)$_POST['worktype'];
                    $upd['workgraphic'] = (int)$_POST['workgraphic'];
                    $upd['language'] = $_POST['language'];
                    $upd['pay'] = (int)$_POST['pay'];
                    $upd['contact_phone'] = $_POST['contact_phone'];
                    $upd['contact_email'] = $_POST['contact_email'];
                    $upd['text'] = $_POST['text'];

                    $resume->Edit($id, $upd);

                    adm_redirect("?module=resume&action=list");
                }
                else adm_message("Действие запрещено");
            }
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "delete" && isset($_GET['id']))
        {
            $id = (int)$_GET['id'];
            if ($resume->CheckAccess($id))
            {
                $resume->Delete($id);
                adm_redirect("?module=resume&action=list");
            }
            else adm_message("Действие запрещено");
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>