<?php

defined("ADMIN") or die("FAIL");

define('ITEMS_ON_PAGE', 50);

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {

        $category_list = $DB->GetAll("SELECT id, c_level, title FROM categories WHERE c_level!=0 ORDER BY c_left ASC");
        $smarty->assign('category_list', $category_list);

        // Список
        if ($_GET['action'] == 'list')
        {
            $category_id = !empty($_GET['category_id']) ? (int)$_GET['category_id'] : 1;
            $category = $DB->GetRow('SELECT c_left, c_right FROM categories WHERE id = ?', array($category_id));

            $counter = $DB->GetOne('
                SELECT
                  COUNT(*)
                FROM
                  catalog_regexp
                  INNER JOIN categories ON catalog_regexp.category_id = categories.id
                WHERE
                  categories.c_left BETWEEN ? AND ?
            ', array($category['c_left'], $category['c_right']));
            $pager = new Pager($counter, ITEMS_ON_PAGE, 'dynamic');

            $templates = $DB->GetAll('
                SELECT
                  catalog_regexp.id,
                  categories.full_title AS category_title,
                  catalog_regexp.regexp_template
                FROM
                  catalog_regexp
                  INNER JOIN categories ON catalog_regexp.category_id = categories.id
                WHERE
                  categories.c_left BETWEEN ? AND ?
                ORDER BY catalog_regexp.id DESC
                LIMIT ' . $pager->GetLimitStart() . ', ' . $pager->GetLimitEnd()
            , array($category['c_left'], $category['c_right']));

            $num = $pager->GetLimitStart() + 1;
            foreach (array_keys($templates) as $k)
            {
                $templates[$k]['num'] = $num;
                $num++;
            }

            $smarty->assign('templates', $templates);
            $smarty->assign('pager', $pager->GetPages());
            $smarty->assign('category_id', $category_id);
            $smarty->display('canon/list.tpl');
        }

        // Форма добавления
        if ($_GET['action'] == 'add_form')
        {
            $smarty->display('canon/add_form.tpl');
        }

        // Добавление шаблона
        if ($_GET['action'] == 'add')
        {
            $ins = array();
            $ins['regexp_template'] = $_POST['regexp_template'];
            $ins['category_id'] = (int)$_POST['category_id'];
            $ins['need_update'] = 1;

            $DB->AutoExecute('catalog_regexp', $ins, 'INSERT');
            adm_redirect('?module=canon&action=list');
        }

        // Форма редактирования
        if ($_GET['action'] == 'edit_form')
        {
            $template = $DB->GetRow('SELECT * FROM catalog_regexp WHERE id = ?', array((int)$_GET['id']));

            $smarty->assign('template', $template);
            $smarty->display('canon/edit_form.tpl');
        }

        // Редактирование шаблона
        if ($_GET['action'] == 'edit')
        {
            $template = $DB->GetRow('SELECT * FROM catalog_regexp WHERE id = ?', array((int)$_POST['id']));

            $upd = array();
            $upd['regexp_template'] = $_POST['regexp_template'];
            $upd['category_id'] = (int)$_POST['category_id'];

            // Если шаблон изменился, то удаляем компоненты
            if ($upd['regexp_template'] != $template['regexp_template'])
            {
                $upd['need_update'] = 1;
                $DB->Execute('DELETE FROM catalog_regexp_explain WHERE regexp_id = ?', array((int)$_POST['id']));
                $DB->Execute('DELETE FROM catalog_comp_canon WHERE regexp_id = ?', array((int)$_POST['id']));
            }

            // Если изменилась категория, то меняем категорию всем компонентам найденым по этому шаблону
            if ($upd['category_id'] != $template['category_id'])
            {
                $DB->Execute('UPDATE catalog_comp_canon SET category_id = ? WHERE regexp_id = ?', array($upd['category_id'], (int)$_POST['id']));
            }

            $DB->AutoExecute('catalog_regexp', $upd, 'UPDATE', 'id=' . (int)$_POST['id']);
            adm_redirect('?module=canon&action=list');
        }

        // Удаление шаблона
        if ($_GET['action'] == 'delete')
        {
            if (!empty($_GET['id']))
            {
                $DB->Execute('DELETE FROM catalog_regexp WHERE id = ?', array((int)$_GET['id']));
                $DB->Execute('DELETE FROM catalog_regexp_explain WHERE regexp_id = ?', array((int)$_GET['id']));
                $DB->Execute('DELETE FROM catalog_comp_canon WHERE regexp_id = ?', array((int)$_GET['id']));
            }

            adm_redirect('?module=canon&action=list');
        }

    }
}