<?php

require_once(PATH_BACKEND . '/php/classes/class.SimpleRegexp.php');

$max_words = 1000000;       // Кол-во раскрываемых вариантов шаблона, после которого шаблон считается слишком сложным.
$max_variants = 20000;      // Максимальное кол-во возвращаемых атомов в подсказке.
$timeout = 15;              // Таймаут (секунд) выполнения операции.

$response = array();
$response['success'] = 1;
$response['error'] = '';
$response['variants'] = array();
$response['regexp'] = $_POST['regexp'];

if (!empty($_POST['regexp']))
{
    $_timer = time();
    $_timeout_flag = false;

    $_collection = SimpleRegexp::parse_template($_POST['regexp']);
    $regexp = array_pop($_collection);
    if ($regexp->get_count_words() <= $max_words)
    {

        $i = 0;
        $variants = array();

        while (($word = $regexp->get_word($i)) !== false)
        {
            if (time() - $_timer > $timeout)
            {
                $variants = array();
                $_timeout_flag = true;
                break;
            }

            $atoms = implode(' ', get_request_atoms($word));
            $components = $DB->GetAll('
                SELECT
                  atoms
                FROM
                  base_main
                WHERE
                  atoms LIKE ?
                GROUP BY
                  atoms
            ', array($atoms . ' %'));

            if (!empty($components))
            {
                foreach ($components as $comp)
                {
                    $_comp = preg_replace('/^' . $atoms . '/isu', '', $comp['atoms']);
                    if (preg_match('/^[\s]?[\S]+/isu', $_comp, $next_atom))
                    {
                        $variants[] = mb_strtolower(trim($next_atom[0]), 'utf-8');
                    }
                }
            }

            $i++;
        }

        if (!empty($variants))
        {
            $variants = array_unique($variants, SORT_LOCALE_STRING);
            sort($variants, SORT_LOCALE_STRING);

            if (count($variants) > $max_variants)
            {
                $response['error'] = 'Слишком много вариантов.';
            }
            else
            {
                $response['variants'] = $variants;
            }

        }
        else if ($_timeout_flag)
        {
            $response['error'] = 'Истекло время ожидания (' . $timeout . ' сек.).';
        }
        else
        {
            $response['error'] = 'Варианты не найдены.';
        }

    }
    else
    {
        $response['error'] = 'Слишком сложное регулярное выражение.';
    }

}
else
{
    $response['error'] = 'Пустой шаблон.';
}

echo(json_encode($response));
