<?php

$response = array();
$response['success'] = 0;
$response['error'] = '';

if (!empty($_POST['regexp']))
{
    $collection = array();

    // Делаем запрос для каждого регулярного выражения
    foreach($_POST['regexp'] as $regexp)
    {
        if (!empty($regexp[0]) && !empty($regexp[1]))
        {
            $atom_rs = $DB->Execute('SELECT DISTINCT(atoms) FROM base_main WHERE atoms REGEXP ?', array('^' . $regexp[0] . '($| )'));

            // Обрабатываем каждый компонент
            while(!$atom_rs->EOF)
            {
                $_atoms = explode(' ', $atom_rs->fields('atoms'));

                if (!empty($_atoms[$regexp[1] - 1]))
                {
                    // Добавляем найденый атом в коллекцию
                    $collection[] = mb_strtolower($_atoms[$regexp[1] - 1], 'utf-8');
                }

                $atom_rs->MoveNext();
            }
        }
    }

    // Оставляем уникальные атомы и сортируем коллекцию
    if (!empty($collection))
    {
        $collection = array_unique($collection, SORT_LOCALE_STRING);
        sort($collection, SORT_LOCALE_STRING);

        $response['collection'] = $collection;
    }
    else
    {
        $response['error'] = 'Нет вариантов.';
    }

    $response['success'] = 1;
}

echo(json_encode($response));