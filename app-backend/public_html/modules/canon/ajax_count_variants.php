<?php

require_once(PATH_BACKEND . '/php/classes/class.SimpleRegexp.php');

$response = array();
$response['success'] = 0;
$response['count'] = 0;

if (!empty($_POST['template']))
{
    $template = $_POST['template'];
    $response['counter'] = SimpleRegexp::count_words($template);
    $response['formated'] = number_format($response['counter'], 0, '.', ' ');
    $response['success'] = 1;
}

echo json_encode($response);