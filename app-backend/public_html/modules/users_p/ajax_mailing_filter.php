<?php

if (isset($_POST['filter']))
{
    $ids = array();
    switch ($_POST['filter'])
    {
        case "all":
            $ids = $DB->GetCol("SELECT id FROM users WHERE id NOT IN(?, ?)", array(USER_PARTNER_ID, USER_ADMIN_ID));
            break;

        case "active":
            $ids = $DB->GetCol("SELECT id FROM users WHERE id NOT IN(?, ?) AND status=1", array(USER_PARTNER_ID, USER_ADMIN_ID));
            break;

        case "locked":
            $ids = $DB->GetCol("SELECT id FROM users WHERE id NOT IN(?, ?) AND status=0", array(USER_PARTNER_ID, USER_ADMIN_ID));
            break;

        case "wait":
            $ids = $DB->GetCol("SELECT id FROM users WHERE id NOT IN(?, ?) AND status=-1", array(USER_PARTNER_ID, USER_ADMIN_ID));
            break;

        case "price_update_month":
            $ids = $DB->GetCol("SELECT id FROM users WHERE id NOT IN(?, ?) AND price_date < NOW() - INTERVAL 1 MONTH", array(USER_PARTNER_ID, USER_ADMIN_ID));
            break;
    }

    print implode(",", $ids);
}

?>
