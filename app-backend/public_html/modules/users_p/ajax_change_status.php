<?php

include_once(PATH_BACKEND . '/php/classes/class.User.php');
include_once(PATH_BACKEND . "/php/classes/class.Sphinx.php");

$sphinx = new Sphinx($DB);
$response = array();
$response['success'] = false;

do if (!empty($_POST['user_id']))
{
    // Если прислан ID главного администратора или он совпадает с ID текущего пользователя
    if ((int)$_POST['user_id'] == USER_ADMIN_ID || (int)$_POST['user_id'] == USER_PARTNER_ID || (int)$_POST['user_id'] == $_SESSION['user_id'])
    {
        $response['status'] = 0;
        $response['success'] = true;
        break;
    }

    $users = new User($DB, $DB2);

    $user = $DB->GetRow('SELECT id, title, status, email, cnt FROM users WHERE id = ?', array($_POST['user_id']));
    $upd = array('status' => false);
    if ($user['status'] == 1)
    {
        $upd['status'] = 0;
    }
    elseif ($user['status'] == 0)
    {
        $upd['status'] = 1;
    }

    if ($upd['status'] !== false)
    {
        // Обновляем статус пользователя
        $users->Edit($user['id'], $upd);

        // Проверяем изменился ли статус
        $response['status'] = $DB->GetOne('SELECT status FROM users WHERE id = ?', array($user['id']));

        // Обновляем статус предложений пользователя
        if ($user['cnt'] < MAX_CNT_RT_UPDATE)
        {
            if (!empty($response['status']))
            {
                $sphinx->add_user_rt_index($user['id']);
            }
            else
            {
                $sphinx->remove_user_rt_index($user['id']);
            }
        }

        if ($response['status'] == $upd['status'])
        {
            // Отправляем e-mail уведомление
            $mail = new PHPMailer();
            $mail->SetFrom('admin@einfo.ru', 'Einfo');
            $mail->Subject = '[einfo.ru] Изменение статуса аккаунта';

            $smarty->assign('id', $user['id']);
            $smarty->assign('status_old', $user['status']);

            $user['status'] = $upd['status'];
            $smarty->assign('user', $user);

            $mail->Body = $smarty->fetch('users/mail_status.tpl');

            ob_start();
            $email_to = preg_split('/[,;\s]/', $user['email'], -1, PREG_SPLIT_NO_EMPTY);
            foreach ($email_to as $value)
            {
                $mail->AddAddress(trim($value));
            }

            $mail->Send();
            ob_clean();

            $response['success'] = true;
        }

    }
}
while (false);

echo(json_encode($response));