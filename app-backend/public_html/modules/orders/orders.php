<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_ORDERS_ONPAGE", 1000);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        // ------------------------------------
        if ($_GET['action'] == "list")
        {
            $all_items = $DB->GetOne('
                SELECT
                  COUNT(orders.id)
                FROM
                  orders
                  INNER JOIN orders_items AS item ON item.order_id = orders.id
                WHERE
                  item.user_id = ?
                GROUP BY
                  orders.id
            ', array($_SESSION['user_id']));

            $pager = new Pager($all_items, MAX_ORDERS_ONPAGE, "dynamic");

            $list = $DB->GetAll('
                SELECT
                  orders.*,
                  COUNT(item.id) AS cnt
                FROM
                  orders
                  INNER JOIN orders_items AS item ON item.order_id = orders.id
                WHERE
                  item.user_id = ?
                  AND orders.valid = 1
                GROUP BY orders.id
                ORDER BY orders.date DESC
                LIMIT ' . $pager->GetLimitStart() . ', ' . $pager->GetLimitEnd()
            , array($_SESSION['user_id']));

            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $list[$k] = $value;
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $list[$k]['num'] = $i;
                $i++;
            }

			$smarty->assign("list", $list);
			$smarty->assign("pager", $pager->GetPages());
            $smarty->display("orders/list.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "view-form" && isset($_GET['order_id']))
        {
            $order_id = $_GET['order_id'];
            $basket = $DB->GetRow('
                SELECT
                  orders.*,
                  clients.email AS contact_email,
                  clients.name AS contact_name,
                  clients.phone AS contact_phone
                FROM
                  orders
                  INNER JOIN orders_items AS items ON items.order_id = orders.id
                  INNER JOIN clients ON orders.client_id = clients.id
                WHERE
                  items.user_id = ?
                  AND orders.id = ?
            ', array($_SESSION['user_id'], $order_id));
            if ($basket)
            {
                $basket['date'] = $date->GetRusDate($basket['date'], 4);
                $smarty->assign("basket", $basket);

                $basket_items = $DB->GetAll("SELECT * FROM orders_items WHERE user_id=? AND order_id=?", array($_SESSION['user_id'], $basket['id']));
                $smarty->assign("basket_items", $basket_items);

                $smarty->display("orders/view-form.tpl");
                $smarty->clear_all_assign();
            }
            else adm_message("Заявка не найдена");


        }
        // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>
