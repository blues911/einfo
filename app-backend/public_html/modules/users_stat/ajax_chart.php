<?php

$response = 'error';

if (!empty($_POST['action']) && !empty($_POST['user_id']))
{
    // id пользователя
    $user_id = (int)$_POST['user_id'];

    if ($user_id > 0)
    {
        // Получаем метку времени "ОТ"
        if (!empty($_POST['date_from']))
        {
            $timestamp_from = is_int($_POST['date_from']) ? $_POST['date_from'] : strtotime($_POST['date_from']);
        }
        else
        {
            $_date_from = $DB->GetOne('
                SELECT MIN(`date`) FROM users_stat WHERE user_id = ? AND action = ?',
                array($user_id, $_POST['action'])
            );

            $timestamp_from = empty($_date_from) ? time() : strtotime($_date_from);
        }

        // Получаем метку времени "ДО"
        if (!empty($_POST['date_to']))
        {
            $timestamp_to = is_int($_POST['date_to']) ? $_POST['date_to'] : strtotime($_POST['date_to']);
        }
        else
        {
            $timestamp_to = time();
        }

        // Меняем время местами при необходимости
        if ($timestamp_from > $timestamp_to)
        {
            list($timestamp_to, $timestamp_from) = array($timestamp_from, $timestamp_to);
        }

        // Конвертируем метки времени в дату вида 'Y-m-d H:j:s'
        $date_from = date('Y-m-d', $timestamp_from);
        $date_to = date('Y-m-d', $timestamp_to);

        // Выполняем запрос к базе данных
        if ($_POST['action'] == 'items_in_orders')
        {
            $_data = $DB->GetAll(
                'SELECT
                  DATE(orders.date) AS `date`,
                  COUNT(orders_items.id) AS cnt
                FROM
                  orders_items
                  INNER JOIN orders ON orders.id = orders_items.order_id
                WHERE
                  orders_items.user_id = ?
                  AND orders.date BETWEEN ? AND ?
                GROUP BY DATE(orders.date)
                ORDER BY orders.date ASC',
                array(
                    $user_id,
                    $date_from . ' 00:00:00',
                    $date_to . ' 23:59:59'
                )
            );
        }
        else
        {
            $_data = $DB->GetAll('
                SELECT
                  DATE(`date`) AS `date`,
                  COUNT(id) AS cnt
                FROM
                  users_stat
                WHERE
                  user_id = ?
                  AND `action` = ?
                  AND `date` BETWEEN ? AND ?
                GROUP BY DATE(`date`)
                ORDER BY `date` ASC
            ', array(
                $user_id,
                $_POST['action'],
                $date_from . ' 00:00:00',
                $date_to . ' 23:59:59'
            ));
        }

        // Создаём массив вида array[date] = cnt
        $data = array();
        foreach ($_data as $k => $v)
        {
            $data[$v['date']] = $v['cnt'];
        }

        // Заполняем недостающие даты
        for ($date = $date_from;
             $date != date('Y-m-d', strtotime('+1 day', $timestamp_to));
             $date = date('Y-m-d', strtotime('+1 day', strtotime($date))))
        {
            $_cnt = !empty($data[$date]) ? $data[$date] : 0;
            $data[$date] = $_cnt;
        }

        // Сортируем массив
        ksort($data);

        if (count($data) > 1)
        {
            // Выводим данные для AmCharts
            $prefix = '';
            $response = '[' . PHP_EOL;
            foreach ($data as $date => $value)
            {
                $response .= $prefix . '{ "date":"' . $date . '", "value":' . $value . ' }';
                $prefix = ',' . PHP_EOL;
            }
            $response .= PHP_EOL . ']';
        }
        else
        {
            $response = 'short_period';
        }
    }
}

echo $response;