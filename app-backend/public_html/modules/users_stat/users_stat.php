<?php

defined("ADMIN") or die("FAIL");

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {
        if ($_GET['action'] == 'view')
        {
            $search_period = empty($_POST['period']) ? 30 : (int)$_POST['period'];

            $user_id = $_SESSION['user_id'];
            $date_min = $DB->GetOne('SELECT MIN(`date`) FROM users_stat WHERE user_id = ?', array($user_id));

            // Дата ОТ
            if (!empty($_POST['date_from']))
            {
                $date_from = date('Y-m-d', strtotime($_POST['date_from']));
                $search_period = 0;
            }
            else
            {
                if (!empty($date_min) && (strtotime($date_min) > strtotime('-' . $search_period . ' day')))
                {
                    $date_from = date('Y-m-d', strtotime($date_min));
                }
                else
                {
                    $date_from = date('Y-m-d', strtotime('-' . $search_period . ' day'));
                }
            }

            // Дата ДО
            if (!empty($_POST['date_to']))
            {
                $date_to = date('Y-m-d', strtotime($_POST['date_to']));
            }
            else
            {
                $date_to = date('Y-m-d', time());
            }

            // Меняем даты местами, при необходимости
            if (strtotime($date_from) > strtotime($date_to))
            {
                list($date_from, $date_to) = array($date_to, $date_from);
            }

            $date_from .= ' 00:00:00';
            $date_to .= ' 23:59:59';

            // Загрузка счетчиков
            $items_in_orders = get_users_stat_counter($user_id, 'items_in_orders', $date_from, $date_to, $DB);
            $profile_view = get_users_stat_counter($user_id, 'profile_view', $date_from, $date_to, $DB);
            $profile_click_site = get_users_stat_counter($user_id, 'profile_click_site', $date_from, $date_to, $DB);
            $profile_click_email = get_users_stat_counter($user_id, 'profile_click_email', $date_from, $date_to, $DB);

            // Дата в формате датапикера в шаблоне
            $date_from_view = date('d.m.Y', strtotime($date_from));
            $date_to_view = date('d.m.Y', strtotime($date_to));
            $date_min_view = date('d.m.Y', strtotime($date_min));
            $date_max_view = date('d.m.Y');

            $smarty->assign('search_period', $search_period);
            $smarty->assign('date_from', $date_from_view);
            $smarty->assign('date_to', $date_to_view);
            $smarty->assign('date_min', $date_min_view);
            $smarty->assign('date_max', $date_max_view);
            $smarty->assign('profile_view', $profile_view);
            $smarty->assign('profile_click_site', $profile_click_site);
            $smarty->assign('profile_click_email', $profile_click_email);
            $smarty->assign('items_in_orders', $items_in_orders);
            $smarty->assign('user_id', $user_id);
            $smarty->display('users_stat/view.tpl');
        }

    }
}