<?php

defined("ADMIN") or die("FAIL");

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        // подключение к пользовательской БД
        $DB2 = ADONewConnection(DB_USERS_DSN);
        $DB2->SetFetchMode(ADODB_FETCH_ASSOC);
        $DB2->Execute("SET NAMES utf8");
        $DB2->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
        $DB2->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
        $DB2->cacheSecs = DB_CACHE_TIME;
        //$DB2->debug = true;

		// пользователь
		include_once (PATH_BACKEND . "/php/classes/class.User.php");
		$user = new User($DB, $DB2);

	    // ------------------------------------
	    if ($_GET['action'] == 'edit-form' || $_GET['action'] == 'edit-account')
	    {
            $countries = $DB->GetAll("SELECT * FROM ip2ruscity_countries WHERE country_id!=0 ORDER BY country ASC");
            $cities = $DB->GetAll("SELECT * FROM ip2ruscity_cities WHERE city_id!=0 ORDER BY city ASC");

            $user_fields_filter = $_GET['action'] == 'edit-account' ? 'IN ("login", "password")' : 'NOT IN ("login", "password")';
	        $content = $DB->GetRow("SELECT * FROM users WHERE id=?", array($_SESSION['user_id']));
	        if ($content['level_id'] == 0)
	        {
    	        $user_fields = $DB->GetAll('SELECT *, 1 as editable, 0 as fill FROM users_fields WHERE users_fields.field ' . $user_fields_filter . ' ORDER BY position ASC');
	        }
	        else
	        {
    	        $user_fields = $DB->GetAll('SELECT users_fields.*, levels_fields.editable,  levels_fields.fill
                                            FROM users_fields
                                               INNER JOIN access_levels_fields levels_fields ON levels_fields.field_id=users_fields.id
                                            WHERE levels_fields.level_id=? AND users_fields.field ' . $user_fields_filter . '
                                            ORDER BY users_fields.position ASC', $content['level_id']);
	        }

            $editable = array();
            $fill = array();
            foreach ($user_fields as $k => $value)
            {
                $user_fields[$k]['value'] = $content[$value['field']];
                if (!$value['editable']) $editable[] = $value['field'];
                if ($value['fill'] && $value['field'] != "password") $fill[] = $value['field'];
            }

            $smarty->assign('from', $_GET['action']);
            $smarty->assign("user_fields", $user_fields);
            $smarty->assign("editable", $editable);
            $smarty->assign("fill", $fill);
            $smarty->assign("user", $content);
            $smarty->assign("countries", $countries);
            $smarty->assign("cities", $cities);
            $smarty->assign("hash", md5(iconv('utf-8', 'windows-1251', $content['login'].$content['password'])));
	        $smarty->display("profile/edit-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------

	    // ------------------------------------
	    else if ($_GET['action'] == "edit")
	    {
            foreach ($_POST as $k => $value)
            {
                $upd[$k] = $_POST[$k];
            }

	        if (isset($_POST['password']) && $_POST['password'] != "")
	        {
	            $upd['password'] = md5(iconv('utf-8', 'windows-1251', $_POST['password']));
	        }
	        else unset($upd['password']);

            if (isset($_POST['country_id']))
            {
                $upd['city_id'] = ($_POST['country_id'] == 186) ? (int)$_POST['city_id'] : 0;
            }

            $upd['individual_buyers'] = (isset($_POST['individual_buyers'])) ? 1 : 0;
            $upd['email_notification'] = (isset($_POST['email_notification'])) ? 1 : 0;
            $upd['allow_download_price'] = (isset($_POST['allow_download_price'])) ? 1 : 0;
            $upd['price_with_nds'] = (isset($_POST['price_with_nds'])) ? 1 : 0;

	        $user->Edit((int)$_SESSION['user_id'], $upd);

	        adm_redirect('?module=profile&action=' . $_POST['from']);
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>
