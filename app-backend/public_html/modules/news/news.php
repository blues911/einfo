<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_IMGS_ONPAGE", 30);
define("MAX_NEWS_ONPAGE", 30);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    include_once (PATH_BACKEND . "/php/classes/class.News.php");
	    $news = new News($DB, $SETTINGS['news'], $_SESSION['user_id']);

	    // ------------------------------------
	    if ($_GET['action'] == "add-form")
	    {
	        foreach ($SETTINGS['news'] as $k => $value)
	        {
	            $smarty->assign($k, $value);
	        }

            $image_list = $DB->GetAll('SELECT * FROM news_images WHERE user_id = ? ORDER BY upload_date DESC', array($_SESSION['user_id']));

            foreach (array_keys($image_list) as $k)
            {
                $image_list[$k]['img'] = 'http://' . NEWSIMG_URL . '/' . $image_list[$k]['img'];
            }

            $smarty->assign('image_list', $image_list);
            $smarty->display("news/add-form.tpl");
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "add")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $data['date'] = date("Y-m-d H:i:s");
                $data['status'] = ($SETTINGS['news']['status_mode'] == "post") ? 1 : 0;
                $data['title'] = $_POST['title'];
                $data['text'] = $_POST['text'];
                $data['img'] = $_FILES['img'];
                $news->Add($data);

                adm_redirect("?module=news&action=list");
            }
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "list")
        {
            $sql_order = (!isset($_GET['order']) || $_GET['order'] == "desc") ? "desc" : "asc";

            $all_items = $DB->GetOne("SELECT COUNT(*) FROM news WHERE user_id=?", array($_SESSION['user_id']));

            $pager = new Pager($all_items, MAX_NEWS_ONPAGE, "dynamic");

            $list = $DB->GetAll("SELECT id, date, status, title FROM news WHERE user_id=? ORDER BY date ".$sql_order." LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd(), array($_SESSION['user_id']));
            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $list[$k] = $value;
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
        	$smarty->assign("list_order", $sql_order);
        	$smarty->assign("pager", $pager->GetPages());
            $smarty->display("news/list.tpl");
            $smarty->clear_all_assign();
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "delete" && isset($_GET['id']))
        {
            $id = (int)$_GET['id'];
            if ($news->CheckAccess($id))
            {
                $news->Delete($id);
                adm_redirect("?module=news&action=list");
            }
            else adm_message("Действие запрещено");
	    }
	    // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit-form")
        {
            $id = (int)$_GET['id'];
            if ($news->CheckAccess($id))
            {
    	        foreach ($SETTINGS['news'] as $k => $value)
    	        {
    	            $smarty->assign($k, $value);
    	        }

                $content = $DB->GetRow("SELECT * FROM news WHERE id=?", array($id));
                if ($content['img'] != "")
                {
                    $content['img'] = 'http://' . NEWSIMG_URL . '/' . $content['img'];
                }

                $image_list = $DB->GetAll('SELECT * FROM news_images WHERE user_id = ? ORDER BY upload_date DESC', array($_SESSION['user_id']));

                foreach (array_keys($image_list) as $k)
                {
                    $image_list[$k]['img'] = 'http://' . NEWSIMG_URL . '/' . $image_list[$k]['img'];
                }

                $smarty->assign('image_list', $image_list);
        	    $smarty->assign("content", $content);
        	    $smarty->display("news/edit-form.tpl");
        	    $smarty->clear_all_assign();
            }
            else adm_message("Действие запрещено");
        }
        // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "edit" && isset($_GET['id']))
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $id = (int)$_GET['id'];
                if ($news->CheckAccess($id))
                {
                    $data['id'] = $id;
                    $data['status'] = ($SETTINGS['news']['status_mode'] == "post") ? 1 : 0;
                    $data['title'] = $_POST['title'];
                    $data['text'] = $_POST['text'];
                    $data['img'] = $_FILES['img'];
                    $news->Edit($data);

                    adm_redirect("?module=news&action=list");
                }
                else adm_message("Действие запрещено");
            }
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "images-form")
	    {
	        // данные для upload
	        foreach ($SETTINGS['news'] as $k => $value)
	        {
	            $smarty->assign($k, $value);
	        }

	        // вывод загруженных изображений
	        $all_items = $DB->GetOne("SELECT COUNT(*) FROM news_images WHERE user_id=?", array($_SESSION['user_id']));
	        $pager = new Pager($all_items, MAX_IMGS_ONPAGE, "dynamic");
	        $list = $DB->GetAll("SELECT * FROM news_images WHERE user_id=? ORDER BY upload_date DESC LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd(), array($_SESSION['user_id']));
	        foreach ($list as $k => $value)
	        {
	            $list[$k]['img'] = 'http://' . NEWSIMG_URL . '/' . $value['img'];
	        }

	        $smarty->assign("list", $list);
	        $smarty->assign("pager", $pager->GetPages());

	        $smarty->display("news/images-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "image-upload")
	    {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                $imgext = $news->CheckFormat($_FILES['upload_img']['name']);
                if ($imgext !== false)
                {
                    if ($news->CheckSize($_FILES['upload_img']['tmp_name']) !== false)
                    {
                        $imgname = "news_".md5(uniqid()).".".$imgext;

                        $ins['user_id'] = $_SESSION['user_id'];
                        $ins['upload_date'] = date("Y-m-d H:i:s");
                        $ins['img'] = $imgname;
                        $ins['blob'] = file_get_contents($_FILES['upload_img']['tmp_name']);
                        $ins['title'] = $_POST['upload_title'];

                        $DB->Execute('INSERT INTO news_images SET user_id = ?, upload_date = ?, img = ?, img_blob = ?, title = ?', $ins);

                        adm_redirect("?module=news&action=images-form");
                    }
                    else
                    {
                        adm_message("<strong>Изображение имеет недопустимый размер</strong><br />(Разрешенная максимальная ширина &mdash; ".$SETTINGS['news']['img_max_width']."px, высота &mdash; ".$SETTINGS['news']['img_max_height']."px)");
                    }
                }
                else
                {
                    adm_message("<b>Изображение имеет недопустимый формат</b><br />(Разрешенные форматы &mdash; ".mb_strtoupper($SETTINGS['news']['img_allow_formats'], 'utf-8').")");
                }
            }
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "image-geturl")
	    {
	        $imgname = $DB->GetOne("SELECT img FROM news_images WHERE user_id=? AND id=?", array($_SESSION['user_id'], (int)$_GET['id']));
	        $url = 'http://' . NEWSIMG_URL . '/' . $imgname;

	        $smarty->assign("url", $url);
	        $smarty->display("news/images-geturl.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>