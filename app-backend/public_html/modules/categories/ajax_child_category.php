<?php

$category_id = (int)$_POST['parent_category'];

$list = $DB->GetAll("SELECT t1.id, t1.title
                     FROM categories t1, categories t2
                     WHERE t2.id=?
                     	AND t1.c_left BETWEEN t2.c_left
                     	AND t2.c_right
                     	AND t1.c_level=t2.c_level+1
                     ORDER BY t1.c_left", array($category_id));

$smarty->assign("list", $list);
$result = $smarty->display("categories/child_category.tpl");

?>