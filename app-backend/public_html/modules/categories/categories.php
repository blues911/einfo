<?php

defined("ADMIN") or die("FAIL");

/////////////////////////////

// обновление полных путей для категорий
$sql['update_catpaths'] = "UPDATE categories,
                              (SELECT node.id,
                                      GROUP_CONCAT(parent.path ORDER BY parent.c_left ASC SEPARATOR '/') as full_path,
                                      GROUP_CONCAT(parent.title ORDER BY parent.c_left ASC SEPARATOR ' / ') as full_title
                                  FROM categories node, categories parent
                                  WHERE node.c_left BETWEEN parent.c_left AND parent.c_right AND parent.c_level<>0
                                  GROUP BY node.id
                                  ORDER BY node.c_left) as list
                           SET categories.full_path=list.full_path, categories.full_title=list.full_title
	                       WHERE categories.id=list.id";


// ------------------------------------

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        // инициализация dbtree
        include_once (PATH_BACKEND . "/php/classes/dbtree/class.dbtree.php");

        $tree = new CDBTree('categories', 'c', $DB);
        //$tree->clear(array("title" => "КОРЕНЬ"));

        // ------------------------------------
        if ($_GET['action'] == "add")
        {
            $cat['title'] = $_POST['title'];
            $cat['path'] = $_POST['path'];
            $cat['descr'] = !empty($_POST['descr']) ? $_POST['descr'] : '';
            $cat['descr_position'] = $_POST['descr_position'];

            $category_after = (int)$_POST['category_after'];
            if ($category_after == 0) $tree->insert((int)$_POST['parent_category'], '', $cat);
            else $tree->insertNear($category_after, '', $cat);

            $DB->Execute($sql['update_catpaths']);

            adm_redirect("?module=categories&action=list");
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "add-form")
        {
    	    $categories = $DB->GetAll("SELECT id, c_level, title FROM categories WHERE c_level!=0 ORDER BY c_left ASC");

            $smarty->assign("categories", $categories);
    	    $smarty->display("categories/add-form.tpl");
    	    $smarty->clear_all_assign();
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "list")
        {
            $list = $DB->GetAll("SELECT id, c_level, title FROM categories WHERE c_level!=0 ORDER BY c_left ASC");

            $smarty->assign("list", $list);
            $smarty->display("categories/list.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "shift")
        {
            $tree->changeBranchOrder((int)$_GET['id'], $_GET['type']);
            adm_redirect("?module=categories&action=list");
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "delete")
        {
            $id = (int)$_GET['id'];

            // Удаляем записи всех подкатегорий
            $DB->Execute('
                        DELETE
                        FROM catalog_comp_canon
                        WHERE category_id IN (
                              SELECT c.id
                              FROM categories AS cat
                                LEFT JOIN categories AS c ON c.c_left >= cat.c_left AND c.c_right <= cat.c_right
                              WHERE cat.id = ?
                              )', array($id)
            );

            // Удаляем категорию
            $tree->DeleteAll($id);

            $DB->Execute($sql['update_catpaths']);

            // Удаляем шаблоны для канонических имен если есть
            if ($regexp_ids = $DB->GetCol('SELECT id FROM catalog_regexp WHERE category_id = ?', array($id)))
            {
                $DB->Execute('DELETE FROM catalog_regexp WHERE id IN (' . implode(',', $regexp_ids) . ')');
            }

            adm_redirect("?module=categories&action=list");
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "edit-form")
        {
            $content = $DB->GetRow("SELECT * FROM categories WHERE id=?", array((int)$_GET['id']));
    	    $categories = $DB->GetAll("SELECT id, c_level, title FROM categories WHERE c_level!=0 ORDER BY c_left ASC");
    	    $parent_cats = $DB->GetAll("SELECT id, title, c_level FROM categories WHERE c_level!=0 AND c_left < ? AND c_right > ? ORDER BY c_left", array($content['c_left'], $content['c_right']));

            if (!$parent = array_pop($parent_cats))
            {
                $parent = array('id' => 1);
            }

    	    $smarty->assign("content", $content);
    	    $smarty->assign("categories", $categories);
    	    $smarty->assign("parent_cats", $parent_cats);
            $smarty->assign('parent_id', $parent['id']);

    	    $smarty->display("categories/edit-form.tpl");
    	    $smarty->clear_all_assign();
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "edit")
        {
            $id = (int)$_GET['id'];
            $upd['title'] = $_POST['title'];
            $upd['path'] = $_POST['path'];
            $upd['descr'] = !empty($_POST['descr']) ? $_POST['descr'] : '';
            $upd['descr_position'] = $_POST['descr_position'];

            $DB->AutoExecute("categories", $upd, "UPDATE", "id=".$id);


            $new_parent_category = (int)$_POST['new_parent_category'];
            if ($new_parent_category != 0)
            {
                $tree->MoveAll($id, $new_parent_category);
            }

            $category_after = (int)$_POST['category_after'];
            if ($category_after != 0)
            {
                $tree->MoveAllAfter($id, $category_after);
            }

            $DB->Execute($sql['update_catpaths']);

            adm_redirect("?module=categories&action=list");
        }
        //--------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>