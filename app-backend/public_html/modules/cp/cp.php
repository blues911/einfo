<?php

defined("ADMIN") or die("FAIL");

if (isset($_GET['action']))
{

    // ------------------------------------
    if ($_GET['action'] == "help")
    {
        $docpath = "index";

        if (isset($_GET['doc']))
        {
            if (preg_match("/^([-a-z0-9_]+)\.?([-a-z0-9_]*)$/i", $_GET['doc'], $matches))
            {
                if (file_exists("templates/cp/help/".$_GET['doc'].".html"))
                {
                    $docpath = $_GET['doc'];
                }
            }
        }

        if ($docpath == "online.price_file")
        {
            $hash = $DB->GetOne("SELECT MD5(CONCAT(login, password)) FROM users WHERE id=?", array($_SESSION['user_id']));
            $smarty->assign("hash", $hash);
        }

        $smarty->display("cp/help/".$docpath.".html");
    }
    // ------------------------------------

    // ------------------------------------
    else if ($_GET['action'] == "disclaimer")
    {
        $disclaimer = $DB->GetRow("SELECT account.disclaimer AS text, user.disclaimer AS status
                                   FROM accounts account
                                	  LEFT JOIN users user ON user.level_id=account.level_id
                                   WHERE user.id=?", array($_SESSION['user_id']));

        $smarty->assign("disclaimer", $disclaimer);
        $smarty->display("cp/disclaimer.tpl");
    }
    // ------------------------------------

    // ------------------------------------
    else if ($_GET['action'] == "disclaimer-accept")
    {
        $DB->GetRow("UPDATE users SET disclaimer=1 WHERE id=?", array($_SESSION['user_id']));
        adm_redirect("");
    }
    // ------------------------------------

    // ------------------------------------
    else if ($_GET['action'] == "exit")
    {
        session_unset();
        setcookie("session_hash", "", time() - 1);
        adm_redirect("");
    }
    // ------------------------------------

    // ------------------------------------
    else if ($_GET['action'] == "logoff")
    {
        if (isset($_SESSION['logoff']) && $_SESSION['logoff'])
        {
            $_SESSION['user_id'] = $_SESSION['logoff_id'];
            unset($_SESSION['logoff']);
            unset($_SESSION['logoff_id']);

            if (isset($_GET['users_p']))
            {
                adm_redirect("?module=users_p&action=list");
            }
            else
            {
                adm_redirect("?module=users&action=list");            
            }        
        }
    }
    // ------------------------------------
}





?>