<?php

defined("ADMIN") or die("FAIL");

/////////////////////////////

define("MAX_LEVELS_ONPAGE", 50);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    // ------------------------------------
	    if ($_GET['action'] == "add-form")
	    {
            $modules = $DB->GetAll("SELECT * FROM modules ORDER BY position ASC");
            $fields = $DB->GetAll("SELECT * FROM users_fields ORDER BY position ASC");

            $smarty->assign("modules", $modules);
            $smarty->assign("fields", $fields);
	        $smarty->display("levels/add-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "add")
	    {

	        $DB->Execute("INSERT INTO access_levels (title) VALUES (?)", array($_POST['title']));

	        $level_id = $DB->Insert_ID();
	        if ($level_id !== false)
	        {
	            if (isset($_POST['access_modules']))
	            {
    	            foreach ($_POST['access_modules'] as $value)
    	            {
    	                $DB->Execute("INSERT INTO access (level_id, module_id) VALUES (?, ?)", array($level_id, (int)$value));
    	            }
    	        }

    	        if (isset($_POST['fields']))
    	        {
    	            foreach ($_POST['fields'] as $field_id => $value)
    	            {
    	                $f_editable = (int)isset($_POST['editable'][$field_id]);
    	                $f_fill = (int)isset($_POST['fill'][$field_id]);
    	                $DB->Execute("INSERT INTO access_levels_fields (level_id, field_id, editable, fill)
    	                              VALUES (?, ?, ?, ?)", array($level_id, (int)$field_id, $f_editable, $f_fill));
    	            }
	            }

	        }

	        adm_redirect("?module=levels&action=list");
	    }
	    // ------------------------------------


        // ------------------------------------
	    else if ($_GET['action'] == "list")
	    {
	        $all_items = $DB->GetOne("SELECT COUNT(*) FROM access_levels");

	        $pager = new Pager($all_items, MAX_LEVELS_ONPAGE, "dynamic");

	        $list = $DB->GetAll("SELECT * FROM access_levels ORDER BY title ASC LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());

	        $i = $pager->GetLimitStart() + 1;
	        foreach ($list as $k => $value)
	        {
	            $list[$k]['num'] = $i;
	            $i++;
	        }

	        $smarty->assign("list", $list);
	        $smarty->assign("pager", $pager->GetPages());

	        $smarty->display("levels/list.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit-form")
	    {
	        $level_id = (int)$_GET['id'];

	        $content = $DB->GetRow("SELECT * FROM access_levels WHERE id=?", array($level_id));

            $modules = $DB->GetAll("SELECT * FROM modules ORDER BY position ASC");
            $access = $DB->GetCol("SELECT module_id FROM access WHERE level_id=?", array($level_id));
            foreach ($modules as $k => $value)
            {
                if (in_array($value['id'], $access))
                {
                    $modules[$k]['access'] = true;
                }
            }

            $fields = $DB->GetAll("SELECT * FROM users_fields ORDER BY position ASC");
            $level_fields = $DB->GetAll("SELECT field_id, editable, fill FROM access_levels_fields WHERE level_id=?", array($level_id));
            foreach ($fields as $k => $value)
            {
                foreach ($level_fields as $field)
                {
                    if ($value['id'] == $field['field_id'])
                    {
                        $fields[$k]['checked'] = true;
                        $fields[$k]['editable'] = $field['editable'];
                        $fields[$k]['fill'] = $field['fill'];
                    }
                }
            }

            $smarty->assign("content", $content);
            $smarty->assign("modules", $modules);
            $smarty->assign("fields", $fields);
	        $smarty->display("levels/edit-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit" && isset($_GET['id']))
	    {
	        $id = (int)$_GET['id'];
	        $DB->Execute("UPDATE access_levels SET title=? WHERE id=?", array($_POST['title'], $id));

	        $DB->Execute("DELETE FROM access WHERE level_id=?", array($id));
            if (isset($_POST['access_modules']))
            {
                foreach ($_POST['access_modules'] as $value)
                {
                    $DB->Execute("INSERT INTO access (level_id, module_id) VALUES (?, ?)", array($id, (int)$value));
                }
            }

			$DB->Execute("DELETE FROM access_levels_fields WHERE level_id=?", array($id));
	        if (isset($_POST['fields']))
	        {
	            foreach ($_POST['fields'] as $field_id => $value)
	            {
	                $f_editable = (int)isset($_POST['editable'][$field_id]);
	                $f_fill = (int)isset($_POST['fill'][$field_id]);
	                $DB->Execute("INSERT INTO access_levels_fields (level_id, field_id, editable, fill)
	                              VALUES (?, ?, ?, ?)", array($id, (int)$field_id, $f_editable, $f_fill));
	            }
         	}

	        adm_redirect("?module=levels&action=list");
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "delete" && isset($_GET['id']))
	    {
	        $id = (int)$_GET['id'];

	        if ($DB->GetOne("SELECT COUNT(*) FROM users WHERE level_id=?", $id))
	        {
	            adm_message("Нельзя удалить уровень доступа, если с ним связан какой-нибудь пользователь.");
	        }
	        else if ($DB->GetOne("SELECT COUNT(*) FROM accounts WHERE level_id=?", $id))
	        {
	            adm_message("Нельзя удалить уровень доступа, если с ним связан какой-нибудь вид аккаунта.");
	        }
	        else
            {
	            $DB->Execute("DELETE FROM access WHERE level_id=?", $id);
	            $DB->Execute("DELETE FROM access_levels WHERE id=?", $id);
	            $DB->Execute("DELETE FROM access_levels_fields WHERE level_id=?", $id);

	            adm_redirect("?module=levels&action=list");
	        }
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>