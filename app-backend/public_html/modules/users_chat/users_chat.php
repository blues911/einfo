<?php

defined("ADMIN") or die("FAIL");
define("MAX_CHATS_ONPAGE", 50);

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
	  {
        // подключение к пользовательской БД
        $DB2 = ADONewConnection(DB_USERS_DSN);
        $DB2->SetFetchMode(ADODB_FETCH_ASSOC);
        $DB2->Execute("SET NAMES utf8");
        $DB2->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
        $DB2->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
        $DB2->cacheSecs = DB_CACHE_TIME;

        if ($_GET['action'] == "list")
	      {
            $all_items = $DB->GetAll("SELECT guest_hash, COUNT(id) FROM users_chat WHERE user_id = ? GROUP BY guest_hash", array($_SESSION['user_id']));

            $pager = new Pager(count($all_items), MAX_CHATS_ONPAGE, "dynamic");

            $list = $DB->GetAll("SELECT users_chat.*, users_chat_tokens.ip
                                    FROM users_chat
                                    LEFT JOIN users_chat_tokens
                                        ON users_chat_tokens.owner_value = users_chat.guest_hash
                                    WHERE users_chat.user_id = ?
                                        AND users_chat_tokens.is_active = 1
                                    GROUP BY users_chat.guest_hash
                                    ORDER BY users_chat.date DESC LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd(), array($_SESSION['user_id']));

            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $city_name = $DB->GetOne('SELECT ip2ruscity_cities.city AS city_name
                                            FROM ip2ruscity_ip_compact
                                            LEFT JOIN ip2ruscity_cities
                                                ON ip2ruscity_cities.city_id = ip2ruscity_ip_compact.city_id
                                            WHERE INET_ATON(?) BETWEEN ip2ruscity_ip_compact.num_ip_start
                                                AND ip2ruscity_ip_compact.num_ip_end', array($value['ip']));
                   
                $list[$k] = $value;
                $list[$k]['title'] = !empty($city_name) ? $value['ip'] . ' ('. $city_name .')' : $value['ip'];
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
            $smarty->assign("pager", $pager->GetPages());
            $smarty->display("users_chat/list.tpl");
            $smarty->clear_all_assign();
        }
        else if ($_GET['action'] == "view")
	      {
            $user_id = (int)$_GET['user_id'];
            $guest_hash = $_GET['guest_hash'];

            $list = $DB->GetAll("SELECT users_chat.*, users.title as user_title, users_chat_tokens.ip
                                    FROM users_chat
                                    LEFT JOIN users
                                        ON users.id = users_chat.user_id
                                    LEFT JOIN users_chat_tokens
                                        ON users_chat_tokens.owner_value = users_chat.guest_hash
                                    WHERE users_chat.user_id = ?
                                        AND users_chat.guest_hash = ?
                                        AND users_chat_tokens.is_active = 1
                                    ORDER BY date ASC", array($user_id, $guest_hash));

            foreach ($list as $k => $value)
            {
                $list[$k] = $value;
                $list[$k]['date'] = $date->GetRusDate($value['date'], 4);
            }

            $city_name = $DB->GetOne('SELECT ip2ruscity_cities.city AS city_name
                                        FROM ip2ruscity_ip_compact
                                        LEFT JOIN ip2ruscity_cities
                                            ON ip2ruscity_cities.city_id = ip2ruscity_ip_compact.city_id
                                        WHERE INET_ATON(?) BETWEEN ip2ruscity_ip_compact.num_ip_start
                                            AND ip2ruscity_ip_compact.num_ip_end', array($list[0]['ip']));

            $title = !empty($city_name) ? $list[0]['ip'] . ' ('. $city_name .')' : $list[0]['ip'];

            $smarty->assign("title", $title);
            $smarty->assign("list", $list);
            $smarty->display("users_chat/view.tpl");
            $smarty->clear_all_assign();
        }
    }
}
else
{
    adm_message($control->GetMessage());
}