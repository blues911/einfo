<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_USERS_ONPAGE", 500);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        // ------------------------------------
        if ($_GET['action'] == "list")
        {
            $sql_order = (!isset($_GET['order']) || $_GET['order'] == "asc") ? "asc" : "desc";

            $all_items = $DB->GetOne("SELECT COUNT(*) FROM users WHERE status=1");
            $pager = new Pager($all_items, MAX_USERS_ONPAGE, "dynamic");

            $list = $DB->GetAll("SELECT users.id, users.title, users.about, votes.rating
                                 FROM users
                                    LEFT JOIN rating_votes votes ON votes.user_id=users.id AND user_voter_id=?
                                 WHERE users.status=1
                                 ORDER BY users.title ".$sql_order."
                                 LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd(), array($_SESSION['user_id']));
            
            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $list[$k] = $value;
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
            $smarty->assign("list_order", $sql_order);
        	$smarty->assign("pager", $pager->GetPages());
            $smarty->display("rating/list.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------
        

        // ------------------------------------
        else if ($_GET['action'] == "vote")
        {
            if ($_SERVER['REQUEST_METHOD'] == "POST")
            {
                foreach ($_POST['user_vote'] as $user_id => $rating)
                {
                    $DB->Execute("REPLACE INTO rating_votes VALUES(?, ?, ?)", array($_SESSION['user_id'], (int)$user_id, (float)$rating));
                }

                adm_redirect("?module=rating&action=list");
            }
        }
        // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>