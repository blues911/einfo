<?php

if (isset($_POST['id']))
{
    $id = (int)$_POST['id'];
    $userdoc = $DB->GetRow("SELECT * FROM users_docs WHERE id=?", array($id));

    if ($userdoc)
    {
        @unlink(USERDOCS_PATH."/".$userdoc['file']);
        @unlink(USERDOCS_SMALL_PATH."/".$userdoc['file']);

        $DB->Execute("DELETE FROM users_docs WHERE id=?", array($id));

        print "OK";
    }
}

?>
