<?php

if (isset($_POST['filter']))
{
    $ids = array();
    switch ($_POST['filter'])
    {
        case "all":
            $ids = $DB->GetCol("SELECT id FROM users");
            break;

        case "active":
            $ids = $DB->GetCol("SELECT id FROM users WHERE status=1");
            break;

        case "locked":
            $ids = $DB->GetCol("SELECT id FROM users WHERE status=0");
            break;

        case "wait":
            $ids = $DB->GetCol("SELECT id FROM users WHERE status=-1");
            break;

        case "price_update_month":
            $ids = $DB->GetCol("SELECT id FROM users WHERE price_date < NOW() - INTERVAL 1 MONTH");
            break;
    }

    print implode(",", $ids);
}

?>
