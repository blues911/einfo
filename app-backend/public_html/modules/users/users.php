<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_USERS_ONPAGE", 500);

unset($_SESSION['users_p']);

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
        // подключение к пользовательской БД
        $DB2 = ADONewConnection(DB_USERS_DSN);
        $DB2->SetFetchMode(ADODB_FETCH_ASSOC);
        $DB2->Execute("SET NAMES utf8");
        $DB2->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
        $DB2->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
        $DB2->cacheSecs = DB_CACHE_TIME;
        //$DB2->debug = true;

		// пользователь
		include_once (PATH_BACKEND . "/php/classes/class.User.php");
		$user = new User($DB, $DB2);

	    // ------------------------------------
	    if ($_GET['action'] == "add-form")
	    {
            $levels = $DB->GetAll("SELECT * FROM access_levels");
            $countries = $DB->GetAll("SELECT * FROM ip2ruscity_countries WHERE country_id!=0 ORDER BY country ASC");
            $cities = $DB->GetAll("SELECT * FROM ip2ruscity_cities WHERE city_id!=0 ORDER BY city ASC");

            $smarty->assign("levels", $levels);
            $smarty->assign("countries", $countries);
            $smarty->assign("cities", $cities);

	        $smarty->display("users/add-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "add")
	    {
	        $ins['status'] = (int)$_POST['status'];
	        $ins['level_id'] = (int)$_POST['level'];
          $ins['register_date'] = date("Y-m-d H:i:s");
          $ins['price_limit'] = (int)$_POST['price_limit'];
	        $ins['title'] = $_POST['title'];
	        $ins['login'] = $_POST['login'];
	        $ins['password'] = md5(iconv('utf-8', 'windows-1251', $_POST['password']));
	        $ins['country_id'] = (int)$_POST['country_id'];
	        $ins['city_id'] = isset($_POST['city_id']) ? (int)$_POST['city_id'] : 0;
	        $ins['address'] = $_POST['address'];
	        $ins['phone1'] = $_POST['phone1'];
          $ins['phone1_ext'] = $_POST['phone1_ext'];
          $ins['phone2'] = $_POST['phone2'];
          $ins['phone2_ext'] = $_POST['phone2_ext'];
	        $ins['fax'] = $_POST['fax'];
	        $ins['email'] = $_POST['email'];
          $ins['email_price_notification'] = $_POST['email_price_notification'];
          $ins['email_order_notification'] = $_POST['email_order_notification'];
	        $ins['www'] = $_POST['www'];
	        $ins['icq'] = $_POST['icq'];
	        $ins['skype'] = $_POST['skype'];
	        $ins['person_fio'] = $_POST['person_fio'];
	        $ins['person_email'] = $_POST['person_email'];
	        $ins['person_info'] = $_POST['person_info'];
          $ins['inn'] = $_POST['inn'];
          $ins['kpp'] = $_POST['kpp'];
          $ins['ogrn'] = $_POST['ogrn'];
          $ins['address_jur'] = $_POST['address_jur'];
          $ins['address_post'] = $_POST['address_post'];
	        $ins['comment'] = $_POST['comment'];
	        $ins['about'] = $_POST['about'];
          $ins['individual_buyers'] = isset($_POST['individual_buyers']) ? 1 : 0;
          $ins['min_order_amount'] = $_POST['min_order_amount'];
          $ins['allow_download_price'] = isset($_POST['allow_download_price']) ? 1 : 0;
          $ins['price_with_nds'] = !empty($_POST['price_with_nds']) ? 1 : 0;
          $ins['email_notification'] = isset($_POST['email_notification']) ? 1 : 0;
          $ins['api_allowed_ip'] = isset($_POST['api_allowed_ip']) ? trim($_POST['api_allowed_ip']) : '';
          $ins['viber'] = isset($_POST['viber']) ? trim($_POST['viber']) : '' ;
          $ins['whatsapp'] = isset($_POST['whatsapp']) ? trim($_POST['whatsapp']) : '' ;
          $ins['telegram'] = isset($_POST['telegram']) ? trim($_POST['telegram']) : '' ;
          $ins['facebook'] = isset($_POST['facebook']) ? trim($_POST['facebook']) : '' ;
          $ins['vk'] = isset($_POST['vk']) ? trim($_POST['vk']) : '' ;
          $ins['instagram'] = isset($_POST['instagram']) ? trim($_POST['instagram']) : '' ;

	        $user_id = $user->Add($ins);

          $DB->Execute("INSERT INTO users_last_visit (user_id) VALUES (?)", array($user_id));

          if (!empty($_FILES['docs']))
          {
              foreach ($_FILES['docs']['name'] as $k => $value)
              {
                  if ($value != "")
                  {
                      $finfo = pathinfo($value);
                      $unique_id = md5(uniqid());
                      $user_doc = $unique_id . '.' . $finfo['extension'];
                      $user_doc_small = $unique_id . '.small.' . $finfo['extension'];

                      move_uploaded_file($_FILES['docs']['tmp_name'][$k], PATH_TMP . '/' . $user_doc);

                      $blob = file_get_contents(PATH_TMP . '/' . $user_doc);
                      $blob_small = null;

                      if (in_array(strtolower($finfo['extension']), array('jpg', 'jpeg', 'gif', 'png', 'bmp')))
                      {
                          image_magick('convert ' . PATH_TMP . '/' . $user_doc . ' -resize 70 -quality 95 ' . PATH_TMP . '/' . $user_doc_small);
                          $blob_small = file_get_contents(PATH_TMP . '/' . $user_doc_small);
                          unlink(PATH_TMP . '/' . $user_doc_small);
                      }

                      unlink(PATH_TMP . '/' . $user_doc);

                      $DB->Execute(
                          'INSERT INTO users_docs SET user_id = ?, title = ?, `file` = ?, fshow = ?, file_blob = ?, file_small_blob = ?',
                          array($user_id, $_POST['docs_titles'][$k], $user_doc, (int)$_POST['docs_show'][$k], $blob, $blob_small)
                      );
                  }
              }
          }

	        adm_redirect("?module=users&action=list");
	    }
	    // ------------------------------------


        // ------------------------------------
	    else if ($_GET['action'] == "list")
	    {
	    	$k_pager = 1;
	    	$sql_view = "status!=-99";
	        if (isset($_GET['view']))
	        {
	            switch ($_GET['view'])
	            {
	                case "valid":
	                	$sql_view = "status=-2";
	                	break;

	                case "mod":
	                	$sql_view = "status=-1";
	                	break;

	                case "locked":
	                	$sql_view = "status=0";
	                	break;

	                case "active":
	                	$sql_view = "status=1";
	                	break;

	                case "one_list":
	                	$k_pager = 10000000;
	                	break;
	            }
	        }

	        $all_items = $DB->GetOne("SELECT COUNT(*) FROM users WHERE ".$sql_view);
	        $pager = new Pager($all_items, MAX_USERS_ONPAGE * $k_pager, "dynamic");

	        $list = $DB->GetAll("SELECT * FROM users WHERE ".$sql_view." ORDER BY id ASC LIMIT ".$pager->GetLimitStart().", ".$pager->GetLimitEnd());

	        $i = $pager->GetLimitStart() + 1;
	        foreach ($list as $k => $value)
	        {
	            $list[$k]['num'] = $i;
	            $i++;
	        }

	        $smarty->assign("list", $list);
	        $smarty->assign("pager", $pager->GetPages());
	        $smarty->assign("view", isset($_GET['view']) ? $_GET['view'] : "all");
	        $smarty->assign("dist", isset($_GET['dist']));

	        $smarty->display("users/list.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit-form")
	    {
            $id = (int)$_GET['id'];

	        $content = $DB->GetRow("SELECT *, CONCAT('dist_', MD5(CONCAT(login, `password`))) AS buffer_table FROM users WHERE id=?", array($id));
            $levels = $DB->GetAll("SELECT * FROM access_levels");
            $users_docs = $DB->GetAll("SELECT * FROM users_docs WHERE user_id=? ORDER BY id ASC", array($id));

            $countries = $DB->GetAll("SELECT * FROM ip2ruscity_countries WHERE country_id!=0 ORDER BY country ASC");
            $cities = $DB->GetAll("SELECT * FROM ip2ruscity_cities WHERE city_id!=0 ORDER BY city ASC");

            $content['register_date'] = $date->GetRusDate($content['register_date'], 4);

            $smarty->assign("users_docs_url", USERDOCS_URL);
            $smarty->assign("content", $content);
            $smarty->assign("levels", $levels);
            $smarty->assign("users_docs", $users_docs);
            $smarty->assign("countries", $countries);
            $smarty->assign("cities", $cities);
	        $smarty->display("users/edit-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "edit" && isset($_GET['id']) && !empty($_POST['title']))
	    {
            $id = (int)$_GET['id'];
            
            if (isset($_POST['status'])) $upd['status'] = (int)$_POST['status'];
            if (isset($_POST['level'])) $upd['level_id'] = (int)$_POST['level'];
            $upd['price_limit'] = (int)$_POST['price_limit'];
            $upd['title'] = $_POST['title'];
            $upd['login'] = $_POST['login'];
            if (isset($_POST['password']) && $_POST['password'] != "") $upd['password'] = md5(iconv('utf-8', 'windows-1251', $_POST['password']));
            $upd['country_id'] = (int)$_POST['country_id'];
            $upd['city_id'] = isset($_POST['city_id']) ? (int)$_POST['city_id'] : 0;
            $upd['address'] = $_POST['address'];
            $upd['phone1'] = $_POST['phone1'];
            $upd['phone1_ext'] = $_POST['phone1_ext'];
            $upd['phone2'] = $_POST['phone2'];
            $upd['phone2_ext'] = $_POST['phone2_ext'];
            $upd['fax'] = $_POST['fax'];
            $upd['email'] = $_POST['email'];
            $upd['email_price_notification'] = $_POST['email_price_notification'];
            $upd['email_order_notification'] = $_POST['email_order_notification'];
            $upd['www'] = $_POST['www'];
            $upd['icq'] = $_POST['icq'];
            $upd['skype'] = $_POST['skype'];
            $upd['person_fio'] = $_POST['person_fio'];
            $upd['person_email'] = $_POST['person_email'];
            $upd['person_info'] = $_POST['person_info'];
            $upd['inn'] = $_POST['inn'];
            $upd['kpp'] = $_POST['kpp'];
            $upd['ogrn'] = $_POST['ogrn'];
            $upd['address_jur'] = $_POST['address_jur'];
            $upd['address_post'] = $_POST['address_post'];
            $upd['comment'] = $_POST['comment'];
            $upd['about'] = $_POST['about'];
            $upd['individual_buyers'] = isset($_POST['individual_buyers']) ? 1 : 0;
            $upd['min_order_amount'] = $_POST['min_order_amount'];
            $upd['allow_download_price'] = isset($_POST['allow_download_price']) ? 1 : 0;
            $upd['price_with_nds'] = !empty($_POST['price_with_nds']) ? 1 : 0;
            $upd['email_notification'] = isset($_POST['email_notification']) ? 1 : 0;
            $upd['cnt'] = $DB->GetOne('SELECT COUNT(*) FROM base_main WHERE user_id = ?', array($id));
            $upd['api_allowed_ip'] = isset($_POST['api_allowed_ip']) ? trim($_POST['api_allowed_ip']) : '';
            $upd['viber'] = isset($_POST['viber']) ? trim($_POST['viber']) : '' ;
            $upd['whatsapp'] = isset($_POST['whatsapp']) ? trim($_POST['whatsapp']) : '' ;
            $upd['telegram'] = isset($_POST['telegram']) ? trim($_POST['telegram']) : '' ;
            $upd['facebook'] = isset($_POST['facebook']) ? trim($_POST['facebook']) : '' ;
            $upd['vk'] = isset($_POST['vk']) ? trim($_POST['vk']) : '' ;
            $upd['instagram'] = isset($_POST['instagram']) ? trim($_POST['instagram']) : '' ;

            // изменение Update_time для буферной таблицы и отправка уведомление пользователю при смене статуса
            $status_old = $DB->GetOne("SELECT status FROM users WHERE id=?", array($id));
            if ((isset($upd['status']) && $upd['status'] != $status_old))
            {
                // отправка уведомления
                $mail = new PHPMailer();
                $mail->SetFrom("admin@einfo.ru", "Einfo");
                $mail->Subject = "[einfo.ru] Изменение статуса аккаунта";

                $smarty->assign("id", $id);
                $smarty->assign("user", $upd);
                $smarty->assign("status_old", $status_old);
                $mail->Body = $smarty->fetch("users/mail_status.tpl");

                $email_to = preg_split("/[,;\s]/", $upd['email'], -1, PREG_SPLIT_NO_EMPTY);
                foreach ($email_to as $value)
                {
                    $mail->AddAddress(trim($value));
                }

                $mail->Send();
            }

            $user->Edit($id, $upd);

            // документы пользователя
            if (!empty($_POST['docs_edit_show']))
            {
                foreach ($_POST['docs_edit_show'] as $k => $value)
                {
                    $DB->Execute("UPDATE users_docs SET fshow=? WHERE id=?", array((int)$value, $k));
                }
            }

            if (!empty($_FILES['docs']))
            {
                foreach ($_FILES['docs']['name'] as $k => $value)
                {
                    if ($value != "")
                    {
                        $finfo = pathinfo($value);
                        $unique_id = md5(uniqid());
                        $user_doc = $unique_id . '.' . $finfo['extension'];
                        $user_doc_small = $unique_id . '.small.' . $finfo['extension'];

                        move_uploaded_file($_FILES['docs']['tmp_name'][$k], PATH_TMP . '/' . $user_doc);

                        $blob = file_get_contents(PATH_TMP . '/' . $user_doc);
                        $blob_small = null;

                        if (in_array(strtolower($finfo['extension']), array('jpg', 'jpeg', 'gif', 'png', 'bmp')))
                        {
                            image_magick('convert ' . PATH_TMP . '/' . $user_doc . ' -resize 70 -quality 95 ' . PATH_TMP . '/' . $user_doc_small);
                            $blob_small = file_get_contents(PATH_TMP . '/' . $user_doc_small);
                            unlink(PATH_TMP . '/' . $user_doc_small);
                        }

                        unlink(PATH_TMP . '/' . $user_doc);

                        $DB->Execute(
                            'INSERT INTO users_docs SET user_id = ?, title = ?, `file` = ?, fshow = ?, file_blob = ?, file_small_blob = ?',
                            array($id, $_POST['docs_titles'][$k], $user_doc, (int)$_POST['docs_show'][$k], $blob, $blob_small)
                        );
                    }
                }
            }
            
            // Обновляем статус предложений пользователя
            if (isset($_POST['status']) && $upd['cnt'] < MAX_CNT_RT_UPDATE)
            {
                include_once(PATH_BACKEND . "/php/classes/class.Sphinx.php");
                $sphinx = new Sphinx($DB);
                $status_new = (int)$_POST['status'];
                $status_old = (int)$status_old;

                if ($status_new == 1 && $status_new != $status_old)
                {
                    $sphinx->add_user_rt_index($id);
                }

                if ($status_old == 1 && $status_old != $status_new)
                {
                    $sphinx->remove_user_rt_index($id);
                }
            }

            adm_redirect("?module=users&action=list");
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "delete")
	    {
            if (isset($_GET['id']))
            {
                $DB->Execute("DELETE FROM users_last_visit WHERE user_id = ?", array((int)$_GET['id']));
                $DB->Execute("DELETE FROM users_chat WHERE user_id = ?", array((int)$_GET['id']));
                $DB->Execute("DELETE FROM users_chat_tokens WHERE owner_type = 'user' AND owner_value = ?", array((int)$_GET['id']));
                $user->Delete((int)$_GET['id']);
            }
            else if (isset($_POST['ids']))
            {
                if (!empty($_POST['ids']) && is_array($_POST['ids']))
                {
                    foreach ($_POST['ids'] as $user_id)
                    {
                        $DB->Execute("DELETE FROM users_last_visit WHERE user_id = ?", array((int)$user_id));
                        $DB->Execute("DELETE FROM users_chat WHERE user_id = ?", array((int)$_GET['id']));
                        $DB->Execute("DELETE FROM users_chat_tokens WHERE owner_type = 'user' AND owner_value = ?", array((int)$_GET['id']));
                        $user->Delete((int)$user_id);
                    }
                }
            }

	        adm_redirect("?module=users&action=list");
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "login" && isset($_GET['id']))
	    {
	        $_SESSION['logoff'] = true;
	        $_SESSION['logoff_id'] = $_SESSION['user_id'];
	        $_SESSION['user_id'] = (int)$_GET['id'];

	        adm_redirect("");
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "search-form")
	    {
	        $smarty->display("users/search-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "mailing-form")
	    {
            $users_list = $DB->GetAll("SELECT * FROM users ORDER BY id ASC");
            $smarty->assign("users_list", $users_list);

	        $smarty->display("users/mailing-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------


	    // ------------------------------------
	    else if ($_GET['action'] == "mailing")
	    {
            $mail = new PHPMailer();
            $mail->AddReplyTo($_POST['mailing_from_email'], $_POST['mailing_from_name']);
            $mail->SetFrom($_POST['mailing_from_email'], $_POST['mailing_from_name']);
            $mail->Subject = $_POST['mailing_subject'];
            $mail->AltBody = strip_tags($_POST['mailing_text']);
            $mail->MsgHTML($_POST['mailing_text']);

            $emails = array();
            if (!empty($_POST['user_sel']))
            {
                $emails = $DB->GetCol("SELECT email FROM users WHERE email!='' AND id IN(".implode(",", $_POST['user_sel']).")");
            }            
            
            foreach ($emails as $k => $email)
            {
                $email_to = preg_split("/[,;\s]/", $email, -1, PREG_SPLIT_NO_EMPTY);
                foreach ($email_to as $value)
                {
                    $mail->AddAddress(trim($value));
                }
                $mail->Send();
                $mail->ClearAddresses();
				usleep(1000000);
            }
            
            adm_message("Рассылка успешно завершена. Всего было отправлено писем &mdash; <strong>".count($emails)."</strong>");
	    }
	    // ------------------------------------
	}
}
else
{
    adm_message($control->GetMessage());
}




?>
