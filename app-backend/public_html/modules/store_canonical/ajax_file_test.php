<?php

$result = array(
    'success' => false
);

$file = !empty($_FILES['file']) ? $_FILES['file'] : null;

if ($file)
{
    $_file_path = $file['tmp_name'];

    include_once (PATH_BACKEND . "/php/classes/class.StoreCanonical.php");
    $canonical = new StoreCanonical($DB);

    $check_result = $canonical->check_file($_file_path);

    $result['list'] = empty($check_result['error']) ? $check_result['list'] : array();
    $result['error'] = $check_result['error'];
    $result['success'] = empty($check_result['error']);
}

echo json_encode($result);