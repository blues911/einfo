<?php

defined("ADMIN") or die("FAIL");

///////////////////////////////////

define("MAX_ONPAGE", 50);

if ($control->CheckModuleAccess($_GET['module']))
{
    if (isset($_GET['action']))
    {
        include_once (PATH_BACKEND . "/php/classes/class.StoreCanonical.php");
        $canonical = new StoreCanonical($DB);

        // ------------------------------------
        if ($_GET['action'] == "list")
        {
            $filter = array();

            $filter['search'] = (!empty($_GET['search'])) ? urldecode(trim($_GET['search'])) : '';

            $all_items = $canonical->get_count($filter);

            $pager = new Pager($all_items, MAX_ONPAGE, "dynamic");

            $filter['limit']['start'] = $pager->GetLimitStart();
            $filter['limit']['end'] = $pager->GetLimitEnd();

            $list = $canonical->get_list($filter);

            $i = $pager->GetLimitStart() + 1;
            foreach ($list as $k => $value)
            {
                $list[$k]['num'] = $i;
                $i++;
            }

            $smarty->assign("list", $list);
            $smarty->assign("host_frontend", HOST_FRONTEND);
            $smarty->assign("search", $filter['search']);
            $smarty->assign("pager", $pager->GetPages());
            $smarty->display("store_canonical/list.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "add")
        {
            $comp_title = !empty($_POST['comp_title']) ? trim($_POST['comp_title']) : false;

            if ($comp_title)
            {
                $canonical->add($comp_title);
            }

            adm_redirect("?module=store_canonical&action=list");
        }
        // ------------------------------------


        // ------------------------------------
        else if ($_GET['action'] == "delete")
        {
            $ids = !empty($_GET['id']) ? (int)$_GET['id'] : (!empty($_POST['ids']) ? $_POST['ids'] : null);

            $canonical->delete($ids);

            adm_redirect("?module=store_canonical&action=list");
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "upload-form")
        {
            $smarty->display("store_canonical/upload-form.tpl");
            $smarty->clear_all_assign();
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "upload")
        {
            if (!empty($_FILES['file']))
            {
                $file = $_FILES['file'];
                $replace = !empty($_POST['replace']);

                // Проверяем файл и получаем
                $check_result = $canonical->check_file($file['tmp_name']);

                if (empty($check_result['error']))
                {
                    // Если выбрана полная замена очищаем таблицу
                    if ($replace)
                    {
                        $canonical->delete_all();
                    }

                    // Вносим данные из файла
                    $canonical->add($check_result['list']);
                }
            }

            adm_redirect("?module=store_canonical&action=list");
        }
        // ------------------------------------

        // ------------------------------------
        else if ($_GET['action'] == "export")
        {
            $list = $canonical->get_list();

            if (!empty($list))
            {
                // Очистка буфера
                if (ob_get_level())
                {
                    ob_end_clean();
                }

                header('Content-disposition: attachment; filename=comp_title_list.txt');
                header('Content-type: text/plain');

                // Распечатываем построчно comp_title
                foreach ($list as $item)
                {
                    echo $item['comp_title'] . "\r\n";
                }

                exit();
            }
            else adm_redirect("?module=store_canonical&action=list");
        }

    }
}