<?php

$iframe_body = "<script>parent.ShowFileTemplate('%s');</script>";

if (isset($_FILES['price']) && $_FILES['price']['name'] != "")
{
    if (!$_POST['xml_format'])
    {
	    $ins['upload_date'] = date("Y-m-d H:i:s");
	    $ins['user_id'] = $_SESSION['user_id'];
	    $ins['use_ftemplate'] = isset($_POST['use_ftemplate']) ? 1 : 0;
	    $ins['currency'] = $_POST['currency'];

	    $finfo = pathinfo($_FILES['price']['name']);
	    $ins['tmpfile'] = md5(uniqid()).".".$finfo['extension'];

	    if (preg_match("/^([a-z]+)_([a-z]+)$/", $_POST['price_type'], $matches))
	    {
	        $ins['upload_type'] = $matches[2];

	        if ($matches[1] == "stock")
	        {
	            $ins['stock'] = isset($_POST['stock_hide']) ? -1 : 1;
	        }
	        else
	        {
	            $ins['delivery'] = ($_POST['delivery_time'] != "") ? (int)$_POST['delivery_time'] : 0;
	        }
	   	}

	    $DB->AutoExecute("tmp_prices", $ins, "INSERT");

	    $upload_id = $DB->Insert_ID();

	    if (move_uploaded_file($_FILES['price']['tmp_name'], PATH_PRICES."/0_tmp/".$ins['tmpfile']))
	    {
	        printf($iframe_body, "upload(".$upload_id.")");
	    }
	    else printf($iframe_body, "error");
    }
	else
	{
	    if (move_uploaded_file($_FILES['price']['tmp_name'], PATH_PRICES."/3_xml/".md5(uniqid()).".xml"))
	    {
	        printf($iframe_body, "upload(0)");
	    }
	    else printf($iframe_body, "error");
	}
}


?>