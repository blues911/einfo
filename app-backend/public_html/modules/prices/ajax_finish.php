<?php

$upload_id = (int)$_GET['upload_id'];
$upload = $DB->GetRow("SELECT * FROM tmp_prices WHERE id=?", $upload_id);

if ($upload && $upload['user_id'] == $_SESSION['user_id'])
{
    $sortable = explode(",", $_POST['sortable']);

    if ($_POST['save_ftemplate'])
    {
        $DB->Execute("DELETE FROM users_templates WHERE user_id=?", $upload['user_id']);

        $i = 1;
        foreach ($sortable as $value)
        {
            $DB->Execute("INSERT INTO users_templates VALUES(?, ?, ?)", array($upload['user_id'], $value, $i));
            $i++;
        }
    }

    $tagsinfo = $DB->GetAll("SELECT id, tag FROM price_columns WHERE id IN(".implode(",", $sortable).")");
    foreach ($sortable as $fid)
    {
        foreach ($tagsinfo as $tag)
        {
            if ($fid == $tag['id'])
            {
                $tags[] = $tag['tag'];
                break;
            }
            else if ($fid == 0)
            {
                $tags[] = "FAIL";
                break;
            }
        }
    }

    $price = new Price(PATH_PRICES . '/0_tmp/' . $upload['tmpfile'], 'utf-8');
    $price->set_tags(array_slice($tags, 0, $price->get_columns_cnt()));

    if ($upload['stock'] != 0)
    {
        $sd = ($upload['stock'] == 1) ? "stock" : "stock_hide";
    }
    else
    {
        $sd = ($upload['delivery'] != 0) ? "delivery_".$upload['delivery'] : "delivery";
    }

    // формирование имени файла
    $finfo = pathinfo($upload['tmpfile']);
    $fname = implode(".", array($upload['user_id'],
                                $upload['upload_type'],
                                $upload['currency'],
                                $sd,
                                $finfo['extension']));

    $old_files = $DB->GetCol('SELECT filename FROM users_prices WHERE user_id = ?', array($upload['user_id']));
    if (!empty($old_files))
    {
        foreach ($old_files as $old_file_name)
        {
            unlink(PATH_PRICES . '/1_prices/' . $old_file_name);
        }

        $DB->Execute('DELETE FROM users_prices WHERE user_id = ?', array($upload['user_id']));
    }

    if ($price->save_to_preload(PATH_PRICES . '/1_prices/' . $fname))
    {
        $price->save_tags($upload['user_id']);
        $DB->Execute("DELETE FROM tmp_prices WHERE id=?", $upload_id);
    }
}