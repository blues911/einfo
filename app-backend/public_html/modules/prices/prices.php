<?php

defined("ADMIN") or die("FAIL");

if ($control->CheckModuleAccess($_GET['module']))
{
	if (isset($_GET['action']))
	{
	    // подключение к пользовательской БД
        $DB2 = ADONewConnection(DB_USERS_DSN);
        $DB2->SetFetchMode(ADODB_FETCH_ASSOC);
        $DB2->Execute("SET NAMES utf8");
        $DB2->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
        $DB2->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
        $DB2->cacheSecs = DB_CACHE_TIME;
        //$DB2->debug = true;


	    // ------------------------------------
	    if ($_GET['action'] == "upload-form")
	    {
            $ftemplate = $DB->GetOne("SELECT COUNT(*) FROM users_templates WHERE user_id=?", array($_SESSION['user_id']));

            $smarty->assign("ftemplate", $ftemplate);
            $smarty->assign("id", $_SESSION['user_id']);
	        $smarty->display("prices/upload-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------

	    // ------------------------------------
	    else if ($_GET['action'] == "settings-form")
	    {
	        $smarty->display("prices/settings-form.tpl");
	        $smarty->clear_all_assign();
	    }
	    // ------------------------------------

	    // ------------------------------------
	    else if ($_GET['action'] == "settings")
	    {
	        $user_tbl = DB_USERS_PREFIX.$DB->GetOne("SELECT MD5(CONCAT(login,password)) FROM users WHERE id=?", array($_SESSION['user_id']));
            if ($DB2->GetOne("SHOW TABLES LIKE '".$user_tbl."'") !== false)
            {
                $modified = false;
                $table_lock = $DB2->GetOne("SHOW OPEN TABLES FROM e2_users WHERE In_use > 0 AND `Table` = ?", array($user_tbl));

    	        if (isset($_POST['delete_all']) && !$table_lock)
    	        {
                  $DB->Execute('UPDATE users SET price_file_hash = ? WHERE id = ?', array(null, $_SESSION['user_id']));
                  $DB2->Execute("TRUNCATE " . $user_tbl);
                  $modified = true;
                  adm_message("Данные успешно обновлены. Обновленный прайс появится на сайте через некоторое время.");
    	        }
    	        else if (!$table_lock)
    	        {
    	            if (isset($_POST['delete_sale_stock']))
    	            {
    	                $DB2->Execute("DELETE FROM ".$user_tbl." WHERE stock!=0");
    	                $modified = true;
    	            }

    	            if (isset($_POST['delete_sale_delivery']))
    	            {
    	                $DB2->Execute("DELETE FROM ".$user_tbl." WHERE delivery!=0");
    	                $modified = true;
    	            }
                    adm_message("Данные успешно обновлены. Обновленный прайс появится на сайте через некоторое время.");
    	        }
                else
                {
                    adm_message("В данный момент очистка прайс-листа не возможна. Идет обработка данных. Попробуйте повторить через 15-20 минут.");
                }
    	    }
	    }
	    // ------------------------------------

	    // ------------------------------------
	    else if ($_GET['action'] == "export-form")
	    {
	        $smarty->display("prices/export-form.tpl");
	        $smarty->clear_all_assign();
	    }
        // ------------------------------------
        else if ($_GET['action'] == "export-categories")
        {
            $categories = $DB->GetAll("SELECT * FROM categories WHERE c_right-c_left=1 ORDER BY c_left ASC");

            $export_data = "";
            $header_file_ext = "txt";
            $header_content_type = "plain/txt";

            switch ($_POST['export_categories_format'])
            {
                case "txt":
                    foreach ($categories as $value)
                    {
                        $export_data .= $value['full_title']."\r\n";
                    }

                    $header_file_ext = "txt";
                    $header_content_type = "plain/txt";
                    break;
            }

            $export_data = iconv('utf-8', 'windows-1251', $export_data);

            // отдача в браузер на скачивание
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: ".$header_content_type);
            header("Content-Disposition: attachment; filename=ExportCategories.".$header_file_ext);
            header("Content-Length: ".mb_strlen($export_data));
            print $export_data;
            exit();

        }
        // ------------------------------------
	    else if ($_GET['action'] == "export-price")
	    {
          $user_tbl = DB_USERS_PREFIX.$DB->GetOne("SELECT MD5(CONCAT(login,password)) FROM users WHERE id=?", array($_SESSION['user_id']));
          $show_tables_like = $DB2->GetOne("SHOW TABLES LIKE '".$user_tbl."'");
          $total_components = $DB2->GetOne("SELECT COUNT(*) FROM " . $user_tbl);

          if ($show_tables_like !== false && $total_components > 0)
          {
              // отдача в браузер на скачивание
              header("Pragma: public");
              header("Expires: 0");
              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              header("Cache-Control: private", false);
              header("Content-Type: application/vnd.ms-excel");
              header("Content-Disposition: attachment; filename=ExportPrice_".date("d-m-Y").".txt");

              $price_type = trim($_POST['price_type']);
              $price_columns = array();
              $rs = $DB->Execute("SELECT name, title FROM price_columns");
              while (!$rs->EOF)
              {
                  $price_columns[$rs->fields["name"]] = $rs->fields["title"];
                $rs->MoveNext();
              }

            // формирование условия выборки
            $arr_options = array();
            if ($price_type != 'all')
            {
                if ($price_type == 'sale_stock')
                {
                    $arr_options[] = "(stock!=0)";
                }

                if ($price_type == 'sale_delivery')
                {
                    $arr_options[] = "(delivery!=0)";
                }
            }

              // получение данных из БД
              $i = 0;
              $rs = $DB2->Execute("SELECT * FROM " . $user_tbl . ((count($arr_options)) ? " WHERE " . implode(" OR ", $arr_options) : ""));
              while (!$rs->EOF)
              {
                  $export_row = array();
                  foreach ($price_columns as $k => $value)
                  {
                      if (isset($rs->fields[$k]))
                      {
                          $export_row[$k] = ($i != 0) ? $rs->fields[$k] : $value;
                      }

                      elseif ($k == 'roz')
                      {
                          $_price = $rs->fields['prices'];
                          $_price = json_decode($_price, true);

                          if (!empty($_price))
                          {
                              $_price = current($_price);
                              $_roz = !empty($_price['p']) ? $_price['p'] : 0;
                              $_roz_cur = !empty($_price['c']) ? $_price['c'] : 'rur';

                              $export_row['roz'] = ($i != 0) ? $_roz : $price_columns['roz'];
                              $export_row['roz_cur'] = ($i != 0) ?$_roz_cur : $price_columns['roz_cur'];
                          }
                      }
                  }

                  if ($_POST['codepage'] == 'cp1251')
                  {
                      echo(iconv('utf-8', 'windows-1251', implode("\t", $export_row) . "\r\n"));
                  }
                  elseif ($_POST['codepage'] == 'utf8')
                  {
                      echo implode("\t", $export_row) . "\r\n";
                  }

                  if ($i != 0)
                  {
                      $rs->MoveNext();
                  }
                  $i++;
              }
              exit;
    	    }
    	    else
          {
              adm_message("Ваш прайс-лист в базе данных отсутствует.");
          }
	    }

	}
}
else
{
    adm_message($control->GetMessage());
}
