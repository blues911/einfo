<?php

$upload_id = (int)$_GET['upload_id'];
$upload = $DB->GetRow("SELECT * FROM tmp_prices WHERE id=?", $upload_id);

if ($upload && $upload['user_id'] == $_SESSION['user_id'])
{
    // выборка полей для прайса
    $columns = $DB->GetAll("SELECT id, title FROM price_columns");
    $columns_order = array();

    if ($upload['use_ftemplate'])
    {
        $columns_order = $DB->GetCol("SELECT column_id FROM users_templates WHERE user_id=? ORDER BY position ASC", $_SESSION['user_id']);
    }

    // выборка 2-х случайных строк из прайса
    $price = new Price(PATH_PRICES . '/0_tmp/' . $upload['tmpfile']);
    if ($price->save_to_preload(PATH_PRICES . '/0_tmp/' . $upload['tmpfile'] . '.csv'))
    {
        $DB->Execute('UPDATE tmp_prices SET tmpfile = ? WHERE id = ?', array($upload['tmpfile'] . '.csv', $upload_id));
    }

    $rndrows = $price->get_random_rows(2);

    $filerow = array();
    for ($i = 0; $i < $price->get_columns_cnt(); $i++)
    {
        $filerow[$i]['selected'] = isset($columns_order[$i]) ? $columns_order[$i] : 0;
        $filerow[$i]['rnd1'] = isset($rndrows[0][$i]) ? $rndrows[0][$i] : "";
        $filerow[$i]['rnd2'] = isset($rndrows[1][$i]) ? $rndrows[1][$i] : "";
    }

    // буквенный порядок
    $letters = array();
    for ($i = 0; $i < 26; $i++)
    {
        $letters[] = chr(65 + $i);
    }

    $smarty->assign("columns", $columns);
    $smarty->assign("filerow", $filerow);
    $smarty->assign("letters", $letters);
    $smarty->display("prices/ftemplate.tpl");
}


?>