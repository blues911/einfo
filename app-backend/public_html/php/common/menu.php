<?php

$menu = $DB->GetAll("SELECT modules.*
	                     FROM modules
	                        INNER JOIN access ON access.module_id=modules.id
	                        INNER JOIN users ON users.level_id=access.level_id
	                     WHERE users.id=? AND users.disclaimer=1 AND modules.showmenu=1
	                     ORDER BY modules.position ASC", array($_SESSION['user_id']));

if (!$menu)
{
    $accesslevel = $DB->GetOne("SELECT level_id FROM users WHERE id=?", array($_SESSION['user_id']));
    if ($accesslevel == 0)
    {
        $menu = $DB->GetAll("SELECT * FROM modules WHERE showmenu=1 ORDER BY position ASC");

        // проверка на новые сообщения в форуме
        if (!empty($_COOKIE['forum_update']))
        {
            $m_forum_cookie = unserialize($_COOKIE['forum_update']);
        }

        $m_list = $DB->GetAll("SELECT topic.id, MAX(message.date) AS message_date
                             FROM forum_topics topic
                                LEFT JOIN forum_messages message ON message.topic_id=topic.id
                             GROUP BY topic.id");

        foreach ($m_list as $k => $value)
        {
            if (!isset($m_forum_cookie[$value['id']]) || strtotime($value['message_date']) > $m_forum_cookie[$value['id']])
            {
                $smarty->assign("forum_update", true);
                break;
            }
        }
    }
}

if ($menu)
{
    foreach ($menu as $k => $value)
    {
        $menu[$k]['selected'] = (isset($_GET['module']) && $value['name'] == $_GET['module']);
    }
}

$smarty->assign("menu", $menu);
$smarty->assign("disclaimer", $userinfo['disclaimer'] == 0);
$container['menu'] = $smarty->fetch("menu.tpl");
$smarty->clear_all_assign();



?>