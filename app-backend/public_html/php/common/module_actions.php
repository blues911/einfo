<?php

$pathfile = "modules/".$_GET['module']."/actions.xml";

if (file_exists($pathfile))
{
	$xml = simplexml_load_file($pathfile);

	$actions = array();
	$i = 0;

	if (preg_match("/^([-a-z0-9_]+)\./i", $_GET['action'], $matches))
	{
	    $act_xml = $xml->xpath("/actions/action[@submodule='".$matches[1]."']");
	}
	else $act_xml = $xml;


	foreach ($act_xml as $item)
	{
	    $actions[$i]['title'] = $item;
	    $actions[$i]['icon'] = $item['icon'];
	    $actions[$i]['url'] = "?module=".$_GET['module']."&action=".$item['link'];

	    $i++;
	}

	$smarty->assign("actions", $actions);
	$container['actions'] = $smarty->fetch("actions.tpl");
	$smarty->clear_all_assign();
}

?>