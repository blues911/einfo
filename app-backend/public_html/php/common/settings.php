<?php

// загрузка настроек из БД
$SETTINGS = array();
$setn = $DB->GetAll("SELECT * FROM settings");
foreach ($setn as $value)
{
    @list($section, $param) = explode(":", $value['var']);
    if (isset($param))
    {
        $SETTINGS[$section][$param] = $value['value'];
    }
    else
    {
        $SETTINGS[$section] = $value['value'];
    }
}

?>