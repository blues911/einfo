<?php

/**
 * Class CatalogWorkerManager
 *
 * Класс реализующий менеджера, который ищет канонические шаблоны для парсинга,
 * формирует пул задачи и передаёт его свободному воркеру (CatalogWorkerCompare).
 *
 * Общая задача:
 * 1. Сгенерировать все возможные варианты удовлетворяющие регулярному выражению.
 * 2. Найти компоненты в таблице (base_main), которые соответствуют сгенерированному каноническому имени
 *    по вхождению атомов и условию LIKE '<имя>%'.
 * 3. Посчитать количество предложений, медианы цен и записать их в таблицу comp_canon.
 *
 * Одновременно может работать только один менеджер. Количество воркеров любое.
 *
 * Задача представляет из себя сериализованный массив содержащий следующие элементы:
 * 0 - id канонического шаблона (таблица catalog_regexp), обрабатываемого в этой задаче;
 * 1 - id категории, к которой привязываются найденые компоненты;
 * 2 - строка с текущим регулярным выражением (в шаблоне их может быть несколько);
 * 3 - левый индекс (ОТ) слова из текущего регулярного выражения;
 * 4 - правый индекс (ДО) слова из текущего регулярного выражения.
 */
class CatalogWorkerManager {

    /**
     * Количество "слов" в задаче.
     * @var int
     */
    private $task_size = 10000;

    /**
     * Таймаут воркера (сек.), после которого он считается "зависшим".
     * @var int     секунды
     */
    private $timeout_compare = 60;

    /**
     * Таймаут воркера (сек.), после которого он удаляется из таблицы БД.
     * @var int     секунды
     */
    private $timeout_compare_remove = 120;

    /**
     * Таймаут итерации (микросек.) при ожидании нового шаблона для парсинга.
     * @var int     микросекунды
     */
    private $timeout_sleep = 200000;

    /**
     * Таймаут итерации (микросек.) при ожидании свободного воркера для текущей задачи.
     * @var int     микросекунды
     */
    private $timeout_set_task = 200000;

    /**
     * Id с которым менеджер записывает свое текущее состояние в таблицу БД.
     * @var string
     */
    private $id = 'MANAGER';


    /**
     * Текущий шаблон в обработке.
     * @var array
     */
    private $template;

    /**
     * Счетчик текущей итерации.
     * Массив [левый индекс регулярного выражения, правый индекс регулярного выражения].
     * @var array
     */
    private $_iterator;

    /**
     * Сериализованная строка с последним заданием, либо false.
     * @var array/bool
     */
    private $_last_work;


    /**
     * Ссылка на подключение ADODB.
     * @var
     */
    private $DB;

    /**
     * Признак отладки.
     * @var bool
     */
    public $debug;


    /**
     * Конструктор класса.
     * @param $DB   Ссылка на подключение ADODB.
     */
    public function __construct(&$DB, $debug = false)
    {
        $this->DB = $DB;
        $this->debug = $debug;
        $this->template = null;
        $this->_iterator = array(0, 0);
        $this->_last_work = false;

        $this->_init_settings();

        $this->_restore_session();
    }

    private function _init_settings()
    {
        if (defined('POOL_SIZE'))
        {
            $this->task_size = POOL_SIZE;
        }

        if (defined('TIMEOUT_COMPARE'))
        {
            $this->timeout_compare = TIMEOUT_COMPARE;
        }

        if (defined('TIMEOUT_COMPARE_REMOVE'))
        {
            $this->timeout_compare_remove = TIMEOUT_COMPARE_REMOVE;
        }

        if (defined('TIMEOUT_SLEEP'))
        {
            $this->timeout_sleep = TIMEOUT_SLEEP;
        }

        if (defined('TIMEOUT_SET_TASK'))
        {
            $this->timeout_set_task = TIMEOUT_SET_TASK;
        }

        return true;
    }

    /**
     * Восстанавливает сессию после ошибки или падения менеджера.
     * @return bool
     */
    private function _restore_session()
    {
        $session = $this->DB->GetOne('SELECT task FROM catalog_workers WHERE worker_id = ?', array($this->id));

        if (!empty($session))
        {
            $session = unserialize($session);

            $template = $session[0];
            $_iterator = $session[1];
            $_last_work = $session[2];

            $_template = $this->DB->GetRow('SELECT * FROM catalog_regexp WHERE id = ?', array($template['id']));

            if (!empty($_template))
            {
                if ($template['regexp_template'] == $_template['regexp_template'] &&
                    $template['category_id'] == $_template['category_id'])
                {
                    if (!empty($_last_work))
                    {
                        $this->set_task($_last_work);
                    }

                    $this->template = $template;
                    $this->_iterator = $_iterator;
                }
            }
        }

        return true;
    }


    /**
     * Вывод отладочной информации.
     * @param string $text      Текст сообщения.
     */
    public function log($text)
    {
        if ($this->debug)
        {
            echo $text . PHP_EOL;
        }
    }


    /**
     * Сохраняем текущее состояние менеджера в БД.
     * @return bool
     */
    private function _save_session()
    {
        $session = array();

        $session[0] = $this->template;
        $session[1] = $this->_iterator;
        $session[2] = $this->_last_work;

        $session = serialize($session);

        $time = time();
        $this->DB->Execute('
            INSERT INTO
              catalog_workers (worker_id, task, last_time)
            VALUES
              (?, ?, ?)
            ON DUPLICATE KEY UPDATE
              task = ?,
              last_time = ?
        ', array($this->id, $session, $time, $session, $time));

        return true;
    }


    /**
     * Возвращает текущую задачу в виде сериализованного массива.
     * Массив [id канонического шаблона, id категории, строка регулярного выражения,
     * левый индекс регулярного выражения, правый индекс регулярного выражения].
     * @return bool|string      Строка с задачей, либо false.
     */
    public function get_task()
    {
        // Проверяем есть ли текущий шаблон
        if (empty($this->template))
        {
            $this->template = $this->get_template();
            $this->_iterator = array(0, 0);

            if(!empty($this->template))
            {
                $this->remove_template_components($this->template['id']);
                $this->log('## Parse template ' . $this->template['regexp_template']);
            }
        }

        // Если есть текущий шаблон, то формируем задачу
        if (!empty($this->template))
        {
            $_regexp = $this->template['regexp_collection'][$this->_iterator[0]];
            $_regexp_end = $_regexp->get_count_words() - 1;

            $regexp_id = $this->template['id'];
            $category_id = $this->template['category_id'];

            $begin = $this->_iterator[1] == 0 ? 0 : $this->_iterator[1] + 1;

            $end = $begin + $this->task_size;
            if ($end >= $_regexp_end)
            {
                $end = $_regexp_end;

                if (empty($this->template['regexp_collection'][$this->_iterator[0] + 1]))
                {
                    $this->set_template_need_update($this->template['id'], 0);
                    $this->log('## Template complete');
                    $this->template = null;
                    $this->_iterator = array(0, 0);
                }
                else
                {
                    $this->_iterator[0] += 1;
                    $this->_iterator[1] = 0;
                }
            }
            else
            {
                $this->_iterator[1] = $end;
            }

            $task = serialize(array($regexp_id, $category_id, $_regexp->regexp, $begin, $end));
        }
        else
        {
            $task = false;
        }

        $this->_last_work = $task;
        $this->_save_session();

        return $task;
    }


    /**
     * Забирает задачу у зависшего воркера.
     * @return bool|string      Строка с задачей, либо false.
     */
    public function get_sleeping_task()
    {
        $time = time();
        $worker = $this->DB->GetRow('
            SELECT SQL_NO_CACHE
              *
            FROM
              catalog_workers
            WHERE
              task IS NOT NULL
              AND last_time < ?
              AND worker_id != ?
        ', array($time - $this->timeout_compare, $this->id));

        if (!empty($worker))
        {
            if ($worker['last_time'] < $this->timeout_compare_remove)
            {
                $this->DB->Execute('DELETE FROM catalog_workers WHERE worker_id = ?', array($worker['worker_id']));
            }
            else
            {
                $this->DB->Execute('UPDATE catalog_workers SET task = NULL WHERE worker_id = ?', array($worker['worker_id']));
            }

            $this->log('- Get sleeping task from ' . $worker['worker_id']);

            $this->_last_work = $worker['task'];
            $this->_save_session();

            return $worker['task'];
        }
        else
        {
            return false;
        }
    }

    /**
     * Ожидает освобождения любого воркера и передает ему задачу.
     * @param string $task      Строка с задачей.
     * @return bool             true
     */
    public function set_task($task)
    {
        while(true)
        {
            $time = time();
            $free_compare = $this->DB->GetOne('
                SELECT
                  worker_id
                FROM
                  catalog_workers
                WHERE
                  task IS NULL
                  AND last_time > ?
                  AND worker_id != ?
            ', array($time - $this->timeout_compare, $this->id));

            if (!empty($free_compare))
            {
                $this->DB->Execute('
                    UPDATE
                      catalog_workers
                    SET
                      task = ?
                    WHERE
                      worker_id = ?
                ', array($task, $free_compare));

                $this->log('+ Set task to ' . $free_compare);

                return true;
            }
            else
            {
                usleep($this->timeout_set_task);
            }
        }
    }


    /**
     * Удаляет зависшие воркеры.
     * @return bool     true
     */
    private function _remove_sleeping_workers()
    {
        $this->DB->Execute('
            DELETE FROM catalog_workers WHERE task IS NULL AND last_time < ? AND worker_id != ?
        ', array(time() - $this->timeout_compare_remove, $this->id));

        return true;
    }


    /**
     * Пауза.
     * @return bool     true
     */
    public function sleep()
    {
        $this->_remove_sleeping_workers();
        usleep($this->timeout_sleep);
        return true;
    }


    /**
     * Проверяем наличие канонического шаблона требующего парсинга.
     * Если указан template_id, то загружаем указаный шаблон.
     * @param int $template_id      Id шаблона
     * @return array    Массив со строкой из БД;
     *                  пустой массив, если нет шаблонов для обработки;
     *                  false в случае ошибки.
     */
    public function get_template($template_id = -1)
    {
        if ($template_id == -1)
        {
            $template = $this->DB->GetRow('SELECT SQL_NO_CACHE * FROM catalog_regexp WHERE need_update = 1 LIMIT 1');

            if (empty($template))
            {
                return false;
            }
        }
        else
        {
            $template = $this->DB->GetRow('SELECT SQL_NO_CACHE * FROM catalog_regexp WHERE id = ?', array($template_id));

            if (empty($template))
            {
                return false;
            }
        }

        $template['regexp_collection'] = SimpleRegexp::parse_template($template['regexp_template']);

        return $template;
    }


    /**
     * Удаляем компоненты, которые были найдены по данному шаблону ранее, из БД.
     * @param int $template_id   Id канонического шаблона.
     * @return bool              True - успех; false - неудача.
     */
    public function remove_template_components($template_id)
    {
        return $this->DB->Execute('DELETE FROM catalog_comp_canon WHERE regexp_id = ?', array($template_id));
    }


    /**
     * Устанавливаем статус need_update для канонического шаблона.
     * @param int $template_id      Id канонического шаблона
     * @param int $need_update      1 - необходимо обновление, 0 - обновление не нужно
     * @return bool
     */
    public function set_template_need_update($template_id, $need_update)
    {
        return $this->DB->Execute('UPDATE catalog_regexp SET need_update = ? WHERE id = ?', array($need_update, $template_id));
    }

}
