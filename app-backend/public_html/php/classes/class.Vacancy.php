<?php

/*
 * Класс: Vacancy
 * Назначение: Управление вакансия на сайте
 *
 */


class Vacancy
{
    private $DB;
    private $userID;

    // конструктор
    function __construct(&$DBlink, $user_id)
    {
        $this->DB = &$DBlink;
        $this->userID = $user_id;
    }

    // проверка на разрешенность доступа
    public function CheckAccess($id)
    {
        $allow = $this->DB->GetOne("SELECT COUNT(*) FROM vacancy WHERE user_id=? AND id=?", array($this->userID, $id));
        return ($allow == 1);
    }

    // добавление вакансии
    function Add($ins)
    {
        $this->DB->AutoExecute("vacancy", $ins, "INSERT");
    }

    // редактирование вакансии
    function Edit($vacancy_id, $upd)
    {
        $this->DB->AutoExecute("vacancy", $upd, "UPDATE", "id=".$vacancy_id);
    }

    // удаление вакансии
    function Delete($id)
    {
        $this->DB->Execute("DELETE FROM vacancy WHERE id=?", array($id));
    }
}

?>