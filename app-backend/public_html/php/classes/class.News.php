<?php

/*
 * Класс: News
 * Назначение: Управление новостями на сайте
 *
 */


class News
{
    private $DB;
    private $userID;
    public $settings;

    // конструктор
    function __construct(&$DBlink, $setn, $user_id)
    {
        $this->DB = &$DBlink;
        $this->settings = $setn;
        $this->userID = $user_id;
    }


    // проверка изображения на формат
    public function CheckFormat($path)
    {
        if ($path != "")
        {
            $imginfo = pathinfo($path);
            $imgext = mb_strtolower($imginfo['extension'], 'utf-8');
            if (in_array($imgext, explode(",", $this->settings['img_allow_formats'])))
            {
                return $imgext;
            }
        }

        return false;
    }


    // проверка изображения на размер
    public function CheckSize($path)
    {
        list($imgwidth, $imgheight) = getimagesize($path);
        if ($imgwidth <= $this->settings['img_max_width'] && $imgheight <= $this->settings['img_max_height'])
        {
            return array("width" => $imgwidth, "height" => $imgheight);
        }
        else return false;
    }


    // проверка на разрешенность доступа
    public function CheckAccess($id)
    {
        $allow = $this->DB->GetOne("SELECT COUNT(*) FROM news WHERE user_id=? AND id=?", array($this->userID, $id));
        return ($allow == 1);
    }


    // добавление новости
    function Add($data)
    {
        $ins['user_id'] = $this->userID;
        $ins['date'] = $data['date'];
        $ins['status'] = $data['status'];
        $ins['title'] = $data['title'];
        $ins['text'] = $data['text'];

        // проверка изображения
        $imgext = $this->CheckFormat($data['img']['name']);
        if ($imgext !== false)
        {
            $ins['img'] = 'news_' . md5(uniqid()) . '.' . $imgext;
            move_uploaded_file($data['img']['tmp_name'], PATH_TMP . '/' . $ins['img']);
            image_magick('convert ' . PATH_TMP . '/' . $ins['img'] . ' -resize ' . $this->settings['thumbnail_max_width'] . ' -quality 95 ' . PATH_TMP . '/' . $ins['img']);
            $ins['img_blob'] = file_get_contents(PATH_TMP . '/' . $ins['img']);
            unlink(PATH_TMP . '/' . $ins['img']);
        }

        $this->DB->AutoExecute("news", $ins, "INSERT");
    }


    // редактирование новости
    function Edit($data)
    {
        $upd['status'] = $data['status'];
        $upd['title'] = $data['title'];
        $upd['text'] = $data['text'];

        // проверка изображения
        $imgext = $this->CheckFormat($data['img']['name']);
        if ($imgext !== false)
        {
            $upd['img'] = 'news_' . md5(uniqid()) . '.' . $imgext;
            move_uploaded_file($data['img']['tmp_name'], PATH_TMP . '/' . $upd['img']);
            image_magick('convert ' . PATH_TMP . '/' . $upd['img'] . ' -resize ' . $this->settings['thumbnail_max_width'] . ' -quality 95 ' . PATH_TMP . '/' . $upd['img']);
            $upd['img_blob'] = file_get_contents(PATH_TMP . '/' . $upd['img']);
            unlink(PATH_TMP . '/' . $upd['img']);
        }

        $this->DB->AutoExecute("news", $upd, "UPDATE", "id=".$data['id']);
    }


    // изменение статуса новости
    function ChangeStatus($id, $status)
    {
        $this->DB->Execute("UPDATE news SET status=? WHERE id=?", array($status, $id));
    }


    // удаление новости
    function Delete($id)
    {
        $this->DB->Execute("DELETE FROM news WHERE id=?", array($id));
    }

    public function GetFile($id)
    {
        return $this->DB->GetRow('SELECT `blob`, `img` FROM news_images WHERE `id` = ?', array($id));
    }

    public function GetFileByName($filename)
    {
        return $this->DB->GetRow('SELECT `blob`, `img` FROM news_images WHERE `img` = ?', array($filename));
    }

    public function SetFile($id, $blob)
    {
        $this->DB->Execute('UPDATE news_images SET `blob` = ? WHERE `id` = ?', array($blob, $id));
        return true;
    }
}