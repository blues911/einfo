<?php

/*
 * Класс: StoreCanonical
 * Назначение: Управление индексацией для страниц компонентов
 *
 */

class StoreCanonical
{
    private $DB;

    // конструктор
    function __construct(&$DB)
    {
        $this->DB = $DB;
    }

    // добавление страниц для индекса
    function add($data)
    {
        $values = array();
        $query = 'INSERT IGNORE INTO store_canonical (comp_title) VALUES ';

        if (!is_array($data))
        {
            $data = array($data);
        }

        foreach ($data as $value)
        {
            $values[] = '(' . $this->DB->qstr($value) . ')';
        }

        if (empty($values)) return false;

        $query .= implode(', ', $values);

        if ($this->DB->Execute($query))
        {
            return true;
        }

        return false;
    }

    // Удаление страниц для индекса
    function delete($data)
    {
        $ids = array();

        if (!is_array($data))
        {
            $data = array($data);
        }

        foreach ($data as $value)
        {
            $ids[] = (int)$value;
        }

        if (empty($ids)) return false;

        $this->DB->Execute('DELETE FROM store_canonical WHERE id IN (' . implode(', ', $ids) . ')');

        return true;
    }

    // Удаление всех страниц для индекса
    function delete_all()
    {
        $this->DB->Execute('DELETE FROM store_canonical');

        return true;
    }

    // Список пользователей
    public function get_count($filter = array())
    {
        $filter = $this->_sql_filter($filter);

        $list = $this->DB->GetOne('
            SELECT COUNT(1)
            FROM (
                SELECT
                  *
                FROM
                  store_canonical
                ' . $filter['where'] . '
            ) AS t'
        );

        return (int)$list;
    }

    // Список пользователей
    public function get_list($filter = array())
    {
        $filter = $this->_sql_filter($filter);

        return $this->DB->GetAll('
            SELECT
              *
            FROM
              store_canonical
            ' . $filter['where']
              . $filter['order']
              . $filter['limit']
        );
    }

    // Фильтры
    protected function _sql_filter($filter = array())
    {
        // Сортировка по умолчанию
        if (empty($filter['order']))
        {
            $filter['order'] = ' ORDER BY comp_title ASC';
        }

        // Поиск по совпадению
        if (!empty($filter['search']))
        {
            $filter['where'][] = ' comp_title LIKE "%' . $this->DB->addq($filter['search']) . '%"';
        }

        // Склеиваем where
        if (isset($filter['where']))
        {
            if (count($filter))
            {
                $filter['where'] = ' WHERE ' . implode(' AND ', $filter['where']);
            }
            else
            {
                $filter['where'] = '';
            }
        }
        else
        {
            $filter['where'] = '';
        }

        // Лимит
        if (isset($filter['limit']))
        {
            if (is_array($filter['limit']) && isset($filter['limit']['start'], $filter['limit']['end']))
            {
                $filter['limit'] = ' LIMIT ' . $filter['limit']['start'] . ', ' . $filter['limit']['end'];
            }
            else
            {
                $filter['limit'] = ' LIMIT ' . $filter['limit'];
            }
        }

        return $filter;
    }

    public function check_file($file_path)
    {
        $result = array(
            'list' => array(),
            'error'    => ''
        );

        if (!is_readable($file_path)) $result['error'] = 'no_file';

        // Парсим файл
        if ($good = @fopen($file_path, 'r'))
        {
            while (($buffer = fgets($good, 4096)) !== false)
            {
                $comp_title = trim($buffer);

                $result['list'][] = $comp_title;
            }
        }

        if (empty($result['list'])) $result['error'] = 'no_items';

        return $result;
    }
}