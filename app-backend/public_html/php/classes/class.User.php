<?php

/*
 * Класс: User
 * Назначение: Работа с пользователями
 *
 */

class User
{
    private $DB;		// идентификатор БД (пользовательская)
    private $DB2;		// идентификатор БД (буферная)
    private $message;	// сообщение класса

    // конструктор
    function __construct(&$DBlink, &$DB2link = null)
    {
        $this->DB = &$DBlink;

        if (is_null($DB2link))
        {
            $this->DB2 = &$DBlink;
        }
        else
        {
            $this->DB2 = &$DB2link;
        }
    }

    // системное сообщение
    public function GetMessage()
    {
        return $this->message;
    }


    // добавление пользователя
    public function Add($ins)
    {
        $this->DB->AutoExecute("users", $ins, "INSERT");
        $id = $this->DB->Insert_ID();

        $this->DB->Execute('
            INSERT INTO
              users_status
            SET
              user_id = ?,
              `status` = ?,
              `access_level` = ?,
              `date` = NOW()
        ', array($id, $ins['status'], $ins['level_id']));

        $dist_hash = md5(iconv('utf-8', 'windows-1251', $ins['login'].$ins['password']));
        $this->DB2->Execute(
            'CREATE TABLE ' . DB_USERS_PREFIX . $dist_hash . ' (
                `id` bigint unsigned NOT NULL AUTO_INCREMENT,
                `category` varchar(255) NOT NULL DEFAULT "",
                `title` varchar(255) NOT NULL DEFAULT "",
                `descr` varchar(255) NOT NULL DEFAULT "",
                `manuf` varchar(64) NOT NULL DEFAULT "",
                `prices` text,
                `mfgdate` year(4) NOT NULL DEFAULT "0000",
                `doc_url` varchar(255) NOT NULL DEFAULT "",
                `comp_url` varchar(255) NOT NULL,
                `action` enum("sale","buy") NOT NULL DEFAULT "sale",
                `stock` int(11) NOT NULL DEFAULT "0",
                `stock_remote` int(11) NOT NULL DEFAULT "0",
                `delivery` tinyint(3) NOT NULL DEFAULT "0",
                PRIMARY KEY (`id`),
                UNIQUE KEY `uniq` (`title`,`category`,`mfgdate`,`action`,`delivery`,`descr`,`manuf`)
            ) ENGINE=MyISAM DEFAULT CHARSET=cp1251'
        );

        return $id;
    }

    // редактирование пользователя
    public function Edit($user_id, $upd)
    {
        $authdata = $this->DB->GetRow('SELECT login, password, status, level_id, MD5(CONCAT(login, `password`)) AS `hash` FROM users WHERE id=?', array($user_id));

        // проверка смены статуса
        if ((isset($upd['status']) && $upd['status'] != $authdata['status']) || (isset($upd['level_id']) && $upd['level_id'] != $authdata['level_id']))
        {
            $this->DB->Execute(
                'INSERT INTO
                  users_status
                SET
                  user_id = ?,
                  `status` = ?,
                  `access_level` = ?,
                  `date` = NOW()',
                array($user_id, isset($upd['status']) ? $upd['status'] : $authdata['status'] , isset($upd['level_id']) ? $upd['level_id'] : $authdata['level_id'])
            );

            if ($upd['status'] == 1)
            {
                $this->DB2->Execute('REPAIR TABLE ' . DB_USERS_PREFIX . $authdata['hash']);
            }
        }


        // проверка уникальности логина
        if (isset($upd['login']) && $upd['login'] != $authdata['login'])
        {
            $login_exist = $this->DB->GetOne("SELECT COUNT(*) FROM users WHERE login=?", array($upd['login']));
            if ($login_exist)
            {
                unset($upd['login']);
            }
        }

        // переименование буферной таблицы
        $authdata = $this->DB->GetRow("SELECT login, password FROM users WHERE id=?", array($user_id));
        if ($authdata !== false)
        {
            if (isset($upd['password']) || (isset($upd['login']) && $upd['login'] != $authdata['login']))
            {
                $oldname = DB_USERS_PREFIX.md5(iconv('utf-8', 'windows-1251', $authdata['login'].$authdata['password']));
                $is_buf = $this->DB2->GetOne("SHOW TABLES LIKE '".$oldname."'");
                if ($is_buf !== false)
                {
                    $nlogin = isset($upd['login']) ? $upd['login'] : $authdata['login'];
                    $npassword = isset($upd['password']) ? $upd['password'] : $authdata['password'];
                    $newname = DB_USERS_PREFIX.md5(iconv('utf-8', 'windows-1251', $nlogin.$npassword));
                    $this->DB2->Execute("RENAME TABLE ".$oldname." TO ".$newname);
                }
        	}
        }

        $this->DB->AutoExecute("users", $upd, "UPDATE", "id=".$user_id);
    }


    // удаление пользователя
    public function Delete($user_id)
    {
        $user_hash = $this->DB->GetOne("SELECT MD5(CONCAT(login,password)) FROM users WHERE id=?", array($user_id));

        $this->DB->Execute("DELETE FROM users WHERE id=?", array($user_id));
        $this->DB->Execute("DELETE FROM users_templates WHERE user_id=?", array($user_id));
        $this->DB2->Execute("DROP TABLE IF EXISTS ".DB_USERS_PREFIX.$user_hash);

        $news_images = $this->DB->GetCol("SELECT img FROM news WHERE user_id=?
         							      UNION
         							      SELECT img FROM news_images WHERE user_id=?", array($user_id, $user_id));
        foreach ($news_images as $value)
        {
             unlink(NEWSIMG_PATH."/".$value);
        }

        $banners = $this->DB->GetCol("SELECT banner FROM users_banners WHERE user_id=?", array($user_id));
        foreach ($banners as $value)
        {
             unlink(BANNERIMG_PATH."/".$value);
        }

        $userdocs = $this->DB->GetCol("SELECT file FROM users_docs WHERE user_id=?", array($user_id));
        foreach ($userdocs as $value)
        {
            unlink(USERDOCS_PATH."/".$value);
            unlink(USERDOCS_SMALL_PATH."/".$value);
        }

        // Удаляем из base_main и других таблиц
        $this->DB->Execute('DELETE FROM base_main WHERE user_id=?', array($user_id));
        $this->DB->Execute('DELETE FROM users_status WHERE user_id=?', array($user_id));
        $this->DB->Execute('DELETE FROM users_stat WHERE user_id=?', array($user_id));
        $this->DB->Execute('DELETE FROM billing WHERE user_id=?', array($user_id));

        $this->DB->Execute("DELETE FROM news WHERE user_id=?", array($user_id));
        $this->DB->Execute("DELETE FROM news_images WHERE user_id=?", array($user_id));
        $this->DB->Execute("DELETE FROM users_banners WHERE user_id=?", array($user_id));
        $this->DB->Execute("DELETE FROM users_docs WHERE user_id=?", array($user_id));
        $this->DB->Execute("DELETE FROM vacancy WHERE user_id=?", array($user_id));
        $this->DB->Execute("DELETE FROM resume WHERE user_id=?", array($user_id));

        $this->DB->Execute("DELETE FROM rating_votes WHERE user_voter_id=? OR user_id=?", array($user_id, $user_id));
        $this->DB->Execute("DELETE FROM clients_list_log WHERE user_id=?", array($user_id));

        $this->DB->Execute("UPDATE forum_topics SET user_id=NULL WHERE user_id=?", array($user_id));
        $this->DB->Execute("UPDATE forum_messages SET user_id=NULL WHERE user_id=?", array($user_id));

        include_once (PATH_FRONTEND . '/php/classes/class.Search.php');
        include_once (PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

        $sphinxRT = new SearchSphinxQL();

        // Удаляем связанные с пользователем RT индексы
        $sphinxRT->delete_rt_index_by_user((int)$user_id);
    }

    // Список пользователей
    public function get_list($filter = array())
    {
        $filter = $this->_sql_filter($filter);

        $users = $this->DB->GetAll('
            SELECT
              *
            FROM
              users
            ' . $filter['where']
            . $filter['order']
        );

        return $users;
    }

    // Фильтры
    protected function _sql_filter($filter = array())
    {
        // Сортировка по умолчанию
        if (empty($filter['order']))
        {
            $filter['order'] = ' ORDER BY id ASC';
        }
        elseif ($filter['order'] == 'title_asc')
        {
            $filter['order'] = ' ORDER BY title ASC';
        }

        // Без админ пользователей
        if (!empty($filter['without_admins']))
        {
            $filter['where'][] = 'id NOT IN (' . USER_ADMIN_ID . ', ' . USER_PARTNER_ID . ')';
        }

        // LIKE фильтр по наименованию пользователя
        if (!empty($filter['title_like']))
        {
            $filter['where'][] = 'title LIKE "%' . $filter['title_like'] . '%"';
        }
        elseif (!empty($filter['title_or_id_like']))
        {
            $filter['where'][] = 'title LIKE "%' . $filter['title_or_id_like'] . '%" OR id = ' . $this->DB->qstr($filter['title_or_id_like']);
        }

        // Склеиваем where
        if (isset($filter['where']))
        {
            if (count($filter))
            {
                $filter['where'] = ' WHERE ' . implode(' AND ', $filter['where']);
            }
            else
            {
                $filter['where'] = '';
            }
        }
        else
        {
            $filter['where'] = '';
        }

        return $filter;
    }
}
