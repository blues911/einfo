<?php

/*
 *
 * Класс: MyDate
 * Назначение: вывод даты в различных форматах (включая русский вариант),
 * формирования календаря.
 *
 */

class MyDate
{
    private $year;
    private $month;
    private $day;
    private $weekday;
    private $hour;
    private $minute;
    private $second;
    private $unixtime;

    private $month_rus = array('', 'январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь');
    private $month_rus_p = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
    private $weekday_rus = array('пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс');


    function __construct($date = null)
    {
        $date = ($date === null) ? time() : $date;
        $this->SetDate($date);
    }


    // преобразование даты в формат unixtime
    public static function Convert2Unixtime($date)
    {
        $date = trim($date);

        if (preg_match("/^\d{10}$/u", $date))
        {
            $result = (int)$date;
        }
        else if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/u", $date, $matches))
        {
            $result = mktime($matches[4], $matches[5], $matches[6], $matches[2], $matches[3], $matches[1]);
        }
        else if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/u", $date, $matches))
        {
            $result = mktime(0, 0, 0, $matches[2], $matches[3], $matches[1]);
        }
        else if (preg_match("/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/u", $date, $matches))
        {
            $result = mktime($matches[4], $matches[5], $matches[6], $matches[2], $matches[3], $matches[1]);
        }
        else if (preg_match("/^(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/u", $date, $matches))
        {
            $result = mktime($matches[4], $matches[5], $matches[6], $matches[2], $matches[3], $matches[1]);
        }
        else
        {
            $result = 0;
        }

        return $result;
    }


    // установка текущей даты
    public function SetDate($date)
    {
        $this->unixtime = self::Convert2Unixtime($date);

        $this->year = date("Y", $this->unixtime);
    	$this->month = date("n", $this->unixtime);
    	$this->day = date("j", $this->unixtime);
    	$this->hour = date("H", $this->unixtime);
    	$this->minute = date("i", $this->unixtime);
    	$this->second = date("s", $this->unixtime);

    	$this->weekday = date("j", $this->unixtime);
        $this->weekday = ($this->weekday == 0) ? 7 : $this->weekday;
    }


    // заполнение нулями (1 -> 01)
    private function FillZero($number)
    {
        return ($number > 0 && $number < 10) ? '0'.$number : $number;
    }


    // получение русской даты в различных форматах
    public function GetRusDate()
    {
        $args = func_get_args();
        if (count($args) == 2)
        {
            $this->SetDate($args[0]);
            $type = $args[1];
        }
        else
        {
            $type = $args[0];
        }


    	switch ($type)
    	{
            //01.01.2006
    		case 1:
    			$result = $this->FillZero($this->day).".".$this->FillZero($this->month).".".$this->year;
    		break;

            //01.01.2006 14:33
    		case 2:
    			$result = $this->FillZero($this->day).".".$this->FillZero($this->month).".".$this->year." ".$this->hour.":".$this->minute;
    		break;

            //1 января, 2006
    		case 3:
    			$result = $this->day." ".$this->month_rus_p[$this->month].", ".$this->year;
    		break;

            //1 января 2006, 14:43
    		case 4:
    			$result = $this->day." ".$this->month_rus_p[$this->month]." ".$this->year.", ".$this->hour.":".$this->minute;
    		break;

            // array(day, month, year)
    		case 5:
    			$result = array("day" => $this->day, "month" => $this->month_rus_p[$this->month], "year" => $this->year);
    		break;

    		default:
    			$result = '-';
        }

    	return $result;
    }


    // получить русский месяц
    private function GetRusMonth()
    {
    	return $this->month_rus[$this->month - 1];
    }


    // генерация календаря
    public function Calendar()
    {
    	$calendar = array();

    	for ($i = 0; $i <= 6; $i++)
    	{
    		$calendar[0][$i] = $this->weekday_rus[$i];
    	}

    	$first_day = date('w', mktime(0, 0, 0, $this->month, 1, $this->year));
    	$first_day = ($first_day == 0) ? 6 : $first_day - 1;
        $num_days = date('t', $this->unixtime);

        $i = 1;
        $j = 0;
        $k = 1;
        for ($i = 1; $i <= 6; $i++)
        {
        	for ($j = 0; $j <= 6; $j++)
        	{
        		if (($i == 1 && $first_day > $j) || ($k > $num_days))
        			$calendar[$i][$j] = '';
        		else
        		{
        			$calendar[$i][$j] = $k;
        			$k++;
        		}
        	}
        }
       	if (array_sum($calendar[5]) == 0)
       		unset($calendar[5]);
       	if (array_sum($calendar[6]) == 0)
       		unset($calendar[6]);

    	return $calendar;
    }

}



?>