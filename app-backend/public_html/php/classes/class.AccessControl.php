<?php

/*
 * Класс: AccessControl
 * Назначение: Аутентификация пользователя и проверка прав доступ
 *
 */

class AccessControl
{
    private $DB;		// идентификатор БД (пользовательская)
    private $message;	// сообщение класса

    // конструктор
    function __construct(&$DBlink)
    {
        $this->DB = &$DBlink;
    }

    // системное сообщение
    public function GetMessage()
    {
        return $this->message;
    }

    // запуск сессии
    public function CheckSession()
    {
        if (!isset($_SESSION['auth']) && isset($_COOKIE['session_hash']) && preg_match("/^[a-z0-9]{32}$/i", $_COOKIE['session_hash']))
        {
            $user_id = $this->DB->GetOne("SELECT id FROM users WHERE session_hash=? AND status=1", array($_COOKIE['session_hash']));
            if ($user_id !== false)
            {
                $_SESSION['auth'] = true;
                $_SESSION['user_id'] = $user_id;
            }
        }
    }

    // аутентификация пользователя
    public function Auth($login, $password, $remember)
    {
        $hpassword = md5(iconv('utf-8', 'windows-1251', $password));
        $user_id = $this->DB->GetOne("SELECT id FROM users WHERE login=? AND password=? AND status=1", array($login, $hpassword));

        if ($user_id != null)
        {
            $_SESSION['auth'] = true;
            $_SESSION['user_id'] = $user_id;

            if ($remember)
            {
                $level = $this->DB->GetOne("SELECT level_id FROM users WHERE id=?", array($user_id));
                if ($level != 0)
                {
                    $session_hash = md5(uniqid());
                    setcookie("session_hash", $session_hash, time() + 3600 * 24 * USER_REMEMBER_DAYS);
                    $this->DB->Execute("UPDATE users SET session_hash=? WHERE id=?", array($session_hash, $user_id));
                }
            }

            return true;
        }
        else
        {
            $this->message = "Доступ запрещен. Ваша учетная запись не найдена или она временно заблокирована.";
            return false;
        }
    }

    // проверка: авторизован или нет
    public function IsAuth()
    {
        $auth = false;
        if (isset($_SESSION['auth']) && isset($_SESSION['user_id']) && $_SESSION['auth'])
        {
            $check = $this->DB->GetOne("SELECT COUNT(*) FROM users WHERE id=? AND status=1", array($_SESSION['user_id']));
            if ($check)
            {
                $auth = true;
            }
            else
            {
                $this->message = "Доступ запрещен.";
            }
        }
        else
        {
            $this->message = "Доступ запрещен.";
        }

        return $auth;
    }

    // проверка на доступ к конкретному модулю
    public function CheckModuleAccess($module)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $ip_regexp = '/\b' . str_replace('.', '\.', $ip) . '\b/';
        $ip_filter = false;

        $module_ip_filter = $this->DB->GetOne('SELECT ip_filter FROM modules WHERE name = ?', array($module));

        if (empty($module_ip_filter))
        {
            $ip_filter = true;
        }
        else
        {
            if (preg_match($ip_regexp, $module_ip_filter))
            {
                $ip_filter = true;
            }
        }

        if ($ip_filter)
        {
            $access = $this->DB->GetOne("SELECT modules.id
	                                 FROM modules
	                                    INNER JOIN access ON access.module_id=modules.id
	                                    INNER JOIN users ON users.level_id=access.level_id
	                                 WHERE modules.name=? AND users.id=? AND users.status=1 AND users.disclaimer=1", array($module, $_SESSION['user_id']));

            $access2 = $this->DB->GetOne("SELECT id FROM users WHERE id=? AND level_id=0", array($_SESSION['user_id']));

            if ($access != false || $access2 != false) return true;
            else
            {
                $this->message = "Доступ к модулю запрещен.";
                return false;
            }
        }
        else
        {
            $this->message = "Доступ к модулю запрещен по IP.";
            return false;
        }

    }
}


?>