<?php

/*
 * Класс: Pager
 * Назначение: генерирование постраничной навигации
 *
 */

class Pager
{
    private $allItems;			// всего элементов
    private $numPageItems;		// кол-во элементов на одной странице
    private $mode;				// режим "статичный", "динамичный"
    private $printPages = 10;	// кол-во выводимых страниц

    private $pages = array();
    private $limitStart;
    private $limitEnd;
    private $currentPage;


    function __construct($all_items, $num_items, $mode)
    {
        $this->allItems = (int)$all_items;
        $this->numPageItems = (int)$num_items;
        $this->mode = $mode;

        $this->DefineCurrentPage();
        $this->GeneratePages();
        $this->Limits();
    }

    // определение текущей страницы
    private function DefineCurrentPage()
    {
        if (preg_match("/page[-=](\d+)/iu", $_SERVER['REQUEST_URI'], $matches))
        {
            $this->currentPage = $matches[1];
        }
        else
        {
            $this->currentPage = 1;
        }
    }


    // LIMIT для sql-запроса
    private function Limits()
    {
        $this->limitStart = ($this->currentPage - 1) * $this->numPageItems;
        $this->limitEnd = $this->numPageItems;
    }


    // определение ссылки для страницы
    private function PageLink($numpage)
    {
        $result = "";

        if ($this->mode == "dynamic")
        {
            if (preg_match("/page=\d+/iu", $_SERVER['REQUEST_URI']))
            {
                $result = preg_replace("/page=\d+/iu", "page=".$numpage, $_SERVER['REQUEST_URI']);
            }
            else if (preg_match("/\?/iu", $_SERVER['REQUEST_URI']))
            {
                $result = $_SERVER['REQUEST_URI']."&page=".$numpage;
            }
            else
            {
                $result = $_SERVER['REQUEST_URI']."?page=".$numpage;
            }
        }
        else if ($this->mode == "static")
        {
            if (preg_match("/page-\d+/iu", $_SERVER['REQUEST_URI']))
            {
                $result = preg_replace("/page-\d+/iu", "page-".$numpage, $_SERVER['REQUEST_URI']);
            }
            else if (preg_match("/-/iu", $_SERVER['REQUEST_URI']))
            {
                $result = preg_replace("/\.html$/iu", "_page-".$numpage.".html", $_SERVER['REQUEST_URI']);
            }
            else if (preg_match("/\.html$/iu", $_SERVER['REQUEST_URI']))
            {
                $result = preg_replace("/\.html$/iu", "_page-".$numpage.".html", $_SERVER['REQUEST_URI']);
            }
            else
            {
                $result = $_SERVER['REQUEST_URI']."page-".$numpage.".html";
            }
        }

        return $result;
    }


    // генерация постраничной навигации
    private function GeneratePages()
    {
        $all_sections = ceil($this->allItems / ($this->numPageItems * $this->printPages));
        $cur_section = ceil($this->currentPage / $this->printPages);
        $all_pages = ceil($this->allItems / $this->numPageItems);
        $start_p = (($cur_section - 1) * $this->printPages) + 1;
        $end_p = $cur_section * $this->printPages;

        $n = 0;
        for ($i = $start_p; $i <= $end_p && $i <= $all_pages; $i++)
        {
            $this->pages['pages'][$n]['num'] = $i;
            $this->pages['pages'][$n]['link'] = $this->PageLink($i);

            $n++;
        }

        if ($cur_section < $all_sections)
        {
            $this->pages['next'] = $this->PageLink($end_p + 1);
            $this->pages['next_num'] = $end_p + 1;
        }
        else
        {
            $this->pages['next'] = false;
        }

        if ($cur_section > 1)
        {
            $this->pages['prev'] = $this->PageLink($start_p - 1);
            $this->pages['prev_num'] = $start_p - 1;
        }
        else
        {
            $this->pages['prev'] = false;
        }

        $this->pages['current'] = $this->currentPage;

        $this->pages['show_pages'] = ($this->allItems > $this->numPageItems);

    }


    public function GetPages()
    {
        return $this->pages;
    }


    public function GetLimitStart()
    {
        return $this->limitStart;
    }


    public function GetLimitEnd()
    {
        return $this->limitEnd;
    }


    public function GetCurrentPage()
    {
        return $this->currentPage;
    }
}

?>