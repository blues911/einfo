<?php

class Billing {

    private $DB;

    public function __construct(&$DB)
    {
        $this->DB = &$DB;
    }

    protected function _sql_filter($filter)
    {
        // Сортировка по умолчанию
        if (empty($filter['order']))
        {
            $filter['order'] = ' ORDER BY billing.create_date DESC';
        }

        // Id платежа
        if (!empty($filter['id']))
        {
            if (is_array($filter['id']))
            {
                foreach (array_keys($filter['id']) as $k)
                {
                    $filter['id'][$k] = $this->DB->qstr($filter['id'][$k]);
                }

                $filter['where'][] = 'billing.id IN (' . implode(',', $filter['id']) . ')';
            }
            else
            {
                $filter['where'][] = 'billing.id = ' . $this->DB->qstr($filter['id']);
            }
        }

        // User_id
        if (!empty($filter['user_id']))
        {
            $filter['where'][] = 'billing.user_id = ' . (int)$filter['user_id'];
        }

        // Status
        if (isset($filter['status']) && $filter['status'] != -1)
        {
            $filter['where'][] = 'billing.status = ' . (int)$filter['status'];
        }

        // Active from
        if (!empty($filter['active_from']))
        {
            $active_from = date('Y-m-d', strtotime($filter['active_from']));
            $filter['where'][] = $this->DB->qstr($active_from) . ' <= billing.active_to';
        }

        // Active to
        if (!empty($filter['active_to']))
        {
            $active_to = date('Y-m-d', strtotime($filter['active_to']));
            $filter['where'][] = $this->DB->qstr($active_to) . ' >= billing.active_from ';
        }

        // Склеиваем where
        if (isset($filter['where']))
        {
            if (count($filter))
            {
                $filter['where'] = ' WHERE ' . implode(' AND ', $filter['where']);
            }
            else
            {
                $filter['where'] = '';
            }
        }
        else
        {
            $filter['where'] = '';
        }

        // LIMIT
        if (isset($filter['limit_offset']) && isset($filter['limit']))
        {
            $filter['limit'] = ' LIMIT ' . (int)$filter['limit_offset'] . ',' . (int)$filter['limit'];
        }
        elseif (isset($filter['limit']))
        {
            $filter['limit'] = ' LIMIT ' . (int)$filter['limit'];
        }
        else
        {
            $filter['limit'] = '';
        }

        return $filter;
    }

    public function get_list($filter = array())
    {
        $filter = $this->_sql_filter($filter);

        $bills = $this->DB->GetAll(
            'SELECT
              billing.id,
              billing.user_id,
              users.title AS user_title,
              billing.create_date,
              billing.payment_number,
              billing.payment_date,
              billing.amount,
              billing.active_from,
              billing.active_to,
              billing.file_type,
              billing.`status`
            FROM
              billing
              INNER JOIN users ON users.id = billing.user_id'
            . $filter['where']
            . $filter['order']
            . $filter['limit']
        );

        foreach (array_keys($bills) as $k)
        {
            $bills[$k]['create_date'] = date('d.m.Y H:i:s', strtotime($bills[$k]['create_date']));
            $bills[$k]['payment_date'] = date('d.m.Y', strtotime($bills[$k]['payment_date']));
            $bills[$k]['active_from'] = date('d.m.Y', strtotime($bills[$k]['active_from']));
            $bills[$k]['active_to'] = date('d.m.Y', strtotime($bills[$k]['active_to']));
        }

        return $bills;
    }

    public function get_count($filter = array())
    {
        $filter = $this->_sql_filter($filter);

        $cnt = $this->DB->GetOne(
            'SELECT COUNT(*)
            FROM
              billing
              INNER JOIN users ON users.id = billing.user_id'
            . $filter['where']
            . $filter['order']
        );

        return $cnt;
    }

    public function get_item($filter = array())
    {
        $filter = $this->_sql_filter($filter);

        $bill = $this->DB->GetRow(
            'SELECT
              billing.id,
              billing.user_id,
              users.title AS user_title,
              billing.create_date,
              billing.payment_number,
              billing.payment_date,
              billing.amount,
              billing.active_from,
              billing.active_to,
              billing.file_type,
              billing.`status`
            FROM
              billing
              INNER JOIN users ON users.id = billing.user_id'
            . $filter['where']
            . $filter['order']
        );

        if (!empty($bill))
        {
            $bill['create_date'] = date('d.m.Y H:i:s', strtotime($bill['create_date']));
            $bill['payment_date'] = date('d.m.Y', strtotime($bill['payment_date']));
            $bill['active_from'] = date('d.m.Y', strtotime($bill['active_from']));
            $bill['active_to'] = date('d.m.Y', strtotime($bill['active_to']));
        }

        return $bill;
    }

    public function get_file($id)
    {
        return $this->DB->GetRow('SELECT `file`, `file_type` FROM billing WHERE `id` = ?', array($id));
    }

    public function set_file($id, $blob, $file_type)
    {
        $this->DB->Execute('UPDATE billing SET `file` = ?, `file_type` = ? WHERE `id` = ?', array($blob, $file_type, $id));
        return true;
    }

    public function add($ins)
    {
        $query = 'INSERT INTO billing SET ';

        if (array_key_exists('create_date', $ins))
        {
            $ins['create_date'] = date('Y-m-d H:i:s', strtotime($ins['create_date']));
        }

        if (array_key_exists('payment_date', $ins))
        {
            $ins['payment_date'] = date('Y-m-d', strtotime($ins['payment_date']));
        }

        if (array_key_exists('active_from', $ins))
        {
            $ins['active_from'] = date('Y-m-d', strtotime($ins['active_from']));
        }

        if (array_key_exists('active_to', $ins))
        {
            $ins['active_to'] = date('Y-m-d', strtotime($ins['active_to']));
        }

        $insert_array = array();
        foreach ($ins as $k => $val)
        {
            $insert_array[] = '`' . $k . '` = ' . $this->DB->qstr($val);
        }

        if (!array_key_exists('create_date', $ins))
        {
            $insert_array[] = '`create_date` = NOW()';
        }

        $query .= implode(', ', $insert_array);

        $this->DB->Execute($query);

        return $this->DB->Insert_ID();
    }

    public function edit($id, $upd)
    {
        $query = 'UPDATE billing SET ';

        if (array_key_exists('payment_date', $upd))
        {
            $upd['payment_date'] = date('Y-m-d', strtotime($upd['payment_date']));
        }

        if (array_key_exists('active_from', $upd))
        {
            $upd['active_from'] = date('Y-m-d', strtotime($upd['active_from']));
        }

        if (array_key_exists('active_to', $upd))
        {
            $upd['active_to'] = date('Y-m-d', strtotime($upd['active_to']));
        }

        $update_array = array();
        foreach ($upd as $k => $val)
        {
            $update_array[] = '`' . $k . '` = ' . $this->DB->qstr($val);
        }

        $query .= implode(', ', $update_array) . ' WHERE id = ' . $this->DB->qstr($id);

        $this->DB->Execute($query);

        return true;
    }

    public function change_status($id, $status = null)
    {
        if (is_null($status))
        {
            $this->DB->Execute('UPDATE billing SET `status` = IF(`status` = 1, 0, 1) WHERE id = ?', array($id));
        }
        else
        {
            $this->DB->Execute('UPDATE billing SET `status` = ? WHERE id = ?', array($status, $id));
        }

        return $this->DB->GetOne('SELECT `status` FROM billing WHERE id = ?', array($id));
    }

    public function change_status_notify($id, $new_status = null)
    {
        if ((int)$_SESSION['user_id'] != USER_ADMIN_ID)
        {
            $bill = $this->get_item(array('id' => $id));
            $old_status = $bill['status'];

            if (is_null($new_status))
            {
                $new_status = $old_status == 1 ? 0 : 1;
            }

            if ($old_status != $new_status)
            {
                ob_start();
                // Отправляем e-mail уведомление
                $mail = new PHPMailer();
                $mail->SetFrom('admin@einfo.ru', 'Einfo');
                $mail->Subject = '[einfo.ru] Изменение статуса платежки';

                $statuses = array(0 => 'Отменен', 1 => 'Подтвержден');

                $mail->Body = 'Статус платежки изменен.' . PHP_EOL . PHP_EOL .
                    'Id: ' . $bill['id'] . PHP_EOL .
                    'Пользователь: ' . $bill['user_title'] . ' [' . $bill['id'] . ']' . PHP_EOL .
                    'Платежка: ' . $bill['payment_number'] . ' от ' . date('d.m.Y', strtotime($bill['payment_date'])) . PHP_EOL .
                    'Сумма: ' . $bill['amount'] . ' руб.' . PHP_EOL .
                    'Активность пользователя: ' . date('d.m.Y', strtotime($bill['active_from'])) . ' - ' . date('d.m.Y', strtotime($bill['active_to'])) . PHP_EOL .
                    'Новый статус платежа: ' . $statuses[$new_status] . PHP_EOL . PHP_EOL .
                    'http://' . HOST_BACKEND . '/?module=billing&action=edit-form&id=' . $bill['id'];

                $mail->AddAddress('admin@einfo.ru');

                $result = $mail->Send();
                ob_clean();

                return $result;
            }
        }

        return true;
    }

    public function get_billing_status_list($billing_status = null, $user_status = null, $date = 'today')
    {
        if (empty($date))
        {
            $date = date('Y-m-d');
        }
        elseif (is_string($date))
        {
            $date = date('Y-m-d', strtotime($date));
        }
        else
        {
            $date = date('Y-m-d', $date);
        }

        $query = 'SELECT
              users.id AS user_id,
              users.title AS user_title,
              users.email AS user_email,
              IF(COUNT(billing.id) > 0, 1, 0) AS billing_status
            FROM
              users
              LEFT JOIN billing ON users.id = billing.user_id AND billing.`status` = 1 AND ? BETWEEN billing.active_from AND billing.active_to
            WHERE
              users.id NOT IN (' . USER_ADMIN_ID . ', ' . USER_PARTNER_ID . ')';

        if (isset($user_status))
        {
            $query .= ' AND users.`status` = ' . (int)$user_status;
        }

        $query .= ' GROUP BY users.id';

        if (isset($billing_status))
        {
            $query .= ' HAVING billing_status = ' . (int)$billing_status;
        }

        $billing_status_list = $this->DB->GetAll($query, array($date));

        return $billing_status_list;
    }

    public function get_billing_last_active_to($user_id, $date = 'today')
    {
        if (empty($date))
        {
            $date = date('Y-m-d');
        }
        elseif (is_string($date))
        {
            $date = date('Y-m-d', strtotime($date));
        }
        else
        {
            $date = date('Y-m-d', $date);
        }

        $active_to = $this->DB->GetOne('SELECT active_to FROM billing WHERE user_id = ? AND `status` = 1 AND active_to < ? ORDER BY active_to DESC LIMIT 1', array($user_id, $date));

        return $active_to;
    }

} 