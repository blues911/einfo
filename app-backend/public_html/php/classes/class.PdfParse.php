<?php

class PdfParse
{
    public $DB;

    public $img_path = '/img/full/';
    public $img_path_list = '/img/list/';
    public $img_path_view = '/img/view/';

    public $max_width_list = 125;
    public $max_width_view = 200;

    public function __construct(&$DB)
    {
        $this->DB = $DB;
        $this->img_path = DATASHEET_PATH . $this->img_path;
        $this->img_path_list = DATASHEET_PATH . $this->img_path_list;
        $this->img_path_view = DATASHEET_PATH . $this->img_path_view;
    }
    

    // Список
    public function get_list()
    {
        return $this->DB->GetAll('
            SELECT
                comp_canon,
                local
            FROM
                datasheet
            WHERE
                local <> ""
                AND img = ""
        ');
    }


    // Создание миниатюр
    public function create_miniature($img_name)
    {
        $dir_name = $this->get_dir_name($img_name);

        if (!file_exists($this->img_path_view . $dir_name . '/'))
        {
            $oldmask = umask(0);
            mkdir($this->img_path_view . $dir_name, 0777);
            umask($oldmask);
        }

        image_magick('convert '.$this->img_path.$dir_name.'/'.$img_name.' -resize ' . $this->max_width_view . ' -quality 95 '.$this->img_path_view.$dir_name.'/'.$img_name);

        if (!file_exists($this->img_path_list . $dir_name . '/'))
        {
            $oldmask = umask(0);
            mkdir($this->img_path_list . $dir_name, 0777);
            umask($oldmask);
        }

        image_magick('convert '.$this->img_path.$dir_name.'/'.$img_name.' -resize ' . $this->max_width_list . ' -quality 95 '.$this->img_path_list.$dir_name.'/'.$img_name);
    }


    // Функция создания уникального имени файла
    public function generate_file_name($file_name)
    {
        $file_info = pathinfo($file_name);

        $new_name = md5(uniqid()) . '.' . $file_info['extension'];
        $dir_name = $this->get_dir_name($new_name);


        if (!file_exists($this->img_path . $dir_name . '/'))
        {
            $oldmask = umask(0);
            mkdir($this->img_path . $dir_name, 0777);
            umask($oldmask);
        }

        if (file_exists($this->img_path . $dir_name . '/' . $new_name))
        {
            return $this->generate_file_name($new_name);
        }
        else
        {
            return $new_name;
        }
    }

    // Удаление изображений
    public function delete_img($filename)
    {
        $dir = $this->get_dir_name($filename);
        @unlink($this->img_path . $dir . '/' . $filename);
        @unlink($this->img_path_list . $dir . '/' . $filename);
        @unlink($this->img_path_view . $dir . '/' . $filename);
        return true;
    }

    // Путь к папке с файлом
    public function get_dir_name($file_name)
    {
        return mb_substr($file_name, 0, 2);
    }

    // Рендер первой страницы pdf
    public function pdf_render_page($pdf_path, $img_name)
    {
        @image_magick('convert -density 100 ' . $pdf_path . '[0] ' . $this->img_path . $this->get_dir_name($img_name) . '/' . $img_name);

        return file_exists($this->img_path . $this->get_dir_name($img_name) . '/' . $img_name);
    }
}

?>