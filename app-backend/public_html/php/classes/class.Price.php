<?php

class Price {

    private $file_path;
    private $encoding;
    private $delimiter = null;
    private $is_binary = null;
    private $columns_cnt = null;

    private $file_handle = null;

    private $tags = array();

    private $DB;

    public function __construct($file_path, $encoding = 'windows-1251')
    {
        global $DB;

        $this->file_path = $file_path;
        $this->encoding = $encoding;

        $this->DB = $DB;
    }

    public function __destruct()
    {
        if (is_resource($this->file_handle))
        {
            fclose($this->file_handle);
        }
    }

    public function get_next_row()
    {
        $row_string = fgets($this->get_file_handle());

        if ($row_string !== false)
        {
            return $this->prepare_row($row_string);
        }
        else
        {
            rewind($this->get_file_handle());
            return false;
        }
    }

    public function can_read()
    {
        return is_readable($this->file_path);
    }

    private function get_file_handle()
    {
        if (is_null($this->file_handle))
        {
            $this->file_handle = fopen($this->file_path, 'rb');
        }

        return $this->file_handle;
    }

    public function get_columns_cnt()
    {
        if (is_null($this->columns_cnt))
        {
            $columns_cnt = 0;
            $row_cnt = 0;

            $handle = fopen($this->file_path, 'rb');

            while(($row = fgets($handle)) !== false && $row_cnt < 1000)
            {
                if ($this->encoding != 'utf-8')
                {
                    $row = iconv($this->encoding, 'utf-8', $row);
                }

                if ($row_array = explode($this->get_delimiter(), $row))
                {
                    $cnt = count($row_array);

                    if ($cnt > $columns_cnt)
                    {
                        $columns_cnt = $cnt;
                    }
                }

                $row_cnt++;
            }

            fclose($handle);

            $this->columns_cnt = $columns_cnt;
        }

        return $this->columns_cnt;
    }

    public function get_random_rows($cnt)
    {
        $selected_rows = array();

        if ($this->can_read())
        {
            $row_collection = array();

            $handle = fopen($this->file_path, 'rb');
            $row_cnt = 0;

            while(($row = fgets($handle, 4096)) !== false && $row_cnt < 1000)
            {
                $row_collection[] = $this->prepare_row($row);
                $row_cnt++;
            }

            fclose($handle);

            shuffle($row_collection);
            $selected_rows = array_slice($row_collection, 0, $cnt);
            unset($row_collection);
        }

        return $selected_rows;
    }

    public function set_tags($tags)
    {
        $this->tags = $tags;
    }

    public function get_first_row()
    {
        $row_array = array();

        $handle = fopen($this->file_path, 'rb');
        $row = fgets($handle);

        return $this->prepare_row($row);
    }

    public function save_to_preload($file_path)
    {
        if (is_resource($this->file_handle))
        {
            fclose($this->file_handle);
        }

        if ($this->is_binary())
        {
            // in2csv test.xlsx | csvformat -T -M "::nl::" | bbe -e 's/\n/ /g' | bbe -e 's/::nl::/\n/g' > test.csv
            exec('in2csv ' . $this->file_path . ' | csvformat -T -M "::nl::" | bbe -e "s/\n/ /g" | bbe -e "s/::nl::/\n/g" > ' . $file_path);
        }
        else
        {
            // Проверяем кодировку файла
            $this->check_encoding();

            if ($this->encoding != 'utf-8' && !getenv('COMSPEC'))
            {
                exec('iconv -c -f ' . $this->encoding . ' -t utf-8 ' . $this->file_path . ' > ' . $file_path);
            }
            else
            {
                copy($this->file_path, $file_path);
            }
        }

        unlink($this->file_path);

        $this->file_path = $file_path;
        $this->encoding = 'utf-8';
        $this->file_handle = null;

        return true;
    }

    public function save_tags($user_id)
    {
        $upload_time = date('Y-m-d H:i:s');
        $filename = basename($this->file_path);
        $tags = serialize($this->tags);
        $this->DB->Execute('INSERT INTO users_prices SET user_id = ?, upload_time = ?, filename = ?, tags = ?', array($user_id, $upload_time, $filename, $tags));
    }

    public function save($file_path, $id, &$debug = false)
    {
        $file_path = preg_replace('/\.[^\.]*$/', '', $file_path) . '.csv';

        $this->reset_iterator();

        $nl = "\r\n";

        try
        {
            $tag_line = implode("\t", $this->tags) . $nl;

            $file = fopen($file_path, 'w');
            fwrite($file, $tag_line);

            $debug = array('ok' => 0, 'fail' => 0);
            while (($row = $this->get_next_row()) !== false)
            {
                $skip = false;

                foreach ($row as $__value)
                {
                    // Ставим флаг пропуска если встретилось недопустимое значение
                    if (preg_match('/_unnamed/sui', $__value)) $skip = true;
                }

                // Пропускаем строку
                if ($skip) continue;

                if (fwrite($file, implode("\t", $row) . $nl))
                {
                    $debug['ok']++;
                }
                else
                {
                    $debug['fail']++;
                }
            }

            fclose($file);

            $this->DB->Execute('DELETE FROM users_prices WHERE id = ?', array($id));

            fclose($this->file_handle);
            unlink($this->file_path);

            $this->file_path = $file_path;
            $this->encoding = 'utf-8';
            $this->file_handle = null;

            return true;
        }
        catch(Exception $exception)
        {
            return false;
        }
    }

    private function is_binary()
    {
        if (is_null($this->is_binary))
        {
            $binary = false;
            $char_cnt = 0;

            $handle = fopen($this->file_path, 'rb');
            while(($c = fgetc($handle)) !== false && $char_cnt < 100000)
            {
                if ($c != chr(9) && $c != chr(10) && $c != chr(13) && ord($c) < 0x20)
                {
                    $binary = true;
                    break;
                }

                $char_cnt++;
            }
            fclose($handle);

            $this->is_binary = $binary;
        }

        return $this->is_binary;
    }

    private function get_delimiter()
    {
        if (is_null($this->delimiter))
        {
            $tab = 0;
            $tz = 0;
            $row_cnt = 0;

            $handle = fopen($this->file_path, 'rb');
            while(($row = fgets($handle)) !== false && $row_cnt < 1000)
            {
                if ($this->encoding !== 'utf-8')
                {
                    $row = iconv($this->encoding, 'utf-8', $row);
                }

                $tab += mb_substr_count($row, "\t", 'utf-8');
                $tz += mb_substr_count($row, ';', 'utf-8');

                $row_cnt++;
            }

            fclose($handle);

            if ($tab >= $tz)
            {
                $this->delimiter = "\t";
            }
            else
            {
                $this->delimiter = ';';
            }
        }

        return $this->delimiter;
    }

    private function prepare_row($row_string)
    {
        if ($this->encoding != 'utf-8')
        {
            $row_string = iconv($this->encoding, 'utf-8', $row_string);
        }

        $row_array = str_getcsv(trim($row_string, " \n\r\0\x0B"), $this->get_delimiter());

        return array_pad($row_array, $this->get_columns_cnt(), '');
    }

    private function reset_iterator()
    {
        if (is_resource($this->file_handle))
        {
            rewind($this->file_handle);
        }
    }

    // Проверяем кодировку файла
    private function check_encoding()
    {
        $in = fopen($this->file_path, 'r');
        while (!feof($in))
        {
            $line = fgets($in, 4096);
            $encoding = mb_detect_encoding($line, mb_list_encodings(), true);

            if (strtolower($encoding) == 'utf-8')
            {
                $this->encoding = 'utf-8';
                break;
            }
        }
        fclose($in);
    }
} 