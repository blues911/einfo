<?php

require_once(PATH_FRONTEND . '/php/libs/sphinxapi.php');

class CatalogWorkerCompare {

    /**
     * Id воркера.
     * @var string
     */
    private $worker_id;

    /**
     * Указатель на ADODBNewConnection.
     * @var ADODBNewConnection
     */
    private $DB;

    /**
     * Поисковый движок Sphinx
     * @var SphinxClient
     */
    private $sphinx;

    /**
     * Признак отладки.
     * @var bool
     */
    private $debug;

    /**
     * Период обновления статуса воркера в БД.
     * @var int     секунды
     */
    private $timeout_status_update = 5;

    /**
     * Таймаут переподключения к Sphinx в случае разрыва связи.
     * @var int     секунды
     */
    private $timeout_sphinx_reconnect = 5;

    /**
     * Таймаут итерации в режиме ожидания работы.
     * @var int     секунды
     */
    private $timeout_sleep = 1;

    /**
     * Таймштамп последнего обновления статуса в БД.
     * @var int
     */
    private $_last_time_update;

    /**
     * Курс доллара.
     * @var float
     */
    private $usd;

    /**
     * Курс евро.
     * @var float
     */
    private $eur;


    /**
     * Конструктор класса.
     * @param ADODBNewConnection $DB    Указатель на подключение к БД.
     * @param bool $debug               Признак отладки.
     */
    public function __construct(&$DB, $debug = false)
    {
        $this->DB = $DB;
        $this->debug = $debug;

        // Инициализируем воркер
        $time = time();
        while(true)
        {
            $this->worker_id = md5(uniqid(rand(0,1000)));
            if ($DB->Execute('INSERT INTO catalog_workers SET worker_id = ?, task = NULL, last_time = ?', array($this->worker_id, $time)))
            {
                $this->_last_time_update = $time;
                break;
            }
        }

        $this->_init_settings();
        $this->_update_cur_rates();

        // Подключаемся к Sphinx
        $this->_sphinx_connect();

        $this->log('Worker start...');
    }


    /**
     * Инициализация настроек из констант.
     * Если константы не объявлены, то оставляем значения по-умолчанию.
     */
    private function _init_settings()
    {
        if (defined('TIMEOUT_STATUS_UPDATE'))
        {
            $this->timeout_status_update = TIMEOUT_STATUS_UPDATE;
        }

        if (defined('TIMEOUT_SPHINX_RECONNECT'))
        {
            $this->timeout_sphinx_reconnect = TIMEOUT_SPHINX_RECONNECT;
        }

        if (defined('TIMEOUT_SLEEP'))
        {
            $this->timeout_sleep = TIMEOUT_SLEEP;
        }
    }


    /**
     * Ожидаем успешного подключения к Sphinx.
     * @return bool
     */
    private function _sphinx_connect()
    {
        while(true)
        {
            $this->log('Connect to sphinx -> ', false);

            // Соединение со Sphinx Engine
            $this->sphinx = new SphinxClient();
            $this->sphinx->SetServer(SPHINX_HOST, SPHINX_PORT);
            $this->sphinx->SetMatchMode(SPH_MATCH_EXTENDED);
            $this->sphinx->SetLimits(0, 3000, 3000, 3000);

            if($this->sphinx->Status() !== false)
            {
                $this->log('OK');
                return true;
            }
            else
            {
                $this->log('FAIL');
                sleep($this->timeout_sphinx_reconnect);
            }
        }
    }


    /**
     * Проверяем статус подключения к Sphinx.
     * @return bool
     */
    private function _sphinx_status()
    {
        if ($this->sphinx->Status() === false)
        {
            return $this->_sphinx_connect();
        }
        else
        {
            return true;
        }
    }


    /**
     * Обновляем статус воркера по таймауту.
     * Плюс обновляем курсы валют.
     */
    public function update_status()
    {
        $time = time();
        if (($time - $this->_last_time_update) >= $this->timeout_status_update)
        {
            $this->DB->Execute('
                INSERT INTO
                  catalog_workers (worker_id, last_time)
                VALUES
                  (?, ?)
                ON DUPLICATE KEY UPDATE
                  last_time = ?
            ', array(
                $this->worker_id,
                $time,
                $time
            ));
            $this->_last_time_update = $time;

            $this->_update_cur_rates();
        }
    }

    /**
     * Обновляем курсы валют.
     * @return bool
     */
    private function _update_cur_rates()
    {
        $this->usd = $this->DB->GetOne('SELECT `value` FROM `settings` WHERE `var` = ?', array('cur_rates:usd'));
        $this->eur = $this->DB->GetOne('SELECT `value` FROM `settings` WHERE `var` = ?', array('cur_rates:eur'));
        return true;
    }

    /**
     * Проверяем наличие работы.
     * @return bool
     */
    public function get_work()
    {
        $work = $this->DB->GetOne('SELECT SQL_NO_CACHE task FROM catalog_workers WHERE worker_id = ?', array($this->worker_id));

        if (empty($work))
        {
            return false;
        }

        return $work;
    }


    /**
     * Выполняем работу.
     * @param $serialized_pool
     */
    public function do_work($serialized_pool)
    {
        $this->log('---> Start work...');

        $work = unserialize($serialized_pool);

        foreach($work['words'] as $word)
        {
            $this->log($word, false);

            $this->update_status();

            if ($this->_is_word_exist($word))
            {
                if ($sphinx_matches = $this->_sphinx_query($word))
                {
                    $this->_compare($word, $sphinx_matches, $work['regexp_id'], $work['category_id']);
                }
            }

            $this->log(PHP_EOL, false);
        }

        $this->_work_complete();
    }


    public function get_task()
    {

        $task = $this->DB->GetOne('SELECT SQL_NO_CACHE task FROM catalog_workers WHERE worker_id = ?', array($this->worker_id));

        if (!empty($task))
        {
            return $task;
        }
        else
        {
            return false;
        }
    }


    public function do_task($task)
    {
        /*
         *  Task array:
         * [0] -> regexp_id
         * [1] -> category_id
         * [2] -> regexp string
         * [3] -> стартовый индекс
         * [4] -> конечный индекс
         */

        $this->log('---> Start task...');

        $task = unserialize($task);

        $regexp = new SimpleRegexp($task[2]);

        for ($i = $task[3]; $i <= $task[4]; $i++)
        {
            $this->update_status();
            $word = $regexp->get_word($i);

            $this->log($word, false);

            if ($this->_is_word_exist($word))
            {
                if ($sphinx_matches = $this->_sphinx_query($word))
                {
                    $this->_compare($word, $sphinx_matches, $task[0], $task[1]);
                }
            }

            $this->log(PHP_EOL, false);
        }

        $this->_work_complete();
    }


    /**
     * Спим в ожидании работы.
     * @return int
     */
    public function sleep()
    {
        $this->update_status();
        sleep($this->timeout_sleep);
        return true;
    }


    /**
     * Проверяем через LIKE наличие вхождений слова в БД.
     * @param string $word     Слово.
     * @return bool
     */
    private function _is_word_exist($word)
    {
        $word_exist = $this->DB->GetOne('SELECT SQL_NO_CACHE id FROM base_main WHERE comp_title LIKE ? LIMIT 0, 1', array($word . '%'));

        if (!empty($word_exist))
        {
            return true;
        }

        return false;
    }


    /**
     * Формируем атомы для запроса в Sphinx.
     * @param string $word      Слово.
     * @return string
     */
    private function _get_atoms($word)
    {
        return implode(' ', get_request_atoms($word));
    }


    /**
     * Делаем запрос в Sphinx.
     * @param string $word      Слово.
     * @return array            Массив вхождений Sphinx.
     */
    private function _sphinx_query($word)
    {
        while(true)
        {
            if ($this->_sphinx_status())
            {
                $sphinx_query = '"^' . $this->_get_atoms($word) . '"';

                if ($sphinx_result = $this->sphinx->Query($sphinx_query, SPHINX_INDEX))
                {
                    if (!empty($sphinx_result['matches']))
                    {
                        return $sphinx_result['matches'];
                    }
                    else
                    {
                        return array();
                    }
                }
                else
                {
                    if ($this->sphinx->IsConnectError)
                    {
                        continue;
                    }
                    else
                    {
                        return array();
                    }
                }
            }
        }
    }


    /**
     * Находим все вхождения по заданым условиям.
     * Обновляем таблицу канонических компонентов.
     * @param string $word          Слово.
     * @param array $sphinx_matches Массив вхождений Sphinx.
     * @param int $regexp_id        Id канонического шаблона.
     * @param int $category_id      Id категории.
     */
    private function _compare($word, $sphinx_matches, $regexp_id, $category_id)
    {
        $comp_ids = array_keys($sphinx_matches);
        $this->log(' (' . count($comp_ids) . ')', false);

        $list_rs = $this->DB->Execute('
            SELECT SQL_NO_CACHE
              main.id AS id,
              main.prices AS prices
            FROM
              base_main AS main
              INNER JOIN users ON main.user_id = users.id
            WHERE
              users.status = 1
              AND main.id IN (' . implode(',', $comp_ids) . ')
              AND main.atoms LIKE ?
        ', array($this->_get_atoms($word) . '%'));

        $list = array('id' => array(), 'price' => array());
        while(!$list_rs->EOF)
        {
            $list['id'][] = $list_rs->fields('id');

            $prices = json_decode($list_rs->fields('prices'), true);

            if (!empty($prices))
            {
                $price = array_shift($prices);
    
                if ($price['c'] == 'rur')
                {
                    $list['price'][] = $price['p'];
                }
                else if ($price['c'] == 'usd')
                {
                    $list['price'][] = $price['p'] * $this->usd;
                }
                else
                {
                    $list['price'][] = $price['p'] * $this->eur;
                }
            }

            $list_rs->MoveNext();
        }

        if (!empty($list['id']))
        {
            $canon = array(
                'offer_cnt' => count($list['id']),
                'price_median' => median($list['price'])
            );

            $this->log(' -> ' . $canon['offer_cnt'], false);

            if ($canon['offer_cnt'] >= CATALOG_MIN_SUPPLY)
            {
                $this->DB->Execute('
                    INSERT LOW_PRIORITY IGNORE INTO
                        catalog_comp_canon
                    SET
                        regexp_id = ?,
                        category_id = ?,
                        comp_canon = ?,
                        offer_cnt = ?,
                        price_median = ?
                ', array(
                    $regexp_id,
                    $category_id,
                    $word,
                    $canon['offer_cnt'],
                    $canon['price_median']
                ));
            }
        }
    }


    /**
     * Обновляем статус воркера, указывая, что работа завершена.
     * @return bool
     */
    private function _work_complete()
    {
        $time = time();
        $this->DB->Execute('
            INSERT INTO
              catalog_workers (worker_id, task, last_time)
            VALUES
              (?, NULL, ?)
            ON DUPLICATE KEY UPDATE
              task = NULL,
              last_time = ?
        ', array(
            $this->worker_id,
            $time,
            $time
        ));

        $this->_last_time_update = $time;

        $this->log('---> Work complete...');
        return true;
    }


    /**
     * Вывод отладочной информации.
     * @param string $text      Текст сообщения.
     * @param bool $php_eol     Перевод на следующую строку.
     */
    public function log($text, $php_eol = true)
    {
        if ($this->debug)
        {
            if ($php_eol)
            {
                $text .= PHP_EOL;
            }

            echo $text;
        }
    }

}