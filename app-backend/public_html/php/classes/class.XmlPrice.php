<?php

/*
 * Класс: XmlPrice
 * Назначение: Работа с прайс-листами в формате xml (спецификации einfo и efind)
 *
 */


class XmlPrice
{
    private $_errorLog = array();
    private $_convertXmlTemplate = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
                            <price>
                                <header>
                                    <hash>#hash#</hash>
                                    <status>all</status>
                                    <uploadtype>replace</uploadtype>
                                </header>

                                <data>
                                    #items#
                                </data>
                            </price>';

    private $_einfo_xml_tags = array(
        'title'         => array('required' => true, 'correct' => 0, 'skipped' => 0),
        'description'   => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'manufacture'   => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'mfgdate'       => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'docs'          => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'comp_url'      => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'stock'         => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'stock_remote'  => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'delivery'      => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'price'         => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'prices'        => array('required' => false, 'correct' => 0, 'skipped' => 0)
    );

    private $_einfo_json_tags = array(
        'title'         => array('required' => true, 'correct' => 0, 'skipped' => 0),
        'description'   => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'manufacture'   => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'mfgdate'       => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'docs'          => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'comp_url'      => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'stock'         => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'stock_remote'  => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'delivery'      => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'price_opt'     => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'price_opt_cur' => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'price_roz'     => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'price_roz_cur' => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'prices'        => array('required' => false, 'correct' => 0, 'skipped' => 0)
    );

    private $_efind_tags = array(
        'part'          => array('required' => true, 'correct' => 0, 'skipped' => 0),
        'mfg'           => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'note'          => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'pdf'           => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'cur'           => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'p1'            => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'p2'            => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'p3'            => array('required' => false, 'correct' => 0, 'skipped' => 0),
        'stock'         => array('required' => false, 'correct' => 0, 'skipped' => 0),
    );

    // конструктор
    function __construct()
    {
        libxml_use_internal_errors(true);
    }

    // загрузка xml в виде строки
    public function loadXmlString($xml_string)
    {
        $xml = simplexml_load_string($xml_string);
        if ($xml === false)
        {
            $errors = libxml_get_errors();
            foreach ($errors as $error)
            {
                $this->_writeLog($error);
            }

            libxml_clear_errors();

            return false;
        }
        else
        {
            return $xml;
        }
    }

    // Функция добавляющая блоки CDATA
    public function xmlCdataPrepare($xml)
    {
        return preg_replace(
            '/<([^<>]+)(\s*[^<>]*)>(?!\s*<(?!!\[CDATA\[)[^<>]+>)\s*(?:<!\[CDATA\[)?(.*?)(?:\]\]>)?\s*<\/\1>/isu',
            '<$1$2><![CDATA[$3]]></$1>',
            $xml
        );
    }

    public function prepareXmlFileFromJson($file_path, $user, $need_tags_info = false)
    {
        $cnt = array('total' => 0, 'ok' => 0, 'fail' => 0);
        $tmp_file_path = (PATH_PRICES . '/0_tmp/' . md5(uniqid()) . '.tmp');
        $tags = $this->_einfo_json_tags;

        $tmp_handle = fopen($tmp_file_path, 'w');
        $file_handle = fopen($file_path, 'r');

        fwrite(
            $tmp_handle,
            '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
                <price>
                    <header>
                        <action><![CDATA[sale]]></action>
                        <status><![CDATA[all]]></status>
                        <uploadtype><![CDATA[replace]]></uploadtype>
                    </header>
                    <data>' . PHP_EOL
        );

        while ($cnt['total'] < $user['price_limit'] && !feof($file_handle))
        {
            $data = json_decode(fgets($file_handle), true);

            if ($need_tags_info)
                $tags = $this->_check_tag($data, $tags, 'array');

            $cnt['total']++;

            if (!empty($data))
            {
                $row = '<item>' . PHP_EOL;

                $row .= '<prices>' . PHP_EOL;

                // Новый вариант цен
                if (isset($data['prices']))
                {
                    foreach ($data['prices'] as $p)
                    {
                        $row .= '<price n="' . $p['n'] . '" c="' . $p['c'] . '">' . $p['p'] . '</price>' . PHP_EOL;
                    }

                    unset($data['prices']);
                }
                // Старый вариант цен
                else 
                {
                    $data['price_roz'] = empty($data['price_roz']) ? 0 : $data['price_roz'];
                    $data['price_roz_cur'] = empty($data['price_roz_cur']) ? 'rur' : $data['price_roz_cur'];

                    $row .= '<price n="1" c="' . $data['price_roz_cur'] . '">' . $data['price_roz'] . '</price>' . PHP_EOL;

                    unset($data['price_opt'], $data['price_opt_cur'], $data['price_roz'], $data['price_roz_cur']);
                }

                $row .= '</prices>' . PHP_EOL;

                foreach ($data as $tag => $value)
                {
                    $row .= '<' . $tag . '><![CDATA[' . $value . ']]></'. $tag . '>' . PHP_EOL;
                }

                $row .= '</item>' . PHP_EOL;

                fwrite($tmp_handle, $row);

                $cnt['ok']++;
            }
            else
            {
                $cnt['fail']++;
            }
        }

        fwrite($tmp_handle, '</data></price>');

        fclose($tmp_handle);
        fclose($file_handle);

        // Переименовываем временный файл в $file_path
        copy($tmp_file_path, $file_path);

        unlink($tmp_file_path);

        return array(
            'cnt' => $cnt,
            'tags' => $tags
        );
    }

    public function prepareXmlFileFromXml($file_path, $type, $user, $target_file_path = null, $need_tags_info = false, &$hash = null)
    {
        $cnt = array('total' => 0, 'ok' => 0, 'fail' => 0);

        $tmp_file_path = (PATH_PRICES . '/0_tmp/' . md5(uniqid()) . '.tmp');

        // --- Подготовка файла ---

        // 1. Определяем кодировку

        $handle = fopen($file_path, 'r');
        $head = fread($handle, 1000);
        fclose($handle);

        if (preg_match('/encoding.?=.?windows-1251/is', $head))
        {
            // 2. Конвертируем файл при необходимости

            exec('iconv -c -f windows-1251 -t utf-8 ' . $file_path . ' > ' . $tmp_file_path);
            rename($tmp_file_path, $file_path);
        }

        // 3. Открываем временный файл для записи
        $handle = fopen($tmp_file_path, 'w');

        // 4. Инициализируем XML String Streamer
        $stream_header = new Prewk\XmlStringStreamer\Stream\File($file_path, 8192);
        $stream_item = new Prewk\XmlStringStreamer\Stream\File($file_path, 8192);

        // --- EINFO ---
        if ($type == 'einfo_xml')
        {
            $tags = $this->_einfo_xml_tags;

            // 1. Подготавливаем блок <header>

            $parser_header = new Prewk\XmlStringStreamer\Parser\UniqueNode(array('uniqueNode' => 'header'));
            $iterator_header = new Prewk\XmlStringStreamer($parser_header, $stream_header);
            $header_node = $iterator_header->getNode();

            $header_node = clearstr($this->xmlCdataPrepare($header_node));

            // 2. Проверяем валидность блока <header>

            if ($header_xml_node = simplexml_load_string($header_node))
            {
                $hash = isset($header_xml_node->hash) ? $header_xml_node->hash : false;

                // 3. Записываем начало xml во временный файл (блок <header>, открываем тег <body>)
                fwrite($handle, '<?xml version="1.0" encoding="utf-8" standalone="yes"?>' . PHP_EOL
                    . '<price>' . $header_node . PHP_EOL . '<data>' . PHP_EOL);

                // 4. Подготавливаем каждый блок <item> и записываем во временный файл

                $parser_item = new Prewk\XmlStringStreamer\Parser\UniqueNode(array('uniqueNode' => 'item'));
                $iterator_item = new Prewk\XmlStringStreamer($parser_item, $stream_item);

                $chunk = array('string' => '', 'cnt' => 0);
                
                while ($cnt['total'] < $user['price_limit'] && $item = $iterator_item->getNode())
                {
                    $item = clearstr($this->xmlCdataPrepare($item));

                    if ($xml_node = simplexml_load_string($item))
                    {
                        $str = '<item>' . PHP_EOL;
                        $str .= '<title><![CDATA[' . $xml_node->title . ']]></title>' . PHP_EOL;
                        $str .= '<category><![CDATA[' . $xml_node->category . ']]></category>' . PHP_EOL;
                        $str .= '<description><![CDATA[' . $xml_node->description . ']]></description>' . PHP_EOL;
                        $str .= '<manufacture><![CDATA[' . $xml_node->manufacture . ']]></manufacture>' . PHP_EOL;
                        $str .= '<prices>' . PHP_EOL;

                        // Новый вариант цен
                        if (isset($xml_node->prices))
                        {
                            foreach($xml_node->prices->price as $p)
                            {
                                $attr = $p->attributes();
                                $str .= '<price n="' . (string)$attr['n'] . '" c="' . (string)$attr['c'] . '">' . (string)$p . '</price>' . PHP_EOL;
                            }
                        }
                        // Старый вариант цен
                        else
                        {
                            $price = $xml_node->xpath('price[@type="roz"]');

                            // Если не найдена розничная цена, берём оптовую
                            if (!isset($price[0]))
                            {
                                $price = $xml_node->xpath('price[@type="opt"]');
                            }

                            // Если цена определена
                            if (!empty($price))
                            {
                                $str .= '<price n="1" c="' . $price[0]['currency'] . '">' . $price[0] . '</price>' . PHP_EOL;
                            }
                        }

                        $str .= '</prices>' . PHP_EOL;
                        $str .= '<mfgdate>' . $xml_node->mfgdate . '</mfgdate>' . PHP_EOL;
                        $str .= '<docs><![CDATA[' . $xml_node->docs . ']]></docs>' . PHP_EOL;
                        $str .= '<comp_url><![CDATA[' . $xml_node->comp_url . ']]></comp_url>' . PHP_EOL;
                        $str .= '<stock>' . preg_replace('/\D/', '', $xml_node->stock) . '</stock>' . PHP_EOL;
                        $str .= '<stock_remote>' . preg_replace('/\D/', '', $xml_node->stock_remote) . '</stock_remote>' . PHP_EOL;
                        $str .= '<delivery>' . $xml_node->delivery . '</delivery>' . PHP_EOL;
                        $str .= '</item>' . PHP_EOL;

                        $chunk['string'] .= $str;
                        $chunk['cnt']++;
                        $cnt['ok']++;

                        if ($chunk['cnt'] >= 10000)
                        {
                            fwrite($handle, $chunk['string']);
                            $chunk['string'] = '';
                            $chunk['cnt'] = 0;
                        }

                        if ($need_tags_info)
                            $tags = $this->_check_tag($xml_node, $tags, 'xml');
                    }
                    else
                    {
                        $cnt['fail']++;

                        if ($need_tags_info)
                            $tags = $this->_check_tag($xml_node, $tags, 'xml');
                    }

                    $cnt['total']++;
                }

                if ($chunk['cnt'] > 0)
                {
                    fwrite($handle, $chunk['string']);
                    $chunk['string'] = '';
                    $chunk['cnt'] = 0;
                }

                // 5. Записываем конец xml во временный файл (закрываем все теги)
                fwrite($handle, '</data></price>' . PHP_EOL);
            }
        }
        // --- EFIND ---
        else
        {
            $tags = $this->_efind_tags;

            // 1. Подготавливаем блок <header> в формате EINFO
            $user_hash = md5(iconv('utf-8', 'windows-1251', $user['login'].$user['password']));
            $header_node = '<header>' . PHP_EOL . '<hash>' . $user_hash . '</hash>' . PHP_EOL . '<status>all</status>'
                . PHP_EOL . '<uploadtype>replace</uploadtype>' . PHP_EOL . '</header>' . PHP_EOL;

            // 2. Записываем начало xml во временный файл (блок <header>, открываем тег <body>)
            fwrite($handle, '<?xml version="1.0" encoding="utf-8" standalone="yes"?>' . PHP_EOL
                . '<price>' . PHP_EOL . $header_node . '<data>' . PHP_EOL);

            // 3. Подготавливаем каждый блок <line> в формате EINFO (<item>) и записываем во временный файл

            $parser_line = new Prewk\XmlStringStreamer\Parser\UniqueNode(array('uniqueNode' => 'line'));
            $iterator_line = new Prewk\XmlStringStreamer($parser_line, $stream_item);

            $chunk = array('string' => '', 'cnt' => 0);
            while ($cnt['total'] < $user['price_limit'] && $line = $iterator_line->getNode())
            {
                $line = clearstr($this->xmlCdataPrepare($line));
                if ($line = simplexml_load_string($line))
                {
                    if (!empty($line->part))
                    {
                        if (!empty($line->cur))
                        {
                            $currency = self::parseCurrency($line->cur);
                            if (!$currency)
                            {
                                $currency = 'usd';
                            }
                        }
                        else
                        {
                            $currency = 'usd';
                        }
                        // формирование XML
                        $item = '<item>' . PHP_EOL;
                        $item .= '<title><![CDATA[' . mb_substr($line->part, 0, 255, 'utf-8') . ']]></title>' . PHP_EOL;

                        if (!empty($line->note))
                        {
                            $item .= '<description><![CDATA[' . mb_substr($line->note, 0, 255, 'utf-8') . ']]></description>' . PHP_EOL;
                        }

                        if (!empty($line->mfg))
                        {
                            $item .= '<manufacture><![CDATA[' . mb_substr($line->mfg, 0, 255, 'utf-8') . ']]></manufacture>' . PHP_EOL;
                        }

                        if (!empty($line->p1))
                        {
                            $item .= '<prices><price n="1" c="' . $currency . '">' . (float)$line->p1 . '</price></prices>' . PHP_EOL;
                        }

                        if (!empty($line->pdf))
                        {
                            $item .= '<docs><![CDATA[' . $line->pdf . ']]></docs>' . PHP_EOL;
                        }

                        $line->stock = mb_strtolower($line->stock, 'utf-8');
                        $line->delivery = mb_strtolower($line->delivery, 'utf-8');

                        if (isset($line->instock))
                        {
                            if ($line->instock == 1)
                            {
                                if (isset($line->stock))
                                {
                                    $item .= '<stock>' . (int)self::parseStock($line->stock) . '</stock>' . PHP_EOL;
                                }
                                else
                                {
                                    $item .= '<stock>-1</stock>' . PHP_EOL;
                                }
                            }
                            else
                            {
                                $item .= '<stock>0</stock>' . PHP_EOL;
                                $item .= '<delivery>' . (int)self::parseDelivery($line->stock) . '</delivery>' . PHP_EOL;
                            }
                        }
                        else
                        {
                            if (isset($line->stock) && in_array($line->stock, array('да', 'на складе')))
                            {
                                $item .= '<stock>-1</stock>' . PHP_EOL;
                            }
                            elseif (isset($line->stock) && is_numeric((string)$line->stock))
                            {
                                $item .= '<stock>' . (int)$line->stock . '</stock>' . PHP_EOL;
                            }
                            elseif (isset($line->stock) && in_array($line->stock, array('0', 'нет')))
                            {
                                $item .= '<stock>0</stock>' . PHP_EOL;
                            }
                            else
                            {
                                $item .= '<stock>0</stock>' . PHP_EOL;
                            }
                        }

                        $item .= '<stock_remote>0</stock_remote>' . PHP_EOL;
                        $item .= '</item>' . PHP_EOL;

                        if (simplexml_load_string($item))
                        {
                            $chunk['string'] .= $item;
                            $chunk['cnt']++;
                            $cnt['ok']++;

                            if ($chunk['cnt'] >= 10000)
                            {
                                fwrite($handle, $chunk['string']);
                                $chunk['string'] = '';
                                $chunk['cnt'] = 0;
                            }
                        }
                        else
                        {
                            $cnt['fail']++;
                        }
                    }
                    else
                    {
                        $cnt['fail']++;
                    }

                    if ($need_tags_info)
                        $tags = $this->_check_tag($line, $tags, 'xml');
                }
                else
                {
                    $cnt['fail']++;

                    if ($need_tags_info)
                        $tags = $this->_check_tag($line, $tags, 'xml');
                }

                $cnt['total']++;
            }

            if ($chunk['cnt'] > 0)
            {
                fwrite($handle, $chunk['string']);
                $chunk['string'] = '';
                $chunk['cnt'] = 0;
            }

            // 4. Записываем конец xml во временный файл (закрываем все теги)
            fwrite($handle, '</data>' . PHP_EOL . '</price>');
        }

        // --- Сохранение файла ---

        fclose($handle);

        // 1. Переименовываем временный файл в $target_file_path, либо в $file_path (если $target_file_path - пустое значение)
        if (!empty($target_file_path))
        {
            copy($tmp_file_path, $target_file_path);
            unlink($file_path);
        }
        else
        {
            copy($tmp_file_path, $file_path);
        }

        unlink($tmp_file_path);

        return array(
            'cnt' => $cnt,
            'tags' => $tags
        );
    }

    // Функция подготовки XML. Добавляет блоки CDATA, чтобы экранировать
    // символы и неоднозначные конструкции.
    public function prepareXml($source, $type)
    {
        // Определяем кодировку
        if (preg_match('/encoding.?=.?windows-1251/is', $source))
        {
            $source = iconv('windows-1251', 'utf-8', $source);
        }

        $xml_parts = array();

        // EINFO
        if ($type == 'einfo_xml')
        {
            // Разделяем блоки <header> и <data>
            $_xml_parts = preg_split('/<\/header[\s]*?>.*?<data[^>]*?>/isu', $source, 2);
            if (!empty($_xml_parts) && count($_xml_parts) == 2)
            {
                // Подготавливаем блок <header>
                preg_match('/<header[^>]*?>.*<\/header[^>]*?>/isu', $_xml_parts[0] . '</header>', $_matches);
                $_header = $this->xmlCdataPrepare($_matches[0]);

                // Проверяем валидность блока <header>
                if(simplexml_load_string($_header))
                {
                    // Добавляем блок <header> в массив
                    $xml_parts[] = '<price>' . $_header;

                    // Разделяем блок <data> на отдельные блоки <item>
                    preg_match('/<data[^>]*?>.*<\/data[^>]*?>/isu', '<data>' . $_xml_parts[1], $_matches);
                    $_body = $_matches[0];

                    unset($_matches);
                    unset($_xml_parts);

                    $_xml_parts = preg_split('/<\/item[\s]*?>.*?<item[^>]*?>/isu', $_body);
                    unset($_body);

                    if (!empty($_xml_parts) && $cnt = count($_xml_parts))
                    {
                        // Подготавливаем каждый блок <item>
                        $i = 1;
                        foreach ($_xml_parts as $k => $_part)
                        {
                            // Добавляем теги <item> и </item>
                            if ($k > 0) { $_part = '<item>' . $_part; }
                            if ($k < ($cnt - 1)) { $_part .= '</item>'; }

                            unset($_xml_parts[$k]);

                            preg_match('/<item[^>]*?>.*<\/item[^>]*?>/isu', $_part, $_matches);
                            if ($_item = $_matches[0])
                            {
                                $_item = $this->xmlCdataPrepare($_item);
                                if (simplexml_load_string($_item))
                                {
                                    $xml_parts[] = $_item;
                                    $i++;
                                }
                            }
                        }
                        // добавляем первому элементу открывающий тег <data>
                        $xml_parts[1] = '<data>' . $xml_parts[1];
                        // добавляем последнему элементу закрывающие теги </data></price>
                        $xml_parts[$i-1] .= '</data></price>';

                        unset($_xml_parts);
                    }
                }
            }
        }
        // EFIND
        elseif ($type == 'efind')
        {
            // Разделяем на отдельные блоки <line>
            $_xml_parts = preg_split('/<\/line[\s]*?>.*?<line[^>]*?>/isu', $source);
            if (!empty($_xml_parts) && $cnt = count($_xml_parts))
            {
                // Подготавливаем каждый блок <line>
                $i = 0;
                foreach ($_xml_parts as $k => $_part)
                {
                    // Добавляем теги <line> и </line>
                    if ($k > 0) { $_part = '<line>' . $_part; }
                    if ($k < ($cnt - 1)) { $_part .= '</line>'; }

                    unset($_xml_parts[$k]);

                    preg_match('/<line[^>]*?>.*<\/line[^>]*?>/isu', $_part, $_matches);
                    if ($_line = $_matches[0])
                    {
                       $_line = $this->xmlCdataPrepare($_line);
                        if (simplexml_load_string($_line))
                        {
                            $xml_parts[] = $_line;
                            $i++;
                        }
                    }
                }
                // добавляем первому элементу открывающий тег <data>
                $xml_parts[0] = '<data>' . $xml_parts[0];
                // добавляем последнему элементу закрывающий тег </data>
                $xml_parts[$i-1] .= '</data>';

                unset($_xml_parts);
            }
        }

        // Собираем массив подготовленных блоков в строку.
        // (implode не используется для экономии памяти)
        $return = '';
        foreach($xml_parts as $k => $v)
        {
            unset($xml_parts[$k]);
            $return .= $v;
        }
        unset($xml_parts);

        // Очищаем лог
        //libxml_clear_errors();

        // Возвращаем результат
        return $return;
    }

    // загрузка xml в виде строки
    public function loadXmlFile($xml_file)
    {
        $xml = simplexml_load_file($xml_file);
        if ($xml === false)
        {
            $errors = libxml_get_errors();
            foreach ($errors as $error)
            {
                $this->_writeLog($error);
            }

            libxml_clear_errors();

            return false;
        }
        else
        {
            return $xml;
        }
    }

    // Проверяем хеш
    public function compareHash($xml, $hash, $type)
    {
        $msg = 'Проверка пользовательского хеша: ';
        if ($type == 'efind')
        {
            $this->_writeMessageLog($msg . 'НЕ ТРЕБУЕТСЯ' . PHP_EOL);
            return true;
        }
        else
        {
            if ($_xml_hash = $xml->header->hash)
            {
                if ($_xml_hash == $hash)
                {
                    $this->_writeMessageLog($msg . '<span style="color: green;">СОВПАДАЕТ</span>' . PHP_EOL);
                    return true;
                }
                else
                {
                    $this->_writeMessageLog($msg . '<span style="color: red;">НЕ СОВПАДАЕТ</span>' . PHP_EOL);
                    return false;
                }
            }
        }
    }

    // кол-во стро к в файле формата einfo
    public function countRows($xml, $type)
    {
        if (isset($xml->data->item) && $type == 'einfo_xml')
        {
            return count($xml->data->item);
        }
        elseif (isset($xml->line) && $type == 'efind')
        {
            return count($xml->line);
        }
        else
        {
            return 0;
        }
    }

    // валидация обязательных полей (тегов) в xml
    public function validateXml($xml, $type)
    {
        // Выбираем формат xml
        // EINFO
        if ($type == 'einfo_xml')
        {
            $tags = $this->_einfo_tags;

            // Проверяем наличие корневого тега data в xml
            if (!isset($xml->data))
            {
                $this->_writeMessageLog('Корневой тег &lt;data&gt; не обнаружен.');
                return false;
            }
            // Проверяем наличие минимум одного тега item в xml
            if (!isset($xml->data->item))
            {
                $this->_writeMessageLog('Компонентов с тегом &lt;item&gt; не обнаружено.');
                return false;
            }
            // Формируем список компонентов для проверки
            $items = $xml->data->item;
        }
        // EFIND
        elseif ($type == 'efind')
        {
            $tags = $this->_efind_tags;

            // Проверяем наличие минимум одного тега line в xml
            if (!isset($xml->line))
            {
                $this->_writeMessageLog('Компонентов с тегом &lt;line&gt; не обнаружено.');
                return false;
            }
            // Формируем список компонентов для проверки
            $items = $xml->line;
        }
        else
        {
            $this->_writeMessageLog('Неизвестный формат прайс-листа');
            return false;
        }

        // Проверяем наличие необходимых тегов внутри комнонента
        foreach ($items as $item)
        {
            foreach ($tags as $tag => $info)
            {
                if (isset($item->$tag))
                {
                    $tags[$tag]['correct']++;
                }
                else
                {
                    $tags[$tag]['skipped']++;
                }
            }
        }

        // Формируем сообщение лога
        $_msg = 'Результаты проверки тегов: ' . PHP_EOL;
        $return = true;
        $_num = 1;

        foreach ($tags as $tag => $info)
        {
            if ($info['required'] && $info['skipped'] > 0)
            {
                $return  = false;
            }

            $_msg .= $_num . '. <span';
            $color = '';
            if ($info['required'] && $info['skipped'] > 0)
            {
                $color = 'red';
            }
            elseif ($info['skipped'] == 0)
            {
                $color = 'green';
            }
            if ($color)
            {
                $_msg .= ' style="color: ' . $color . ';"';
            }
            $_msg .= '>&lt;' . $tag . '&gt; - корректно ' . $info['correct'] . ', ' .
                    'отсутствует ' . $info['skipped'] . ';</span> ' . PHP_EOL;
            $_num++;
        }
        $this->_writeMessageLog($_msg);

        // Возвращаем результат проверки
        return $return;
    }


    // конвертация файла из формата efind в формат einfo
    public function convertEfind2Einfo($xml_string, $user)
    {
        $xml = simplexml_load_string($xml_string);
        if ($xml !== false)
        {
            if (isset($xml->line))
            {
                $xml_items = '';
                foreach ($xml->line as $line)
                {
                    if (!empty($line->part))
                    {
                        if (!empty($line->cur))
                        {
                            $currency = self::parseCurrency($line->cur);
                            if (!$currency)
                            {
                                $currency = 'usd';
                            }
                        }
                        else
                        {
                            $currency = 'usd';
                        }

                        // формирование XML
                        $xml_items .= "\n<item>\n";
                        $xml_items .= "<title><![CDATA[".mb_substr($line->part, 0, 255)."]]></title>\n";

                        if (!empty($line->note)) $xml_items .= "<description><![CDATA[".mb_substr($line->note, 0, 255,'utf-8')."]]></description>\n";
                        if (!empty($line->mfg)) $xml_items .= "<manufacture><![CDATA[".mb_substr($line->mfg, 0, 255)."]]></manufacture>\n";
                        if (!empty($line->p1)) $xml_items .= "<prices><price n=\"1\" c=\"".$currency."\">".(float)$line->p1."</price></prices>\n";
                        if (!empty($line->pdf)) $xml_items .= "<docs><![CDATA[".$line->pdf."]]></docs>\n";

                        $line->stock = mb_strtolower($line->stock, 'utf-8');
                        $line->delivery = mb_strtolower($line->delivery, 'utf-8');

                        if (isset($line->instock))
                        {
                            if ($line->instock == 1)
                            {
                                if (isset($line->stock))
                                {
                                    $xml_items .= "<stock>".(int)self::parseStock($line->stock)."</stock>\n";
                                }
                                else
                                {
                                    $xml_items .= "<stock>-1</stock>\n";
                                }
                            }
                            else
                            {
                                $xml_items .= "<stock>0</stock>\n";
                                $xml_items .= "<delivery>".(int)self::parseDelivery($line->stock)."</delivery>\n";
                            }
                        }
                        else
                        {
                            if (isset($line->stock) && in_array($line->stock, array('да', 'на складе')))
                            {
                                $xml_items .= "<stock>-1</stock>\n";
                            }
                            elseif (isset($line->stock) && is_numeric((string)$line->stock))
                            {
                                $xml_items .= "<stock>".(int)$line->stock."</stock>\n";
                            }
                            elseif (isset($line->stock) && in_array($line->stock, array('0', 'нет')))
                            {
                                $xml_items .= "<stock>0</stock>\n";
                            }
                            else
                            {
                                $xml_items .= "<stock>0</stock>\n";
                            }
                        }

                        $xml_items .= "<stock_remote>0</stock_remote>\n";
                        $xml_items .= "</item>\n";
                    }
                }

                $xml_result = str_replace('#hash#', md5(iconv('utf-8', 'windows-1251', $user['login'].$user['password'])), $this->_convertXmlTemplate);
                $xml_result = str_replace('#items#', $xml_items, $xml_result);

                return $xml_result;
           }
           else
           {
               $this->_writeMessageLog('Не удалось найти ни одного тэга <line>');
               return false;
           }
        }
        else
        {
            $errors = libxml_get_errors();
            foreach ($errors as $error)
            {
                $this->_writeLog($error);
            } 
            
            libxml_clear_errors();

            return false;
        }
    }

    // парсинг валюты
    static function parseCurrency($val)
    {
        if (preg_match("/usd|doll|дол|\\$/iu", $val))
        {
            $result = 'usd';
        }
        elseif (preg_match("/eur|евр|евро|€/iu", $val))
        {
            $result = 'eur';
        }
        elseif (preg_match("/р|руб|rur|rub/iu", $val))
        {
            $result = 'rur';
        }
        else
        {
            $result = false;
        }

        return $result;
    }

    // парсинг времени поставок
    static function parseDelivery($val)
    {
        if (preg_match("/\d+/u", $val, $matches))
        {
            $result = $matches[0];
        }
        else
        {
            $result = false;
        }

        return $result;
    }

    // парсинг кол-ва на складе
    static function parseStock($val)
    {
        if (preg_match("/\d+/u", $val, $matches))
        {
            $result = $matches[0];
        }
        else
        {
            $result = false;
        }

        return $result;
    }

    // Проверка какие данные указаны в товаре, какие нет
    private function _check_tag($component_data, $tags, $type)
    {
        if ($type == 'array')
        {
            foreach (array_keys($tags) as $tag)
            {
                if (isset($component_data[$tag]))
                    $tags[$tag]['correct']++;
                else
                    $tags[$tag]['skipped']++;
            }

            if (isset($component_data['prices']))
            {
                unset($tags['price_opt']);
                unset($tags['price_opt_cur']);
                unset($tags['price_roz']);
                unset($tags['price_roz_cur']);
            }
            else
                unset($tags['prices']);
        }

        if ($type == 'xml')
        {
            foreach (array_keys($tags) as $tag)
            {
                if (isset($component_data->$tag))
                    $tags[$tag]['correct']++;
                else
                    $tags[$tag]['skipped']++;
            }

            if (isset($component_data->prices))
                unset($tags['price']);
            else
                unset($tags['prices']);
        }


        return $tags;
    }

    // запись ошибок в лог
    private function _writeLog($error)
    {
        $log = 'Line: '.$error->line.', Column: '.$error->column."\n";

        switch ($error->level)
        {
            case LIBXML_ERR_WARNING:
                $log .= 'Warning '.$error->code.': ';
                break;
            case LIBXML_ERR_ERROR:
                $log .= 'Error '.$error->code.': ';
                break;
            case LIBXML_ERR_FATAL:
                $log .= 'Fatal Error '. $error->code.': ';
                break;
        }

        $log .= trim($error->message);

        if ($error->file)
        {
            $log .= "\nFile: ".$error->file;
        }

        $this->_errorLog[] = $log."\n";
    }

    // запись текстового сообщения в лог
    private function _writeMessageLog($message)
    {
        $this->_errorLog[] = $message;
    }

    // очиста лога
    public function clearLog()
    {
        $this->_errorLog = array();
    }

    // получения лога
    public function getLog()
    {
        return $this->_errorLog;
    }

    // получения лога в текстовом виде
    public function displayLog()
    {
        return implode("---------------------\n", $this->getLog());
    }

}

?>
