<?php

/*
 * Класс: SocialPoll
 * Назначение: Управление формой соц. опроса
 *
 */


class SocialPolls
{
    private $DB;

    // конструктор
    function __construct(&$DBlink)
    {
        $this->DB = &$DBlink;
    }

    // добавление соц. опроса
    function Add($data)
    {
        $ins['status'] = $data['status'];
        $ins['views'] = $data['views'];
        $ins['title'] = $data['title'];
        $ins['questions'] = $data['questions'];

        $this->DB->AutoExecute("social_polls", $ins, "INSERT");
    }

    // редактирование соц. опроса
    function Edit($data)
    {
        $upd['status'] = $data['status'];
        $upd['views_min'] = $data['views_min'];
        $upd['views_max'] = $data['views_max'];
        $upd['title'] = $data['title'];
        $upd['questions'] = $data['questions'];

        $this->DB->AutoExecute("social_polls", $upd, "UPDATE", "id=" . $data['id']);
    }

    // удаление соц. опроса
    function Delete($id)
    {
        $this->DB->Execute("DELETE FROM social_polls WHERE id=?", array($id));
    }

    // сменить статус соц. опроса
    function ChangeStatus($id)
    {
        $item = $this->DB->GetRow("SELECT * FROM social_polls WHERE id=?", array($id));
        $status = ($item['status'] == 1) ? 0 : 1;

        $this->DB->Execute('UPDATE social_polls SET status=0');
        $this->DB->Execute('UPDATE social_polls SET status=? WHERE id=?', array($status, $id));

        return $status;
    }
}