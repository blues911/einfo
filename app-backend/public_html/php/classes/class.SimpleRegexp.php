<?php

/**
 * Класс реализующий разбор и разворачивание упрощенных регулярных выражений,
 * поддерживающих одиночные символы, перечесление символов (напр., [-A-Zабв23])
 * и квантификаторы (напр., {2,5} или {3}).
 */
class SimpleRegexp extends ArrayObject {

    public $regexp;
    public $matches;
    public $dict;

    private $_counter;
    private $_parts;

    static private $re_groupmatch = '\(.*?\|.*?\)';
    static private $re_chars = '[\[].*?[\]]';
    static private $re_quantifiers = '[\{].*?[\}]';
    static private $re_symbols = '[^\[\]\{\}]{1}';

    /**
     * Массив символов, разделяющих шаблон на отдельные регулярные выражения.
     * @var array
     */
    static private $template_separator = array(';');

    /**
     * Класс реализующий разбор и разворачивание упрощенных регулярных выражений,
     * поддерживающих одиночные символы, перечесление символов (напр., [-A-Zабв23])
     * и квантификаторы (напр., {2,5} или {3}).
     *
     * @param string $regexp    Упрощенное регулярное выражение (напр., AM[0-9]{2}L)
     */
    public function __construct($regexp)
    {
        $this->regexp = $regexp;
        $this->matches = self::explode($this->regexp);
        $this->_parts = self::_get_parts($this->matches);
        $this->dict = self::_get_dict($this->_parts);
        $this->_counter = array();
    }

    public function offsetExists($index) {
        return $this->get_word($index) !== false;
    }

    public function offsetSet($index, $newval) {}
    public function offsetUnset($index) {}

    public function offsetGet($index) {
        return $this->get_word($index);
    }

    public function count() {
        $count = (float)self::count_words($this->regexp);
        return $count;
    }

    /**
     * Разбивает шаблон на регулярные выражения, используя в качестве разделителя
     * один из символов self::$templateSeparator. Возвращает массив объектов
     * класса SimpleRegexp созданных на основе полученых регулярок.
     *
     * @param string $template  Шаблон с упрощенными регулярными выражениями (напр., 'AM[0-9]{2}L; TMT[SD][0-9]{3})')
     * @return \SimpleRegexp    Массив объектов класса SimpleRegexp
     */
    public static function parse_template($template)
    {
        $collection = array();
        foreach (self::_parse_template($template) as $regexp) {
            if ($regexp != ' ')
            {
                $regexp = trim($regexp);
            }
            $collection[] = new SimpleRegexp($regexp);
        }

        return $collection;
    }

    private static function _parse_template($template)
    {
        $separator = '/(' . implode('|', self::$template_separator) . ')/isu';
        return preg_split($separator, $template);
    }

    /**
     * Возвращает кол-во возможных слов, которые можно составить
     * на основе упр. регулярного выражения $regexp_or_template.
     *
     * @param string $regexp_or_template    Строка с упр. регулярным выражением
     * @return int                          См. описание функции
     */
    public static function count_words($regexp_or_template)
    {
        $counter = 0;
        foreach(self::_parse_template($regexp_or_template) as $regexp)
        {
            $_count = 0;
            $_parts = self::_get_parts(self::explode($regexp));
            foreach (self::_count_dict($_parts) as $v)
            {
                if ($_count == 0)
                {
                    $_count += $v;
                }
                else
                {
                    $_count *= $v;
                }
            }
            $counter += $_count;
        }

        return $counter;
    }

    public function get_count_words()
    {
        return self::count_words($this->regexp);
    }

    /**
     * Конвертирует обычный целочисленный индекс в массив счетчиков словарей.
     * @param type $index
     * @return type
     */
    public function index2counter($index)
    {
        $size = array();
        foreach($this->dict as $dict)
        {
            $size[] = count($dict);
        }

        // size[3, 6, 2]
        $counter = array();
        $rest = $index;
        $_last = count($size) - 1;
        foreach (array_keys($size) as $k)
        {
            if ($k < $_last)
            {
                $offset = array_slice($size, $k + 1);

                $divide = 1;
                foreach ($offset as $_offset)
                {
                    $divide *= $_offset;
                }

                $counter[$k + 1] = floor($rest / $divide) + 1;
                $rest = $rest % $divide;
            }
            else
            {
                $counter[$k + 1] = $rest + 1;
            }
        }

        return $counter;
    }


    /**
     * Возвращает массив содержащий в порядке вхождения следующие виды подстрок:
     * перечесление символов (напр., [-a-zA-Z123]),
     * квантификатор (напр., {1,3} или {2}),
     * груповой переключатель (напр., (regex1|regex2)),
     * отдельный символ (напр., B).
     *
     * @param string $regexp    Строка содержащая регулярное выражение (напр., AM[0-9]{2}L)
     * @return array            См. описание функции
     */
    static public function explode($regexp)
    {
        $matches = array();
        $_regexp = '/(' . implode(')|(', array(self::$re_groupmatch, self::$re_chars, self::$re_quantifiers, self::$re_symbols)) . ')/isu';
        preg_match_all($_regexp, $regexp, $matches);
        return $matches[0];
    }

    /**
     * Возвращает все возможные последовательности символов из массива $sequence
     * состоящие от $min до $max символов.
     *
     * @param array $sequence   Массив содержащий словарь символов
     * @param int $min          Мин. кол-во символов в последовательности
     * @param int $max          Макс. кол-во символов в последовательности (если не указано, то равно $min)
     * @return array            Массив всех последовательностей ("слов") удовлетворяющих условию
     */
    static public function make_dict($sequence, $min = 1, $max = false)
    {
        if ($max === false) { $max = $min; }
        if ($min > $max) { list($min, $max) = array($max, $min); }

        if (is_array($sequence) && $n = count($sequence))
        {
            $dict = array();
            for ($i = $min; $i <= $max; $i++) {
                if($i > 0)
                {
                    $counter = array_fill(1, $i, 1);
                    while(true) {
                        $word = '';
                        // Проверяем последнее ли это слово
                        $break_flag = false;
                        foreach (array_keys($counter) as $k) {
                            if ($counter[$k] > $n)
                            {
                                if (!empty($counter[$k+1]))
                                {
                                    $counter[$k+1]++;
                                    $counter[$k] = 1;
                                }
                                else
                                {
                                    $break_flag = true;
                                }
                            }
                        }
                        if($break_flag)
                        {
                            break;
                        }
                        // Собираем слово из символов
                        foreach ($counter as $k => $v) {
                            $word .= $sequence[$v-1];
                        }
                        $dict[] = $word;

                        $counter[1]++;
                    }
                }
                else
                {
                    $dict[] = '';
                }
            }

            return $dict;
        }
        else
        {
            return array();
        }
    }

    /**
     * Возвращает двумерный массив содержащий тип и содержимое подстрок из
     * упрощенного регулярного выражения.
     * Например: array(array('type'=>'quant','value'=>'{2,5}'),
     * array('type'=>'chars','value'=>'[a-z]')).
     *
     * @param array $matches    Массив подстрок
     * @return type             См. описание функции
     */
    static private function _get_parts($matches)
    {
        $parts = array();

        foreach ($matches as $key => $value) {
            if (preg_match('/\(.*?\|.*?\)/isu', $value))
            {
                $type = 'groupmatch';
            }
            else if (preg_match('/\[.*?\]/isu', $value))
            {
                $type = 'chars';
            }
            else if (preg_match('/\{.*?\}/isu', $value))
            {
                $type = 'quant';
            }
            else
            {
                $type = 'symbol';
            }
            $parts[] = array('type' => $type, 'value' => $value);
        }

        return $parts;
    }

    /**
     * Возвращает массив содержащий словарь для каждой части регулярного
     * выражения (в виде вложенного массива).
     * Например: array(array('1','2','3'),array('AA','AB','BA','BB')).
     *
     * @param array $parts  Двумерный массив, возвращаемый self::_getParts()
     * @return array        См. описание функции
     */
    static private function _get_dict($parts)
    {
        $dict = array();

        foreach (array_keys($parts) as $k) {
            $type = $parts[$k]['type'];
            $value = $parts[$k]['value'];

            if (!empty($parts[$k+1]))
            {
                $next_type = $parts[$k+1]['type'];
                $next_value = $parts[$k+1]['value'];
            }
            else
            {
                $next_type = 'empty';
                $next_value = '';
            }

            $_dict = array();
            if ($type == 'symbol')
            {
                $_dict = array($value);
            }
            else if ($type == 'chars')
            {
                $_dict = SimpleRegexp_Chars::make_dict($value);
            }
            else if ($type == 'groupmatch')
            {
                $_dict = SimpleRegexp_GroupMatch::make_dict($value);
            }
            else
            {
                continue;
            }

            $min = $max = 1;
            if ($next_type == 'quant')
            {
                SimpleRegexp_Quantifiers::explode($next_value, $min, $max);
            }

            //$dict[] = SimpleRegexp::make_dict($_dict, $min, $max);
            $dict[] = new SimpleRegexp_Dict($_dict, $min, $max);
        }

        return $dict;
    }

    /**
     * Возвращает массив, который содержит количество возможных вариантов
     * для каждой части упр. регулярного выражения.
     *
     * @param array $parts  Массив, возвращаемый функцией self::_get_parts()
     * @return array        См. описание функции
     */
    static private function _count_dict($parts)
    {
        $dict_counters = array();

        foreach (array_keys($parts) as $k)
        {
            $type = $parts[$k]['type'];
            $value = $parts[$k]['value'];

            if (!empty($parts[$k+1]))
            {
                $next_type = $parts[$k+1]['type'];
                $next_value = $parts[$k+1]['value'];
            }
            else
            {
                $next_type = 'empty';
                $next_value = '';
            }

            if ($type == 'symbol')
            {
                $num_of_chars = 1;
            }
            else if ($type == 'chars')
            {
                $num_of_chars = count(SimpleRegexp_Chars::make_dict($value));
            }
            else if ($type == 'groupmatch')
            {
                $num_of_chars = SimpleRegexp_GroupMatch::count_dict($value);
            }
            else
            {
                continue;
            }

            $min = $max = 1;
            if ($next_type == 'quant')
            {
                SimpleRegexp_Quantifiers::explode($next_value, $min, $max);
            }

            $dict_counters[] = self::_count_variants($num_of_chars, $min, $max);
        }

        return $dict_counters;
    }

    /**
     * Считаем количество возможных вариантов строки из словаря
     * размером $num_of_chars символов в слове, состоящем от
     * $min до $max символов.
     *
     * @param int $num_of_chars     Количество символов в словаре
     * @param int $min              Минимальный размер слова
     * @param int $max              Максимальный размер слова
     * @return int                  Количество возможных слов
     */
    static private function _count_variants($num_of_chars, $min, $max)
    {
        if ($max < $min)
        {
            list($min, $max) = array($max, $min);
        }

        $count = 0;
        for ($i = $min; $i <= $max; $i++)
        {
            if ($i == 0)
            {
                $count++;
            }
            else
            {
                $count += pow($num_of_chars, $i);
            }
        }
        return $count;
    }

    /**
     * Возвращает слово соответстующее счетчкикам в counter. Увеличивает
     * счетчик на еденицу.
     *
     * @param array $dict       Массив со словарями
     * @param array $counter    Массив со счетчиками
     * @return mixed            Слово или false
     */
    static public function _explain_one($dict, &$counter = null)
    {
        // Считаем количество частей в регулярке
        $dict_cnt = count($dict);
        if ($dict_cnt < 1)
        {
            return false;
        }
        // Считаем кол-во слов в последнем словаре
        $last_dict_cnt = count($dict[$dict_cnt-1]);

        // Создаем счетчик при необходимости
        if (empty($counter))
        {
            $counter = array_fill(1, $dict_cnt, 1);
        }

        // Пересчитываем счетчики
        foreach (array_keys($counter) as $k)
        {
            if ($counter[$k] > count($dict[$k-1]))
            {
                if (!empty($counter[$k+1]))
                {
                    $counter[$k] = 1;
                    $counter[$k+1]++;
                }
            }
        }
        // Проверяем последнее ли это слово
        if ($counter[$dict_cnt] > $last_dict_cnt)
        {
            return false;
        }

        // Формируем "слово"
        $_word = '';
        foreach ($counter as $k => $v) {
            $_word .= $dict[$k-1][$v-1];
        }

        // Увеличиваем счетчик
        $counter[1]++;

        // Возвращаем "слово"
        return $_word;

    }

    /**
     * Возвращает массив всех возможных "слов" удовлетворяющих упрощенному
     * регулярному выражению (напр., AM[0-9]{2}L).
     *
     * @param mixed $regexp_or_matches  Текст регулярки, либо массив, возвращаемый функцией self::explode()
     * @return array                    См. описание функции
     */
    static public function explain_to_array($regexp_or_matches)
    {
        if (is_array($regexp_or_matches))
        {
            $matches = $regexp_or_matches;
        }
        else
        {
            $matches = self::explode($regexp_or_matches);
        }

        $dict = self::_get_dict(self::_get_parts($matches));

        // Собираем возможные варианты "слов"
        $words = array();

        $dict_cnt = count($dict);
        if ($dict_cnt < 1)
        {
            return array();
        }
        $last_dict_cnt = count($dict[$dict_cnt-1]);

        $counter = array_fill(1, $dict_cnt, 1);
        while(($_word = self::_explain_one($dict, $counter)) !== false) {
            $words[] = $_word;
        }

        sort($words, SORT_LOCALE_STRING);
        return $words;
    }

    public function get_word($index)
    {
        $counter = $this->index2counter($index);
        $word = self::_explain_one($this->dict, $counter);
        return $word;
    }

    /**
     * Возвращает массив всех возможных "слов" удовлетворяющих упрощенному
     * регулярному выражению (напр., AM[0-9]{2}L).
     *
     * @return array    См. описание функции
     */
    public function get_words()
    {
        return self::explain_to_array($this->matches);
    }

    public function get_words_count()
    {
        return self::count_words($this->regexp);
    }

    /**
     * Итератор, который возвращает по одному слову из возможных, и false в
     * конце или при ошибке.
     *
     * @return mixed    Слово или false
     */
    public function explain()
    {
        $word = self::_explain_one($this->dict, $this->_counter);

        // Если возвращает false то сбрасываем счетчик
        if ($word === false)
        {
            $this->_counter = array();
        }

        return $word;
    }

}

/**
 * Реализация словаря с доступом как к массиву.
 */
class SimpleRegexp_Dict extends ArrayObject
{
    private $alphabet;
    private $min;
    private $max;

    public function __construct($alphabet, $min = 1, $max = null)
    {
        $this->alphabet = $alphabet;

        if (is_null($max))
        {
            $max = $min;
        }

        if ($min > $max)
        {
            list($min, $max) = array($max, $min);
        }

        $this->min = $min;
        $this->max = $max;
    }

    /* Реализация доступа к объекту как к массиву */
    public function offsetSet($offset, $value) {}
    public function offsetUnset($offset) {}

    public function offsetExists($offset) {
        return ($this->get_word($offset + 1) !== false);
    }

    public function offsetGet($offset) {
        return $this->get_word($offset + 1);
    }

    public function count()
    {
        $num_of_chars = count($this->alphabet);

        $count = 0;
        for ($i = $this->min; $i <= $this->max; $i++)
        {
            if ($i == 0)
            {
                $count++;
            }
            else
            {
                $count += pow($num_of_chars, $i);
            }
        }
        return $count;
    }
    /* -------------- */


    private function _get_coord($index)
    {
        $index += -1;

        $alphabet_size = count($this->alphabet);

        $return = array('chars' => 0, 'index' => 0);

        if ($index >= 0)
        {
            $return['chars'] = $this->min;
            $return['index'] = $index;

            $_max = 0;
            while(true)
            {
                $return['index'] = $return['index'] - $_max;
                $_max = pow($alphabet_size, $return['chars']);

                if ($return['index'] < $_max)
                {
                    break;
                }

                $return['chars']++;
            }
        }

        return $return;
    }

    private function _get_word($coord)
    {
        $index = $coord['index'];
        $alphabet_size = count($this->alphabet);

        if ($coord['chars'] < 1)
        {
            return '';
        }

        if ($coord['chars'] > $this->max)
        {
            return false;
        }

        $counter = array_fill(0, $coord['chars'], 0);

        $i = $coord['chars'] - 1;
        while($index >= $alphabet_size)
        {
            $rest = $index % $alphabet_size;
            $index = floor($index / $alphabet_size);

            $counter[$i] = $rest;
            $i--;
        }
        $counter[$i] = $index;

        $word = '';
        foreach($counter as $char_index)
        {
            $word .= $this->alphabet[$char_index];
        }

        return $word;
    }

    public function get_word($index)
    {
        $coord = $this->_get_coord($index);
        $word = $this->_get_word($coord);

        return $word;
    }

}

/**
 * Класс реализующий объявление набора символов (напр., [-А-Яabs2-5]).
 */
class SimpleRegexp_Chars
{

    public $string;
    public $matches;
    public $dict;

    static private $re = '([^-\[\]]-[^-\[\]])|([^\]\[](?!-))';
    static private $dicts = array(
        array(
            'a','b','c','d','e','f','g','h','i','j','k','l','m',
            'n','o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        ),
        array(
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л',
            'м','н','о','п','р','с','т','у','ф','х','ц','ч','ш',
            'щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л',
            'М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш',
            'Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ),
        array(
            '0','1','2','3','4','5','6','7','8','9'
        )
    );

    /**
     * Класс реализующий объявление набора символов (напр., [-А-Яabs2-5]).
     *
     * @param string $string    Строка вида [-A-Z23], либо -A-Z23
     */
    public function __construct($string)
    {
        $this->string = $string;
        $this->matches = self::explode($this->string);
        $this->dict = self::make_dict($this->matches);
    }

    /**
     * Возвращает массив с подстроками из $string следующих видов:
     * диапазон символов (напр., A-D, a-z, a-Z, 0-9),
     * одиночный символ (напр., b, 1, -).
     *
     * @param string $string    Строка для разбора (напр., [-A-Z23] или -A-Z23)
     * @return array            См. описание функции
     */
    static public function explode($string)
    {
        $matches = array();
        $_regexp = '/' . self::$re . '/isu';
        preg_match_all($_regexp, $string, $matches);
        return $matches[0];
    }

    /**
     * Возвращает массив символов начиная с символа $leftChar до символа
     * $rightChar. Оба символа должны находится в одном из словарей self::$dicts.
     *
     * @param char $leftChar    Левый символ диапазона
     * @param char $rightChar   Правый символ диапазона
     * @return array            Массив с последовательностью символов (либо пустой массив)
     */
    static public function get_chars_sequense($leftChar, $rightChar)
    {
        foreach (self::$dicts as $_dict) {
            $left_key = array_search($leftChar, $_dict);
            $right_key = array_search($rightChar, $_dict);
            if ($left_key !== false && $right_key !== false)
            {
                if ($left_key > $right_key)
                {
                    list($left_key, $right_key) = array($right_key, $left_key);
                }
                $slice = array_slice($_dict, $left_key, 1 + $right_key - $left_key);
                return $slice;
            }
        }
        return array();
    }

    /**
     * Возвращает массив всех символов удовлетворяющих объявлению вида [-A-Z23].
     *
     * @param mixed $string_or_matches  Строка (напр., [-A-Z23]) или массив с подстроками (возвращаемый функц. self::explode())
     * @return array                    См. описание функции
     */
    static public function make_dict($string_or_matches)
    {
        $dict = array();
        if (!is_array($string_or_matches))
        {
            $string_or_matches = self::explode($string_or_matches);
        }
        foreach ($string_or_matches as $value) {
            $_matches = array();
            if (preg_match('/(.)-(.)/isu', $value, $_matches))
            {
                $sequence = self::get_chars_sequense($_matches[1], $_matches[2]);
                $dict = array_merge($dict, $sequence);
            }
            else
            {
                $dict[] = $value;
            }
        }
        return $dict;
    }

}

/**
 * Класс реализующий квантификаторы вида {2,5} или {3}
 */
class SimpleRegexp_Quantifiers
{

    public $string;
    public $min;
    public $max;

    /**
     * Класс реализующий квантификаторы вида {2,5} или {3}
     *
     * @param string $string    Квантификатор вида {2,5} или {3}
     */
    public function __construct($string) {
        $this->string = $string;
        self::explode($this->string, $this->min, $this->max);
    }

    /**
     * Помещает минимальное и максимальное значения квантификатора (напр., {2,5} или {3})
     * в переменные &$min и &$max, соответственно.
     *
     * @param string $string    Квантификатор (напр., {2,5} или {3})
     * @param int $min          Минимальное значение квантификатора
     * @param int $max          Максимальное значение квантификатора
     * @return boolean          true - при удаче, false - в случае ошибки
     */
    static public function explode($string, &$min = 0, &$max = 0)
    {
        $_matches = array();
        if (preg_match('/([\d]+)(?:[\D]+)([\d]+)/isu', $string, $_matches))
        {
            $min = $_matches[1];
            $max = $_matches[2];
            if ($min > $max)
            {
                list($min, $max) = array($max, $min);
            }
            return true;
        }
        else if (preg_match('/(?:\{)([\d]+)(?:\})/isu', $string, $_matches))
        {
            $min = $max = $_matches[1];
            return true;
        }
        else
        {
            $min = $max = 1;
            return false;
        }
    }

}

/*
 * Класс реализующий груповой переключатель вида (tmp1|tmp2)
 */
class SimpleRegexp_GroupMatch {

    public $string;
    public $dict;

    public function __construct($string)
    {
        $this->string = $string;
        $this->dict = self::make_dict($this->string);
    }

    static public function convert_to_template($string)
    {
        return str_replace('|', ';', trim($string, '()'));
    }

    /**
     * Возвращает все возможные "слова" составленные из регулярных выражений
     * в груповом переключателе. Пример: (ВЫРАЖЕНИЕ_1|ВЫРАЖЕНИЕ_2)
     *
     * @param int $string
     * @return type
     */
    static public function make_dict($string)
    {
        $_dict = array();
        $string = self::convert_to_template($string);

        foreach (SimpleRegexp::parse_template($string) as $regexp)
        {
            $_dict = array_merge($_dict, $regexp->get_words());
        }

        return $_dict;
    }

    /**
     * Возвращает количество возможных вариантов, возвращаемых
     * групповым переключателем (усл1|усл2).
     *
     * @param string $string    Строка с групповым переключателем (reg1|reg2)
     * @return int              См. описание функции
     */
    static public function count_dict($string)
    {
        $_count = 0;
        $string = self::convert_to_template($string);

        foreach (SimpleRegexp::parse_template($string) as $regexp)
        {
            $_count += $regexp->get_words_count();
        }

        return $_count;
    }

}