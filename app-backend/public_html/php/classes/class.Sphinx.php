<?php

/**
 *  Класс для работы со Sphinx
 */

class Sphinx
{
    protected $DB;

    // объект Sphinx
    protected $sphinx_pdo = null;

    // Конструктор
    function __construct(&$DB)
    {
        $this->DB = $DB;

        try
        {
            $this->sphinx_pdo = new PDO(
                'mysql:host=' . SPHINXQL_HOST . ';port=' . SPHINXQL_PORT . ';charset=utf8',
                '',
                '',
                array(
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false
                )
            );
        }
        catch (PDOException $e)
        {
            $this->sphinx_pdo = null;
        }
    }

    // ---------------------------------------------------

    // Добавление в RT индекс предложений пользователя
    public function add_user_rt_index($user_id)
    {
        if (!$this->sphinx_pdo) return false;

        $user_id = (int)$user_id;

        // Получаем выборку компонентов от пользователя
        $rs = $this->DB->Execute(
            'SELECT main.id, main.atoms2 AS atoms ' .
            'FROM base_main AS main ' .
            'LEFT JOIN users ON users.id = main.user_id ' .
            'WHERE users.status = 1 AND main.user_id = ' . $user_id
        );

        $cnt = 0;

        if (is_object($rs))
        {
            // Для каждого обновляем/добавляем запись в RT индекс
            while (!$rs->EOF)
            {
                $query = $this->sphinx_pdo->prepare('REPLACE INTO ' . SPHINX_RT_INDEX . ' (id, user_id, atoms) VALUES (?, ?, ?)');
                $query->execute(array($rs->fields('id'), $user_id, $rs->fields('atoms')));

                $cnt++;

                $rs->MoveNext();
            }
        }

        return $cnt;
    }

    // Маскирование в RT индексе предложений пользователя как отсутствующих
    public function remove_user_rt_index($user_id)
    {
        if (!$this->sphinx_pdo) return false;

        $user_id = (int)$user_id;

        // Получаем выборку компонентов от пользователя
        $rs = $this->DB->Execute(
            'SELECT main.id, main.atoms2 AS atoms ' .
            'FROM base_main AS main ' .
            'WHERE main.user_id = ' . $user_id
        );

        $cnt = 0;

        if (is_object($rs))
        {
            // Для каждого обнуляем запись в RT индекс
            while (!$rs->EOF)
            {
                $query = $this->sphinx_pdo->prepare('REPLACE INTO ' . SPHINX_RT_INDEX . ' (id, user_id, atoms) VALUES (?, ?, ?)');
                $query->execute(array($rs->fields('id'), $user_id, ''));

                $cnt++;

                $rs->MoveNext();
            }
        }

        return $cnt;
    }
}
