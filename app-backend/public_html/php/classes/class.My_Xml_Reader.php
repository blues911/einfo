<?php

/*
 * �����: My_Xml_Reader
 * ����������: ����������� xml-������� � ������
 *
 */

class My_Xml_Reader extends XmlReader {
    protected $array = array();

    public function toArray()
    {
        $array = array();

        if (self::NONE == $this->nodeType)
        {
            $this->read();
        }

        while ($this->nodeType != self::ELEMENT) {
            $this->read();
        }

        do
        {
            $array[$this->localName] = $this->_toArray();
        }
        while($this->read());

        unset($array['#text']);

        return $array;
    }

    protected function _toArray(array $array = array())
    {
        $current = $this->localName;

        while (true)
        {
            switch ($this->nodeType) {
                case self::SIGNIFICANT_WHITESPACE:
                    break;
                case self::END_ELEMENT:
                    if ($this->localName == $current)
                    {
                        break 2;
                    }
                    break;
                case self::ATTRIBUTE;
                    break;
                case self::COMMENT:
                    break;
                case self::ELEMENT:
                    $key = $this->localName;

                    if ($current != $key)
                    {
                        $data = $this->_toArray();

                        if (is_string($data))
                        {
                            $data = array($key => $data);
                        }

                        if ($this->hasAttributes)
                        {
                            $index = 0;

                            for ($index=0; $index<=$this->attributeCount; $index++)
                            {
                                $this->moveToAttributeNo($index);
                                $data['#attributes'][$this->name] = $this->value;
                            }

                            $this->moveToElement();
                        }

                        if (true == isset($array[$key]))
                        {
                            if (false === isset($array[$key][0]))
                            {
                                $oldData = $array[$key];
                                unset($array[$key]);
                                $array[$key][] = $oldData;
                            }

                            $array[$key][] = $data;
                        }
                        else
                        {
                            $array[$key] = $data;
                        }
                    }
                    break;
                case self::DOC:
                    break;
                case self::END_ENTITY:
                    break;
                case self::ENTITY:
                    break;
                case self::ENTITY_REF:
                    break;
                case self::LOADDTD:
                    break;
                case self::NONE:
                    break 2;
                case self::NOTATION:
                    break;
                case self::PI:
                    break;
                case self::TEXT:
                    $array = $this->value;
                    break;
                case self::CDATA:
                    $array = $this->value;
                    break;
                default:
                    var_dump($this->nodeType);
                    break;
            }
            $this->read();
        }

        return $array;
    }
}

?>