<?php

/*
 * Класс: Resume
 * Назначение: Управление вакансия на сайте
 *
 */


class Resume
{
    private $DB;
    private $userID;

    // конструктор
    function __construct(&$DBlink, $user_id)
    {
        $this->DB = &$DBlink;
        $this->userID = $user_id;
    }

    // проверка на разрешенность доступа
    public function CheckAccess($id)
    {
        $allow = $this->DB->GetOne("SELECT COUNT(*) FROM resume WHERE user_id=? AND id=?", array($this->userID, $id));
        return ($allow == 1);
    }

    // добавление резюме
    function Add($ins)
    {
        $this->DB->AutoExecute("resume", $ins, "INSERT");
    }

    // редактирование резюме
    function Edit($resume_id, $upd)
    {
        $this->DB->AutoExecute("resume", $upd, "UPDATE", "id=".$resume_id);
    }

    // удаление резюме
    function Delete($id)
    {
        $this->DB->Execute("DELETE FROM resume WHERE id=?", array($id));
    }
}

?>