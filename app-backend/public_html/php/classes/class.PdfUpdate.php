<?php

class PdfUpdate
{
    public $DB;

    public $pdf_path;

    private $_update_days_limit = 30;

    public function __construct(&$DB)
    {
        $this->DB = $DB;
        $this->pdf_path = DATASHEET_PATH . '/pdf/';
    }

    public function set_update_days_limit($limit)
    {
        $this->_update_days_limit = (int)$limit;

    }

    // Список
    public function get_list($limit = false, $first_row = 0)
    {
        $limit_sql = '';
        if (!empty($limit))
        {
            $limit_sql = 'LIMIT ' . $first_row . ', ' . $limit;
        }


        return $this->DB->GetAll('
            SELECT
              `comp_canon`,
              `local`,
              `remote`,
              `update`,
              IF(`img` <> "", 1, 0) AS have_images
            FROM
              datasheet
            WHERE
              `remote` <> ""
              AND (`local` = "" OR `update` < DATE(NOW() - INTERVAL ' . $this->_update_days_limit . ' DAY))
            '
            . $limit_sql
        );
    }

    // Статус и header удаленного файла
    public function get_remote_file_status($url)
    {
        stream_context_set_default(array('http' => array('method' => 'HEAD')));

        $return = false;

        $header = @get_headers($url, 1);

        if(!empty($header) && is_array($header))
        {
            if($header[0] == 'HTTP/1.1 200 OK' || $header[0] == 'HTTP/1.0 200 OK' )
            {
                $return['header'] = 200;

                if(!empty($header['Last-Modified']))
                {
                    $return['last_modified'] = date('Y-m-d H:i:s', strtotime($header['Last-Modified']));
                }
            }
            else if ($header[0] == 'HTTP/1.1 403 Forbidden' || $header[0] == 'HTTP/1.0 403 Forbidden')
            {
                $return['header'] = 403;
            }
        }

        return $return;
    }

    // Загрузка файла с сервера и сохраниние
    public function download_file($url)
    {
        stream_context_set_default(array('http' => array('method' => 'GET')));

        $file_name = $this->generate_file_name();

        file_put_contents($this->pdf_path . $this->get_dir_name($file_name) . '/' . $file_name, fopen($url, 'r'));

        return $file_name;
    }

    // Функция создания уникального имени файла .pdf
    public function generate_file_name()
    {
        $new_name = md5(uniqid()) . '.pdf';
        $dir_name = $this->get_dir_name($new_name);


        if (!file_exists($this->pdf_path . $dir_name . '/'))
        {
            $oldmask = umask(0);
            mkdir($this->pdf_path . $dir_name, 0777);
            umask($oldmask);
        }

        if (file_exists($this->pdf_path . $dir_name . '/' . $new_name))
        {
            return $this->generate_file_name();
        }
        else
        {
            return $new_name;
        }
    }

    // Изменение имени файла, даты обновления и размера
    public function set_file_name($comp_canon, $file_name)
    {
        $file_size = filesize($this->pdf_path . $this->get_dir_name($file_name) . '/' . $file_name);

        $this->DB->Execute('UPDATE datasheet SET `local` = ?, `update` = NOW(), `size` = ? WHERE comp_canon = ?', array($file_name, $file_size, $comp_canon));

        return true;
    }

    // Изменение даты обновления pdf-файла (скачивание нового файла, либо просто проверка актуальности)
    public function set_pdf_update($comp_canon, $date = null)
    {
        $date = ($date === null) ? 'NOW()' : $this->DB->qstr($date);

        $this->DB->Execute('UPDATE datasheet SET `update` = '. $date .' WHERE comp_canon = ?', array($comp_canon));

        return true;
    }

    // Удаление файлов
    public function delete_file($comp_canon)
    {
        $file_name = $this->DB->GetOne('SELECT `local` FROM datasheet WHERE comp_canon = ?', array($comp_canon));

        if (!empty($file_name))
        {
            @unlink($this->pdf_path . $this->get_dir_name($file_name) . '/' . $file_name);

            $this->DB->Execute('UPDATE datasheet SET `local` = "" WHERE comp_canon = ?', array($comp_canon));
        }

        return true;
    }

    // Удаление PDF
    public function delete_pdf($filename)
    {
        @unlink($this->pdf_path . $this->get_dir_name($filename) . '/' . $filename);
        return true;
    }

    // Путь к папке с файлом
    public function get_dir_name($file_name)
    {
        return mb_substr($file_name, 0, 2);
    }

}

?>