<?php

class AdvYandex
{
    protected $api_url = 'https://api.direct.yandex.com/json/v5/';
    protected $notification_email = 'sales@microchip.ru';

    protected $units;
    protected $_counter = 0;

    public $DB;
    protected $settings;
    public $log = array();

    public $statistic = array(
        'campaign' => array(
            'add' => 0
        ),
        'banner' => array(
            'add' => 0,
            'update' => 0,
            'update_price' => 0,
            'resume' => 0,
            'stop' => 0
        )
    );

    public function __construct(&$DB)
    {
        $this->DB = $DB;
    }

    public function get_api_url()
    {
        return rtrim($this->api_url, '\\/') . '/';
    }

    public function get_settings($var = null)
    {
        if (is_null($this->settings))
        {
            $this->settings = array();
            $_settings =  $this->DB->GetAll('SELECT * FROM adv_system WHERE system = "yandex"');

            foreach ($_settings as $row)
            {
                $this->settings[$row['var']] = $row['value'];
            }
        }

        if (isset($var))
        {
            return $this->settings[$var];
        }
        else
        {
            return $this->settings;
        }
    }

    public function get_login()
    {
        return $this->get_settings('yandex_login');
    }

    public function get_token()
    {
        return $this->get_settings('yandex_token');
    }

    public function get_units()
    {
        if (is_null($this->units))
        {
            $this->send_request('dictionaries', 'get', array(
                'DictionaryNames' => array('Currencies')
            ));
        }

        $units = explode('/', $this->units);

        return isset($units[1]) ? (int)$units[1] : false;
    }

    public function log($msg)
    {
        $this->log[] = $msg;
    }

    public function send_request($service, $method, $params = null)
    {
        if (isset($this->units) && $this->units <= 0)
        {
            throw new Exception('No more units.');
        }

        $headers = array(
            'Authorization' => 'Bearer ' . $this->get_token(),
            'Accept-Language' => 'ru',
            'Client-Login' => $this->get_login(),
            'Content-Type' => 'application/json; charset=utf-8'
        );

        $data = array(
            'method' => $method,
            'params' => $params
        );

        $body = json_encode($data);
        $url = $this->get_api_url() . $service;

        // Отправка запроса и получние ответа от сервера
        $response = Requests::post($url, $headers, $body, array(
            'timeout' => 300
        ));

        // Сохраняем информацию о кол-ве баллов
        if (isset($response->headers['Units']))
        {
            $this->units = $response->headers['Units'];
        }

        // Парсим ответ
        $result = json_decode($response->body, true);

        if (!empty($result['error']))
        {
            $this->log('Ошибка Яндекс.Директ [' . $result['error']['error_code'] . '] - отправка запроса');
        }

        if ($service != 'dictionaries')
        {
            // Логирование
            $log_data = array(
                'REQUEST' => array(
                    'url' => $url,
                    'headers' => $headers,
                    'data' => $data,
                    'body' => $body
                ),
                'RESPONSE' => array(
                    'headers' => $response->headers,
                    'body' => $response->body,
                    'data' => $result
                )
            );

            ob_start();
            echo('[' . date('r') . ']' . PHP_EOL);
            print_r($log_data);
            file_put_contents(PATH_ROOT . '/_log/adv_yandex.log', ob_get_contents() . PHP_EOL . PHP_EOL, FILE_APPEND);
            ob_clean();
        }

        return $result;
    }

    // Список компонентов для создания или изменения объявлений
    public function new_banners_get_list($limit = false, $first_row = 0)
    {
        $limit_sql = '';
        if (!empty($limit))
        {
            $limit_sql = 'LIMIT ' . $first_row . ', ' . $limit;
        }

        return $this->DB->GetAll('SELECT
                                    comp_canon, title, descr, url, phrase, price,
                                    adtitle_1, adtitle_2, adtitle_3, adtitle_4,
                                    adlink_1, adlink_2, adlink_3, adlink_4
                                  FROM adv_yandex AS adv_yandex
                                  WHERE adv_yandex.action & 1 = 1
                                  AND adv_yandex.show = 1 ORDER BY comp_canon ASC '
            . $limit_sql);
    }

    // Массив компаний и количества объявлений
    public function get_campaigns_info()
    {
        $campaign_result_list = array();

        // Получаем кол-во объявлений в компаниях из базы данных.
        $campaign_db_list = array();
        $_campaign_db_list = $this->DB->GetAll(
            'SELECT campaign_id, COUNT(banner_id) AS cnt FROM adv_yandex WHERE campaign_id IS NOT NULL GROUP BY campaign_id'
        );

        foreach($_campaign_db_list as $_row)
        {
            $campaign_db_list[$_row['campaign_id']] = $_row['cnt'];
        }

        unset($_campaign_db_list);

        $result = $this->send_request('campaigns', 'get', array(
            'SelectionCriteria' => array(
                'States' => array('ON', 'OFF', 'SUSPENDED')
            ),
            'FieldNames' => array('Id', 'Name', 'State', 'Status')
        ));

        if (!empty($result['result']['Campaigns']))
        {
            $campaign_list = $result['result']['Campaigns'];

            // Префикс названия компании и подготовка регулярного выражения
            $campaign_prefix = $this->get_settings('campaign_prefix');
            $campaign_name_pattern = "/^" . preg_quote($campaign_prefix, '/') . "(.*)/iu";

            // Удаление из списка компании не связанные с текущей системой
            foreach($campaign_list as $k => $campaign)
            {
                if (!preg_match($campaign_name_pattern , $campaign['Name']))
                {
                    unset($campaign_list[$k]);
                }
            }

            // Количество объявлений в компании
            foreach($campaign_list as $campaign)
            {
                if (!empty($campaign_db_list[$campaign['Id']]))
                {
                    $banners_count = $campaign_db_list[$campaign['Id']];
                }
                else
                {
                    $result = $this->send_request('adgroups', 'get', array(
                        'SelectionCriteria' => array(
                            'CampaignIds' => array($campaign['Id'])
                        ),
                        'FieldNames' => array('Id')
                    ));

                    if (isset($result['result']['AdGroups']) || (array_key_exists('result', $result) && count($result['result']) == 0))
                    {
                        $banners_count = !empty($result['result']['AdGroups']) ? count($result['result']['AdGroups']) : 0;
                    }
                    else
                    {
                        return false;
                    }
                }

                $campaign_result_list[] = array(
                    'campaign_id' => $campaign['Id'],
                    'campaign_info' => $campaign,
                    'banners' => $banners_count,
                    'free' => 1000 - $banners_count
                );
            }
        }

        return $campaign_result_list;
    }

    public function get_ad_group_template($name, $campaign_id, array $region_ids = array(0))
    {
        return array(
            'Name' => $name,
            'CampaignId' => $campaign_id,
            'RegionIds' => $region_ids,
        );
    }

    public function api_ad_group_add($name, $campaign_id, array $region_ids = array(0))
    {
        $r = $this->send_request('adgroups', 'add', array(
            'AdGroups' => array(
                $this->get_ad_group_template($name, $campaign_id, $region_ids)
            )
        ));

        if (!empty($r['result']['AddResults'][0]['Id']))
        {
            return $r['result']['AddResults'][0]['Id'];
        }

        return false;
    }

    public function get_text_ad_template($title, $text, $href, $is_update, $sitelink_set_id = null)
    {
        $r = array(
            'Title' => $title,
            'Text' => $text,
            'Href' => $href
        );

        if (empty($is_update))
        {
            $r['Mobile'] = 'NO';
        }

        if (!empty($sitelink_set_id))
        {
            $r['SitelinkSetId'] = $sitelink_set_id;
        }

        return $r;
    }

    public function get_ad_template($ad_group_id, $title, $text, $href, $is_update, $sitelink_set_id = null)
    {
        return array(
            'TextAd' => $this->get_text_ad_template($title, $text, $href, $is_update, $sitelink_set_id),
            'AdGroupId' => (int)$ad_group_id
        );
    }

    public function api_ad_add($group_id, $title, $text, $href, $sitelink_set_id = null)
    {
        $r = $this->send_request('ads', 'add', array(
            'Ads' => array(
                array(
                    'TextAd' => $this->get_text_ad_template($title, $text, $href, false, $sitelink_set_id),
                    'AdGroupId' => $group_id
                )
            )
        ));

        if (!empty($r['result']['AddResults'][0]['Id']))
        {
            return $r['result']['AddResults'][0]['Id'];
        }

        return false;
    }

    public function api_ad_update(array $ad_templates)
    {
        $params = array(
            'Ads' => array()
        );

        foreach ($ad_templates as $ad_id => $template)
        {
            $params['Ads'][] = array(
                'Id' => $ad_id,
                'TextAd' => $template
            );
        }

        $r = $this->send_request('ads', 'update', $params);

        return $r;
    }

    public function get_keyword_template($group_id, $keyword, $bid)
    {
        return array(
            'AdGroupId' => $group_id,
            'Keyword' => $keyword,
            'Bid' => $bid
        );
    }

    public function api_keyword_add($group_id, $keyword, $bid)
    {
        $r = $this->send_request('keywords', 'add', array(
            'Keywords' => array(
                $this->get_keyword_template($group_id, $keyword, $bid)
            )
        ));

        if (!empty($r['result']['AddResults'][0]['Id']))
        {
            return $r['result']['AddResults'][0]['Id'];
        }

        return false;
    }

    public function get_sitelink_template($title, $href)
    {
        return array(
            'Title' => $title,
            'Href' => $href
        );
    }

    /**
     * Получить id SitelinksSet из ранее созданных. Иначе
     * создать новый.
     *
     * @param array $sitelinks
     * @return int|bool
     */
    public function get_sitelink_set_id(array $sitelinks = array())
    {
        $hash = md5(serialize($sitelinks));

        $id = $this->DB->GetOne('SELECT id FROM adv_yandex_sitelink WHERE hash = ?', array($hash));

        if (empty($id))
        {
            $id = $this->api_sitelink_set_add($sitelinks);
        }

        return $id;
    }

    public function api_sitelink_set_add(array $sitelinks = array())
    {
        if (!empty($sitelinks))
        {
            $r = $this->send_request('sitelinks', 'add', array(
                'SitelinksSets' => array(
                    array(
                        'Sitelinks' => $sitelinks
                    )
                )
            ));

            if (!empty($r['result']['AddResults'][0]['Id']))
            {
                $id = (int)$r['result']['AddResults'][0]['Id'];
                $hash = md5(serialize($sitelinks));
                $this->DB->Execute('INSERT INTO adv_yandex_sitelink (hash, id) VALUES (?, ?)', array($hash, $id));
                return $id;
            }
        }

        return false;
    }

    public function api_sitelink_set_delete(array $sitelink_set_ids)
    {
        $r = $this->send_request('sitelinks', 'delete', array(
            'SelectionCriteria' => array(
                'Ids' => (array)$sitelink_set_ids
            )
        ));

        $r = (!empty($r['result']['DeleteResults'][0]['Id'])) ? true : false;

        $deleted_ids = array();
        foreach ($r['result']['DeleteResults'] as $result_row)
        {
            if (!empty($result_row['Id']))
            {
                $deleted_ids[] = (int)$result_row['Id'];
            }
        }

        if (!empty($deleted_ids))
        {
            $this->DB->Execute('DELETE FROM adv_yandex_sitelink WHERE id IN (' . implode(',', $deleted_ids) . ')');
        }

        return $r;
    }

    public function get_text_campaign_template($name)
    {
        return array(
            'Name' => $name,
            'StartDate' => date('Y-m-d'),
            'Notification' => array(
                'EmailSettings' => array(
                    'Email' => $this->notification_email,
                    'SendWarnings' => 'YES',
                    'SendAccountNews' => 'YES',
                    'CheckPositionInterval' => 60,
                    'WarningBalance' => 10
                )
            ),
            'TextCampaign' => array(
                'BiddingStrategy' => array(
                    'Search' => array(
                        'BiddingStrategyType' => 'HIGHEST_POSITION'
                    ),
                    'Network' => array(
                        'BiddingStrategyType' => 'NETWORK_DEFAULT',
                        'NetworkDefault' => array(
                            'LimitPercent' => 50,
                            'BidPercent' => 50
                        )
                    )
                )
            )
        );
    }

    public function api_campaign_add($name)
    {
        $r = $this->send_request('campaigns', 'add', array(
            'Campaigns' => array(
                $this->get_text_campaign_template($name)
            )
        ));

        if (!empty($r['result']['AddResults'][0]['Id']))
        {
            return $r['result']['AddResults'][0]['Id'];
        }

        return false;
    }

    // Добавление объявлений в компании
    public function create_new_banners($campaign_list, $new_banners)
    {
        $result = array(
            'banners' => array(),
            'campaigns_update' => array()
        );

        foreach ($campaign_list as $campaign)
        {
            // Если есть свободные места добавляется объявление
            if ($campaign['free'] > 0 && !empty($new_banners))
            {
                // Устанавливаем размер пакета.
                $chunk_size = ($campaign['free'] < ADV_YANDEX_REQUEST_CHUNK) ? $campaign['free'] : ADV_YANDEX_REQUEST_CHUNK;

                // Сбрасываем ключи $new_banners.
                $new_banners = array_values($new_banners);

                // Шаблоны групп объявлений
                $ad_group_templates = array();
                $ad_group_counter = 0;

                foreach ($new_banners as $new_banner)
                {
                    $ad_group_templates[] = $this->get_ad_group_template($new_banner['title'], $campaign['campaign_id']);
                    $ad_group_counter++;
                    if ($ad_group_counter == $chunk_size)
                    {
                        break;
                    }
                }

                // Добавляем группы объявлений
                $ad_group_result = $this->send_request('adgroups', 'add', array(
                    'AdGroups' => $ad_group_templates
                ));

                // Ключевые слова
                $keyword_templates = array();
                $keyword_counter = 0;

                foreach ($ad_group_result['result']['AddResults'] as $k => $ad_group_row)
                {
                    $new_banner = $new_banners[$k];

                    if (!empty($ad_group_row['Id']))
                    {
                        $keyword_templates[] = $this->get_keyword_template(
                            (int)$ad_group_row['Id'],
                            $new_banner['phrase'],
                            $new_banner['price'] * 1000000
                        );
                    }
                    else
                    {
                        $keyword_templates[] = array();
                    }

                    $keyword_counter++;
                    if ($keyword_counter == $chunk_size)
                    {
                        break;
                    }
                }

                // Добавляем ключевые слова
                $keyword_result = $this->send_request('keywords', 'add', array(
                    'Keywords' => $keyword_templates
                ));

                // SitelinksSet
                $sitelink_set_ids = array();
                $sitelink_set_counter = 0;

                foreach ($new_banners as $new_banner)
                {
                    $sitelinks = array();
                    for ($i = 1; $i <= 4; $i++)
                    {
                        if (!empty($new_banner['adtitle_' . $i]) && !empty($new_banner['adlink_' . $i]))
                        {
                            $sitelinks[] = $this->get_sitelink_template($new_banner['adtitle_' . $i], $new_banner['adlink_' . $i]);
                        }
                    }

                    $sitelink_set_ids[] = $this->get_sitelink_set_id($sitelinks);

                    $sitelink_set_counter++;
                    if ($sitelink_set_counter == $chunk_size)
                    {
                        break;
                    }
                }

                // Шаблоны баннеров
                $banner_templates = array();
                $banner_counter = 0;

                foreach ($new_banners as $k => $new_banner)
                {
                    $ad_group_id = !empty($ad_group_result['result']['AddResults'][$k]['Id']) ? $ad_group_result['result']['AddResults'][$k]['Id'] : null;
                    $sitelink_set_id = !empty($sitelink_set_ids[$k]) ? $sitelink_set_ids[$k] : null;

                    $banner_templates[] = $this->get_ad_template(
                        $ad_group_id,
                        $new_banner['title'],
                        $new_banner['descr'],
                        'http://' . HOST_FRONTEND . $new_banner['url'],
                        false,
                        $sitelink_set_id
                    );

                    $banner_counter++;
                    if ($banner_counter == $chunk_size)
                    {
                        break;
                    }
                }

                // Добавляем баннеры
                $banner_result = $this->send_request('ads', 'add', array(
                    'Ads' => $banner_templates
                ));


                $_result_counter = 0;
                foreach (array_keys($new_banners) as $k)
                {
                    if (!empty($banner_result['result']['AddResults'][$k]['Id']))
                    {
                        $banner_id = (int)$banner_result['result']['AddResults'][$k]['Id'];
                        $keyword_id = !empty($keyword_result['result']['AddResults'][$k]['Id']) ? (int)$keyword_result['result']['AddResults'][$k]['Id'] : null;

                        // Обновление статуса и параметров объявления
                        $this->DB->Execute(
                            'UPDATE adv_yandex SET banner_id = ?, campaign_id = ?, phrase_id = ?, action = 0, update_date = NOW() WHERE comp_canon = ?',
                            array($banner_id, $campaign['campaign_id'], $keyword_id, $new_banners[$k]['comp_canon'])
                        );

                        $this->statistic['banner']['add']++;
                    }

                    unset($new_banners[$k]);

                    $_result_counter++;
                    if ($_result_counter == $chunk_size)
                    {
                        break;
                    }
                }

                $result['campaigns_update'][] = $campaign['campaign_id'];
            }
        }

        $result['banners'] = $new_banners;

        return $result;
    }

    // Обновление объявлений
    public function update_banners()
    {
        if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
        {
            return false;
        }

        $update_handler = function(AdvYandex $direct, array $banner_ids, array $list)
        {
            $result = $direct->send_request('ads', 'get', array(
                'SelectionCriteria' => array(
                    'Ids' => $banner_ids
                ),
                'FieldNames' => array('Id', 'AdGroupId'),
                'TextAdFieldNames' => array('SitelinkSetId')
            ));

            if(!empty($result['result']['Ads']))
            {
                $ad_templates = array();

                foreach($result['result']['Ads'] as $item)
                {
                    $sitelinks = array();

                    if (!empty($list[$item['Id']]['sitelinks']))
                    {
                        foreach ($list[$item['Id']]['sitelinks'] as $_sitelink)
                        {
                            $sitelinks[] = $direct->get_sitelink_template($_sitelink['Title'], $_sitelink['Href']);
                        }
                    }

                    // Добавляем новый SitelinkSet
                    $sitelink_set_id = null;
                    if (!empty($sitelinks))
                    {
                        $sitelink_set_id = $direct->get_sitelink_set_id($sitelinks);
                    }

                    $ad_templates[$item['Id']] = $direct->get_text_ad_template(
                        $list[$item['Id']]['title'],
                        $list[$item['Id']]['descr'],
                        $list[$item['Id']]['url'],
                        true,
                        $sitelink_set_id
                    );
                }

                $update_result = $direct->api_ad_update($ad_templates);

                foreach ($update_result['result']['UpdateResults'] as $result_row)
                {
                    if (!empty($result_row['Id']))
                    {
                        $direct->DB->Execute('UPDATE adv_yandex SET action = action ^ 16, update_date = NOW() WHERE banner_id = ?', array($result_row['Id']));
                        $direct->statistic['banner']['update']++;
                    }
                }
            }
        };

        $banners_list = $this->DB->GetAll(
            'SELECT
              comp_canon, title, descr, url, banner_id,
              adtitle_1, adtitle_2, adtitle_3, adtitle_4,
              adlink_1, adlink_2, adlink_3, adlink_4
            FROM adv_yandex AS adv_yandex
            WHERE adv_yandex.action & 18 = 18
            AND banner_id IS NOT NULL ORDER BY comp_canon ASC'
        );

        if (!empty($banners_list))
        {
            $list = $banner_ids = array();

            foreach ($banners_list as $banner)
            {
                $this->_counter++;

                $banner_ids[] = $banner['banner_id'];

                $list[$banner['banner_id']] = array(
                    'title' => $banner['title'],
                    'descr' => $banner['descr'],
                    'url' => 'http://' . HOST_FRONTEND . $banner['url'],
                    'sitelinks' => array()
                );

                for ($i = 1; $i <= 4; $i++)
                {
                    if (!empty($banner['adtitle_' . $i]) && !empty($banner['adlink_' . $i]))
                    {
                        $list[$banner['banner_id']]['sitelinks'][] = array(
                            'Title' => $banner['adtitle_' . $i],
                            'Href' => $banner['adlink_' . $i]
                        );
                    }
                }

                if ($this->_counter >= ADV_YANDEX_REQUEST_CHUNK)
                {
                    $update_handler($this, $banner_ids, $list);

                    $list = $banner_ids = array();
                    $this->_counter = 0;

                    if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
                    {
                        break;
                    }
                }
            }

            if (!empty($banner_ids))
            {
                $update_handler($this, $banner_ids, $list);
            }

        }

        return true;
    }

    // Создание нескольких компаний
    public function create_new_campaigns($count = 1)
    {
        $campaign_result_list = array();

        // Префикс названия компании
        $campaign_prefix = $this->get_settings('campaign_prefix');

        for ($i = 1; $i <= $count; $i++)
        {
            // Добавление компании
            $campaign_id = $this->api_campaign_add($campaign_prefix . '_' . uniqid());

            if (!empty($campaign_id))
            {
                $campaign_result_list[] = array(
                    'campaign_id' => $campaign_id,
                    'banners' => 0,
                    'free' => 1000
                );

                $this->statistic['campaign']['add']++;
            }
        }

        return $campaign_result_list;
    }

    // Запуск существующих остановленных объявлений
    public function resume_banners()
    {
        if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
        {
            return false;
        }

        $banners_list = $this->DB->GetAll('SELECT banner_id FROM adv_yandex WHERE `action` & 6 = 6 AND `show` = 1 AND banner_id IS NOT NULL');

        if (!empty($banners_list))
        {
            // Разархивировываем объявления
            $_ids = array();
            foreach ($banners_list as $banner)
            {
                $_ids[] = $banner['banner_id'];
            }

            $this->unarchive_banners($_ids);

            // Запускаем объявления
            $resume_handler = function(AdvYandex $direct, array $list)
            {
                $result = $direct->send_request('ads', 'resume', array(
                    'SelectionCriteria' => array(
                        'Ids' => $list
                    )
                ));

                if(!empty($result['result']['ResumeResults']))
                {
                    foreach ($result['result']['ResumeResults'] as $result_row)
                    {
                        if (!empty($result_row['Id']))
                        {
                            $direct->DB->Execute('UPDATE adv_yandex SET action = action ^ 4, update_date = NOW() WHERE banner_id = ?', array($result_row['Id']));
                            $direct->statistic['banner']['resume']++;
                        }
                    }
                }
            };

            $list = array();
            foreach ($banners_list as $banner)
            {
                $this->_counter++;

                $list[] = $banner['banner_id'];

                if ($this->_counter >= ADV_YANDEX_REQUEST_CHUNK)
                {
                    $resume_handler($this, $list);

                    $list = array();
                    $this->_counter = 0;

                    if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
                    {
                        break;
                    }
                }

            }

            if (!empty($list))
            {
                $resume_handler($this, $list);
            }
        }

        return true;
    }

    // Проверяем наличие архивированных объявлений,
    // и при необходимости разархивировываем их.
    public function unarchive_banners($banners_ids)
    {
        $result = 0;

        // Получаем список заархивированных баннеров

        $archived_banners = array();

        for ($i = 0; $i < count($banners_ids); $i += 2000)
        {
            $_ids = array_slice($banners_ids, $i, 2000);

            $response = $this->send_request('ads', 'get', array(
                'SelectionCriteria' => array(
                    'Ids' => $_ids,
                    'States' => array('ARCHIVED')
                ),
                'FieldNames' => array('Id', 'CampaignId')
            ));

            if (!empty($response['result']['Ads']))
            {
                foreach ($response['result']['Ads'] as $_banner)
                {
                    $archived_banners[$_banner['CampaignId']][] = $_banner['Id'];
                }
            }
        }

        // Разархивировываем баннеры

        if (!empty($archived_banners))
        {
            foreach ($archived_banners as $campaign_id => $campaign_banners)
            {
                for ($i = 0; $i < count($campaign_banners); $i += 2000)
                {
                    $_ids = array_slice($campaign_banners, $i, 2000);

                    $response = $this->send_request('ads', 'unarchive', array(
                        'SelectionCriteria' => array(
                            'Ids' => $_ids
                        )
                    ));

                    if (!empty($response['result']['UnarchiveResults']))
                    {
                        foreach ($response['result']['UnarchiveResults'] as $result_row)
                        {
                            if (!empty($result_row['Id']))
                            {
                                $result++;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    // Остановка существующих запущенных объявлений
    public function stop_banners()
    {
        if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
        {
            return false;
        }

        $banners_list = $this->DB->GetAll('SELECT banner_id FROM adv_yandex WHERE `action` & 6 = 6 AND `show` = 0 AND banner_id IS NOT NULL');

        if(!empty($banners_list))
        {
            $stop_handler = function(AdvYandex $direct, array $list)
            {
                $result = $direct->send_request('ads', 'suspend', array(
                    'SelectionCriteria' => array(
                        'Ids' => $list
                    )
                ));

                if(!empty($result['result']['SuspendResults']))
                {
                    foreach ($result['result']['SuspendResults'] as $result_row)
                    {
                        if (!empty($result_row['Id']))
                        {
                            $direct->DB->Execute('UPDATE adv_yandex SET action = action ^ 4, update_date = NOW() WHERE banner_id = ?', array($result_row['Id']));
                            $direct->statistic['banner']['stop']++;
                        }
                    }
                }
            };

            $list = array();

            foreach ($banners_list as $banner)
            {
                $this->_counter++;

                $list[] = $banner['banner_id'];

                if ($this->_counter >= ADV_YANDEX_REQUEST_CHUNK)
                {
                    $stop_handler($this, $list);

                    $list = array();
                    $this->_counter = 0;

                    if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
                    {
                        break;
                    }
                }

            }

            if (!empty($list))
            {
                $stop_handler($this, $list);
            }
        }

        return true;
    }

    // Обновление ставок
    public function update_price()
    {
        if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
        {
            return false;
        }

        $phrases_list = $this->DB->GetAll('SELECT comp_canon, price, banner_id, campaign_id, phrase_id FROM adv_yandex WHERE `action` & 10 = 10 AND phrase_id IS NOT NULL');

        if (!empty($phrases_list))
        {
            $update_handler = function(AdvYandex $direct, array $list, array $list_result)
            {
                $result = $direct->send_request('bids', 'set', array(
                    'Bids' => $list
                ));

                if(!empty($result['result']['SetResults']))
                {
                    foreach ($result['result']['SetResults'] as $k => $result_row)
                    {
                        if (!empty($result_row['KeywordId']))
                        {
                            $direct->DB->Execute('UPDATE adv_yandex SET action = action ^ 8, update_date = NOW() WHERE comp_canon = ?', array($list_result[$k]));
                            $direct->statistic['banner']['update_price']++;
                        }
                    }
                }
            };

            $list = $list_result = array();

            foreach ($phrases_list as $phrase)
            {
                $this->_counter++;

                $list[] = array(
                    'KeywordId' => $phrase['phrase_id'],
                    'Bid' => $phrase['price'] * 1000000
                );

                $list_result[] =  $phrase['comp_canon'];

                if ($this->_counter >= ADV_YANDEX_REQUEST_CHUNK)
                {
                    $update_handler($this, $list, $list_result);

                    $list = $list_result = array();
                    $this->_counter = 0;

                    if ($this->get_units() <= ADV_YANDEX_MIN_UNITS)
                    {
                        break;
                    }
                }
            }

            if (!empty($list))
            {
                $update_handler($this, $list, $list_result);
            }

        }

        return true;
    }

    // Отправка объявлений на модерацию
    public function moderate_banners($campaign_ids = null)
    {
        // Получаем список компаний
        $campaign_ids = empty($campaign_ids) ? array() : (array)$campaign_ids;

        if (empty($campaign_ids))
        {
            $campaigns = $this->send_request('campaigns', 'get', array(
                'SelectionCriteria' => array(
                    'Types' => array('TEXT_CAMPAIGN'),
                    'States' => array('ON', 'OFF', 'SUSPENDED')
                ),
                'FieldNames' => array('Id')
            ));

            $campaign_ids = array();

            if (!empty($campaigns['result']['Campaigns']))
            {
                foreach ($campaigns['result']['Campaigns'] as $result_row)
                {
                    if (!empty($result_row['Id']))
                    {
                        $campaign_ids[] = (int)$result_row['Id'];
                    }
                }
            }
        }

        if (empty($campaign_ids))
        {
            return false;
        }

        // Получаем список объявлений в компании со статусом DRAFT (требует модерации)
        $campaign_ids_chunks = array_chunk($campaign_ids, 10);
        $ad_ids = array();

        foreach ($campaign_ids_chunks as $campaign_ids_chunk)
        {
            $ads = $this->send_request('ads', 'get', array(
                'SelectionCriteria' => array(
                    'CampaignIds' => $campaign_ids_chunk,
                    'Statuses' => array('DRAFT')
                ),
                'FieldNames' => array('Id')
            ));

            if (!empty($ads['result']['Ads']))
            {
                foreach ($ads['result']['Ads'] as $result_row)
                {
                    if (!empty($result_row['Id']))
                    {
                        $ad_ids[] = $result_row['Id'];
                    }
                }
            }
        }

        // Отправляем объявления на модерацию
        if (!empty($ad_ids))
        {
            $ad_ids_chunks = array_chunk($ad_ids, 10000);

            foreach ($ad_ids_chunks as $ad_ids_chunk)
            {
                $this->send_request('ads', 'moderate', array(
                    'SelectionCriteria' => array(
                        'Ids' => $ad_ids_chunk
                    )
                ));
            }
        }

        return true;
    }

    // Ссылка яндекс oauth
    public function get_auth_link()
    {
        return 'https://oauth.yandex.ru/authorize?response_type=code&client_id=' . $this->get_settings('yandex_application_id');
    }

    // Параметры авторизации
    public function get_authorization_param($code)
    {
        return array(
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => $this->get_settings('yandex_application_id'),
            'client_secret' => $this->get_settings('yandex_application_pwd')
        );
    }

    // Обновление токена
    public function update_token($token)
    {
        $this->DB->Execute('DELETE FROM adv_system WHERE system = "yandex" AND var = "yandex_token"');
        $this->DB->Execute('INSERT INTO adv_system (var, system, value) VALUES ("yandex_token", "yandex", ?)', array($token));
    }

    // Синхронизация таблицы с таблицей компонентов
    public function adv_yandex_sync()
    {
        // Синхронизация компонентов
        $sync = $this->DB->GetAll('
            SELECT
              canon.id AS cursor_id,
              canon.comp_canon AS cursor_title,
              canon.offer_cnt AS cursor_offer_cnt,
              adv_yandex.show AS cursor_show_status,
              IF(canon.offer_cnt <> adv_yandex.offer_cnt AND (adv_yandex.offer_cnt = 0 OR canon.offer_cnt = 0), 1, 0) AS cursor_update_offer_cnt,
              IF(adv_yandex.comp_canon IS NULL, 1, 0) AS cursor_create_new,
              IF(canon.offer_cnt > 0 AND canon.adv_show = 1, 1, 0) AS cursor_show_banner
            FROM
              catalog_comp_canon AS canon
              LEFT JOIN adv_yandex AS adv_yandex ON adv_yandex.comp_canon = canon.comp_canon
            WHERE
              adv_yandex.comp_canon IS NULL
              OR canon.offer_cnt <> adv_yandex.offer_cnt
              OR canon.adv_show <> adv_yandex.show
            GROUP BY canon.comp_canon
        ');

        $default = $this->get_settings();

        foreach ($sync as $banner)
        {
            if ($banner['cursor_create_new'] == 1)
            {
                $this->DB->Execute('
                    INSERT INTO adv_yandex (comp_canon, offer_cnt, title, descr, url, phrase, price, `show`, `action`,
                    adtitle_1, adlink_1, adtitle_2, adlink_2, adtitle_3, adlink_3, adtitle_4, adlink_4)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                ', array(
                    $banner['cursor_title'],
                    $banner['cursor_offer_cnt'],
                    str_replace('%COMPONENT%', $banner['cursor_title'], $default['banner_title']),
                    str_replace('%COMPONENT%', $banner['cursor_title'], $default['banner_descr']),
                    str_replace('%COMPONENT%', urlencode($banner['cursor_title']), $default['catalog_path']),
                    $this->phrase_sanitize($banner['cursor_title']),
                    $default['banner_price'],
                    $banner['cursor_show_banner'],
                    1,
                    str_replace('%COMPONENT%', $banner['cursor_title'], $default['yandex_adtitle_1']),
                    str_replace('%COMPONENT%', urlencode($banner['cursor_title']), $default['yandex_adlink_1']),
                    str_replace('%COMPONENT%', $banner['cursor_title'], $default['yandex_adtitle_2']),
                    str_replace('%COMPONENT%', urlencode($banner['cursor_title']), $default['yandex_adlink_2']),
                    str_replace('%COMPONENT%', $banner['cursor_title'], $default['yandex_adtitle_3']),
                    str_replace('%COMPONENT%', urlencode($banner['cursor_title']), $default['yandex_adlink_3']),
                    str_replace('%COMPONENT%', $banner['cursor_title'], $default['yandex_adtitle_4']),
                    str_replace('%COMPONENT%', urlencode($banner['cursor_title']), $default['yandex_adlink_4'])
                ));
            }
            else if ($banner['cursor_update_offer_cnt'])
            {
                $this->DB->Execute('
                    UPDATE
                      adv_yandex
                    SET
                      `show` = ?,
                      offer_cnt = ?,
                      `action` = IF(banner_id IS NULL, 1, `action` | 6)
                    WHERE
                      comp_canon = ?
                ', array(
                    $banner['cursor_show_banner'],
                    $banner['cursor_offer_cnt'],
                    $banner['cursor_title']
                ));
            }
            else if ($banner['cursor_show_status'] != $banner['cursor_show_banner'])
            {
                $this->DB->Execute('
                    UPDATE
                      adv_yandex
                    SET
                      `show` = ?,
                      `action` = IF(banner_id IS NULL, 1, `action` | 6)
                    WHERE
                      comp_canon = ?
                ', array(
                    $banner['cursor_show_banner'],
                    $banner['cursor_title']
                ));
            }
        }

        $check_rs = $this->DB->Execute('SELECT comp_canon, action FROM adv_yandex');

        while(!$check_rs->EOF)
        {
            $exist_comp = $this->DB->GetOne('SELECT id FROM catalog_comp_canon WHERE comp_canon = ?', array($check_rs->fields('comp_canon')));

            if (empty($exist_comp) && $check_rs->fields('action') != 0)
            {
                $this->DB->Execute('
                    UPDATE
                      adv_yandex
                    SET
                      `show` = 0,
                      offer_cnt = 0,
                      `action` = IF(banner_id IS NULL, 0, `action` | 6)
                    WHERE
                      comp_canon = ?
                ', array($check_rs->fields('comp_canon')));
            }

            $check_rs->MoveNext();
        }

        // Генерация заголовка и текста объявлений
        $this->generate_ad_content();

        return true;
    }

    // Обновление описание для текстовых объявлений
    public function generate_ad_content()
    {
        // Подготавливаем временную таблицу со срезом adv_yandex
        $this->DB->Execute('DROP TEMPORARY TABLE IF EXISTS adv_yandex_tmp');
        $this->DB->Execute('CREATE TEMPORARY TABLE adv_yandex_tmp LIKE adv_yandex');
        $this->DB->Execute('INSERT INTO adv_yandex_tmp SELECT * FROM adv_yandex');

        // Шаблоны объявлений
        $templates = $this->DB->GetAll('SELECT * FROM adv_template ORDER BY id DESC');

        foreach ($templates as $template)
        {
            // Список компонентов для данного шаблона
            $banners_list = $this->DB->GetAll('
                SELECT
                  adv_yandex.comp_canon,
                  adv_yandex.title,
                  adv_yandex.descr,
                  canon.comp_canon AS comp_title,
                  canon.category_id,
                  canon.price_median AS price,
                  adv_yandex.adtitle_1,
                  adv_yandex.adlink_1,
                  adv_yandex.adtitle_2,
                  adv_yandex.adlink_2,
                  adv_yandex.adtitle_3,
                  adv_yandex.adlink_3,
                  adv_yandex.adtitle_4,
                  adv_yandex.adlink_4
                FROM
                  adv_yandex AS adv_yandex
                  INNER JOIN catalog_comp_canon AS canon ON canon.comp_canon = adv_yandex.comp_canon
                  INNER JOIN categories AS category ON category.id = canon.category_id
                  INNER JOIN categories AS cat ON cat.id = ?
                WHERE
                  category.c_left BETWEEN cat.c_left AND cat.c_right
                  AND canon.comp_canon LIKE ?
                  AND adv_yandex.show = 1
            ', array(
                $template['category_id'],
                $template['comp_like']
            ));

            $banners_list = !empty($banners_list) ? $banners_list : array();

            $this->update_ad_content($banners_list, $template);
        }

        // Обновление остальных объявлений
        $banners_list = $this->DB->GetAll('
            SELECT
              adv_yandex.comp_canon,
              adv_yandex.title,
              adv_yandex.descr,
              canon.comp_canon AS comp_title,
              canon.price_median AS price
            FROM
              adv_yandex AS adv_yandex
              INNER JOIN catalog_comp_canon AS canon ON canon.comp_canon = adv_yandex.comp_canon
            WHERE
              adv_yandex.title = "" OR adv_yandex.descr = ""
        ');

        $banners_list = !empty($banners_list) ? $banners_list : array();

        $template = $this->get_settings();

        $template['yandex_title'] = $template['banner_title'];
        $template['yandex_descr'] = $template['banner_descr'];

        $this->update_ad_content($banners_list ,$template);

        // Сбрасываем action для записей, которые фактически не изменились
        $this->DB->Execute(
            'UPDATE
              adv_yandex_tmp AS tmp,
              adv_yandex AS adv
            SET
              adv.action = tmp.action
            WHERE
              tmp.comp_canon = adv.comp_canon AND
              tmp.offer_cnt = adv.offer_cnt AND
              tmp.title = adv.title AND
              tmp.descr = adv.descr AND
              tmp.url = adv.url AND
              tmp.phrase = adv.phrase AND
              tmp.price = adv.price AND
              tmp.show = adv.show AND
              tmp.adtitle_1 = adv.adtitle_1 AND
              tmp.adtitle_2 = adv.adtitle_2 AND
              tmp.adtitle_3 = adv.adtitle_3 AND
              tmp.adtitle_4 = adv.adtitle_4 AND
              tmp.adlink_1 = adv.adlink_1 AND
              tmp.adlink_2 = adv.adlink_2 AND
              tmp.adlink_3 = adv.adlink_3 AND
              tmp.adlink_4 = adv.adlink_4'
        );

        // Удаляем временную таблицу
        $this->DB->Execute('DROP TEMPORARY TABLE IF EXISTS adv_yandex_tmp');
    }

    // Обновление описания объявлений
    public function update_ad_content($banners_list, $template)
    {
        foreach ($banners_list as $banner)
        {
            // Подготовка данных
            $content = str_replace('%COMPONENT%', $banner['comp_title'], $template);

            if ($banner['price'] > 0)
            {
                $content = str_replace('%PRICE%', number_format($banner['price'], 2, '.', ''), $content);
                $content = str_replace('%PRICE_INT%', ceil($banner['price']), $content);
            }
            else
            {
                $content = str_replace('%PRICE%', '', $content);
                $content = str_replace('%PRICE_INT%', '', $content);
            }

            // Проверка длинны заголовка
            if (mb_strlen($content['yandex_title']) > 33)
            {
                $content['yandex_title'] = mb_substr($content['yandex_title'], 0, 33);
            }

            // Проверка длинны описания
            if (mb_strlen($content['yandex_descr']) > 75)
            {
                $content['yandex_descr'] = mb_substr($content['yandex_descr'], 0, 75);
            }

            // Обновление данных
            if ($content['yandex_title'] != $banner['title'] || $content['yandex_descr'] != $banner['descr'] ||
                $content['yandex_adtitle_1'] != $banner['adtitle_1'] || $content['yandex_adtitle_2'] != $banner['adtitle_2'] ||
                $content['yandex_adtitle_3'] != $banner['adtitle_3'] || $content['yandex_adtitle_4'] != $banner['adtitle_4'] ||
                $content['yandex_adlink_1'] != $banner['adlink_1'] || $content['yandex_adlink_2'] != $banner['adlink_2'] ||
                $content['yandex_adlink_3'] != $banner['adlink_3'] || $content['yandex_adlink_4'] != $banner['adlink_4'])
            {
                $this->DB->Execute(
                    'UPDATE
                      adv_yandex
                    SET
                      title = ?,
                      descr = ?,
                      adtitle_1 = ?,
                      adtitle_2 = ?,
                      adtitle_3 = ?,
                      adtitle_4 = ?,
                      adlink_1 = ?,
                      adlink_2 = ?,
                      adlink_3 = ?,
                      adlink_4 = ?,
                      action = IF((action & 1) = 0, action | 18, action)
                    WHERE
                      comp_canon = ?',
                    array(
                        $content['yandex_title'],
                        $content['yandex_descr'],
                        $content['yandex_adtitle_1'],
                        $content['yandex_adtitle_2'],
                        $content['yandex_adtitle_3'],
                        $content['yandex_adtitle_4'],
                        $content['yandex_adlink_1'],
                        $content['yandex_adlink_2'],
                        $content['yandex_adlink_3'],
                        $content['yandex_adlink_4'],
                        $banner['comp_canon']
                    )
                );
            }
        }
    }

    // Обновление описания объявлений
    public function normalize_table()
    {
        $this->DB->Execute('UPDATE adv_yandex SET action = (IF(action = 2, 0, IF(action = 5 OR action = 9 OR action = 17, 1, action)))');

        // Сброс action поля для объявлений с пустыми (phrase_id, banner_id или campaign_id)
        // и с нулевым первым битом в поле action
        $this->DB->Execute('UPDATE adv_yandex SET action = 0 WHERE `action` & 1 != 1 AND (phrase_id IS NULL OR banner_id IS NULL OR campaign_id IS NULL)');
    }

    // Очистка фразы от недопустимых символов
    public function phrase_sanitize($phrase)
    {
        $phrase = filter_var($phrase, FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW);
        $phrase = preg_replace('/[^a-zA-Zа-яА-ЯёЁ0-9- ]/us', ' ', $phrase);
        $phrase = preg_replace('/[ ]{2,}/us', ' ', $phrase);
        $phrase = trim($phrase);

        return $phrase;
    }

    // Получить ID групп фраз со статусом «Мало показов»
    public function get_rarely_group_ids(array $campaign_ids)
    {
        $result = array();

        $r = $this->send_request('keywords', 'get', array(
                                                'SelectionCriteria' => array(
                                                    'CampaignIds'     => $campaign_ids,
                                                    'ServingStatuses' => array('RARELY_SERVED')
                                                ),
                                                'FieldNames' => array('AdGroupId')
                                            ));

        if (!empty($r['result']))
        {
            foreach ($r['result']['Keywords'] as $group)
            {
                $result[] = strval($group['AdGroupId']);
            }
        }

        return $result;
    }

    // Получить ID объявлений групп с редкими ключевиками
    public function get_rarely_banner_ids(array $group_ids)
    {
        $result = array();

        $ads_list = $this->send_request('ads', 'get', array(
                                                 'SelectionCriteria' => array(
                                                     'AdGroupIds'     => $group_ids
                                                 ),
                                                 'FieldNames' => array('Id')
                                             ));

        if (!empty($ads_list['result']))
        {
            foreach ($ads_list['result']['Ads'] as $banner)
            {
                $result[] = strval($banner['Id']);
            }
        }

        return $result;
    }
}