<?php

/**
 * Class PdfViewer
 *
 * Работа с документами формата .pdf
 *
 * Возможности:
 * 1) Экспорт изображений из .pdf файла в директорию;
 * 2) Параметрический поиск изображения в директории;
 * 3) Удаление директории
 *
 * @since 29-05-2013
 * @property string $images_dir
 */
class PdfViewer
{
    /**
     * @var string - текст первой ошибки
     */
    public $error_msg = '';

    /**
     * @var string $_images_dir - директория для работы с изображениями
     */
    private $_images_dir;

    /**
     * @var string $_images_temp_dir - временная директория для хранения файлов
     */
    private $_images_temp_dir;

    /**
     * Экспорт изображений из файла .pdf
     *
     * @param string $file_path - путь к pdf файлу
     * @return bool
     */
    public function images_export($file_path)
    {
        // Имя директории для экспорта
        $this->_images_temp_dir = uniqid('export_') . '/';

        // Установить временную директорию для экспорта
        if (empty($this->_images_dir))
        {
            $this->_images_dir = sys_get_temp_dir() . '/';
        }

        // Проверка переменных на правильность
        $this->check_validation($file_path, 'Не верно указан путь к файлу .pdf -> экспорт остановлен', 'file_not_found');
        $this->check_validation($this->_images_dir, 'Не найдена директория для экспорта или директория не доступна для чтения -> экспорт остановлен', 'is_readable');

        if (empty($this->error_msg))
        {
            $images_temp_dir = $this->_images_dir . $this->_images_temp_dir;

            mkdir($images_temp_dir);

            exec(XPDF . 'pdfimages -j ' . $file_path . ' ' . $images_temp_dir);

            $files = scandir($images_temp_dir);

            foreach ($files as $file)
            {
                $file_info = pathinfo($file);

                // Создать файл с новым расширением и удалить старый
                if ($file_info['extension'] != 'jpg')
                {
                    image_magick('convert ' . $images_temp_dir . $file . ' ' . $images_temp_dir . $file_info['filename'] . '.jpg' );

                    @unlink($images_temp_dir . $file);
                }
            }

            return $images_temp_dir;
        }
        else
        {
            return false;
        }
    }

    /**
     * Параметрический поиск изображения в директории
     *
     * @var array $options (min_width - минимальная ширина изображения
     *                      max_width - максимальная ширина изображения
     *                      min_height - минимальная высота изображения
     *                      max_height - максимальная высота изображения
     *                      ratio - соотношение сторон (например: 2:1)
     *                      cnt - количество возвращаемых изображений)
     *
     * @return array - массив найденных изображений
     */
    public function images_search($options)
    {
        $images = array();

        // Директория для поиска изображений
        if (isset($this->_images_temp_dir))
        {
            $images_search_dir = $this->_images_dir . $this->_images_temp_dir;
        }
        else
        {
            $images_search_dir = $this->_images_dir;
        }

        // Проверка переменных на правильность
        $this->check_validation($images_search_dir, 'Не найдена директория для поиска -> поиск остановлен', 'dir_not_found');

        if (empty($this->error_msg))
        {
            $files = scandir($images_search_dir);
            $extensions = array('jpg', 'jpeg', 'png');
            $i = 0;

            // Необходимая пропорция изображения
            if (!empty($options['ratio']))
            {
                $options['ratio'] = explode(':', $options['ratio']);
                $options['ratio'][0] = (float)str_replace(',', '.', $options['ratio'][0]);
                $options['ratio'][1] = (float)str_replace(',', '.', $options['ratio'][1]);
                $options['ratio'] = max($options['ratio'][0], $options['ratio'][1]) / min($options['ratio'][0], $options['ratio'][1]);
            }

            foreach ($files as $file)
            {
                $extension = pathinfo($file, PATHINFO_EXTENSION);

                if ($file != '.' && $file != '..' && in_array($extension, $extensions))
                {
                    // Свойства изображения (ширина, высота, тип файла и т.д.)
                    $image_size = getimagesize($images_search_dir . $file);

                    // Пропорция текущего изображения
                    $current_ratio = max($image_size[0], $image_size[1]) / min($image_size[0], $image_size[1]);

                    // Проверка на минимальную ширину
                    if (!empty($options['min_width']) && $image_size[0] < $options['min_width'])
                    {
                        continue;
                    }

                    // Проверка на максимальную ширину
                    if (!empty($options['max_width']) && $image_size[0] > $options['max_width'])
                    {
                        continue;
                    }

                    // Проверка на минимальную высоту
                    if (!empty($options['min_height']) && $image_size[1] < $options['min_height'])
                    {
                        continue;
                    }

                    // Проверка на максимальную высоту
                    if (!empty($options['max_height']) && $image_size[1] > $options['max_height'])
                    {
                        continue;
                    }

                    // Проверка на соотношение сторон
                    if (!empty($options['ratio']) && $current_ratio > $options['ratio'])
                    {
                        continue;
                    }

                    // Проверка на кол-во найденных изображений
                    if (!empty($options['cnt']) && count($images) == $options['cnt'])
                    {
                        break;
                    }

                    $images[$i]['name'] = $file;
                    $images[$i]['width'] = $image_size[0];
                    $images[$i]['height'] = $image_size[0];
                    $images[$i]['extension'] = $extension;
                    $images[$i]['size'] = filesize($images_search_dir . $file);

                    $i++;
                }
            }

            return $images;
        }
        else
        {
            return false;
        }
    }

    /**
     * Очистка каталога с изображениями
     * @param string $dir_name - путь удаляемой директории
     */
    public function images_clear_dir($dir_name = '')
    {
        if (empty($dir_name))
        {
            $dir_name = $this->_images_dir . $this->_images_temp_dir;
        }

        // Проверка переменных на правильность
        $this->check_validation($dir_name, 'Не найдена директория для очистки -> очистка остановлена', 'dir_not_found');

        // Если ошибок нет, сканируем директорию и рекурсивно удаляем все файлы
        if (empty($this->error_msg))
        {
            $files = scandir($dir_name);

            foreach ($files as $file)
            {
                if ($file != '.' && $file != '..')
                {
                    if (filetype($dir_name . '/' . $file) == 'dir')
                    {
                        // Удаляем директорию рекурсивно
                        $this->images_clear_dir($dir_name . '/' . $file);
                    }
                    else
                    {
                        // Удаляем файл
                        @unlink($dir_name . '/' . $file);
                    }
                }
            }

            rmdir($dir_name);
        }
    }

    /**
     * Проверка переменных на правильность
     *
     * @param string $var - проверяемая переменная
     * @param string $error_msg - текст ошибки
     * @param string $key_check - ключ проверки
     * @return mixed
     */
    protected function check_validation($var, $error_msg, $key_check)
    {
        $is_error = false;

        switch ($key_check)
        {
            case 'var_empty':
                $is_error = (!empty($var)) ? false : true;
                break;
            case 'file_not_found':
                $is_error = (file_exists($var)) ? false : true;
                break;
            case 'dir_not_found':
                $is_error = (is_dir($var)) ? false : true;
                break;
            case 'is_readable':
                $is_error = (is_readable($var)) ? false : true;
                break;
        }

        if ($is_error && empty($this->error_msg))
        {
            $this->error_msg = $error_msg;
        }
    }

    /**
     * Обработчик установки значений свойствам
     *
     * @param string $name - название свойства
     * @param mixed $value - значение свойства
     */
    public function __set($name, $value)
    {
        switch ($name)
        {
            case 'images_dir':
                $this->_images_dir = rtrim($value, '/') . '/';
                break;
        }
    }
}
