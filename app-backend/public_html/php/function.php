<?php

function printr($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die;
}

// микросекунды
function timer()
{
     $a = explode(" ",microtime());
     return (float)$a[0] + (float)$a[1];
}

// Поиск медианы в одномерном массиве
function median($array)
{
    sort($array);
    $array_length = count($array);

    if($array_length >= 2)
    {
        if ($array_length % 2)
        {
            $key = floor($array_length / 2);
            return $array[$key];
        }
        else
        {
            $right_key = $array_length / 2;
            $left_key = $right_key - 1;
            $result = ($array[$left_key] + $array[$right_key]) / 2;
            return $result;
        }
    }
    else if ($array_length == 1)
    {
        return $array[0];
    }
    else
    {
        return false;
    }
}

// человекочитаемый размер файла (bytes, Kb, Mb, Gb)
function filesize_human($size)
{
     if ($size < 1024) $new_size = "$size bytes";
     elseif ($size <= 1048576) $new_size = round($size / 1024, 0).' Kb';
     elseif ($size <= 1073741824) $new_size = round($size / 1048576, 2).' Mb';
     else $new_size = round($size / 1073741824, 3).' Gb';
     return $new_size;
}

// переименование существующего файла
function rename_existfile ($path, $prefix = null)
{
    if (file_exists($path))
    {
        $pinfo = pathinfo($path);
        $pinfo['basename'] = preg_replace("/^".$prefix."_/", "", $pinfo['basename']);
        $prefix = ($prefix) ? ++$prefix : 1;
        $new_path = $pinfo['dirname']."/".$prefix."_".$pinfo['basename'];

        return rename_existfile($new_path, $prefix);
    }
    else
    {
        return basename($path);
    }
}


// Image Magick
function image_magick($cmd)
{
    if (!preg_match("/;/", $cmd))
    {
        exec(IMAGE_MAGICK.$cmd);
        return 1;
    }
    else return 0;
}


// unicode escape
function unicode_escape($str)
{
	return preg_replace('/%u([0-9A-F]{4})/se', 'iconv("UTF-16BE", "UTF-8", pack("H4", "$1"))', $str);
}

// convert utf8
function utf_convert($str)
{
	return iconv("utf-8", "windows-1251", $str);
}

// validate url
function is_valid_url($url)
{
    return preg_match("/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,6}\/?.*$/i", $url);
}


// prepare string
function pstr($str, $mode = "none")
{
    $str = trim($str);
    switch ($mode)
    {
        case "text":
        	$str = htmlspecialchars($str, ENT_QUOTES);
        	break;

        case "none": break;
    }

    return $str;
}

// csv string explode
function csv_explode($str, $delimiter, $enclosure, $unicode = true)
{
    $out = array();
    $regexp = '/(^' . $enclosure . '.*?' . $enclosure . '(?=' . $delimiter . '))' .
        '|((?<=' . $delimiter . ')' . $enclosure . '.*?' . $enclosure . '(?=' . $delimiter . '))' .
        '|((?<=' . $delimiter . ')' . $enclosure . '.*?' . $enclosure . '$)' .
        '|(^[^' . $delimiter . ']*)' .
        '|(^[^' . $delimiter . ']*(?=' . $delimiter . '))' .
        '|((?<=' . $delimiter . ')[^' . $delimiter . ']*(?=' . $delimiter . '))' .
        '|((?<=' . $delimiter . ')[^' . $delimiter . ']*$)/is';
    if ($unicode)
    {
        $regexp .= 'u';
    }

    if (preg_match_all($regexp, $str, $matches))
    {
        foreach ($matches[0] as $word)
        {
            $word = trim($word, $enclosure);
            $out[] = $word;
        }
    }

    return $out;
}

// ----------------------------------
function stripslashes_for_array(&$arr)
{
   foreach($arr as $k=>$v)
   {
       if (is_array($v))
       {
           stripslashes_for_array($v);
           $arr[$k] = $v;
       }
       else
       {
           $arr[$k] = stripslashes($v);
       }
   }
}

function fix_magic_quotes_gpc()
{
   if (get_magic_quotes_gpc())
   {
       stripslashes_for_array($_POST);
       stripslashes_for_array($_GET);
       stripslashes_for_array($_COOKIE);
   }
}

// деактивирует при необходииости директиву "magic_quotes_gpc"
fix_magic_quotes_gpc();
// ----------------------------------

// Разбиение строки на атомы для поиска
function get_request_atoms($str)
{
    $atoms = array();
    if (preg_match_all('/([A-Z]+|[А-ЯЁ]+|[0-9]+)/uis', $str, $matches))
    {
        $atoms = $matches[0];
    }

    return $atoms;
}

// Разбиение строки на атомы для поиска, тестовый алгоритм
function get_request_atoms_2($str)
{
    $atoms = array();
    $str = mb_ereg_replace('\W', '', $str);
    $steps = mb_strlen($str) - 2;

    for ($_s = 0; $_s < $steps; $_s++)
    {
        $atoms[] = mb_substr($str, $_s, 3);
    }

    return $atoms;
}

// разбор на атомы с учетом кавычек
function get_request_atoms_quotes($str)
{
    $arr = preg_split("/\"/i", $str);

    $arr_cnt = count($arr);
    if ($arr_cnt > 1 && $arr_cnt % 2 == 0)
    {

        $arr[$arr_cnt - 2] .= " ".$arr[$arr_cnt - 1];
        unset($arr[$arr_cnt - 1]);
    }

    $atoms = array();
    foreach ($arr as $k => $value)
    {
        if ($k % 2 == 1)
        {
            array_push($atoms, $value);
        }
        else if (trim($value) != "") {
            $atoms = array_merge($atoms, get_request_atoms($value));
        }
    }

    return $atoms;
}

// encode для mod_rewrite
function my_urlencode($url)
{
    $url = urlencode($url);
    $url = str_replace("%2F", "%7Cr", $url);
    $url = str_replace("%5C", "%7Cl", $url);

    return  $url;
}

// decode для mod_rewrite
function my_urldecode($url)
{
    $url = str_replace("%7Cr", "%2F", $url);
    $url = str_replace("%7Cl", "%5C", $url);
    $url = urldecode($url);

    return  $url;
}

/////////////////////////////////////////


// болванка для вывода сообщение в админке
function adm_message($str)
{
     print "<div class=message>".$str."</div>";
}

// редирект javascript
function adm_redirect($url)
{
    print "<script language=javascript>document.location='/".$url."';</script>";
}

// Получаем значение счетчика пользовательской статистики
function get_users_stat_counter($user_id, $action, $date_from, $date_to, &$DB)
{
    if ($action == 'items_in_orders')
    {
        return $DB->GetOne(
            'SELECT
              COUNT(orders_items.id) AS cnt
            FROM
              orders_items
              INNER JOIN orders ON orders.id = orders_items.order_id
            WHERE
              orders_items.user_id = ?
              AND orders.date BETWEEN ? AND ?',
            array(
                $user_id,
                $date_from,
                $date_to
            )
        );
    }
    else
    {
        return $DB->GetOne('
            SELECT
              COUNT(id)
            FROM
              users_stat
            WHERE
              user_id = ?
              AND `action` = ?
              AND `date` BETWEEN ? AND ?
        ', array(
                $user_id,
                $action,
                $date_from,
                $date_to
            )
        );
    }
}

// Получаем реальный размер файла в байтах.
function filesize_byte($file) {
    $size = filesize($file);
    if ($size < 0)
        if (!(strtoupper(substr(PHP_OS, 0, 3)) == 'WIN'))
            $size = trim(`stat -c%s $file`);
        else{
            $fsobj = new COM("Scripting.FileSystemObject");
            $file = $fsobj->GetFile($file);
            $size = $file->Size;
        }
    return $size;
}

// Очищаем строку от непечатных символов
function clearstr($str){
    return preg_replace('/[^[:print:]а-яА-ЯёЁ]/u', '', $str);
}

// Удаляем старые файлы из папки
function remove_dir_overhead($dir_path, $max_dir_size)
{
    $return = false;

    $dir_path = rtrim($dir_path, '/\\') . '/';

    while (true)
    {
        $dir_files = array(
            'name' => array(),
            'size' => array(),
            'mtime' => array()
        );

        if ($dir_handle = opendir($dir_path))
        {
            $i = 0;
            while (false !== ($file = readdir($dir_handle)))
            {
                if ($file != '.' && $file != '..' && is_readable($dir_path . $file))
                {
                    $i++;

                    $dir_files['name'][$i] = $dir_path . $file;
                    $dir_files['size'][$i] = filesize_byte($dir_path . $file);
                    $dir_files['mtime'][$i] = filemtime($dir_path . $file);
                }
            }
            closedir($dir_handle);
        }
        else
        {
            $return = false;
            break;
        }

        $dir_size = array_sum($dir_files['size']);

        if ($dir_size < $max_dir_size)
        {
            $return = true;
            break;
        }

        asort($dir_files['mtime'], SORT_NUMERIC);
        reset($dir_files['mtime']);

        $i = key($dir_files['mtime']);

        if (is_null($i))
        {
            $return = false;
            break;
        }
        else
        {
            unlink($dir_files['name'][$i]);
        }
    }

    return $return;
}

// Делаем sleep при повышеной нагрузке на сервер
function sleep_overload($max_load = null)
{
    if (is_null($max_load))
    {
        if (defined('MAX_SERVER_LOAD'))
        {
            $max_load = MAX_SERVER_LOAD;
        }
        else
        {
            $max_load = 0.8;
        }
    }

    while(true)
    {
        if (function_exists('sys_getloadavg'))
            $load = sys_getloadavg();
        else
            $load[0] = 0;

        if ($load[0] >= $max_load)
        {
            sleep(1);
        }
        else
        {
            break;
        }
    }

    return true;
}

// Тестовые логи
function debug_log($msg)
{
    file_put_contents(PATH_ROOT.'/_log/debug.log', $msg, FILE_APPEND);
}

/**
 * Очистка текста от обрамляющих парных кавычек
 *
 * @param $title
 * @return string
 */
function clean_quotes($title)
{
    while (true)
    {
        $title = preg_replace('/^\'(.+)\'$/', '$1', $title);
        $title = trim($title);

        $title = preg_replace('/^\"(.+)\"$/', '$1', $title);
        $title = trim($title);

        if (!preg_match('/^\'(.+)\'$/', $title) && !preg_match('/^\"(.+)\"$/', $title))
        {
            break;
        }
    }

    return trim($title);
}