<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {randomizer} function plugin
 *
 * Type:     function<br>
 * Name:     randomizer<br>
 * Date:     April 23, 2004<br>
 * Purpose:  randomize through given values<br>
 * Input:
 *         - values = comma separated list of values to cycle,
 *                    or an array of values to cycle
 *                    (this can be left out for subsequent calls)
 *         - delimiter = the value delimiter, default is ","
 *         - assign = boolean, assigns to template var instead of
 *                    printed.
 *
 * Examples:<br>
 * <pre>
 * {randomize values="one,two,three,four,five,six,seven,eight,nine,ten"}
 * </pre>
 * @author Hermawan Haryanto <hermawan@codewalkers.com>
 * @version  1.0
 * @param array
 * @param Smarty
 * @return string|null
 */
function smarty_function_randomize ($params, &$smarty)
{
	static $cycle_vars;
	$print = (isset ($params['print'])) ? (bool)$params['print']:true;
	if (!in_array ('values', array_keys($params)))
	{
		if (!isset ($cycle_vars[$name]['values']))
		{
			$smarty->trigger_error ('randomize: missing \'values\' parameter');
			return;
		}
	}
	else
	{
		if (isset ($cycle_vars[$name]['values']) && $cycle_vars[$name]['values'] != $params['values']) $cycle_vars[$name]['index'] = 0;
		$cycle_vars[$name]['values'] = $params['values'];
	}
	$cycle_vars[$name]['delimeter'] = (isset ($params['delimeter'])) ? $params['delimeter']:',';
	if (is_array ($cycle_vars[$name]['values'])) $cycle_array = $cycle_vars[$name]['values'];
	else $cycle_array = explode ($cycle_vars[$name]['delimeter'], $cycle_vars[$name]['values']);

	srand ((float) microtime() * 10000000);
	$rand_keys = array_rand ($cycle_array, 2);
	$retval = $cycle_array[$rand_keys[0]];
	if (isset ($params['assign']))
	{
		$print = false;
		$smarty->assign($params['assign'], $retval);
	}

	if ($print) return $retval;
}

?>