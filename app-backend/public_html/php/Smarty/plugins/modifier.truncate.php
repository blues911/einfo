<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty truncate modifier plugin
 *
 * Type:     modifier<br>
 * Name:     truncate<br>
 * Purpose:  Truncate a string to a certain length if necessary,
 *           optionally splitting in the middle of a word, and
 *           appending the $etc string.
 * @link http://smarty.php.net/manual/en/language.modifier.truncate.php
 *          truncate (Smarty online manual)
 * @param string
 * @param integer
 * @param string
 * @param boolean
 * @return string
 */
function smarty_modifier_truncate($string, $length = 80, $etc = '...',
                                  $break_words = false)
{
    if ($length == 0)
        return '';

    if (function_exists('mb_strlen') && function_exists('mb_substr'))
    {
        if (mb_strlen($string, 'utf-8') > $length) {
            $length -= mb_strlen($etc, 'utf-8');
            if (!$break_words)
            {
                $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length+1, 'utf-8'));
            }

            return mb_substr($string, 0, $length, 'utf-8').$etc;
        }
        else
        {
            return $string;
        }
    }
    else
    {
    if (strlen($string) > $length) {
        $length -= strlen($etc);
        if (!$break_words)
            {
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));
            }

        return substr($string, 0, $length).$etc;
        }
        else
        {
        return $string;
}
    }

}

/* vim: set expandtab: */

?>
