<?php
/*
 * Smarty plugin
 * -----------------------------
 * ����:	modifier.smart_num.php
 * ���:		modifier
 * ���:		smart_num
 * ����������:	"�����" ����� ����� � ��������� ������
 * ------------------------------
 */

function smarty_modifier_smart_num($num)
{
    if (preg_match("/^[-0-9]+\.\d+$/", $num))
    {
        $result = preg_replace("/0*$/", "", (string)$num);
    	$result = preg_replace("/\.$/", "", $result);
    }
    else
    {
        $result = (string)$num;
    }

    return $result;
}

?>
