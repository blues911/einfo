<?php

$dir = __DIR__ . '/';

require_once($dir . 'XmlStringStreamer.php');
require_once($dir . 'XmlStringStreamer/ParserInterface.php');
require_once($dir . 'XmlStringStreamer/StreamInterface.php');
require_once($dir . 'XmlStringStreamer/Parser/StringWalker.php');
require_once($dir . 'XmlStringStreamer/Parser/UniqueNode.php');
require_once($dir . 'XmlStringStreamer/Stream/File.php');
require_once($dir . 'XmlStringStreamer/Stream/Stdin.php');
