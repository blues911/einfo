<?php

header("Content-type: text/html; charset=utf-8");

include_once("../../config.php");
include_once(PATH_BACKEND . "/php/function.php");
include_once(PATH_BACKEND . "/php/Smarty/Smarty.class.php");
include_once(PATH_BACKEND . "/php/adodb/adodb.inc.php");
include_once(PATH_BACKEND . "/php/classes/class.Price.php");
include_once(PATH_BACKEND . "/php/classes/class.AccessControl.php");
include_once(PATH_BACKEND . "/php/classes/class.Pager.php");
include_once(PATH_BACKEND . "/php/classes/class.MyDate.php");
include_once(PATH_BACKEND . "/php/classes/class.phpmailer.php");

// подключение к БД
$DB = ADONewConnection(DB_MAIN_DSN);
$DB->SetFetchMode(ADODB_FETCH_ASSOC);
$DB->Execute("SET NAMES utf8");
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;
//$DB->debug = true;

// подключение к БД (пользовательская)
$DB2 = ADONewConnection(DB_USERS_DSN);
$DB2->SetFetchMode(ADODB_FETCH_ASSOC);
$DB2->Execute('SET NAMES utf8');
$DB2->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB2->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB2->cacheSecs = DB_CACHE_TIME;
//$DB2->debug = true;

// контроль доступа
$control = new AccessControl($DB);

// дата
$date = new MyDate();

// шаблонизатор
$smarty = new Smarty;
$smarty->template_dir = PATH_BACKEND."/templates";
$smarty->compile_dir = PATH_TMP."/tpl-user";

// -----------------------------------------------

session_start();
$control->CheckSession();


if (isset($_GET['x']) && preg_match("/^([-a-z0-9_]+)\.(ajax_[-a-z0-9_]+)$/i", $_GET['x'], $matches))
{
    if ($matches[1] == "_staff" && $matches[2] == "ajax_clear_templates")
    {
        $smarty->clear_compiled_tpl();
    }
    elseif ($matches[1] == "_register" || $matches[1] == "_remember" || $matches[2] == "ajax_yandex_auth" || $control->IsAuth())
    {
        if ($matches[1] == "_register" || $matches[1] == "_remember" || $matches[2] == "ajax_yandex_auth" || $control->CheckModuleAccess($matches[1]))
        {
            if (file_exists("modules/".$matches[1]."/".$matches[2].".php"))
            {
                include_once (PATH_BACKEND . "/php/common/settings.php");
                $smarty->assign("settings", $SETTINGS);

                include_once ("modules/".$matches[1]."/".$matches[2].".php");
            }
            else print "Файл не существует.";
        }
        else print $control->GetMessage();
    }
    else print $control->GetMessage();
}
else print "Неверный запрос.";