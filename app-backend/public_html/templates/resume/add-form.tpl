<link href="templates/resume/style.css" rel="stylesheet" type="text/css" />

{literal}
<script>
function SubmitForm()
{
	filled = ['fio', 'speciality', 'city'];
	if (CheckFieldsForm(filled))
	{
		$('add_resume').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}
}

</script>
{/literal}


<h1>Добавление резюме</h1>

<form name="add_resume" id="add_resume" method="post" enctype="multipart/form-data" action="?module=resume&action=add">

	<table width="100%" border="0" cellspacing="0" cellpadding="7" id="resume">
	  <tr>
		<td width="150">* ФИО:</td>
		<td><input name="fio" id="fio" type="text" style="width:350px" /></td>
	  </tr>
	  <tr>
		<td>Пол:</td>
		<td><input name="sex" type="radio" value="m" checked /> мужской &nbsp;<input name="sex" type="radio" value="w" /> женский</td>
	  </tr>	  	
	  <tr>
		<td>* Специальность:</td>
		<td><input name="speciality" id="speciality" type="text" style="width:350px" /></td>
	  </tr>
	  <tr>
		<td>Опыт работы<br />по данной специальности:</td>
		<td>
			<select name="experience" id="experience">
				<option value="1">1 год</option>
				<option value="2">2 года</option>
				<option value="3">3 года</option>
				<option value="4">4 года</option>
				<option value="5">более 5 лет</option>
			</select>		
		</td>
	  </tr>
	  <tr>
		<td>Возраст:</td>
		<td><input name="age" id="age" type="text" style="width:40px" /></td>
	  </tr>	
	  <tr>
		<td>Образование:</td>
		<td>
			<select name="education" id="education">
				<option value="1">высшее</option>
				<option value="2">неполное высшее</option>
				<option value="3">среднее специальное</option>
				<option value="4">среднее</option>
				<option value="5">учащийся</option>
			</select>		
		</td>
	  </tr>	  
	  <tr>
		<td>* Город:</td>
		<td><input name="city" id="city" type="text" style="width:200px" /></td>
	  </tr>
	  <tr>
		<td>Место работы:</td>
		<td>
		<select name="workplace" id="workplace">
			<option value="0">не важно</option>
			<option value="1">работа в офисе</option>
			<option value="2">удаленная работа</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Тип работы:</td>
		<td>
		<select name="worktype" id="worktype">
			<option value="0">не важно</option>
			<option value="1">постоянная</option>
			<option value="2">разовая</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>График работы:</td>
		<td>
		<select name="workgraphic" id="workgraphic">
			<option value="0">не важно</option>
			<option value="1">полная занятость</option>
			<option value="2">неполная занятость</option>
			<option value="3">свободный</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Иностранные языки,<br />уровень владения:</td>
		<td><input name="language" id="language" type="text" style="width:200px" /></td>
	  </tr>	  
	  <tr>
		<td>Зарплата:</td>
		<td>от <input name="pay" id="pay" type="text" style="width:50px" /> у.е.</td>
	  </tr>
	  <tr>
		<td>Телефон:</td>
		<td><input name="contact_phone" id="contact_phone" type="text" style="width:200px" /></td>
	  </tr>	 
	  <tr>
		<td>Электронная почта:</td>
		<td><input name="contact_email" id="contact_email" type="text" style="width:200px" /></td>
	  </tr>	
	  <tr>
		<td>Текст резюме:</td>
		<td><textarea name="text" id="info" rows="10" style="width: 350px" ></textarea></td>
	  </tr> 	  	  	  	  	  	  	    
	</table>

	
	<br />
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button" ><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить резюме</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>	
  
</form>
 