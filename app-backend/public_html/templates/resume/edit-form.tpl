<link href="templates/resume/style.css" rel="stylesheet" type="text/css" />

{literal}
<script>
function SubmitForm()
{
	filled = ['fio', 'speciality', 'city'];
	if (CheckFieldsForm(filled))
	{
		$('edit_resume').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}
}

</script>
{/literal}


<h1>Редактирование резюме</h1>

<form name="edit_resume" id="edit_resume" method="post" enctype="multipart/form-data" action="?module=resume&action=edit&id={$content.id|escape}">

	<table width="100%" border="0" cellspacing="0" cellpadding="7" id="resume">
	  <tr>
		<td width="150">* ФИО:</td>
		<td><input name="fio" id="fio" type="text" style="width:350px" value="{$content.fio|escape}" /></td>
	  </tr>
	  <tr>
		<td>Пол:</td>
		<td><input name="sex" type="radio" value="m" {if $content.sex == "m"}checked{/if} /> мужской &nbsp;<input name="sex" type="radio" value="w" {if $content.sex == "w"}checked{/if} /> женский</td>
	  </tr>		
	  <tr>
		<td >* Название вакансии (специальность):</td>
		<td><input name="speciality" id="speciality" type="text" style="width:350px" value="{$content.speciality|escape}" /></td>
	  </tr>
	  <tr>
		<td>Опыт работы<br />по данной специальности:</td>
		<td>
			<select name="experience" id="experience">
				<option value="1" {if $content.experience == 1}selected{/if}>1 год</option>
				<option value="2" {if $content.experience == 2}selected{/if}>2 года</option>
				<option value="3" {if $content.experience == 3}selected{/if}>3 года</option>
				<option value="4" {if $content.experience == 4}selected{/if}>4 года</option>
				<option value="5" {if $content.experience == 5}selected{/if}>более 5 лет</option>
			</select>		
		</td>
	  </tr>
	  <tr>
		<td>Возраст:</td>
		<td><input name="age" id="age" type="text" style="width:40px" value="{$content.age|escape}" /></td>
	  </tr>	
	  <tr>
		<td>Образование:</td>
		<td>
			<select name="education" id="education">
				<option value="1" {if $content.education == 1}selected{/if}>высшее</option>
				<option value="2" {if $content.education == 2}selected{/if}>неполное высшее</option>
				<option value="3" {if $content.education == 3}selected{/if}>среднее специальное</option>
				<option value="4" {if $content.education == 4}selected{/if}>среднее</option>
				<option value="5" {if $content.education == 5}selected{/if}>учащийся</option>
			</select>		
		</td>
	  </tr>	  
	  <tr>
		<td>* Город:</td>
		<td><input name="city" id="city" type="text" style="width:200px" value="{$content.city|escape}" /></td>
	  </tr>
	  <tr>
		<td>Место работы:</td>
		<td>
		<select name="workplace" id="workplace">
			<option value="0" {if $content.workplace == 0}selected{/if}>не важно</option>
			<option value="1" {if $content.workplace == 1}selected{/if}>работа в офисе</option>
			<option value="2" {if $content.workplace == 2}selected{/if}>удаленная работа</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Тип работы:</td>
		<td>
		<select name="worktype" id="worktype">
			<option value="0" {if $content.worktype == 0}selected{/if}>не важно</option>
			<option value="1" {if $content.worktype == 1}selected{/if}>постоянная</option>
			<option value="2" {if $content.worktype == 2}selected{/if}>разовая</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>График работы:</td>
		<td>
		<select name="workgraphic" id="workgraphic">
			<option value="0" {if $content.workgraphic == 0}selected{/if}>не важно</option>
			<option value="1" {if $content.workgraphic == 1}selected{/if}>полная занятость</option>
			<option value="2" {if $content.workgraphic == 2}selected{/if}>неполная занятость</option>
			<option value="3" {if $content.workgraphic == 3}selected{/if}>свободный</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Иностранные языки,<br />уровень владения:</td>
		<td><input name="language" id="language" type="text" style="width:200px" value="{$content.language|escape}" /></td>
	  </tr>	  
	  <tr>
		<td>Зарплата:</td>
		<td>от <input name="pay" id="pay" type="text" style="width:50px" value="{$content.pay|escape}" /> у.е.</td>
	  </tr>
	  <tr>
		<td>Телефон:</td>
		<td><input name="contact_phone" id="contact_phone" type="text" style="width:200px" value="{$content.contact_phone|escape}" /></td>
	  </tr>	 
	  <tr>
		<td>Электронная почта:</td>
		<td><input name="contact_email" id="contact_email" type="text" style="width:200px" value="{$content.contact_email|escape}" /></td>
	  </tr>	
	  <tr>
		<td>Текст резюме:</td>
		<td><textarea name="text" id="text" rows="10" style="width: 350px">{$content.text|escape}</textarea></td>
	  </tr> 	  	  	  	  	  	  	    
	</table>

	
	<br />
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button" ><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
	</div>
  
</form>
 