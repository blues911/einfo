{* Smarty *}

<h1>Экспорт</h1>

<form id="export_file_form" method="post" action="/ajaxer.php?x=datasheet.ajax_export">
    <fieldset>

        <div class="form-item">
            * Категория:<br>
            <select name="category_id" style="width: 322px; ">
                <option value="1">КОРЕНЬ</option>
                {foreach from=$category_list item=category}
                    <option value="{$category.id|escape}">{""|indent:$category.c_level*4:"&nbsp;"}{$category.title|escape}</option>
                {/foreach}
            </select>
        </div>

        <div style="padding: 3px;">
            <label><input type="checkbox" name="without_pdf" value="1" />&nbsp;Без PDF-документации</label>
        </div>

        <div class="form-item">
            <button onclick="export_file_form.submit();" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Экспортировать</button>
        </div>

    </fieldset>
</form>