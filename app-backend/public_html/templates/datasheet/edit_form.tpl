{* Smarty *}

{literal}
    <script type="text/javascript">

        jQuery(document).ready(function(){

            jQuery('#add_datasheet input').keypress(function(eventObject)
            {
                if (eventObject.which == 13)
                {
                    SubmitForm();
                }
            });

        });

        function SubmitForm()
        {
            if (comp_canon.value == "")
            {
                alert('Не заполнено поле "Каноническое имя"');
                return false;
            }

            if (remote.value == "")
            {
                alert('Не заполнено поле "Ссылка на документацию в интернете"');
                return false;
            }

            $('add_datasheet').submit();
        }

    </script>
{/literal}

<h1>Редактирование документации</h1>

<form name="add_datasheet" id="add_datasheet" method="post" action="?module=datasheet&action=edit">

    <fieldset>

        <input type="hidden" name="comp_canon_old" value="{$datasheet.comp_canon|escape}" />

        <div style="position: relative;">

            <div class="form-item">
                * Каноническое имя:<br>
                <input type="text" id="comp_canon" name="comp_canon" style="width: 322px;" value="{$datasheet.comp_canon|escape}" />
            </div>

            <div class="form-item">
                * Ссылка на документацию в интернете:<br>
                <input type="text" id="remote" name="remote" style="width: 322px;" value="{$datasheet.remote|escape}" />
            </div>

        </div>

    </fieldset>

    <div class="form-item">
        <button onclick="SubmitForm();" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
        <button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>
    </div>

</form>