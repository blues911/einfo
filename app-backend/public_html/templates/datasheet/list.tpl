{* Smarty *}

{literal}
    <script type="text/javascript">

        function CheckAll()
        {
            var status = jQuery('#check_all').prop('checked');
            jQuery('#group_action input[name]').each(function()
            {
                jQuery(this).prop('checked', status);
            });
        }

    </script>
{/literal}

<h1>Документация</h1>


<div>
    <form name="search" id="search" method="get">

        <input type="hidden" name="module" value="datasheet" />
        <input type="hidden" name="action" value="list" />

        <div style="text-align: right;">
            <label style="margin: -20px 3px -3px 0; cursor: pointer;">
                <input type="checkbox" name="view_all" onchange="search.submit();" value="1" {if $view_all}checked{/if} /> показать все
            </label>
        </div>

        <div class="form-item" style="text-align: right;">
            Название: <input type="text" name="like" value="{$like|escape}" style="width: 150px;" />
            <input type="submit" value="Искать" style="width: 80px;" />
        </div>

    </form>
</div>

<form name="group_action" id="group_action" method="post" action="?module=datasheet&action=group_action">

<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">

    <tr>
        <th style="width: 40px;">№</th>
        <th style="width: 150px;">Каноническое имя</th>
        <th>Ссылка</th>
        <th style="width: 40px;"></th>
        <th style="width: 40px;">Ред.</th>
        <th style="width: 40px;"><input type="checkbox" id="check_all" onclick="CheckAll();" /></th>
    </tr>

    {foreach from=$datasheets item=datasheet}
        <tr>
            <td align="center">{$datasheet.num|escape}.</td>
            <td align="left"><a href="?module=datasheet&action=edit_form&comp_canon={$datasheet.comp_canon|escape}">{$datasheet.comp_canon|escape}</a></td>
            <td align="left"><a href="{$datasheet.remote|escape}" target="_blank">{$datasheet.remote|escape}</a></td>
            <td align="center">
                {if $datasheet.local}
                    <a href="{$datasheet.local_href|escape}" target="_blank"><img src="/i/pdf_16.png" title="Локальный файл"></a>
                {/if}
            </td>
            <td align="center">
                <a href="?module=datasheet&action=edit_form&comp_canon={$datasheet.comp_canon|escape}">
                    <img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" />
                </a>
            </td>
            <td align="center">
                <input type="checkbox" name="datasheet['{$datasheet.comp_canon|escape}']" value="1" />
            </td>
        </tr>
    {/foreach}

</table>

<div class="form-item" style="text-align: right; margin-top: 15px;">
    <button type="submit" name="do" value="delete" style="margin-right: 0;"><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Удалить</button>
</div>

</form>

{if $pager}
    {include file='pager.tpl'}
{/if}
