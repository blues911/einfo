{* Smarty *}

<h1>Импорт</h1>

<p style="color: red;">{if $import_result.error}Файл не выбран{/if}</p>
<p style="color: green;">{if $import_result.success}Импорт выполнен успешно{/if}</p>

<form id="import_file_form" method="post" enctype="multipart/form-data" action="?module=datasheet&action=import">

    <fieldset style="float: left;">

        <div>
            <input type="hidden" name="action_import" value="1" />

            <div class="form-item">
                <input type="file" name="import_file" />
            </div>

            <div class="form-item">
                <button onclick="import_file_form.submit();" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Импортировать</button>
            </div>
        </div>

    </fieldset>

    <div class="info" style="float: right; width: 330px;">
        Необходимо импортировать файл в формате CSV. Каждый компонент с новой строки. Колонки разделяются табуляцией.
        Первая колонка &mdash; название компонента. Вторая колонка &mdash; ссылка на файл документации в интернете.
    </div>

</form>
