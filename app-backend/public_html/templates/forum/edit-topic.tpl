{literal}
<script>

function SubmitForm()
{
	filled = ['title'];
	if (CheckFieldsForm(filled))
	{
		$('edit_topic').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}	
}
</script>
{/literal}


<h1>Редактирование темы</h1>

<form name="edit_topic" id="edit_topic" method="post" enctype="multipart/form-data" action="?module=forum&action=edit-topic&id={$content.id|escape}">
	
	<div class="form-item">
		* Тема сообщения:<br />
		<input name="title" id="title" type="text" style="width: 550px" value="{$content.title|escape}" />
	</div>
	
	<div class="form-item" style="margin-top: 20px">
		Текст сообщения:<br />
		<textarea name="text" id="text" rows="10" style="width: 550px" >{$content.text|escape}</textarea>
	</div>			
			
	<br />
	<br />
	
	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>						
	</div> 
	
  
</form>
 