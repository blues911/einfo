{literal}
<script>

function SubmitForm()
{
	filled = ['title'];
	if (CheckFieldsForm(filled))
	{
		$('add_topic').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}	
}
</script>
{/literal}


<h1>Добавление новой темы</h1>

<form name="add_topic" id="add_topic" method="post" enctype="multipart/form-data" action="?module=forum&action=add-topic">
	
	<div class="form-item">
		* Тема сообщения:<br />
		<input name="title" id="title" type="text" style="width: 550px" />
	</div>
	
	<div class="form-item" style="margin-top: 20px">
		Текст сообщения:<br />
		<textarea name="text" id="text" rows="10" style="width: 550px" ></textarea>
	</div>			
			
	<br />
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button" ><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить тему</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>		
  
</form>
 