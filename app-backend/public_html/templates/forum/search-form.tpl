<link href="templates/forum/style.css" rel="stylesheet" type="text/css" />
<script>

{literal}

Event.observe(window, 'load', function() {
	Event.observe('search', 'submit', function(evt) {
		Search();
		Event.stop(evt);
	}.bindAsEventListener(window));
});

function Search()
{
	if ($F('q').length >= 3) request = $F('q');
	else
	{
		alert("Поисковый запрос должен быть не менее 3-х символов");
		request = false;
	}
	
	if (request)
	{
		$('search_process').show();
		new Ajax.Request('ajaxer.php?x=forum.ajax_search', {
			 method: 'get',			 
			 parameters: { q: $F('q')},
			 onSuccess: function(transport)
			 {
			 	$('search_process').hide();
				$('search_result').update(transport.responseText);
			 }						 
		});
		
	}

	return false;			
}
 
</script>
{/literal}


<h1>Поиск по форуму</h1>

<form method="post" enctype="application/x-www-form-urlencoded" name="search" id="search" action="#" >
	<div  style="margin: 10px 0px 20px 0px">
		<input name="q" id="q" type="text" style="width: 400px" value="{$q|escape}" >&nbsp;&nbsp;&nbsp;<button onclick="Search()" type="button">Найти</button>
	</div>
</form>

<div class="spacer"></div>
<div align="center" id="search_process" style="display: none; margin: 10px 0px 10px 0px"><img src="i/icons/icon_process.gif" align="absmiddle" hspace="10" /> Идет поиск &#8230;</div>
<div id="search_result"></div>