<link href="templates/forum/style.css" rel="stylesheet" type="text/css" />

<h1>Форум</h1>


<table width="100%" border="1" cellpadding="8" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items" id="forum-list">
  <tr>
    <th style="text-align:left">Темы форума</th>
	<th width="50" style="text-align: center">Ответов</th>
    <th width="130" style="text-align: right">Последнее сообщ.</th>
  </tr>
  
  {foreach from=$list item=item}
  <tr valign="top" >
    <td>
		<div class="from">{$item.user_title|default:"[ удален ]"|escape}</div>
		<div class="title {if $item.pinned}pinned{/if}"><a href="?module=forum&action=view-topic&id={$item.id|escape}">{$item.title|cut_words:30|escape}</a> {if $admin_level AND $item.topic_update}<sup style="color: red">new</sup>{/if}</div>
		<div class="anot">{$item.text|cut_words:30|truncate:150|escape}</div>
	</td>
    <td align="center" class="nmess">{$item.nmess|escape}</td>
    <td align="right">
		<div class="date">{if $item.message_date}{$item.message_date|escape}{else}{$item.date|escape}{/if}</div>
	</td>
  </tr>
  {/foreach}

</table>

{include file="pager.tpl"}