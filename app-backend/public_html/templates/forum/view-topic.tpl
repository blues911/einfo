<link href="templates/forum/style.css" rel="stylesheet" type="text/css" />

{if $admin_level}
<script>
{literal}

function PinnedTopic(id)
{
	new Ajax.Request('ajaxer.php?x=forum_mod.ajax_pinned_topic', { 
				 method: 'post',
				 parameters: { id: id },
				 onSuccess: function(transport) {
					$('pinned-topic').update(transport.responseText == 1 ? 'раскрепить' : 'закрепить');
				 }
			});
}

function DeleteTopic(id)
{
	if(confirm('Вы действительно хотите удалить данный топик?'))
	{
		new Ajax.Request('ajaxer.php?x=forum_mod.ajax_delete_topic', { 
					 method: 'post',
					 parameters: { id: id },
					 onSuccess: function(c) {
						document.location = '?module=forum&action=list';
					 }
				});
	}
} 

function DeleteMessage(id)
{
	if(confirm('Вы действительно хотите удалить данное сообщение?'))
	{
		new Ajax.Request('ajaxer.php?x=forum_mod.ajax_delete_message', { 
					 method: 'post',
					 parameters: { id: id },
					 onSuccess: function(transport) {
						 $('message[' + id + ']').hide();
					 }
				});
	}
}

{/literal}
</script>
{/if}


<div class="forum-message forum-topic">
	
		<div class="func">
			{if $topic.allow_edit OR $admin_level}<a href="?module=forum&action=edit-topic-form&id={$topic.id|escape}">редактировать</a>{/if}
			{if $admin_level}
			 / <a href="javascript: DeleteTopic({$topic.id|escape})">удалить</a> /
			<a href="javascript: PinnedTopic({$topic.id|escape})" id="pinned-topic">{if $topic.pinned}раскрепить{else}закрепить{/if}</a>
			{/if}
		</div>
	
	<div class="from"><strong>{$topic.user_title|default:"[ удален ]"|escape}</strong> <span>&nbsp;/&nbsp; {$topic.date|escape}</span></div>
	<h1>{$topic.title|cut_words:30|escape}</h1>
	{$topic.text|cut_words:30|escape|nl2br}
</div>

{foreach from=$messages item=item}	
	<div class="forum-message" id="message[{$item.id|escape}]">
		<div class="func">
			{if $item.allow_edit OR $admin_level}<a href="?module=forum&action=edit-message-form&id={$item.id|escape}">редактировать</a>{/if}
			{if $admin_level} / <a href="javascript: DeleteMessage({$item.id|escape})">удалить</a>{/if}
		</div>
		
		<div class="from"><strong>{$item.user_title|default:"[ удален ]"|escape}</strong> <span>&nbsp;/&nbsp; {$item.date|escape}</span></div>
		{$item.text|cut_words:30|escape|nl2br}
	</div>
{/foreach}

{include file="pager.tpl"}

<div class="spacer"></div>

{literal}
<script>

function SubmitForm()
{
	filled = ['text'];
	if (CheckFieldsForm(filled))
	{
		$('add_message').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}	
}
</script>
{/literal}

<h3 style="color: #345CAF;">Написать ответ:</h3>
<form name="add_message" id="add_message" method="post" enctype="multipart/form-data" action="?module=forum&action=add-message">
	
	<input name="topic_id" id="topic_id"  type="hidden" value="{$topic.id|escape}" />
	
	<div class="form-item">
		* Текст сообщения:<br />
		<textarea name="text" id="text" rows="7" style="width: 550px" ></textarea>
	</div>			

	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button" ><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить сообщение</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>		
  
</form>
