{literal}
<script>

function SubmitForm()
{
	filled = ['text'];
	if (CheckFieldsForm(filled))
	{
		$('edit_message').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}	
}
</script>
{/literal}


<h1>Редактирование сообщения</h1>

<form name="edit_message" id="edit_message" method="post" enctype="multipart/form-data" action="?module=forum&action=edit-message&id={$content.id|escape}">
	
	<div class="form-item">
		Тема: <a href="?module=forum&action=view-topic&id={$content.topic_id|escape}"><strong>{$content.topic_title|escape}</strong></a>
	</div>
	
	<div class="form-item" style="margin-top: 20px">
		Текст сообщения:<br />
		<textarea name="text" id="text" rows="10" style="width: 550px" >{$content.text|escape}</textarea>
	</div>			
			
	<br />
	<br />
	
	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>						
	</div> 
	
  
</form>
 