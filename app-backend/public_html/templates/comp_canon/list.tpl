{* Smarty *}

{literal}
    <script type="text/javascript">

        function CheckAll()
        {
            var status = jQuery('#check_all').prop('checked');
            jQuery('#group_action input[name]').each(function()
            {
                jQuery(this).prop('checked', status);
            });
        }

    </script>
{/literal}

<h1>Каталог компонентов</h1>

<form name="search" id="search" method="get">
    <div style="margin: -20px 3px -5px 0; text-align: right;">
        <label style="cursor: pointer;">
            <input type="checkbox" onchange="search.submit();" name="view_all" value="1" {if $view_all}checked{/if} /> показать все
        </label>
    </div>
    <div>
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; vertical-align: top;">

                        <input type="hidden" name="module" value="comp_canon" />
                        <input type="hidden" name="action" value="list" />

                        <div class="form-item">
                            Категория:
                            <select id="form_category_id" name="category_id" onchange="search.submit();" style="width: 250px; ">
                                <option value="1" {if $category_id == 1}selected{/if}>КОРЕНЬ</option>
                                {foreach from=$category_list item=category}
                                    <option value="{$category.id|escape}" {if $category_id == $category.id}selected{/if}>{""|indent:$category.c_level*4:"&nbsp;"}{$category.title|escape}</option>
                                {/foreach}
                            </select>
                        </div>

                </td>
                <td style="text-align: right; vertical-align: top;">
                    <div class="form-item">
                        Имя: <input type="text" name="like" value="{$like|escape}" style="width: 150px;" />

                        <select name="adv_yandex">
                            <option value="-1">Яндекс.Директ</option>
                            <option value="0" {if $adv_yandex == 0}selected{/if}>Без рекламы</option>
                            <option value="1" {if $adv_yandex == 1}selected{/if}>С рекламой</option>
                        </select>

                        <select name="type">
                            <option value="-1">Тип привязки</option>
                            <option value="0" {if $type == 0}selected{/if}>Ручная</option>
                            <option value="1" {if $type == 1}selected{/if}>Автоматическая</option>
                        </select>

                        <input type="submit" value="Искать" style="width: 80px;" />
                    </div>
                </td>
            </tr>

        </table>
    </div>




</form>


<form name="group_action" id="group_action" method="post" action="?module=comp_canon&action=group_action">

<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">

    <tr>
        <th style="width: 40px;">№</th>
        <th>Каноническое имя</th>
        <th style="width: 200px;">Категория</th>
        <th style="width: 20px;"></th>
        <th style="width: 20px;"></th>
        <th style="width: 20px;"></th>
        <th style="width: 40px;">Ред.</th>
        <th style="width: 20px;"><input type="checkbox" id="check_all" onclick="CheckAll();" /></th>
    </tr>

    {foreach from=$list item=comp}
        <tr>
            <td align="center">{$comp.num|escape}.</td>
            <td align="left"><a href="?module=comp_canon&action=edit_form&id={$comp.id|escape}">{$comp.comp_canon|escape}</a></td>
            <td align="left">{$comp.category_title|escape}</td>
            <td align="center">
                {if $comp.adv_show}
                    <img src="/i/adv_16.png" title="Показывать рекламу">
                {/if}
            </td>
            <td align="center">
                {if $comp.datasheet_remote}
                    <a href="{$comp.datasheet_remote|escape}" target="_blank" title="Документация"><img src="/i/pdf_16.png"></a>
                {/if}
            </td>
            <td align="center">
                {if $comp.manual}
                    <span class="label-green" title="Ручная привязка">Р</span>
                {else}
                    <span class="label-orange" title="Автоматическая привязка">А</span>
                {/if}
            </td>
            <td align="center">
                <a href="?module=comp_canon&action=edit_form&id={$comp.id|escape}">
                    <img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" />
                </a>
            </td>
            <td align="center">
                <input type="checkbox" name="comp[{$comp.id|escape}]" value="1" />
            </td>
        </tr>
    {/foreach}

</table>

<div class="form-item" style="text-align: right; margin-top: 15px;">
    <button type="submit" name="do" value="adv_show" style="margin-right: 0;"><img src="i/icons/icon_pub-on.gif" width="15" height="15" hspace="5" align="absmiddle" />Выгрузить в директ</button>
    <button type="submit" name="do" value="adv_hide" style="margin-right: 0;"><img src="i/icons/icon_pub-off.gif" width="15" height="15" hspace="5" align="absmiddle" />Отменить выгрузку в директ</button>
    <button type="submit" name="do" value="delete" style="margin-right: 0;"><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Удалить</button>
</div>

</form>

{if $pager}
    {include file='pager.tpl'}
{/if}
