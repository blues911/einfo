{* Smarty *}

{literal}
    <script type="text/javascript">

        jQuery(document).ready(function(){

            jQuery('#add_comp input').keypress(function(eventObject)
            {
                if (eventObject.which == 13)
                {
                    SubmitForm();
                }
            });

        });

        function SubmitForm()
        {
            if (comp_canon.value == "")
            {
                alert('Не заполнено поле "Каноническое имя"');
                return false;
            }

            $('add_comp').submit();
        }

    </script>
{/literal}

<h1>Добавление компонента</h1>

<form name="add_comp" id="add_comp" method="post" enctype="multipart/form-data" action="?module=comp_canon&action=add">

    <fieldset>

        <legend>Свойства компонента</legend>

        <div style="position: relative; float: left;">

            <div class="form-item">
                * Категория:<br>
                <select id="category_id" name="category_id" style="width: 322px; ">
                    {foreach from=$category_list item=category}
                        <option value="{$category.id|escape}">{""|indent:$category.c_level*7-7:"&nbsp;"}{$category.title|escape}</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-item">
                * Каноническое имя:<br>
                <input type="text" id="comp_canon" name="comp_canon" style="width: 322px;" />
            </div>

            <div class="form-item">
                Документация (удаленный сервер):<br>
                <input type="text" id="datasheet_remote" name="datasheet_remote" style="width: 322px;" />
            </div>

        </div>

    </fieldset>

    <div class="form-item">
        <button onclick="SubmitForm();" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить компонент</button>
        <button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>
    </div>

</form>