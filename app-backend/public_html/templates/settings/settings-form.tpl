<link href="templates/settings/style.css" rel="stylesheet" type="text/css" />


<h1>Настройки системы</h1>

<form action="?module=settings&action=settings-save" method="post" name="settings" id="settings">

	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Курс валют и НДС</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >Курс валют</td>
				<td width="50%" >
					1$ = <input type="text" name="cur_rates:usd" value="{$settings.cur_rates.usd|escape}" style="width: 40px"> руб.
					&nbsp;&nbsp;&nbsp;&nbsp;
					&euro; 1 = <input type="text" name="cur_rates:eur" value="{$settings.cur_rates.eur|escape}" style="width: 40px"> руб.
				</td>
			  </tr>
                <tr>
                    <td width="50%" class="set">НДС</td>
                    <td width="50%"><input type="text" name="nds:value" value="{$settings.nds.value|escape}" /></td>
                </tr>
			</table>
		</div>
	</div>

	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Каталог (прайсы)</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >Максимальное количество компонентов на одной странице</td>
				<td width="50%" ><input type="text" name="catalog:max_comp_on_page" value="{$settings.catalog.max_comp_on_page|escape}"></td>
			  </tr>	  
			  <tr>
				<td class="set">Максимальный размер прайс-листа предприятия, при котором он показывается полностью</td>
				<td ><input type="text" name="catalog:max_show_all" value="{$settings.catalog.max_show_all|escape}"></td>
			  </tr>
			</table>
		</div>
	</div>
	
	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Поиск</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
                <tr>
                    <td width="50%" class="set" >Максимальное количество компонентов на странице поиска</td>
                    <td width="50%"><input type="text" name="search:max_on_page" value="{$settings.search.max_on_page|escape}"></td>
                </tr>
                <tr>
                    <td width="50%" class="set">Количество компонентов возрвращаемых Sphinx</td>
                    <td width="50%"><input type="text" name="search:sphinx_limit" value="{$settings.search.sphinx_limit|escape}"></td>
                </tr>
			</table>
		</div>
	</div>


	
	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Предприятия</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >Максимальное количество предприятий<br />на странице раздела</td>
				<td width="50%" ><input type="text" name="users:max_on_page" value="{$settings.users.max_on_page|escape}"></td>
			  </tr>
			  <tr>
				<td class="set" >Количество последних новостей<br />в визитке предприятия</td>
				<td><input type="text" name="users:last_news" value="{$settings.users.last_news|escape}"></td>
			  </tr>	
			  <tr>
				<td class="set" >Количество последних вакансий<br />в визитке предприятия</td>
				<td><input type="text" name="users:last_vacancy" value="{$settings.users.last_vacancy|escape}"></td>
			  </tr>				                	  	  	  
			</table>
		</div>
	</div>
		

	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Новости</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set">Форматы изображений, разрешенные для загрузки</td>
				<td width="50%" ><input type="text" name="news:img_allow_formats" value="{$settings.news.img_allow_formats|escape}"></td>
			  </tr>
			  <tr>
				<td class="set" >Максимальное количество ссылок в тексте новости</td>
				<td ><input type="text" name="news:count_links" value="{$settings.news.count_links|escape}"></td>
			  </tr>	
			  <tr>
				<td class="set" >Максимальная ширина картинки (в аннотации)</td>
				<td >ширина <input type="text" name="news:thumbnail_max_width" value="{$settings.news.thumbnail_max_width|escape}" style="width: 50px"> px</td>
			  </tr>				  
			  <tr>
				<td class="set" >Максимальный размер картинок для загрузки (галерея)</td>
				<td >ширина <input type="text" name="news:img_max_width" value="{$settings.news.img_max_width|escape}" style="width: 50px"> px, высота <input type="text" name="news:img_max_height" value="{$settings.news.img_max_height|escape}" style="width: 50px"> px</td>
			  </tr>	
			  <tr>
				<td class="set" >Тип модерации</td>
				<td ><input name="news:status_mode" type="radio" value="pre" {if $settings.news.status_mode == "pre"}checked{/if} /> премодерация &nbsp;&nbsp; <input name="news:status_mode" type="radio" value="post" {if $settings.news.status_mode == "post"}checked{/if} /> постмодерация</td>
			  </tr>	
			  <tr>
				<td class="set">Максимальное количество новостей<br />на странице раздела</td>
				<td ><input type="text" name="news:max_on_page" value="{$settings.news.max_on_page|escape}"></td>
			  </tr>
			  <tr>
				<td class="set" >Максимальное количество новостей<br />на главной странице</td>
				<td ><input type="text" name="news:max_on_indexpage" value="{$settings.news.max_on_indexpage|escape}"></td>
			  </tr>				  		  	  	  
			</table>
		</div>
	</div>
	
	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Вакансии</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >Максимальное количество вакансий<br />на странице раздела</td>
				<td width="50%" ><input type="text" name="vacancy:max_on_page" value="{$settings.vacancy.max_on_page|escape}"></td>
			  </tr>	              	  	  	  
			</table>
		</div>
	</div>
	
	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Резюме</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >Максимальное количество резюме<br />на странице раздела</td>
				<td width="50%" ><input type="text" name="resume:max_on_page" value="{$settings.resume.max_on_page|escape}"></td>
			  </tr>	              	  	  	  
			</table>
		</div>
	</div>

	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Баннеры пользователей</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set">Форматы баннеров, разрешенные для загрузки</td>
				<td width="50%" ><input type="text" name="users_banners:allow_formats" value="{$settings.users_banners.allow_formats|escape}"></td>
			  </tr>
			  <tr>
				<td class="set">Допустимые размеры баннеров</td>
				<td ><input type="text" name="users_banners:allow_sizes" value="{$settings.users_banners.allow_sizes|escape}" style="width: 90%"></td>
			  </tr>
			</table>
		</div>
	</div>
	
	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Новости системы</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >Максимальное количество новостей<br />на странице раздела</td>
				<td width="50%" ><input type="text" name="sysnews:max_on_page" value="{$settings.sysnews.max_on_page|escape}"></td>
			  </tr>	              	  	  	  
			</table>
		</div>
	</div>		
	
	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Форум поддержки</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >Максимальное количество тем<br />на одной странице</td>
				<td width="50%" ><input type="text" name="forum:topics_on_page" value="{$settings.forum.topics_on_page|escape}"></td>
			  </tr>	 
			  <tr>
				<td class="set" >Максимальное количество сообщений<br />на одной странице</td>
				<td ><input type="text" name="forum:messages_on_page" value="{$settings.forum.messages_on_page|escape}"></td>
			  </tr>				               	  	  	  
			</table>
		</div>
	</div>	
	
	<div class="settings">
		<div class="settings-header">
			<div class="settings-title">Буферные таблицы</div>
		</div>
		<div class="settings-params">
			<table width="100%" id="base" border="0" cellspacing="0" cellpadding="8" >
			  <tr>
				<td width="50%" class="set" >LAST BUFFER UPDATE</td>
				<td width="50%" ><input type="text" name="last_buff_update" value="{$settings.last_buff_update|escape}"></td>
			  </tr>	              	  	  	  
			</table>
		</div>
	</div>		

	

	<br />	
	<div align="center">
		<button type="submit"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить настройки</button></td>
	</div>
    	
	
</form>
