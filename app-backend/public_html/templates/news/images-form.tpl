<script src="templates/news/script.js" type="text/javascript"></script>
<link href="templates/news/style.css" rel="stylesheet" type="text/css" />

{literal}
<script>
    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    jQuery('#preview_upload_img').attr('src', e.target.result).show();
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    jQuery(document).ready(function()
    {
        document.getElementById('upload_img').addEventListener('change', handleFileSelect, false);
    });
</script>
{/literal}

<h1>Загрузить картинку в галерею</h1>

<div class="form-item">
	<form action="?module=news&action=image-upload" id="upload" name="upload" method="post" enctype="multipart/form-data">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="110" valign="top" ><img src="i/no_image.jpg" width="92" id="preview_upload_img"/></td>
			<td valign="middle">
				<div>Загрузить изображение:<br /><input name="upload_img" id="upload_img" type="file" style="width: 60%" /></div>
				<div>Название (комментарий):<br /><input name="upload_title" id="upload_title" type="text" style="width: 80%"  /></div>
			</td>
		  </tr>
		  <tr>
			<td width="110" valign="top" >&nbsp;</td>
			<td valign="middle" style="padding-bottom: 10px">
				<div class="prompt" style="margin-bottom: 15px">
					Разрешается загружать изображения только следующих форматов &mdash; {$img_allow_formats|upper|escape}.<br />
					Максимальный размер по ширине &mdash; {$img_max_width|escape}px, по высоте &mdash; {$img_max_height|escape}px.
				</div>
				<button onClick="SubmitUploadForm()" type="button"><img src="i/icons/btn_upload.gif" width="15" height="15" hspace="5" align="absmiddle" />Загрузить</button>
			</td>
		  </tr>	  
		</table>
	</form>	
</div>

<div class="spacer"></div>

<h1>Список загруженных</h1>

<table width="100%" border="0" id="images_list">
	{foreach from=$list item=item name=imgs}
		{if $smarty.foreach.imgs.first}<tr>{/if}
			<td>
				<div id="item[{$item.id|escape}]">
					<a href="?module=news&action=image-geturl&id={$item.id|escape}"><img src="{$item.img|escape}" width="90" alt="{$item.title|escape}" title="{$item.title|escape}" class="img_item" /></a>
					<div>{$item.title|escape}</div>
					<span><a href="?module=news&action=image-geturl&id={$item.id|escape}">Получить URL</a>&nbsp;|&nbsp;<a href="javascript: DeleteImg({$item.id|escape})" ><img src="i/icons/btn_cancel.gif" width="15" height="15" alt="Удалить" title="Удалить" align="absmiddle" style="border: 0px;" /></a></span>
				</div>
			</td>
		{if  $smarty.foreach.imgs.iteration % 4 == 0  AND !$smarty.foreach.imgs.last}
			</tr><tr>		
		{elseif $smarty.foreach.imgs.last}
			</tr>		
		{/if}
	{/foreach}
</table>


{include file="pager.tpl"}