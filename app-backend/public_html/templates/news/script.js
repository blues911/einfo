// submits
function SubmitUploadForm()
{
	if ($F('upload_img') != "")
	{
		$('upload').submit();		
	}
	else alert("Выберите файл для загрузки");
}

function SubmitNewsForm(form)
{
	filled = ['title'];
	if (CheckFieldsForm(filled))
	{
		tmp = tinyMCE.get('text').getContent().sub(/<\/a>/, '', max_links);
		if (tmp.match(/<\/a>/))
		{
			alert('Количество ссылок в тексте новости превышает допуcтимый предел (максимум ' + max_links + ')');
		}
		else $(form).submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");
	}

}

///////////////////////////////////////////////

function DeleteImg(id)
{
	if(confirm('Вы действительно хотите удалить данное изображение?'))
	{

		new Ajax.Request('ajaxer.php?x=news.ajax_deleteimg', { 
					 method: 'post',
					 parameters: { id: id },
					 onSuccess: function(transport) {
						 if (transport.responseText == 'OK') $('item[' + id + ']').hide();
						 else alert("Доступ запрещен");
					 }
				});
	}	
}

function DeleteNews(id, title)
{
	if(confirm('Вы действительно хотите удалить новость "' + jQuery('<p />').html(title).text() + '"?'))
	{
		document.location = '?module=news&action=delete&id=' + id;
	}	
}

