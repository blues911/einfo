<link href="templates/news/style.css" rel="stylesheet" type="text/css" />
<script src="templates/news/script.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	EnableEditor('text');
	max_links = {$count_links|escape};

    {literal}
    jQuery(document).on('click', '.img_preview img', function()
    {
        var src = jQuery(this).attr('src');
        var title = jQuery(this).attr('title');

        tinyMCE.execCommand('mceInsertRawHTML',false,'<img title="' + title + '" src="' + src + '">');
    });

    jQuery(document).on('input', '.img_preview_filter input:first', function()
    {
        var title = String(jQuery('.img_preview_filter input:first').val()).toLowerCase();

        if (title.length)
        {
            jQuery('.img_preview img').each(function()
            {
                var img = jQuery(this).parents('.thumb');
                var img_title = String(jQuery(this).attr('title')).toLowerCase();

                if (img_title.indexOf(title) > -1)
                {
                    img.show();
                }
                else
                {
                    img.hide();
                }
            });
        }
        else
        {
            jQuery('.img_preview img').parents('.thumb').show();
        }
    });

    jQuery(document).on('submit', '#add_news', function()
    {
        var error = false;
        jQuery(this).find('#title, #text').each(function()
        {
            if (String(jQuery(this).val()).trim().length == 0)
            {
                error = true;
                return false;
            }
        });

        if (error)
        {
            alert('Необходимо заполнить все поля.');
            return false;
        }

        if (String(jQuery(this).find('#img').val()).trim().length == 0)
        {
            alert('Необходимо выбрать изображение.');
            return false;
        }

        var link_regex = /<\/a>/;
        var text = String(jQuery(this).find('#text').val()).sub(link_regex, '', max_links);
        if (link_regex.test(text))
        {
            alert('Количество ссылок в тексте новости превышает допуcтимый предел (максимум ' + max_links + ')');
            return false;
        }

        return true;
    });

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    jQuery('#image_text').hide();
                    jQuery('#image_preview').attr('src', e.target.result).show();
                    jQuery('#image_click').show();
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    jQuery(document).ready(function()
    {
        document.getElementById('img').addEventListener('change', handleFileSelect, false);
    });

    {/literal}

</script>

<h1>Добавление новости</h1>

<form name="add_news" id="add_news" method="post" enctype="multipart/form-data" action="?module=news&action=add">

    <div class="form-item" style="position: relative;">
        <div class="image_upload">
            <label>
                <div id="image_text" class="header">Выбрать<br>изображение (обязательно)</div>
                <input id="img" name="img" type="file" style="display: none;">
                <img id="image_preview" src="" style="display: none; width: 200px;">
                <span id="image_click" style="display: none;">
                    Нажмите чтобы выбрать другое изображение.
                </span>
            </label>
            <div style="padding: 6px;">
                Данное изображение отображается сбоку от анонса новости на главной странице и в списке новостей.<br>
                Формат &mdash; JPG, GIF, PNG
            </div>
        </div>
        <div style="position: absolute; left: 220px; top: 0; right: 6px;">
            Заголовок (обязательно):<br />
            <textarea name="title" id="title" style="width: 100%; resize: vertical;"></textarea>
        </div>
    </div>

    <div class="form-item" style="position: relative; margin-top: 20px;">
        <div>
            Полный текст новости (обязательно, можно вставлять картинки из галереи ниже):<br />
            <textarea name="text" id="text" rows="20" style="width: 675px" ></textarea>
            <div class="prompt">Максимальное количество ссылок в тексте новости &mdash; {$count_links|escape}</div>
            
        </div>

        <div style="margin-top: 20px">Перетащите мышкой картинку отсюда в текст новости</div>
        <div class="img_preview_container">       
            
            <div class="img_preview_filter">
                <input type="text" placeholder="Фильтр изображений" />
            </div>
            <div class="img_preview">
                <div>
                {if $image_list}
                    {foreach from=$image_list item=image}
                        <div class="thumb">
                            <div>
                                <img title="{$image.title|escape}" src="{$image.img|escape}">
                                {if $image.title}
                                    <div>{$image.title|escape}</div>
                                {/if}
                            </div>
                        </div>
                    {/foreach}
                {else}
                    <p style="margin: 8px;">
                        Для использования картинок в тексте новости необходимо их предварительно
                        <a href="?module=news&action=images-form">загрузить в галерею</a>.
                    </p>
                {/if}
                </div>
            </div>
        </div>

    </div>

    <br />

    <div class="form-item">
        <button type="submit" ><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить новость</button>
        <button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>
    </div>

    {if $status_mode == "pre"}
        <div class="form-item" style="margin-top: 30px">
            <fieldset class="attention">
                <legend>&nbsp;Внимание&nbsp;</legend>
                <div style="width: 40px; float: left">
                    <img src="i/icons/attention.gif" alt="Внимание" width="20" height="18" hspace="8" vspace="8" />
                </div>
                <div style="margin: 5px 0px 0px 40px">
                    В разделе &laquo;Новости&raquo; действует система модерации. Это значит, что ваша новость появится на сайте только после проверки модератором.
                </div>
            </fieldset>
        </div>
    {/if}

</form>