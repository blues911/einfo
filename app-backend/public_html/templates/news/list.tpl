<link href="templates/news/style.css" rel="stylesheet" type="text/css" />
<script src="templates/news/script.js" type="text/javascript"></script>


<h1>Новости</h1>


<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">
  <tr align="center">
    <th width="5%">№</th>
    <th width="45%">Заголовок</th>
    <th width="25%">Дата
	{if $list_order == "desc"}
	<a href="?module=news&action=list&order=asc"><img src="i/icons/sort_desc.gif" alt="Сортировка по убыванию" width="9" height="5" hspace="5" border="0" />
	{else}
	<a href="?module=news&action=list&order=desc"><img src="i/icons/sort_asc.gif" alt="Сортировать по возрастанию" width="9" height="5" hspace="5" border="0" />
	{/if}
	</a></th>
    <th width="100">Статус</th>
    <th width="30">Ред.</th> 	
    <th width="30">Удал.</th>
  </tr>
  {foreach from=$list item=news}
  <tr valign="top" >
    <td align="center">{$news.num|escape}.</td>
    <td><a href="?module=news&action=edit-form&id={$news.id|escape}">{$news.title|escape}</a></td>
    <td align="center">{$news.date|escape}</td>
    <td align="center" style="font: 10px Tahoma; line-height: 1em; ">
    
		{if $news.status == 1}
			<span style="color: green">опубликована</span>
		{elseif $news.status == -1}
			<span style="color: red">отклонена</span>
		{else}
			<span style="color: blue">ожидает проверки</span>
		{/if}    

	</td>
	<td align="center"><a href="?module=news&action=edit-form&id={$news.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center"><a href="#" onClick="DeleteNews({$news.id|escape}, '{$news.title|escape}')"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a></td>
  </tr>
  {/foreach}

</table>

{include file="pager.tpl"}