<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>EINFO: вход в панель управления</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="i/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	
	<table border="0" align="center" cellpadding="0" cellspacing="0" id="enter">
	  <tr>
		<td id="header-enter"><img src="i/logo.gif" width="315" height="64" alt="Вход в панель администратора"/></td>
	  </tr>
	  <tr>
		<td >
			<form action="/" method="post" name="auth">
			  <input name="auth" type="hidden" value="1"/>
			  <table width="100%"  border="0" cellspacing="0" cellpadding="4">
				<tr>
				  <td width="22%" align="right">Логин</td>
				  <td width="44%"><input name="login" type="text" tabindex="1"/></td>
				  <td width="34%"  align="center" valign="middle">
                      <button type="submit" tabindex="3" style="width: 100px" >
                          Войти <img src="i/icons/icon_key.gif" width="18" height="18" align="absmiddle" style="margin-left: 6px" />
                      </button>
                  </td>
				</tr>
				<tr>
				  <td align="right">Пароль</td>
				  <td><input name="password" type="password" tabindex="2"/></td>
                  <td><a href="?remember" class="enter-link" style="color: gray">забыли пароль?</a></td>
				</tr>
				<tr>
				  <td></td>
				  <td><input name="remember" id="remember" type="checkbox" value="1" /> запомнить меня</td>
				  <td align="left"><a href="http://www.einfo.ru/about/" target="_blank" class="enter-link">Регистрация <span>&rarr;</span></a></td>
				</tr>				
			  </table>
			  <br />
			</form>	
		</td>
	  </tr>
	</table>
	
	{if $auth_message}<div id="auth-message">{$auth_message|escape}</div>{/if}


</body>
</html>
