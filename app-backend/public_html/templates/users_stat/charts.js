
AmCharts.dayNames = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
AmCharts.shortDayNames = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];
AmCharts.monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
AmCharts.shortMonthNames = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];

function makeChart(div, user_id, action, date_from, date_to)
{
    var div_height = jQuery('#' + div).css('height');
    var div_style = '"height: ' + div_height + '; line-height: ' + div_height + '; font-size: 12pt; color: #ccc;"';

    var html_error = '<p style=' + div_style + '>Ошибка загрузки графика</p>';
    var html_short_period = '<div style=' + div_style + '>Выбран слишком короткий период для построения графика</div>';

    var success = false;
    jQuery.ajax({
        url: '/ajaxer.php?x=users_stat.ajax_chart',
        type: 'POST',
        data: {
            user_id: user_id,
            action: action,
            date_from: date_from,
            date_to: date_to
        },
        success: function(response)
        {
            if (response != 'error' && response != 'short_period')
            {
                var data = eval(response);
                AmCharts.makeChart(div, {
                    "type": "serial",
                    "theme": "light",
                    "pathToImages": "/i/js/amcharts/images/",
                    "dataDateFormat": "YYYY-MM-DD",
                    "valueAxes": [{
                        "axisAlpha": 0,
                        "position": "left"
                    }],
                    "graphs": [{
                        "id": "g1",
                        "bullet": "round",
                        "bulletBorderAlpha": 1,
                        "bulletColor": "#FFFFFF",
                        "bulletSize": 5,
                        "hideBulletsCount": 50,
                        "lineThickness": 2,
                        "fillAlphas": 0.5,
                        "title": "red line",
                        "useLineColorForBulletBorder": true,
                        "valueField": "value"
                    }],
                    "chartCursor": {
                        "cursorPosition": "mouse",
                        "pan": true,
                        "categoryBalloonDateFormat": "DD MMM YYYY"
                    },
                    "categoryField": "date",
                    "categoryAxis": {
                        "parseDates": true,
                        "dashLength": 1,
                        "minorGridEnabled": true,
                        "position": "top",
                        "dateFormats": [
                            {period:'fff',format:'JJ:NN:SS'},
                            {period:'ss',format:'JJ:NN:SS'},
                            {period:'mm',format:'JJ:NN'},
                            {period:'hh',format:'JJ:NN'},
                            {period:'DD',format:'DD MMM'},
                            {period:'WW',format:'DD MMM'},
                            {period:'MM',format:'MMMM'},
                            {period:'YYYY',format:'YYYY'}
                        ]
                    },
                    exportConfig:{
                        menuRight: '20px',
                        menuBottom: '50px',
                        menuItems: [{
                            icon: '/i/js/amcharts/images/export.png',
                            format: 'png'
                        }]
                    },
                    "dataProvider": data
                });
                jQuery('#' + div).css('background-color', '#ffffff');
            }
            else
            {
                if (response == 'short_period')
                {
                    jQuery('#' + div).html(html_short_period);
                }
                else
                {
                    jQuery('#' + div).html(html_error);
                }
            }
        },
        error: function()
        {
            jQuery('#' + div).html(html_error);
        }
    });
}