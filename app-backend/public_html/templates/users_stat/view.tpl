{* Smarty *}

<script type="text/javascript" src="/i/js/amcharts/amcharts.js"></script>
<script type="text/javascript" src="/i/js/amcharts/serial.js"></script>
<script type="text/javascript" src="/i/js/amcharts/themes/light.js"></script>

<script type="text/javascript" src="/templates/users_stat/charts.js"></script>

<script type="text/javascript">

var user_id = {$user_id|escape};

{literal}

    jQuery(document).ready(function()
    {
        jQuery('input.datepicker').Zebra_DatePicker({
            days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            show_select_today: 'Сегодня',
            lang_clear_date: 'Очистить',
            format: 'd.m.Y',
            readonly_element: false,
            view: 'months',
            onSelect: function()
            {
                jQuery('#search_period select:first').val(0);
            }
        });

        jQuery('input.datepicker').on('change', function()
        {
            jQuery('#search_period select:first').val(0);
        });

        // Графики
        makeChart('items_in_orders', user_id, 'items_in_orders', jQuery('#date_from').val(), jQuery('#date_to').val());
        makeChart('profile_view', user_id, 'profile_view', jQuery('#date_from').val(), jQuery('#date_to').val());
        makeChart('profile_click_site', user_id, 'profile_click_site', jQuery('#date_from').val(), jQuery('#date_to').val());
        //makeChart('profile_click_email', user_id, 'profile_click_email', jQuery('#date_from').val(), jQuery('#date_to').val());

        // Выбор периода
        jQuery('#search_period select').on('change', function()
        {
            if (jQuery('#search_period select:first').val() > 0)
            {
                jQuery('#search_period').submit();
            }
        });
    });

</script>

{/literal}

<h1>Пользовательская статистика</h1>

<div align="right" style="margin: -20px 0px 10px 0px">

    <div style="float: left;">
        <form id="search_period" method="post" action="?module=users_stat&action=view">
            <select name="period" style="position: relative; top: 13px;">
                <option value="0" {if $search_period == 0}selected{/if}>период</option>
                <option value="7" {if $search_period == 7}selected{/if}>7 дней</option>
                <option value="30" {if $search_period == 30}selected{/if}>30 дней</option>
                <option value="90" {if $search_period == 90}selected{/if}>90 дней</option>
                <option value="365" {if $search_period == 365}selected{/if}>365 дней</option>
            </select>
        </form>
    </div>

    <div style="float: right;">
        <form name="search" id="search" method="post" action="?module=users_stat&action=view" >

            <p>
                <strong>Период: </strong>
                <input type="text" class="datepicker" id="date_from" name="date_from" data-last-value="{$date_from|escape}" value="{$date_from|escape}" style="width: 100px;" />
                <input type="text" class="datepicker" id="date_to" name="date_to" data-last-value="{$date_to|escape}" value="{$date_to|escape}" style="width: 100px;" />
                <input type="submit" value="Применить" />
            </p>

        </form>
    </div>

</div>

<table class="stat__container">
    <tr>
        <td>
            <div class="stat__counter">
                <p class="stat__counter--value">{$items_in_orders|escape}</p>
                <p class="stat__counter--descr">количество заказанных позиций</p>
            </div>
        </td>
        <td>
            <div id="items_in_orders" class="stat__chart">
                <img src="/i/ajax_loader.gif">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="stat__counter">
                <p class="stat__counter--value">{$profile_view|escape}</p>
                <p class="stat__counter--descr">количество просмотров профиля компании в поиске</p>
            </div>
        </td>
        <td>
            <div id="profile_view" class="stat__chart">
                <img src="/i/ajax_loader.gif">
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="stat__counter">
                <p class="stat__counter--value">{$profile_click_site|escape}</p>
                <p class="stat__counter--descr">количество кликов на адрес сайта</p>
            </div>
        </td>
        <td>
            <div id="profile_click_site" class="stat__chart">
                <img src="/i/ajax_loader.gif">
            </div>
        </td>
    </tr>
</table>