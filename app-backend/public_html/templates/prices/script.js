/* upload-form */

var uploadID = null;
var submitForm = false;

// обработка действий склад/на заказ
function ChangePrice()
{
    $('stock_info').show();
}

// отображение полей в зависимости от типа файла
var xml_format = 0;
function FileFormat()
{
	if ($F('price').match(/\.xml$/))
	{
		$('tblrow_3', 'tblrow_4', 'tblrow_5').invoke('hide');
		if ($('tblrow_2')) $('tblrow_2').hide();
		xml_format = 1;
	}
	else
	{
		$('tblrow_3', 'tblrow_4', 'tblrow_5').invoke('show');
		if ($('tblrow_2')) $('tblrow_2').show();
		xml_format = 0;
	}
	
	UpdateLegend();
}


// формирование подсказки "имя файла"
function UpdateLegend()
{
	var legend = ['', '', '', '', ''];
	
	legend[0] = id;
	legend[2] = GetCheckedValue($('upload_price').currency);
	
	if ($F('price') != '')
	{
		legend[4] = $F('price').split('.').last();
	}								  
	
	legend[3] = ($F('stock_hide')) ? 'stock_hide' : 'stock';
	
	legend[1] = 'replace';
	
	$('price_legend_name').update(legend.join('.'));	
}


// загрузка файла
function Upload()
{
	if (!submitForm)
	{
		var success = false;
		
		if (!GetCheckedValue($('upload_price').price_type) && !xml_format)
		{
			alert("Не выбран тип загружаемых позиций");			
		}
		else if ($F('price') == '')
		{
			alert("Не выбран файл для загрузки");			
		}		
		else
		{
			$('xml_format').value = xml_format;
			$('upload_price').submit();
			$('upload_price').disable();
			$('btn_upload').hide();
            $('ajax_loader').show();
			$('status').update('Идет загрузка файла, ждите ...');
			submitForm = true;
		}
	}
}


// запуск предварительного анализа файла с выводом шаблона для полей
function ShowFileTemplate(mess)
{
	var match = mess.match(/^upload\((\d+)\)$/);
	if (match && match[1] > 0)
	{
		uploadID = match[1];
		$('status').update('Загрузка файла завершена, идет предварительный анализ ...');
		new Ajax.Updater('ftemplate', 'ajaxer.php?x=prices.ajax_ftemplate&upload_id=' + uploadID, {
						 evalScripts: true,
						 onSuccess: function(transport) {
							 $('ajax_loader').hide();
                             $('status').update('Выберите порядок полей в файле.');
						 }					 
		});
	}
	else if (match[1] == 0)
	{
		$('ajax_loader').hide();
        $('status').update('<strong>Загрузка прайс-листа завершена. Для его обработки потребуется некоторое время, поэтому информация на сайте обновится не сразу.</strong>');
	}
	else
	{
		$('status').update('Во время загрузки произошла ошибка попробуйте еще раз.');							
	}
}


// завершение загрузки
function UploadFinish()
{
	$('status').update('Идет завершение загрузки прайс-листа');
	var sortable = new Array();
	var i = 0;
	while (true)
	{
		var id = 'column[' + i + ']';
		if ($(id) != undefined)
		{
			sortable[i] = $F(id);
			i++;			
		}
		else break;
	}
	
	if (sortable[0] != undefined)
	{
		new Ajax.Request('ajaxer.php?x=prices.ajax_finish&upload_id=' + uploadID, {
			parameters: {
				save_ftemplate: $('save_ftemplate').checked ? 1 : 0,
				sortable: sortable.join(',')
			},
			onSuccess: function(transport)
			{
				$('ftemplate').hide();
				$('ajax_loader').hide();
				$('status').update('<strong>Загрузка прайс-листа завершена. Для его обработки потребуется некоторое время, поэтому информация на сайте обновится не сразу.</strong>');
			}
		});
	}
	else {alert('Выберите названия полей');}
}


/* settings-form */
function ChangeDeleteCheckbox()
{
	var action = ($F('delete_all')) ? 'disable' : 'enable';
	$('delete_sale_stock', 'delete_sale_delivery', 'delete_buy_stock', 'delete_buy_delivery').invoke(action);	
}
