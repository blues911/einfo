<script src="templates/prices/script.js" type="text/javascript"></script>
<link href="templates/prices/style.css" rel="stylesheet" type="text/css" />

<h1>Пакетные действия</h1>
<h2>Удаление из прайс-листа</h2>
<form name="settings" id="settings" method="post" enctype="multipart/form-data" action="?module=prices&action=settings" >

	<div class="form-item">
		<ul class="settings_delete">
			<li ><input name="delete_all" id="delete_all" type="checkbox" value="1" onclick="ChangeDeleteCheckbox()" /> <strong>удалить все позиции из прайс-листа</strong></li>
		</ul>
	</div>	
	
	<br />
	<br />
		
	<div class="form-item">
		<button type="submit"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>						
	</div> 
</form>