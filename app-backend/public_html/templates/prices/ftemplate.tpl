<form name="sortable" id="sortable" method="post" enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="1" cellpadding="0" id="sortable">
	  <tr>
	    <th width="25">№</th>
		<th>Порядок полей</th>
		<th width="30%">Строка 1</th>
		<th width="30%">Строка 2</th>
	  </tr>
	{foreach from=$filerow item=row key=k name=rows}  
	  <tr>
		<td style="color: gray">{$smarty.foreach.rows.iteration|escape}-{cycle values=$letters}</td>
		<td class="select_column">	
			
			<select name="column[{$k|escape}]" id="column[{$k|escape}]">
				<option value="0" style="color: gray">[пустое поле]</option>
			{foreach from=$columns item=column}			
				<option value="{$column.id|escape}" {if $column.id==$row.selected}selected{/if}>{$column.title|escape}</option>
			{/foreach}
			</select>	
		</td>
		<td class="example_column">{$row.rnd1|truncate:26:"&#8230;":true|escape}&nbsp;</td>
		<td class="example_column">{$row.rnd2|truncate:26:"&#8230;":true|escape}&nbsp;</td>
	  </tr>
	{/foreach}  
	  
		
	</table>
	
	<div id="layer_save_ftemplate" ><input name="save_ftemplate" id="save_ftemplate" type="checkbox" value="1" checked /> сохранить шаблон (порядок следования полей)</div>
	
	<div align="right">
		<button onClick="UploadFinish()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Завершить загрузку</button>
	</div>
</form>
