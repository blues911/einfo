<script src="/i/js/tooltip.js" type="text/javascript"></script>
<script src="templates/prices/script.js" type="text/javascript"></script>
<link href="templates/prices/style.css" rel="stylesheet" type="text/css" />

<h1>Загрузка прайс-листов</h1>

<form name="upload_price" id="upload_price" method="post" enctype="multipart/form-data" action="ajaxer.php?x=prices.ajax_upload" target="iframe_upload"  onclick="UpdateLegend()">

	<fieldset>
		<legend>Параметры загрузки</legend>
	
		<div class="form-item">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbl_params">
			  <tr id="tblrow_1">
				<td width="22%"><strong>Загрузить прайс:</strong> [<a href="#" title="Выберите файл для загрузки в CSV, XLS или XML формате">?</a>]</td>
				<td>
					<input name="price" id="price" type="file" style="width: 100%" onchange="FileFormat()" />
					<input name="xml_format" id="xml_format" type="hidden" value="0" />
				</td>
			  </tr>
			  {if $ftemplate}	
			  <tr id="tblrow_2" style="display: none">
				<td><strong>Шаблон загрузки:</strong></td>
				<td><input name="use_ftemplate" id="use_ftemplate" type="checkbox" value="1" checked /> использовать шаблон (порядок полей в файле) [<a href="#" title="Отметьте этот чекбокс, если у Вас уже был сохранен шаблон, тогда он установится автоматически, в противном случае Вам будет предложен пустой, незаполненный шаблон. Если Вы не сохраняли шаблон или загружаете прайс-лист в первый раз, то значение этого чекбокса ни на что не влияет, просто проигнорируйте эту настройку.">?</a>]</td>
			  </tr>
			  {/if}				  
			  <tr id="tblrow_3" style="display: none">
				<td><strong>Валюта:</strong></td>
				<td>
					<input name="currency" id="currency" type="radio" value="usd" /> $ &nbsp;&nbsp;&nbsp;
					<input name="currency" id="currency" type="radio" value="rur" checked /> руб. &nbsp;&nbsp;&nbsp;
					<input name="currency" id="currency" type="radio" value="eur" />&euro;&nbsp;&nbsp;&nbsp;
					[<a href="#" title="Задайте валюту по умолчанию. Эта валюта будет использоваться только в том случае, если загрузчик не сможет определить валюту позиции из Вашего файла.">?</a>]			
				</td>
			  </tr>
			  <tr id="tblrow_4" style="display: none">
				<td valign="top"><strong>Загружаемые позиции:</strong></td>
				<td style="padding-bottom: 20px">
				
					<ul class="price_type" style="display: none;">
						<li><input name="price_type" id="price_type" type="radio" value="stock_replace" onclick="ChangePrice()" checked /></li>
					</ul>				
					
					<div id="stock_info" style="display: none"><input name="stock_hide" id="stock_hide" type="checkbox" value="1" /> скрыть количество позиций на складе</div>

				</td>
			  </tr>
			  <tr id="tblrow_5" style="display: none;">
				<td colspan="2" id="price_legend">Имя файла: &laquo;<span id="price_legend_name">af17a6d2be6676b4cf53b3ae81796fa6.update.rur.stock_hide.buy.csv</span>&raquo;</td>
			  </tr>				  			  			   
			</table>
		</div>
	</fieldset>

	
	<div class="form-item" style="margin-bottom: 10px" id="btn_upload" align="center">
		<button onClick="Upload()" type="button" style="width: 200px" ><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Загрузить</button>
	</div>			
	
	<iframe name="iframe_upload" id="iframe_upload" style="border: 0px; width: 0px; height: 0px;"></iframe>	
</form>	

<div class="spacer"></div>
	
<div><img src="i/ajax_loader_transp.gif" id="ajax_loader" style="display: none; margin-bottom: -3px;">&nbsp;<span id="status"></span></div>

<div id="ftemplate"></div>

<script>
	ChangePrice();
	var id = '{$id|escape}';
</script>


