<script src="templates/prices/script.js" type="text/javascript"></script>
<link href="templates/prices/style.css" rel="stylesheet" type="text/css" />

<h1>Экспорт</h1>


<form name="export-price" id="export-price" method="post" enctype="multipart/form-data" action="?module=prices&action=export-price" >
    <fieldset>
        <legend>Экспорт прайс-листа</legend>

        <div class="form-item" style="margin-bottom: 20px">
            Формат экспорта:
            <select name="export_price_format" id="export_price_format">
                <option value="csv" selected>CSV - разделитель табуляция</option>
            </select>

            <div style="display: inline-flex;align-items: baseline;margin-left: 30px;">
                Кодировка:&nbsp;&nbsp;
                <label><input type="radio" name="codepage" value="cp1251" checked> cp1251</label>
                &nbsp;&nbsp;
                <label><input type="radio" name="codepage" value="utf8"> utf-8</label>
            </div>
        </div>

        <div class="form-item">
            <ul class="export_price_option">
                <li >
                    <label>
                        <input name="price_type" id="export_all" type="radio" value="all" checked/> <strong>все позиции из прайс-листа</strong>
                    </label>
                </li>
                <li>
                    <label>
                        <input name="price_type" id="export_price_sale_stock" type="radio" value="sale_stock" /> позиции <strong>на продажу</strong>, которые являются <strong>складскими</strong>
                    </label>
                </li>
                <li>
                    <label>
                        <input name="price_type" id="export_price_sale_delivery" type="radio" value="sale_delivery" /> позиции <strong>на продажу</strong>, которые поставляются <strong>на заказ</strong>
                    </label>
                </li>
            </ul>
        </div>

        <br />

        <div class="form-item">
            <button type="submit"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Экспортировать прайс-лист</button>
        </div>
    </fieldset>
</form>


<form name="export-categories" id="export-categories" method="post" enctype="multipart/form-data" action="?module=prices&action=export-categories" >
    <fieldset>
        <legend>Экспорт общего рубрикатора</legend>

        <div class="form-item" style="margin-bottom: 20px">
            Формат экспорта:
            <select name="export_categories_format" id="export_categories_format">
                <option value="txt" selected>TXT - текстовая версия</option>              
            </select>
        </div>

        <div class="form-item">
            <button type="submit"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Экспортировать рубрикатор</button>
        </div>
    </fieldset>
</form>