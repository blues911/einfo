<link href="templates/social_polls/style.css" rel="stylesheet" type="text/css" />
<script src="templates/social_polls/script.js" type="text/javascript"></script>

<h1>Редактирование соц. опроса</h1>

<form id="social_poll_form" method="post" action="?module=social_polls&action=edit&id={$content.id}">
    
    <fieldset>
		    <legend>Общая информация</legend>

        <div class="form-item">
            <input type="checkbox" name="status" {if $content.status == 1}checked{/if} />
            Статус активации<br/><span style="color: #888; margin-left: 4px;">если отмечен, все остальные будут деактивированы</span>
        </div>

        <br/>

        Название:<br/>
        <div class="form-item">
            <input name="title" type="text" value="{$content.title}" style="width: 100%;" />
        </div>

        <br/>

        Кол-во просмотров (страниц) мин:<br/>
        <div class="form-item">
            <input name="views_min" type="number" value="{$content.views_min}" required />
        </div>

        <br/>

        Кол-во просмотров (страниц) макс:<br/>
        <div class="form-item">
            <input name="views_max" type="number" value="{$content.views_max}" required />
        </div>

    </fieldset>

    <br/>

    <fieldset id="form-questions">

		    <legend>Вопросы формы</legend>

        <br/>

        <div class="form-item">
            <button id="add-question"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
            Добавить вопрос
        </div>

        {if !empty($content.questions)}
            {foreach from=$content.questions key=q_key item=question}
                <div class="question">
                    <span class="title">Вопрос</span>
                    <div class="form-item inline-flex">
                        <select name="question_type[{$q_key}]">
                            <option value="text" {if $question.type == 'text'}selected{/if}>Тип: текст</option>
                            <option value="radio" {if $question.type == 'radio'}selected{/if}>Тип: варианты</option>
                        </select>
                        <button class="delete-question"><img src="i/icons/icon_del.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
                    </div>
                    <div class="form-item">
                        <textarea name="question_text[{$q_key}]" rows="4" style="width: 100%;" required placeholder="Текст вопроса">{$question.text}</textarea>
                    </div>
                    <div class="question-answers" {if $question.type != 'radio'}style="display: none;"{/if}>
                        <div class="form-item">
                            <button class="add-question-answer"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
                            Добавить вариант ответа
                        </div>
                        {if isset($question.answers) && !empty($question.answers)}
                            {foreach from=$question.answers key=a_key item=answer}
                                <div class="answer form-item inline-flex">
                                    <input type="text" name="question_answers[{$q_key}][{$a_key}]" value="{$answer}" required placeholder="Текст ответа" />
                                    <button class="delete-question-answer"><img src="i/icons/icon_del.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                </div>
            {/foreach}
        {/if}

    </fieldset>

    <br/>

    <div class="form-item">
        <button id="submit-form"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
    </div>

</form>

<!-- Шаблон вопроса -->
<div id="tpl-question" style="display: none;">
    <div class="question">
        <span class="title">Вопрос</span>
        <div class="form-item inline-flex">
            <select name="">
                <option value="text">Тип: текст</option>
                <option value="radio">Тип: варианты</option>
            </select>
            <button class="delete-question"><img src="i/icons/icon_del.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
        </div>
        <div class="form-item">
            <textarea name="" rows="4" style="width: 100%;" required placeholder="Текст вопроса"></textarea>
        </div>
        <div class="question-answers" style="display: none;">
            <div class="form-item">
                <button class="add-question-answer"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
                Добавить вариант ответа
            </div>
        </div>
    </div>
</div>

<!-- Шаблон ответа для вопроса -->
<div id="tpl-question-answer" style="display: none;">
    <div class="answer form-item inline-flex">
        <input type="text" name="" required placeholder="Текст ответа"/>
        <button class="delete-question-answer"><img src="i/icons/icon_del.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
    </div>
</div>