{literal}
<script language="javascript">

    jQuery(document).ready(function()
    {
        jQuery('a.delete').on('click', function(e)
        {
            e.preventDefault();
            var id = jQuery(this).attr('data-id');
            if (confirm('Вы действительно хотите удалить соц. опрос?'))
            {
                document.location = '?module=social_polls&action=delete&id=' + id;
            }
        });

        jQuery('a.status').on('click', function(e)
        {
            e.preventDefault();
            var id = jQuery(this).attr('data-id');
            if (confirm('Вы уверены, что хотите сменить статус соц. опроса?'))
            {
                jQuery.ajax({
                    url: 'ajaxer.php?x=social_polls.ajax_change_status',
                    type: 'POST',
                    dataType: 'json',
                    data: { id: id},
                    success: function(response)
                    {
                        var items = jQuery('a.status');
                        var icon = {
                            active: 'i/icons/icon_ok_active.gif',
                            inactive: 'i/icons/icon_ok_inactive.gif'
                        }

                        jQuery('a.status').find('img').attr('src', icon.inactive);

                        for (var i = 0; i < items.length; i++)
                        {
                            if (Number(jQuery(items[i]).attr('data-id')) == response.item.id && response.item.status == 1)
                            {
                                jQuery(items[i]).find('img').attr('src', icon.active);
                                break;
                            }
                        }
                    },
                    error: function()
                    {
                        alert('Произошла ошибка сервера.');
                    }
                });
            }
        });
    });

</script>
{/literal}

<h1>Cоц. опросы</h1>

<table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
    <tr align="center">
        <th width="5%">№</th>
        <th>Название</th>
        <th>Кол-во просмотров (страниц)</th>
        <th width="30">Статус</th>
        <th width="30">Ред.</th> 	
        <th width="30">Удал.</th>
    </tr>
    {foreach from=$list item=item}
    <tr valign="top">
        <td align="left">{$item.num|escape}.</td>
        <td align="left"><a href="?module=social_polls&action=edit-form&id={$item.id|escape}">{$item.title|escape}</a></td>
        <td align="center">{$item.views|escape}</td>
        <td align="center"><a href="#" class="status" data-id="{$item.id|escape}"><img src="{if $item.status == 1}i/icons/icon_ok_active.gif{else}i/icons/icon_ok_inactive.gif{/if}" alt="Публиковать" /></a></td>
        <td align="center"><a href="?module=social_polls&action=edit-form&id={$item.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
        <td align="center"><a href="#" class="delete" data-id="{$item.id|escape}"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a></td>
    </tr>
    {/foreach}
</table>

{include file="pager.tpl"}