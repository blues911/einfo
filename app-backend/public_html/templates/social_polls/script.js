jQuery(document).ready(function()
{
    // Добавить вопрос
    jQuery('#add-question').on('click', function(e)
    {
        e.preventDefault();

        jQuery('#tpl-question')
            .find('.question')
            .clone()
            .appendTo('#form-questions');
    });

    // Удалить вопрос
    jQuery(document).on('click', '.delete-question', function(e)
    {
        e.preventDefault();

        jQuery(this)
            .parents('.question')
            .remove();
    });

    // Добавить вариант ответа
    jQuery(document).on('click', '.add-question-answer', function(e)
    {
        e.preventDefault();

        jQuery('#tpl-question-answer')
            .find('.answer')
            .clone()
            .appendTo(jQuery(this).parents('.question-answers'));
    });

    // Удалить вариант ответа
    jQuery(document).on('click', '.delete-question-answer', function(e)
    {
        e.preventDefault();

        jQuery(this).parent().remove();
    });

    jQuery(document).on('change', '.question select', function(e)
    {
        e.preventDefault();
        var type = jQuery(this).val();

        if (type == 'radio')
        {
            jQuery(this)
                .parents('.question')
                .find('.question-answers')
                .css({'display': 'block'});
        }
        else
        {
            jQuery(this)
                .parents('.question')
                .find('.question-answers')
                .css({'display': 'none'});
        }
    });

    jQuery('#social_poll_form').on('click', '#submit-form', function(e)
    {
        e.preventDefault();

        UpdateIndexes();
        jQuery('#social_poll_form').submit();
    });

    function UpdateIndexes()
    {
        var question_index = 1;
        var questions = jQuery('#form-questions').find('.question');

        if (questions.length > 0)
        {
            for (var i = 0; i < questions.length; i++)
            {
                jQuery(questions[i]).find('select').attr('name', 'question_type[' + question_index + ']');
                jQuery(questions[i]).find('textarea').attr('name', 'question_text[' + question_index + ']');

                if (jQuery(questions[i]).find('select').val() == 'radio')
                {
                    var answer_index = 1;
                    var answers = jQuery(questions[i]).find('.answer');

                    if (answers.length > 0)
                    {
                        for (var j = 0; j < answers.length; j++)
                        {
                            jQuery(answers[j]).find('input').attr('name', 'question_answers[' + question_index + '][' + answer_index + ']');
                            answer_index++;
                        }
                    }
                }

                question_index++;
            }
        }
    }
});
