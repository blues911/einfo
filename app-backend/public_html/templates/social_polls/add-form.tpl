<link href="templates/social_polls/style.css" rel="stylesheet" type="text/css" />
<script src="templates/social_polls/script.js" type="text/javascript"></script>

<h1>Добавление соц. опроса</h1>

<form id="social_poll_form" method="post" action="?module=social_polls&action=add">
    
    <fieldset>
		    <legend>Общая информация</legend>

        <div class="form-item">
            <input type="checkbox" name="status" />
            Статус активации<br/><span style="color: #888; margin-left: 4px;">если отмечен, все остальные будут деактивированы</span>
        </div>

        <br/>

        Название:<br/>
        <div class="form-item">
            <input name="title" type="text" required style="width: 100%;" />
        </div>

        <br/>

        Кол-во просмотров (страниц) мин:<br/>
        <div class="form-item">
            <input name="views_min" type="number" value="0" required />
        </div>

        <br/>

        Кол-во просмотров (страниц) макс:<br/>
        <div class="form-item">
            <input name="views_max" type="number" value="0" required />
        </div>

    </fieldset>

    <br/>

    <fieldset id="form-questions">

		    <legend>Вопросы формы</legend>

        <br/>

        <div class="form-item">
            <button id="add-question"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
            Добавить вопрос
        </div>

    </fieldset>

    <br/>

    <div class="form-item">
        <button id="submit-form"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить</button>
    </div>

</form>

<!-- Шаблон вопроса -->
<div id="tpl-question" style="display: none;">
    <div class="question">
        <span class="title">Вопрос</span>
        <div class="form-item inline-flex">
            <select name="">
                <option value="text">Тип: текст</option>
                <option value="radio">Тип: варианты</option>
            </select>
            <button class="delete-question"><img src="i/icons/icon_del.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
        </div>
        <div class="form-item">
            <textarea name="" rows="4" style="width: 100%;" required placeholder="Текст вопроса"></textarea>
        </div>
        <div class="question-answers" style="display: none;">
            <div class="form-item">
                <button class="add-question-answer"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
                Добавить вариант ответа
            </div>
        </div>
    </div>
</div>

<!-- Шаблон ответа для вопроса -->
<div id="tpl-question-answer" style="display: none;">
    <div class="answer form-item inline-flex">
        <input type="text" name="" required placeholder="Текст ответа"/>
        <button class="delete-question-answer"><img src="i/icons/icon_del.gif" width="15" height="15" hspace="5" align="absmiddle" /></button>
    </div>
</div>