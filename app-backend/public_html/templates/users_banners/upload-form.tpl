<script src="templates/users_banners/script.js" type="text/javascript"></script>
<link href="templates/users_banners/style.css" rel="stylesheet" type="text/css" />

<h1>Загрузить баннер</h1>

<div class="form-item">
	<form action="?module=users_banners&action=upload" id="upload" name="upload" method="post" enctype="multipart/form-data">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="110" valign="top" ><img src="i/no_image.jpg" width="92" id="preview_upload_img"/></td>
			<td valign="middle">
				<div>Загрузить баннер:<br /><input name="upload_banner" id="upload_banner" type="file" style="width: 60%" onchange="PreviewImg('upload_banner')" /></div>
				<div>
                    Сссылка для баннера:<br />
                    <input name="upload_url" id="upload_url" type="text" style="width: 80%"  />
                    <div class="prompt">(поле должно быть пустым или начинаться с http://, например &mdash; http://www.example.ru)</div>
                </div>
			</td>
		  </tr>
		  <tr>
			<td width="110" valign="top" >&nbsp;</td>
			<td valign="middle" style="padding-bottom: 10px">
				<div style="margin: 15px 0; color: gray">
					Разрешается загружать баннеры только следующих форматов: {$allow_formats|escape}<br />
                    Допустимые размеры: {$allow_sizes|escape}
				</div>
				<button onClick="SubmitUploadForm()" type="button"><img src="i/icons/btn_upload.gif" width="15" height="15" hspace="5" align="absmiddle" />Загрузить</button>
			</td>
		  </tr>	  
		</table>
	</form>	
</div>

<div class="spacer"></div>

<h1>Список загруженных</h1>

<table width="100%" border="0" id="banners_list">
	{foreach from=$list item=item name=banners}
		{if $smarty.foreach.banners.first}<tr>{/if}
			<td>
				<div id="item[{$item.id|escape}]" class="item">
					{if $item.url}
                        <a href="{$item.url|escape}" target="_blank"><img src="http://{$item.banner|escape}" width="150" alt="{$item.url|escape}" title="{$item.url|escape}" class="banner_item" /></a>
                    {else}
                        <img src="http://{$item.banner|escape}" width="150" class="banner_item" />
                    {/if}
	
                    <div style="margin-top: 5px">
                        <strong>Баннер №{$item.num|escape}</strong>: {$item.size_x|escape}x{$item.size_y|escape} <a href="javascript: DeleteBanner({$item.id|escape})" ><img src="i/icons/btn_cancel.gif" width="15" height="15" alt="Удалить" title="Удалить" align="absmiddle" style="border: 0px;" /></a>
                        {if $item.url}<div class="prompt" style="padding-top: 0">{$item.url|truncate:35|escape}</div>{/if}
                    </div>
				</div>
			</td>
		{if  $smarty.foreach.banners.iteration % 3 == 0  AND !$smarty.foreach.banners.last}
			</tr><tr>		
		{elseif $smarty.foreach.banners.last}
			</tr>		
		{/if}
	{/foreach}
</table>


{include file="pager.tpl"}