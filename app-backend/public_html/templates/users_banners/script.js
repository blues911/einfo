function SubmitUploadForm()
{
	if ($F('upload_banner') != "")
	{
        if ($F('upload_url') == '' || isValidUrl($F('upload_url')))
        {
            $('upload').submit();
        }
        else alert("Вы ввели некорректный URL баннера");
	}
	else alert("Выберите файл для загрузки");
}

function DeleteBanner(id) {
	if(confirm('Вы действительно хотите удалить данный баннер?')) {
		new Ajax.Request('ajaxer.php?x=users_banners.ajax_delete', {
             method: 'post',
             parameters: { id: id },
             onSuccess: function(transport) {
                 if (transport.responseText == 'OK') {
                     $('item[' + id + ']').hide();
                 }
                 else {
                     alert("Доступ запрещен");
                 }
             }
        });
	}	
}
