{literal}
    <script language="javascript">
        jQuery(document).ready(function(){
            var search_form = jQuery('#view_mode');
            var add_form = jQuery('#add_item');

            jQuery(document).on('click', '#delete_all', function(e){
                jQuery('#form_remove').find('.checkbox_remove').prop('checked', e.target.checked);
            });

            search_form.on('click', '.btn', function(e){
                document.location = '?module=store_canonical&action=list&search=' + jQuery('#search').val().trim();
            });

            search_form.on('keydown', '#search', function(e){
                if (e.keyCode == 13)
                {
                    document.location = '?module=store_canonical&action=list&search=' + jQuery('#search').val().trim();
                }
            });

            add_form.on('click', '.btn', function(){
                add_form.submit();
            });

            add_form.on('keyup', '#add', function(e){
                if (e.keyCode == 13)
                {
                    add_form.submit();
                }
            });

            search_form.on('submit', function(e){ return false; });
        });

        function delete_item(id)
        {
            if (confirm('Вы действительно хотите удалить запись?'))
            {
                document.location="?module=store_canonical&action=delete&id=" + id;
            }
        }

        function delete_selected_items()
        {
            if (confirm('Вы действительно хотите удалить записи?'))
            {
                jQuery('#form_remove').submit();
            }
        }
    </script>
{/literal}

<h1>Список</h1>

<div align="right" style="margin: -20px 0 10px">
    <form name="view_mode" id="view_mode" method="post" action="#" >
        <input type="text" name="search" id="search" value="{$search|default:''}" placeholder="Введите поисковую фразу" style="width:300px;"/>
        <input type="button" name="send" value="Показать" class="btn">
    </form>
</div>

<div align="right" style="margin: 20px 0 10px">
    <form name="add_item" id="add_item" method="post" action="?module=store_canonical&action=add" >
        <label>
            Добавление компонента&nbsp;&nbsp;&nbsp;
            <input type="text" name="comp_title" id="comp_title" placeholder="Введите название" style="width:300px;"/>
        </label>
        <input type="button" name="send" value="Добавить" class="btn">
    </form>
</div>

<form id="form_remove" method="post" action="?module=store_canonical&action=delete">
    <table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">
        <tr align="center">
            <th width="1px">№</th>
            <th>Наименование</th>
            <th width="70">
                <label><input type="checkbox" id="delete_all"/> Удал.</label>
            </th>
        </tr>
        {foreach from=$list item=item}
            <tr valign="top" >
                <td align="center">{$item.num}.</td>
                <td><a href="//{$host_frontend}/store/{$item.comp_title|escape}/" target="_blank">{$item.comp_title|escape}</a></td>
                <td align="center">
                    <a href="javascript:;" onclick="delete_item({$item.id})"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a>
                    <input type="checkbox" name="ids[]" value="{$item.id}" class="checkbox_remove"/>
                </td>
            </tr>
        {foreachelse}
            <tr valign="top">
                <td>&nbsp;</td>
                <td colspan="2">Не найдено записей</td>
            </tr>
        {/foreach}
    </table>

    <button type="button" style="float: right; margin: 10px 0 20px;" onclick="delete_selected_items();">
        <img src="i/icons/icon_del.gif" align="absmiddle" height="15" hspace="5" width="15">
        Удалить выбранных
    </button>
</form>

<div style="clear:both;">{include file="pager.tpl"}</div>