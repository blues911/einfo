<script src="/i/js/tooltip.js" type="text/javascript"></script>

{literal}
    <script language="javascript">
        var can_submit = false;
        var error = {
            no_file: 'Файл не доступен',
            no_items: 'Нет подходящих записей',
            server_error: 'Что-то пошло не так. Попробуйте ещё раз.',
            wrong_format: 'Несовместимый формат файла.'
        };

        jQuery(document).ready(function(){
            // Проверка загружаемого файла
            jQuery(document).on('change', '#file', function(e){
                var file = $(e.target).files[0];
                var data = new FormData();
                var preview = jQuery(document).find('#preview');

                preview.text('').removeClass('error');

                if (file.type != 'text/plain')
                {
                    preview.text(error.wrong_format);
                    return false;
                }

                data.set('file', file);

                jQuery.ajax({
                    url: 'ajaxer.php?x=store_canonical.ajax_file_test',
                    type: 'POST',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(result)
                    {
                        result = JSON.parse(result);

                        if (result.success && result.list.length)
                        {
                            preview.text('Найдено строк: ' + result.list.length);
                            can_submit = true;
                        }
                        else
                        {
                            preview.text(error[result.error])
                        }
                    },
                    error: function(e)
                    {
                        preview.text(error.server_error).addClass('error');
                    }
                });
            });

            jQuery(document).on('click', '#btn_upload', function(){
                if (!can_submit) return false;

                jQuery('#upload_list').submit();
            });
        });
    </script>
    <style>
        #tooltip {
            background: #E7EFF5;
            border: 1px solid #627BA6;
            color: #2E3652;
            margin: 7px;
            padding: 5px 7px;
            position: absolute;
            visibility: hidden;
            z-index: 3;
        }
    </style>
{/literal}

<h1>Загрузка списка</h1>

<form name="upload_list"
      id="upload_list"
      method="post"
      enctype="multipart/form-data"
      action="?module=store_canonical&action=upload">

    <fieldset>
        <legend>Параметры загрузки</legend>

        <div class="form-item">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tbl_params">
                <tr id="tblrow_1">
                    <td width="22%" style="vertical-align: top;"><strong>Загрузить прайс:</strong> [<a href="#" title="Выберите файл для загрузки в TXT формате">?</a>]</td>
                    <td style="vertical-align: top;">
                        <input name="file" id="file" type="file" style="width: 100%"/>
                        <label style="display:inline-block; margin-top: 12px;">
                            <input name="replace" id="replace" type="checkbox" value="1" />
                            Полная замена
                        </label>
                    </td>
                </tr>
            </table>
        </div>
    </fieldset>

    <div id="preview"></div>

    <div class="form-item" style="margin-bottom: 10px" id="btn_upload" align="center">
        <button type="button" style="width: 200px" ><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Загрузить</button>
    </div>
</form>

<div class="spacer"></div>