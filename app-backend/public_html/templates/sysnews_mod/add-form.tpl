{literal}
<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
EnableEditor('text');

function SubmitForm()
{
	filled = ['title'];
	if (CheckFieldsForm(filled))
	{
		$('add_sysnews').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}	
}
</script>
{/literal}


<h1>Добавление новости</h1>

<form name="add_sysnews" id="add_sysnews" method="post" enctype="multipart/form-data" action="?module=sysnews_mod&action=add">
	
	<div class="form-item">
		* Заголовок:<br />
		<input name="title" id="title" type="text" style="width: 550px" />
	</div>
	
	<div class="form-item" style="margin-top: 20px">
		Полный текст новости:<br />
		<textarea name="text" id="text" rows="10" style="width: 550px" ></textarea>
	</div>			
			
	<br />
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button" ><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить новость</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>		
  
</form>
 