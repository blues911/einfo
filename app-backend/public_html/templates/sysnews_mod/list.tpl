{literal}
<script>

function DeleteSysnews(id, title)
{
	if(confirm('Вы действительно хотите удалить новость "' + jQuery('<p />').html(title).text() + '"?'))
	{
		document.location = '?module=sysnews_mod&action=delete&id=' + id;
	}	
}

</script>
{/literal}


<h1>Новости системы</h1>


<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">
  <tr align="center">
    <th width="5%">№</th>
    <th >Заголовок</th>
    <th width="25%">Дата
	{if $list_order == "desc"}
	<a href="?module=sysnews_mod&action=list&order=asc"><img src="i/icons/sort_desc.gif" alt="Сортировка по убыванию" width="9" height="5" hspace="5" border="0" />
	{else}
	<a href="?module=sysnews_mod&action=list&order=desc"><img src="i/icons/sort_asc.gif" alt="Сортировать по возрастанию" width="9" height="5" hspace="5" border="0" />
	{/if}
	</a></th>
    <th width="30">Ред.</th> 	
    <th width="30">Удал.</th>
  </tr>
  {foreach from=$list item=item}
  <tr valign="top" >
    <td align="center">{$item.num|escape}.</td>
    <td><a href="?module=sysnews_mod&action=edit-form&id={$item.id|escape}">{$item.title|escape}</a></td>
    <td align="center">{$item.date|escape}</td>
	<td align="center"><a href="?module=sysnews_mod&action=edit-form&id={$item.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center"><a href="#" onclick="DeleteSysnews({$item.id|escape}, '{$item.title|escape}')"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a></td>
  </tr>
  {/foreach}

</table>

{include file="pager.tpl"}