{literal}
<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
EnableEditor('text');

function SubmitForm()
{
	filled = ['title'];
	if (CheckFieldsForm(filled))
	{
		$('edit_sysnews').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}	
}
</script>
{/literal}


<h1>Редактирование новости</h1>

<form name="edit_sysnews" id="edit_sysnews" method="post" enctype="multipart/form-data" action="?module=sysnews_mod&action=edit&id={$content.id|escape}">
	
	<div class="form-item">
		* Заголовок:<br />
		<input name="title" id="title" type="text" style="width: 550px" value="{$content.title|escape}" />
	</div>
	
	<div class="form-item" style="margin-top: 20px">
		Полный текст новости:<br />
		<textarea name="text" id="text" rows="10" style="width: 550px" >{$content.text|escape}</textarea>
	</div>					
			
	<br />
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
	</div>

  
</form>
 