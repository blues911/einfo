{literal}
<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
EnableEditor('disclaimer');

function SubmitForm()
{
	if ($F('title') == "")
	{
		alert('Не заполнено поле "Название вида аккаунта"');
		return false;	
	}
	
	if ($F('access_levels') == 0)
	{
		alert('Не выбран уровень доступа');
		return false;	
	}					
	
	$('add_accounts').submit();
}

</script>
{/literal}


<h1>Добавление нового вида аккаунта</h1>

<form name="add_accounts" id="add_accounts" method="post" enctype="multipart/form-data" action="?module=accounts&action=add">

	<div class="form-item">
		* Название вида аккаунта:<br />
		<input name="title" id="title" type="text" style="width:300px" />
	</div>	

	<div class="form-item">
		Краткое описание:<br />
		<textarea name="description" id="description"  rows="3" style="width:550px" ></textarea>
	</div>
	
	<div class="form-item">
		* Уровень доступа:<br />
		<select name="access_levels" id="access_levels">
			<option value="0" selected style="color: gray;">[ --- не выбрано --- ]</option>
			{foreach from=$levels item=item}					 
			<option value="{$item.id|escape}">{$item.title|escape}</option>
			{/foreach}
		</select>
	</div>	
	
	<div class="form-item">
		Требует модерации: <input name="moderate" type="radio" value="1" checked /> да &nbsp; <input name="moderate" type="radio" value="0" /> нет
	</div>
	
	<div class="form-item" style="margin-top: 20px">
		Пользовательское соглашение:<br />
		<textarea name="disclaimer" id="disclaimer" rows="20" style="width: 550px" ></textarea>
		<p><input name="disclaimer_change" id="disclaimer_change" type="checkbox" value="1" /> обновить пользовательское соглашение</p>
	</div>		
	
	
	<br />
	<br />
		
	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить вид аккаунта</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>  
</form>