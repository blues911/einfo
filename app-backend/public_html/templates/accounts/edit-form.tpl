{literal}
<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
EnableEditor('disclaimer');

function SubmitForm()
{
	if ($F('title') == "")
	{
		alert('Не заполнено поле "Название вида аккаунта"');
		return false;	
	}
	
	if ($F('access_levels') == 0)
	{
		alert('Не выбран уровень доступа');
		return false;	
	}		
	
	$('edit_accounts').submit();
}

</script>
{/literal}


<h1>Редактирование вида аккаунта</h1>

<form name="edit_accounts" id="edit_accounts" method="post" enctype="multipart/form-data" action="?module=accounts&action=edit&id={$content.id|escape}">

	<div class="form-item">
		* Название вида аккаунта:<br />
		<input name="title" id="title" type="text" style="width:300px" value="{$content.title|escape}" />
	</div>	

	<div class="form-item">
		Краткое описание:<br />
		<textarea name="description" id="description"  rows="3" style="width:550px" >{$content.description|escape}</textarea>
	</div>
	
	<div class="form-item">
		* Уровень доступа:<br />
		<select name="access_levels" id="access_levels">
			<option value="0" selected style="color: gray;">[ --- не выбрано --- ]</option>
			{foreach from=$levels item=item}					 
			<option value="{$item.id|escape}" {if $content.level_id == $item.id}selected{/if}>{$item.title|escape}</option>
			{/foreach}
		</select>
	</div>
	
	<div class="form-item">
		Требует модерации: <input name="moderate" type="radio" value="1" {if $content.moderate == 1}checked{/if} /> да &nbsp; <input name="moderate" type="radio" value="0" {if $content.moderate == 0}checked{/if} /> нет
	</div>	
	
	<div class="form-item" style="margin-top: 20px">
		Пользовательское соглашение:<br />
		<textarea name="disclaimer" id="disclaimer" rows="20" style="width: 550px" >{$content.disclaimer|escape}</textarea>
		<p><input name="disclaimer_change" id="disclaimer_change" type="checkbox" value="1" /> обновить пользовательское соглашение</p>
	</div>	
	
	<br />
	<br />
		
	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>						
	</div> 
</form>