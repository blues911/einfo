{if $disclaimer}
<div id="disclaimer-message">
	<p><strong>Внимание! Произошло обновление условий Пользовательского соглашения.</strong></p> 
	<p>Для того, чтобы получить доступ к своему аккаунту, 
	пожалуйста, <a href="/?module=cp&action=disclaimer">подтвердите свое согласие</a> с новым Пользовательским соглашением.</p>
</div>
{/if}

{foreach from=$menu item=item}
	{if $item.title != ""}
		<div class="menu-item {if $item.admin}admin-item{/if}" style="background-image: url(i/icons/icon_null{if $item.selected}_sel{/if}.gif)">
		{if $item.selected}
			<a href="?module={$item.name|escape}&action={$item.default_action|escape}"><span>{$item.title|escape}</span></a>
		{else}
			<a href="?module={$item.name|escape}&action={$item.default_action|escape}">{$item.title|escape}</a>
		{/if}
		
		{if $item.name == "forum" AND $forum_update}<sup style="color: red">new</sup>{/if}
		</div>
	{else}
		<div >&nbsp;</div>
	{/if}
{/foreach}