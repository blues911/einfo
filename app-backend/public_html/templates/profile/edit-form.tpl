<link href="templates/profile/style.css" rel="stylesheet" type="text/css" />

{literal}
<script>
function SubmitForm()
{
	if (CheckFieldsForm(filled))
	{
        if ($('www') && $F('www') != '' && !isValidUrl($F('www')))
        {
            alert("Неверный формат URL, адрес сайта должен начинаться с «http://»");
            return false;
        }

		$('edit_profile').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}
}

function UpdateCountry() {
    if ($F('country_id') == 186) {
        $('city_id').enable();
    }
    else {
        $('city_id').disable();
        $('city_id').selectedIndex = 0;
    }
}

</script>
{/literal}


<h1>{if $from == 'edit-account'}Изменение логина/пароля{else}Редактирование профиля{/if}</h1>

<form name="edit_profile" id="edit_profile" method="post" enctype="multipart/form-data" action="?module=profile&action=edit">	

	<table width="100%" border="0" cellspacing="0" cellpadding="8" id="profile">

	  {foreach from=$user_fields item=item}
          <tr>
              <td width="170">{if $item.fill}* {/if}{$item.title|escape}:</td>
              <td >
                  {if $item.field == "password"}
                      <input name="password" id="password" type="password" style="width:380px" />
                      <div class="prompt">(текущий пароль не отображается, для его изменения просто введите новый)</div>
                  {elseif $item.field == "login"}
                      <input name="{$item.field|escape}" id="{$item.field|escape}" type="text" style="width:380px" value="{$item.value|escape}" />
                  {elseif $item.field == "country_id"}
                      <select id="country_id" name="country_id" onchange="UpdateCountry()">
                          <option value="0">не определена</option>
                          {foreach from=$countries item=country}
                              <option value="{$country.country_id|escape}" {if $country.country_id == $item.value}selected{/if}>{$country.country|escape}</option>
                          {/foreach}
                      </select>
                  {elseif $item.field == "city_id"}
                      <select id="city_id" name="city_id">
                          <option value="0">не определен</option>
                          {foreach from=$cities item=city}
                              <option value="{$city.city_id|escape}" {if $city.city_id == $item.value}selected{/if}>{$city.city|escape}</option>
                          {/foreach}
                      </select>
                  {elseif $item.field == "individual_buyers" || $item.field == "email_notification" || $item.field == "allow_download_price" || $item.field == "price_with_nds"}
                      <input type="checkbox" id="{$item.field|escape}" name="{$item.field|escape}" value="1" title="{$item.title|escape}" {if $item.value}checked{/if} />
                  {elseif $item.field == 'about'}
                      <textarea id="{$item.field|escape}" name="{$item.field|escape}" style="width: 380px; min-height: 100px; resize: vertical;">{$item.value|escape}</textarea>
                  {else}
                      <input name="{$item.field|escape}" id="{$item.field|escape}" type="text" style="width:380px" value="{$item.value|escape}" />
                  {/if}
              </td>
          </tr>
	  {/foreach}

      {if $user.type == "dist"}
      <tr>
          <td><strong>Максимально разрешенное кол-во строк в прайс-листе</strong></td>
          <td><strong style="font-size: 120%">{$user.price_limit|escape}</strong></td>
      </tr>

      <tr>
          <td><strong>Ваш уникальный HASH</strong><br />(для xml файлов)</td>
          <td><strong style="font-size: 120%">{$hash|escape}</strong></td>
      </tr>
      {/if}
	</table>

    <script type="text/javascript">
        UpdateCountry();
    </script>
	
	<br />
	<br />

    <input type="hidden" name="from" value="{$from|escape}" />

	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
	</div> 
</form>

<script>
	filled = $w('{foreach from=$fill item=item}{$item|escape} {/foreach}');
	$w('{foreach from=$editable item=item}{$item|escape} {/foreach}').each(Form.Element.disable);
</script>

{literal}
    <script type="text/javascript">
        jQuery(function()
        {
            jQuery('#phone1, #phone2, #fax, #viber, #whatsapp').mask('+7 (999) 999-99-99');
        });

        jQuery('#phone1_ext, #phone2_ext').on('input paste change blur', function()
        {
            var elem = jQuery(this);

            elem.val(String(elem.val()).replace(/[^\d]/g, '').substr(0, 4));
        });
    </script>
{/literal}

