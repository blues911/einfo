<h1>Регистрация</h1>
<div class="spacer"></div>

<div id="atype">
	<h2 style="margin-bottom: 25px;">Выберите тип аккаунта:</h2>

	{foreach from=$accounts item=item}	
	<dl class="account_type">
		<dt><h2><a href="?register={$item.id|escape}">{$item.title|escape}</a></h2></dt>
		<dd>
			{$item.description|escape}
			<div align="right"><a href="?register={$item.id|escape}">зарегистрироваться</a> &rarr;</div>
		</dd>
	</dl>
	{/foreach}

</div>

