<h1>Регистрация</h1>
<div class="spacer"></div>

<h2>&laquo;{$account.title|escape}&raquo;</h2>

<div id="descr">{$account.description|escape}</div>

<form action="#" method="post" enctype="application/x-www-form-urlencoded" name="register" id="register">

	<table border="0" cellspacing="0" cellpadding="7">
	  {foreach from=$fields item=item}
	  <tr>
		<td width="170">{if $item.fill}* {/if}{$item.title|escape}</td>
		<td>
		{if $item.field == "login"}
			<input name="login" id="login" type="text" style="width:90%" />
			<div id="login_message" style="display: none">Данный логин уже используется в системе, выберите другой.</div>
		{elseif $item.field == "password"}
			<input name="password" id="password" type="password" style="width:200px" />
			<br />
			повторите пароль: <br />
			<input name="password_retype" id="password_retype" type="password" style="width:200px" />
        {elseif $item.field == "country_id"}
            <select id="country_id" name="country_id" onchange="UpdateCountry()">
                <option value="0">не определена</option>
            {foreach from=$countries item=country}
                <option value="{$country.country_id|escape}" {if $country.country_id == 186}selected{/if}>{$country.country|escape}</option>
            {/foreach}
            </select>
        {elseif $item.field == "city_id"}
            <select id="city_id" name="city_id">
                <option value="0">не определен</option>
            {foreach from=$cities item=city}
                <option value="{$city.city_id|escape}">{$city.city|escape}</option>
            {/foreach}
            </select>
		{else}
			<input name="{$item.field|escape}" id="{$item.field|escape}" type="text" style="width:90%" />
		{/if}		
		</td>
	  </tr>	  
	  {/foreach}
	 <tr>
		<td><img src="/protect/kcaptcha/"></td>
		<td>
			* Защита от спама (введите код):<br />
			<input name="captcha" id="captcha" type="text" style="width: 200px" />
			<div style="display: none;" id="captcha_message">Введенный код неверен, попробуйте еще раз.</div>
		</td>
	 </tr> 	
	 <tr>
		<td colspan="2">
			<input name="disclaimer" id="disclaimer" type="checkbox" value="1" onclick="DisclaimerCheck()" /> <strong>Я ознакомился с условиями Пользовательского соглашения.</strong>
			<div id="disclaimer_text">{$account.disclaimer|escape}</div>
		</td>
	 </tr>	   
	  <tr id="reg_tr">
		<td><input name="account_type" id="account_type" type="hidden" value="{$account.id|escape}" /></td>
		<td align="right"><button type="button" onclick="Register()" id="reg_button" style="display: none">Зарегистрироваться</button></td>
	  </tr>	  
	</table>
	
	<div id="reg_message_x" align="center" style="display: none">
		<div id="reg_message">
		{if $account.moderate}
			<strong>Ваш аккаунт зарегистрирован.</strong><br />
			После его валидации (наш ваш email была отправлена специальная ссылка) и рассмотрения его модератором он будет активирован.  Для доступа к аккаунту используйте адрес <a href="http://www.einfo.su" target="_blank">http://www.einfo.su</a> и свой логин с паролем.
		{else}
			<strong>Ваш аккаунт зарегистрирован.</strong><br />
			После его валидации (наш ваш email была отправлена специальная ссылка) он будет сразу активирован. Для доступа к аккаунту используйте адрес <a href="http://www.einfo.su" target="_blank">http://www.einfo.su</a> и свой логин с паролем.
		{/if}
		</div>
	</div>
	
</form>

<script>
	filled = $w('captcha {foreach from=$fill item=item}{$item|escape} {/foreach}');
	arr_fields = $w('captcha {foreach from=$arr_fields item=item}{$item|escape} {/foreach}');
</script>
