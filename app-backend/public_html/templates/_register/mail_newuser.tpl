Здравствуйте!

В системе был зарегистрирован новый участник, который успешно прошел валидацию.
Данные нового участника:
-----------------------
ID: {$user.id}
Наименование организации: {$user.title|replace:"&quot;":"\""}
Логин: {$user.login|replace:"&quot;":"\""|default:"—"}
Страна: {$user.country|replace:"&quot;":"\""|default:"—"}
Город: {$user.city|replace:"&quot;":"\""|default:"—"}
Адрес: {$user.address|replace:"&quot;":"\""|default:"—"}
Телефон: {$user.phone|replace:"&quot;":"\""|default:"—"}
Факс: {$user.fax|replace:"&quot;":"\""|default:"—"}
Электронная почта: {$user.email|replace:"&quot;":"\""|default:"—"}
Сайт: {$user.www|replace:"&quot;":"\""|default:"—"}
ICQ: {$user.icq|replace:"&quot;":"\""|default:"—"}
Skype: {$user.skype|replace:"&quot;":"\""|default:"—"}
Контактная персона (ФИО): {$user.person_fio|replace:"&quot;":"\""|default:"—"}
Контактная персона (E-mail): {$user.person_email|replace:"&quot;":"\""|default:"—"}
Контактная персона (Дополнительная информация): {$user.person_info|replace:"&quot;":"\""|default:"—"}
Информация о компании: {$user.about|replace:"&quot;":"\""|default:"—"}
ИНН: {$user.inn|replace:"&quot;":"\""|default:"—"}
КПП: {$user.kpp|replace:"&quot;":"\""|default:"—"}
ОГРН (ОГРНИП): {$user.ogrn|replace:"&quot;":"\""|default:"—"}
Юридический адрес: {$user.address_jur|replace:"&quot;":"\""|default:"—"}

--
Йа робот.
