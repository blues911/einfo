{* Smarty *}

<h1 style="margin-bottom: 0px;">Биллинг</h1>

<form id="form-billing-filter" action="/" method="GET">
    <input type="hidden" name="module" value="billing" />
    <input type="hidden" name="action" value="list" />

    <div style="text-align: right; margin-bottom: 15px;">
        <div style="text-align: right; margin-bottom: 10px;"><a href="?module=billing&action=list">сбросить фильтры</a></div>

        <div style="display: inline-block">
            Пользователь:&nbsp;
            <a href="javascript:;" id="user_search_toggle"><img src="/i/icons/icon_search.gif"></a>
            <input id="user_autocomplete" type="text" placeholder="Искать..." style="width: 294px; display: none;" />
            <select id="user_select" name="user_id" style="width: 300px; margin: 1px 0;" onchange="form_submit();">
                <option value="0" {if $filter.user_id == 0}selected{/if}>Любой пользователь</option>
                {foreach from=$user_list item=user}
                    <option value="{$user.id|escape}" {if $filter.user_id == $user.id}selected{/if}>{$user.title|escape} [{$user.id|escape}]</option>
                {/foreach}
            </select>
        </div>

        <div style="display: inline-block; margin-left: 10px;">
            Статус&nbsp;платежа:&nbsp;
            <select name="status" onchange="form_submit();">
                <option value="-1" {if $filter.status == -1}selected{/if}>Не важен</option>
                <option value="1" {if $filter.status == 1}selected{/if}>Подтвержден</option>
                <option value="0" {if $filter.status == 0}selected{/if}>Отменен</option>
            </select>
        </div>

        <div style="display: inline-block; margin-top: 10px;">
            Период&nbsp;активности:&nbsp;
            {if !empty($periods)}
                <select id="filter-period" onchange="change_filter_period();">
                    <option>другой</option>
                    {foreach from=$periods item=period}
                        <option data-from="{$period.from|escape}" data-to="{$period.to|escape}" {if $period.selected}selected{/if}>{$period.title|escape}</option>
                    {/foreach}
                </select>
            {/if}
            <input type="text" id="filter-from" name="active_from" class="datepicker-filter" value="{$filter.active_from|escape}" onchange="form_submit();" /> &mdash;
            <input type="text" id="filter-to" name="active_to" class="datepicker-filter" value="{$filter.active_to|escape}" onchange="form_submit();" />
        </div>

        <div style="display: inline-block; margin-left: 10px;">
            <input type="checkbox" name="one_page" value="1" style="position: relative; top: 3px;" {if $filter.one_page}checked{/if} onchange="form_submit();" />одним списком
        </div>

        <input type="submit" style="display: none;" />

    </div>
</form>

<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">
    <thead>
    <tr>
        <th width="15">Id</th>
        <th style="text-align: left;">Платеж</th>
        <th width="175" style="text-align: left;">Пользователь</th>
        <th width="75">Сумма</th>
        <th width="150">Активность пользователя</th>
        <th width="18"></th>
        <th width="18"></th>
        <th width="18"></th>
    </tr>
    </thead>
    <tbody>
    {if $list}
        {foreach from=$list item=item}
            <tr data-id="{$item.id|escape}">
                <td style="text-align: right;">{$item.id|escape}.</td>
                <td style="text-align: left;">
                    <a href="?module=billing&action=edit-form&id={$item.id|escape}">
                        {$item.payment_number|escape} от {$item.payment_date|escape}
                    </a>
                </td>
                <td><a href="?module=users&action=edit-form&id={$item.user_id|escape}">{$item.user_title|escape}</a> [{$item.user_id|escape}]</td>
                <td style="text-align: center;">{$item.amount|escape} руб.</td>
                <td style="text-align: center;">{$item.active_from|escape} &mdash; {$item.active_to|escape}</td>
                <td style="text-align: center; vertical-align: middle;">
                    <span id="status" style="cursor: pointer;" onclick="change_status({$item.id|escape});">
                        <img id="status-1" border="0" src="i/icons/icon_ok_active.gif" {if $item.status != 1}style="display: none;"{/if}>
                        <img id="status-0" border="0" src="i/icons/icon_ok_inactive.gif" {if $item.status != 0}style="display: none;"{/if}>
                    </span>
                    <span id="status-loading" style="display: none;">
                        <img border="0" src="i/ajax_loader_transp.gif">
                    </span>
                </td>
                <td style="text-align: center; vertical-align: middle;">
                    <a href="?module=billing&action=edit-form&id={$item.id|escape}">
                        <img border="0" src="i/icons/icon_edit.gif">
                    </a>
                </td>
                <td style="text-align: center; vertical-align: middle;">
                    {if $item.file_type}
                        <a href="/ajaxer.php?x=billing.ajax_file&id={$item.id|escape}" target="_blank">
                            <img border="0" src="i/icons/icon_list.gif">
                        </a>
                    {/if}
                </td>
            </tr>
        {/foreach}
        <tr>
            <td colspan="3" style="text-align: right; font-weight: bold;">
                СУММА К ОПЛАТЕ:
            </td>
            <td style="text-align: center; font-weight: bold;">{$sum|number_format:2:".":""|escape} руб.</td>
            <td colspan="4"></td>
        </tr>
    {/if}
    </tbody>
</table>

{include file="pager.tpl"}

{literal}
<script>
    jQuery('input.datepicker-filter').Zebra_DatePicker({
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        show_select_today: 'Сегодня',
        lang_clear_date: 'Очистить',
        format: 'd.m.Y',
        readonly_element: false,
        view: 'months',
        onSelect: function()
        {
            form_submit();
        },
        onClear: function()
        {
            form_submit();
        }
    });

    jQuery('input.datepicker-filter').mask('99.99.9999', { placeholder: '__.__.____' });

    jQuery(document).on('click', '#user_search_toggle', function()
    {
        var select = jQuery('#user_select');
        var autocomplete = jQuery('#user_autocomplete');

        if (select.hasClass('hidden'))
        {
            select.removeClass('hidden').show();
            autocomplete.hide();
        }
        else
        {
            select.addClass('hidden').hide();
            autocomplete.show().focus();
        }
    });

    jQuery(document).ready(function()
    {
        var source = [];
        jQuery('#user_select option').each(function()
        {
            var node = {
                label: jQuery(this).text(),
                value: jQuery(this).text(),
                id: jQuery(this).val()
            };

            source.push(node);
        });

        jQuery('#user_autocomplete').autocomplete({
            source: source,
            select: function(event, ui)
            {
                jQuery('#user_select').val(Number(ui.item.id)).change();
            }
        });
    });


    function form_submit()
    {
        jQuery('#form-billing-filter').submit();
    }

    function change_filter_period()
    {
        var option = jQuery('#filter-period option:selected:first');

        var from = option.data('from');
        var to = option.data('to');

        if (from)
        {
            jQuery('#filter-from').val(from);
        }

        if (to)
        {
            jQuery('#filter-to').val(to);
        }

        if (from || to)
        {
            form_submit();
        }
    }

    function change_status(id)
    {
        if (confirm('Вы уверены, что хотите сменить статус платежа?'))
        {
            var tr = jQuery('tr[data-id=' + id + ']:first');
            var status = tr.find('#status');
            var status_loading = tr.find('#status-loading');

            status.hide();
            status_loading.show();

            jQuery.ajax({
                type: 'POST',
                url: '/ajaxer.php?x=billing.ajax_change_status',
                data: { id: id },
                dataType: 'json',
                success: function(response)
                {
                    if (response.success)
                    {
                        var status_1 = tr.find('#status-1');
                        var status_0 = tr.find('#status-0');

                        if (response.status == 1)
                        {
                            status_1.show();
                            status_0.hide();
                        }
                        else
                        {
                            status_1.hide();
                            status_0.show();
                        }
                    }
                },
                error: function()
                {
                    alert('Произошла ошибка смены статуса.');
                },
                complete: function()
                {
                    status_loading.hide();
                    status.show();
                }
            });
        }
    }
</script>
{/literal}