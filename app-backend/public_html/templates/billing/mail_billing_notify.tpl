Здравствуйте!

Вам необходимо оплатить наши услуги до {$date}.
Иначе Ваш аккаунт (ID {$user.{$user.title|replace{$date}\""|escape}) будет заблокирован {$date|escape}.

--
C уважением,
Администрация www.einfo.ru