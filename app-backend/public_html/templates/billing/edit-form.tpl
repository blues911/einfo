{* Smarty *}

<h1>Редактирование платежа</h1>

<form id="form-edit" action="?module=billing&action=edit&id={$bill.id|escape}" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Данные о платеже</legend>

        <table border="0" cellpadding="0" cellspacing="6">
            <tr>
                <td style="text-align: right; font-weight: bold;">Дата создания:</td>
                <td>{$bill.create_date|escape}</td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold;">* Пользователь:</td>
                <td>
                    <a href="javascript:;" id="user_search_toggle"><img src="/i/icons/icon_search.gif"></a>
                    <input id="user_autocomplete" type="text" placeholder="Искать..." style="width: 294px; display: none;" />
                    <select id="user_id" name="user_id" style="width: 300px; margin: 1px 0;">
                        {foreach from=$user_list item=user}
                            <option value="{$user.id|escape}" {if $bill.user_id == $user.id}selected{/if}>{$user.title|escape} [{$user.id|escape}]</option>
                        {/foreach}
                    </select>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold;">Номер платежки:</td>
                <td><input type="text" id="payment_number" name="payment_number" value="{$bill.payment_number|escape}" /></td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold;">* Дата платежа:</td>
                <td><input type="text" id="payment_date" name="payment_date" class="datepicker" value="{$bill.payment_date|escape}" /></td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold;">* Сумма:</td>
                <td><input type="text" id="amount" name="amount" style="text-align: right;" value="{$bill.amount|escape}" /> руб.</td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold;">* Активность пользователя:</td>
                <td>
                    <input type="text" id="active_from" name="active_from" class="datepicker-filter" value="{$bill.active_from|escape}" /> &mdash;
                    <input type="text" id="active_to" name="active_to" class="datepicker-filter" value="{$bill.active_to|escape}" />
                    {if !empty($periods)}
                        <select id="period" onchange="change_period();">
                            <option value="0">другой</option>
                            {foreach from=$periods item=period name=periods}
                                <option value="{$smarty.foreach.periods.iteration|escape}" data-from="{$period.from|escape}" data-to="{$period.to|escape}" {if $period.selected}selected{/if}>{$period.title|escape}</option>
                            {/foreach}
                        </select>
                    {/if}
                </td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold;">Файл:</td>
                <td>
                    {if $bill.file_type}<a href="/ajaxer.php?x=billing.ajax_file&id={$bill.id|escape}" target="_blank">Просмотр текущего файла</a>{/if}
                    <input type="file" id="file" name="file" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right; font-weight: bold;">* Статус платежа:</td>
                <td>
                    <select id="status" name="status">
                        <option value="1" {if $bill.status == 1}selected{/if}>Подтвержден</option>
                        <option value="0" {if $bill.status == 0}selected{/if}>Отменен</option>
                    </select>
                </td>
            </tr>
        </table>
    </fieldset>

    <button type="button" onclick="form_submit();"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
</form>

{literal}
    <script>
        jQuery('input.datepicker-filter').Zebra_DatePicker({
            days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            show_select_today: 'Сегодня',
            lang_clear_date: 'Очистить',
            format: 'd.m.Y',
            readonly_element: false,
            view: 'months',
            onSelect: function()
            {
                reset_period();
            },
            onClear: function()
            {
                reset_period();
            }
        });

        jQuery(document).on('input', 'input.datepicker-filter', function()
        {
            reset_period();
        });

        jQuery('input.datepicker-filter').mask('99.99.9999', { placeholder: '__.__.____' });

        jQuery(document).on('click', '#user_search_toggle', function()
        {
            var select = jQuery('#user_id');
            var autocomplete = jQuery('#user_autocomplete');

            if (select.hasClass('hidden'))
            {
                select.removeClass('hidden').show();
                autocomplete.hide();
            }
            else
            {
                select.addClass('hidden').hide();
                autocomplete.show().focus();
            }
        });

        jQuery(document).ready(function()
        {
            var source = [];
            jQuery('#user_id option').each(function()
            {
                var node = {
                    label: jQuery(this).text(),
                    value: jQuery(this).text(),
                    id: jQuery(this).val()
                };

                source.push(node);
            });

            jQuery('#user_autocomplete').autocomplete({
                source: source,
                select: function(event, ui)
                {
                    jQuery('#user_id').val(Number(ui.item.id)).change();
                    jQuery('#user_search_toggle').click();
                    jQuery('#payment_number').focus();
                }
            });
        });

        function change_period()
        {
            var option = jQuery('#period option:selected:first');

            var from = option.data('from');
            var to = option.data('to');

            if (from)
            {
                jQuery('#active_from').val(from);
            }

            if (to)
            {
                jQuery('#active_to').val(to);
            }
        }

        function reset_period()
        {
            jQuery('#period').val(0);

            var from = jQuery('#active_from').val();
            var to = jQuery('#active_to').val();

            var period = jQuery('#period option[data-from="' + from + '"][data-to="' + to + '"]:first');

            console.log(period);

            if (period.length)
            {
                jQuery('#period').val(period.val());
            }
        }

        function form_submit()
        {
            var user_id = Number(jQuery('#user_id').val());
            var payment_number = jQuery.trim(jQuery('#payment_number').val());
            var payment_date = jQuery('#payment_date').val();
            var amount = Number(String(jQuery.trim(jQuery('#amount').val())).replace(/[^\d,.]/g, '').replace(/,/g, '.'));
            var active_from = jQuery('#active_from').val();
            var active_to = jQuery('#active_to').val();
            var status = Number(jQuery('#status').val());

            jQuery('#payment_number').val(payment_number);
            jQuery('#amount').val(amount);

            if (!payment_date)
            {
                alert('Не заполнено поле "Дата платежа".');
                return false;
            }

            if (!amount)
            {
                alert('Не заполнено поле "Cумма".');
                return false;
            }

            if (!active_from || !active_to)
            {
                alert('Не заполнено поле "Активность пользователя".');
                return false;
            }

            jQuery('#form-edit').submit();
        }
    </script>
{/literal}