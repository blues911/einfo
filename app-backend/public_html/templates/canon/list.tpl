{* Smarty *}

{literal}
    <script type="text/javascript">

        function DeleteTemplate(id, title)
        {
            if (confirm('Вы действительно хотите удалить шаблон "' + jQuery('<p />').html(title).text() + '"?'))
            {
                document.location = "?module=canon&action=delete&id=" + id;
            }
        }

    </script>
{/literal}

<h1>Канонические шаблоны</h1>

<div align="right" style="margin: -20px 0px 10px 0px">
    <form name="search" id="search" method="get">

        <input type="hidden" name="module" value="canon" />
        <input type="hidden" name="action" value="list" />

        <div class="form-item">
            Категория:
            <select id="form_category_id" name="category_id" onchange="search.submit();" style="width: 322px; ">
                <option value="1" {if $category_id == 1}selected{/if}>КОРЕНЬ</option>
                {foreach from=$category_list item=category}
                    <option value="{$category.id|escape}" {if $category_id == $category.id}selected{/if}>{""|indent:$category.c_level*7:"&nbsp;"}{$category.title|escape}</option>
                {/foreach}
            </select>
        </div>

    </form>
</div>

<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">

    <tr>
        <th style="width: 40px;">№</th>
        <th style="width: 200px;">Категория</th>
        <th>Шаблон</th>
        <th style="width: 40px;">Ред.</th>
        <th style="width: 40px;">Удал.</th>
    </tr>

    {foreach from=$templates item=template}
        <tr>
            <td align="center">{$template.num|escape}.</td>
            <td align="left">{$template.category_title|escape}</td>
            <td align="center" style="word-break: break-all;">{$template.regexp_template|mb_truncate:50:"...":"UTF-8"|escape}</td>
            <td align="center">
                <a href="?module=canon&action=edit_form&id={$template.id|escape}">
                    <img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" />
                </a>
            </td>
            <td align="center">
                <a href="#" onclick="DeleteTemplate({$template.id|escape}, '{$template.regexp_template|mb_truncate:50:"...":"UTF-8"|escape}');">
                    <img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/>
                </a>
            </td>
        </tr>
    {/foreach}

</table>

{include file='pager.tpl'}