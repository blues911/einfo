{* Smarty *}

<link rel="stylesheet" href="templates/canon/style.css" type="text/css">

<script type="text/javascript" src="templates/canon/script.js"></script>

<h1>Редактирование шаблона</h1>

<form name="edit_canon" id="edit_canon" method="post" enctype="multipart/form-data" action="?module=canon&action=edit">

    <input type="hidden" name="id" value="{$template.id|escape}" />

    <fieldset>

        <legend>Свойства шаблона</legend>

        <div style="position: relative;">

            <div id="canon__category">
                <div id="canon__category_title">Категория</div>
                <div id="canon__category_input">
                    <select id="form_category_id" name="category_id">
                        {foreach from=$category_list item=category}
                            <option value="{$category.id|escape}" {if $template.category_id == $category.id}selected{/if}>{""|indent:$category.c_level*7-7:"&nbsp;"}{$category.title|escape}</option>
                        {/foreach}
                    </select>
                </div>
            </div>

            <div id="canon__template">
                <div id="canon__template_title">
                    Шаблон [<a class="tooltip_anchor" href="javascript:;">?</a>]
                    <div class="tooltip" style="width: 250px;">{literal}
                        Шаблон может состоять из нескольких упрощенных регулярных выражений, разделенных знаком "точка с запятой" (<strong>;</strong>).<br>
                        <br />
                        Справка по синтаксису регулярных выражений:<br />
                        <strong>x</strong> &mdash; символ "x";<br />
                        <strong>[a-x]</strong> &mdash; диапазон символов от "a" до "x" (поддерживается латиница, кириллица и цифры);<br />
                        <strong>[ads]</strong> &mdash; перечисление символов (или "a", или "d", или "s");<br />
                        <strong>[a-dxz]</strong> &mdash; или символ в диапазоне от "a" до "d", или "x", или "z";<br />
                        <strong>{2}</strong> &mdash; указывает, что нужна последовательность двух символов из набора слева (например, [a-c]{2} или z{2});<br />
                        <strong>{1,3}</strong> &mdash; указывает, что нужна последовательность от 1-го до 3-х символов из набора слева (например, [0-9]{1,3} или d{1,3});<br />
                        <strong>(regexp1|regexp2|regexp3)</strong> &mdash; указывает, что в указанном месте будет результат выполнения
                                                                           одного из регулярных выражений, разделенных знаком "вертикальная черта" (<strong>|</strong>).
                    {/literal}</div>
                </div>
                <div id="canon__template_case">
                    регистр:
                    <img src="i/strtolower.png">
                    <a href="javascript:;" onclick="template_change_case('low');">нижний</a>
                    <img src="i/strtoupper.png">
                    <a href="javascript:;" onclick="template_change_case('high');">верхний</a>
                </div>
            </div>

            <div id="canon__template_input">
                <textarea type="text" id="form_regexp_template" name="regexp_template">{$template.regexp_template|escape}</textarea>
            </div>

            <div id="regexp_control">

                <button type="button" onclick="count_variants();">Посчитать раскрытие</button>

                <span id="variant_counter">
                    <span></span>
                    <img id="count_loading" src="i/ajax_loader_transp.gif">
                </span>

                <button type="submit" style="float: right; font-weight: bold;">Сохранить</button>

            </div>

            <div>

                <div class="tab_control" style="padding: 0; margin: 0 -6px 0 0; text-align: right;">
                    <div class="tab tab-active" data-num="1">
                        <a href="javascript:;">Произвольный атом</a>
                    </div>
                    <div class="tab" data-num="2">
                        <a href="javascript:;">Последний атом</a>
                    </div>
                </div>


                <div class="tab_content" data-num="1">

                    <table id="row_collection">
                        <tr id="header_row">
                            <td>Регулярное выражение</td>
                            <td>Номер атома</td>
                            <td></td>
                        </tr>
                        <tr id="original_row" style="display: none;">
                            <td><input id="regexp" type="text" style="width: 300px;" /></td>
                            <td>
                                <a class="minus" href="javascript:;"></a>
                                <input id="atom" type="text" style="width: 50px;" value="1" />
                                <a class="plus" href="javascript:;"></a>
                            </td>
                            <td>
                                <button id="minus" type="button" style="padding: 1px; float: left;"><img src="i/icons/rating_minus.gif"></button>
                                <button id="plus" type="button" style="padding: 1px; float: left;"><img src="i/icons/rating_plus.gif"></button>
                            </td>
                        </tr>
                    </table>

                    <button type="button" onclick="get_atom_tips();" style="margin-left: 0;">Анализировать атомы</button>

                </div>


                <div class="tab_content" data-num="2" hidden>

                    <button type="button" onclick="get_variant_tips();" style="margin-left: 0px;">Подсказать атом</button>

                </div>

            </div>

            <div id="variant_control">

                <select id="atoms_operation" disabled>
                    <option data-action="variant_tips_change_case('high');" data-option="0" selected>Верхний регистр</option>
                    <option data-action="variant_tips_change_case('low');" data-option="0">Нижний регистр</option>
                    <option data-action="remove_atoms_longer(option);" data-option="1">Убрать атомы длиннее N символов</option>
                    <option data-action="remove_atoms_shortly(option);" data-option="1">Убрать атомы короче N символов</option>
                    <option data-action="atoms_preg_match(/[\D]+/);" data-option="0">Убрать атомы только из цифр</option>
                    <option data-action="atoms_preg_match(/[\d]+/);" data-option="0">Убрать атомы только из символов</option>
                    <option data-action="atoms_preg_match(/[a-zA-Z\d]+/);" data-option="0">Убрать кириллицу</option>
                    <option data-action="atoms_preg_match(/[а-яА-ЯёЁ\d]+/);" data-option="0">Убрать латиницу</option>
                </select>

                <input id="atoms_operation_option" type="text" disabled />

                <button type="button" onclick="variant_control_submit();" disabled>OK</button>

                <button type="button" style="float: right;" onclick="append_variants();" disabled>+ добавить</button>

            </div>

            <div id="variant_tips">
                <div id="variant_loading" style="text-align: center; margin: 15px auto;">
                    <img src="i/ajax_loader_transp.gif">
                </div>
                <div id="variant_tips_text"></div>
            </div>

        </div>

    </fieldset>

</form>

