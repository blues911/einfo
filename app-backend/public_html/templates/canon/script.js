var ATOMS = [];
var row_id = 0;

jQuery(document).ready(function()
{
    // Скрываем или показываем поле для ввода количества, при выборе
    // действия над атомами из выпадающего списка.
    var atoms_operation = jQuery('#atoms_operation');
    atoms_operation.change(function()
    {
        var option = jQuery('#atoms_operation_option');

        if (atoms_operation.find(':selected').data('option') == 1)
        {
            option.show();
        }
        else
        {
            option.hide();
        }
    });
    atoms_operation.change();

    // Сабмит формы
    jQuery('#edit_canon').submit(function()
    {
        if (jQuery('#form_regexp_template').val() == "")
        {
            alert('Не заполнено поле "Шаблон"');
            return false;
        }

        return true;
    });

    jQuery('#add_canon').submit(function()
    {
        if (jQuery('#form_regexp_template').val() == "")
        {
            alert('Не заполнено поле "Шаблон"');
            return false;
        }

        return true;
    });

    jQuery('a.tooltip_anchor').mouseover(function()
    {
        jQuery(this).siblings('.tooltip').show();
    });
    jQuery('a.tooltip_anchor').mouseout(function()
    {
        jQuery(this).siblings('.tooltip').hide();
    });

    add_row('original_row');

    jQuery('.tab').click(function()
    {
        var num = jQuery(this).data('num');

        jQuery('.tab_content').hide();
        jQuery('.tab_content[data-num=' + num + ']').show();

        jQuery('.tab').removeClass('tab-active');
        jQuery(this).addClass('tab-active');
    });

    plus_minus_init();

});

function count_variants()
{
    var span = jQuery('#variant_counter span');
    var loading = jQuery('#count_loading');

    loading.show();
    span.hide();

    jQuery.ajax({
        type: 'POST',
        url: '/ajaxer.php?x=canon.ajax_count_variants',
        dataType: 'json',
        data: {'template': jQuery('#form_regexp_template').val() },
        success: function(response)
        {
            if (response.success)
            {
                loading.hide();
                if (response.counter <= 1000000000000)
                {
                    span.show().text(response.formated);
                }
                else
                {
                    span.show().text('> 1 000 000 000 000');
                }
            }
            else
            {
                loading.hide();
                span.show().text('Ошибка');
            }
        },
        error: function()
        {
            loading.hide();
            span.show().text('Ошибка');
        }
    });
}

function get_variant_tips()
{
    var regexp = jQuery('#form_regexp_template').val();
    var output = jQuery('#variant_tips');
    var output_text = jQuery('#variant_tips_text');
    var loading = jQuery('#variant_loading');
    var control = jQuery('#variant_control');

    output_text.hide();
    loading.show();
    output.show();
    control.children().attr('disabled', 'disabled');

    jQuery.ajax({
        type: 'POST',
        url: '/ajaxer.php?x=canon.ajax_get_variant_tips',
        dataType: 'json',
        data: {'regexp': regexp },
        success: function(response)
        {
            if (response.success)
            {
                if (response.error)
                {
                    output_text.text(response.error).show();
                }
                else
                {
                    update_variant_tips(response.variants);
                }
            }
            else
            {
                output_text.text('Непредвиденная ошибка.').show();
            }
            output.show();
            loading.hide();
        },
        error: function ()
        {
            output_text.text('Непредвиденная ошибка').show();
            output.show();
            loading.hide();
        }
    });
}

function update_variant_tips(variants)
{
    var output = jQuery('#variant_tips');
    var output_text = jQuery('#variant_tips_text');
    var control = jQuery('#variant_control');
    var counter_output = jQuery('#atoms_counter');
    var count = variants.length;

    ATOMS = variants;

    if (count > 1)
    {
        output_text.text('(' + implode('|', variants) + ')');
        control.children().removeAttr('disabled');
        counter_output.text(count);
    }
    else if (count == 1)
    {
        output_text.text(variants[0]);
        control.children().removeAttr('disabled');
        counter_output.text(count);
    }
    else
    {
        output_text.text('Нет вариантов.');
        control.children().attr('disabled', 'disabled');
        counter_output.text('');
    }

    output.show();
    output_text.show();
}

function template_change_case(lower_or_upper)
{
    var target_dom = jQuery('#form_regexp_template');
    var text = target_dom.val();

    if (lower_or_upper == 'low')
    {
        text = text.toLowerCase();
    }
    else
    {
        text = text.toUpperCase();
    }

    target_dom.val(text);
}

function variant_control_submit()
{
    var operation = jQuery('#atoms_operation').find(':selected');
    var action = operation.data('action');
    var option_flag = operation.data('option');

    if (option_flag)
    {
        var option = jQuery('#atoms_operation_option').val();
    }

    eval(action);
}

function remove_atoms_longer(option)
{
    for (var i = ATOMS.length; i >= 0; i--)
    {
        if (i in ATOMS)
        {
            if (ATOMS[i].length > option)
            {
                ATOMS.splice(i, 1);
            }
        }
    }

    update_variant_tips(ATOMS);
}

function remove_atoms_shortly(option)
{
    for (var i = ATOMS.length; i >= 0; i--)
    {
        if (i in ATOMS)
        {
            if (ATOMS[i].length < option)
            {
                ATOMS.splice(i, 1);
            }
        }
    }

    update_variant_tips(ATOMS);
}

function atoms_preg_match(option)
{
    for (var i = ATOMS.length; i >= 0; i--)
    {
        if (i in ATOMS)
        {
            if (option.test(ATOMS[i]) == false)
            {
                ATOMS.splice(i, 1);
            }
        }
    }

    update_variant_tips(ATOMS);
}

function variant_tips_change_case(lower_or_upper)
{
    for (var i = ATOMS.length; i >= 0; i--)
    {
        if (i in ATOMS)
        {
            if (lower_or_upper == 'high')
            {
                ATOMS[i] = ATOMS[i].toUpperCase();
            }
            else
            {
                ATOMS[i] = ATOMS[i].toLowerCase();
            }
        }
    }

    update_variant_tips(ATOMS);
}

function append_variants()
{
    var input = document.querySelector('#form_regexp_template');
    var text = input.value + jQuery('#variant_tips_text').text();

    input.value = text;
    input.focus();
    input.selectionStart = input.value.length;
    input.scrollTop = input.scrollHeight;
}


// atom_tips
function add_row(id)
{
    row_id++;

    var new_row = jQuery('#' + id).clone();
    var place = jQuery('#' + id);

    var minus = new_row.find('#minus');
    var plus = new_row.find('#plus');

    new_row.insertAfter(place).show().attr('id', 'row-' + row_id);

    minus.attr('onclick', 'remove_row(\'row-' + row_id + '\');');
    plus.attr('onclick', 'add_row(\'row-' + row_id + '\');');

    plus_minus_init();
}

function count_rows()
{
    return jQuery('#row_collection tr[id^=row-]').length;
}

function remove_row(id)
{
    if (count_rows() > 1)
    {
        jQuery('#' + id).remove();
    }
    else
    {
        jQuery('#' + id + ' #regexp').val('');
        jQuery('#' + id + ' #atom').val('1');
    }
}

function get_atom_tips()
{
    var output = jQuery('#variant_tips');
    var output_text = jQuery('#variant_tips_text');
    var loading = jQuery('#variant_loading');
    var control = jQuery('#variant_control');

    var collection = [];
    var regexp = [];

    output_text.hide();
    output.show();
    loading.show();
    control.children().attr('disabled', 'disabled');

    jQuery('#row_collection tr[id^=row-]').each(function()
    {
        regexp = [];
        regexp[0] = jQuery(this).find('#regexp').val();
        regexp[1] = jQuery(this).find('#atom').val();

        collection.push(regexp);
    });

    jQuery.ajax({
        type: 'POST',
        url: '/ajaxer.php?x=canon.ajax_get_atom_tips',
        dataType: 'json',
        data: { regexp: collection },
        success: function(response)
        {
            if (response.error)
            {
                output_text.text(response.error).show();
            }
            else
            {
                update_variant_tips(response.collection);
            }
            loading.hide();
        },
        error: function()
        {
            loading.hide();
            output_text.text('Непредвиденная ошибка.').show();
        }
    });
}

function plus_minus_init()
{
    jQuery('.plus, .minus').off();

    jQuery('.plus').click(function()
    {
        var input = jQuery(this).siblings('input');
        var value = Number(input.val()) + 1;

        if (isNaN(value) || value < 1)
        {
            value = 1;
        }

        input.val(value);
    });

    jQuery('.minus').click(function()
    {
        var input = jQuery(this).siblings('input');
        var value = Number(input.val()) - 1;

        if (isNaN(value) || value < 1)
        {
            value = 1;
        }

        input.val(value);
    });

    jQuery('.plus, .minus').siblings('input').change(function()
    {
        var input = jQuery(this);
        var value = Number(input.val());

        if (isNaN(value) || value < 1)
        {
            value = 1;
        }

        input.val(value);
    });
}
