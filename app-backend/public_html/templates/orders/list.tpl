<h1>Заявки</h1>

<table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
    <th width="5%">№</th>
    <th>Заявка</th>
    <th width="150">Кол-во компонентов</th>
    <th width="150">Дата</th>
  </tr>
  {foreach from=$list item=item}
  <tr valign="top" >
    <td align="center">{$item.num|escape}.</td>
    <td><a href="?module=orders&action=view-form&order_id={$item.id|escape}">Заявка #{$item.id|escape}</a></td>
    <td align="center">{$item.cnt|escape}</td>
    <td align="center">{$item.date|escape}</td>
  </tr>
  {/foreach}

</table>

{include file="pager.tpl"}