
<h1>Просмотр заявки #{$basket.id|escape}</h1>

<div style="width: 70%; margin: 20px 0; border: 1px solid gray; background-color: #F0F0F0">
    <table width="100%" cellpadding="5" cellspacing="0" rules="rows" class="table-items">
        <tr>
            <td width="150"><strong>Дата заявки</strong></td>
            <td>{$basket.date|escape}</td>
        </tr>
        <tr>
            <td><strong>Контактное лицо</strong></td>
            <td>{$basket.contact_name|escape|default:"&mdash;"}</td>
        </tr>
        <tr>
            <td><strong>Контактный телефон</strong></td>
            <td>{$basket.contact_phone|escape|default:"&mdash;"}</td>
        </tr>
        <tr>
            <td><strong>Контактный email</strong></td>
            <td>{if $basket.contact_email}<a href="mailto:{$basket.contact_email|escape}">{$basket.contact_email|escape}</a>{else}&mdash;{/if}</td>
        </tr>
        <tr>
            <td><strong>Комментарии</strong></td>
            <td>{$basket.comments|escape|default:"&mdash;"|nl2br}</td>
        </tr>
    </table>

</div>

<table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
  <tr>
    <th>Наименование</th>
    <th width="10%" align="center">Произв.</th>
    <th width="15%" align="center">Цена</th>
    <th width="50" align="center">Кол-во</th>
  </tr>
  {foreach from=$basket_items item=item}
  <tr>
    <td>
        <div><strong>{$item.title|escape}</strong></div>
        <div>{$item.descr|escape}</div>
    </td>
    <td>{$item.manuf|escape}</td>
    <td align="center">
        {$item.prices|prepare_prices}
    </td>
    <td align="center">{$item.rcount|escape}</td>
  </tr>
  {/foreach}
</table>


