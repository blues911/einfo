{* Smarty *}

{literal}
    <script type="text/javascript">

        function SubmitForm()
        {
            if (form_comp_like.value == "")
            {
                alert('Не заполнено поле "Условие LIKE"');
                return false;
            }

            $('add_template').submit();
        }

    </script>
{/literal}

<h1>Добавление шаблона</h1>

<form name="add_template" id="add_template" method="post" enctype="multipart/form-data" action="?module=advertising&action=add">

    <fieldset>

        <legend>Свойства шаблона</legend>

        <div style="position: relative; float: left;">

            <div class="form-item">
                * Категория:<br>
                <select id="form_category_id" name="category_id" style="width: 322px;">
                    {foreach from=$category_list item=category}
                        <option value="{$category.id|escape}">{""|indent:$category.c_level*7:"&nbsp;"}{$category.title|escape}</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-item">
                * Условие LIKE:<br>
                <input type="text" id="form_comp_like" name="comp_like" style="width: 322px;" />
            </div>

        </div>

    </fieldset>

    <fieldset>

        <legend>Яндекс.Директ</legend>

        <div style="position: relative; float: left;">

            <div class="form-item">
                Заголовок объявления:<br />
                <input type="text" id="form_yandex_title" name="yandex_title" style="width: 322px;" />
            </div>

            <div class="form-item">
                Описание объявления:<br />
                <input type="text" id="form_yandex_descr" name="yandex_descr" style="width: 620px;" />
            </div>

        </div>

    </fieldset>

    <fieldset>
        <legend>Быстрые ссылки</legend>

        <table style="width: 100%; ">
            <thead>
            <tr>
                <th style="font-weight: normal;">Текст ссылки</th>
                <th style="font-weight: normal;">Адрес ссылки</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input type="text" name="yandex_adtitle_1" style="width: 310px;" /></td>
                <td><input type="text" name="yandex_adlink_1" style="width: 310px;" /></td>
            </tr>
            <tr>
                <td><input type="text" name="yandex_adtitle_2" style="width: 310px;" /></td>
                <td><input type="text" name="yandex_adlink_2" style="width: 310px;" /></td>
            </tr>
            <tr>
                <td><input type="text" name="yandex_adtitle_3" style="width: 310px;" /></td>
                <td><input type="text" name="yandex_adlink_3" style="width: 310px;" /></td>
            </tr>
            <tr>
                <td><input type="text" name="yandex_adtitle_4" style="width: 310px;" /></td>
                <td><input type="text" name="yandex_adlink_4" style="width: 310px;" /></td>
            </tr>
            </tbody>
        </table>
    </fieldset>

    <div class="form-item">
        <button onclick="SubmitForm();" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить шаблон</button>
        <button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>
    </div>

</form>