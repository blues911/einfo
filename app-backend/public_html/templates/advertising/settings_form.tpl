{* Smarty *}

{literal}
    <script type="text/javascript">

        function SubmitForm()
        {
            if (yandex_banner_title.value == "" ||
                yandex_banner_descr.value == "" ||
                yandex_banner_price.value == "" ||
                yandex_campaign_prefix.value == "" ||
                yandex_catalog_path.value == "" ||
                yandex_application_id.value == "" ||
                yandex_application_pwd.value == "" ||
                yandex_login.value == "" ||
                yandex_token.value == "")
            {
                alert('Заполните все поля');
                return false;
            }

            $('settings').submit();
        }

    </script>
{/literal}

<h1>Настройки контекстной рекламы</h1>



<form name="settings" id="settings" method="post" enctype="multipart/form-data" action="?module=advertising&action=settings_edit">

    <fieldset>

        <legend>Системные настройки Яндекс.Директ</legend>

        <div style="position: relative; float: left;">

            <div class="form-item">
                * banner_title:<br>
                <input type="text" id="yandex_banner_title" name="yandex_banner_title" value="{$settings.yandex.banner_title|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * banner_descr:<br>
                <input type="text" id="yandex_banner_descr" name="yandex_banner_descr" value="{$settings.yandex.banner_descr|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adtitle_1:<br>
                <input type="text" id="yandex_adtitle_1" name="yandex_adtitle_1" value="{$settings.yandex.yandex_adtitle_1|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adlink_1:<br>
                <input type="text" id="yandex_adlink_1" name="yandex_adlink_1" value="{$settings.yandex.yandex_adlink_1|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adtitle_2:<br>
                <input type="text" id="yandex_adtitle_2" name="yandex_adtitle_2" value="{$settings.yandex.yandex_adtitle_2|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adlink_2:<br>
                <input type="text" id="yandex_adlink_2" name="yandex_adlink_2" value="{$settings.yandex.yandex_adlink_2|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adtitle_3:<br>
                <input type="text" id="yandex_adtitle_3" name="yandex_adtitle_3" value="{$settings.yandex.yandex_adtitle_3|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adlink_3:<br>
                <input type="text" id="yandex_adlink_3" name="yandex_adlink_3" value="{$settings.yandex.yandex_adlink_3|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adtitle_4:<br>
                <input type="text" id="yandex_adtitle_4" name="yandex_adtitle_4" value="{$settings.yandex.yandex_adtitle_4|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                yandex_adlink_4:<br>
                <input type="text" id="yandex_adlink_4" name="yandex_adlink_4" value="{$settings.yandex.yandex_adlink_4|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * banner_price:<br>
                <input type="text" id="yandex_banner_price" name="yandex_banner_price" value="{$settings.yandex.banner_price|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * campaign_prefix:<br>
                <input type="text" id="yandex_campaign_prefix" name="yandex_campaign_prefix" value="{$settings.yandex.campaign_prefix|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * catalog_path:<br>
                <input type="text" id="yandex_catalog_path" name="yandex_catalog_path" value="{$settings.yandex.catalog_path|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * yandex_application_id:<br>
                <input type="text" id="yandex_application_id" name="yandex_application_id" value="{$settings.yandex.yandex_application_id|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * yandex_application_pwd:<br>
                <input type="text" id="yandex_application_pwd" name="yandex_application_pwd" value="{$settings.yandex.yandex_application_pwd|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * yandex_login:<br>
                <input type="text" id="yandex_login" name="yandex_login" value="{$settings.yandex.yandex_login|escape}" style="width: 322px;" />
            </div>

            <div class="form-item">
                * yandex_token:<br>
                <input type="text" id="yandex_token" name="yandex_token" value="{$settings.yandex.yandex_token|escape}" style="width: 322px;" /><br>
                <a href="https://oauth.yandex.ru/authorize?response_type=code&client_id={$settings.yandex.yandex_application_id|escape}">Получить токен</a>&nbsp;
                <span style="color: red;">{$token_error|escape}</span><span style="color: green;">{$token_info|escape}</span>
            </div>

        </div>

    </fieldset>

    <div class="form-item">
        <button onclick="SubmitForm();" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
        <button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>
    </div>

</form>