<link href="templates/news/style.css" rel="stylesheet" type="text/css" />
<script src="templates/news/script.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	EnableEditor('text');
</script>


<h1>Редактирование новости</h1>


<form name="edit_news" id="edit_news" method="post" enctype="multipart/form-data" action="?module=news_mod&action=edit&id={$content.id|escape}">
	
	<div class="form-item" style="font-size: 15px; margin-bottom: 20px">
		<strong>Статус:</strong> 
		<input name="status" type="radio" value="1" {if $content.status == 1}checked{/if} /><span style="color: green">опубликовать</span>  
		<input name="status" type="radio" value="0" {if $content.status == 0}checked{/if} /><span style="color: blue">ждет проверки</span>  
		<input name="status" type="radio" value="-1" {if $content.status == -1}checked{/if} /><span style="color: red">отклонить</span>
		
	</div>

	
	<div class="form-item">
		Заголовок:<br />
		<input name="title" id="title" type="text" style="width: 550px" value="{$content.title|escape}" />
	</div>
	
	<div class="form-item" style="margin-top: 20px">
		Полный текст новости:<br />
		<textarea name="text" id="text" rows="20" style="width: 550px" >{$content.text|escape}</textarea>
	</div>					
						
	
	<div class="form-item" style="margin: 20px 0">
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="110" valign="top" >
			{if $content.img != ""}
				<img src="{$content.img|escape}" width="92" id="preview_img"/>
			{else}
				<img src="i/no_image.jpg" width="92" id="preview_img"/>
			{/if}			
				
			</td>
			<td valign="middle">
				Загрузить изображение:<br />
				<input name="img" id="img" type="file" style="width: 90%" onChange="PreviewImg('img')" /><br />
				<div class="prompt">Разрешается загружать изображения только следующих форматов &mdash; {$img_allow_formats|upper|escape}.</div>
			</td>
		  </tr>
		</table>
	</div>				
	
	<br />
	
	<div class="form-item">
		<button type="submit"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
	</div>
		
  
</form>
 