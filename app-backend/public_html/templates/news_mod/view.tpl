{literal}
<script>
function SubmitForm(id)
{
	document.location = '?module=news_mod&action=changestatus&status=' + GetCheckedValue($('changestatus').status) + '&id=' + id;
}
</script>
<style>
#view_text {
	border: 1px solid #B9B9B9;
	background-color: #F4F4F4;
	padding: 10px;
	font-size: 12px;
}
#view_text a:link, a:visisted {
	text-decoration: underline;
}
#view_text a:hover {
	text-decoration: none;
}
#user {
	margin: 20px 0 4px 0;
}
#user span {
	padding: 5px 10px;
	border: 1px solid #B9B9B9;
	border-bottom: none;
	background-color: #E9E9E9;
}
</style>
{/literal}

<h1>Просмотр новости</h1>

<div >
	<form name="changestatus" id="changestatus" action="#" method="get">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td style="font-size: 15px">
				<input name="status" type="radio" value="1" {if $content.status == 1}checked{/if} /><span style="color: green">опубликовать</span>  
				<input name="status" type="radio" value="0" {if $content.status == 0}checked{/if} /><span style="color: blue">ждет проверки</span>  
				<input name="status" type="radio" value="-1" {if $content.status == -1}checked{/if} /><span style="color: red">отклонить</span>

				&nbsp;<input name="save" type="button" value="Сохранить" onclick="SubmitForm({$content.id|escape})" />
				
			</td>
			<td align="right"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" align="absmiddle" /> <a href="?module=news_mod&action=edit-form&id={$content.id|escape}">редактировать</a></td>
		  </tr>
		</table>		
	</form> 
</div>

<div class="spacer"></div>	

<div id="user"><span ><strong>Компания:</strong> <a href="?module=users&action=edit-form&id={$content.user_id|escape}">{$content.user_title|escape}</a></span></div>
<div id="view_text">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 10px">
  <tr>
    {if $content.img != ""}<td width="90" valign="top"><img src="{$content.img|escape}" width="80" align="left" style="margin: 0 10px 10px 0"></td>{/if}
    <td valign="top"><h2>{$content.title|escape}</h2></td>
  </tr>
</table>
{$content.text nofilter}
</div>
