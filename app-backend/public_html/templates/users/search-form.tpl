{literal}
<script language="javascript">
function DeleteUser(id)
{
	if(confirm('Вы действительно хотите удалить пользователя ?'))
	{
		document.location="?module=users&action=delete&id=" + id;
	}
}

// ------------ ajax ---------------
Event.observe(window, 'load', function() {
	Event.observe('search', 'submit', function(evt) {
		Search();
		Event.stop(evt);
	}.bindAsEventListener(window));
});

function Search()
{
	if ($F('q') != "") request = $F('q');
	else
	{
		alert("Заполните поле для поиска");
		request = false;
	}
	
	if (request)
	{
		$('search_process').show();
		new Ajax.Request('ajaxer.php?x=users.ajax_search', {
			 method: 'get',			 
			 parameters: { q: $F('q')},
			 onSuccess: function(transport)
			 {
			 	$('search_process').hide();
				$('search_result').update(transport.responseText);
			 }						 
		});
		
	}

	return false;			
}
 
</script>
{/literal}

<h1>Поиск пользователей</h1>

<form name="search" id="search" method="post" action="#" >
	<div  style="margin: 10px 0px 20px 0px">
		Поисковый запрос: <input name="q" id="q" type="text" style="width: 250px" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<button onclick="Search()" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Найти</button>
	</div>		
</form>
<div class="spacer"></div>
<div align="center" id="search_process" style="display: none; margin: 10px 0px 10px 0px"><img src="i/icons/icon_process.gif" align="absmiddle" hspace="10" /> Идет поиск &#8230;</div>
<div id="search_result"></div>

