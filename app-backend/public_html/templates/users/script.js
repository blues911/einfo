// отправка формы
function Mailing(){
	filled = ['mailing_from_name', 'mailing_from_email', 'mailing_subject'];
	if (CheckFieldsForm(filled)) {
		$('mailing-form').submit();
	}
	else {
		alert("Не все обязательные поля заполнены");
	}
}


// фильтрация адресатов
function MailingFilter() {
    $('mailing_filter').disable();

    new Ajax.Request('ajaxer.php?x=users.ajax_mailing_filter', {
        parameters: {
            filter: $F('mailing_filter')
        },
        onSuccess: function(transport) {            
            var ids = transport.responseText.split(',');
            var filter_bool = GetCheckedValue($('mailing-form').mailing_filter_bool);
            ids.each(function(s) {
                if (filter_bool == 1) {
                    $('user_sel[' + s + ']').checked = true;
                }
                else {
                    $('user_sel[' + s + ']').checked = false;
                }
            });
            $('mailing_filter').enable();
        }
	});
}

// добавление полей для пользовательских файлов
function MoreBlocks(readBlock, writeBlock) {
	var newFields = $(readBlock).cloneNode(true);
	newFields.id = '';
	newFields.style.display = 'block';

	var insertHere = $(writeBlock);
	insertHere.parentNode.insertBefore(newFields,insertHere);
}

function UsersDocDelete(id) {
	if(confirm('Вы действительно хотите удалить данный файл?'))	{
		new Ajax.Request('ajaxer.php?x=users.ajax_delete_doc', {
             method: 'post',
             parameters: { id: id },
             onSuccess: function(transport) {
                if (transport.responseText == 'OK') {
                    $('userdoc_' + id).hide();
                }
                else {
                    alert("При удалении произошла ошибка, попробуйте еще раз.");
                }
             }
        });
	}
}

function UsersDocShow(obj) {
    $(obj).next().value = obj.checked ? 1 : 0;
}