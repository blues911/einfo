{literal}
<script type="text/javascript" src="templates/users/script.js"></script>
<script>
function SubmitForm()
{
	if ($F('title') == "")
	{
		alert('Не заполнено поле "Название компании / ФИО"');
		return false;
	}

	if ($F('login') == "")
	{
		alert('Не заполнено поле "Логин"');
		return false;
	}

    if ($F('email') == "")
    {
        alert('Не заполнено поле "Электронная почта (показывается на сайте)"');
        return false;
    }

    if ($F('email_price_notification') == "")
    {
        alert('Не заполнено поле "Электронная почта для уведомлений об обработке прайса"');
        return false;
    }

    if ($F('email_order_notification') == "")
    {
        alert('Не заполнено поле "Электронная почта для уведомлений о заказах"');
        return false;
    }

	if ($('level') && $F('level') == 0)
	{
		alert('Не заполнено поле "Уровень доступа"');
		return false;
	}

	$('edit_users').submit();
}

function UpdateCountry() {
    if ($F('country_id') == 186) {
        $('city_id').enable();
    }
    else {
        $('city_id').disable();
        $('city_id').selectedIndex = 0;
    }
}

function show_password()
{
    jQuery('#change_password').hide();
    jQuery('#password').show().removeAttr('disabled');
    jQuery('#password_prompt').show();
}

</script>
{/literal}


<h1>Редактирование пользователя</h1>

{if $content.status == 1}
<div align="right">
	<img src="i/icons/icon_key.gif" width="18" height="18" border="0" alt="Залогиниться под пользователем" align="absmiddle" />
	<a href="?module=users&action=login&id={$content.id|escape}">Залогиниться под пользователем</a>
</div>
{/if}
<input type="text" id="user_id" value="{$content.id|escape}" hidden=""/>
<form name="edit_users" id="edit_users" method="post" enctype="multipart/form-data" action="?module=users&action=edit&id={$content.id|escape}">

	<fieldset>
		<legend>Статус в системе </legend>

		<div class="form-item"  style="margin-top: 10px;">
			<strong>Дата регистрации:</strong> {$content.register_date|escape}
		</div>

		{if $content.level_id != 0}
		<div class="form-item">
            {if $content.status == -2}<div style="color: gray; margin-bottom: 5px">Ожидает валидации аккаунта...</div>{/if}
			<strong>Статус:</strong> &nbsp;
			<input name="status" type="radio" value="-1" {if $content.status == -1}checked{/if} /> ожидает модерации &nbsp;&nbsp;
			<input name="status" type="radio" value="0" {if $content.status == 0}checked{/if} /> заблокированный &nbsp;&nbsp;
			<input name="status" type="radio" value="1" {if $content.status == 1}checked{/if} /> активный &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</div>
		{/if}

		<div class="form-item">
			* Название компании / ФИО:<br />
			<input name="title" id="title" type="text" style="width:322px" value="{$content.title|escape}" />
		</div>

		<div class="form-item">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr valign="top">
				<td style="padding-right: 15px;" width="30%">* Логин:<br /><input name="login" id="login" type="text" style="width:100%" value="{$content.login|escape}" /></td>
				<td style="padding-right: 15px;" width="30%">
					Пароль:<br />
                    <span id="change_password" onclick="show_password();" style="cursor: pointer; text-decoration: none; border-bottom: 1px dashed; color: #0C57A6;">Изменить пароль</span>
                    <input style="display: none;" disabled name="password" id="password" type="text" style="width:100%" />
                    <div id="password_prompt" class="prompt" style="display: none;">(текущий пароль не отображается, для его изменения просто введите новый)</div>
				</td>
				<td>* Уровень доступа:<br />
				{if $content.level_id == 0}
					<strong>Администратор</strong>
				{else}
					<select name="level" id="level">
						{foreach from=$levels item=item}
						<option value="{$item.id|escape}" {if $content.level_id==$item.id}selected{/if}>{$item.title|escape}</option>
						{/foreach}
					</select>
				{/if}

				</td>
			  </tr>
			</table>
		</div>

        <div>
            Буфферная таблица: {$content.buffer_table|escape}
        </div>
	</fieldset>

	<fieldset>
		<legend>Общая информация</legend>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;">
                    Страна:<br />
                    <select id="country_id" name="country_id" style="width: 150px" onchange="UpdateCountry()">
                        <option value="0">не определена</option>
                    {foreach from=$countries item=item}
                        <option value="{$item.country_id|escape}" {if $item.country_id == $content.country_id}selected{/if}>{$item.country|escape}</option>
                    {/foreach}
                    </select>
                </td>
				<td style="padding-right: 15px;">
                    Город:<br />
                    <select id="city_id" name="city_id" style="width: 150px">
                        <option value="0">не определен</option>
                    {foreach from=$cities item=item}
                        <option value="{$item.city_id|escape}" {if $item.city_id == $content.city_id}selected{/if}>{$item.city|escape}</option>
                    {/foreach}
                    </select>
                </td>
				<td >Адрес:<br /><input name="address" id="address" type="text" style="width:250px"  value="{$content.address|escape}" /></td>
			  </tr>
			</table>
            <script type="text/javascript">
                UpdateCountry();
            </script>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="padding-right: 15px;">Основной телефон:<br /><input name="phone1" id="phone1" type="text" style="width:165px" value="{$content.phone1|escape}" /></td>
					<td style="padding-right: 15px;">Добавочный номер:<br /><input name="phone1_ext" type="text" id="phone1_ext" style="width:165px" value="{$content.phone1_ext|escape}" /></td>
				</tr>
			</table>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="padding-right: 15px;">Дополнительный телефон:<br /><input name="phone2" id="phone2" type="text" style="width:165px" value="{$content.phone2|escape}" /></td>
					<td style="padding-right: 15px;">Добавочный номер:<br /><input name="phone2_ext" id="phone2_ext" type="text" style="width:165px" value="{$content.phone2_ext|escape}" /></td>
				</tr>
			</table>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;">Факс:<br /><input name="fax" id="fax" type="text" style="width:165px" value="{$content.fax|escape}" /></td>
				<td >* Электронная почта (показывается на сайте):<br /><input name="email" id="email" type="text" style="width:170px" value="{$content.email|escape}" /></td>
			  </tr>
			</table>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;">{if $content.www}<a href="{$content.www|escape}" target="_blank">{/if}Сайт{if $content.www}</a>{/if}:<br /><input name="www" id="www" type="text" style="width:165px" value="{$content.www|escape}" /></td>
				<td style="padding-right: 15px;">ICQ:<br /><input name="icq" id="icq" type="text" style="width:165px" value="{$content.icq|escape}" /></td>
				<td >Skype:<br /><input name="skype" id="skype" type="text" style="width:165px" value="{$content.skype|escape}" /></td>
			  </tr>
			</table>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
        <td style="padding-right: 15px;">Viber:<br /><input name="viber" id="viber" type="text" style="width:165px" value="{$content.viber|escape}" /></td>
        <td style="padding-right: 15px;">WhatsApp:<br /><input name="whatsapp" id="whatsapp" type="text" style="width:165px" value="{$content.whatsapp|escape}" /></td>
        <td >Telegram:<br /><input name="telegram" id="telegram" type="text" style="width:165px" value="{$content.telegram|escape}" /></td>
			  </tr>
			</table>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
        <td style="padding-right: 15px;">Facebook:<br /><input name="facebook" id="facebook" type="text" style="width:165px" value="{$content.facebook|escape}" /></td>
        <td style="padding-right: 15px;">ВК:<br /><input name="vk" id="vk" type="text" style="width:165px" value="{$content.vk|escape}" /></td>
        <td >Instagram:<br /><input name="instagram" id="instagram" type="text" style="width:165px" value="{$content.instagram|escape}" /></td>
			  </tr>
			</table>
		</div>

    <div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;">ИНН:<br /><input name="inn" id="inn" type="text" style="width:165px" value="{$content.inn|escape}" /></td>
				<td style="padding-right: 15px;">КПП:<br /><input name="kpp" id="kpp" type="text" style="width:165px" value="{$content.kpp|escape}" /></td>
				<td >ОГРН/ОГРНИП:<br /><input name="ogrn" id="ogrn" type="text" style="width:210px" value="{$content.ogrn|escape}" /></td>
			  </tr>
			</table>
		</div>

        <div class="form-item">
			Юридический адрес:<br />
            <input name="address_jur" id="address_jur" type="text" style="width:540px" value="{$content.address_jur|escape}" />
		</div>

    <div class="form-item">
			Адрес доставки почтовой корреспонденции:<br />
      <input name="address_post" id="address_post" type="text" style="width:540px" value="{$content.address_post|escape}" />
		</div>

		<div class="form-item">
			Информация о компании:<br />
			<textarea name="about" id="about" rows="4" style="width:98%">{$content.about|escape}</textarea>
		</div>

        <div class="form-item">
            <input type="checkbox" id="individual_buyers" name="individual_buyers" value="1" {if $content.individual_buyers}checked{/if} />
            работа с физическими лицами
        </div>

        <div class="form-item">
            Минимальная сумма заказа:<br />
            <input name="min_order_amount" id="min_order_amount" type="text" style="width: 200px;" value="{$content.min_order_amount|escape}" />
        </div>

        <div class="form-item">
            <input type="checkbox" id="allow_download_price" name="allow_download_price" value="1" {if $content.allow_download_price}checked{/if} />
            разрешать скачивать прайс
        </div>

        <div class="form-item">
            Цена включает НДС:
            <label style="display: inline-block;"><input type="radio" name="price_with_nds" value="1" {if $content.price_with_nds != 0}checked{/if}> да</label>
            <label style="display: inline-block;"><input type="radio" name="price_with_nds" value="0" {if $content.price_with_nds == 0}checked{/if}> нет</label>
        </div>
    </fieldset>

    <fieldset>
        <legend>Уведомления</legend>

        <div class="form-item">
            * Электронная почта для уведомлений об обработке прайса:<br />
            <input name="email_price_notification" id="email_price_notification" type="text" style="width: 170px;" value="{$content.email_price_notification|escape}" />
        </div>

        <div class="form-item">
            * Электронная почта для уведомлений о заказах:<br />
            <input name="email_order_notification" id="email_order_notification" type="text" style="width: 170px;" value="{$content.email_order_notification|escape}" />
        </div>

        <div class="form-item">
            <input id="email_notification" type="checkbox" value="1" name="email_notification" {if $content.email_notification}checked{/if} />
            уведомления на эл. почту
        </div>
    </fieldset>

	<fieldset>
		<legend>Контактная персона</legend>

		<div class="form-item">
			ФИО:<br /><input name="person_fio" id="person_fio" type="text" style="width:90%" value="{$content.person_fio|escape}" />
		</div>

		<div class="form-item">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;" width="35%">{if $content.person_email}<a href="mailto://{$content.person_email|escape}?subject=[einfo] Сообщение администратора.&body=Здравствуйте, {$content.person_fio|escape}.">{/if}Электронная почта{if $content.person_email}</a>{/if}:<br /><input name="person_email" id="person_email" type="text" style="width:100%" value="{$content.person_email|escape}" /></td>
				<td style="padding-right: 15px;">Дополнительная информация:<br /><input name="person_info" id="person_info" type="text" style="width:100%" value="{$content.person_info|escape}" /></td>
			  </tr>
			</table>
		</div>
	</fieldset>


	<fieldset>
		<legend>Документы пользователя</legend>
        <div class="form-item">
            <ol>
            {foreach from=$users_docs item=item}
                <li id="userdoc_{$item.id|escape}">
                    <input type="checkbox" name="docs_edit_show_x[{$item.id|escape}]" value="1" title="Показывать на сайте" onclick="UsersDocShow(this)" {if $item.fshow}checked{/if} />
                    <input type="hidden" name="docs_edit_show[{$item.id|escape}]" value="{if $item.fshow}1{else}0{/if}" />
                    <a href="http://{$users_docs_url|escape}/{$item.file|escape}" target="_blank">{if $item.title}{$item.title|escape}{else}{$item.file|escape}{/if}</a> / <span class="grey_link" style="font-size: 9px"><a href="javascript: UsersDocDelete({$item.id|escape});">удалить</a></span>
                </li>
            {/foreach}
            </ol>

            <div id="readblock_docs" style="display: none">
                <input type="checkbox" name="docs_show_x[]" value="1" title="Показывать на сайте" onclick="UsersDocShow(this)" />
                <input type="hidden" name="docs_show[]" value="0" />
                <input type="text" name="docs_titles[]" value="" style="width: 250px "/>
                <input type="file" name="docs[]"  /> <img src="i/icons/btn_cancel.gif" height="15" width="15" alt="Удалить" style="cursor: pointer" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" >
            </div>
            <div id="writeblock_docs"></div>
        </div>
        <div><a href="javascript: MoreBlocks('readblock_docs', 'writeblock_docs');" >+ Добавить</a></div>
	</fieldset>


	<fieldset>
		<legend>API</legend>

		<div class="form-item">
            Разрешенные IP-адреса для выгрузки заказов через API:<br /><input name="api_allowed_ip" id="api_allowed_ip" type="text" value="{$content.api_allowed_ip|escape}" style="width:95%" />
            <small>Перечислять через запятую, если поле пустое, то выгрузка разрешена на любой IP-адрес</small>
		</div>

	</fieldset>


	<fieldset>
		<legend>Разное</legend>

		<div class="form-item">
			Максимальное количество строк в прайс-листе:<br /><input name="price_limit" id="price_limit" type="text" value="{$content.price_limit|escape}" style="width:50%" />
		</div>

		<div class="form-item">
			Комментарии к пользователю:<br />
			<textarea name="comment" id="comment" rows="4" style="width:95%">{$content.comment|escape}</textarea>
		</div>

	</fieldset>

	<br />

	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
	</div>
</form>

{literal}
    <script type="text/javascript">
        jQuery(function()
        {
            jQuery('#phone1, #phone2, #fax, #viber, #whatsapp').mask('+7 (999) 999-99-99');
        });

        jQuery('#phone1_ext, #phone2_ext').on('input paste change blur', function()
        {
            var elem = jQuery(this);

            elem.val(String(elem.val()).replace(/[^\d]/g, '').substr(0, 4));
        });

        // Проверка логина на уникальность
        jQuery(document).ready(function(){
            jQuery('#login').on('change', function()
            {
                var login = jQuery(this);
                var id = jQuery(user_id);
                console.log(id.val());

                new Ajax.Request('ajaxer.php?x=users.ajax_check_login', {
                    method: 'post',
                    parameters: { login: login.val(), id: id.val() },
                    onSuccess: function(data) {
                        if (data.responseText !== 'OK') {
                            alert("Данный логин уже используется в системе, выберите другой");
                            login.addClass('f_incorrect');
                        } else {
                            login.removeClass('f_incorrect');
                        }
                    }
                });
            });
        });
    </script>
{/literal}
