<link rel="stylesheet" type="text/css" href="templates/users/style.css" media="all"/>
<script type="text/javascript" src="templates/users/script.js"></script>
<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>

<script language="javascript" type="text/javascript">
	EnableEditor('mailing_text');
</script>


<h1>Рассылка</h1>

<form name="mailing-form" id="mailing-form" method="post" enctype="multipart/form-data" action="?module=users&action=mailing">

	<div class="form-item">

	<fieldset>
		<legend>Параметры электронного письма</legend>
            <table width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td width="50%">
                        Имя отправителя *:<br />
                        <input name="mailing_from_name" id="mailing_from_name" type="text" style="width: 98%" value="Anton Drokin" />
                    </td>
                    <td>
                        E-mail отправителя *:<br />
                        <input name="mailing_from_email" id="mailing_from_email" type="text" style="width: 98%" value="admin@einfo.ru" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Тема письма *:<br />
                        <input name="mailing_subject" id="mailing_subject" type="text" style="width: 99%" value="[einfo.ru] - сообщение администратора"/>
                    </td>
                </tr>
            </table>
        </fieldset>
	</div>

    <div class="form-item">
        <div style="margin: 10px 0">
            Фильтры выборки адресатов:<br />
            <select name="mailing_filter" id="mailing_filter">
                <option value="all">ВСЕ</option>
                <option value="active">активные</option>
                <option value="locked">заблокированные</option>
                <option value="wait">ожидающие модерации</option>
                <option value="price_update_month">прайс-лист не обновлялся более 1 месяца</option>
            </select> &nbsp;
            <label for="bool_true"><input type="radio" name="mailing_filter_bool" id="bool_true" value="1" checked /> выбрать</label>
            <label for="bool_false"><input type="radio" name="mailing_filter_bool" id="bool_false" value="0" /> снять</label> &nbsp;&nbsp;&nbsp;
            <button type="button" onclick="MailingFilter()">Фильтр</button>
        </div>
        
        <div id="users_list">
            <table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
              <tr align="center">
                <th width="5%">№</th>
                <th >Пользователи</th>
                <th width="50">ID</th>
                <th width="80">E-mail</th>
                <th width="50">Статус</th>
              </tr>
              {foreach from=$users_list item=item}
              <tr valign="top" {if $item.level_id == 0}style="background-color: #E9E9E9"{/if}>
                <td align="center" style="padding-right: 5px;"><input type="checkbox" name="user_sel[{$item.id|escape}]" id="user_sel[{$item.id|escape}]" value="{$item.id|escape}" /></td>
                <td><a href="?module=users&action=edit-form&id={$item.id|escape}">{$item.title|escape}</a> <span style="color: gray">[{$item.login|escape}]</span></td>
                <td align="center">{$item.id|escape}</td>
                <td align="center">{$item.email|escape}</td>
                <td align="center">
                    {if $item.status == -1}
                    <img src="i/icons/icon_moder.gif" border="0" alt="Ожидает модерации" title="Ожидает модерации" />
                    {elseif $item.status == 0}
                    <img src="i/icons/icon_ok_inactive.gif" border="0" alt="Заблокирован" title="Заблокирован" />
                    {else}
                    <img src="i/icons/icon_ok_active.gif" border="0" alt="Активный" title="Активный" />
                    {/if}
                </td>
              </tr>
              {/foreach}
            </table>
        </div>
    </div>

	<div class="form-item" style="margin-top: 20px">
		Текст рассылки:<br />
		<textarea name="mailing_text" id="mailing_text" rows="15" style="width: 98%" >
            Здравствуйте!<br />
			<br />
			<br />
			<br />---<br />
            С уважением, Антон Павлович Дрокин<br />
			администратор einfo.ru.<br />
			тел. (499) 579-8509<br />
        </textarea>
	</div>

    <br />
    <br />

	<div class="form-item">
		<button onclick="Mailing()" type="button" ><img src="i/icons/icon_mail.gif" width="15" height="18" hspace="5" align="absmiddle" />Осуществить рассылку</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>
	</div>

</form>

