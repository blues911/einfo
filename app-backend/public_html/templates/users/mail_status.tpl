Здравствуйте!

Статус Вашего аккаунта (ID {$id} - {$user.title|replace:"&quot;":"\""|escape}) поменялся на "{if $user.status == -1}Ожидает модерации{elseif $user.status == 0}Заблокирован{elseif $user.status == 1}Активный{elseif $user.status == -2}Ожидает валидации{/if}".
Прежний статус был "{if $status_old == -1}Ожидает модерации{elseif $status_old == 0}Заблокирован{elseif $status_old == 1}Активный{elseif $status_old == -2}Ожидает валидации{/if}".

--
C уважением,
Администрация www.einfo.ru
