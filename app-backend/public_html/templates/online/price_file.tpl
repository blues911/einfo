{literal}

<style>
    #xml-log {
        padding: 5px 10px;
        background-color: #F5F5F5;
        font-family: monospace;
    }
</style>

<script type="text/javascript">

function TestXml(url) {
	if (CheckFieldsForm(['price_file'])) {
        if ($F('price_file') != '') {

            $('xml-log').hide();
            $('xml-log-loading').show();
            new Ajax.Request('ajaxer.php?x=online.ajax_price_test', {
                 method: 'get',
                 parameters: {
                     url: $F('price_file'),
                     type: $F('price_file_type'),
                     login: $F('price_login'),
                     pass: $F('price_password')
                 },
                 onSuccess: function(transport)
                 {
                     $('xml-log-loading').hide();
                     $('xml-log').show();
                     $('xml-log').update(transport.responseText);
                 }
            });
            
        }
        else alert("Вы ввели некорректный URL файла");
	}
	else {
		alert("Не все обязательные поля заполнены");
	}
}

function SubmitForm()
{
    if (CheckFieldsForm(['price_file'])) {
        if ($F('price_file') != '') {
            $('price_file-form').submit();
        }
        else alert("Вы ввели некорректный URL файла");
    }
    else {
        alert("Не все обязательные поля заполнены");
    }
}

</script>
{/literal}

<h1>Прайс-файлы</h1>

<div class="message" style="margin: 5px 0 20px 0; text-align: left;">

<ul>
    <li style="margin: 0 0 5px;">Укажите полный URL файла с прайс-листом в формате XML или JSON</li>
    <li style="margin: 0 0 5px;">Укажите логин и пароль в случае, если доступ к файлу требует аутентификации. Если аутентификации не требуется, оставьте эти поля пустыми</li>
    <li style="margin: 0 0 5px;">Выберите формат файла (по <a href="http://www.einfo.su/?module=cp&action=help&doc=online.price_file">спецификации einfo.ru</a> или <a href="http://www.efind.ru/services/partnership/online/specs/">efind.ru</a>)</li>
    <li style="margin: 0 0 5px;">Файл должен содержать полностью весь склад</li>
    <li style="margin: 0 0 5px;">Файл в XML формате должен содержать не более 300000 позиций, большие количества передаются только в JSON формате</li>
    <li style="margin: 0 0 5px;">Данные забираются автоматически дважды в сутки</li>
    <li style="margin: 0 0 5px;">При онлайн загрузке через XML или JSON все ранее загруженные позиции из базы данных удаляются полностью</li>
    <li style="margin: 0 0 5px;">Тестирование файлов большого объема может занять продолжительное время (1млн. позиций тестируется около 5 минут)</li>
    <li style="margin: 0 0 5px;">
        XML, JSON файл может быть как статичным файлом, так и генерироваться в реальном времени простейшим скриптом
        (<a href="https://www.einfo.ru/media/export_to_xml.phps" target="_blank">пример XML скрипта</a>,
        <a href="https://www.einfo.ru/media/export_example.xml" target="_blank">пример XML файла</a>,
        <a href="https://www.einfo.ru/media/export_to_json.phps" target="_blank">пример JSON скрипта</a>,
        <a href="https://www.einfo.ru/media/export_example.json" target="_blank">пример JSON файла</a>).
        Скрипты примеров могут быть бесплатно, при наличии технических возможностей, адаптированы под ваши условия. Потребуется доступ к вебсайту и серверу баз данных на вашем сайте. За подробностями обращайтесь к администратору einfo.ru.
    </li>
    <li style="margin: 0 0 5px;">В случае затруднений обратитесь к администратору einfo.ru, либо программисту обслуживающему вашу систему</li>
</ul>

</div>


<div class="form-item">
	<form action="?module=online&action=price_file" id="price_file-form" name="price_file-form" method="post" enctype="multipart/form-data">

        <table width="100%" cellpadding="4" cellspacing="0" border="0" style="margin-bottom: 10px">
            <tr>
                <td style="width: 270px;">
                    Укажите путь к файлу:<br />
                    <input type="text" id="price_file" name="price_file" value="{$user.price_file_url|escape}" style="width: 98%;max-width: 245px;" onkeypress="return DisableEnterKey(event)"/>
                </td>
                <td style="width: 50px">
                    Формат:<br />
                    <select id="price_file_type" name="price_file_type" style="margin-left: 4px;">
                        <option value="einfo_xml" {if $user.price_file_type == 'einfo_xml'}selected="selected"{/if}>einfo.ru xml</option>
                        <option value="einfo_json" {if $user.price_file_type == 'einfo_json'}selected="selected"{/if}>einfo.ru json</option>
                        <option value="efind" {if $user.price_file_type == 'efind'}selected="selected"{/if}>efind.ru</option>
                    </select>
                </td>
                <td style="padding-left: 24px;">
                    Логин:<br /><input type="text" id="price_login" name="price_login" value="{$user.price_file_login|escape}" style="max-width: 145px;"/>
                </td>
                <td>
                    Пароль:<br /><input type="text" id="price_password" name="price_password" value="{$user.price_file_pass|escape}" style="max-width: 145px;"/>
                </td>
            </tr>
        </table>

        <div>
            <button onclick="TestXml()" type="button" style="margin-left: 4px;">Тестировать</button>
            <input type="checkbox" name="price_file_use" id="price_file_use" {if $user.price_file_use}checked{/if} style="position: relative; top: 3px;" /> активировать файл
            <button onclick="SubmitForm()" type="button" style="float: right; margin-right: 4px;"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить настройки</button>
        </div>

        {if $post}<div  style="text-align: center; color: green">Данные были успешно сохранены.</div>{/if}

	</form>	
</div>

<div class="spacer"></div>


<div id="xml-log" style="display: none"></div>
<div id="xml-log-loading" style="display: none; text-align: center; color: gray">
    <img src="i/ajax_loader2.gif" width="16" height="16" alt="Идет проверка" valign="middle" /> &nbsp; Идет проверка файла ...
</div>


