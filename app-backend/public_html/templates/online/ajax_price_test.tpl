{if empty($error)}
    <p>
        Всего позиций: {$rows.total|escape}.
    </p>

    <p>
    {foreach from=$tags_info key=tag item=info}
        <div {if $info.required && $info.skipped > 0}style="color: red;"{elseif $info.skipped == 0}style="color: green;"{/if}>
            {if $file_type == 'xml'}&lt;{$tag|escape}&gt;{else}{$tag|escape}{/if} - корректно {$info.correct|escape}, отсутствует {$info.skipped|escape};
        </div>
    {/foreach}
    </p>
{else}
    <p style="color: red; font-weight: bold;">{$error|escape}</p>
{/if}

