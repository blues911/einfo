{literal}
<script>
function SubmitForm()
{
	if ($F('title') == "")
	{
		alert('Не заполнено поле "Название модуля"');
		return false;	
	}
	
	if ($F('name') == "")
	{
		alert('Не заполнено поле "Идентификатор модуля"');
		return false;	
	}
	
	if ($F('default_action') == "")
	{
		alert('Не заполнено поле "Действие (action) по умолчанию"');
		return false;	
	}					
	
	$('add_modules').submit();
}

</script>
{/literal}


<h1>Добавление модуля</h1>

<form name="add_modules" id="add_modules" method="post" enctype="multipart/form-data" action="?module=modules&action=add">

	<div class="form-item">
		* Название модуля:<br />
		<input name="title" id="title" type="text" style="width:300px" />
	</div>	

	<div class="form-item">
		* Идентификатор модуля:<br />
		<input name="name" id="name" type="text" style="width:250px" />
	</div>
	
	<div class="form-item">
		* Действие (action) по умолчанию:<br />
		<input name="default_action" id="default_action" type="text" style="width:250px" />
	</div>
	
	<div class="form-item">
		Позиция в меню:<br />
		<input name="position" id="position" type="text" style="width:100px" />
	</div>

    <div class="form-item">
        Допустимые IP адреса:<br />
        <span style="color: #808080">(разделитель &mdash; пробел, новая строка или любой<br />символ кроме точки)</span><br />
        <textarea name="ip_filter" style="resize: vertical; min-height: 100px; width: 300px;" placeholder="Разрешены все"></textarea>
    </div>
	
	<div class="form-item">
		<input name="admin" id="admin" type="checkbox" value="1" /> административный модуль
	</div>			
	
	<div class="form-item">
		<input name="showmenu" type="checkbox" value="1" checked /> показывать в основном меню
	</div>		
	
	<br />
	<br />
		
	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить модуль</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>  
</form>