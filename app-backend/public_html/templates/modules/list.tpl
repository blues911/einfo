{literal}
<script language="javascript">

function DeleteModule(id, title)
{
	if(confirm('Вы действительно хотите удалить модуль "' + jQuery('<p />').html(title).text() + '"?'))
	{
		document.location="?module=modules&action=delete&id=" + id;
	}
}


</script>
{/literal}

<h1>Модули</h1>

<table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
  <tr align="center">
    <th width="5%">№</th>
    <th>Модули</th>
    <th width="90">IP</th>
	<th width="30">Ред.</th>
    <th width="30">Удал.</th> 	
  </tr>
  {foreach from=$list item=item}
  <tr valign="top">
    <td align="center" style="padding-right: 5px;">{$item.num|escape}.</td>
    <td><a href="?module=modules&action=edit-form&id={$item.id|escape}">{$item.title|escape}</a></td>
    <td align="left">{$item.ip_filter|escape|nl2br}</td>
    <td align="center"><a href="?module=modules&action=edit-form&id={$item.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center"><a href="#" onClick="DeleteModule({$item.id|escape}, '{$item.title|escape}')"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a></td>
  </tr>
  {/foreach}
</table>

{include file="pager.tpl"}
