<link href="templates/vacancy/style.css" rel="stylesheet" type="text/css" />

{literal}
<script>
function SubmitForm()
{
	filled = ['speciality', 'city'];
	if (CheckFieldsForm(filled))
	{
		$('add_vacancy').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}
}

</script>
{/literal}


<h1>Добавление вакансии</h1>

<form name="add_vacancy" id="add_vacancy" method="post" enctype="multipart/form-data" action="?module=vacancy&action=add">

	<table width="100%" border="0" cellspacing="0" cellpadding="7" id="vacancy">
	  <tr>
		<td width="150">* Название вакансии (специальность):</td>
		<td><input name="speciality" id="speciality" type="text" style="width:350px" /></td>
	  </tr>
	  <tr>
		<td>Опыт работы:</td>
		<td>
			<select name="experience" id="experience">
				<option value="1">от 1 года</option>
				<option value="2">от 2 лет</option>
				<option value="3">от 3 лет</option>
				<option value="4">от 4 лет</option>
				<option value="5">от 5 лет</option>
			</select>		
		</td>
	  </tr>
	  <tr>
		<td>Возраст:</td>
		<td>от <input name="age_start" id="age_start" type="text" style="width:40px" /> до <input name="age_end" id="age_end" type="text" style="width:40px" /> лет</td>
	  </tr>	
	  <tr>
		<td>* Город:</td>
		<td><input name="city" id="city" type="text" style="width:200px" /></td>
	  </tr>
	  <tr>
		<td>Место работы:</td>
		<td>
		<select name="workplace" id="workplace">
			<option value="0">не важно</option>
			<option value="1">работа в офисе</option>
			<option value="2">удаленная работа</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Тип работы:</td>
		<td>
		<select name="worktype" id="worktype">
			<option value="0">не важно</option>
			<option value="1">постоянная</option>
			<option value="2">разовая</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>График работы:</td>
		<td>
		<select name="workgraphic" id="workgraphic">
			<option value="0">не важно</option>
			<option value="1">полная занятость</option>
			<option value="2">неполная занятость</option>
			<option value="3">свободный</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Иностранные языки:</td>
		<td><input name="language" id="language" type="text" style="width:200px" /></td>
	  </tr>	  
	  <tr>
		<td>Зарплата:</td>
		<td>от <input name="pay" id="pay" type="text" style="width:50px" /> у.е.</td>
	  </tr>
	  <tr>
		<td>Контактное лицо:</td>
		<td><input name="contact_person" id="contact_person" type="text" style="width:350px" /></td>
	  </tr>	
	  <tr>
		<td>Телефон:</td>
		<td><input name="contact_phone" id="contact_phone" type="text" style="width:200px" /></td>
	  </tr>	 
	  <tr>
		<td>Факс:</td>
		<td><input name="contact_fax" id="contact_fax" type="text" style="width:200px" /></td>
	  </tr>	
	  <tr>
		<td>Электронная почта:</td>
		<td><input name="contact_email" id="contact_email" type="text" style="width:200px" /></td>
	  </tr>	
	  <tr>
		<td>Дополнительная информация:</td>
		<td><textarea name="info" id="info" rows="6" style="width: 350px" ></textarea></td>
	  </tr> 	  	  	  	  	  	  	    
	</table>

	
	<br />
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button" ><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить вакансию</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>	
  
</form>
 