<link href="templates/vacancy/style.css" rel="stylesheet" type="text/css" />

{literal}
<script>
function SubmitForm()
{
	filled = ['speciality', 'city'];
	if (CheckFieldsForm(filled))
	{
		$('edit_vacancy').submit();
	}
	else
	{
		alert("Не все обязательные поля заполнены");	
	}
}

</script>
{/literal}


<h1>Редактирование вакансии</h1>

<form name="edit_vacancy" id="edit_vacancy" method="post" enctype="multipart/form-data" action="?module=vacancy&action=edit&id={$content.id|escape}">

	<table width="100%" border="0" cellspacing="0" cellpadding="7" id="vacancy">
	  <tr>
		<td width="150">* Название вакансии (специальность):</td>
		<td><input name="speciality" id="speciality" type="text" style="width:350px" value="{$content.speciality|escape}" /></td>
	  </tr>
	  <tr>
		<td>Опыт работы:</td>
		<td>
			<select name="experience" id="experience">
				<option value="1" {if $content.experience == 1}selected{/if}>от 1 года</option>
				<option value="2" {if $content.experience == 2}selected{/if}>от 2 лет</option>
				<option value="3" {if $content.experience == 3}selected{/if}>от 3 лет</option>
				<option value="4" {if $content.experience == 4}selected{/if}>от 4 лет</option>
				<option value="5" {if $content.experience == 5}selected{/if}>от 5 лет</option>
			</select>		
		</td>
	  </tr>
	  <tr>
		<td>Возраст:</td>
		<td>от <input name="age_start" id="age_start" type="text" style="width:40px" value="{$content.age_start|escape}" /> до <input name="age_end" id="age_end" type="text" style="width:40px" value="{$content.age_end|escape}" /> лет</td>
	  </tr>	
	  <tr>
		<td>* Город:</td>
		<td><input name="city" id="city" type="text" style="width:200px" value="{$content.city|escape}" /></td>
	  </tr>
	  <tr>
		<td>Место работы:</td>
		<td>
		<select name="workplace" id="workplace">
			<option value="0" {if $content.workplace == 0}selected{/if}>не важно</option>
			<option value="1" {if $content.workplace == 1}selected{/if}>работа в офисе</option>
			<option value="2" {if $content.workplace == 2}selected{/if}>удаленная работа</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Тип работы:</td>
		<td>
		<select name="worktype" id="worktype">
			<option value="0" {if $content.worktype == 0}selected{/if}>не важно</option>
			<option value="1" {if $content.worktype == 1}selected{/if}>постоянная</option>
			<option value="2" {if $content.worktype == 2}selected{/if}>разовая</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>График работы:</td>
		<td>
		<select name="workgraphic" id="workgraphic">
			<option value="0" {if $content.workgraphic == 0}selected{/if}>не важно</option>
			<option value="1" {if $content.workgraphic == 1}selected{/if}>полная занятость</option>
			<option value="2" {if $content.workgraphic == 2}selected{/if}>неполная занятость</option>
			<option value="3" {if $content.workgraphic == 3}selected{/if}>свободный</option>
		</select>		
		</td>
	  </tr>
	  <tr>
		<td>Иностранные языки:</td>
		<td><input name="language" id="language" type="text" style="width:200px" value="{$content.language|escape}" /></td>
	  </tr>	  
	  <tr>
		<td>Зарплата:</td>
		<td>от <input name="pay" id="pay" type="text" style="width:50px" value="{$content.pay|escape}" /> у.е.</td>
	  </tr>
	  <tr>
		<td>Контактное лицо:</td>
		<td><input name="contact_person" id="contact_person" type="text" style="width:350px" value="{$content.contact_person|escape}" /></td>
	  </tr>	
	  <tr>
		<td>Телефон:</td>
		<td><input name="contact_phone" id="contact_phone" type="text" style="width:200px" value="{$content.contact_phone|escape}" /></td>
	  </tr>	 
	  <tr>
		<td>Факс:</td>
		<td><input name="contact_fax" id="contact_fax" type="text" style="width:200px" value="{$content.contact_fax|escape}" /></td>
	  </tr>	
	  <tr>
		<td>Электронная почта:</td>
		<td><input name="contact_email" id="contact_email" type="text" style="width:200px" value="{$content.contact_email|escape}" /></td>
	  </tr>	
	  <tr>
		<td>Дополнительная информация:</td>
		<td><textarea name="info" id="info" rows="6" style="width: 350px">{$content.info|escape}</textarea></td>
	  </tr> 	  	  	  	  	  	  	    
	</table>

	
	<br />
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button" ><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button>
	</div>
  
</form>
 