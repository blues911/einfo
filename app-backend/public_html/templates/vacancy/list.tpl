{literal}
<script>
function DeleteVacancy(id, title)
{
	if(confirm('Вы действительно хотите удалить вакансию "' + jQuery('<p />').html(title).text() + '"?'))
	{
		document.location = '?module=vacancy&action=delete&id=' + id;
	}	
}
</script>
{/literal}


<h1>Вакансии</h1>


<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">
    <th width="5%">№</th>
    <th width="55%">Название вакансии (специальность)</th>
    <th >Дата
	{if $list_order == "desc"}
	<a href="?module=vacancy&action=list&order=asc"><img src="i/icons/sort_desc.gif" alt="Сортировка по убыванию" width="9" height="5" hspace="5" border="0" />
	{else}
	<a href="?module=vacancy&action=list&order=desc"><img src="i/icons/sort_asc.gif" alt="Сортировать по возрастанию" width="9" height="5" hspace="5" border="0" />
	{/if}
	</a></th>
    <th width="30">Ред.</th> 	
    <th width="30">Удал.</th>
  </tr>
  {foreach from=$list item=item}
  <tr valign="top" >
    <td align="center">{$item.num|escape}.</td>
    <td><a href="?module=vacancy&action=edit-form&id={$item.id|escape}">{$item.speciality|escape}</a></td>
    <td align="center">{$item.date|escape}</td>
	<td align="center"><a href="?module=vacancy&action=edit-form&id={$item.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center"><a href="#" onclick="DeleteVacancy({$item.id|escape}, '{$item.speciality|escape}')"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a></td>
  </tr>
  {/foreach}

</table>

{include file="pager.tpl"}