<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>

{literal}
<script>

EnableEditor('descr');

function SubmitForm()
{
	postform = true;
	
	if ($F('title') == '')
	{
		postform = false;
		alert('Поле "Название категории" должно быть заполнено');
	}	
	else if (!(/^[-a-z0-9_]+$/i).test($F('path')))
	{
		postform = false;
		alert('Поле "Идентификатор (путь)" содержит недопустимые символы');
	}			
	
	if (postform) $('add_category').submit();
}

function LoadCategory()
{
	new Ajax.Updater('i_category_after', 'ajaxer.php?x=categories.ajax_child_category', {
		parameters: { parent_category: $F('parent_category') }			
	});
}
</script>
{/literal} 

<h1>Добавление категории</h1>

<form name="add_category" id="add_category" method="post" enctype="multipart/form-data" action="?module=categories&action=add">				
	
	<div class="form-item">
	  Родительская категория:<br />
	  <select name="parent_category" id="parent_category" onChange="LoadCategory()">
		<option value="1" selected >&#8250; КОРЕНЬ</option>						 
		{foreach from=$categories item=category}
		<option value="{$category.id|escape}" >{$category.title|escape|indent:$category.c_level*7:"&nbsp;"}</option>
		{/foreach}						
	  </select>
	  <p style="margin-left: 50px">
		&rarr; разместить после:<br />
		<span id="i_category_after"></span>
	  </p>					  
				  
	</div>
	
	<div class="form-item">
		* Название категории:<br />
		<input name="title" id="title" type="text" style="width:400px" />						
	</div>
	
	<div class="form-item">
		* Идентификатор (путь) категории:<br />
		<input name="path" id="path" type="text" style="width:400px" />						
	</div>

    <div class="form-item">
        Описание категории:<br />
        <textarea name="descr" id="descr" rows="20" style="width: 550px;"></textarea>
    </div>

    <div class="form-item">
        Расположение описания:
        <select name="descr_position">
            <option value="bottom">Низ</option>
            <option value="top">Верх</option>
        </select>
    </div>
	
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить категорию</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>	
  
</form>
<script>LoadCategory();</script>
				
 