<script language="javascript" type="text/javascript" src="modules/_tiny_mce/tiny_mce.js"></script>

{literal}
<script>

EnableEditor('descr');

function SubmitForm()
{
	postform = true;
	
	if ($F('title') == '')
	{
		postform = false;
		alert('Поле "Название категории" должно быть заполнено"');
	}
	else if (!(/^[-a-z0-9_]+$/i).test($F('path')))
	{
		postform = false;
		alert('Поле "Идентификатор (путь)" содержит недопустимые символы');
	}		
	
	if (postform) $('edit_category').submit();
}

function LoadCategory()
{
    new Ajax.Updater('i_category_after', 'ajaxer.php?x=categories.ajax_child_category', {
        parameters: { parent_category: $F('parent_category') }
    });
}

jQuery(document).ready(function()
{
    LoadCategory();
});

</script>
{/literal} 

<h1>Редактирование категории</h1>

<form name="edit_category" id="edit_category" method="post" enctype="multipart/form-data" action="?module=categories&action=edit&id={$content.id|escape}">
	
	<div class="form-item">
	  КОРЕНЬ <span style="font-size: 14px">&rarr;</span>
	  {foreach from=$parent_cats item=cats}
	  <a href="?module=categories&action=edit-form&id={$cats.id|escape}">{$cats.title|escape}</a> <span style="font-size: 14px">&rarr;</span>
	  {/foreach}
	  <span style="color: gray">{$content.title|escape}</span>
	</div>
	
	<div class="form-item">
	  Переместить в:<br />
	  <select name="new_parent_category" id="parent_category" onChange="LoadCategory()">
		<option value="{$parent_id|escape}" selected style="color: gray">[ --- не перемещать --- ]</option>
		<option value="1" >&#8250; КОРЕНЬ </option> 
		 
		{foreach from=$categories item=category}
		<option value="{$category.id|escape}" >{$category.title|escape|indent:$category.c_level*7:"&nbsp;"}</option>
		{/foreach}	
		
	  </select>

      <p style="margin-left: 50px">
          &rarr; разместить после:<br />
          <span id="i_category_after"></span>
      </p>

	</div>
	
	<div class="form-item">
		* Название категории:<br />
		<input name="title" id="title" type="text" style="width:400px" value="{$content.title|escape}" />
	</div>	
	
	<div class="form-item">
		* Идентификатор (путь) категории:<br />
		<input name="path" id="path" type="text" style="width:400px" value="{$content.path|escape}" />
	</div>

    <div class="form-item">
        Описание категории:<br />
        <textarea name="descr" id="descr" rows="20" style="width: 550px;">{$content.descr|escape}</textarea>
    </div>

    <div class="form-item">
        Расположение описания:
        <select name="descr_position">
            <option value="bottom" {if $content.descr_position == 'bottom'}selected{/if}>Низ</option>
            <option value="top" {if $content.descr_position == 'top'}selected{/if}>Верх</option>
        </select>
    </div>
	
	<br />
	
	<div class="form-item">
		<button onclick="SubmitForm()" type="button"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Изменить данные</button>
	</div>	
  
</form>			
 