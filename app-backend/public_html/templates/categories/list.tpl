{literal}
<script language="javascript">
function DeleteCategory(id, title)
{
	if(confirm('Вы действительно хотите удалить категорию "' + jQuery('<p />').html(title).text() + '"?'))
	{
		document.location="?module=categories&action=delete&id=" + id;
	}
}
	
</script>
{/literal}

<h1>Рубриктор</h1>

<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">
  <tr align="center">
    <th >Категория</th>
    <th width="60">Позиция</th>	
    <th width="30">Ред.</th> 	
    <th width="30">Удал.</th>
  </tr>
  {foreach from=$list item=category name=cat}
  <tr valign="top">
    <td >
	{if $category.c_level == 1}
		{""|indent:$category.c_level*7:"&nbsp;"}<strong>&#8250; <a href="?module=categories&action=edit-form&id={$category.id|escape}">{$category.title|escape}</a></strong>
	{else}
		{""|indent:$category.c_level*7:"&nbsp;"}&#8250; <a href="?module=categories&action=edit-form&id={$category.id|escape}">{$category.title|escape}</a>
	{/if}
	</td>
	<td align="center">
		<a href="?module=categories&action=shift&type=up&id={$category.id|escape}"><img src="i/icons/icon_listup.gif" border="0" alt="Вверх" /></a>
		<a href="?module=categories&action=shift&type=down&id={$category.id|escape}"><img src="i/icons/icon_listdw.gif" border="0" alt="Вниз" /></a>
	</td> 
	<td align="center"><a href="?module=categories&action=edit-form&id={$category.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center"><a href="#" onclick="DeleteCategory({$category.id|escape}, '{$category.title|escape}')"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a></td>
  </tr>
  {/foreach}

</table>

