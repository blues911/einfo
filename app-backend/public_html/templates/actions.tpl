{if $actions}		
		<div style="clear: both; margin-bottom: 20px;">
			<div id="actions">
			{foreach from=$actions item=item}
				<li class="action-item"><img src="i/icons/{$item.icon|escape}" border="0" style="margin-right: 4px" align="absmiddle" /><a href="{$item.url|escape}">{$item.title|escape}</a></li>
			{/foreach}
			</div>
			<img src="i/actions_end.gif" width="5" height="40" id="actions-end" />
		</div>
{/if}
		