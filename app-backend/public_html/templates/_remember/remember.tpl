<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>{$main.page_title|escape}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex, nofollow">
	<link rel="stylesheet" type="text/css" href="/i/css/remember.css" />

	<script type="text/javascript" src="/i/js/prototype.js" ></script> 
	<script type="text/javascript" src="/i/js/common.js"></script>
	<script type="text/javascript" src="/i/js/remember.js"></script>
</head>

<body>

	<div id="container">{$main.content nofilter}</div>

</body>
</html>