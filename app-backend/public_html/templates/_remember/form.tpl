<h1>Восстановление пароля</h1>
<div class="spacer"></div>

<p><strong>ВНИМАНИЕ! Вы можете восстановить пароль только если ваш аккаунт был активным.</strong></p>

<form action="#" method="post" enctype="application/x-www-form-urlencoded" name="remember" id="remember">

	<table border="0" cellspacing="0" cellpadding="7">
	 <tr>
		<td width="170" >Введите ваш e-mail в системе</td>
		<td>
            <input type="text" id="email" name="email" style="width:90%" />
		</td>
	 </tr>
	  
	 <tr>
		<td><img src="/protect/kcaptcha/"></td>
		<td>
			* Защита от спама (введите код):<br />
			<input name="captcha" id="captcha" type="text" style="width: 170px" />
			<div style="display: none;" id="captcha_message">Введенный код неверен, попробуйте еще раз.</div>
		</td>
	 </tr> 
	 <tr>
		<td>&nbsp;</td>
		<td align="right"><button type="button" onclick="Remember()" id="btn">Напомнить пароль</button></td>
	  </tr>	  
	</table>
	

    <div id="message_OK" class="message" style="display: none">
        <strong>На вашу электронную почту выслано письмо с ссылкой для активации<br />нового пароля.</strong>
    </div>

    <div id="message_NONE" class="message" style="display: none">
        Указанный e-mail не найден в базе данных или он не является активным.
    </div>

    <div id="message_NOT_VALID" class="message" style="display: none">
        Указанный e-mail является не корректным, пожалуйста введите правильный адрес.
    </div>

    <div id="message_CAPTCHA" class="message" style="display: none">
        Код на картинке не совпадает с введенным вами, пожалуйста<br />введите правильный код.
    </div>
	
</form>

