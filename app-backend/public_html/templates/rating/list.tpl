{literal}
<style>
.rate_btn label { font-size: 13px; color: #3967C2; padding-right: 7px; font-weight: bold; }
</style>
{/literal}


<h1>Рейтинговое голосование</h1>

<form method="post" action="?module=rating&action=vote">
    <table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">
        <tr>
        <th width="5%">№</th>
        <th>
            Участник
            {if $list_order == "desc"}
            <a href="?module=rating&action=list&order=asc"><img src="i/icons/sort_desc.gif" alt="Сортировка по убыванию" width="9" height="5" hspace="5" border="0" />
            {else}
            <a href="?module=rating&action=list&order=desc"><img src="i/icons/sort_asc.gif" alt="Сортировать по возрастанию" width="9" height="5" hspace="5" border="0" />
            {/if}
            </a>
        </th>
        <th width="150">Рейтинг</th>

      </tr>
      {foreach from=$list item=item}
      <tr valign="top" >
        <td align="center">{$item.num|escape}.</td>
        <td>
            <strong>{$item.title|escape}</strong>
            <div class="prompt">{$item.about|escape}</div>
        </td>
        <td align="center" class="rate_btn">
            <input type="radio" name="user_vote[{$item.id|escape}]" id="user_{$item.id|escape}_vote1" value="-1" {if $item.rating == -1}checked{/if} /><label for="user_{$item.id|escape}_vote1"><img src="i/icons/rating_minus.gif"  width="16" height="16" title="Отрицательный отзыв" alt="Отрицательный отзыв"/></label>
            <input type="radio" name="user_vote[{$item.id|escape}]" id="user_{$item.id|escape}_vote2" value="0" {if $item.rating == 0 OR !$item.rating}checked{/if} /><label for="user_{$item.id|escape}_vote2">0</label>
            <input type="radio" name="user_vote[{$item.id|escape}]" id="user_{$item.id|escape}_vote3" value="1" {if $item.rating == 1}checked{/if} /><label for="user_{$item.id|escape}_vote3"><img src="i/icons/rating_plus.gif"  width="16" height="16" title="Положительный отзыв" alt="Положительный отзыв"/></label>
        </td>
      </tr>
      {/foreach}

    </table>
    <div style="margin-top: 15px"><button type="submit"><img src="i/icons/btn_save.gif" width="15" height="15" hspace="5" align="absmiddle" />Сохранить изменения</button></div>
</form>

{include file="pager.tpl"}
