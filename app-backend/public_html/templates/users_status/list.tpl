{* Smarty *}

<h1>Активность пользователей</h1>

<div id="users_status_filter" align="right">
    <form name="search" id="search" method="post" action="#">

        <p>
            Период с
            <input type="text" class="datepicker" name="date_from" value="{$date.from|escape}" />
            по
            <input type="text" class="datepicker" name="date_to" value="{$date.to|escape}" />
        </p>

        <div>
            <label style="display: inline; position: relative; top: -2px; margin-right: 12px;">
                <input type="checkbox" name="status_check" value="1" {if $status_check}checked{/if} style="top: -2px;" /> только активные в этот период
            </label>
            <select name="access_level">
                <option value="">Уровень доступа</option>
                {foreach from=$access_levels item='_access_level'}
                    <option value="{$_access_level.id|escape}" {if $_access_level.id == $access_level}selected{/if}>{$_access_level.title|escape}</option>
                {/foreach}
            </select>
        </div>

        <p>
            Пользователь
            <input type="text" name="user_title" value="{$user_title|escape}" />
            <input type="submit" value="Применить" />
        </p>

    </form>

    <div class="legend">
        <div class="square grey"></div> Без статуса<br />
        <div class="square yellow"></div> Ожидает модерации<br />
        <div class="square green"></div> Активный<br />
        <div class="square red"></div> Заблокирован
    </div>

</div>

<table width="100%"  border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">

    <tr>
        <th width="10">№</th>
        <th width="200">Пользователь</th>
        <th width="10">Статус</th>
        <th>Таймлайн</th>
    </tr>

    {foreach from=$users item=user}
        <tr>
            <td align="center">{$user.num|escape}.</td>
            <td align="left"><span title="id = {$user.id|escape}">{$user.title|escape}</span></td>
            <td align="center">
                {if $user.status == -1}
                    <img src="i/icons/icon_moder.gif" border="0" alt="Ожидает модерации" title="Ожидает модерации" />
                {elseif $user.status == 0}
                    <img src="i/icons/icon_ok_inactive.gif" border="0" alt="Заблокирован" title="Заблокирован" />
                {elseif $user.status == 1}
                    <img src="i/icons/icon_ok_active.gif" border="0" alt="Активный" title="Активный" />
                {elseif $user.status == -2}
                    <span style="color: gray">&mdash;</span>
                {/if}
            </td>
            <td align="center">
                <div style="white-space: nowrap; padding: 0; width: 100%; height: 20px; background-color: lightyellow; border: solid 1px #000000;display: flex;">
                    {foreach from=$user.tm item=tm}
                        <span style="float: left; margin: 0; padding: 0; height: 100%;min-width: 1px;
                              width: {$tm.width|escape}%; background-color: {if $tm.status == 1}green{elseif $tm.status == -2}grey{elseif $tm.status == -1}yellow{else}red{/if};"
                              title="{$tm.title|escape}">
                        </span>
                    {/foreach}
                </div>
            </td>
        </tr>
    {/foreach}

</table>