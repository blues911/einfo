<link href="templates/levels/style.css" rel="stylesheet" type="text/css" />
<script src="templates/levels/script.js" type="text/javascript"></script>

{literal}
<script>
function SubmitForm()
{
	if ($F('title') == "")
	{
		alert('Не заполнено поле "Название уровня доступа"');
		return false;	
	}			
	
	$('add_levels').submit();
}

</script>
{/literal}


<h1>Добавление уровня доступа</h1>

<form name="add_levels" id="add_levels" method="post" enctype="multipart/form-data" action="?module=levels&action=add">	

	<div class="form-item">
		* Название уровня доступа:<br />
		<input name="title" id="title" type="text" style="width:300px" />
	</div>
	
	<div class="form-item">
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
		  <tr>
			<td valign="top" width="220">
				<fieldset>
					<legend> Связанные модули </legend>
					{foreach from=$modules item=item}
						<div style="margin: 5px 0;{if $item.admin}color: red{/if}"><input name="access_modules[]" id="access_modules[]" type="checkbox" value="{$item.id|escape}" />{$item.title|escape}</div>
					{/foreach}
				</fieldset>			
			</td>
			<td valign="top" align="left">
				<fieldset id="users-fields">
					<legend> Пользовательские поля </legend>
					<table width="100%" border="0" cellspacing="0" cellpadding="5">
					  <tr>
					  	<th width="4%"></th>
						<th width="76%">Поле</th>
						<th width="10%">Ред.</th>
						<th width="10%">Обязат.</th>
					  </tr>	
					  
					  {foreach from=$fields item=item}				
					  <tr>
					  	<td><input name="fields[{$item.id|escape}]" id="fields[{$item.id|escape}]" type="checkbox" value="{$item.id|escape}" onclick="ToggleField(this)" /></td>
						<td>{$item.title|escape} <span class="prompt">[&nbsp;{$item.field|escape}&nbsp;]</span></td>
						<td align="center"><input name="editable[{$item.id|escape}]" id="editable[{$item.id|escape}]" type="checkbox" value="{$item.id|escape}" disabled /></td>
						<td align="center"><input name="fill[{$item.id|escape}]" id="fill[{$item.id|escape}]" type="checkbox" value="{$item.id|escape}" disabled /></td>
					  </tr>
					  {/foreach}

					</table>								
				</fieldset>			
			</td>
		  </tr>
		</table>
	</div>	

	
	<br />
	<br />
		
	<div class="form-item">
		<button onClick="SubmitForm()" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить уровень доступа</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>		
	</div>  
</form>