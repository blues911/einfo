function ToggleField(object)
{
	if (object.checked)
	{
		$('editable[' + object.value + ']').enable();
		$('fill[' + object.value + ']').enable();		
	}
	else
	{
		$('editable[' + object.value + ']').disable();
		$('fill[' + object.value + ']').disable();		
	}
}