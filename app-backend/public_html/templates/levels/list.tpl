{literal}
<script language="javascript">

function DeleteLevel(id, title)
{
	if(confirm('Вы действительно хотите удалить уровень доступа "' + jQuery('<p />').html(title).text() + '"?'))
	{
		document.location="?module=levels&action=delete&id=" + id;
	}
}


</script>
{/literal}

<h1>Уровни доступа</h1>

<table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
  <tr align="center">
    <th width="5%">№</th>
    <th >Уровни доступа</th>
	<th width="30">Ред.</th>
    <th width="30">Удал.</th> 	
  </tr>
  {foreach from=$list item=item}
  <tr valign="top">
    <td align="center" style="padding-right: 5px;">{$item.num|escape}.</td>
    <td><a href="?module=levels&action=edit-form&id={$item.id|escape}">{$item.title|escape}</a></td>
	<td align="center"><a href="?module=levels&action=edit-form&id={$item.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center"><a href="#" onClick="DeleteLevel({$item.id|escape}, '{$item.title|escape}')"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a></td>
  </tr>
  {/foreach}
</table>

{include file="pager.tpl"}
