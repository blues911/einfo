{if $pager.show_pages}
<div align="right" class="pages">
	Страницы:
	{if $pager.prev}
		<li><a href="{$pager.prev|escape}" rel="{$pager.prev_num|escape}">&larr;</a></li>
	{/if}
	
	{foreach from=$pager.pages item=item}
		{if $item.num == $pager.current}
		<li class="page-selected">{$item.num|escape}</li>
		{else}		
		<li><a href="{$item.link|escape}" rel="{$item.num|escape}">{$item.num|escape}</a></li>
		{/if}		
	{/foreach}	
	
	{if $pager.next}
		<li><a href="{$pager.next|escape}" rel="{$pager.next_num|escape}">&rarr;</a></li>
	{/if}				
</div>
{/if}