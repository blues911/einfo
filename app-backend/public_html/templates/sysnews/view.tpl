{literal}
<style>
#sysnews { margin-top: 20px; }
.sysnews { margin: 25px 0;  }
.sysnews .date { color: gray; }
.sysnews h2 { margin-bottom: 7px; }
.sysnews .text { margin-left: 30px; }
.sysnews .text p { margin: 8px 0; }	
</style>
{/literal}

<div id="sysnews">
	<h1>Новости системы</h1>
	
	{foreach from=$sysnews item=item}
	<div class="sysnews">
		<div class="date">{$item.date|escape}</div>
		<h2>{$item.title|escape}</h2>
		<div class="text">{$item.text}</div>
	</dl>
	<div class="spacer"></div>
	{/foreach}
</div>

{include file="pager.tpl"}