{literal}
<script type="text/javascript">
    jQuery(document).ready(function()
    {

        jQuery('#dates').submit(function()
        {
            jQuery('#top').hide();
            jQuery('#loading').show();
            return true;
        });

    });
</script>
{/literal}

<h1>Статистика компонентов из заявок</h1>

<div class="info">
    Сведения о самых популярных компонентах в заявках...
</div>

<div align="right" style="margin: 10px 0px 60px 0px">
    <form id="dates" method="get">
        <p>
            <input type="hidden" name="module" value="stats_order" />
            <input type="hidden" name="action" value="search" />
            <div style="float: left;">
                Поисковый запрос: <input type="text" name="query" value="{$query|escape}" />
            </div>
            <div style="float: right;">
                Период с
                <input type="text" class="datepicker" name="date_from" value="{$date_from|escape}" />
                по
                <input type="text" class="datepicker" name="date_to" value="{$date_to|escape}" />
                <input type="submit" value="Применить" />
            </div>
        </p>
    </form>
</div>

<table id="top" width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#CCCCCC" rules="rows" class="table-items">

    <tr>
        <th width="5%">№</th>
        <th>Компонент</th>
        <th width="150">Кол-во заявок</th>
        <th width="200">Дата последней заявки</th>
    </tr>

    {foreach from=$top item=row}
        <tr valign="top" >
            <td align="center">{$row.num|escape}.</td>
            <td>
                <strong>{$row.title|escape}</strong>
            </td>
            <td align="center">{$row.cnt|escape}</td>
            <td align="center">{$row.last_date|escape}</td>
        </tr>
    {/foreach}

</table>

<div id="loading" hidden="hidden" style="text-align: center;">
    <h2>Загрузка данных может занять некоторое время...</h2>
    <img src="/i/ajax_loader.gif">
</div>






