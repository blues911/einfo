{literal}
<style>
    .subscribe {
        padding-bottom: 20px;
    }

    .subscribe img {
        margin-bottom: -12px;
    }

    .subscribe .green {
        color: #2da43b;
    }

    .subscribe .red {
        color: #c23939;
    }
</style>
{/literal}

<h1>Чат</h1>

<div class="js_subscribe_content subscribe">
    <p>Загрузка...</p>
</div>

<div class="js_chat_table" style="display: none">
    <table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
        <tr align="center">
            <th width="5%">№</th>
            <th>Клиент</th>
            <th>Дата</th>
        </tr>
        {foreach from=$list item=item}
        <tr valign="top">
            <td align="left">{$item.num|escape}.</td>
            <td align="left"><a href="?module=users_chat&action=view&user_id={$item.user_id|escape}&guest_hash={$item.guest_hash|escape}">{$item.title|escape}</a></td>
            <td align="center">{$item.date|escape}</td>
        </tr>
        {/foreach}
    </table>

    {include file="pager.tpl"}
</div>