<h1>Клиент: {$title|escape} - архив переписки</h1>

<ul class="chat_history">
    {foreach from=$list item=item}
        {if $item.sender_type == 'user'}
            <li class="ms_left">
                <span clas="date">{$item.date|escape}</span>
                <p>{$item.text|escape|nl2br}</p>
            </li>
        {else}
            <li class="ms_right">
                <span clas="date">{$item.date|escape}</span>
                <p>{$item.text|escape|nl2br}</p>
            </li>
        {/if}    
    {/foreach}
</ul>