<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>EINFO: панель управления</title>
    <meta name="chat:type" content="backend" />
    <meta name="chat:user_id" content="{$main.userID|escape}" />
    <meta name="chat:host" content="{$chat_host|escape}" />
    <meta name="chat:host_ws" content="{$chat_host_ws|escape}" />
    <meta name="chat:host_frontend" content="{$chat_host_frontend|escape}" />
    <meta name="chat:last_visit_interval" content="{$chat_last_visit_interval|escape}" />
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/i/favicon.ico" type="image/x-icon" rel="icon" />
    <link href="/i/js/jquery_ui.css" rel="stylesheet" type="text/css" />
    <link href="/i/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/i/css/datepicker/metallic.css" type="text/css">
    <script src="/i/js/jquery.min.js"></script>
    <script src="/i/js/jquery_ui.js"></script>
    <script src="/i/js/jquery.maskedinput.min.js"></script>
    <script src="/i/js/favico.min.js"></script>
    <script src="/i/js/zebra_datepicker.js"></script>
    <script src="/i/js/common.js" type="text/javascript"></script>
    <script src="/i/js/prototype.js" type="text/javascript"></script>
</head>
<body>

	<table width="990" border="0" cellspacing="0" cellpadding="0" align="center" id="main">
	  <tr>
		<td colspan="2" id="header">
			<div id="panel-top" class="fwhite">
				<li><a href="?module=cp&action=help&doc={$main.help_url|escape}">Помощь</a></li>
                <li><a href="?module=profile&action=edit-account">Изменить логин/пароль</a></li>
				<li><a href="?module=profile&action=edit-form">Редактировать профиль</a></li>
				<li>[ <a href="?module=cp&action=exit">Выход</a> ]</li>
			</div>
			<div id="logo">
				<a href="/"><img src="i/logo.gif" alt="Панель администратора" width="315" height="64" /></a>
			</div>
			<div id="siteurl">
				{$main.userlogin|escape} [ ID: {$main.userID|escape} ]
			</div>
		</td>
	  </tr>
	  <tr>
		<td width="250" id="menu-box" valign="top">
			<div id="menu-header"><b>Администрируемые разделы</b></div>
			<div id="menu">
			
			{$main.menu nofilter}
												
			</div>
		</td>
		<td width="740" valign="top">
			<div id="mpanel" class="fwhite">{$main.company_title|escape} {if $main.logoff}[ <a href="?module=cp&action=logoff{if $main.users_p}&users_p=1{/if}">Logoff</a> ]{/if}</div>
			
			{$main.actions nofilter}
			
			<div id="content-box" class="content">
				{$main.content nofilter}
			</div>
			
		</td>
	  </tr>
	  <tr>
		<td id="copyrights" class="fwhite">Einfo &copy; <a href="/?module=cp&action=disclaimer">Пользовательское соглашение</a></td>
		<td valign="bottom"><div id="footer-line"></div></td>
	  </tr>
	</table>

    <!-- JS -->
    {if $main.users_chat_access}
        <script src="//{$chat_host|escape}/static/js/init.js?v=6" type="application/javascript"></script>
    {/if}

</body>
</html>
