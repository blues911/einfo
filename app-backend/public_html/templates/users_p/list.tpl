{literal}
<script language="javascript">
function SelectView()
{
	document.location='?module=users_p&action=list&view=' + $F('viewer') + ($F('dist') ? '&dist=1' : '');
}

function DeleteUser(id)
{
    var status = Number(jQuery('tr[data-id=' + id + ']:first').data('user-status'));

    if (status == 1)
    {
        alert('Нельзя удалить активного пользователя.');
    }
    else
    {
        if(confirm('Вы действительно хотите удалить пользователя ' + jQuery('tr[data-id=' + id + ']:first').data('user-title') + '?'))
        {
            document.location="?module=users_p&action=delete&id=" + id;
        }
    }
}

// Переключаем статус пользователя между активный/заблокированный
function changeStatus(id)
{
    if (confirm('Вы уверены, что хотите сменить статус пользователя ' + jQuery('tr[data-id=' + id +']:first').data('user-title') + '?'))
    {
        // Определяем элементы интерфейса
        var tr = jQuery('tr[data-id=' + id + ']');
        var loader = tr.find('td[data-type=status]').find('img[data-type=loader]');
        var active = tr.find('td[data-type=status]').find('img[data-status=1]');
        var blocked = tr.find('td[data-type=status]').find('img[data-status=0]');
        var do_login = tr.find('td[data-type=do_login]').find('a');
        var checkbox = tr.find('input.checkbox_remove:first');

        // Запоминаем текущий статус элементов
        var status = {
            active: active.attr('hidden'),
            blocked: blocked.attr('hidden'),
            do_login: do_login.attr('hidden')
        };

        // Показываем лоадер
        active.hide();
        blocked.hide();
        do_login.hide();
        loader.show();

        // Выполняем аякс-запрос
        jQuery.ajax({
            url: 'ajaxer.php?x=users_p.ajax_change_status',
            type: 'POST',
            dataType: 'json',
            data: { user_id: id },
            success: function(response)
            {
                if (response.success)
                {
                    // Отображаем новый статус
                    loader.hide();

                    tr.data('user-status', response.status);

                    if (response.status == 0)
                    {
                        blocked.show();
                        checkbox.removeAttr('disabled', 'disabled');
                    }
                    else if (response.status == 1)
                    {
                        active.show();
                        do_login.show();
                        checkbox.attr('disabled', 'disabled').removeAttr('checked');
                    }
                }
                else
                {
                    loader.hide();
                    if (status.active != 'hidden') { active.show(); }
                    if (status.blocked != 'hidden') { blocked.show(); }
                    if (status.do_login != 'hidden') { do_login.show(); }
                }

            },
            error: function()
            {
                loader.hide();
                if (status.active != 'hidden') { active.show(); }
                if (status.blocked != 'hidden') { blocked.show(); }
                if (status.do_login != 'hidden') { do_login.show(); }
            }
        });
    }
}


</script>
{/literal}

<h1>Пользователи</h1>

<div align="right" style="margin: -20px 0px 10px 0px">	
	<form name="view_mode" id="view_mode" method="post" action="#" >
        
		<span style="padding-right: 10px"><input name="dist" id="dist" type="checkbox" value="1" {if $dist}checked{/if} /> дистрибьюторы</span>

		<select name="viewer" id="viewer">
			<option value="all" {if $view == "all"}selected{/if}>все подряд</option>
            <option value="valid" {if $view == "valid"}selected{/if}>ожидающие валидации аккаунта</option>
			<option value="mod" {if $view == "mod"}selected{/if}>ожидающие модерации</option>
			<option value="locked" {if $view == "locked"}selected{/if}>заблокированные</option>
			<option value="active" {if $view == "active"}selected{/if}>только активные</option>
			<option value="one_list" {if $view == "one_list"}selected{/if}>все одним списком</option>			
		</select>		
		<input type="button" name="send" value="Показать" class="btn"  onclick="SelectView()">
	</form>
</div>

<table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
  <tr align="center">
    <th width="5%">№</th>
    <th >Пользователи</th>
	<th width="40">ID</th>
	<th width="40">Lim</th>
	<th width="60">Login</th>
	<th width="30">Статус</th>
	<th width="30">Ред.</th>
    <th width="30">Удал.</th> 	
  </tr>
  {foreach from=$list item=item}
  <tr valign="top" data-id="{$item.id|escape}" data-user-status="{$item.status|escape}" data-user-title="{$item.title|escape}" {if $item.level_id == 0}style="background-color: #E9E9E9"{/if}>
    <td align="center" style="padding-right: 5px;">{$item.num|escape}.</td>
    <td><a href="?module=users_p&action=edit-form&id={$item.id|escape}">{$item.title|escape}</a> <span style="color: gray">[{$item.login|escape}]</span></td>
	<td align="center">{$item.id|escape}</td>
	<td align="right">{$item.price_limit|escape}</td>
    <td align="center" data-type="do_login"><a href="?module=users_p&action=login&id={$item.id|escape}" {if $item.status != 1}hidden{/if}><img src="i/icons/icon_key.gif" width="18" height="18" border="0" alt="Залогиниться под пользователем" /></a></td>
	<td align="center" data-type="status" {if $item.status == 0 or $item.status == 1}onclick="changeStatus({$item.id|escape});" style="cursor: pointer;"{/if}>
        {if $item.status == -1}
            <img src="i/icons/icon_moder.gif" border="0" alt="Ожидает модерации" title="Ожидает модерации" />
        {elseif $item.status == 0 or $item.status == 1}
            <img src="i/icons/icon_ok_inactive.gif" border="0" data-status="0" alt="Заблокирован" title="Заблокирован" {if $item.status == 1}hidden{/if} />
            <img src="i/icons/icon_ok_active.gif" border="0" data-status="1" alt="Активный" title="Активный" {if $item.status == 0}hidden{/if} />
            <img src="i/ajax_loader2.gif" data-type="loader" hidden />
        {elseif $item.status == -2}
            <span style="color: gray">&mdash;</span>
        {/if}
	</td>
	<td align="center"><a href="?module=users_p&action=edit-form&id={$item.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center">{if $item.level_id != 0}<a href="#" onclick="DeleteUser({$item.id|escape})"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a>{/if}</td>
  </tr>
  {/foreach}
</table>

{include file="pager.tpl"}
