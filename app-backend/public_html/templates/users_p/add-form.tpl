{literal}
<script type="text/javascript" src="templates/users_p/script.js"></script>
<script>
function SubmitForm()
{
	if ($F('title') == "")
	{
		alert('Не заполнено поле "Название компании / ФИО"');
		return false;
	}

	if ($F('login') == "")
	{
		alert('Не заполнено поле "Логин"');
		return false;
	}

	if ($F('password') == "")
	{
		alert('Не заполнено поле "Пароль"');
		return false;
	}

    if ($F('email') == "")
    {
        alert('Не заполнено поле "Электронная почта (показывается на сайте)"');
        return false;
    }

    if ($F('email_price_notification') == "")
    {
        alert('Не заполнено поле "Электронная почта для уведомлений об обработке прайса"');
        return false;
    }

    if ($F('email_order_notification') == "")
    {
        alert('Не заполнено поле "Электронная почта для уведомлений о заказах"');
        return false;
    }

	if ($F('level') == 0)
	{
		alert('Не заполнено поле "Уровень доступа"');
		return false;
	}

	$('add_users').submit();
}

function UpdateCountry() {
    if ($F('country_id') == 186) {
        $('city_id').enable();
    }
    else {
        $('city_id').disable();
        $('city_id').selectedIndex = 0;
    }
}

</script>
{/literal}


<h1>Добавление пользователя</h1>

<form name="add_users" id="add_users" method="post" enctype="multipart/form-data" action="?module=users_p&action=add">

	<fieldset>
		<legend>Статус в системе</legend>

		<div class="form-item">
            <strong>Статус:</strong> &nbsp;
			<input name="status" type="radio" value="-1" /> ожидает модерации &nbsp;&nbsp;
			<input name="status" type="radio" value="0" /> заблокированный &nbsp;&nbsp;
			<input name="status" type="radio" value="1" checked /> активный &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</div>

		<div class="form-item">
			* Название компании / ФИО:<br />
			<input name="title" id="title" type="text" style="width:322px" />
		</div>

		<div class="form-item">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;" width="30%">* Логин:<br /><input name="login" id="login" type="text" style="width:100%" /></td>
				<td style="padding-right: 15px;" width="30%">* Пароль:<br /><input name="password" id="password" type="text" style="width:100%" /></td>
				<td>* Уровень доступа:<br />
					<select name="level" id="level">
						<option value="0" selected style="color: gray;">[ --- не выбрано --- ]</option>
						{foreach from=$levels item=item}
						<option value="{$item.id|escape}">{$item.title|escape}</option>
						{/foreach}
					</select>
				</td>
			  </tr>
			</table>
		</div>
	</fieldset>

	<fieldset>
		<legend>Общая информация</legend>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;">
                    Страна:<br />
                    <select id="country_id" name="country_id" style="width: 150px" onchange="UpdateCountry()">
                        <option value="0">не определена</option>
                    {foreach from=$countries item=item}
                        <option value="{$item.country_id|escape}" {if $item.country_id == 186}selected{/if}>{$item.country|escape}</option>
                    {/foreach}
                    </select>
                </td>
				<td style="padding-right: 15px;">
                    Город:<br />
                    <select id="city_id" name="city_id" style="width: 150px">
                        <option value="0">не определен</option>
                    {foreach from=$cities item=item}
                        <option value="{$item.city_id|escape}">{$item.city|escape}</option>
                    {/foreach}
                    </select>
                </td>
				<td >Адрес:<br /><input name="address" id="address" type="text" style="width:250px" /></td>
			  </tr>
			</table>
		</div>

        <div class="form-item">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-right: 15px;">Основной телефон:<br /><input name="phone1" id="phone1" type="text" style="width:165px" value="" /></td>
                    <td style="padding-right: 15px;">Добавочный номер:<br /><input name="phone1_ext" type="text" id="phone1_ext" style="width:165px" value="" /></td>
                </tr>
            </table>
        </div>

        <div class="form-item">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-right: 15px;">Дополнительный телефон:<br /><input name="phone2" id="phone2" type="text" style="width:165px" value="" /></td>
                    <td style="padding-right: 15px;">Добавочный номер:<br /><input name="phone2_ext" id="phone2_ext" type="text" style="width:165px" value="" /></td>
                </tr>
            </table>
        </div>

        <div class="form-item">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-right: 15px;">Факс:<br /><input name="fax" id="fax" type="text" style="width:165px" /></td>
                    <td >* Электронная почта (показывается на сайте):<br /><input name="email" id="email" type="text" style="width:170px" /></td>
                </tr>
            </table>
        </div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;">Сайт:<br /><input name="www" id="www" type="text" style="width:165px" /></td>
				<td style="padding-right: 15px;">ICQ:<br /><input name="icq" id="icq" type="text" style="width:165px" /></td>
				<td >Skype:<br /><input name="skype" id="skype" type="text" style="width:170px" /></td>
			  </tr>
			</table>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
        <td style="padding-right: 15px;">Viber:<br /><input name="viber" id="viber" type="text" style="width:165px" /></td>
        <td style="padding-right: 15px;">WhatsApp:<br /><input name="whatsapp" id="whatsapp" type="text" style="width:165px"/></td>
        <td >Telegram:<br /><input name="telegram" id="telegram" type="text" style="width:165px" /></td>
			  </tr>
			</table>
		</div>

		<div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
        <td style="padding-right: 15px;">Facebook:<br /><input name="facebook" id="facebook" type="text" style="width:165px" /></td>
        <td style="padding-right: 15px;">ВК:<br /><input name="vk" id="vk" type="text" style="width:165px" /></td>
        <td >Instagram:<br /><input name="instagram" id="instagram" type="text" style="width:165px" /></td>
			  </tr>
			</table>
		</div>

    <div class="form-item">
			<table  border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;">ИНН:<br /><input name="inn" id="inn" type="text" style="width:165px" /></td>
				<td style="padding-right: 15px;">КПП:<br /><input name="kpp" id="kpp" type="text" style="width:165px" /></td>
				<td >ОГРН/ОГРНИП:<br /><input name="ogrn" id="ogrn" type="text" style="width:210px" /></td>
			  </tr>
			</table>
		</div>

        <div class="form-item">
			Юридический адрес:<br />
            <input name="address_jur" id="address_jur" type="text" style="width:540px" />
		</div>
    
    <div class="form-item">
			Адрес доставки почтовой корреспонденции:<br />
      <input name="address_post" id="address_post" type="text" style="width:540px" />
		</div>

		<div class="form-item">
			Информация о компании:<br />
			<textarea name="about" id="about" rows="4" style="width:98%"></textarea>
		</div>

        <div class="form-item">
            <input type="checkbox" id="individual_buyers" name="individual_buyers" value="1" />
            работа с физическими лицами
        </div>

        <div class="form-item">
            Минимальная сумма заказа:<br />
            <input name="min_order_amount" id="min_order_amount" type="text" style="width: 200px;" />
        </div>

        <div class="form-item">
            <input type="checkbox" id="allow_download_price" name="allow_download_price" value="1" />
            разрешать скачивать прайс
        </div>

        <div class="form-item">
            Цена включает НДС:
            <label style="display: inline-block;"><input type="radio" name="price_with_nds" value="1" checked> да</label>
            <label style="display: inline-block;"><input type="radio" name="price_with_nds" value="0"> нет</label>
        </div>
	</fieldset>

    <fieldset>
        <legend>Уведомления</legend>

        <div class="form-item">
            * Электронная почта для уведомлений об обработке прайса:<br />
            <input name="email_price_notification" id="email_price_notification" type="text" style="width: 170px;" />
        </div>

        <div class="form-item">
            * Электронная почта для уведомлений о заказах:<br />
            <input name="email_order_notification" id="email_order_notification" type="text" style="width: 170px;" />
        </div>

        <div class="form-item">
            <input id="email_notification" type="checkbox" value="1" name="email_notification" />
            уведомления на эл. почту
        </div>
    </fieldset>

	<fieldset>
		<legend>Контактная персона</legend>

		<div class="form-item">
			ФИО:<br /><input name="person_fio" id="person_fio" type="text" style="width:90%" />
		</div>

		<div class="form-item">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td style="padding-right: 15px;" width="35%">Электронная почта:<br /><input name="person_email" id="person_email" type="text" style="width:100%" /></td>
				<td style="padding-right: 15px;">Дополнительная информация:<br /><input name="person_info" id="person_info" type="text" style="width:100%" /></td>
			  </tr>
			</table>
		</div>
    </fieldset>


	<fieldset>
		<legend>Документы пользователя</legend>
        <div class="form-item">
            <div id="readblock_docs" style="display: none">
                <input type="checkbox" name="docs_show_x[]" value="1" title="Показывать на сайте" onclick="UsersDocShow(this)" />
                <input type="hidden" name="docs_show[]" value="0" />
                <input type="text" name="docs_titles[]" value="" style="width: 250px "/>
                <input type="file" name="docs[]"  /> <img src="i/icons/btn_cancel.gif" height="15" width="15" alt="Удалить" style="cursor: pointer" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" >
            </div>
            <div id="writeblock_docs"></div>
        </div>
        <div><a href="javascript: MoreBlocks('readblock_docs', 'writeblock_docs');" >+ Добавить</a></div>
	</fieldset>


    <fieldset>
        <legend>API</legend>

        <div class="form-item">
            Разрешенные IP-адреса для выгрузки заказов через API:<br /><input name="api_allowed_ip" id="api_allowed_ip" type="text" value="" style="width:95%" />
            <small>Перечислять через запятую, если поле пустое, то выгрузка разрешена на любой IP-адрес</small>
        </div>

    </fieldset>


	<fieldset>
		<legend>Разное</legend>

		<div class="form-item">
			Максимальное количество строк в прайс-листе:<br /><input name="price_limit" id="price_limit" type="text" value="500000" style="width:50%" />
		</div>

		<div class="form-item">
			Комментарии к пользователю:<br />
			<textarea name="comment" id="comment" rows="4" style="width:95%"></textarea>
		</div>

	</fieldset>

	<br />

	<div class="form-item">
		<button onclick="SubmitForm()" type="button"><img src="i/icons/btn_add.gif" width="15" height="15" hspace="5" align="absmiddle" />Добавить пользователя</button>
		<button type="reset" ><img src="i/icons/btn_cancel.gif" width="15" height="15" hspace="5" align="absmiddle" />Очистить</button>
	</div>
</form>

{literal}
    <script type="text/javascript">
        jQuery(function()
        {
            jQuery('#phone1, #phone2, #fax, #viber, #whatsapp').mask('+7 (999) 999-99-99');
        });

        jQuery('#phone1_ext, #phone2_ext').on('input paste change blur', function()
        {
            var elem = jQuery(this);

            elem.val(String(elem.val()).replace(/[^\d]/g, '').substr(0, 4));
        });

        // Проверка логина на уникальность
        jQuery(document).ready(function(){
            jQuery('#login').on('change', function()
            {
                var login = jQuery(this);

                new Ajax.Request('ajaxer.php?x=users.ajax_check_login', {
                    method: 'post',
                    parameters: { login: login.val(), new: true },
                    onSuccess: function(data) {
                        if (data.responseText !== 'OK') {
                            alert("Данный логин уже используется в системе, выберите другой");
                            login.addClass('f_incorrect');
                        } else {
                            login.removeClass('f_incorrect');
                        }
                    }
                });
            });
        });
    </script>
{/literal}
