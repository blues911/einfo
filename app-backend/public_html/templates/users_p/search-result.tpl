<table width="100%" cellpadding="3" cellspacing="0"  rules="rows" class="table-items">
  <tr align="center">
    <th width="5%">№</th>
    <th >Пользователи</th>
	<th width="30">ID</th>
	<th width="60">Login</th>
	<th width="30">Статус</th>
	<th width="30">Ред.</th>
    <th width="30">Удал.</th> 	
  </tr>
  {foreach from=$list item=item}
  <tr valign="top" {if $item.level_id == 0}style="background-color: #E9E9E9"{/if}>
    <td align="center" style="padding-right: 5px;">{$item.num|escape}.</td>
    <td><a href="?module=users_p&action=edit-form&id={$item.id|escape}">{$item.title|escape}</a> <span style="color: gray">[{$item.login|escape}]</span></td>
	<td align="center">{$item.id|escape}</td>
	<td align="center">{if $item.status == 1}<a href="?module=users_p&action=login&id={$item.id|escape}"><img src="i/icons/icon_key.gif" width="18" height="18" border="0" alt="Залогиниться под пользователем" /></a>{/if}</td>
	<td align="center">
		{if $item.status == -1}
		<img src="i/icons/icon_moder.gif" border="0" alt="Ожидает модерации" title="Ожидает модерации" />
		{elseif $item.status == 0}
		<img src="i/icons/icon_ok_inactive.gif" border="0" alt="Заблокирован" title="Заблокирован" />
		{elseif $item.status == 1}
		<img src="i/icons/icon_ok_active.gif" border="0" alt="Активный" title="Активный" />
		{elseif $item.status == -2}
		<span style="color: gray">&mdash;</span>
		{/if}
	</td>
	<td align="center"><a href="?module=users_p&action=edit-form&id={$item.id|escape}"><img src="i/icons/icon_edit.gif" width="15" height="18" border="0" alt="Редактировать" /></a></td>
    <td align="center">{if $item.level_id != 0}<a href="#" onclick="DeleteUser({$item.id|escape})"><img src="i/icons/icon_del.gif" border="0" width="15" height="18" alt="Удалить"/></a>{/if}</td>
  </tr>
  {/foreach}
</table>