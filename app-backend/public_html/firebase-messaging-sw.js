/**
 * firebase-messanging-sw.js
 *
 * Service worker for browser
 */

importScripts('https://www.gstatic.com/firebasejs/7.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.2.0/firebase-messaging.js');

firebase.initializeApp({
    messagingSenderId: '593124678630'
});

const messaging = firebase.messaging();