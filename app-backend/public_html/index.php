<?php

include_once("../../config.php");
include_once(PATH_BACKEND . "/php/function.php");
include_once(PATH_BACKEND . "/php/Smarty/Smarty.class.php");
include_once(PATH_BACKEND . "/php/adodb/adodb.inc.php");
include_once(PATH_BACKEND . "/php/classes/class.AccessControl.php");
include_once(PATH_BACKEND . "/php/classes/class.Pager.php");
include_once(PATH_BACKEND . "/php/classes/class.MyDate.php");
include_once(PATH_BACKEND . "/php/classes/class.phpmailer.php");


// подключение к БД
$DB = ADONewConnection(DB_MAIN_DSN);
$DB->SetFetchMode(ADODB_FETCH_ASSOC);
$DB->Execute("SET NAMES utf8");
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;
//$DB->debug = true;

// контроль доступа
$control = new AccessControl($DB);

// дата
$date = new MyDate();

// шаблонизатор
$smarty = new Smarty;
$smarty->template_dir = PATH_BACKEND."/templates";
$smarty->compile_dir = PATH_TMP."/tpl-user";

// --------------------------------------------------------------------------

session_start();
$control->CheckSession();

if (isset($_POST['auth']) && $_POST['login'] != "" && $_POST['password'] != "")
{
    if (!$control->Auth($_POST['login'], $_POST['password'], isset($_POST['remember'])))
    {
        $smarty->assign("auth_message", $control->GetMessage());
    }
}

if ($control->IsAuth())
{
    define("ADMIN", 1);

    $userinfo = $DB->GetRow("SELECT * FROM users WHERE id=?", array($_SESSION['user_id']));

    $container['company_title'] = $userinfo['title'];
    $container['userlogin'] = $userinfo['login'];
    $container['userID'] = $userinfo['id'];
    $container['help_url'] = "index";
    $container['users_p'] = !empty($_SESSION['users_p']);
    $container['users_chat_access'] = $control->CheckModuleAccess('users_chat');

    ob_start();
        include_once (PATH_BACKEND . "/php/common/menu.php");

	    if (isset($_GET['module']) && preg_match("/^[-a-z0-9_]+$/i", $_GET['module']))
	    {
	        if (file_exists("modules/".$_GET['module']."/".$_GET['module'].".php"))
	        {
	            include_once (PATH_BACKEND . "/php/common/settings.php");
	            include_once (PATH_BACKEND . "/php/common/module_actions.php");
                $smarty->assign("settings", $SETTINGS);
                
	            include_once ("modules/".$_GET['module']."/".$_GET['module'].".php");

	            // URL для справки
	            $container['help_url'] = (isset($_GET['action'])) ? $_GET['module'].".".$_GET['action'] : $_GET['module'];
	        }
	        else
	        {
	            adm_message("Данный модуль не существует");
	        }
	    }
	    else if ($userinfo['disclaimer'] == 0)
	    {
	        header("Location: ?module=cp&action=disclaimer");
	    }
        // Если пользователь авторизовался с внутренней страницы, возвращаем его обратно
        elseif (isset($_POST['auth']) && !empty($_SERVER['HTTP_REFERER']) && preg_match('/^http(s)?:\/\/' . HOST_BACKEND . '\//i', $_SERVER['HTTP_REFERER']))
        {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
	    else
	    {
	        header("Location: ?module=sysnews&action=view");
	        //include_once ("templates/default.html");
	    }

	    $container['content'] = ob_get_contents();
    ob_end_clean();

    $container['logoff'] = (isset($_SESSION['logoff']) && $_SESSION['logoff']) ? $_SESSION['logoff_id'] : false;

    $smarty->assign("main", $container);
    $smarty->assign("chat_host", HOST_CHAT);
    $smarty->assign("chat_host_ws", HOST_CHAT_WS);
    $smarty->assign("chat_host_frontend", HOST_FRONTEND);
		$smarty->assign("chat_last_visit_interval", CHAT_LAST_VISIT_INTERVAL);
    $smarty->display("_main.tpl");
}
else if (isset($_GET['register']))
{

    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
    exit;

    ob_start();
        include_once ("modules/_register/register.php");
        $container['content'] = ob_get_contents();
    ob_end_clean();

    $smarty->assign("main", $container);
    $smarty->display("_register/register.tpl");
}
else if (isset($_GET['remember']))
{
    ob_start();
        include_once ("modules/_remember/remember.php");
        $container['content'] = ob_get_contents();
    ob_end_clean();

    $smarty->assign("main", $container);
    $smarty->display("_remember/remember.tpl");
}
else
{
    $smarty->display("enter.tpl");
}