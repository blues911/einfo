function GetCheckedValue(radioObj)
{
	if(!radioObj) return false;
	
	if(radioObj.length == undefined)
	{
		if(radioObj.checked) return radioObj.value;
		else return false;
	}
	
	for(i = 0; i < radioObj.length; i++)
	{
		if(radioObj[i].checked)	return radioObj[i].value;
	}
	
	return false;
}

function EnableEditor(ids)
{
	tinyMCE.init({
		mode : "exact",
		elements: ids,
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_buttons1 : "cut,copy,paste,separator,undo,redo,separator,bold,italic,underline,forecolor,separator,bullist,numlist,separator,outdent,indent,separator,link,unlink,image,separator,removeformat,cleanup,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		language : "ru"
	});
}

function PreviewImg(id)
{	
	if ($F(id) != "")
	{
		$("preview_" + id).src = $F(id);
	}
}

function CheckFieldsForm(fields)
{
	fields_error = new Array();
	
	fields.each(function(f, index) { if ($F(f) == '' || $F(f) == -1) fields_error.push(f); });
	fields.each(function(f, index) { $(f).removeClassName('f_incorrect'); });
	
	if (fields_error.length == 0) return true;
	else
	{	
		fields_error.each(function(f, index) { $(f).className = 'f_incorrect'; }); 
		return false;
	}	
}

function isValidUrl(url)
{
     return url.match(/^(ht|f)tps?:\/\/[a-zа-яё0-9-\.]+\.[a-zа-яё]{2,6}\/?.*$/i);
}

function isValidIp(url)
{
    return url.match(/^(ht|f)tps?:\/\/(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}\/?.*$/);
}

function DisableEnterKey(e)
{
     var key;
     if(window.event) {
          key = window.event.keyCode;
     }
     else {
          key = e.which;
     }

     return (key != 13);
}

// CSS fix
var cssFix = function() {
	var u = navigator.userAgent.toLowerCase(),

	addClass = function(el,val) {
		if(!el.className) {
			el.className = val;
		}
		else {
      		var newCl = el.className;
			newCl += (" "+val);
			el.className = newCl;
		}
	},

	is = function(t) { return ( u.indexOf(t) != -1) };

	addClass(document.getElementsByTagName('html')[0],[
	(!(/opera|webtv/i.test(u))&&/msie (\d)/.test(u))?('ie ie'+RegExp.$1)
	  :is('firefox/2')?'gecko ff2'
	  :is('firefox/3')?'gecko ff3'
	  :is('gecko/')?'gecko'
	  :is('opera/9')?'opera opera9':/opera (\d)/.test(u)?'opera opera'+RegExp.$1
	  :is('konqueror')?'konqueror'
	  :is('applewebkit/')?'webkit safari'
	  :is('mozilla/')?'gecko':'',
	(is('x11')||is('linux'))?' linux'
	  :is('mac')?' mac'
	  :is('win')?' win':''
	].join(" "));
}();

// Датапикер

jQuery(document).ready(function()
{
    jQuery('input.datepicker').Zebra_DatePicker({
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        show_select_today: 'Сегодня',
        lang_clear_date: 'Очистить',
        format: 'd.m.Y',
        readonly_element: false,
        view: 'months'
    });

    jQuery('input.datepicker').mask('99.99.9999', { placeholder: '__.__.____' });
});

// Implode
function implode(glue, pieces) {
    return ((pieces instanceof Array) ? pieces.join (glue) : pieces);
}
