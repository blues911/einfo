// -------------- remember ----------------------
function Remember()
{
	if (CheckFieldsForm(['email', 'captcha'])) {
        $$('.message').each(function(el) { el.hide(); });

        new Ajax.Request('ajaxer.php?x=_remember.ajax_post', {
            method: 'post',
            parameters: { email: $F('email'), captcha: $F('captcha') },
            onSuccess: function(transport) {
                if (transport.responseText == 'OK') {
                    $('message_OK').show();
                    $('email').disable();
                    $('captcha').disable();
                    $('btn').hide();
                }
                else if (transport.responseText == 'NONE') {
                    $('message_NONE').show();
                }
                else if (transport.responseText == 'NOT_VALID') {
                    $('message_NOT_VALID').show();
                }
                else if (transport.responseText == 'CAPTCHA') {
                    $('message_CAPTCHA').show();
                }
                else if (transport.responseText == 'SUPPORT') {
                    $('message_SUPPORT').show();
                }
            }
        });

	}
	else alert("Не все обязательные поля заполнены");	
}