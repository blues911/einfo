// -------------- register ----------------------
function DisclaimerCheck() 
{
	if ($F('disclaimer'))
	{
		$('reg_button').show();		
	}
	else
	{
		$('reg_button').hide();		
	}	
}

function Register()
{
	filled.push('password_retype');	
	
	if (CheckFieldsForm(filled))
	{
		if ($F('password') == $F('password_retype'))
		{
            if ($('www') && $F('www') != '' && !isValidUrl($F('www')))
            {
                alert("�������� ������ URL, ����� ����� ������ ���������� � �http://�");
                return false;
            }

            $('reg_tr').hide();
            new Ajax.Request('ajaxer.php?x=_register.ajax_check_login', {
                method: 'post',
                parameters: { login: $F('login') },
                onSuccess: function(transport) {
                    if (transport.responseText == 'OK')
                    {
                        $('login_message').hide();
                        PostRegisterData();
                    }
                    else
                    {
                        $('login_message').show();
                        $('reg_tr').show();
                        alert("������ ����� ��� ������������ � �������, �������� ������");
                    }
                }
            });

		}
		else alert("��������� ������ �� ���������");
	}
	else alert("�� ��� ������������ ���� ���������");	
}

function PostRegisterData()
{
	post_data = { account_type: $F('account_type') };
	arr_fields.each(function(s) { post_data[s] = $F(s); });
	
	arr_fields.each(Form.Element.disable);
	arr_fields.push('password_retype');	
	
	$('reg_message_x').hide();
	$('captcha_message').hide();
	
	new Ajax.Request('ajaxer.php?x=_register.ajax_post', {						 
		method: 'post',
		parameters: post_data,
		onSuccess: function(transport) {				
			if (transport.responseText == 'OK')
			{
				arr_fields.each(Form.Element.disable);
				$('reg_message_x').show();
			}
			else if (transport.responseText == 'CAPTCHA')
			{
				arr_fields.each(Form.Element.enable);
				alert("��������� ��� �������, ���������� ��� ���");	
				$('captcha_message').show();
				$('reg_tr').show();							
			}			
			else
			{	
				arr_fields.each(Form.Element.enable);
				$('reg_tr').show();
				alert("��������� ������ ��� �������� ������. ���������� ��� ���.");	
			}
		}
	});	
}

function UpdateCountry() {
    if ($F('country_id') == 186) {
        $('city_id').enable();
    }
    else {
        $('city_id').disable();
        $('city_id').selectedIndex = 0;
    }
}