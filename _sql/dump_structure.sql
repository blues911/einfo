/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.5.44-0ubuntu0.12.04.1 : Database - e2_main
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `access` */

DROP TABLE IF EXISTS `access`;

CREATE TABLE `access` (
  `level_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `module_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`level_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `access_levels` */

DROP TABLE IF EXISTS `access_levels`;

CREATE TABLE `access_levels` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=cp1251;

/*Table structure for table `access_levels_fields` */

DROP TABLE IF EXISTS `access_levels_fields`;

CREATE TABLE `access_levels_fields` (
  `level_id` int(11) unsigned NOT NULL DEFAULT '0',
  `field_id` int(11) unsigned NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '0',
  `fill` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`level_id`,`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `accounts` */

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_id` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `moderate` tinyint(1) NOT NULL DEFAULT '1',
  `disclaimer` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `level` (`level_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=cp1251;

/*Table structure for table `adv_system` */

DROP TABLE IF EXISTS `adv_system`;

CREATE TABLE `adv_system` (
  `var` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `system` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `value` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  UNIQUE KEY `system_var_uniq` (`system`,`var`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `adv_template` */

DROP TABLE IF EXISTS `adv_template`;

CREATE TABLE `adv_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned DEFAULT NULL,
  `comp_like` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `yandex_title` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `yandex_descr` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `yandex_adtitle_1` varchar(255) DEFAULT NULL,
  `yandex_adlink_1` varchar(255) DEFAULT NULL,
  `yandex_adtitle_2` varchar(255) DEFAULT NULL,
  `yandex_adlink_2` varchar(255) DEFAULT NULL,
  `yandex_adtitle_3` varchar(255) DEFAULT NULL,
  `yandex_adlink_3` varchar(255) DEFAULT NULL,
  `yandex_adtitle_4` varchar(255) DEFAULT NULL,
  `yandex_adlink_4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Table structure for table `adv_yandex` */

DROP TABLE IF EXISTS `adv_yandex`;

CREATE TABLE `adv_yandex` (
  `comp_canon` varchar(255) NOT NULL,
  `offer_cnt` int(11) NOT NULL,
  `title` varchar(33) CHARACTER SET cp1251 NOT NULL,
  `descr` varchar(75) CHARACTER SET cp1251 NOT NULL,
  `url` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `phrase` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `price` float(10,2) unsigned NOT NULL,
  `show` int(11) unsigned DEFAULT NULL,
  `action` int(11) NOT NULL DEFAULT '0',
  `banner_id` int(11) unsigned DEFAULT NULL,
  `campaign_id` int(11) unsigned DEFAULT NULL,
  `phrase_id` int(11) unsigned DEFAULT NULL,
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adtitle_1` varchar(255) DEFAULT NULL,
  `adlink_1` varchar(255) DEFAULT NULL,
  `adtitle_2` varchar(255) DEFAULT NULL,
  `adlink_2` varchar(255) DEFAULT NULL,
  `adtitle_3` varchar(255) DEFAULT NULL,
  `adlink_3` varchar(255) DEFAULT NULL,
  `adtitle_4` varchar(255) DEFAULT NULL,
  `adlink_4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`comp_canon`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `adv_yandex_sitelink` */

DROP TABLE IF EXISTS `adv_yandex_sitelink`;

CREATE TABLE `adv_yandex_sitelink` (
  `hash` varchar(32) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`hash`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `adv_yandex_snapshot` */

DROP TABLE IF EXISTS `adv_yandex_snapshot`;

CREATE TABLE `adv_yandex_snapshot` (
  `comp_canon` varchar(255) NOT NULL,
  `offer_cnt` int(11) NOT NULL,
  `title` varchar(33) CHARACTER SET cp1251 NOT NULL,
  `descr` varchar(75) CHARACTER SET cp1251 NOT NULL,
  `url` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `phrase` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `price` float(10,2) unsigned NOT NULL,
  `show` int(11) unsigned DEFAULT NULL,
  `action` int(11) NOT NULL DEFAULT '0',
  `banner_id` int(11) unsigned DEFAULT NULL,
  `campaign_id` int(11) unsigned DEFAULT NULL,
  `phrase_id` int(11) unsigned DEFAULT NULL,
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `adtitle_1` varchar(255) DEFAULT NULL,
  `adlink_1` varchar(255) DEFAULT NULL,
  `adtitle_2` varchar(255) DEFAULT NULL,
  `adlink_2` varchar(255) DEFAULT NULL,
  `adtitle_3` varchar(255) DEFAULT NULL,
  `adlink_3` varchar(255) DEFAULT NULL,
  `adtitle_4` varchar(255) DEFAULT NULL,
  `adlink_4` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`comp_canon`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `api_log` */

DROP TABLE IF EXISTS `api_log`;

CREATE TABLE `api_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `token_id` int(11) unsigned DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(255) NOT NULL DEFAULT '0.0.0.0',
  `request` text,
  `response` text,
  `service` varchar(255) NOT NULL DEFAULT 'empty',
  PRIMARY KEY (`id`),
  KEY `token_id_index` (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=cp1251;

/*Table structure for table `api_users` */

DROP TABLE IF EXISTS `api_users`;

CREATE TABLE `api_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `date_register` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_valid_to` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_uniq` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=cp1251;

/*Table structure for table `base_main` */

DROP TABLE IF EXISTS `base_main`;

CREATE TABLE `base_main` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(255) NOT NULL,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `atoms` varchar(255) NOT NULL,
  `atoms2` varchar(255) NOT NULL,
  `comp_title` varchar(255) NOT NULL,
  `manuf_title` varchar(255) NOT NULL,
  `descr` varchar(255) NOT NULL,
  `roz` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
  `roz_cur` enum('rur','usd','eur') NOT NULL DEFAULT 'rur',
  `opt` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
  `opt_cur` enum('rur','usd','eur') NOT NULL DEFAULT 'rur',
  `mfgdate` year(4) NOT NULL DEFAULT '0000',
  `doc_url` varchar(255) NOT NULL DEFAULT '',
  `comp_url` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `delivery` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `comp_title_index` (`comp_title`),
  KEY `hash_index` (`hash`),
  KEY `main_index` (`id`,`user_id`,`comp_title`),
  KEY `user_index` (`user_id`),
  KEY `atoms_index` (`atoms`),
  KEY `atoms2` (`atoms2`)
) ENGINE=MyISAM AUTO_INCREMENT=1135556366 DEFAULT CHARSET=cp1251;

/*Table structure for table `basket_tmp` */

DROP TABLE IF EXISTS `basket_tmp`;

CREATE TABLE `basket_tmp` (
  `basket_key` varchar(32) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `main_id` bigint(21) unsigned NOT NULL,
  `cnt` int(11) unsigned NOT NULL,
  `confirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `uniq` (`basket_key`,`main_id`),
  KEY `com_main` (`main_id`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `billing` */

DROP TABLE IF EXISTS `billing`;

CREATE TABLE `billing` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payment_number` varchar(255) DEFAULT NULL,
  `payment_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `active_from` date NOT NULL DEFAULT '0000-00-00',
  `active_to` date NOT NULL DEFAULT '0000-00-00',
  `file` longblob,
  `file_type` varchar(64) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `billing_user_id_index` (`user_id`),
  KEY `billing_active_from_index` (`active_from`),
  KEY `billing_active_to_index` (`active_to`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=cp1251;

/*Table structure for table `catalog_comp_canon` */

DROP TABLE IF EXISTS `catalog_comp_canon`;

CREATE TABLE `catalog_comp_canon` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT,
  `regexp_id` int(11) unsigned DEFAULT NULL,
  `category_id` int(11) unsigned DEFAULT NULL,
  `comp_canon` varchar(255) NOT NULL,
  `offer_cnt` int(11) unsigned NOT NULL DEFAULT '0',
  `price_opt_median` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
  `price_roz_median` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
  `adv_show` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `comp_canon_unique` (`comp_canon`),
  KEY `category_offer_cnt_index` (`category_id`,`offer_cnt`)
) ENGINE=MyISAM AUTO_INCREMENT=1708986 DEFAULT CHARSET=cp1251;

/*Table structure for table `catalog_regexp` */

DROP TABLE IF EXISTS `catalog_regexp`;

CREATE TABLE `catalog_regexp` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `regexp_template` text NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  `need_update` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category_id_index` (`category_id`),
  KEY `need_update_index` (`need_update`)
) ENGINE=MyISAM AUTO_INCREMENT=181 DEFAULT CHARSET=cp1251;

/*Table structure for table `catalog_workers` */

DROP TABLE IF EXISTS `catalog_workers`;

CREATE TABLE `catalog_workers` (
  `worker_id` varchar(32) NOT NULL,
  `task` mediumtext,
  `last_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`worker_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `c_left` int(11) unsigned NOT NULL DEFAULT '0',
  `c_right` int(11) unsigned NOT NULL DEFAULT '0',
  `c_level` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `descr` text NOT NULL,
  `descr_position` enum('top','bottom') NOT NULL DEFAULT 'bottom',
  `path` varchar(100) NOT NULL DEFAULT '',
  `full_path` varchar(255) NOT NULL DEFAULT '',
  `full_title` varchar(255) NOT NULL,
  `cnt` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nav` (`path`,`c_left`,`c_right`),
  KEY `title` (`title`),
  KEY `flag` (`c_level`,`c_left`)
) ENGINE=MyISAM AUTO_INCREMENT=225152 DEFAULT CHARSET=cp1251;

/*Table structure for table `cli_log` */

DROP TABLE IF EXISTS `cli_log`;

CREATE TABLE `cli_log` (
  `user_id` int(11) unsigned NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `script` varchar(255) NOT NULL,
  `ok` int(11) unsigned NOT NULL DEFAULT '0',
  `fail` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `clients` */

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '-1: ожидает активации; 0 - не активный; 1 - активный;',
  `date_register` datetime NOT NULL,
  `date_last_login` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `details_in_order` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Добавлять ли details к коменнтариям заказа',
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `auth_hash` varchar(32) NOT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=44051 DEFAULT CHARSET=cp1251;

/*Table structure for table `clients_list_log` */

DROP TABLE IF EXISTS `clients_list_log`;

CREATE TABLE `clients_list_log` (
  `ip` varchar(15) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `client_id` int(11) unsigned NOT NULL,
  `list` enum('black','white') NOT NULL,
  `action` enum('add','delete') NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `action_black_white_list_user_id_client_id_index` (`action`,`list`,`user_id`,`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `components_effect_analysis` */

DROP TABLE IF EXISTS `components_effect_analysis`;

CREATE TABLE `components_effect_analysis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rows_count` int(11) unsigned NOT NULL,
  `current_row` int(11) unsigned NOT NULL DEFAULT '0',
  `count_profile_click` int(11) unsigned DEFAULT NULL,
  `count_profile_view` int(11) unsigned DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=298 DEFAULT CHARSET=cp1251;

/*Table structure for table `content_monitoring` */

DROP TABLE IF EXISTS `content_monitoring`;

CREATE TABLE `content_monitoring` (
  `cm_id` char(32) NOT NULL,
  `cm_md5` char(32) DEFAULT NULL,
  `cm_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `datasheet` */

DROP TABLE IF EXISTS `datasheet`;

CREATE TABLE `datasheet` (
  `comp_canon` varchar(255) NOT NULL,
  `remote` varchar(255) NOT NULL,
  `local` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`comp_canon`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `event_stat` */

DROP TABLE IF EXISTS `event_stat`;

CREATE TABLE `event_stat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stat_id` varchar(50) NOT NULL COMMENT 'Кука отдельного пользователя',
  `date` datetime NOT NULL,
  `ip` varchar(20) NOT NULL DEFAULT '',
  `hardware` enum('mobile','desktop') NOT NULL DEFAULT 'desktop',
  `useragent` varchar(100) NOT NULL DEFAULT '',
  `referer` varchar(255) NOT NULL DEFAULT '',
  `screen` varchar(50) NOT NULL DEFAULT '',
  `event_type` varchar(100) NOT NULL DEFAULT '',
  `event_label` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `stat_id_index` (`stat_id`),
  KEY `event_type_index` (`event_type`),
  KEY `event_label_index` (`event_label`)
) ENGINE=MyISAM AUTO_INCREMENT=12749973 DEFAULT CHARSET=cp1251;

/*Table structure for table `filters` */

DROP TABLE IF EXISTS `filters`;

CREATE TABLE `filters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title_regexp` varchar(255) NOT NULL,
  `title_like` text NOT NULL,
  `need_update` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category_index` (`category_id`),
  KEY `need_update_index` (`need_update`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=cp1251;

/*Table structure for table `forum_messages` */

DROP TABLE IF EXISTS `forum_messages`;

CREATE TABLE `forum_messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `topic_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mess` (`topic_id`,`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=cp1251;

/*Table structure for table `forum_topics` */

DROP TABLE IF EXISTS `forum_topics`;

CREATE TABLE `forum_topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) unsigned DEFAULT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `pinned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=cp1251;

/*Table structure for table `ip2ruscity_cities` */

DROP TABLE IF EXISTS `ip2ruscity_cities`;

CREATE TABLE `ip2ruscity_cities` (
  `city_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL,
  `phone_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  KEY `region` (`region_id`),
  KEY `city` (`city`)
) ENGINE=MyISAM AUTO_INCREMENT=449 DEFAULT CHARSET=utf8 COMMENT='Города';

/*Table structure for table `ip2ruscity_countries` */

DROP TABLE IF EXISTS `ip2ruscity_countries`;

CREATE TABLE `ip2ruscity_countries` (
  `country_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`country_id`),
  KEY `country` (`country`)
) ENGINE=MyISAM AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;

/*Table structure for table `ip2ruscity_ip2country` */

DROP TABLE IF EXISTS `ip2ruscity_ip2country`;

CREATE TABLE `ip2ruscity_ip2country` (
  `num_ip_start` int(10) unsigned NOT NULL DEFAULT '0',
  `num_ip_end` int(10) unsigned NOT NULL DEFAULT '0',
  `country_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`num_ip_start`,`num_ip_end`,`country_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `ip2ruscity_ip_compact` */

DROP TABLE IF EXISTS `ip2ruscity_ip_compact`;

CREATE TABLE `ip2ruscity_ip_compact` (
  `num_ip_start` int(10) unsigned NOT NULL DEFAULT '0',
  `num_ip_end` int(10) unsigned NOT NULL DEFAULT '0',
  `city_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`num_ip_start`,`num_ip_end`,`city_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `ip2ruscity_regions` */

DROP TABLE IF EXISTS `ip2ruscity_regions`;

CREATE TABLE `ip2ruscity_regions` (
  `region_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `region` varchar(255) NOT NULL,
  PRIMARY KEY (`region_id`),
  KEY `region` (`region`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Регионы';

/*Table structure for table `load_tracking` */

DROP TABLE IF EXISTS `load_tracking`;

CREATE TABLE `load_tracking` (
  `ip` varchar(15) NOT NULL,
  `first_hit` double(53,2) unsigned NOT NULL,
  `cnt` int(10) NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `map_component` */

DROP TABLE IF EXISTS `map_component`;

CREATE TABLE `map_component` (
  `comp_title` varchar(255) NOT NULL,
  `atom_1` varchar(32) DEFAULT NULL,
  `atom_2` varchar(32) DEFAULT NULL,
  `atom_3` varchar(32) DEFAULT NULL,
  `atom_4` varchar(32) DEFAULT NULL,
  `atom_5` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`comp_title`),
  KEY `atoms_index` (`atom_1`,`atom_2`,`atom_3`,`atom_4`,`atom_5`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `showmenu` tinyint(1) NOT NULL DEFAULT '1',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `default_action` varchar(100) DEFAULT NULL,
  `ip_filter` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=cp1251;

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `img` varchar(100) NOT NULL DEFAULT '',
  `img_blob` longblob,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`,`status`)
) ENGINE=MyISAM AUTO_INCREMENT=22301 DEFAULT CHARSET=cp1251;

/*Table structure for table `news_images` */

DROP TABLE IF EXISTS `news_images`;

CREATE TABLE `news_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `upload_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL DEFAULT '',
  `img` varchar(100) NOT NULL DEFAULT '',
  `img_blob` longblob,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=208 DEFAULT CHARSET=cp1251;

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comments` text NOT NULL,
  `client_id` int(11) NOT NULL,
  `valid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `send` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=91965 DEFAULT CHARSET=cp1251;

/*Table structure for table `orders_items` */

DROP TABLE IF EXISTS `orders_items`;

CREATE TABLE `orders_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `rcount` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `manuf` varchar(255) NOT NULL,
  `descr` varchar(255) NOT NULL,
  `roz` float unsigned NOT NULL DEFAULT '0',
  `roz_cur` enum('rur','usd','eur') NOT NULL DEFAULT 'rur',
  `opt` float unsigned NOT NULL DEFAULT '0',
  `opt_cur` enum('rur','usd','eur') NOT NULL DEFAULT 'rur',
  `mfgdate` year(4) NOT NULL DEFAULT '0000',
  PRIMARY KEY (`id`),
  KEY `basket` (`order_id`),
  KEY `users` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=148795 DEFAULT CHARSET=cp1251;

/*Table structure for table `price_columns` */

DROP TABLE IF EXISTS `price_columns`;

CREATE TABLE `price_columns` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `tag` varchar(100) NOT NULL DEFAULT '',
  `func` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=cp1251;

/*Table structure for table `rating_votes` */

DROP TABLE IF EXISTS `rating_votes`;

CREATE TABLE `rating_votes` (
  `user_voter_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `rating` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_voter_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `resume` */

DROP TABLE IF EXISTS `resume`;

CREATE TABLE `resume` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `fio` varchar(255) NOT NULL DEFAULT '',
  `sex` enum('m','w') NOT NULL DEFAULT 'm',
  `speciality` varchar(255) NOT NULL DEFAULT '',
  `experience` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `age` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `education` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `workplace` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `worktype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `workgraphic` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `language` varchar(255) NOT NULL DEFAULT '',
  `pay` varchar(255) NOT NULL DEFAULT '',
  `contact_phone` varchar(255) NOT NULL DEFAULT '',
  `contact_email` varchar(255) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=cp1251;

/*Table structure for table `search_stat` */

DROP TABLE IF EXISTS `search_stat`;

CREATE TABLE `search_stat` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) DEFAULT NULL,
  `ip_host` varchar(255) DEFAULT NULL,
  `query_user` varchar(255) NOT NULL,
  `query_regex` varchar(255) NOT NULL DEFAULT '',
  `cnt_result` int(11) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  `search_source` enum('main','mobile','telegram') DEFAULT NULL,
  `request_from_user` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `query` (`query_user`,`weight`),
  KEY `date` (`date`),
  KEY `ip` (`ip`),
  KEY `top_index` (`date`,`query_user`),
  KEY `search_source` (`search_source`),
  KEY `user_search` (`request_from_user`,`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=13344896 DEFAULT CHARSET=cp1251;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `var` varchar(80) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`var`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `sysnews` */

DROP TABLE IF EXISTS `sysnews`;

CREATE TABLE `sysnews` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=cp1251;

/*Table structure for table `tmp_prices` */

DROP TABLE IF EXISTS `tmp_prices`;

CREATE TABLE `tmp_prices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `upload_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `tmpfile` varchar(255) NOT NULL DEFAULT '',
  `use_ftemplate` tinyint(1) NOT NULL DEFAULT '1',
  `currency` enum('rur','usd','eur') NOT NULL DEFAULT 'rur',
  `upload_type` enum('replace','add','update') NOT NULL DEFAULT 'replace',
  `stock` tinyint(3) NOT NULL DEFAULT '0',
  `delivery` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25669 DEFAULT CHARSET=cp1251;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_id` smallint(6) NOT NULL DEFAULT '-1',
  `session_hash` varchar(32) NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `valid_key` varchar(40) NOT NULL,
  `price_date` datetime NOT NULL DEFAULT '1970-01-01 03:00:00',
  `price_limit` mediumint(8) unsigned NOT NULL DEFAULT '3000000',
  `title` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `about` text NOT NULL,
  `country_id` int(11) unsigned NOT NULL DEFAULT '0',
  `city_id` int(11) unsigned NOT NULL DEFAULT '0',
  `address` varchar(255) NOT NULL,
  `phone1` varchar(150) NOT NULL,
  `phone1_ext` varchar(4) NOT NULL,
  `phone2` varchar(18) NOT NULL,
  `phone2_ext` varchar(4) NOT NULL,
  `fax` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_price_notification` varchar(100) NOT NULL,
  `email_order_notification` varchar(100) NOT NULL,
  `www` varchar(100) NOT NULL,
  `icq` varchar(11) NOT NULL,
  `skype` varchar(50) NOT NULL,
  `person_fio` varchar(50) NOT NULL,
  `person_email` varchar(150) NOT NULL,
  `person_info` varchar(255) NOT NULL,
  `inn` varchar(20) NOT NULL,
  `kpp` varchar(20) NOT NULL,
  `ogrn` varchar(20) NOT NULL,
  `address_jur` varchar(255) NOT NULL,
  `address_post` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `disclaimer` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `rating` float NOT NULL DEFAULT '0',
  `price_file_type` enum('efind','einfo_xml','einfo_json') DEFAULT NULL,
  `price_file_url` varchar(255) NOT NULL,
  `price_file_login` varchar(255) NOT NULL,
  `price_file_pass` varchar(255) NOT NULL,
  `price_file_use` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `individual_buyers` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `min_order_amount` varchar(255) NOT NULL,
  `email_notification` tinyint(1) NOT NULL DEFAULT '1',
  `cnt` int(11) unsigned NOT NULL DEFAULT '0',
  `allow_download_price` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `price_with_nds` tinyint(1) NOT NULL DEFAULT '1',
  `api_allowed_ip` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `list_index` (`status`,`id`,`rating`)
) ENGINE=MyISAM AUTO_INCREMENT=3031 DEFAULT CHARSET=cp1251;

/*Table structure for table `users_banners` */

DROP TABLE IF EXISTS `users_banners`;

CREATE TABLE `users_banners` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `banner` varchar(100) NOT NULL,
  `banner_blob` longblob,
  `url` varchar(255) NOT NULL,
  `size_x` smallint(4) unsigned NOT NULL,
  `size_y` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

/*Table structure for table `users_docs` */

DROP TABLE IF EXISTS `users_docs`;

CREATE TABLE `users_docs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(100) NOT NULL,
  `fshow` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `file_blob` longblob,
  `file_small_blob` longblob,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=cp1251;

/*Table structure for table `users_fields` */

DROP TABLE IF EXISTS `users_fields`;

CREATE TABLE `users_fields` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `field` (`field`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=cp1251;

/*Table structure for table `users_prices` */

DROP TABLE IF EXISTS `users_prices`;

CREATE TABLE `users_prices` (
  `id` int(21) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `filename` varchar(255) NOT NULL,
  `tags` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5806 DEFAULT CHARSET=utf8;

/*Table structure for table `users_stat` */

DROP TABLE IF EXISTS `users_stat`;

CREATE TABLE `users_stat` (
  `id` bigint(21) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(15) NOT NULL DEFAULT '0.0.0.0',
  `referer` varchar(255) NOT NULL,
  `search_atoms` varchar(255) NOT NULL,
  `action` enum('profile_view','profile_click_site','profile_click_email','profile_click_phone') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_action_date_index` (`user_id`,`action`,`date`),
  KEY `search_atoms_index` (`search_atoms`),
  KEY `ip_index` (`ip`)
) ENGINE=MyISAM AUTO_INCREMENT=5367594 DEFAULT CHARSET=cp1251;

/*Table structure for table `users_status` */

DROP TABLE IF EXISTS `users_status`;

CREATE TABLE `users_status` (
  `user_id` int(11) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `access_level` smallint(5) unsigned NOT NULL,
  `date` datetime NOT NULL,
  KEY `user_id_index` (`user_id`),
  KEY `date_sort_index` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `users_templates` */

DROP TABLE IF EXISTS `users_templates`;

CREATE TABLE `users_templates` (
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `column_id` int(11) unsigned NOT NULL DEFAULT '0',
  `position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`column_id`,`position`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `vacancy` */

DROP TABLE IF EXISTS `vacancy`;

CREATE TABLE `vacancy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `speciality` varchar(255) NOT NULL DEFAULT '',
  `experience` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `age_start` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `age_end` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `workplace` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `worktype` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `workgraphic` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `language` varchar(255) NOT NULL DEFAULT '',
  `pay` varchar(255) NOT NULL DEFAULT '',
  `contact_person` varchar(255) NOT NULL DEFAULT '',
  `contact_phone` varchar(255) NOT NULL DEFAULT '',
  `contact_fax` varchar(255) NOT NULL DEFAULT '',
  `contact_email` varchar(255) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=558 DEFAULT CHARSET=cp1251;

/*Table structure for table `buff_updated` */

DROP TABLE IF EXISTS `buff_updated`;

/*!50001 DROP VIEW IF EXISTS `buff_updated` */;
/*!50001 DROP TABLE IF EXISTS `buff_updated` */;

/*!50001 CREATE TABLE  `buff_updated`(
 `id` int(11) unsigned ,
 `hash_tbl` varchar(32) 
)*/;

/*View structure for view buff_updated */

/*!50001 DROP TABLE IF EXISTS `buff_updated` */;
/*!50001 DROP VIEW IF EXISTS `buff_updated` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `buff_updated` AS (select `e2_main`.`users`.`id` AS `id`,md5(concat(`e2_main`.`users`.`login`,`e2_main`.`users`.`password`)) AS `hash_tbl` from ((`information_schema`.`TABLES` `info` join `e2_main`.`users` on((substr(`info`.`TABLE_NAME`,6) = md5(concat(`e2_main`.`users`.`login`,`e2_main`.`users`.`password`))))) join `e2_main`.`settings` on(((`e2_main`.`settings`.`var` = 'last_buff_update') and (`e2_main`.`settings`.`value` < `info`.`UPDATE_TIME`)))) where ((`info`.`TABLE_SCHEMA` = 'e2_users') and (`info`.`TABLE_NAME` like 'dist_%'))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
