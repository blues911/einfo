DELIMITER $$

DROP FUNCTION IF EXISTS `prepare_prices`$$

CREATE FUNCTION `prepare_prices`(prices TEXT, nds DECIMAL(12,4), usd_rate DECIMAL(12,4), eur_rate DECIMAL(12,4)) RETURNS text CHARSET cp1251
    DETERMINISTIC
BEGIN

    DECLARE i INT DEFAULT 0;
    DECLARE p DECIMAL(12,4) DEFAULT 0.0000;
    DECLARE result TEXT;

    SET result = prices;

    WHILE i < JSON_COUNT(result) DO

        SET p = JSON_EXTRACT(result, CONCAT('$[',i,'].p'));
        SET result = JSON_REPLACE(result, CONCAT('$[',i,'].p'), p * nds);

        IF (JSON_EXTRACT(result, CONCAT('$[',i,'].c')) = "usd") THEN
			      SET result = JSON_REPLACE(result, CONCAT('$[',i,'].c'), '"rur"');
            SET result = JSON_REPLACE(result, CONCAT('$[',i,'].p'), p * usd_rate);
        END IF;

        IF (JSON_EXTRACT(result, CONCAT('$[',i,'].c')) = "eur") THEN
			      SET result = JSON_REPLACE(result, CONCAT('$[',i,'].c'), '"rur"');
            SET result = JSON_REPLACE(result, CONCAT('$[',i,'].p'), p * eur_rate);
        END IF;

        SET i = i + 1;
 
    END WHILE;

    RETURN result;

END $$

DELIMITER ;