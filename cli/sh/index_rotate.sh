#!/bin/bash

cd /var/hosting/einfo/data/www/cli/

/usr/bin/indexer --config /etc/sphinxsearch/sphinx.conf --all --rotate
/usr/bin/mysql -h 127.0.0.1 -P 9306 -e'show index rt status'
/usr/bin/php -f cli.php truncate_rt_index
/usr/bin/mysql -h 127.0.0.1 -P 9306 -e'show index rt status'
/usr/bin/mysql -h 127.0.0.1 -P 9306 -e'show index main status'