#!/bin/bash

cd /var/hosting/einfo/data/www/cli/

/usr/bin/php -f cli.php getcurval
#/usr/bin/php -f cli.php catalog_canon_update > /dev/null

/usr/bin/find /var/hosting/einfo/data/www/tmp/adodb_cache/ -type f -exec rm -f {} \;
rm -f /var/hosting/einfo/data/www/tmp/tpl-einfo/*
rm -f /var/hosting/einfo/data/www/tmp/tpl-user/*
