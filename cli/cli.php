<?php

// подключение основных модулей
include_once('../config.php');
include_once(PATH_BACKEND . '/php/function.php');
include_once(PATH_BACKEND . '/php/adodb/adodb.inc.php');
include_once(PATH_BACKEND . "/php/classes/class.Price.php");
include_once(PATH_FRONTEND . '/php/smarty/Smarty.class.php');
include_once(PATH_BACKEND . '/php/classes/class.MyDate.php');

// подключение к БД (главная)
$DB = ADONewConnection(DB_MAIN_DSN);
$DB->SetFetchMode(ADODB_FETCH_ASSOC);
$DB->Execute('SET NAMES utf8');
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;
//$DB->debug = true;

// подключение к БД (пользовательская)
$DB2 = ADONewConnection(DB_USERS_DSN);
$DB2->SetFetchMode(ADODB_FETCH_ASSOC);
$DB2->Execute('SET NAMES utf8');
$DB2->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB2->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB2->cacheSecs = DB_CACHE_TIME;
//$DB2->debug = true;

// Настройки
$SETTINGS = array();
$setn = $DB->GetAll('SELECT * FROM settings');
foreach ($setn as $value)
{
    @list($section, $param) = explode(':', $value['var']);
    if (isset($param))
    {
        $SETTINGS[$section][$param] = $value['value'];
    }
    else
    {
        $SETTINGS[$section] = $value['value'];
    }
}

// Smarty
$smarty = new Smarty();
$smarty->template_dir = PATH_FRONTEND . '/templates';
$smarty->compile_dir = PATH_TMP . '/tpl-einfo';
$smarty->error_reporting = 6135;

// Requests
require PATH_FRONTEND . '/php/libs/Requests/library/Requests.php';
Requests::register_autoloader();

// подключение испольняемого файла
try
{
    if (!isset($_GET['x']))
    {
        $_GET['x'] = !empty($argv[1]) ? $argv[1] : '';
    }

    if (preg_match("/^([-a-z0-9_]+)$/i", $_GET['x']))
    {
        if (file_exists('php/' . $_GET['x'] . '.php'))
        {
            $timer_start = microtime(true);

            echo 'Exec "' . $_GET['x'] . '", output:' . PHP_EOL . '----' . PHP_EOL;

            include_once ('php/' . $_GET['x'] . '.php');

            $timer = microtime(true) - $timer_start;
            echo '----' . PHP_EOL . 'Total time: ' . round($timer, 6) . ' sec.' . PHP_EOL;
            echo 'Memory Peak Usage: ' . round(memory_get_peak_usage(true) / 1048576, 1) . ' MB';
        }
        else throw new Exception('Error 404', 404);
    }
    else throw new Exception('Error 404', 404);
}
catch (Exception $e)
{
    if ($e->getCode() == 404)
    {
        exit('Exec "' . $_GET['x'] . '" not found' . PHP_EOL);
    }
}

echo PHP_EOL;
