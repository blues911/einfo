<?php

include_once(PATH_BACKEND . '/php/classes/class.PdfUpdate.php');
include_once(PATH_BACKEND . '/php/classes/class.PdfParse.php');

echo('Start datasheet render...' . PHP_EOL);

$pdf_update = new PdfUpdate($DB);
$pdf_parse = new PdfParse($DB);

$datasheet_list = $pdf_parse->get_list();

foreach($datasheet_list as $datasheet)
{
    echo($datasheet['comp_canon'] . ' (' . $datasheet['local'] . ') -> ' );

    // Путь к файлу pdf
    $file_path = $pdf_update->pdf_path . $pdf_update->get_dir_name($datasheet['local']) . '/' . $datasheet['local'];

    $new_image_name = $pdf_parse->generate_file_name('imagename.jpg');

    $render = $pdf_parse->pdf_render_page($file_path, $new_image_name);

    if ($render)
    {
        echo ('OK' . PHP_EOL);
        $DB->Execute('UPDATE datasheet SET img = ? WHERE comp_canon = ?', array($new_image_name, $datasheet['comp_canon']));

        $pdf_parse->create_miniature($new_image_name);
    }
    else
    {
        echo ('FAIL' . PHP_EOL);
    }
}