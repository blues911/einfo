<?php

require(PATH_FRONTEND . '/php/libs/Requests/library/Requests.php');
Requests::register_autoloader();

$headers = array('Content-Type' => 'application/json');

echo('Set webhook: ' . TELEGRAM_HOOK_URL . PHP_EOL);

$response = Requests::post(TELEGRAM_API_URL . 'setWebhook', $headers, json_encode(array(
    'url' => TELEGRAM_HOOK_URL
)));

echo($response->body . PHP_EOL);