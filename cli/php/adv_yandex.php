<?php

define('ADV_YANDEX_REQUEST_CHUNK', 100); // Количество баннеров в одном запросе.
define('ADV_YANDEX_MIN_UNITS', 5000); // Минимальное кол-во баллов для работы Яндекс.Директ.

include_once(PATH_BACKEND . '/php/classes/class.AdvYandex.php');

$DB->Execute('SET NAMES utf8');
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;

echo 'Start sync with Yandex.Direct' . PHP_EOL;

// Инициализация
$yandex = new AdvYandex($DB);

try
{
    // Синхронизация таблиц
    $yandex->adv_yandex_sync();

    // Проверка токена
    $set_token = $yandex->get_token();

    if (!empty($set_token))
    {
        // Получение настроек
        $yandex->get_settings();

        // Проверка количества баллов
        $units = $yandex->get_units();

        if ($units <= ADV_YANDEX_MIN_UNITS)
        {
            echo('The number of remaining points - ' . (int)$units . PHP_EOL);
            echo('Script stopped.' . PHP_EOL);
        }
        else
        {
            echo('Processing...' . PHP_EOL);

            // Отправка на модерацию объявлений со статусом DRAFT.
            $yandex->moderate_banners();

            // Запуск изменения цены показа
            $yandex->update_price();

            // Запуск остановленных объявлений
            $yandex->resume_banners();

            // Остановка запущенных объявлений
            $yandex->stop_banners();

            // Изменение заголовка и описания объявлений
            $yandex->update_banners();

            // ----- Добавление новых объявлений -----

            // Список объявлений для добавления
            $new_banners_all = $yandex->new_banners_get_list();

            if (!empty($new_banners_all) && $yandex->get_units() > ADV_YANDEX_MIN_UNITS)
            {
                echo 'New banners -> ' . count($new_banners_all) . PHP_EOL;

                $new_banners_all = array_chunk($new_banners_all, ADV_YANDEX_REQUEST_CHUNK, true);

                $_counter = 0;
                foreach ($new_banners_all as $new_banners)
                {
                    $_counter += count($new_banners);

                    // Список компаний и количества объявлений в них
                    $campaign_list = $yandex->get_campaigns_info();

                    if (!empty($campaign_list))
                    {
                        // Добавление объявлений к существующим компаниям
                        $result = $yandex->create_new_banners($campaign_list, $new_banners);

                        // Список оставшися не добаленных баннеров
                        $remaining_banners = $result['banners'];
                    }
                    else
                    {
                        $remaining_banners = $new_banners;
                    }

                    // Если остались не добавленные объявления создаем новую компанию и добавляем их
                    if (count($remaining_banners) > 0)
                    {
                        // Подсчет количества необходимых новых компаний
                        $campaigns_need = ceil(count($remaining_banners) / 1000);

                        // Создание необходимых новых компаний
                        $campaign_list = $yandex->create_new_campaigns($campaigns_need);

                        // Добавление объявлений к новым компаниям
                        $result = $yandex->create_new_banners($campaign_list, $remaining_banners);

                        $campaigns_update = $result['campaigns_update'];
                    }

                    if ($yandex->get_units() <= ADV_YANDEX_MIN_UNITS)
                    {
                        break;
                    }
                }
            }

            // Отправка на модерацию объявлений со статусом DRAFT.
            $yandex->moderate_banners();

            // Смена стастусов на дефолтные
            $yandex->normalize_table();
        }
    }
}
catch(Exception $e)
{
    echo(PHP_EOL . 'Error: ' . $e->getMessage() . PHP_EOL . PHP_EOL);
}

echo('Results:' . PHP_EOL);
echo('Campaigns - ' . $yandex->statistic['campaign']['add'] . ' created;' . PHP_EOL);
echo('Banners - ' . $yandex->statistic['banner']['add'] . ' created, ' . $yandex->statistic['banner']['update'] . ' updated, '
    . $yandex->statistic['banner']['update_price'] . ' updated prices, ' . $yandex->statistic['banner']['resume'] . ' resumed, '
    . $yandex->statistic['banner']['stop'] . ' stopped;' . PHP_EOL);

echo('Units - ' . $yandex->get_units() . ';' . PHP_EOL);
