<?php

/**
 * Скрипт проверки поисковой статистики.
 * Отделяет запросы поисковых машин от запросов пользователей.
 */

// Ограничение на выборку из базы
$packet_size = 100000000;

// Набор шаблонов для проверки ботов
$se_pattern = array(
    '.+yandex\.(ru|net|com)$',
    '.+google(bot)?\.com$',
    '.+msn\.com$',
    '.+mail\.ru$',
);

// Начало проверки
echo 'Search start [' . date('Y-m-d H:i:s') . ']' . PHP_EOL;

while (true)
{
    // Получаем выборку непроверенных IP из базы
    $ip_list = $DB->GetCol('SELECT 
                                    ip 
                                FROM
                                    search_stat 
                                WHERE ip IS NOT NULL 
                                    AND ip != "" 
                                    AND request_from_user IS NULL
                                GROUP BY ip LIMIT ' . $packet_size);

    if (empty($ip_list)) break;

    foreach ($ip_list as $ip)
    {
        // По умолчанию считаем что запрос не от бота
        $request_from_user = 1;

        // Получаем имя хоста
        $hostname = gethostbyaddr($ip);

        // Если результат не равен запросу
        if ($hostname != $ip)
        {
            // Проверяем совпадение с основными ботами
            if (preg_match('/(' . implode(')|(', $se_pattern) . ')/i', $hostname))
            {
                // Указываем что это бот
                $check_result = 'Search bot';
                $request_from_user = 0;
            }
            // Указываем что это домен
            else
            {
                $check_result = 'Domain';
            }
        }
        // Указываем что это не домен
        else
        {
            $check_result = 'Not domain';
        }

        echo $ip . '. ' . $check_result . ' - [ ' . $hostname . ' ]' . PHP_EOL;

        // Обновляем запись с учётом проверки
        $DB->Execute('UPDATE search_stat SET request_from_user = ?, ip_host = ? WHERE ip = ?', array($request_from_user, $hostname, $ip));
    }

    echo '---- End of cycle' . PHP_EOL;
}

// Завершение проверки
echo 'Search finish [' . date('Y-m-d H:i:s') . ']' . PHP_EOL;



