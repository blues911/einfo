<?php

// Миграция файлов модуля "Новости" в базу данных

echo('NEWS_IMAGES:' . PHP_EOL);

$list = $DB->GetAll('SELECT id, img FROM news_images WHERE img_blob IS NULL');

$cnt = array('ok' => 0, 'dberror' => 0, 'nofile' => 0);
foreach ($list as $file)
{
    echo($file['id'] . '. ' . $file['img'] . ' --> ');

    if ($blob = @file_get_contents(NEWSIMG_PATH . '/' . $file['img']))
    {
        $result = $DB->Execute('UPDATE news_images SET img_blob = ? WHERE id = ?', array($blob, $file['id']));

        if (!empty($result))
        {
            unlink(NEWSIMG_PATH . '/' . $file['img']);
            $cnt['ok']++;
            echo('OK');
        }
        else
        {
            $cnt['dberror']++;
            echo('DB ERROR');
        }
    }
    else
    {
        $cnt['nofile']++;
        echo('NO FILE');
    }

    echo(PHP_EOL);
}

echo('---------------------------------------------' . PHP_EOL);
echo('OK: ' . $cnt['ok'] . '; DB ERROR: ' . $cnt['dberror'] . '; NO FILE: ' . $cnt['nofile'] . PHP_EOL);

echo(PHP_EOL . 'NEWS:' . PHP_EOL);

$list = $DB->GetAll('SELECT id, img FROM news WHERE img_blob IS NULL AND LENGTH(img) > 0');

$cnt = array('ok' => 0, 'dberror' => 0, 'nofile' => 0);
foreach ($list as $file)
{
    echo($file['id'] . '. ' . $file['img'] . ' --> ');

    if ($blob = @file_get_contents(NEWSIMG_PATH . '/' . $file['img']))
    {
        $result = $DB->Execute('UPDATE news SET img_blob = ? WHERE id = ?', array($blob, $file['id']));

        if (!empty($result))
        {
            unlink(NEWSIMG_PATH . '/' . $file['img']);
            $cnt['ok']++;
            echo('OK');
        }
        else
        {
            $cnt['dberror']++;
            echo('DB ERROR');
        }
    }
    else
    {
        $cnt['nofile']++;
        echo('NO FILE');
    }

    echo(PHP_EOL);
}

echo('---------------------------------------------' . PHP_EOL);
echo('OK: ' . $cnt['ok'] . '; DB ERROR: ' . $cnt['dberror'] . '; NO FILE: ' . $cnt['nofile'] . PHP_EOL);

// Миграция файлов модуля "Пользователи" в базу данных

echo(PHP_EOL . 'USERS_DOCS:' . PHP_EOL);

$list = $DB->GetAll('SELECT id, `file` FROM users_docs WHERE file_blob IS NULL');

$cnt = array('ok' => 0, 'dberror' => 0, 'nofile' => 0);
foreach ($list as $file)
{
    echo($file['id'] . '. ' . $file['file'] . ' --> ');

    if ($blob = @file_get_contents(USERDOCS_PATH . '/' . $file['file']))
    {
        $blob_small = @file_get_contents(USERDOCS_SMALL_PATH . '/' . $file['file']);

        if (empty($blob_small))
        {
            $blob_small = null;
        }

        $result = $DB->Execute('UPDATE users_docs SET file_blob = ?, file_small_blob = ? WHERE id = ?', array($blob, $blob_small, $file['id']));

        if (!empty($result))
        {
            unlink(USERDOCS_PATH . '/' . $file['file']);

            if (!empty($blob_small))
            {
                unlink(USERDOCS_SMALL_PATH . '/' . $file['file']);
            }

            $cnt['ok']++;
            echo('OK');
        }
        else
        {
            $cnt['dberror']++;
            echo('DB ERROR');
        }
    }
    else
    {
        $cnt['nofile']++;
        echo('NO FILE');
    }

    echo(PHP_EOL);
}

echo('---------------------------------------------' . PHP_EOL);
echo('OK: ' . $cnt['ok'] . '; DB ERROR: ' . $cnt['dberror'] . '; NO FILE: ' . $cnt['nofile'] . PHP_EOL);

// Миграция баннеров в базу данных

echo(PHP_EOL . 'BANNERS:' . PHP_EOL);

$list = $DB->GetAll('SELECT id, banner FROM users_banners WHERE banner_blob IS NULL');

$cnt = array('ok' => 0, 'dberror' => 0, 'nofile' => 0);
foreach ($list as $file)
{
    echo($file['id'] . '. ' . $file['banner'] . ' --> ');

    if ($blob = @file_get_contents(BANNERIMG_PATH . '/' . $file['banner']))
    {
        $result = $DB->Execute('UPDATE users_banners SET banner_blob = ? WHERE id = ?', array($blob, $file['id']));

        if (!empty($result))
        {
            unlink(BANNERIMG_PATH . '/' . $file['banner']);

            $cnt['ok']++;
            echo('OK');
        }
        else
        {
            $cnt['dberror']++;
            echo('DB ERROR');
        }
    }
    else
    {
        $cnt['nofile']++;
        echo('NO FILE');
    }

    echo(PHP_EOL);
}

echo('---------------------------------------------' . PHP_EOL);
echo('OK: ' . $cnt['ok'] . '; DB ERROR: ' . $cnt['dberror'] . '; NO FILE: ' . $cnt['nofile'] . PHP_EOL);