<?php

/*
 * Скрипт для создания снапшота рекламных компаний на яндексе
 *
 * Использование: php cli.php _adv_yandex_snapshot
 */

define('ADV_YANDEX_REQUEST_CHUNK', 100); // Количество баннеров в одном запросе.
define('ADV_YANDEX_MIN_UNITS', 10000); // Минимальное кол-во баллов для работы Яндекс.Директ.

include_once(PATH_BACKEND . '/php/classes/class.AdvYandex.php');

$DB->Execute('SET NAMES utf8');
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;

// Инициализация
$yandex = new AdvYandex($DB);
$yandex->get_settings();

$units = $yandex->get_units();

// Функции
$get_campaigns = function() use ($yandex)
{
    $campaigns = array();

    $limit = 1000;
    $offset = null;

    while (true)
    {
        $page = array();
        $page['Limit'] = $limit;

        if (isset($offset))
        {
            $page['Offset'] = $offset;
        }

        $r = $yandex->send_request('campaigns', 'get', array(
            'SelectionCriteria' => array(
                'Types' => array('TEXT_CAMPAIGN'),
                'States' => array('ON', 'OFF', 'SUSPENDED')
            ),
            'FieldNames' => array('Id', 'Name'),
            'Page' => $page
        ));

        if (isset($r['result']))
        {
            if (isset($r['result']['Campaigns']) && is_array($r['result']['Campaigns']))
            {
                foreach ($r['result']['Campaigns'] as $campaign)
                {
                    if (preg_match('/^' . $yandex->get_settings('campaign_prefix') . '/u', $campaign['Name']))
                    {
                        $campaigns[$campaign['Id']] = $campaign;
                    }
                }
            }

            if (isset($r['result']['LimitedBy']))
            {
                $offset += $limit;
            }
            else
            {
                break;
            }
        }
        else
        {
            echo 'Ошибка запроса компаний. ' . (isset($r['error']['error_detail']) ? $r['error']['error_detail'] : '') . PHP_EOL;
            break;
        }
    }

    return $campaigns;
};

$get_ads = function($campaign_id) use ($yandex)
{
    $ads = array();

    $r = $yandex->send_request('ads', 'get', array(
        'SelectionCriteria' => array(
            'CampaignIds' => array($campaign_id)
        ),
        'FieldNames' => array('Id', 'CampaignId', 'AdGroupId', 'State'),
        'TextAdFieldNames' => array('Title', 'Text', 'Href', 'SitelinkSetId')
    ));

    if (isset($r['result']))
    {
        if (isset($r['result']['Ads']) && is_array($r['result']['Ads']))
        {
            foreach ($r['result']['Ads'] as $ad)
            {
                $ads[$ad['Id']] = $ad;
            }
        }
    }
    else
    {
        echo 'Ошибка запроса баннеров. ' . (isset($r['error']['error_detail']) ? $r['error']['error_detail'] : '') . PHP_EOL;
    }

    return $ads;
};

$get_sitelink_sets = function(array $sitelink_set_ids) use ($yandex)
{
    $sitelink_sets = array();
    $sitelink_set_ids_chunks = array_chunk($sitelink_set_ids, 10000);

    foreach ($sitelink_set_ids_chunks as $sitelink_set_ids_chunk)
    {
        $r = $yandex->send_request('sitelinks', 'get', array(
            'SelectionCriteria' => array(
                'Ids' => $sitelink_set_ids_chunk
            ),
            'FieldNames' => array('Id', 'Sitelinks')
        ));

        if (isset($r['result']))
        {
            if (isset($r['result']['SitelinksSets']) && is_array($r['result']['SitelinksSets']))
            {
                foreach ($r['result']['SitelinksSets'] as $sitelink_set)
                {
                    $sitelink_sets[$sitelink_set['Id']] = $sitelink_set;
                }
            }
        }
        else
        {
            echo 'Ошибка запроса SitelinkSets. ' . (isset($r['error']['error_detail']) ? $r['error']['error_detail'] : '') . PHP_EOL;
            break;
        }
    }

    return $sitelink_sets;
};

$get_keywords = function($campaign_id) use ($yandex)
{
    $keywords = array();

    $r = $yandex->send_request('keywords', 'get', array(
        'SelectionCriteria' => array(
            'CampaignIds' => array($campaign_id)
        ),
        'FieldNames' => array('Id', 'AdGroupId', 'Keyword', 'Bid')
    ));

    if (isset($r['result']))
    {
        if (isset($r['result']['Keywords']) && is_array($r['result']['Keywords']))
        {
            foreach ($r['result']['Keywords'] as $keyword)
            {
                $keywords[(string)$keyword['AdGroupId']] = $keyword;
            }
        }
    }
    else
    {
        echo 'Ошибка запроса ключевых слов. ' . (isset($r['error']['error_detail']) ? $r['error']['error_detail'] : '') . PHP_EOL;
    }

    return $keywords;
};

// Запрос
echo 'Units: ' . $units . PHP_EOL;

// Если баллов не хватает на полную выгрузку одной компании завершаем скрипт
if ($units < ADV_YANDEX_MIN_UNITS)
{
    echo('The number of remaining points - ' . (int)$units . PHP_EOL);
    echo('Script stopped.' . PHP_EOL);
    goto end;
}

try
{
    // Получаем список компаний
    $campaigns = $get_campaigns();
    $campaign_ids = array_map(function($v) { return $v['Id']; }, $campaigns);
    echo('Campaigns: ' . count($campaigns) . PHP_EOL);

    // Получаем ID компаний уже записанных в таблицу
    $exist_campaign_ids = $DB->GetCol('SELECT DISTINCT(campaign_id) AS campaign_id FROM adv_yandex_snapshot');

    // Получаем несуществующие в таблице ID компаний
    $diff_campaign_ids = array_diff($campaign_ids, $exist_campaign_ids);
    echo('New campaigns: ' . count($diff_campaign_ids) . PHP_EOL);

    // Массив с данными по компании
    $data = array();

    // Получаем данные одной компании
    foreach ($diff_campaign_ids as $campaign_id)
    {
        $units = $yandex->get_units();

        // Если баллов не хватает на получение данных одной компании завершаем запросы
        if ($units < ADV_YANDEX_MIN_UNITS)
        {
            break;
        }

        // Получаем список баннеров
        $ads = $get_ads($campaign_id);
        echo('Ads: ' . count($ads) . PHP_EOL);

        // Формируем список уникальных Id SitelinkSet
        $sitelink_set_ids = array();
        foreach ($ads as $ad)
        {
            if (!empty($ad['TextAd']['SitelinkSetId']))
            {
                $sitelink_set_ids[] = (int)$ad['TextAd']['SitelinkSetId'];
            }
        }
        $sitelink_set_ids = array_unique($sitelink_set_ids, SORT_NUMERIC);

        // Получаем список SitelinkSet
        $sitelink_sets = $get_sitelink_sets($sitelink_set_ids);
        echo('SitelinkSets: ' . count($sitelink_sets) . PHP_EOL);

        // Получаем список ключевых слов
        $keywords = $get_keywords($campaign_id);
        echo('Keywords: ' . count($keywords) . PHP_EOL);

        foreach ($ads as $ad)
        {
            $keyword = isset($keywords[(string)$ad['AdGroupId']]) ? $keywords[(string)$ad['AdGroupId']] : null;
            $sitelink_set = isset($sitelink_sets[$ad['TextAd']['SitelinkSetId']]) ? $sitelink_sets[$ad['TextAd']['SitelinkSetId']] : array();

            if (empty($keyword))
            {
                continue;
            }

            $_data = array(
                'comp_canon'  => preg_replace('/(.+) - (.+)/ui', '$1', $ad['TextAd']['Text']),
                'offer_cnt'   => 0,
                'title'       => $ad['TextAd']['Title'],
                'descr'       => $ad['TextAd']['Text'],
                'url'         => $ad['TextAd']['Href'],
                'phrase'      => $keyword['Keyword'],
                'price'       => $keyword['Bid'] / 1000000,
                'show'        => ($ad['State'] == 'SUSPEND') ? 0 : 1,
                'action'      => 0,
                'banner_id'   => $ad['Id'],
                'campaign_id' => $ad['CampaignId'],
                'phrase_id'   => $keyword['Id'],
                'update_date' => date('Y-m-d H:i:s'),
                'adtitle_1'   => '',
                'adlink_1'    => '',
                'adtitle_2'   => '',
                'adlink_2'    => '',
                'adtitle_3'   => '',
                'adlink_3'    => '',
                'adtitle_4'   => '',
                'adlink_4'    => ''
            );

            if (!empty($sitelink_set['Sitelinks']))
            {
                $i = 1;
                foreach ($sitelink_set['Sitelinks'] as $sitelink)
                {
                    $_data['adtitle_' . $i] = $sitelink['Title'];
                    $_data['adlink_' . $i] = $sitelink['Href'];

                    if ($i == 4)
                    {
                        break;
                    }

                    $i++;
                }
            }

            $data[] = $_data;
        }
    }

    // Записываем результаты в базу данных
    $rows = 0;
    $err = 0;
    foreach ($data as $new_ad)
    {
        $r = $DB->Execute(
            'INSERT INTO adv_yandex_snapshot (comp_canon, offer_cnt, title, descr, url, phrase, price, `show`, `action`,
            banner_id, campaign_id, phrase_id, update_date, adtitle_1, adlink_1, adtitle_2, adlink_2, adtitle_3, adlink_3, adtitle_4, adlink_4)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            array($new_ad['comp_canon'], $new_ad['offer_cnt'], $new_ad['title'], $new_ad['descr'], $new_ad['url'], $new_ad['phrase'], $new_ad['price'], $new_ad['show'], $new_ad['action'],
                  $new_ad['banner_id'], $new_ad['campaign_id'], $new_ad['phrase_id'], $new_ad['update_date'], $new_ad['adtitle_1'], $new_ad['adlink_1'],
                  $new_ad['adtitle_2'], $new_ad['adlink_2'], $new_ad['adtitle_3'], $new_ad['adlink_3'], $new_ad['adtitle_4'], $new_ad['adlink_4'])
        );

        if ($r === false)
        {
            $err++;
        }
        else
        {
            $rows++;
        }
    }

    echo('Table Rows: ' . $rows . ' success, ' . $err . ' fail' . PHP_EOL);
}
catch (Exception $e)
{
    echo('Error: ' . $e->getMessage() . PHP_EOL);
}

echo('Units: ' . $yandex->get_units() . PHP_EOL);

end: