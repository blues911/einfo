<?php

/*
 * Скрипт для импорта списка компонентов из текстового файла в
 * таблицу catalog_comp_canon базы данных.
 *
 * В текстовом файле каждое имя компонента с новой строчки.
 *
 * Использование: php cli.php _comp_canon_import <файл для импорта>
 */

// Id категории, к которой привязывать импортированные компоненты.
$export_category_id = 160062;

// Максимальный уровень вложености категорий
$max_nesting_level = 2;

// Минимальное количество предложений
$min_offer_cnt = 7;

// ------------------------------------------------------

// инициализация dbtree
include_once (PATH_BACKEND . "/php/classes/dbtree/class.dbtree.php");

$tree = new CDBTree('categories', 'c', $DB);

// ------------------------------------------------------

function special_translit($string,  $to_url = false, $flip = false)
{
    $translit = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'j',
        'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
        'х' => 'x', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ь' => '\'', 'ы' => 'y', 'ъ' => '\'\'', 'э' => 'e\'', 'ю' => 'yu', 'я' => 'ya',
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'J',
        'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'X',
        'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ь' => '\'', 'Ы' => 'Y\'', 'Ъ' => '\'\'', 'Э' => 'E\'', 'Ю' => 'YU', 'Я' => 'YA',
    );

    if ($to_url)
    {
        $translit['ь'] = $translit['ъ'] = $translit['Ь'] = $translit['Ъ'] = $translit['«'] = $translit['»'] = $translit['\''] = $translit['"'] = '';
        $translit['э'] = $translit['Э'] = 'e';
    }

    if (!$flip)
    {
        $result = strtr($string, $translit);
    }
    else
    {
        $result = strtr($string, array_flip($translit));
    }

    $result = str_replace(' ', '_', trim($result));

    return $to_url ? strtolower($result) : $result;
}

function delete_empty_category($total = 1)
{
    global $DB, $tree;

    // Если пустых рубрик больше нет, заканчиваем
    if (empty($total)) return;

    // Получаем количество предложений по этим рубрикам
    $item_cnt = $DB->GetOne('SELECT COUNT(*) FROM catalog_comp_canon WHERE category_id IN (SELECT id FROM categories WHERE c_right - c_left = 1 AND cnt = 0)');
    echo 'Items count to delete ' . $item_cnt . PHP_EOL;

    // Удаляем предложения
    $DB->Execute('DELETE FROM catalog_comp_canon WHERE category_id IN (SELECT id FROM categories WHERE c_right - c_left = 1 AND cnt = 0)');

    // Получаем пустые рубрики
    $category_list = $DB->GetCol('SELECT id FROM categories WHERE c_right - c_left = 1 AND cnt = 0');

    $i = 0;
    // Удаляем рубрики из дерева
    foreach ($category_list as $category_id)
    {
        if ($tree->Delete($category_id))
        {
            $i++;
        }
    }
    echo 'Deleted ' . $i . ' from ' . count($category_list) . ' categories' . PHP_EOL;

    // Получаем количество пустых рубрик после чистки
    $count = $DB->GetOne('SELECT COUNT(*) FROM categories WHERE c_right - c_left = 1 AND cnt = 0');

    // Делаем запрос повторно
    delete_empty_category($count);
}

// Сортировка всех подрубрик в указанной рубрике в алфавитном порядке
function sort_category($category_id)
{
    global $DB, $tree;

    // Получаем ID первой подрубрики
    $first_id = $DB->GetOne(
        'SELECT child.id
         FROM categories AS child
           LEFT JOIN categories AS parent ON parent.c_left < child.c_left AND parent.c_right > child.c_right AND parent.c_level = child.c_level - 1
         WHERE parent.id = ?
         ORDER BY child.c_left ASC
         LIMIT 1',
         array($category_id)
    );

    // Если подрубрик нет - завершаем работу
    if (empty($first_id)) return;

    // Получаем ID подрубрик отсортрованных по алфавиту
    $sort_list = $DB->GetCol(
        'SELECT child.id
         FROM categories AS child
           LEFT JOIN categories AS parent ON parent.c_left < child.c_left AND parent.c_right > child.c_right AND parent.c_level = child.c_level - 1
         WHERE parent.id = ?
         ORDER BY child.title ASC',
        array($category_id)
    );

    foreach ($sort_list as $rubric_id)
    {
        // В случае успешного переноса запускаем сортировку для подрубрик
        if ($tree->MoveAllAfter($rubric_id, $first_id))
        {
            sort_category($rubric_id);
        }

        $first_id = $rubric_id;
    }

    unset($sort_list);
}

// ------------------------------------------------------

require_once(PATH_FRONTEND . '/php/libs/sphinxapi.php');

if ($argc < 3)
{
    echo('Необходимо указать файл для импорта.' . PHP_EOL);
    goto end;
}

if (!is_file($argv[2]))
{
    echo('Файл ' . $argv[2] . ' не найден.' . PHP_EOL);
    goto end;
}

// Соединение со Sphinx Engine
$sphinx = new SphinxClient();
$sphinx->SetServer(SPHINX_HOST, SPHINX_PORT);
$sphinx->SetMatchMode(SPH_MATCH_EXTENDED);
$sphinx->SetLimits(0, 3000, 3000, 3000);

if ($sphinx->Status() == false)
{
    echo('Не удается подключиться к Sphinx.' . PHP_EOL);
    goto end;
}

// Проверка существования заданной категории
if (!$DB->GetOne('SELECT SQL_NO_CACHE id FROM categories WHERE id = ? LIMIT 0, 1', array($export_category_id)))
{
    echo('Указан ID не существующей категории.' . PHP_EOL);
    goto end;
}

$usd = $SETTINGS['cur_rates']['usd'];
$eur = $SETTINGS['cur_rates']['eur'];

// Открываем файл
$file = fopen($argv[2], 'r');

// Получаем по одной строке
while ($row = fgets($file, 512))
{
    $word = trim($row);

    echo($word . '...');

    // Проверяем вхождение слова в базу данных (реализовано по аналогии с catalog_canon_update.php)
    $word_exist = $DB->GetOne('SELECT SQL_NO_CACHE id FROM base_main WHERE comp_title LIKE ? LIMIT 0, 1', array($word));

    if (empty($word_exist))
    {
        echo('NOT EXIST' . PHP_EOL);
        continue;
    }

    // Атомарный поиск компонентов через Sphinx
    $sphinx_query = '"^' . implode(' ', get_request_atoms($word)) . '"';
    $sphinx_result = $sphinx->Query($sphinx_query, SPHINX_INDEX);

    if (empty($sphinx_result['matches']))
    {
        echo('NOT EXIST' . PHP_EOL);
        continue;
    }

    $comp_ids = array_keys($sphinx_result['matches']);

    $atoms = get_request_atoms($word);
    $atoms = !empty($atoms) ? implode(' ', $atoms) . ' ' : '';

    // Выбираем из БД компоненты найденные Sphinx
    $list_rs = $DB->Execute(
        'SELECT SQL_NO_CACHE
          main.id AS id,
          main.prices AS prices
        FROM
          base_main AS main
          INNER JOIN users ON main.user_id = users.id
        WHERE
          users.status = 1
          AND main.id IN (' . implode(',', $comp_ids) . ')
          AND main.atoms LIKE ?',
        array($atoms . '%')
    );

    $list = array('id' => array(), 'price' => array());

    while(!$list_rs->EOF)
    {
        $list['id'][] = $list_rs->fields('id');

        $prices = json_decode($list_rs->fields('prices'), true);

        if (!empty($prices))
        {
            $price = array_shift($prices);

            if ($price['c'] == 'rur')
            {
                $list['price'][] = $price['p'];
            }
            else if ($price['c'] == 'usd')
            {
                $list['price'][] = $price['p'] * $usd;
            }
            else
            {
                $list['price'][] = $price['p'] * $eur;
            }
        }

        $list_rs->MoveNext();
    }

    // Считаем количество предложений и медианны цен
    $canon = array(
        'offer_cnt' => count($list['id']),
        'price_median' => median($list['price'])
    );

    if (empty($canon['offer_cnt']))
    {
        echo('NOT OFFERS' . PHP_EOL);
        continue;
    }

    if ($canon['offer_cnt'] < $min_offer_cnt)
    {
        echo('OFFERS LESS NECESSARY' . PHP_EOL);
        continue;
    }

    // Устанавливаем ID категории компонента
    $parent_cat_id = $export_category_id;
    $title = '';

    // Получаем атомы из названия компонента
    $categories = get_request_atoms($word);

    // Проходимся по атомам компонента
    foreach ($categories as $k => $category)
    {
        // Пока позиция атома меньше максимальной глубины вложенности
        // делаем поиск категории в БД
        if ($k < $max_nesting_level)
        {
            $title .= $category;

            // Проверяем существование категории в БД
            $cat = $DB->GetRow('
                              SELECT child.*
                              FROM categories AS parent
                                 LEFT JOIN categories AS child ON (child.c_left > parent.c_left
                                                                  AND child.c_right < parent.c_right
                                                                  AND child.c_level = parent.c_level + 1)
                              WHERE parent.id = ? AND child.title = ?',
                              array((int)$parent_cat_id, $title)
            );

            // Если категории не существует
            if (empty($cat))
            {
                // Добавляем новую категорию и обновляем ID категории компонента
                $tree->insert((int)$parent_cat_id, '', array(
                                                              'title' => $title,
                                                              'path'  => strtolower(special_translit($category, true))
                                                          ));

                $cat = $DB->GetRow('
                              SELECT child.*
                              FROM categories AS parent
                                 LEFT JOIN categories AS child ON (child.c_left > parent.c_left
                                                                  AND child.c_right < parent.c_right
                                                                  AND child.c_level = parent.c_level + 1)
                              WHERE parent.id = ? AND child.title = ?',
                                   array((int)$parent_cat_id, $title)
                );

                $parent_cat_id = $cat['id'];
            }
            // Если категория найдена, обновляем ID категории компонента
            else
            {
                $parent_cat_id = $cat['id'];
            }
        }
        else continue;
    }

    // Записываем компонент в catalog_comp_canon
    $result = $DB->Execute(
        'INSERT IGNORE INTO
            catalog_comp_canon
         SET
            regexp_id = NULL,
            category_id = ?,
            comp_canon = ?,
            offer_cnt = ?,
            price_median = ?',
        array(
            $parent_cat_id,
            mb_strtoupper($word),
            $canon['offer_cnt'],
            $canon['price_median']
        )
    );

    if ($result)
    {
        echo('OK (' . $canon['offer_cnt'] . ')' . PHP_EOL);
    }
    else
    {
        echo('INSERT ERROR' . PHP_EOL);
    }
}

// Закрываем файл
fclose($file);


// обновление полных путей для категорий
$DB->Execute("UPDATE categories,
                              (SELECT node.id,
                                  GROUP_CONCAT(parent.path ORDER BY parent.c_left ASC SEPARATOR '/') as full_path,
                                  GROUP_CONCAT(parent.title ORDER BY parent.c_left ASC SEPARATOR ' / ') as full_title
                              FROM categories node, categories parent
                              WHERE node.c_left BETWEEN parent.c_left AND parent.c_right AND parent.c_level<>0
                              GROUP BY node.id
                              ORDER BY node.c_left) as list
            SET categories.full_path = list.full_path, categories.full_title = list.full_title
            WHERE categories.id = list.id");

// Пересчет кол-ва компонентов по категориям
$categories = $DB->GetAll('
        SELECT
          cat.id,
          cat.cnt AS cnt_old,
          COUNT(canon.id) AS cnt
        FROM
          categories AS cat
          LEFT OUTER JOIN catalog_comp_canon AS canon ON cat.id = canon.category_id AND canon.offer_cnt >= ?
        GROUP BY cat.id
    ', array(CATALOG_MIN_SUPPLY));

foreach ($categories as $category)
{
    if ($category['cnt_old'] != $category['cnt'])
    {
        $DB->Execute('UPDATE categories SET cnt = ? WHERE id = ?', array($category['cnt'], $category['id']));
    }
}

// Делаем очистку от пустых рубрик
delete_empty_category();

// Выставляем рубрики в алфавитном порядке
sort_category($export_category_id);

//$DB->Execute('DELETE FROM catalog_comp_canon WHERE regexp_id IS NULL AND comp_canon IN (SELECT comp_title FROM base_main WHERE user_id = 1)');
//$DB->Execute('DELETE FROM catalog_comp_canon WHERE regexp_id IS NULL AND comp_canon IN (select DISTINCT(comp_title) from base_main where manuf_title like 'meanwell' or manuf_title like 'mean well' or manuf_title like 'mw')');

end:
