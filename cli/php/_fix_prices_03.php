<?php

// Правки формата цен e2_main.catalog_comp_canon, перенос price_roz_median в price_median.

echo "Правки " . DB_MAIN_NAME . ".catalog_comp_canon \n";

$rs = $DB->Execute('SELECT id, price_roz_median FROM catalog_comp_canon');

while(!$rs->EOF)
{
    $id = $rs->fields('id');
    $price_median = $rs->fields('price_roz_median');

    $DB->Execute('UPDATE catalog_comp_canon SET price_median = ? WHERE id = ?', array($price_median, $id));

    $rs->MoveNext();
}