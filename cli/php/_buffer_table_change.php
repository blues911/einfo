<?php

// --------------------------------------------------------------
// Массовое изменение буферных таблиц
// --------------------------------------------------------------


$tables = $DB2->GetCol("SELECT info.TABLE_NAME
                             FROM information_schema.TABLES AS info
                             WHERE info.TABLE_SCHEMA='e2_users' AND info.TABLE_NAME LIKE 'dist_%'");

if ($tables)
{
    foreach ($tables as $table)
    {
        // Добавляем поле для удалённых складов
        echo 'Table ' . $table . ' altered ... ';
        $DB2->Execute("ALTER TABLE " . $table . " ADD COLUMN `stock_remote` INT(11) DEFAULT 0  NOT NULL AFTER `stock`");
        echo 'OK' . PHP_EOL;
    }
}

