<?php

// -------------------------------------------
// Конвертирование текстовых прайсов в XML
// -------------------------------------------


include_once(PATH_BACKEND . '/php/classes/class.Price.php');

// константы
define("UPLOAD_DIR", PATH_PRICES . '/2_csv/');

// ---------------------------------------------------------------

// заполнение массива тэгов
$price_columns = $DB->GetAll("SELECT * FROM price_columns");
foreach ($price_columns as $value)
{
    $g_tags[$value['name']] = array("tag" => $value['tag'], "func" => $value['func']);
}

// шаблон XML файла
$g_xml = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<price>
	<header>
		<status>#status#</status>
		<uploadtype>#uploadtype#</uploadtype>
	</header>

	<data>
';

$g_xml_end = '
	</data>
</price>';

// соответствие тега цены и тега валюты
$g_cur_arr = array("roz" => "roz_cur");

//-----------------------------------

// анализ файлов для парсинга
$rs = $DB->Execute("SELECT id, login, password FROM users");

if ($dir_handle = opendir(UPLOAD_DIR))
{
    $i = 0;
    while (false !== ($file = readdir($dir_handle)))
    {
        if ($file != "." && $file != "..")
        {
            $file_explode = explode('.', $file);
            $target_file_name = $file_explode[0] . '.xml';

            $pinfo = pathinfo($file);

            if ($pinfo['extension'] == "xml")
            {
                copy(UPLOAD_DIR.$file, PATH_PRICES . '/3_xml/' . $target_file_name);
                unlink(UPLOAD_DIR.$file);
            }
            else
            {
                $fname = explode('.', $file);

                $todel = 1;
                while ($row = $rs->FetchRow())
                {
                    if ($fname[0] == $row['id'])
                    {
                        $file_list[$i]['id'] = $row['id'];
                        $file_list[$i]['file_name'] = $file;
                        $file_list[$i]['file_path'] = UPLOAD_DIR.$file;
                        $cnt_fname = count($fname);
                        for ($j = 1; $j < $cnt_fname; $j++)
                        {
                            if (in_array($fname[$j], array("add", "update", "replace")))
                            {
                                $file_list[$i]['uploadtype'] = $fname[$j];
                            }

                            if (in_array($fname[$j], array("rur", "usd", "eur")))
                            {
                                $file_list[$i]['currency'] = $fname[$j];
                            }

                            if (in_array($fname[$j], array("stock", "stock_hide")))
                            {
                                $file_list[$i]['stock'] = $fname[$j];
                            }

                            if (in_array($fname[$j], array("stock_remote")))
                            {
                                $file_list[$i]['stock_remote'] = $fname[$j];
                            }

                            if (preg_match("/^delivery(_(\d+))?$/u", $fname[$j], $matches))
                            {
                                $file_list[$i]['delivery'] = isset($matches[2]) ? $matches[2] : -1;
                            }
                        }
                        $i++;
                        $todel = 0;
                        break;
                    }
                }
                $rs->MoveFirst();
                if ($todel)
                {
                    unlink(UPLOAD_DIR.$file);
                }

            }
        }
    }
    closedir($dir_handle);

    // парсинг файлов
    if (!empty($file_list))
    {
        $cnt_file_list = count($file_list);
        for ($i = 0; $i < $cnt_file_list; $i++)
        {
            $result = process_file($DB, $file_list[$i], $debug);

            if ($result['error'])
            {
                print "Error (".$file_list[$i]['file_name']."): ".$result['errdesc']."\n";
            }
            else
            {
                print "Processed OK (".$file_list[$i]['file_name']."): (find - ".$result['lines_all'].", fail - ".$result['lines_fail'].")\n";
            }
            unlink(UPLOAD_DIR.$file_list[$i]['file_name']);

            // debug
            $DB->Execute('INSERT INTO cli_log SET user_id = ?, `timestamp` = NOW(), script = ?, ok = ?, fail = ?', array($file_list[$i]['id'], 'csv2xml', $debug['ok'], $debug['fail']));
        }
    }
}
else
{
    echo("Невозможно открыть каталог с файлами.\n");
}


////////////////////////////////////////////////////
// **** Функции по обработке данных *****
////////////////////////////////////////////////////

// парсинг файла и сохранение в XML
function process_file(&$DB, $cmdarr, &$debug = false)
{
    GLOBAL $g_tags, $g_cur_arr, $g_xml, $g_xml_end;

    $result = array();
    $result['error'] = false;
    $result['lines_error'] = false;
    $debug = array('ok' => 0, 'fail' => 0);

    // проверка корректности атрибутов входного файла

    if (!isset($cmdarr['uploadtype']))
    {
        $result['error'] = true;
        $result['errorcode'] = 2;
        $result['errdesc'] = "Отсутствует или некорректен атрибут UPLOADTYPE";
        return $result;
    }

    if (!isset($cmdarr['currency']))
    {
        $result['error'] = true;
        $result['errorcode'] = 5;
        $result['errdesc'] = "Отсутствует или некорректен атрибут CURRENCY";
        return $result;
    }


    if (!isset($cmdarr['stock']) && !isset($cmdarr['delivery']))
    {
        $result['error'] = true;
        $result['errorcode'] = 6;
        $result['errdesc'] = "Отсутствует или некорректен атрибут STOCK или DELIVERY";
        return $result;
    }


    // загрузка прайс-листа
    $price_obj = new Price($cmdarr['file_path'], 'utf-8');
    if ($price_obj->can_read())
    {
        // разбор тегов
        $allow_tags = array();
        $tag_arr = $price_obj->get_first_row();
        array_walk($tag_arr, "pars_element");
        foreach ($g_tags as $key => $value)
        {
            $allow_tags[] = $g_tags[$key]['tag'];
        }

        // массив $tag_arr содержит только допустимые теги, найденные в файле
        // key = номер столбца, value = тег
        $tag_arr = array_intersect($tag_arr, $allow_tags);

        if (!in_array($g_tags['title']['tag'], $tag_arr))
        {
            $result['error'] = true;
            $result['errorcode'] = 3;
            $result['errdesc'] = "Неверный формат тегов (отсутствут тег - ".$g_tags['title']['tag'].")";
            return $result;
        }



        // всего строк в файле
        $lines_all = 0;
        // кол-во неверных строк (тег name пуст)
        $lines_fail = 0;

        // Создаем XML файл
        $xml_file_path = PATH_PRICES . '/3_xml/' . $cmdarr['id'] . '.xml';
        $xml_file = fopen($xml_file_path, 'w');

        // xml заголовок
        $xml = preg_replace('/#uploadtype#/su', $cmdarr['uploadtype'], $g_xml);
        if (isset($cmdarr['stock']))
        {
            $xml = preg_replace('/#status#/su', $cmdarr['stock'], $xml);
        }
        else
        {
            $xml = preg_replace('/#status#/su', 'delivery', $xml);
        }

        fwrite($xml_file, $xml);

        // основной цикл
        $price_obj->get_next_row();
        while(($buf = $price_obj->get_next_row()) !== false)
        {
            $taint_row = array();
            $row = array();
            array_walk($buf, "pars_element");
            $lines_all++;

            foreach ($tag_arr as $key => $value)
            {
                if ($value == $g_tags['title']['tag'] && $buf[$key] == "")
                {
                    $lines_fail++;
                    continue(2);
                }
                elseif ($value == 'E_DESCR' && !empty($taint_row['E_DESCR']))
                {
                    $taint_row['E_DESCR'] .= ' ' . $buf[$key];
                }
                elseif (!isset($buf[$key]))
                {
                    $taint_row[$value] = "";
                }
                else
                {
                    $taint_row[$value] = $buf[$key];
                }
            }


            // парсинг элементов
            foreach ($g_tags as $k => $tag)
            {
                if (isset($taint_row[$tag['tag']]))
                {
                    if ($tag['func'] != "" && function_exists($tag['func']))
                    {
                        $row[$tag['tag']] = call_user_func($tag['func'], $taint_row[$tag['tag']], $cmdarr);
                    }
                    else
                    {
                        $row[$tag['tag']] = $taint_row[$tag['tag']];
                    }
                }
            }


            // обработка валюты (4 уровня)
            foreach ($g_cur_arr as $price => $price_cur)
            {
                if (isset($row[$g_tags[$price]['tag']]))
                {
                    // валюта в ячейки с ценой
                    if (isset($taint_row[$g_tags[$price]['tag']]) && ($tmpcur = proc_currenсy($taint_row[$g_tags[$price]['tag']])))
                    {
                        $row[$g_tags[$price_cur]['tag']] = $tmpcur;
                    }
                    // валюта в спец. теге для данного типа цены
                    elseif (isset($row[$g_tags[$price_cur]['tag']]) && $tmpcur = proc_currenсy($row[$g_tags[$price_cur]['tag']]))
                    {
                        $row[$g_tags[$price_cur]['tag']] = $tmpcur;
                    }
                    // валюта в глобальном теге для всей строчки
                    elseif (isset($row[$g_tags['cur']['tag']]) && $tmpcur = proc_currenсy($row[$g_tags['cur']['tag']]))
                    {
                        $row[$g_tags[$price_cur]['tag']] = $tmpcur;
                    }
                    // валюта в имени файла
                    else
                    {
                        $row[$g_tags[$price_cur]['tag']] = $cmdarr['currency'];
                    }
                }
            }


            // склад / поставка на заказ
            if (isset($cmdarr['stock']))
            {
                if ($cmdarr['stock'] == "stock_hide")
                {
                    if (!empty($row['E_STOCK']) && isset($row['E_DELIVERY']) && $row['E_DELIVERY'] == 0)
                    {
                        $row['E_STOCK'] = -1;
                    }
                }
            }
            else
            {
                if (!isset($row['E_DELIVERY'])) $row['E_DELIVERY'] = $cmdarr['delivery'];
            }

            if (isset($row['E_STOCK']) && $row['E_STOCK'] == 0)
            {
				        if (!isset($cmdarr['stock']) || ($cmdarr['stock'] != 'stock_hide'))
                {
                    unset($row['E_STOCK']);
                }
            }

            if (isset($row['E_STOCK_REMOTE']) && $row['E_STOCK_REMOTE'] == 0)
            {
                unset($row['E_STOCK_REMOTE']);
            }

            if (isset($row['E_DELIVERY']) && $row['E_DELIVERY'] == 0)
            {
                unset($row['E_DELIVERY']);
            }

            // формирование XML
            $_xml_item = "\n<item>\n";
            $_xml_item .= "<title><![CDATA[".$row['E_TITLE']."]]></title>\n";
            if (isset($row['E_DESCR']) && $row['E_DESCR'] != "") $_xml_item .= "<description><![CDATA[".$row['E_DESCR']."]]></description>\n";
            if (isset($row['E_MANUF']) && $row['E_MANUF'] != "") $_xml_item .= "<manufacture><![CDATA[".$row['E_MANUF']."]]></manufacture>\n";
            if (isset($row['E_ROZ']) && $row['E_ROZ'] != "") $_xml_item .= "<prices><price n=\"1\" c=\"".$row['E_ROZ_C']."\">".$row['E_ROZ']."</price></prices>\n";
            if (isset($row['E_MFGDATE']) && $row['E_MFGDATE'] != false) $_xml_item .= "<mfgdate>".$row['E_MFGDATE']."</mfgdate>\n";
            if (isset($row['E_DOC']) && $row['E_DOC'] != "") $_xml_item .= "<docs><![CDATA[".$row['E_DOC']."]]></docs>\n";
            if (isset($row['E_COMP_URL']) && $row['E_COMP_URL'] != "") $_xml_item .= "<comp_url><![CDATA[".$row['E_COMP_URL']."]]></comp_url>\n";
            if (isset($row['E_STOCK'])) $_xml_item .= "<stock>".$row['E_STOCK']."</stock>\n";
            if (isset($row['E_STOCK_REMOTE'])) $_xml_item .= "<stock_remote>".$row['E_STOCK_REMOTE']."</stock_remote>\n";
            if (isset($row['E_DELIVERY'])) $_xml_item .= "<delivery>".$row['E_DELIVERY']."</delivery>\n";
            $_xml_item .= "</item>\n";

            fwrite($xml_file, clearstr($_xml_item) . "\n");
        }

        fwrite($xml_file, $g_xml_end);
        fclose($xml_file);

        $result['lines_all'] = $lines_all;
        $result['lines_fail'] = $lines_fail;

        $debug['ok'] = $lines_all - $lines_fail;
        $debug['fail'] = $lines_fail;

        if (empty($lines_all))
        {
            // TODO: убрать после дебага
            if (!file_exists($xml_file_path))
            {
                $log_data = json_encode(array(
                    'script' => 'csv2xml.php',
                    'xml_file' => '',
                    'id' => str_replace('.xml', '', end(explode('/', $xml_file_path)))
                ));
                $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                debug_log($debug_msg);
            }
            else
            {
                $log_data = json_encode(array(
                    'script' => 'csv2xml.php',
                    'xml_file' => $xml_file_path,
                    'id' => str_replace('.xml', '', end(explode('/', $xml_file_path)))
                ));
                $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                debug_log($debug_msg);
            }

            unlink($xml_file_path);
        }
        else
        {
            // Проверка хеша XML файла
            $file_hash_new = md5_file($xml_file_path);
            $file_hash_old = $DB->GetOne('SELECT price_file_hash FROM users WHERE id = ? AND price_file_hash = ?', array($cmdarr['id'], $file_hash_new));

            if ($file_hash_new !== false && $file_hash_new == $file_hash_old)
            {
                // TODO: убрать после дебага
                if (!file_exists($xml_file_path))
                {
                    $log_data = json_encode(array(
                        'script' => 'csv2xml.php',
                        'xml_file' => '',
                        'id' => str_replace('.xml', '', end(explode('/', $xml_file_path)))
                    ));
                    $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                    debug_log($debug_msg);
                }
                else
                {
                    $log_data = json_encode(array(
                        'script' => 'csv2xml.php',
                        'xml_file' => $xml_file_path,
                        'id' => str_replace('.xml', '', end(explode('/', $xml_file_path)))
                    ));
                    $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                    debug_log($debug_msg);
                }

                unlink($xml_file_path);
            }
        }
 	}
 	else
 	{
        $result['error'] = true;
        $result['errorcode'] = 8;
        $result['errdesc'] = 'Ошибка доступа к файлу ' . $cmdarr['file_path'];
 	}

 	return $result;
}


// callback функция для каждого элемента строки
function pars_element(&$val)
{
    $val = trim(strip_tags($val));
}


// название
function proc_title($val)
{
    $result = mb_substr($val, 0, 255);
    return $result;
}


// производитель
function proc_manuf($val)
{
    $result = $val;
    return $result;
}


// описание
function proc_descr($val)
{
    $result = mb_substr($val, 0, 255);
    return $result;
}


// количество на складе (-1, если складские позиции нужно скрыть)
function proc_stock($val)
{
    $result = 0;

	$val = trim($val);

    $result = preg_replace("/\s/u", "", $val);
    if (preg_match("/[-0-9]+/u", $result, $matches))
    {
        $result = $matches[0];
    }

    return ($result < 0) ? -1 : (int)$result;
}



// время поставки (-1, время поставки не определено)
function proc_delivery($val)
{
    $result = 0;

    $result = preg_replace("/\s/u", "", $val);
    if (preg_match("/[-0-9]+/u", $result, $matches))
    {
        $result = $matches[0];
    }

    return ($result < 0) ? -1 : (int)$result;
}


// валюта
function proc_currenсy($val)
{
    if (preg_match("/usd|doll|дол|\\$/iu", $val))
    {
        $result = "usd";
    }
    elseif (preg_match("/eur|евр|€/iu", $val))
    {
        $result = "eur";
    }
    elseif (preg_match("/р|руб|rur|rub/iu", $val))
    {
        $result = "rur";
    }
    else
    {
        $result = null;
    }

    return $result;
}


// цена
function proc_price($val)
{
    $result = 0;

	$val=trim($val);

	$val = preg_replace('/\s+/u','',$val);

    if (preg_match("/[-0-9,.]+/u", $val, $price))
    {
        if (preg_match("/^(\d*)([-.,\s]*)(\d*)$/u", $price[0], $matches))
        {
            if ($matches[1] != "" && preg_match("/[-.,]/u", $matches[2]) && $matches[3] != "")
            {
                $result = $matches[1].".".$matches[3];
            }
            elseif ($matches[1] != "" && $matches[2] != "" && $matches[3] != "")
            {
                $result = $matches[1].$matches[3];
            }
            elseif ($matches[1] == "" && preg_match("/[-.,]/u", $matches[2]) && $matches[3] != "")
            {
                $result = "0.".$matches[3];
            }
            elseif ($matches[1] != "" && $matches[3] == "")
            {
                $result = $matches[1];
            }
        }
    }

    return (float)$result;
}


// дата изготовления
function proc_mfgdate($val)
{
    if (preg_match("/[0-9]{4}/iu", $val, $matches))
    {
        $result = $matches[0];
    }
    elseif (preg_match("/(?:[0-9]{1,2}[-.,])?([0-9]{2})|(\s*[0-9]{2}\s*)\s*г?/iu", $val, $matches))
    {
        if ((int)$matches[1] <= (int)date("y"))
        {
            $result = "20".$matches[1];
        }
        else
        {
            $result = "19".$matches[1];
        }
    }
    else
    {
        return false;
    }

    return (int)$result;
}


// ссылка на документацию
function proc_doc_url($val)
{
    if ($val != "" && (!preg_match("/(http:\/\/)|(ftp:\/\/)/iu", $val)))
    {
        $result = "http://".$val;
    }
    else
    {
        $result = $val;
    }

    return $result;
}

// URL страницы компонента на сайте поставщика
function proc_comp_url($val)
{
    if ($val != "" && (!preg_match("/(http:\/\/)|(ftp:\/\/)/iu", $val)))
    {
        $result = "http://".$val;
    }
    else
    {
        $result = $val;
    }

    return $result;
}

