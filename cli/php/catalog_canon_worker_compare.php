<?php

require_once(PATH_BACKEND . '/php/classes/class.SimpleRegexp.php');
require_once(PATH_BACKEND . '/php/classes/class.CatalogWorkerCompare.php');


define('TIMEOUT_STATUS_UPDATE', 10);        // Таймаут (секунд) обновления статуса воркера.
define('TIMEOUT_SPHINX_RECONNECT', 10);     // Таймаут (секунд) переподключения к Sphinx в случае разрыва связи.
define('TIMEOUT_SLEEP', 1);                 // Таймаут (секунд) итерации в режиме ожидания работы.

$debug = true;


$worker = new CatalogWorkerCompare($DB, $debug);

while(true)
{
    if ($task = $worker->get_task())
    {
        $worker->do_task($task);
    }
    else
    {
        $worker->sleep();
    }
}