<?php

// --------------------------------------------------------------
// Назначение прав доступа для пользователя efind
// --------------------------------------------------------------


// подключение к БД root'ом
$DB = ADONewConnection('mysqli://root:EUcVR2PpwBkzJUIDSMj6S@localhost/e2_users?port=3306');
$DB->SetFetchMode(ADODB_FETCH_ASSOC);
$DB->Execute("SET NAMES utf8");
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;
//$DB->debug = true;

// --------------------------------------------------------------


$rs = $DB->GetCol("SHOW GRANTS FOR 'efind'@'185.35.221.54'");

foreach ($rs as $val)
{
	if (preg_match("/dist_[0-9a-f]+/u", $val, $matches))
	{
		$DB->Execute("REVOKE ALL PRIVILEGES ON e2_users.".$matches['0']." FROM 'efind'@'185.35.221.54'");
	}
}

$rs = $DB->GetCol("SHOW GRANTS FOR 'efind'@'188.130.181.142'");

foreach ($rs as $val)
{
	if (preg_match("/dist_[0-9a-f]+/u", $val, $matches))
	{
		$DB->Execute("REVOKE ALL PRIVILEGES ON e2_users.".$matches['0']." FROM 'efind'@'188.130.181.142'");
	}
}

$rs = $DB->GetCol("SHOW GRANTS FOR 'efind'@'185.35.221.17'");

foreach ($rs as $val)
{
	if (preg_match("/dist_[0-9a-f]+/u", $val, $matches))
	{
		$DB->Execute("REVOKE ALL PRIVILEGES ON e2_users.".$matches['0']." FROM 'efind'@'185.35.221.17'");
	}
}

$DB->Execute("REVOKE ALL PRIVILEGES ON e2_main.users FROM 'efind'@'185.35.221.54'");
$DB->Execute("REVOKE ALL PRIVILEGES ON e2_main.users FROM 'efind'@'188.130.181.142'");
$DB->Execute("REVOKE ALL PRIVILEGES ON e2_main.users FROM 'efind'@'185.35.221.17'");


$dist_hashes = $DB->GetCol('SELECT MD5(CONCAT(login, password)) FROM e2_main.users WHERE id > 1 AND status = 1');
foreach ($dist_hashes as $hash)
{
	$DB->Execute("GRANT SELECT , INSERT , UPDATE , DELETE, DROP, REFERENCES ON e2_users.".DB_USERS_PREFIX.$hash." TO 'efind'@'185.35.221.54'");
	$DB->Execute("GRANT SELECT , INSERT , UPDATE , DELETE, DROP, REFERENCES ON e2_users.".DB_USERS_PREFIX.$hash." TO 'efind'@'188.130.181.142'");
	$DB->Execute("GRANT SELECT , INSERT , UPDATE , DELETE, DROP, REFERENCES ON e2_users.".DB_USERS_PREFIX.$hash." TO 'efind'@'185.35.221.17'");
}

$DB->Execute("GRANT SELECT (`id`, `login`, `password`, `status`) ON `e2_main`.`users` TO 'efind'@'185.35.221.54'");
$DB->Execute("GRANT SELECT (`id`, `login`, `password`, `status`) ON `e2_main`.`users` TO 'efind'@'188.130.181.142'");
$DB->Execute("GRANT SELECT (`id`, `login`, `password`, `status`) ON `e2_main`.`users` TO 'efind'@'185.35.221.17'");

