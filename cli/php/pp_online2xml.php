<?php

// -------------------------------------------
// Скачивание и конвертировани xml-файлов по url
// -------------------------------------------


include_once(PATH_BACKEND . '/php/classes/class.XmlPrice.php');
include_once(PATH_BACKEND . '/php/xml_string_streamer/autoload.php');

// DEBUG
$debug = true;

$xml_price = new XmlPrice();

// ---------------------------------------------------------------

if ($argc > 2)
{
    $users_id = array_slice($argv, 2);

    foreach (array_keys($users_id) as $k)
    {
        $users_id[$k] = $DB->qstr($users_id[$k]);
    }

    $users = $DB->GetAll('
        SELECT
            id, login, title, password, price_limit, price_file_url, price_file_type,  price_file_login, price_file_pass
        FROM
            users
        WHERE
            id IN (' . implode(',', $users_id) . ')
            AND price_file_url != ""
            AND price_file_url IS NOT NULL
        ORDER BY last_parsing_time ASC
    ');
}
else
{
    $users = $DB->GetAll('
        SELECT
            id, login, title, password, price_limit, price_file_url, price_file_type, price_file_login, price_file_pass
        FROM
            users
        WHERE
            status = 1
            AND price_file_use = 1
            AND price_file_url != ""
        ORDER BY last_parsing_time ASC
    ');
}

foreach ($users as $user)
{
    if ($debug) { echo html_entity_decode('===================' . PHP_EOL . 'User: ' . $user['id'] . '. ' . $user['title'] . PHP_EOL); }

    $timer = array(
        'load_file'     => array('start' => 0, 'finish' => 0, 'time' => 0, 'view' => ''),
        'parser'        => array('start' => 0, 'finish' => 0, 'time' => 0, 'view' => '')
    );

    $timer['load_file']['start'] = microtime(1);

    $file_extension = ($user['price_file_type'] == 'einfo_json') ? 'json' : 'xml';

    $file_path = PATH_PRICES . '/0_tmp/' . md5(uniqid()) . '.' . $file_extension;

    // Скачиваем файл
    $file_handle = fopen($file_path, 'wb');
    $curl = curl_init($user['price_file_url']);
    curl_setopt($curl, CURLOPT_FILE, $file_handle);

    if (!empty($user['price_file_login']) && !empty($user['price_file_pass']))
    {
        curl_setopt($curl, CURLOPT_USERPWD, $user['price_file_login'] . ':' . $user['price_file_pass']);
    }

    curl_exec($curl);
    $curl_info = curl_getinfo($curl);
    curl_close($curl);
    fclose($file_handle);

    if ($curl_info['http_code'] == 200 && is_file($file_path) && filesize($file_path))
    {
        $timer['load_file']['finish'] = microtime(1);
        $timer['load_file']['time'] = $timer['load_file']['finish'] - $timer['load_file']['start'];
        $timer['load_file']['view'] = number_format($timer['load_file']['time'], 2, '.', '') . ' sec.';

        if ($debug) { echo 'File is loaded (' . $user['price_file_url'] . ') for ' . $timer['load_file']['view'] . PHP_EOL; }

        $timer['parser']['start'] = microtime(1);

        // Подготавливаем файл
        sleep_overload();

        if ($file_extension == 'xml')
            $prepare = $xml_price->prepareXmlFileFromXml($file_path, $user['price_file_type'], $user);
        else
            $prepare = $xml_price->prepareXmlFileFromJson($file_path, $user);

        if ($debug) { echo 'Parsing rows - TOTAL: ' . $prepare['cnt']['total'] . '; OK: ' . $prepare['cnt']['ok'] . '; FAIL: ' . $prepare['cnt']['fail'] . PHP_EOL; }

        // Время парсинга
        $timer['parser']['finish'] = microtime(1);
        $timer['parser']['time'] = $timer['parser']['finish'] - $timer['parser']['start'];
        $timer['parser']['view'] = number_format($timer['parser']['time'], 2, '.', '') . ' sec.';

        $DB->Execute('UPDATE users SET last_parsing_time = ? WHERE id = ?', array($timer['parser']['time'], $user['id']));

        if ($debug) { echo 'Parsing time: ' . $timer['parser']['view'] . PHP_EOL; }

        // Перемещаем XML файл из временной папки
        $target_file_path = PATH_PRICES . '/3_xml/' . $user['id'] . '.xml';
        $rename_status = rename($file_path, $target_file_path);

        if ($debug)
        {
            echo ('Rename to ' . $target_file_path);
            if ($rename_status)
            {
                echo (' is OK' . PHP_EOL);
            }
            else
            {
                echo (' is FAIL' . PHP_EOL);
            }
        }

        if (empty($prepare['cnt']['total']))
        {
            // TODO: убрать после дебага
            if (!file_exists($target_file_path))
            {
                $log_data = json_encode(array(
                    'script' => 'online2xml.php',
                    'xml_file' => '',
                    'id' => str_replace('.xml', '', end(explode('/', $target_file_path)))
                ));
                $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                debug_log($debug_msg);
            }
            else
            {
                $log_data = json_encode(array(
                    'script' => 'online2xml.php',
                    'xml_file' => $target_file_path,
                    'id' => str_replace('.xml', '', end(explode('/', $target_file_path)))
                ));
                $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                debug_log($debug_msg);
            }

            // Удаляем файл если количество обработанных позиций равно 0
            unlink($target_file_path);
        }
        else
        {
            // Проверка хеша XML файла
            $file_hash_new = md5_file($target_file_path);
            $file_hash_old = $DB->GetOne('SELECT price_file_hash FROM users WHERE id = ? AND price_file_hash = ?', array($user['id'], $file_hash_new));

            if ($file_hash_new !== false && $file_hash_new == $file_hash_old)
            {
                // TODO: убрать после дебага
                if (!file_exists($target_file_path))
                {
                    $log_data = json_encode(array(
                        'script' => 'online2xml.php',
                        'xml_file' => '',
                        'id' => str_replace('.xml', '', end(explode('/', $target_file_path)))
                    ));
                    $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                    debug_log($debug_msg);
                }
                else
                {
                    $log_data = json_encode(array(
                        'script' => 'online2xml.php',
                        'xml_file' => $target_file_path,
                        'id' => str_replace('.xml', '', end(explode('/', $target_file_path)))
                    ));
                    $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                    debug_log($debug_msg);
                }

                unlink($target_file_path);
            }
        }
    }
    else
    {
        echo("Can't load file from " . $user['price_file_url'] . PHP_EOL);
    }
}
