<?php

include_once(PATH_BACKEND . '/php/classes/class.phpmailer.php');

$debug = true;

$DB->Execute('SET NAMES utf8');
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;

// Корзины, поставщикам из которых нужно отправить уведомления
$baskets = $DB->GetAll('
  SELECT
    orders.*,
    clients.name AS contact_name,
    clients.email AS contact_email,
    clients.phone AS contact_phone
  FROM
    orders
    INNER JOIN clients ON orders.client_id = clients.id
  WHERE
    orders.valid = 1 AND
    orders.send = 0
');

if (!empty($baskets))
{
    foreach($baskets as $basket)
    {

        if ($debug) { echo PHP_EOL . PHP_EOL . 'Order #' . $basket['id'] . ':' . PHP_EOL; }

        // Поставщики, которым нужно отправить уведомления
        $users = $DB->GetAll('
            SELECT
              users.id,
              users.email_order_notification,
              users.title
            FROM
              orders_items AS items
              INNER JOIN users ON items.user_id = users.id
            WHERE
              items.order_id = ?
            GROUP BY user_id
        ', array($basket['id']));

        foreach ($users as $user)
        {
            // Список товаров поставщика из текущей корзины
            $items = $DB->GetAll('
                SELECT
                  *
                FROM
                  orders_items
                WHERE
                  order_id = ?
                  AND user_id = ?
            ', array($basket['id'], $user['id']));

            // Отправляем письмо
            $mail = new PHPMailer();
            $mail->CharSet = 'utf-8';
            $mail->Sender = 'admin@einfo.ru';
            $mail->SetFrom('admin@einfo.ru', 'Робот einfo.ru');
            $mail->AddReplyTo($basket['contact_email'], $basket['contact_name']);
            $mail->Subject = '[einfo.ru] Заявка #' . $basket['id'] . ' на комплектующие';
            $mail->IsHTML();

            $email_to = preg_split("/[,;\s]/u", $user['email_order_notification'], -1, PREG_SPLIT_NO_EMPTY);
            foreach ($email_to as $value)
            {
                $mail->AddAddress(trim($value));
            }

            $smarty->assign('site_host', HOST_FRONTEND);
            $smarty->assign('user_title', $user['title']);
            $smarty->assign('basket', $basket);
            $smarty->assign('basket_items', $items);
            $smarty->assign('settings', $SETTINGS);
            $mail->Body = $smarty->fetch('mail_basket_send.tpl');

            $_send_result = $mail->Send();

            if ($debug) { echo '  > ' . $user['id'] . '. ' . html_entity_decode($user['title']) . ' (' . $user['email_order_notification'] . ')'; }
            if ($_send_result)
            {
                if ($debug) { echo ' - OK' . PHP_EOL; }
            }
            else
            {
                if ($debug) { echo ' - FAIL' . PHP_EOL; }
            }
        }

        $DB->Execute('UPDATE orders SET send = 1 WHERE id = ?', array($basket['id']));
    }
}