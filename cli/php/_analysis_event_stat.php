<?php

/**
 * Скрипт анализа данных из таблицы event_stat
 *
 * Применение: php cli.php _analysis_event_stat
 */

// Значения
$debug = false; // Режим дебага. По умолчанию: false
$max_debug_count_rows = 10000; // Ограничение для дебага и отслеживание результата (по точкам в консоли). По умолчанию: 100 000
$max_dotted_line = 100; // Максимальное количество точек на одной строке
$max_dotted_iteration = 1000; // Количество итераций, после которых проставляется точка. По умолчанию: за каждую 1 тысячу итераций
$max_insert = 10000; // Пакетная вставка данных в запросе. По умолчанию: 10 000

// Дебаг-мод для запросов
$DB->debug = $debug;

// Таблица для дебага
$table_name = $debug ? '_tmp_comp_stat_copy' : '_tmp_comp_stat';

// Массив, в который наполняется обработаннй результат из выборки
$events = array();

// Подготавливаем запрос
$prepare = 'SELECT
        useragent,
        event_label,
        event_type
    FROM event_stat
    WHERE useragent NOT LIKE "%bot%"
      OR useragent LIKE "%CUBOT%"';

// Если режим дебага включен, то ограничиваем основной запрос
if ($debug)
{
	echo '[WARNING]: Debug mode is enabled. Limit rows: ' . $max_debug_count_rows . PHP_EOL;

	$prepare .= ' LIMIT ' . $max_debug_count_rows;
}

// Перебираем данные запроса и храним их в массиве
$i = 0;
$j = 0;
$result = $DB->Execute($prepare);
while ($item = $result->FetchRow())
{
	$event = array(
        'comp_canon'            => '',
        'visit_page'            => 0,
        'search'                => 0,
        'search_comp_select'    => 0,
        'open_request_form'     => 0
    );

    $type = $item['event_type'];

    if ($type == 'visit_page')
    {
        $matches = array();
        if (preg_match('/\/(store|pn)\/(!)?(.+)\//', $item['event_label'], $matches))
        {
            $event['comp_canon'] = $matches[3];
        }
    }
    else if ($type == 'search')
    {
        $event['comp_canon'] = $item['event_label'];
    }
    else if ($type == 'open_request_form')
    {
        $event['comp_canon'] = explode('(', $item['event_label']);
        $event['comp_canon'] = $event['comp_canon'][0];
    }
    else if ($type == 'search_comp_select')
    {
        $event['comp_canon'] = $item['event_label'];
    }

    $event[$type] = 1;

    // Возводим в верхний регистр
    $event['comp_canon'] = trim(
    	mb_strtoupper($event['comp_canon'])
    );

    // Убираем длинные пробелы
    $event['comp_canon'] = preg_replace('/\s{2,}/', '', $event['comp_canon']);

    // Убираем пробелы перед дефисом и после
    $event['comp_canon'] = preg_replace('/\s-\s/', '-', $event['comp_canon']);

    if (empty($event['comp_canon']))
    {
        continue;
    }

    // Если строка подошла под условия, то заполняем массив
    $comp_canon = $event['comp_canon'];
    $events[$comp_canon][$type] = empty($events[$comp_canon][$type]) ? $event[$type] : $events[$comp_canon][$type] + $event[$type];

    if (($i % $max_dotted_line) == 0)
    {
    	echo '.';

    	if (($j % 2) == $max_dotted_line)
        {
            echo PHP_EOL;
        }

        $j++;
    }

    $i++;
}


// Начинаем вставку строк пакетной обработкой. 
echo PHP_EOL . 'Expected to be added to the table...' . PHP_EOL;

$i = 0;
$j = 0;
$query = '';

// Количество строк
$max_events = count($events);

// Если строк меньше, чем требуется для пакетной обработки
$max_insert = $max_events < $max_insert ? $max_events : $max_insert;

foreach ($events as $event_name => $value)
{
	if (($i % $max_insert) == 0 || ($max_events - 1) == $i)
	{
		if ($i > 0)
		{
			$query .= ';';
			$DB->Execute($query);

			echo '.';

			if (($j % 2) == $max_dotted_line)
	        {
	            echo PHP_EOL;
	        }

	        $j++;
		}

		// Если это последний цикл, то завершаем операцию
		if (($max_events - 1) == $i)
		{
			break;
		}

		$query = 'INSERT INTO ' . $table_name . ' (comp_canon, visit_page, search, search_comp_select, open_request_form) VALUES';
	}
	else 
	{
		$query .= ',';
	}


	$value['visit_page'] = empty($value['visit_page']) ? 0 : $value['visit_page'];
	$value['search'] = empty($value['search']) ? 0 : $value['search'];
	$value['search_comp_select'] = empty($value['search_comp_select']) ? 0 : $value['search_comp_select'];
	$value['open_request_form'] = empty($value['open_request_form']) ? 0 : $value['open_request_form'];

	$query .= ' (' . $DB->qstr($event_name) . ', ' . $value['visit_page'] . ', ' . $value['search'] . ', ' . $value['search_comp_select'] . ', ' . $value['open_request_form'] . ')';

	$i++;
}

// Выводим процент сходства
$percent = $DB->GetOne(
    'SELECT (COUNT(cs.comp_canon) / COUNT(cc.id) * 100) AS percent
    FROM catalog_comp_canon cc
    LEFT JOIN (
        SELECT cs.*
        FROM ' . $table_name . ' cs
    ) cs ON cs.comp_canon = cc.comp_canon');

echo PHP_EOL . 'Percent rows from `' . $table_name . '` in `catalog_comp_canon`: ' . $percent . '%' . PHP_EOL;

// -----------------------------------------------------------------------------------

/**
 * Dump'n'die
 *
 * @param $var
 */
function dd($var)
{
    var_dump($var);
    die;
}