<?php

// Правки формата цен e2_main.base_main, перенос roz, roz_cur в prices.

echo "Правки " . DB_MAIN_NAME . ".base_main \n";

$rs = $DB->Execute('SELECT id, roz, roz_cur FROM base_main');

while(!$rs->EOF)
{
    $id = $rs->fields('id');
    $prices = json_encode(array(array('n' => '1', 'c' => $rs->fields('roz_cur'), 'p' => $rs->fields('roz'))));

    $DB->Execute('UPDATE base_main SET prices = ? WHERE id = ?', array($prices, $id));

    $rs->MoveNext();
}