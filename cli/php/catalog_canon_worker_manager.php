<?php

require_once(PATH_BACKEND . '/php/classes/class.SimpleRegexp.php');
require_once(PATH_BACKEND . '/php/classes/class.CatalogWorkerManager.php');


define('POOL_SIZE', 20000);              // Количество "слов" в задаче.
define('TIMEOUT_COMPARE', 60);          // Таймаут воркера (сек.), после которого он считается "зависшим".
define('TIMEOUT_COMPARE_REMOVE', 120);  // Таймаут воркера (сек.), после которого он удаляется из таблицы БД.
define('TIMEOUT_SLEEP', 200000);        // Таймаут итерации (микросек.) при ожидании нового шаблона для парсинга.
define('TIMEOUT_SET_TASK', 200000);     // Таймаут итерации (микросек.) при ожидании свободного воркера для текущей задачи.

$debug = true;


$manager = new CatalogWorkerManager($DB, $debug);

while(true)
{

    if ($task = $manager->get_sleeping_task())
    {
        $manager->set_task($task);
    }
    else if ($task = $manager->get_task())
    {
        $manager->set_task($task);
    }
    else
    {
        $manager->sleep();
    }

}