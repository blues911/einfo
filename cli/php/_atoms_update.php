<?php

/*
* Скрипт замены поля atoms для нового строгого поиска в таблице base_main.
*
* Последний параметр вариативный, задаёт количество изменяемых элементов в одной итерации
* по умолчанию равен 10000
*
* Использование: php cli.php _atoms_update 10000
*/

function elapsed_time($time)
{
    $result = array();

    $total_min = floor($time / 60);

    if ($total_min >= 60)
    {
        $result['hour'] = floor($total_min / 60);
        $result['min'] = $total_min - ($result['hour'] * 60);
    }
    else $result['min'] = $total_min;

    $result['sec'] = round($time - ($total_min * 60), 2);

    return $result;
}

// Получаем стартовый ID
$start_id = $DB->GetOne('SELECT MIN(id) FROM base_main');
$chunk = !empty($argv[2]) ? (int)$argv[2] : 10000;

if (empty($chunk))
{
    echo 'Wrong chunk size!' . PHP_EOL;
    exit;
}

// Получаем максимальный ID  и количество необходимых проходов
$max_id = $DB->GetOne('SELECT MAX(id) FROM base_main');
$total_iteration = ceil($max_id / $chunk);

echo 'Max ID = ' . $max_id . PHP_EOL;
echo 'Total iterations = ' . $total_iteration . PHP_EOL;

echo 'Start update atoms.' . PHP_EOL;
echo 'One point = ' . $chunk . ' IDs' . PHP_EOL;
echo 'One row = ' . ($chunk * 100) . ' IDs' . PHP_EOL;
$start = microtime(true);

// Проходимся по выборке итерациями
for ($i = 1; $i < $total_iteration; $i++)
{
    // Получаем ограничивающий ID для итерации
    $end_id = $start_id + $chunk;

    $DB->Execute(
        'UPDATE LOW_PRIORITY base_main
         SET atoms = CONCAT(TRIM(atoms), " ")
         WHERE id BETWEEN ? AND ?',
         array($start_id, $end_id)
    );

    echo '.';
    if ($i % 10 == 0) echo '|';
    if ($i % 100 == 0) echo PHP_EOL;

    // Устанавливаем новое значение стартового ID
    $start_id = $end_id + 1;
}

// Вычисляем затраченное время
$time = microtime(true) - $start;
$elapsed = elapsed_time($time);

echo PHP_EOL . 'Time elapsed: ';
echo isset($elapsed['hour']) ? $elapsed['hour'] . 'h ' : '';
echo isset($elapsed['min']) ? $elapsed['min'] . 'm ' : '';
echo isset($elapsed['sec']) ? $elapsed['sec'] . 's' : '';
echo PHP_EOL;