<?php

/*
* Скрипт для экспорта списка уникальных названий компонентов
* в текстовый файл из таблицы base_main.
*
* В текстовом файле каждое имя компонента с новой строчки.
*
* Использование: php cli.php _export_short_title /path/to/file.ext
*/

if (!isset($argv[2])) die('File path not implemented!');

$file = $argv[2];

function elapsed_time($time)
{
    $result = array();

    $total_min = floor($time / 60);

    if ($total_min >= 60)
    {
        $result['hour'] = floor($total_min / 60);
        $result['min'] = $total_min - ($result['hour'] * 60);
    }
    else $result['min'] = $total_min;

    $result['sec'] = $time - ($total_min * 60);

    return $result;
}

// -----------------------------------------------------------------------------

echo 'Script started at ' . date('Y-m-d H:i') . PHP_EOL;

// Получаем массив уникальных значений для поставщика 1
$exclude = array_flip($DB->GetCol('SELECT DISTINCT(LOWER(comp_title)) FROM base_main 
WHERE user_id = 1
OR manuf_title = "mw"
OR manuf_title like "mean%w%"
'));


# $exclude = array_flip($DB->GetCol('SELECT 0'));

$cnt = 0;
$start_id = 1;
$chunk = 700000000;

// Получаем максимальный ID  и количество необходимых проходов
$max_id = $DB->GetOne('SELECT MAX(id) FROM base_main');
$total_iteration = ceil($max_id / $chunk);

echo 'Max ID = ' . $max_id . PHP_EOL;
echo 'Total iterations = ' . $total_iteration . PHP_EOL;

// Удаляем предыдущий файл если есть
@unlink($file);

echo 'Start getting results.' . PHP_EOL;
$start = microtime(true);

// Проходимся по выборке итерациями по 10 000 000 id
for ($i = 1; $i < $total_iteration; $i++)
{
    $stop_id = $start_id + $chunk;

    $result = $DB->Execute('SELECT DISTINCT(LOWER(comp_title)) AS comp_title FROM base_main 
    WHERE LENGTH(comp_title) < 24 
    AND LENGTH(comp_title) > 3
	AND id BETWEEN ? AND ?', array($start_id, $stop_id));

    while(is_object($result) && !$result->EOF)
    {
        $comp_title = $result->fields('comp_title');

        if (!isset($exclude[$comp_title]))
        {
            $title = $comp_title;
            $cnt++;

            file_put_contents($file, mb_strtoupper($comp_title) . PHP_EOL, FILE_APPEND);

            if (!($cnt % 100000)) echo '.';
            if (!($cnt % 1000000)) echo '|';
            if (!($cnt % 10000000)) echo PHP_EOL . 'Iteration step ' . $i . PHP_EOL;
        }

        $result->MoveNext();
    }

    $start_id = $stop_id;
}

// Вычисляем затраченное время
$time = microtime(true) - $start;
$elapsed = elapsed_time($time);

echo PHP_EOL . 'Unique titles count = ' . $cnt . PHP_EOL;
echo 'Time elapsed: ';
echo isset($elapsed['hour']) ? $elapsed['hour'] . 'h ' : '';
echo isset($elapsed['min']) ? $elapsed['min'] . 'm ' : '';
echo isset($elapsed['sec']) ? $elapsed['sec'] . 's' : '';
echo PHP_EOL;
