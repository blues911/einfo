<?php

$result = $DB->GetAll(
    'SELECT
        users.id AS user_id,
        MD5(CONCAT(users.login, users.password)) AS `hash`,
        users.title,
        users.status,
        (SELECT COUNT(*) FROM base_main WHERE user_id = users.id) AS cnt_real,
        users.cnt
    FROM users'
);

foreach (array_keys($result) as $k)
{
    $result[$k]['cnt_buffer'] = $DB2->GetOne('SELECT COUNT(*) FROM ' . DB_USERS_PREFIX . $result[$k]['hash']);

    if (($result[$k]['cnt'] == $result[$k]['cnt_real']) && ($result[$k]['cnt'] == $result[$k]['cnt_buffer']))
    {
        unset($result[$k]);
    }
}

if (!empty($result))
{
    foreach ($result as $row)
    {
        echo($row['user_id'] . '. ' . $row['title'] . PHP_EOL);
        echo('CNT[USERS]: ' . $row['cnt'] . '; CNT[BASE_MAIN]: ' . $row['cnt_real'] . '; CNT[BUFFER]: ' . $row['cnt_buffer'] . '; STATUS: ' . $row['status'] . PHP_EOL . PHP_EOL);
    }
}