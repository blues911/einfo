<?php

/* Добавление колонок:
 * e2_main.base_main              (prices)
 * e2_main.orders_items           (prices)
 * e2_main.catalog_comp_canon     (price_median)
 * e2_users.dist_xxx              (prices)
 */

// ------------------------------------------------------------

echo "1) Добавление prices в " . DB_MAIN_NAME . ".base_main \n";

$DB->Execute('ALTER TABLE base_main ADD prices TEXT DEFAULT NULL AFTER opt_cur');

// ------------------------------------------------------------

echo "2) Добавление prices в " . DB_MAIN_NAME . ".orders_items \n";

$DB->Execute('ALTER TABLE orders_items ADD prices TEXT DEFAULT NULL AFTER opt_cur');

// ------------------------------------------------------------

echo "3) Добавление price_median в " . DB_MAIN_NAME . ".catalog_comp_canon \n";

$DB->Execute('ALTER TABLE catalog_comp_canon ADD price_median DECIMAL(12,4) NOT NULL DEFAULT 0.0000 AFTER price_roz_median');

// ------------------------------------------------------------

echo "4) Добавление prices в " . DB_USERS_NAME . ".dist_xxx \n";

$sql_query = sprintf("SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema='%1\$s'", DB_USERS_NAME);
$schema_tables = $DB2->GetAll($sql_query);

foreach ($schema_tables as $item)
{
    $DB2->Execute('ALTER TABLE ' . $item['TABLE_NAME'] . ' ADD prices TEXT DEFAULT NULL AFTER opt_cur');
}