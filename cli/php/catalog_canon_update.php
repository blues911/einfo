<?php

require_once(PATH_FRONTEND . '/php/libs/sphinxapi.php');

$debug = true;

$usd = $SETTINGS['cur_rates']['usd'];
$eur = $SETTINGS['cur_rates']['eur'];

if ($debug) { echo 'Connect to sphinx -> '; }

// Соединение со Sphinx Engine
$sphinx = new SphinxClient();
$sphinx->SetServer(SPHINX_HOST, SPHINX_PORT);
$sphinx->SetMatchMode(SPH_MATCH_EXTENDED);
$sphinx->SetLimits(0, 3000, 3000, 3000);

if ($sphinx->Status() !== false)
{
    echo 'OK' . PHP_EOL;

    // Выбираем элементы
    $canon_rs = $DB->Execute('SELECT * FROM catalog_comp_canon');

    while(!$canon_rs->EOF)
    {
        $word = $canon_rs->fields('comp_canon');

        if ($debug) { echo($word); }

        $word_exist = $DB->GetOne('SELECT SQL_NO_CACHE id FROM base_main WHERE comp_title LIKE ? LIMIT 0, 1', array($word . '%'));
        if (!empty($word_exist))
        {
            $sphinx_query = '"^' . implode(' ', get_request_atoms($word)) . '"';
            $sphinx_result = $sphinx->Query($sphinx_query, SPHINX_INDEX);

            if (!empty($sphinx_result['matches']))
            {
                $comp_ids = array_keys($sphinx_result['matches']);

                if ($debug) { echo ' (' . count($comp_ids) . ')'; }

                $atoms = get_request_atoms($word);
                $atoms = !empty($atoms) ? implode(' ', $atoms) . ' ' : '';

                $list_rs = $DB->Execute('
                    SELECT SQL_NO_CACHE
                      main.id AS id,
                      main.prices AS prices
                    FROM
                      base_main AS main
                      INNER JOIN users ON main.user_id = users.id
                    WHERE
                      users.status = 1
                      AND main.id IN (' . implode(',', $comp_ids) . ')
                      AND main.atoms LIKE ?
                ', array($atoms . '%'));

                $list = array('id' => array(), 'price' => array());
                while(!$list_rs->EOF)
                {
                    $list['id'][] = $list_rs->fields('id');

                    $prices = json_decode($list_rs->fields('prices'), true);

                    if (!empty($prices))
                    {
                        $price = array_shift($prices);
            
                        if ($price['c'] == 'rur')
                        {
                            $list['price'][] = $price['p'];
                        }
                        else if ($price['c'] == 'usd')
                        {
                            $list['price'][] = $price['p'] * $usd;
                        }
                        else
                        {
                            $list['price'][] = $price['p'] * $eur;
                        }
                    }

                    $list_rs->MoveNext();
                }

                $canon = array(
                    'offer_cnt' => count($list['id']),
                    'price_median' => median($list['price'])
                );

                if ($debug) { echo(' -> ' . $canon['offer_cnt']); }

                $DB->Execute('
                    UPDATE
                        catalog_comp_canon
                    SET
                        offer_cnt = ?,
                        price_median = ?
                    WHERE
                        id = ?
                ', array(
                    $canon['offer_cnt'],
                    $canon['price_median'],
                    $canon_rs->fields('id')
                ));
            }

            if ($debug) { echo PHP_EOL; }
        }

        $canon_rs->MoveNext();
    }

    // Пересчет кол-ва компонентов по категориям
    $categories = $DB->GetAll('
        SELECT
          cat.id,
          cat.cnt AS cnt_old,
          COUNT(canon.id) AS cnt
        FROM
          categories AS cat
          LEFT OUTER JOIN catalog_comp_canon AS canon ON cat.id = canon.category_id AND canon.offer_cnt >= ?
        GROUP BY cat.id
    ', array(CATALOG_MIN_SUPPLY));

    foreach ($categories as $category)
    {
        if ($category['cnt_old'] != $category['cnt'])
        {
            $DB->Execute('UPDATE categories SET cnt = ? WHERE id = ?', array($category['cnt'], $category['id']));
        }
    }
}
else
{
    echo 'FAIL';
}