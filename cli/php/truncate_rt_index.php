<?php

include_once (PATH_FRONTEND . '/php/classes/class.Search.php');
include_once (PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

$sphinxRT = new SearchSphinxQL();

// Полная очистка RT индексов
$sphinxRT->truncate_rt_index();
echo 'RT indexes are truncated' . PHP_EOL;
