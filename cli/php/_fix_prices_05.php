<?php

// Правки формата цен e2_users.dist_xxx, перенос roz, roz_cur в prices.

$sql_query = sprintf("SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema='%1\$s'", DB_USERS_NAME);
$schema_tables = $DB2->GetAll($sql_query);

for ($i = 0; $i < count($schema_tables); $i++)
{
    echo "Правки " . DB_USERS_NAME . "." . $schema_tables[$i]['TABLE_NAME'] . " \n";

    $rs = $DB2->Execute('SELECT id, roz, roz_cur FROM ' . $schema_tables[$i]['TABLE_NAME']);

    while(!$rs->EOF)
    {
        $id = $rs->fields('id');
        $prices = json_encode(array(array('n' => '1', 'c' => $rs->fields('roz_cur'), 'p' => $rs->fields('roz'))));

        $DB2->Execute('UPDATE ' . $schema_tables[$i]['TABLE_NAME'] . ' SET prices = ? WHERE id = ?', array($prices, $id));
    
        $rs->MoveNext();
    }
}