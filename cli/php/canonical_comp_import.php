<?php

/*
* Скрипт для внесения канонических компонентов в таблицу canonical_comp.
*
* Использование: php cli.php canonical_comp_import
*/

include_once(PATH_FRONTEND . '/php/ext_function.php');

// Количество выбираемых за проход компонентов
$chunk_size = 10000;

// -----------------------------------------------------------------------------

echo 'Script started at ' . date('Y-m-d H:i') . PHP_EOL;

// Создаём временную таблицу
echo 'Create temporary table...';
$DB->Execute('DROP TABLE IF EXISTS temp_comp_title');
$DB->Execute(
    'CREATE TABLE temp_comp_title(
      `comp_title` VARCHAR(255) NOT NULL,
      PRIMARY KEY (`comp_title`)
    ) ENGINE=MyISAM CHARSET=cp1251 COLLATE=cp1251_general_ci'
);
$_error = $DB->ErrorMsg();
echo (empty($_error) ? 'OK' : 'Fail' . PHP_EOL . $_error) . PHP_EOL;

$i = 1;

$min_id = $DB->GetOne('SELECT MIN(id) FROM base_main');
$max_id = $DB->GetOne('SELECT MAX(id) AS max_id FROM base_main');

echo 'MIN ID = ' . $min_id . PHP_EOL;
echo 'MAX ID = ' . $max_id . PHP_EOL;

$start_id = ($chunk_size * floor($min_id / $chunk_size)) + 1;
$end_id = $start_id + $chunk_size - 1;

echo 'START ID = ' . $start_id . PHP_EOL;
echo 'END ID = ' . $end_id . PHP_EOL;

$iterations = ceil(($max_id - $start_id) / $chunk_size);

echo 'Iteration count = ' . $iterations . PHP_EOL;

// Выбираем из таблицы N компонентов с ID больше чем установленное значение
for ($i = 1; $i <= $iterations; $i++)
{
    $list = $DB->GetAll('SELECT id, comp_title FROM base_main WHERE id BETWEEN ? AND ?', array($start_id, $end_id));

    sleep_overload();

    $_values = array();

    foreach ($list as $item)
    {
        // Проверяем название на каноничность и добавляем в массив в случае успеха
        if (check_canonical($item['comp_title'], $DB))
        {
            $_values[] = '(UPPER(' . $DB->qstr($item['comp_title']) . '))';
        }
    }

    // Если были найдены подходящие записи записываем во временную таблицу
    if (!empty($_values))
    {
        $DB->Execute('INSERT LOW_PRIORITY IGNORE INTO temp_comp_title (comp_title) VALUES ' . implode(', ', $_values));

        $_error = $DB->ErrorMsg();
        echo (!empty($_error) ? $_error . PHP_EOL : '');
    }

    $start_id += $chunk_size;
    $end_id += $chunk_size;

    echo '.';
    if ($i % 10 == 0) echo '|';
    if ($i % 100 == 0) echo PHP_EOL;
}

echo PHP_EOL;

// Получаем количество компонентов во временной таблице
$tmp_cnt = $DB->GetOne('SELECT COUNT(`comp_title`) FROM temp_comp_title');
echo 'Component count in temporary table = ' . $tmp_cnt . PHP_EOL;

// Если есть компоненты для записи запускаем заполнение таблицы
if (!empty($tmp_cnt))
{
    try
    {
        // Предварительно очищаем основную таблицу
        echo 'Table canonical_comp truncate...';
        $DB->Execute('TRUNCATE TABLE canonical_comp');
        echo 'complete' . PHP_EOL;

        // Вносим записи отсортированные по названию
        echo 'Inserting into canonical_comp...';
        $DB->Execute(
            'INSERT INTO canonical_comp (`comp_title`)
             SELECT `comp_title`
             FROM temp_comp_title
             ORDER by `comp_title` ASC'
        );
        $_error = $DB->ErrorMsg();
        echo (empty($_error) ? 'OK' : 'Fail' . PHP_EOL . $_error) . PHP_EOL;
    }
    catch (\Exception $e)
    {
        echo 'Error ' . $e->getCode() . ': ' . $e->getMessage() . PHP_EOL;
    }
}

// Получаем количество компонентов в основной таблице
$cnt = $DB->GetOne('SELECT COUNT(`comp_title`) FROM canonical_comp');
echo 'Component count in canonical_comp = ' . $cnt . PHP_EOL;

// Удаляем временную таблицу
echo 'Drop temporary table...';
$DB->Execute('DROP TABLE IF EXISTS temp_comp_title');
echo 'complete' . PHP_EOL;