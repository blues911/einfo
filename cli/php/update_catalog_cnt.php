<?php

// Обновляем общий счетчик компонентов
$total_cnt = (int)$DB->GetOne('
    SELECT
      COUNT(DISTINCT(comp_title))
    FROM
      base_main
      INNER JOIN users ON base_main.user_id = users.id
    WHERE
      users.status = 1
');

$DB->Execute('
    UPDATE LOW_PRIORITY
      settings
    SET
      settings.value = ?
    WHERE
      settings.var = "catalog:main_cnt"
', array($total_cnt));