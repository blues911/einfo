<?php

// --------------------------------------------------------------
// Массовое изменение буферных таблиц
// --------------------------------------------------------------


$users = $DB2->GetAll('SELECT id, MD5(CONCAT(`login`, `password`)) AS hash FROM ' . DB_MAIN_NAME . '.users');

foreach ($users as $user)
{
    $table = 'dist_' . $user['hash'];

    echo($user['id'] . '. ' . $table . ' --> ');

    $DB2->Execute('DROP TABLE IF EXISTS ' . $table);

    $DB2->Execute(
        'CREATE TABLE ' . $table . ' (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `category` varchar(255) NOT NULL DEFAULT "",
            `title` varchar(255) NOT NULL DEFAULT "",
            `descr` varchar(255) NOT NULL DEFAULT "",
            `manuf` varchar(64) NOT NULL DEFAULT "",
            `roz` decimal(12,4) unsigned NOT NULL DEFAULT "0.0000",
            `roz_cur` enum("rur","usd","eur") NOT NULL DEFAULT "rur",
            `opt` decimal(12,4) unsigned NOT NULL DEFAULT "0.0000",
            `opt_cur` enum("rur","usd","eur") NOT NULL DEFAULT "rur",
            `mfgdate` year(4) NOT NULL DEFAULT "0000",
            `doc_url` varchar(255) NOT NULL DEFAULT "",
            `comp_url` varchar(255) NOT NULL,
            `action` enum("sale","buy") NOT NULL DEFAULT "sale",
            `stock` int(11) NOT NULL DEFAULT "0",
            `stock_remote` int(11) NOT NULL DEFAULT "0",
            `delivery` tinyint(3) NOT NULL DEFAULT "0",
            PRIMARY KEY (`id`),
            UNIQUE KEY `uniq` (`title`,`category`,`mfgdate`,`action`,`delivery`,`descr`,`manuf`)
        ) ENGINE=MyISAM DEFAULT CHARSET=cp1251'
    );

    $DB2->Execute(
        'INSERT INTO ' .
        DB_USERS_NAME . '.' . $table . ' (`category`, `title`, `descr`, `manuf`, `roz`, `roz_cur`, `opt`, `opt_cur`, `mfgdate`, `doc_url`, `comp_url`, `action`, `stock`, `delivery`)
        SELECT
            "" AS `category`,
            `comp_title` AS `title`,
            `descr`,
            `manuf_title` AS `manuf`,
            `roz`,
            `roz_cur`,
            `opt`,
            `opt_cur`,
            `mfgdate`,
            `doc_url`,
            `comp_url`,
            "sale" AS `action`,
            `stock`,
            `delivery`
        FROM
            ' . DB_MAIN_NAME . '.base_main
        WHERE
            user_id = ?',
        array($user['id'])
    );

    // Закрываем буферную таблицу
    $DB2->Execute('FLUSH TABLE ' . DB_USERS_NAME . '.' . $table);

    echo('OK' . PHP_EOL);
}

sleep(5);
$DB->Execute('UPDATE `settings` SET `value` = NOW() WHERE `var` = "last_buff_update"');
