<?php

// --------------------------------------------------------------
// Расчет рейтинга у пользователей
// --------------------------------------------------------------

// Коэффициенты влияния параметров на итоговый рейтинг (в сумме должно быть 1.0)
$koeff = array(
    'update_time' => 0.5,
    'profile_view' => 0.5
);

// Множитель коэффициэнта
$scale = 5;

// Кол-во дней для учета просмотра профиля
$profile_view_days = 7;

// Заполнение нулями
$rating = array();
$users_ids = $DB->GetCol('SELECT id FROM users');
foreach ($users_ids as $user_id)
{
	$rating['update_time'][$user_id] = 0;
	$rating['profile_view'][$user_id] = 0;
}

// Расчет значений по времени обновления

$list_timediff = $DB->GetAll('SELECT id AS user_id, TIMESTAMPDIFF(second, users.price_date, NOW()) AS timediff FROM users');

foreach ($list_timediff as $row)
{
    if ($row['timediff'] > 1209600)
    {
        $rating['update_time'][$row['user_id']] = 1209600;
    }
    else
    {
        $rating['update_time'][$row['user_id']] = $row['timediff'];
    }
}

$update_time_min = min($rating['update_time']);
$update_time_max = max($rating['update_time']);

foreach ($rating['update_time'] as $k => $value)
{
    if ($update_time_max != $update_time_min)
    {
        $rating['update_time'][$k] = ($update_time_max - $value) / ($update_time_max - $update_time_min);
    }
    else
    {
        $rating['update_time'][$k] = 1;
    }
}

// Расчет значения по количеству просмотров профиля

$list_profile_view = $DB->GetAll('
    SELECT
      users.id AS user_id,
      COUNT(DISTINCT(stat.ip)) AS cnt
    FROM
      users
      LEFT OUTER JOIN users_stat AS stat ON
        users.id = stat.user_id
        AND stat.action = "profile_view"
        AND stat.date >= DATE_SUB(CURRENT_DATE, INTERVAL ? DAY)
    GROUP BY users.id
', array($profile_view_days));

// Проверяем ответ от БД и устанавливаем количество просмотров по юзерам
if (!empty($list_profile_view))
{
    foreach ($list_profile_view as $row)
    {
        $rating['profile_view'][$row['user_id']] = $row['cnt'];
    }
}

$profile_view_min = min($rating['profile_view']);
$profile_view_max = max($rating['profile_view']);

foreach ($rating['profile_view'] as $k => $value)
{
    if ($profile_view_min != $profile_view_max)
    {
    	if (($value - $profile_view_min) >= 1)
    	{
        	$rating['profile_view'][$k] = ($profile_view_max - $value) / ($profile_view_max - $profile_view_min);
    	}
    	else
    	{
    		$rating['profile_view'][$k] = 1;
    	}
    }
    else
    {
        $rating['profile_view'][$k] = 1;
    }
}

// Обновление рейтинга

$DB->Execute('UPDATE users SET rating = 0');
$users_ids = $DB->GetCol('SELECT id FROM users WHERE status = 1');
foreach ($users_ids as $user_id)
{
    $calc = 0;
    $calc += isset($rating['update_time'][$user_id]) ? $rating['update_time'][$user_id] * $koeff['update_time'] : 0;
    $calc += isset($rating['profile_view'][$user_id]) ? $rating['profile_view'][$user_id] * $koeff['profile_view'] : 1;
    $calc *= $scale;

    $DB->Execute('UPDATE users SET rating=? WHERE id=?', array($calc, $user_id));
}
