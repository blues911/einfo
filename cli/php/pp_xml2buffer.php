<?php

// --------------------------------------------------------------
// Конвертирование информации из XML-файлов в буферные таблицы
// --------------------------------------------------------------

include_once(PATH_BACKEND . '/php/xml_string_streamer/autoload.php');

// константы
define('UPLOAD_DIR', PATH_PRICES . '/3_xml/');

$g_sql['create_table'] =
  'CREATE TABLE #tbl_name# (
    `id` bigint unsigned NOT NULL AUTO_INCREMENT,
    `category` varchar(255) NOT NULL DEFAULT "",
    `title` varchar(255) NOT NULL DEFAULT "",
    `descr` varchar(255) NOT NULL DEFAULT "",
    `manuf` varchar(64) NOT NULL DEFAULT "",
    `prices` text,
    `mfgdate` year(4) NOT NULL DEFAULT "0000",
    `doc_url` varchar(255) NOT NULL DEFAULT "",
    `comp_url` varchar(255) NOT NULL,
    `action` enum("sale","buy") NOT NULL DEFAULT "sale",
    `stock` int(11) NOT NULL DEFAULT "0",
    `stock_remote` int(11) NOT NULL DEFAULT "0",
    `delivery` tinyint(3) NOT NULL DEFAULT "0",
    PRIMARY KEY (`id`),
      UNIQUE KEY `uniq` (`title`,`category`,`mfgdate`,`action`,`delivery`,`descr`,`manuf`)
  ) ENGINE=MyISAM DEFAULT CHARSET=cp1251'
;

$g_uniq_key = array('title', 'category', 'mfgdate', 'action', 'delivery', 'descr', 'manuf');

function xml_convert($str)
{
    return iconv('utf-8', 'windows-1251', trim($str));
}

//-----------------------------------

if ($dir_handle = opendir(UPLOAD_DIR))
{
    $i = 0;
    while (false !== ($file = readdir($dir_handle)))
    {
        if ($file != '.' && $file != '..' && is_readable(UPLOAD_DIR.$file) && (filesize(UPLOAD_DIR.$file) > 0))
        {
            echo 'Start parse file ' . $file . PHP_EOL;

            // Загрузка XML
            $result = xml2database(UPLOAD_DIR.$file);

            if ($result['error'])
            {
                print PHP_EOL . 'Error ('.$file.'): ' . $result['error_message'] . "\n";
            }
            else
            {
                print PHP_EOL . 'Processed OK (' . $file . ' [total: ' . $result['ok'] . '])' . "\n";

                // Обновление хеша XML файла
                $file_hash = md5_file(UPLOAD_DIR.$file);
                $DB->Execute('UPDATE users SET price_file_hash = ? WHERE id = ?', array($file_hash, $result['user_id']));
            }

            // TODO: убрать после дебага
            if (!file_exists(UPLOAD_DIR.$file))
            {
                $log_data = json_encode(array(
                    'script' => 'xml2buffer.php',
                    'xml_file' => '',
                    'id' => str_replace('.xml', '', end(explode('/', UPLOAD_DIR.$file)))
                ));
                $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                debug_log($debug_msg);
            }
            else
            {
                $log_data = json_encode(array(
                    'script' => 'xml2buffer.php',
                    'xml_file' => UPLOAD_DIR.$file,
                    'id' => str_replace('.xml', '', end(explode('/', UPLOAD_DIR.$file)))
                ));
                $debug_msg = '[' . date('Y-m-d H:i:s') . ']: ' . $log_data . "\n";
                debug_log($debug_msg);
            }
            
            unlink(UPLOAD_DIR.$file);

            $DB->Execute('INSERT INTO cli_log SET user_id = ?, `timestamp` = NOW(), script = ?, ok = ?, fail = ?', array($result['user_id'], 'xml2buffer', $result['ok'], $result['fail']));
        }
    }
    closedir($dir_handle);
}
else 
{
    echo("Невозможно открыть каталог с файлами.\n");
}

// --------------------------------------------

function xml2database($path)
{
    GLOBAL $g_sql, $g_uniq_key, $DB, $DB2;

    if (preg_match('/(\d+)\.xml$/', $path, $matches))
    {
        $file_user_id = $matches[1];
    }
    else
    {
        $result['error'] = true;
        $result['error_message'] = 'Имя файла не соответствует формату.';
        return $result;
    }

    $result = array();
    $result['error'] = false;
    $result['ok'] = 0;
    $result['fail'] = 0;
    $result['user_id'] = 0;

    // Подготавливаем XML String Streamer

    // Создаем потоки для файла
    $stream_header = new Prewk\XmlStringStreamer\Stream\File($path, 8192);
    $stream_item = new Prewk\XmlStringStreamer\Stream\File($path, 8192);

    // Создаем парсеры для header и item
    $parser_header = new Prewk\XmlStringStreamer\Parser\UniqueNode(array('uniqueNode' => 'header'));
    $parser_item = new Prewk\XmlStringStreamer\Parser\UniqueNode(array('uniqueNode' => 'item'));

    // Создаем итераторы для header и item
    $iterator_header = new Prewk\XmlStringStreamer($parser_header, $stream_header);
    $iterator_item = new Prewk\XmlStringStreamer($parser_item, $stream_item);

    $header_node = $iterator_header->getNode();

    if ($header_node = simplexml_load_string($header_node))
    {
        // <uploadtype>
        if (!isset($header_node->uploadtype))
        {
            $result['error'] = true;
            $result['error_message'] = 'Отсутствует тег UPLOADTYPE';
            return $result;
        }
        elseif (!in_array($header_node->uploadtype, array('add', 'replace', 'update')))
        {
            $result['error'] = true;
            $result['error_message'] = 'Тег UPLOADTYPE содержит некорректное значение';
            return $result;
        }

        // <status>
        if (!isset($header_node->status))
        {
            $result['error'] = true;
            $result['error_message'] = 'Отсутствует тег STATUS';
            return $result;
        }
        elseif (!in_array($header_node->status, array('stock', 'stock_hide', 'delivery', 'all')))
        {
            $result['error'] = true;
            $result['error_message'] = 'Тег STATUS содержит некорректное значение';
            return $result;
        }

        $row = $DB->GetRow('SELECT id, MD5(CONCAT(login, `password`)) AS `hash` FROM users WHERE id = ?', array($file_user_id));
        $user_id = $row['id'];
        $result['user_id'] = $user_id;

        if ($user_id !== false)
        {
            $user_tbl = DB_USERS_PREFIX . $row['hash'];

            $user_price_limit = $DB->GetOne('SELECT price_limit FROM users WHERE id=?', array($user_id));

            // проверка на наличие таблицы клиента в БД, если нету создаем
            $user_tbls = $DB2->GetCol('SHOW TABLES LIKE "' . DB_USERS_PREFIX . '%"');
            if (!in_array($user_tbl, $user_tbls))
            {
                // создание буферной таблицы
                $sql = preg_replace('/#tbl_name#/su', $user_tbl, $g_sql['create_table']);
                $DB2->Execute($sql);
            }

            if ($header_node->uploadtype == 'replace')
            {
                $DB2->Execute('DELETE FROM ' . $user_tbl);
            }

            ///////////////////////////////

            // формирование SQL-запросов
            $n = 0;

            $chunk = array('rows' => array(), 'cnt' => 0);
            while ($item = $iterator_item->getNode())
            {
                $item = filter_var($item, FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW);
                $item = simplexml_load_string($item);

                if ($n > $user_price_limit)
                {
                    break;
                }

                $sql = '';
                $record = array();

                // $record - массив (key: название столбца в БД, value: значение)
                if (isset($item->title))
                {
                    $record['title'] = $item->title;
                }
                else
                {
                    continue;
                }

                $record['descr'] = (isset($item->description)) ? $item->description : '';
                $record['manuf'] = (isset($item->manufacture)) ? $item->manufacture : '';

                $record['category'] = '';
                $record['action'] = 'sale';
                
                $record['prices'] = array();

                if (isset($item->prices))
                {
                    foreach ($item->prices->price as $p)
                    {
                        $attr = $p->attributes();
    
                        $record['prices'][] = array(
                            'n' => (string)$attr['n'],
                            'c' => strtolower((string)$attr['c']),
                            'p' => (string)$p
                        );
                    }
                }

                $record['prices'] = json_encode($record['prices']);

                $record['mfgdate'] = (isset($item->mfgdate) && $item->mfgdate != "0") ? $item->mfgdate : '0000';
                $record['doc_url'] = (isset($item->docs)) ? $item->docs : '';
                $record['comp_url'] = (isset($item->comp_url)) ? $item->comp_url : '';
                $record['stock_remote'] = (isset($item->stock_remote)) ? (int)$item->stock_remote : 0;

                switch ($header_node->status)
                {
                    case 'stock':
                        if (empty($item->stock) && empty($item->delivery))
                        {
                            $record['stock'] = 0;
                            $record['delivery'] = -1;
                        }
                        else
                        {
                            $record['stock'] = (isset($item->stock) && $item->stock >= -1) ? $item->stock : 0;
                            $record['delivery'] = (isset($item->delivery) && $item->delivery >= -1) ? $item->delivery : 0;
                        }
                    break;

                    case 'stock_hide':
                        if (empty($item->stock) && empty($item->delivery))
                        {
                            $record['stock'] = 0;
                            $record['delivery'] = -1;
                        }
                        else
                        {
                            $record['stock'] = -1;
                            $record['delivery'] = 0;
                        }
                    break;

                    case 'delivery':
                        $record['stock'] = 0;
                        $record['delivery'] = (isset($item->delivery) && $item->delivery > 0) ? $item->delivery : -1;
                    break;

                    case 'all':
                        $record['stock'] = (isset($item->stock) && $item->stock >= -1) ? $item->stock : 0;
                        $record['delivery'] = (isset($item->delivery) && $item->delivery >= -1) ? $item->delivery : 0;
                    break;
                }

                // реакция на action
                if ($header_node->uploadtype == 'add' || $header_node->uploadtype == 'replace')
                {
                    $chunk['rows'][] = $record;
                    $chunk['cnt']++;
                    $n++;

                    if ($chunk['cnt'] >= 1000)
                    {
                        sleep_overload();
                        $rows_str = array();

                        foreach ($chunk['rows'] as $row)
                        {
                            foreach (array_keys($row) as $k)
                            {
                                $row[$k] = $DB2->qstr(clean_data($row[$k]));
                            }

                            $rows_str[] = '(' . implode(',', array($row['category'], $row['title'], $row['descr'], $row['manuf'], $row['prices'], $row['mfgdate'], $row['doc_url'], $row['comp_url'],
                                    $row['action'], $row['stock'], $row['stock_remote'], $row['delivery'])) . ')';
                        }

                        $sql = 'INSERT IGNORE INTO ' . $user_tbl . ' (`category`, `title`, `descr`, `manuf`, `prices`, `mfgdate`, `doc_url`, `comp_url`, `action`, `stock`, `stock_remote`, `delivery`) VALUES ';
                        $sql .= implode(',', $rows_str);

                        unset($rows_str);
                        $chunk['rows'] = array();
                        $chunk['cnt'] = 0;
                    }
                }
                elseif ($header_node->uploadtype == 'update')
                {
                    $sql = 'UPDATE ' . $user_tbl . ' SET ';
                    $updates = array();
                    $conditions = array();

                    foreach ($record as $k => $v)
                    {
                        if (in_array($k, $g_uniq_key))
                        {
                            $conditions[] = $k . '=' . $DB2->qstr(clean_data($v));
                        }
                        else
                        {
                            $updates[] = $k . '=' . $DB->qstr(clean_data($v));
                        }
                    }

                    $sql .= implode(', ', $updates) . ' WHERE ' . implode(' AND ', $conditions);
                    $n++;
                }

                if (!($n % 100))
                {
                    echo '.';
                }

                if (!($n % 1000))
                {
                    echo '|';
                }

                if (!($n % 10000))
                {
                    echo PHP_EOL;
                }

                // exec SQL
                if ($sql != '')
                {
                    $DB2->Execute($sql);
                }
            }

            $result['ok'] = $n;

            if ($chunk['cnt'] > 0)
            {
                sleep_overload();
                $rows_str = array();

                foreach ($chunk['rows'] as $row)
                {
                    foreach (array_keys($row) as $k)
                    {
                        $row[$k] = $DB2->qstr(clean_data($row[$k]));
                    }

                    $rows_str[] = '(' . implode(',', array($row['category'], $row['title'], $row['descr'], $row['manuf'], $row['prices'], $row['mfgdate'], $row['doc_url'], $row['comp_url'],
                            $row['action'], $row['stock'], $row['stock_remote'], $row['delivery'])) . ')';
                }

                $sql = 'INSERT IGNORE INTO ' . $user_tbl . ' (`category`,`title`,`descr`,`manuf`,`prices`,`mfgdate`,`doc_url`,`comp_url`,`action`,`stock`,`stock_remote`,`delivery`) VALUES ';
                $sql .= implode(',', $rows_str);

                unset($rows_str);

                $chunk['rows'] = array();
                $chunk['cnt'] = 0;

                $DB2->Execute($sql);
            }

            // Проверяем тип поля с ID и изменяем при необходимости
            $type = $DB2->GetOne('SELECT DATA_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = "e2_users" AND TABLE_NAME = "' . $user_tbl . '" AND COLUMN_NAME = "id"');
            if ($type == 'int')
            {
                $DB2->Execute('ALTER TABLE ' . $user_tbl . ' CHANGE `id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT');
            }

            // Закрываем буферную таблицу
            $DB2->Execute('FLUSH TABLE ' . DB_USERS_NAME . '.' .$user_tbl);
        }
        else
        {
            $result['error'] = true;
            $result['error_message'] = 'Указанный пользователь не найден в БД';
            return $result;
        }
    }
    else
    {
        $result['error'] = true;
        $result['error_message'] = 'Невозможно загрузить XML файл';
    }

    return $result;
}

function clean_data($data)
{
    return trim(strip_tags($data));
}


