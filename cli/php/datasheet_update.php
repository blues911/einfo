<?php

include_once(PATH_BACKEND . '/php/classes/class.PdfUpdate.php');
include_once(PATH_BACKEND . '/php/classes/class.PdfParse.php');

echo('Datasheets import start...' . PHP_EOL);

// -------------------

// Количество дней последного обновления файлов pdf
$update_days_limit = 30;

// Время ожидания ответа удаленного сервера в секундах
$socket_timeout = 10;

$pdf_update = new PdfUpdate($DB);
$pdf_parse = new PdfParse($DB);

// Количество дней последного обновления файлов pdf
$pdf_update->set_update_days_limit($update_days_limit);

// Удаление записей и файлов с пустым remote
$empty_datasheet = $DB->GetAll('SELECT `comp_canon`, `local`, `img` FROM datasheet WHERE `remote` = "" OR `remote` IS NULL');
foreach ($empty_datasheet as $ds)
{
    $pdf_update->delete_pdf($ds['local']);
    $pdf_parse->delete_img($ds['img']);
    $DB->Execute('DELETE FROM datasheet WHERE comp_canon = ?', array($ds['comp_canon']));
    echo('Datasheet removed - ' . $ds['comp_canon'] . PHP_EOL);
}

// Получение списка компонентов для проверки pdf
$datasheet_list = $pdf_update->get_list();

ini_set('default_socket_timeout', $socket_timeout);

foreach($datasheet_list as $datasheet)
{
    // Статус файла
    $file_status = $pdf_update->get_remote_file_status($datasheet['remote']);

    // Если файл на удаленном сервере доступен
    if(!empty($file_status) && $file_status['header'] == 200)
    {
        // Удаление старого файла
        if(!empty($datasheet['local']))
        {
            $pdf_update->delete_file($datasheet['comp_canon']);
        }

        // Загрузка файла с удаленного сервера
        $file_name = $pdf_update->download_file($datasheet['remote']);

        if (!empty($file_name))
        {
            // Обновление имени файла
            $pdf_update->set_file_name($datasheet['comp_canon'], $file_name);
        }

        echo('Datasheet updated - ' . $datasheet['comp_canon'] . PHP_EOL);

    }
    // Если локальная версия такая же как удаленная, то обновляем поле pdf_update (дата актуализации файла)
    else if (!empty($file_status['last_modified']) && ($datasheet['update'] >= $file_status['last_modified']))
    {
        $pdf_update->set_pdf_update($datasheet['comp_canon']);

        // Записываем в лог
        echo('Datasheet does not require update - ' . $datasheet['comp_canon'] . PHP_EOL);
    }
    else if (!empty($file_status) && $file_status['header'] == 403)
    {
        echo('403 downloading error - ' . $datasheet['comp_canon'] . PHP_EOL);
    }
    else
    {
        echo('Unknown downloading error - ' . $datasheet['comp_canon'] . PHP_EOL);
    }

}