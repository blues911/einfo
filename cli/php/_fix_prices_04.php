<?php

// Правки формата цен e2_main.orders_items, перенос roz, roz_cur в prices.

echo "Правки " . DB_MAIN_NAME . ".orders_items \n";

$rs = $DB->Execute('SELECT id, roz, roz_cur FROM orders_items');

while(!$rs->EOF)
{
    $id = $rs->fields('id');
    $prices = json_encode(array(array('n' => '1', 'c' => $rs->fields('roz_cur'), 'p' => $rs->fields('roz'))));
    
    $DB->Execute('UPDATE orders_items SET prices = ? WHERE id = ?', array($prices, $id));

    $rs->MoveNext();
}