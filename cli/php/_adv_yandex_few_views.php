<?php

/*
 * Скрипт для получения списка редких ключевиков с возможностью их удаления из БД.
 * Удаление происходит в режиме «work», режим «test» только выводит список в консоль
 *
 * Использование: php cli.php _adv_yandex_few_views <id категории> <режим получения>
 * Например php cli.php _adv_yandex_few_views 6358 test
 */

define('ADV_YANDEX_REQUEST_CHUNK', 1); // Количество ID в одном запросе.
define('ADV_YANDEX_MIN_UNITS', 5000); // Минимальное кол-во баллов для работы Яндекс.Директ.

// ---------------------------------------------------------------

$category_id = isset($argv[2]) && is_numeric($argv[2]) ? (int)$argv[2] : false;
$mode = isset($argv[3]) && in_array($argv[3], array('test', 'work')) ? $argv[3] : false;

// Если не указан один из параметров
if (empty($category_id) || empty($mode))
{
    echo 'Required parameters wrong or missed.' . PHP_EOL;
    echo 'Script stopped.' . PHP_EOL;
    goto end;
}

include_once(PATH_BACKEND . '/php/classes/class.AdvYandex.php');

$DB->Execute('SET NAMES utf8');
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;

// Инициализация
$yandex = new AdvYandex($DB);

// Проверяем наличие категории
$category_list = $DB->GetCol(
                    'SELECT child.id '
                    . 'FROM categories AS parent '
                    . 'LEFT JOIN categories AS child ON (child.c_left >= parent.c_left AND child.c_right <= parent.c_right)'
                    . 'WHERE parent.id = ?',
                    array($category_id)
                    );

// Если категория не найдена
if (empty($category_list))
{
    echo 'Category not found.' . PHP_EOL;
    echo 'Script stopped.' . PHP_EOL;
    goto end;
}

try
{
    // Проверка токена
    $set_token = $yandex->get_token();

    if (!empty($set_token))
    {
        //Получение настроек
        $yandex->get_settings();

        // Проверка количества баллов
        $units = $yandex->get_units();

        $banners_id = array();

        if ($units <= ADV_YANDEX_MIN_UNITS)
        {
            echo('The number of remaining points - ' . (int)$units . PHP_EOL);
            echo('Script stopped.' . PHP_EOL);
        }
        else
        {
            // Получаем количество подходящих компаний
            $total_count = $DB->GetOne(
                              'SELECT COUNT(DISTINCT(adv.campaign_id)) '
                              . 'FROM adv_yandex AS adv '
                              . 'LEFT JOIN catalog_comp_canon AS cat ON (cat.comp_canon = adv.comp_canon) '
                              . 'WHERE '
                              . 'cat.category_id IN (' . implode(', ', $category_list) . ') '
                              . 'AND phrase_id IS NOT NULL'
            );

            // Если не найдено компаний для категории
            if (empty($total_count))
            {
                echo 'Companies in category not found.' . PHP_EOL;
                echo 'Script stopped.' . PHP_EOL;
                goto end;
            }

            // Вычисляем количество проходов
            $max_iteration = ceil($total_count/ADV_YANDEX_REQUEST_CHUNK);

            for ($i = 0; $i < $max_iteration; $i++)
            {
                // Проверка количества баллов
                if ($yandex->get_units() <= ADV_YANDEX_MIN_UNITS)
                {
                    break;
                }

                $_offset = $i * ADV_YANDEX_REQUEST_CHUNK;
                $offset = $_offset > 0 ? $_offset + 1 : 0;

                $adv_campaigns = $DB->GetCol(
                                    'SELECT DISTINCT(campaign_id) '
                                    . 'FROM adv_yandex AS adv '
                                    . 'LEFT JOIN catalog_comp_canon AS cat ON (cat.comp_canon = adv.comp_canon) '
                                    . 'WHERE '
                                    . 'cat.category_id IN (' . implode(', ', $category_list) . ') '
                                    . 'AND phrase_id IS NOT NULL '
                                    . 'LIMIT ' . $offset . ', ' . ADV_YANDEX_REQUEST_CHUNK
                );

                $campaigns = array();
                foreach ($adv_campaigns as $campaign)
                {
                    $campaigns[] = $campaign;
                }

                // Делаем запрос к API Yandex
                $result = $yandex->get_rarely_group_ids($campaigns);

                $result = array_chunk($result, 1000);

                foreach ($result as $chunk)
                {
                    $_banner_ids = $yandex->get_rarely_banner_ids($chunk);

                    $banners_id = array_merge($banners_id, $_banner_ids);
                }
            }
        }

        if (!empty($banners_id))
        {
            $comp_canon_delete = $DB->GetCol('SELECT cat.comp_canon
                                                FROM catalog_comp_canon cat
                                                    INNER JOIN adv_yandex adv ON adv.comp_canon = cat.comp_canon
                                                WHERE cat.category_id IN (' . implode(', ', $category_list) . ')
                                                    AND adv.banner_id IN (' . implode(', ', $banners_id) . ')');

            if (!empty($comp_canon_delete))
            {
                foreach ($comp_canon_delete as $k => $comp_canon)
                {
                    echo $comp_canon;

                    // В рабочем режиме удаляем редкие ключевики из базы
                    if ($mode == 'work')
                    {
                        $DB->Execute('DELETE FROM catalog_comp_canon WHERE comp_canon = ?', array($comp_canon));
                        echo ' -> DELETED';
                    }
                    else
                    {
                        echo ' -> TEST MODE';
                    }

                    echo PHP_EOL;
                }

            }
        }
    }
}
catch (Exception $e)
{
    echo PHP_EOL . 'Error: ' . $e->getMessage() . PHP_EOL . PHP_EOL;
}

echo PHP_EOL . 'Units - ' . $yandex->get_units() . ';' . PHP_EOL;

end: