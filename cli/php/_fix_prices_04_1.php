<?php

// Правки формата цен e2_main.orders_items, перенос opt, opt_cur в prices.

echo "Правки " . DB_MAIN_NAME . ".orders_items \n";

$rs = $DB->Execute('SELECT id, opt, opt_cur FROM orders_items WHERE roz = 0 AND opt > 0');

while(!$rs->EOF)
{
    $id = $rs->fields('id');
    $prices = json_encode(array(array('n' => '1', 'c' => $rs->fields('opt_cur'), 'p' => $rs->fields('opt'))));
    
    $DB->Execute('UPDATE orders_items SET prices = ? WHERE id = ?', array($prices, $id));

    $rs->MoveNext();
}