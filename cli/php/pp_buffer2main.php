<?php

// --------------------------------------------------------------
// Конвертирование пользовательской (буферной) базы в основную
// с использованием временных таблиц для последовательного insert и update
// --------------------------------------------------------------

// Таймаут с момента последнего изменения буфферной таблицы.
$buffer_table_timeout = 10;

sleep(1);

include_once(PATH_BACKEND . '/php/classes/class.phpmailer.php');
include_once (PATH_FRONTEND . '/php/classes/class.Search.php');
include_once (PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

$sphinxRT = new SearchSphinxQL();

$update_flag = false;

// Создаем третью сессию для блокировки буфферных таблиц (READ - разрешить другим читать, но запретить изменять)
$DB3 = ADONewConnection(DB_USERS_DSN);
$DB3->SetFetchMode(ADODB_FETCH_ASSOC);
$DB3->Execute('SET NAMES utf8');
$DB3->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB3->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB3->cacheSecs = DB_CACHE_TIME;

// --------------------------------------------------

$last_buff_update = $DB2->GetOne('SELECT value FROM ' . DB_MAIN_NAME . '.settings WHERE var="last_buff_update"');
$DB2->Execute('UPDATE ' . DB_MAIN_NAME . '.settings SET VALUE = DATE_SUB(NOW(), INTERVAL ' . $buffer_table_timeout . ' SECOND) WHERE var="last_buff_update"');

$buff_updated_hash_query = $DB2->GetAll('
    SELECT
      SUBSTRING(info.TABLE_NAME, 6) AS HASH_TABLE,
      info.UPDATE_TIME
    FROM
      information_schema.TABLES AS info
    WHERE
      info.TABLE_SCHEMA = ?
      AND info.TABLE_NAME LIKE "dist_%"
      AND info.UPDATE_TIME > ?
      AND info.UPDATE_TIME <= DATE_SUB(NOW(), INTERVAL ' . $buffer_table_timeout . ' SECOND)
', array(DB_USERS_NAME, $last_buff_update));

$buff_updated_hash = array();

if (!empty($buff_updated_hash_query))
{
    foreach ($buff_updated_hash_query as $_hash)
    {
        $buff_updated_hash[$_hash['HASH_TABLE']] = $_hash['UPDATE_TIME'];
    }
}

unset($buff_updated_hash_query);

$buff_updated = array();
if ($buff_updated_hash)
{
    $buff_updated = $DB2->GetAll('
        SELECT
          users.id,
          MD5(CONCAT(users.login, users.password)) AS hash_tbl,
          users.title,
          users.status,
          users.rating,
          users.individual_buyers,
          users.price_date,
          users.country_id,
          country.country,
          users.city_id,
          city.city,
          users.email_price_notification,
          users.email_notification
        FROM
          ' . DB_MAIN_NAME . '.users
          LEFT OUTER JOIN ' . DB_MAIN_NAME . '.ip2ruscity_countries AS country ON users.country_id = country.country_id
          LEFT OUTER JOIN ' . DB_MAIN_NAME . '.ip2ruscity_cities AS city ON users.city_id = city.city_id
        WHERE
          MD5(CONCAT(login, password)) IN ("' .implode('","', array_keys($buff_updated_hash)) . '")
          AND users.status = 1
          AND users.id NOT IN (?, ?)
    ', array(USER_ADMIN_ID, USER_PARTNER_ID));
}

$base_table_name = DB_MAIN_NAME . '.base_main';

if (!empty($buff_updated))
{
    $update_flag = true;
}

foreach ($buff_updated as $k => $user)
{
    $cnt = array('ok' => 0, 'fail' => 0, 'delete' => 0, 'insert' => 0, 'update' => 0);

    echo('User ID: ' . $user['id'] . PHP_EOL);

    $price_limit = $DB2->GetOne('SELECT price_limit FROM ' . DB_MAIN_NAME . '.users WHERE id=?', array($user['id']));

    $buffer_table_name = DB_USERS_NAME . '.dist_' . $user['hash_tbl'];
    $hash_table_name = $buffer_table_name . '_hash';

    // Блокируем буфферную таблицу
    $DB3->Execute('LOCK TABLES ' . $buffer_table_name . ' READ');

    // Создаем временную таблицу с хешами
    $DB2->Execute('DROP TEMPORARY TABLE IF EXISTS ' . $hash_table_name);
    $DB2->Execute(
        'CREATE TEMPORARY TABLE ' . $hash_table_name . '(
          `hash` VARCHAR(32) NOT NULL,
          `id` BIGINT(20) UNSIGNED NOT NULL,
          PRIMARY KEY (`hash`)
        ) ENGINE=MEMORY CHARSET=cp1251 COLLATE=cp1251_general_ci'
    );

    // Заполняем таблицу хешами
    sleep_overload();
    $DB2->Execute(
        'INSERT IGNORE INTO ' . $hash_table_name . ' (`hash`, `id`)
        SELECT MD5(CONCAT(`title`, `descr`, `manuf`, `mfgdate`, `doc_url`, `comp_url`)) AS `hash`, `id`
        FROM ' . $buffer_table_name
    );

    // Удаляем позиции из base_main, которых нет в буферной таблице.

    $delete_rs = $DB2->Execute('
        SELECT
          main.hash,
          main.id
        FROM
          ' . $base_table_name . ' AS main
          LEFT OUTER JOIN ' . $hash_table_name . ' AS hash_table ON main.hash = hash_table.hash
        WHERE
          hash_table.hash IS NULL
          AND main.user_id = ?
    ', array($user['id']));

    if (!empty($delete_rs))
    {
        $chunk = array('hash' => array(), 'cnt' => 0);

        while (!$delete_rs->EOF)
        {
            $chunk['hash'][] = $DB->qstr($delete_rs->fields('hash'));
            $chunk['cnt']++;

            if ($chunk['cnt'] >= 1000)
            {
                sleep_overload();
                $delete_result = $DB->Execute(
                    'DELETE LOW_PRIORITY FROM ' . $base_table_name . ' WHERE user_id = ? AND `hash` IN (' . implode(',', $chunk['hash']) . ')',
                    array($user['id'])
                );

                if (!empty($delete_result))
                {
                    $cnt['ok'] += $chunk['cnt'];
                    $cnt['delete'] += $chunk['cnt'];
                }
                else
                {
                    $cnt['fail'] += $chunk['cnt'];
                }

                $chunk['hash'] = array();
                $chunk['cnt'] = 0;
            }

            $sphinxRT->delete_rt_index($delete_rs->fields('id'));

            $delete_rs->MoveNext();
        }

        if ($chunk['cnt'] > 0)
        {
            sleep_overload();
            $delete_result = $DB->Execute(
                'DELETE LOW_PRIORITY FROM ' . $base_table_name . ' WHERE user_id = ? AND `hash` IN (' . implode(',', $chunk['hash']) . ')',
                array($user['id'])
            );

            $sphinxRT->delete_rt_index($delete_rs->fields('id'));

            if (!empty($delete_result))
            {
                $cnt['ok'] += $chunk['cnt'];
                $cnt['delete'] += $chunk['cnt'];
            }
            else
            {
                $cnt['fail'] += $chunk['cnt'];
            }

            $chunk['hash'] = array();
            $chunk['cnt'] = 0;
        }
    }

    // Вставляем позиции в base_main, которых нет в буферной таблице.

    $insert_rs = $DB2->Execute('
        SELECT
          hash_table.hash AS `hash`,
          ? AS user_id,
          "" AS atoms,
          "" AS atoms2,
          buf.title AS comp_title,
          buf.manuf AS manuf_title,
          buf.descr AS descr,
          buf.prices AS prices,
          buf.mfgdate AS mfgdate,
          buf.doc_url AS doc_url,
          buf.comp_url AS comp_url,
          buf.stock AS stock,
          buf.stock_remote AS stock_remote,
          buf.delivery AS delivery
        FROM
          ' . $hash_table_name . ' AS hash_table
          INNER JOIN ' . $buffer_table_name . ' AS buf ON hash_table.id = buf.id
          LEFT OUTER JOIN ' . $base_table_name . ' AS main ON hash_table.hash = main.hash AND main.user_id = ?
        WHERE
          main.hash IS NULL',
        array($user['id'], $user['id'])
    );

    $start_id = $DB->GetOne('SELECT MAX(id) + 1 FROM base_main');

    if (!empty($insert_rs))
    {
        $chunk = array('rows' => array(), 'cnt' => 0);
        while (!$insert_rs->EOF)
        {
            $atoms = get_request_atoms($insert_rs->fields('comp_title'));
            $atoms = !empty($atoms) ? implode(' ', $atoms) . ' ' : '';

            $atoms2 = get_request_atoms_2($insert_rs->fields('comp_title'));
            $atoms2 = !empty($atoms2) ? implode(' ', $atoms2) . ' ' : '';

            if(!empty($atoms))
            {
                $_title = clean_quotes($insert_rs->fields('comp_title'));

                $_row = array(
                    $insert_rs->fields('hash'),
                    $insert_rs->fields('user_id'),
                    $atoms,
                    $atoms2,
                    $_title,
                    $insert_rs->fields('manuf_title'),
                    $insert_rs->fields('descr'),
                    $insert_rs->fields('prices'),
                    $insert_rs->fields('mfgdate'),
                    $insert_rs->fields('doc_url'),
                    $insert_rs->fields('comp_url'),
                    $insert_rs->fields('stock'),
                    $insert_rs->fields('stock_remote'),
                    $insert_rs->fields('delivery')
                );

                foreach (array_keys($_row) as $k)
                {
                    $_row[$k] = $DB2->qstr($_row[$k]);
                }

                $chunk['rows'][] = '(' . implode(',', $_row) . ')';
                $chunk['cnt']++;

                $cnt['ok']++;
                $cnt['insert']++;

                if ($chunk['cnt'] >= 1000)
                {
                    sleep_overload();
                    $query = 'INSERT LOW_PRIORITY INTO ' . $base_table_name . ' (hash,user_id,atoms,atoms2,comp_title,manuf_title,descr,prices,mfgdate,doc_url,comp_url,stock,stock_remote,delivery) ';
                    $query .= 'VALUES ' . implode(',', $chunk['rows']);

                    $DB->Execute($query);

                    unset($query);
                    $chunk['rows'] = array();
                    $chunk['cnt'] = 0;
                }
            }

            $insert_rs->MoveNext();
        }

        if ($chunk['cnt'] > 0)
        {
            sleep_overload();
            $query = 'INSERT LOW_PRIORITY INTO ' . $base_table_name . ' (hash,user_id,atoms,atoms2,comp_title,manuf_title,descr,prices,mfgdate,doc_url,comp_url,stock,stock_remote,delivery) ';
            $query .= 'VALUES ' . implode(',', $chunk['rows']);

            $DB->Execute($query);

            unset($query);
            $chunk['rows'] = array();
            $chunk['cnt'] = 0;
        }
    }

    $end_id = $DB->GetOne('SELECT MAX(id) FROM base_main');

    if ($end_id >= $start_id)
    {
        // Получаем данные для RT индексов
        $insert_rt = $DB->Execute('SELECT id, user_id, atoms2 FROM base_main WHERE id BETWEEN ? AND ?', array($start_id, $end_id));

        if(!empty($insert_rt))
        {
            // Производим вставку RT индексов
            while (!$insert_rt->EOF)
            {
                $sphinxRT->add_rt_index($insert_rt->fields('id'), $insert_rt->fields('user_id'), $insert_rt->fields('atoms2'));
                $insert_rt->MoveNext();
            }
        }
    }

    // Обновляем позиции в base_main

    $update_rs = $DB2->Execute(
        'SELECT
          main.`hash`,
          buf.prices,
          buf.stock,
          buf.stock_remote,
          buf.delivery
        FROM
          ' . $base_table_name . ' AS main
          INNER JOIN ' . $hash_table_name . ' AS hash_table ON main.hash = hash_table.hash
          INNER JOIN ' . $buffer_table_name . ' AS buf ON hash_table.id = buf.id
        WHERE
          main.user_id = ? AND
          (
              STRCMP(main.prices, buf.prices) OR
              main.stock != buf.stock OR
              main.delivery != buf.delivery
          )',
        array($user['id'])
    );

    if(!empty($update_rs))
    {
        while (!$update_rs->EOF)
        {
            $update_result = $DB2->Execute('
                UPDATE LOW_PRIORITY
                  ' . $base_table_name . '
                SET
                  prices = ?,
                  stock = ?,
                  stock_remote = ?,
                  delivery = ?
                WHERE
                  `hash` = ?
                  AND user_id = ?
                ', array(
                    $update_rs->fields('prices'),
                    $update_rs->fields('stock'),
                    $update_rs->fields('stock_remote'),
                    $update_rs->fields('delivery'),
                    $update_rs->fields('hash'),
                    $user['id']
                )
            );

            if (!empty($update_result))
            {
                $cnt['ok']++;
                $cnt['update']++;
            }
            else
            {
                $cnt['fail']++;
            }

            $update_rs->MoveNext();
        }
    }

    // Снимаем блокировку с буфферной таблицы
    $DB3->Execute('UNLOCK TABLES');

    // Удаляем временную таблицу с хешами
    $DB2->Execute('DROP TEMPORARY TABLE IF EXISTS ' . $hash_table_name);

    // debug
    $DB->Execute('INSERT INTO cli_log SET user_id = ?, `timestamp` = NOW(), script = ?, ok = ?, fail = ?', array($user['id'], 'buffer2main', $cnt['ok'], $cnt['fail']));
    echo('ROWS - OK: ' . $cnt['ok'] . ' (DEL: ' . $cnt['delete'] . ', INS: ' . $cnt['insert'] . ', UPD: ' . $cnt['update'] . '); FAIL: ' . $cnt['fail'] . PHP_EOL);

    // Обновляем price_date у пользователя
    $DB->Execute('UPDATE ' . DB_MAIN_NAME . '.users SET price_date=NOW() WHERE id=?', array($user['id']));

    // Обновляем счетчик поставщика
    $user_price_cnt = $DB->GetOne('SELECT COUNT(id) FROM ' . $base_table_name . ' WHERE user_id = ?', array($user['id']));
    $DB->Execute('UPDATE ' . DB_MAIN_NAME . '.users SET cnt = ? WHERE id = ?', array($user_price_cnt, $user['id']));

    // Отправляем письмо
    echo('Mail -> ');
    if ($user['email_notification'])
    {
        $mail = new PHPMailer();
        $mail->CharSet = 'utf-8';
        $mail->Sender = 'admin@einfo.ru';
        $mail->SetFrom('admin@einfo.ru', 'Einfo');
        $mail->Subject = '[einfo.ru] Прайс обработан';
        $mail->IsHTML();

        $email_to = preg_split("/[,;\s]/u", $user['email_price_notification'], -1, PREG_SPLIT_NO_EMPTY);
        foreach ($email_to as $value)
        {
            $mail->AddAddress(trim($value));
        }

        $smarty->assign('user', $user);
        $smarty->assign('cnt', $user_price_cnt);
        $smarty->assign('date', date('d.m.Y G:i'));
        $mail->Body = $smarty->fetch('mail_buffer2main_complete.tpl');

        if ($mail->Send())
        {
            echo 'OK';
        }
        else
        {
            echo 'FAIL';
        }
    }
    else
    {
        echo 'NO';
    }

    // Закрываем буферную таблицу
    $DB2->Execute('FLUSH TABLE ' . $buffer_table_name);
    
	echo PHP_EOL . PHP_EOL;
}

if ($update_flag)
{
    // Выводим метку необходимости обновить индексы Sphinx
    echo(PHP_EOL . 'base_main_updated' . PHP_EOL);
}