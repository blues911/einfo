<?php

/*
 * Скрипт для экспорта прайс листа поставщика в Json формате
 *
 * В текстовом файле каждое предложение с новой строки
 *
 * Использование: php cli.php _export_price_json <id поставщика> <путь до файла>
 */

$user_id = isset($argv[2]) ? $argv[2] : false;
$new_file_name = isset($argv[3]) ? $argv[3] : false;

if (!$user_id)
{
    echo('Не указан id поставщика');
}
else if (!$new_file_name)
{
    echo('Не указано имя файла');
}
else
{
    $buffer_table = $DB->GetOne("SELECT CONCAT('dist_', MD5(CONCAT(login, `password`))) AS buffer_table FROM users WHERE id = ?", array($user_id));

    if (!empty($buffer_table))
    {
        $count = $DB->GetOne('SELECT COUNT(*) FROM e2_users.' . $buffer_table);
        $count_per_query = 100000;
        $queries_count = ceil($count / $count_per_query);

        if ($queries_count > 0)
        {
            $file = fopen($new_file_name, 'w');

            for ($i = 0; $i < $queries_count; $i++)
            {
                $limit_start = $i * $count_per_query;
                $limit_end = $count_per_query;

                $components = $DB->GetAll('
                    SELECT
                        title,
                        category,
                        descr,
                        manuf,
                        opt AS price_opt,
                        opt_cur AS price_opt_cur,
                        roz AS price_roz,
                        roz_cur AS price_roz_cur,
                        mfgdate,
                        doc_url,
                        comp_url,
                        stock,
                        delivery
                    FROM
                        e2_users.' . $buffer_table . '
                    LIMIT ' . $limit_start . ', ' . $limit_end . '
                ');

                foreach ($components as $component)
                {
                    fwrite($file, json_encode($component) . PHP_EOL);
                }
            }

            fclose($file);

            // Закрываем буферную таблицу
            $DB->Execute('FLUSH TABLE ' . DB_USERS_NAME . '.' . $buffer_table);
        }
        else
        {
            echo('У поставщика нет загруженных предложений');
        }
    }
    else
    {
        echo('Поставщик с таким id не найден');
    }
}