<?php

include_once (PATH_FRONTEND . '/php/classes/class.Search.php');
include_once (PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

$sphinxRT = new SearchSphinxQL();

//$rs = $DB->Execute('SELECT id, comp_title FROM base_main WHERE atoms = "" OR atoms IS NULL');
$rs = $DB->Execute('SELECT id, user_id, comp_title FROM base_main');

$i = 0;
while(!$rs->EOF)
{
    $i++;

    $id = $rs->fields('id');
    $user_id = $rs->fields('user_id');
    $title = $rs->fields('comp_title');
    $atoms = get_request_atoms($title);
    $atoms2 = get_request_atoms_2($title);

    $atoms = !empty($atoms) ? implode(' ', $atoms) . ' ' : '';
    $atoms2 = !empty($atoms2) ? implode(' ', $atoms2) . ' ' : '';

    if (!empty($atoms))
    {
        $DB->Execute('UPDATE base_main SET atoms = ?, atoms2 = ? WHERE id = ?', array($atoms, $atoms2, $id));

        // Обновляем RT индекс
        $sphinxRT->edit_rt_index($id, $user_id, $atoms2);
    }
    else
    {
        $DB->Execute('DELETE FROM base_main WHERE id = ?', array($id));

        // Удаляем RT индекс
        $sphinxRT->delete_rt_index($id);
    }


    if (($i % 1000) == 0)
    {
        echo '.';
    }
    if (($i % 100000) == 0)
    {
        echo PHP_EOL;
    }

    $rs->MoveNext();
}

echo PHP_EOL . 'TOTAL: ' . $i . PHP_EOL . PHP_EOL;