<?php

// --------------------------------------------------------------
// Получение курсов валют online
// --------------------------------------------------------------


setlocale (LC_COLLATE, array ('ru_RU', 'rus_RUS', 'ru_RU.cp1251', 'ru_RU.CP1251', 'rus_RUS.1251', 'Russian_Russia.1251'));

$url = "http://cbr.ru/scripts/XML_daily.asp";

// --------------------------------------------
$xml = @simplexml_load_file($url);

if($xml)
{
    foreach($xml->Valute as $data)
    {
        if ($data->NumCode == 840)
        {
            $new_usd = floatval(strtr($data->Value,",","."));
            $curr_usd = floatval($DB->GetOne("SELECT value from settings where var = 'cur_rates:usd'"));
            if($new_usd/$curr_usd > 0.5 && $new_usd/$curr_usd < 1.5)
            {
                $record['value'] = $new_usd;
                $DB->AutoExecute("settings",$record,"UPDATE","var='cur_rates:usd'");
                echo $data->CharCode." - ".$new_usd." руб\n";
            }
        }
        if ($data->NumCode == 978)
        {
            $new_eur = floatval(strtr($data->Value,",","."));
            $curr_eur = floatval($DB->GetOne("SELECT value from settings where var = 'cur_rates:eur'"));
            if($new_eur/$curr_eur > 0.5 && $new_eur/$curr_eur < 1.5)
            {
                $record['value'] = $new_eur;
                $DB->AutoExecute("settings",$record,"UPDATE","var='cur_rates:eur'");
                echo $data->CharCode." - ".$new_eur." руб\n";
            }
        }
    }
}
else
{
    echo("Error in currency XML data\n");
}

