<?php

$billing_notify_days = 4;

include_once(PATH_BACKEND . '/php/classes/class.phpmailer.php');
include_once(PATH_BACKEND . '/php/classes/class.User.php');
include_once(PATH_BACKEND . '/php/classes/class.Billing.php');

$users = new User($DB);
$billing = new Billing($DB);

$smarty->template_dir = PATH_BACKEND . '/templates';

// Отключаем неоплаченные аккаунты

$billing_status_list = $billing->get_billing_status_list(0, 1);

if (!empty($billing_status_list))
{
    echo(PHP_EOL . 'DISABLE STATUS FOR:' . PHP_EOL);

    foreach ($billing_status_list as $billing_status)
    {
        if ($billing_status['billing_status'] == 0)
        {
            echo($billing_status['user_id'] . '. ' . $billing_status['user_title'] . ' --> ');

            $user = array(
                'id' => $billing_status['user_id'],
                'title' => $billing_status['user_title'],
                'email' => $billing_status['user_email'],
                'status' => 0
            );

            // Обновляем статус пользователя
            $users->Edit($user['id'], array('status' => 0));

            // Отправляем e-mail уведомление
            $mail = new PHPMailer();
            $mail->Sender = 'admin@einfo.ru';
            $mail->SetFrom('admin@einfo.ru', 'Einfo');
            $mail->Subject = '[einfo.ru] Изменение статуса аккаунта';

            $smarty->assign('id', $user['id']);
            $smarty->assign('status_old', 1);

            $smarty->assign('user', $user);

            $mail->Body = $smarty->fetch('users/mail_status.tpl');

            $email_to = preg_split('/[,;\s]/', $user['email'], -1, PREG_SPLIT_NO_EMPTY);
            foreach ($email_to as $value)
            {
                $mail->AddAddress(trim($value));
            }

            $result = $mail->Send();

            if (!empty($result))
            {
                echo('MAIL SEND');
            }
            else
            {
                echo('MAIL ERROR');
            }

            echo(PHP_EOL);
        }
    }
}

// Высылаем предупреждения активным пользователям, у которых через $billing_notify_days дней заканчивается оплаченный период.

$billing_status_list = $billing->get_billing_status_list(0, 1, '+' . $billing_notify_days . ' day');

if (!empty($billing_status_list))
{
    echo(PHP_EOL . 'LESS ' . $billing_notify_days . ' DAYS FOR PAY:' . PHP_EOL);

    foreach ($billing_status_list as $billing_status)
    {
        if ($billing_status['billing_status'] == 0)
        {
            echo($billing_status['user_id'] . '. ' . $billing_status['user_title'] . ' --> ');

            $user = array(
                'id' => $billing_status['user_id'],
                'title' => $billing_status['user_title'],
                'email' => $billing_status['user_email']
            );

            $active_to = $billing->get_billing_last_active_to($user['id'], '+' . $billing_notify_days . ' day');

            if (empty($active_to))
            {
                continue;
            }

            // Отправляем e-mail уведомление
            $mail = new PHPMailer();
            $mail->Sender = 'admin@einfo.ru';
            $mail->SetFrom('admin@einfo.ru', 'Einfo');
            $mail->Subject = '[einfo.ru] Необходимо оплатить услуги';

            $smarty->assign('user', $user);
            $smarty->assign('date', date('d.m.Y', strtotime($active_to)));

            $mail->Body = $smarty->fetch('billing/mail_billing_notify.tpl');

            $email_to = preg_split('/[,;\s]/', $user['email'], -1, PREG_SPLIT_NO_EMPTY);
            foreach ($email_to as $value)
            {
                $mail->AddAddress(trim($value));
            }

            $result = $mail->Send();

            if (!empty($result))
            {
                echo('MAIL SEND');
            }
            else
            {
                echo('MAIL ERROR');
            }

            echo(PHP_EOL);
        }
    }
}

echo(PHP_EOL);