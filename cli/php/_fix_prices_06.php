<?php

/* Удаление колонок:
 * e2_main.base_main              (roz, roz_cur, opt, opt_cur)
 * e2_main.orders_items           (roz, roz_cur, opt, opt_cur)
 * e2_main.catalog_comp_canon     (prise_roz_median, price_opt_median)
 * e2_main.price_columns          (opt, opt_cur)
 * e2_users.dist_xxx              (roz, roz_cur, opt, opt_cur)
 */

// ------------------------------------------------------------

echo "1) Удаление (roz, roz_cur, opt, opt_cur) из " . DB_MAIN_NAME . ".base_main \n";

$DB->Execute('ALTER TABLE base_main
                  DROP COLUMN roz,
                  DROP COLUMN roz_cur,
                  DROP COLUMN opt,
                  DROP COLUMN opt_cur');

// ------------------------------------------------------------

echo "2) Удаление (roz, roz_cur, opt, opt_cur) из " . DB_MAIN_NAME . ".orders_items \n";

$DB->Execute('ALTER TABLE orders_items 
                  DROP COLUMN roz,
                  DROP COLUMN roz_cur,
                  DROP COLUMN opt,
                  DROP COLUMN opt_cur');

// ------------------------------------------------------------

echo "3) Удаление (prise_roz_median, price_opt_median) из " . DB_MAIN_NAME . ".catalog_comp_canon \n";

$DB->Execute('ALTER TABLE catalog_comp_canon
                  DROP COLUMN price_roz_median,
                  DROP COLUMN price_opt_median');

// ------------------------------------------------------------

echo "4) Удаление (opt, opt_cur) из " . DB_MAIN_NAME . ".price_columns \n";

$DB->Execute('DELETE FROM price_columns WHERE name = "opt" OR name = "opt_cur"');
$DB->Execute('DELETE FROM users_templates WHERE column_id IN (6,7)');

// ------------------------------------------------------------

echo "5) Удаление (roz, roz_cur, opt, opt_cur) из " . DB_USERS_NAME . ".dist_xxx \n";

$sql_query = sprintf("SELECT TABLE_NAME
                        FROM information_schema.tables
                        WHERE table_schema='%1\$s'", DB_USERS_NAME);

$schema_tables = $DB2->GetAll($sql_query);

foreach ($schema_tables as $item)
{
    $DB2->Execute('ALTER TABLE ' . $item['TABLE_NAME'] . '
                    DROP COLUMN roz,
                    DROP COLUMN roz_cur,
                    DROP COLUMN opt,
                    DROP COLUMN opt_cur');
}