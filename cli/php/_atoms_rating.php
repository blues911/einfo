<?php

echo(PHP_EOL);

print_r($argv);

if ($argc > 2)
{
    $regexp = array();
    for($i = 2; $i < $argc; $i++)
    {
        $regexp[] = trim($argv[$i]);
    }
    $regexp = '^' . implode(' ', $regexp) . '([ ]+.*$|$)';

    echo('Regexp: ' . $regexp . PHP_EOL . PHP_EOL . 'Searching...' . PHP_EOL . PHP_EOL);

    $atoms = $DB->GetAll('
        SELECT
          MID(atoms, 1, LOCATE(" ", atoms)) AS atom,
          COUNT(*) AS cnt
        FROM base_main
        WHERE atoms REGEXP ?
        GROUP BY atom
        ORDER BY cnt DESC
    ', array($regexp));

    if (!empty($atoms))
    {
        echo('Results:' . PHP_EOL);

        foreach ($atoms as $atom)
        {
            echo(str_pad($atom['cnt'], 8, ' ', STR_PAD_LEFT) . '  ' . $atom['atom'] . PHP_EOL);
        }
    }
    else
    {
        echo('Not results.');
    }

}
else
{
    echo('Use ' . $argv[0] . ' ' . $argv[1] . ' <REGEXP>');
}

echo(PHP_EOL);