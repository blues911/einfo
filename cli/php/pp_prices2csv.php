<?php

$max_debug_dir_size = 16106127360;

$price_files = $DB->GetAll('SELECT * FROM users_prices ORDER BY upload_time ASC');

if (!empty($price_files))
{
    $cnt = array('total' => 0, 'ok' => 0, 'fail' => 0);

    foreach ($price_files as $price_file)
    {
        $cnt['total']++;

        echo($cnt['total'] . ': ' . $price_file['filename'] . ' [' . $price_file['user_id'] . '] --> ');

        $preload_filename = PATH_PRICES . '/1_prices/' . $price_file['filename'];

        // debug
        $debug_path = PATH_PRICES . '/debug/';
        $file_size = filesize_byte($preload_filename);

        if ($file_size < $max_debug_dir_size)
        {
            copy($preload_filename, $debug_path . $price_file['filename']);
        }

        remove_dir_overhead($debug_path, $max_debug_dir_size);

        $price = new Price($preload_filename, 'utf-8');
        $price->set_tags(unserialize($price_file['tags']));

        $result = $price->save(PATH_PRICES . '/2_csv/' . $price_file['filename'], $price_file['id'], $debug);

        $DB->Execute('INSERT INTO cli_log SET user_id = ?, `timestamp` = NOW(), script = ?, ok = ?, fail = ?', array($price_file['user_id'], 'prices2csv', $debug['ok'], $debug['fail']));

        if (!empty($result))
        {
            $cnt['ok']++;
            echo('OK' . PHP_EOL);
        }
        else
        {
            $cnt['fail']++;
            echo('FAIL' . PHP_EOL);
        }
    }

    echo('TOTAL PRICES: ' . $cnt['total'] . '; OK: ' . $cnt['ok'] . '; FAIL: ' . $cnt['fail'] . PHP_EOL);
}
else
{
    echo(PHP_EOL . 'No preload prices.' . PHP_EOL);
}