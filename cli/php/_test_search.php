<?php

/*
* Скрипт для сравнения скорости поиска компонента
*
* Использование: php cli.php _test_search $comp_title
*/

$iteration = 1000;
$comp_title = isset($argv[2]) ? $argv[2] : null;
$result = array(
    'with_limit' => 0,
    'with_count' => 0
);

function elapsed_time($time)
{
    $result = array();

    $total_min = floor($time / 60);

    if ($total_min >= 60)
    {
        $result['hour'] = floor($total_min / 60);
        $result['min'] = $total_min - ($result['hour'] * 60);
    }
    else $result['min'] = $total_min;

    $result['sec'] = round($time - ($total_min * 60), 2);

    return $result;
}

// -----------------------------------------------------------------------------

echo 'Script started at ' . date('Y-m-d H:i') . PHP_EOL;

if (empty($comp_title))
{
    echo 'No search string!' . PHP_EOL;
    exit;
}

$t = microtime(true);
for ($i = 0; $i < $iteration; $i++)
{
    $DB->GetOne('SELECT id FROM base_main WHERE comp_title = ? LIMIT 0, 3', array($comp_title));
    if (!($i % 10)) echo '.';
}
echo PHP_EOL;
$result['with_limit'] = microtime(true) - $t;


$t = microtime(true);
for ($i = 0; $i < $iteration; $i++)
{
    $DB->GetOne('SELECT COUNT(*) FROM base_main WHERE comp_title = ?', array($comp_title));
    if (!($i % 10)) echo '_';
}
echo PHP_EOL;
$result['with_count'] = microtime(true) - $t;

echo PHP_EOL;
$elapsed = elapsed_time($result['with_limit']);
echo 'Total time with limit ';
echo isset($elapsed['hour']) ? $elapsed['hour'] . 'h ' : '';
echo isset($elapsed['min']) ? $elapsed['min'] . 'm ' : '';
echo isset($elapsed['sec']) ? $elapsed['sec'] . 's' : '';
echo PHP_EOL;

$elapsed = elapsed_time($result['with_count']);
echo 'Total time with count ';
echo isset($elapsed['hour']) ? $elapsed['hour'] . 'h ' : '';
echo isset($elapsed['min']) ? $elapsed['min'] . 'm ' : '';
echo isset($elapsed['sec']) ? $elapsed['sec'] . 's' : '';
echo PHP_EOL;