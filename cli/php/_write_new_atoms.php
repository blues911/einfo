<?php

/*
* Скрипт для внесения атомов по новому алгоритму в поле atoms_2
* в таблицу base_main.
*
* Использование: php cli.php _write_new_atoms (limit)
*/

function elapsed_time($time)
{
    $result = array();

    $total_min = floor($time / 60);

    if ($total_min >= 60)
    {
        $result['hour'] = floor($total_min / 60);
        $result['min'] = $total_min - ($result['hour'] * 60);
    }
    else $result['min'] = $total_min;

    $result['sec'] = round($time - ($total_min * 60), 2);

    return $result;
}

// -----------------------------------------------------------------------------

echo 'Script started at ' . date('Y-m-d H:i') . PHP_EOL;

$limit = !empty($argv[2]) ? (int)$argv[2] : 10000;

if (empty($limit))
{
    echo 'Wrong limit size!' . PHP_EOL;
    exit;
}

echo 'Start update field atoms2.' . PHP_EOL;
echo 'One point = ' . $limit . ' items' . PHP_EOL;
echo 'One row = ' . ($limit * 100) . ' items' . PHP_EOL;

$start = microtime(true);

// Проходимся по таблице выборкой по $limit строк, пока есть результаты с пустым полем atoms2
$i = 1;
while ($cnt = $DB->GetOne('SELECT COUNT(*) FROM base_main WHERE atoms2 = ""'))
{
    // Получаем значения из таблицы
    $list = $DB->GetAll('SELECT id, atoms FROM base_main WHERE atoms2 = "" LIMIT ?', array($limit));

    $_values = array();
    foreach ($list as $item)
    {
        $atoms2 = get_request_atoms_2($item['atoms']);
        $_values[] = '(' . $item['id'] . ', "' . implode(' ', $atoms2) . '")';
    }

    // Вставляем данные в поле atoms2(вариант для пакетного обновления)
    $DB->Execute(
        'INSERT LOW_PRIORITY INTO base_main (id, atoms2) '
         . 'VALUES ' . implode(', ', $_values)
         . ' ON DUPLICATE KEY UPDATE atoms2 = VALUES(atoms2)'
    );

    echo '.';
    if ($i % 10 == 0) echo '|';
    if ($i % 100 == 0) echo PHP_EOL;

    $i++;
}

// Вычисляем затраченное время
$time = microtime(true) - $start;
$elapsed = elapsed_time($time);

echo PHP_EOL . 'Time elapsed: ';
echo isset($elapsed['hour']) ? $elapsed['hour'] . 'h ' : '';
echo isset($elapsed['min']) ? $elapsed['min'] . 'm ' : '';
echo isset($elapsed['sec']) ? $elapsed['sec'] . 's' : '';
echo PHP_EOL;
