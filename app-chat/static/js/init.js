var host = '//' + get_meta_data('chat:host');

// Стили
var css_chat = document.createElement('link');
css_chat.href = host + '/static/css/chat.css?v=1';
css_chat.rel = 'stylesheet';

// Cкрипты
var js_jquery = document.createElement('script');
js_jquery.src = host + '/static/js/libs/jquery-3.3.1.min.js';
js_jquery.type = 'text/javascript';

var js_jquery_fix = document.createElement('script');
js_jquery_fix.text = 'jQuery.noConflict();';
js_jquery_fix.type = 'text/javascript';

var js_firebase_app = document.createElement('script');
js_firebase_app.src = host + '/static/js/libs/firebase-app.js';
js_firebase_app.type = 'text/javascript';

var js_firebase_messaging = document.createElement('script');
js_firebase_messaging.src = host + '/static/js/libs/firebase-messaging.js';
js_firebase_messaging.type = 'text/javascript';

var js_nicescroll = document.createElement('script');
js_nicescroll.src = host + '/static/js/libs/jquery.nicescroll.min.js';
js_nicescroll.type = 'text/javascript';

var js_socket_io = document.createElement('script');
js_socket_io.src = host + '/static/js/libs/socket.io.js';
js_socket_io.type = 'text/javascript';

var js_app_frontend = document.createElement('script');
js_app_frontend.src = host + '/static/js/app_frontend.js?v=2';
js_app_frontend.type = 'text/javascript';

var js_app_backend = document.createElement('script');
js_app_backend.src = host + '/static/js/app_backend.js?v=2';
js_app_backend.type = 'text/javascript';

// Определяем тип чата
var chat_type = get_meta_data('chat:type');

// Шаблон чата
var chat_template = document.createElement('div');
chat_template.id = 'chat-modal';
chat_template.classList.add('app-' + chat_type);
chat_template.classList.add('hidden');
chat_template.innerHTML = '<div class="header">' +
    '<strong></strong>' +
    '<span class="unread hidden"></span>' +
    '<ul class="controls">' +
    '<li class="toggle-modal">' +
    '<img src="' + host + '/static/icons/minimize-modal.svg" alt="toggle-modal">' +
    '</li>' +
    '<li class="close-modal">' +
    '<img src="' + host + '/static/icons/close-modal.svg" alt="close-modal">' +
    '</li>' +
    '</ul>' +
    '</div>' +
    '<div class="wrapper">' +
        '<div class="contacts">' +
            '<ul></ul>' +
            '</div>' +
        '<div class="messages">' +
            '<div class="alert-overlay"></div>' +
            '<div class="alert">' +
                '<a href="#" class="close"><img src="' + host + '/static/icons/close-alert.svg" alt="close-pop-up" /></a>' +
                '<p></p>' +
                '<a href="#" class="btn-no">Нет</a>' +
                '<a href="#" class="btn-yes">Да</a>' +
                '</div>' +
            '<ul></ul>' +
            '<div class="form">' +
                '<textarea name="message" placeholder="Введите сообщение"></textarea>' +
                '<a href="#" class="btn-send"><img src="' + host + '/static/icons/btn-send.svg" alt="btn-send" /></a>' +
                '</div>' +
            '<div class="form-blocked"></div>' +
            '</div>' +
        '</div>' +
        '<audio id="message-sound">' +
        '<source src="' + host + '/static/new_message.mp3" type="audio/mp3">' +
        '</audio>';

// Из-за конфликта с prototype.js удаляем все prototype.toJSON
if (window.Prototype != undefined)
{
    delete Object.prototype.toJSON;
    delete Array.prototype.toJSON;
    delete Hash.prototype.toJSON;
    delete String.prototype.toJSON;
}

include_sources(function ()
{
    if (chat_type == 'backend')
    {
        var user_id = get_meta_data('chat:user_id');

        if (ls_get('user_id') == null)
        {
            // Установка базовых параметров в Local Storage
            ls_set('user_id', user_id);
            ls_set('active_contact_hash', null);
            ls_set('is_window_minimized', 0);
            ls_set('csrf_token', null);
            ls_set('fcm_token', null);
            ls_set('device_hash', null);
        }
        else if (ls_get('user_id') != user_id)
        {
            // Установка базовых параметров в Local Storage
            ls_set('user_id', user_id);
            ls_set('active_contact_hash', null);
            ls_set('is_window_minimized', 0);
            ls_set('csrf_token', null);
            ls_set('fcm_token', null);
            ls_set('device_hash', null);
        }

        // Для поставщика постоянно определяем csrf_token на случай
        // если пользователь будет заходить из разных браузеров.
        var str_params = 'type=init_token&params[owner_type]=user&params[owner_value]=' + ls_get('user_id');

        ajax_request(str_params, function(result)
        {
            // Инициализируем токен
            ls_set('csrf_token', result.token);

            var device_hash = Math.random().toString(36).substr(2, 10);
            if (!ls_get('device_hash')) ls_set('device_hash', device_hash);

            document.body.appendChild(js_app_backend);
        });
    }
    else if (chat_type == 'frontend')
    {
        if (ls_get('guest_hash') == null)
        {
            var create_guest_hash = function()
            {
                var date = new Date().toISOString().slice(0,10).replace(/-/g, '');
                var rand_str = Math.random().toString(36).substr(2, 6);
                var result = date + '-' + rand_str;

                return result.toUpperCase();
            };

            // Установка базовых параметров в Local Storage
            ls_set('guest_hash', create_guest_hash());
            ls_set('contacts', []);
            ls_set('active_contact_id', null);
            ls_set('is_window_minimized', 0);
            ls_set('csrf_token', null);
            ls_set('fcm_token', null);
        }

        if (ls_get('contacts') == null) ls_set('contacts', []);
        if (ls_get('active_contact_id') == null) ls_set('active_contact_id', null);
        if (ls_get('is_window_minimized') == 0) ls_set('is_window_minimized', 0);

        if (ls_get('csrf_token') == null)
        {
            var str_params = 'type=init_token&params[owner_type]=guest&params[owner_value]=' + ls_get('guest_hash');

            ajax_request(str_params, function(result)
            {
                // Инициализируем токен
                ls_set('csrf_token', result.token);
                document.body.appendChild(js_app_frontend);
            });
        }
        else
        {
            document.body.appendChild(js_app_frontend);
        }
    }
});

// Подключает стили и скрипты приложения
function include_sources(callback)
{
    document.head.appendChild(css_chat);
    document.body.appendChild(chat_template);

    document.body.appendChild(js_jquery);
    js_jquery.onload = function()
    {
        // Избегаем конфликт jquery и prototype.js
        // https://learn.jquery.com/using-jquery-core/avoid-conflicts-other-libraries
        document.body.appendChild(js_jquery_fix);

        document.body.appendChild(js_nicescroll);
        js_nicescroll.onload = function()
        {
            document.body.appendChild(js_socket_io);
            js_socket_io.onload = function()
            {
                document.body.appendChild(js_firebase_app);
                js_firebase_app.onload = function()
                {
                    document.body.appendChild(js_firebase_messaging);
                    js_firebase_messaging.onload = function()
                    {
                        callback.call(this);
                    };
                };
            };
        };
    };
}

// ---------- Функции хелперы ----------

// Находит meta тег по имени и возвращает content
function get_meta_data(name)
{
    var meta_tags = document.getElementsByTagName('meta');

    for (let i = 0; i < meta_tags.length; i++)
    {
        if (meta_tags[i].getAttribute('name') === name)
        {
            return meta_tags[i].getAttribute('content');
        }
    }
}

// Шаблон для AJAX запроса
function ajax_request(data, callback)
{
    var xhr = new XMLHttpRequest();

    xhr.open('POST', host + '/index.php');
    xhr.setRequestHeader('Accept', 'application/json, text/javascript');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send(encodeURI(data));
    xhr.onload = function()
    {
        if (xhr.readyState == 4 && xhr.status == 200)
        {
            callback.call(this, JSON.parse(xhr.responseText));
        }
        else
        {
            console.log(xhr);
        }
    };
}

// Получает параметр из Local Storage
function ls_get(key)
{
    var ls_key = 'chat_' + chat_type + ':' + key;
    var param = JSON.parse(localStorage.getItem(ls_key));

    return (param !== null ) ? param : null;
}

// Устанавливает параметр в Local Storage
function ls_set(key, value)
{
    var ls_key = 'chat_' + chat_type + ':' + key;
    localStorage.setItem(ls_key, JSON.stringify(value));
}
