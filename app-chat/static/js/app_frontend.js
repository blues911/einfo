/**
 * Функционал чата для app-frontend.
 * 
 * Основные разделы файла:
 * - Настройки
 * - Функции начальной инициализации
 * - Вебсокеты
 * - События в окне чата
 * - Функции многоразоваого использования
 * - Функции хелперы
 */

// ---------- Настройки ----------

var $$$ = jQuery;
// meta[name="chat:xxx"]
var host = '//' + $$$('meta[name="chat:host"]').attr('content');
var host_ws = $$$('meta[name="chat:host_ws"]').attr('content');
var host_frontend = $$$('meta[name="chat:host_frontend"]').attr('content');
var last_vist_interval = $$$('meta[name="chat:last_visit_interval"]').attr('content');
// websocket
var ws_client = io.connect(host_ws, {query: {token: ls_get('csrf_token')}});
var ws_typing_timer;
var ws_typing_interval = 1500;
// firebase
var fb_config = {
    apiKey: "AIzaSyCPJ7aQj-ZQhdHb4QGvy6sfiYZtmVNNwFM",
    authDomain: "einfo-notification.firebaseapp.com",
    databaseURL: "https://einfo-notification.firebaseio.com",
    projectId: "einfo-notification",
    storageBucket: "einfo-notification.appspot.com",
    messagingSenderId: "593124678630",
    appId: "1:593124678630:web:9cd8ea94f997136f5c1427"
};
var fb_messaging = {};
// разное
var ns_config = {
    cursorcolor: '#b3b3b3',
    cursorborder: '1px solid #b3b3b3',
    background: '#e6e6e6',
    cursoropacitymin: 1,
    cursorwidth: '4px',
    cursorborderradius: '0px',
    horizrailenabled: false
};
var favicon = new Favico({
    animation: 'none'
});
var last_vist_timer = null;

// ---------- Функции начальной инициализации ----------

// Установка скроллбара
$$$('#chat-modal .contacts').niceScroll(ns_config);
$$$('#chat-modal .messages ul').niceScroll(ns_config);
$$$('#chat-modal .messages .form textarea').niceScroll(ns_config);

// Установка заголовков
$$$('#chat-modal .header strong').text('Онлайн-чат с поставщиками');
$$$('#chat-modal .alert p').text('Хотите скрыть беседу?');
$$$('#chat-modal .form-blocked').text('Поставщик завершил беседу');

// Инициализация firebase для push уведомлений
firebase.initializeApp(fb_config);
fb_messaging = firebase.messaging();

// Сбросить параметры чата если по какой-то причине
// были отключены уведомления в браузере и были диалоги
if (Notification.permission != 'granted' && ls_get('fcm_token') != null)
{
    ls_set('fcm_token', null);
    ls_set('active_contact_id', null);
    ls_set('contacts', []);

    ws_client.disconnect();

    ajax_request({
        type: 'close_messages',
        params: {
            user_id: ls_get('active_contact_id'),
            guest_hash: ls_get('guest_hash'),
            sender_type: 'guest',
            all: true
        }
    }, function(result) {

        // Обновить токен
        ajax_request({
              type: 'init_token',
              params: {
                  owner_type: 'guest',
                  owner_value: ls_get('guest_hash'),
                  update: true
              }
          }, function(result) {

              ls_set('csrf_token', result.token);
          });
    });
}

if (ls_get('fcm_token') == null)
{
    ws_client.disconnect();
}
else
{
    // Проверка состояния окна
    if (ls_get('is_window_minimized') == 1)
    {
        minimize_chat();
    }

    // Проверка контактов и загрузка сообщений
    if (ls_get('contacts') != null)
    {
        var ls_contacts = ls_get('contacts');

        if (ls_contacts.length > 0)
        {
            load_contacts();
            load_messages();
            show_chat();

            // Проверка непрочитанных сообщений
            ajax_request({
                type: 'count_unread_messages',
                params: {
                      guest_hash: ls_get('guest_hash'),
                      sender_type: 'user'
                    }
                }, function(result) {

                var contacts_items = $$$('#chat-modal .contacts ul').find('li');

                if (contacts_items.length > 0 && result.messages_info.length > 0)
                {
                    for (var i = 0; i < contacts_items.length; i++)
                    {
                        for (var j = 0; j < result.messages_info.length; j++)
                        {
                            if (!$$$('#chat-modal').hasClass('minimized'))
                            {
                                if (!$$$(contacts_items[i]).hasClass('active')
                                    && $$$(contacts_items[i]).data('contact-id') == result.messages_info[j].user_id
                                    && result.messages_info[j].total_unread > 0)
                                {
                                    var total = result.messages_info[j].total_unread;

                                    if (total > 99)
                                    {
                                      total = 99;
                                    }

                                    $$$(contacts_items[i]).find('.total-unread')
                                        .removeClass('hidden')
                                        .text(total);
                                }
                            }
                            else
                            {
                                if ($$$(contacts_items[i]).data('contact-id') == result.messages_info[j].user_id
                                    && result.messages_info[j].total_unread > 0)
                                {
                                    var total = result.messages_info[j].total_unread;

                                    if (total > 99)
                                    {
                                        total = 99;
                                    }

                                    $$$(contacts_items[i]).find('.total-unread')
                                        .removeClass('hidden')
                                        .text(total);
                                }
                            }
                        }
                    }

                    set_header_unread_messages();
                    set_favico_unread_messages();
                }
            });
        }
    }

    // Проверка статуса "онлайн" для контактов
    last_vist_timer = setInterval(function() {
        check_contacts_last_visit();
    }, last_vist_interval * 1000);
}

// ---------- Вебсокеты ----------

// Новое сообщение от поставщика
ws_client.on('new_message', function(response)
{
    var messages = $$$('#chat-modal .messages ul');
    var contacts = $$$('#chat-modal .contacts ul');
    var contacts_items = contacts.find('li');

    if (response.sender_type == 'user'
        && response.guest_hash == ls_get('guest_hash')
        && response.user_id == ls_get('active_contact_id'))
    {
        if ($$$('#chat-modal').hasClass('minimized'))
        {
            contacts_items.each(function()
            {
                var contact_id = $$$(this).data('contact-id');
    
                if (response.user_id == contact_id)
                {
                    var el = $$$(this).find('.total-unread');
                    var total = Number(el.text()) + 1;
    
                    if (total > 99)
                    {
                        total = 99;
                    }
    
                    el.text(total);
                    el.removeClass('hidden');
    
                    $$$('#message-sound')[0].play();
                }
            });
    
            set_header_unread_messages();
        }
        else
        {
            read_messages();
        }

        var srv_class = (response.user_weak_activity == 1) ? 'srv' : '';
        messages.find('li.ms-left.dummy').remove();
        messages.append('<li class="ms-left ' + srv_class + '">' + nl2br(response.text) + '</li>');
        messages.animate({
            scrollTop: messages.prop('scrollHeight')
        }, 'slow');
    }
    else if (response.sender_type == 'user'
        && response.guest_hash == ls_get('guest_hash')
        && response.user_id != ls_get('active_contact_id'))
    {
        contacts_items.each(function()
        {
            var contact_id = $$$(this).data('contact-id');

            if (response.user_id == contact_id)
            {
                var el = $$$(this).find('.total-unread');
                var total = Number(el.text()) + 1;

                if (total > 99)
                {
                    total = 99;
                }

                el.text(total);
                el.removeClass('hidden');

                $$$('#message-sound')[0].play();
            }
        });

        set_header_unread_messages();
    }

    set_favico_unread_messages();
});

// Поставщик начал печатать
ws_client.on('start_typing', function(response)
{
    var messages = $$$('#chat-modal .messages ul');

    if (response.sender_type == 'user'
        && response.guest_hash == ls_get('guest_hash')
        && response.user_id == ls_get('active_contact_id'))
    {
        if (messages.find('li.ms-left.dummy').length == 0)
        {
            messages.append('<li class="ms-left dummy"><span class="loading">печатает</span></li>');
            messages.animate({
                scrollTop: messages.prop('scrollHeight')
            }, 'slow');
        }
    }
});

// Поставщик закончил печатать
ws_client.on('end_typing', function(response)
{
    var messages = $$$('#chat-modal .messages ul');

    if (response.sender_type == 'user'
        && response.guest_hash == ls_get('guest_hash')
        && response.user_id == ls_get('active_contact_id'))
    {
        messages.find('li.ms-left.dummy').remove();
        messages.animate({
            scrollTop: messages.prop("scrollHeight")
        }, "slow");
    }
});

// Поставщик закрыл беседу
ws_client.on('close_messages', function(response)
{
    if (response.sender_type == 'user'
        && response.guest_hash == ls_get('guest_hash')
        && response.user_id == ls_get('active_contact_id'))
    {
        var contacts_items = $$$('#chat-modal .contacts ul').find('li');

        contacts_items.each(function()
        {
            var contact_id = $$$(this).data('contact-id');

            if (ls_get('active_contact_id') == contact_id)
            {
                block_form();
            }
        });
    }
});

// ---------- События в окне чата ----------

Notification.onclick = function(event) { console.log(event); };

$$$('.js_subscribe_close').on('click', function ()
{
    fb_show_popup_subscribe('', false);
});

$$$(document).on('click', '.js_popup_btn_subscribe', function (e)
{
    e.preventDefault();
    var id = $$$(this).data('id');

    fb_show_popup_subscribe('', false);

    fb_messaging.requestPermission().then(function() {
        // Повторный клик по кнопке "Онлайн-чат" с тем же id
        $$$('table.search a.start-chat[data-id="'+ id +'"]').click();
    }).catch(function(err) {
        console.log(err);
    });
});

// Начать чат / Добавить новый контакт
$$$(document).on('click', 'table.search a.start-chat', function(e)
{
    e.preventDefault();

    var retry = 0;
    var fb_get_token = function(callback)
    {
        fb_messaging.getToken()
        .then(function(token) {
            if (token)
            {
                ajax_request({
                    type: 'save_fcm_token',
                    params: {
                        fcm_token: token,
                        owner_type: 'guest',
                        owner_value: ls_get('guest_hash')
                    }
                }, function(result) {

                    ls_set('fcm_token', token);
                    fb_show_popup_subscribe('', false);
                });
            }
            else 
            {
                ls_set('fcm_token', null);
                fb_show_popup_subscribe('<p>Ошибка при загрузке чата. Закройте окно и попробуйте еще раз нажать <strong>"Онлайн-чат"</strong>.</p>', true);

                console.log('Не удалось получить токен.');
                return false;
            }
        }).catch(function (err) {
            if (retry == 0)
            {
                // Ошибка с переключением разрешения на уведомления
                // https://github.com/firebase/firebase-js-sdk/issues/2364
                retry = 1;
                fb_get_token();
            }
            else
            {
                ls_set('fcm_token', null);
                fb_show_popup_subscribe('<p>Ошибка при загрузке чата. Закройте окно и попробуйте еще раз нажать <strong>"Онлайн-чат"</strong>.</p>', true);

                console.log(err);
                return false;
            }
        });
    };

    if (Notification.permission == 'denied')
    {
        fb_show_popup_subscribe('<p>Вы заблокировали возможность отправки уведомлений.' +
            '<p>Для того, чтобы пользоваться чатом, необходимо <b>разрешить</b> получение уведомлений.</p>' +
            '<img src="/media/i/notification_denied.png">' +
            '<p>Чтобы разрешить получение уведомлений, откройте <b>сведения о сайте</b>, а затем выберите уведомление и <b>разрешите</b> их получение.</p>',
            true
        );

        return false;
    }

    if (Notification.permission == 'default')
    {
        fb_show_popup_subscribe('<p>Для того, чтобы пользоваться чатом, необходимо <b>разрешить</b> получение уведомлений.</p>' +
            '<div class="popup_subscribe_img">' +
            '<img src="/media/i/notification.png">' +
            '<a class="button light js_popup_btn_subscribe" href="#" data-id="'+ $$$(this).data('id') +'">Подтвердить</a>' +
            '</div>',
            true
        );

        return false;
    }

    if (ls_get('fcm_token') == null)
    {
        fb_get_token();

        ws_client.connect();
    
        if (last_vist_timer == null)
        {
            last_vist_timer = setInterval(function() {
                check_contacts_last_visit();
            }, last_vist_interval * 1000);
        }
    }

    var is_unique = true;
    var comp_title = 'Запрос на позицию: ' + $$$(this).data('comp-title');
    var contact_id = $$$(this).data('contact-id');
    var contact_title = $$$(this).data('contact-title');
    var contacts = $$$('#chat-modal .contacts ul');
    var contacts_items = $$$('#chat-modal .contacts ul').find('li');

    if (contacts_items.length > 0)
    {
        contacts_items.each(function()
        {
            var id = $$$(this).data('contact-id');

            if (contact_id == id)
            {
              is_unique = false;
            }
        });
    }

    // проверить дубликат
    if (is_unique == false)
    {
        maximize_chat();

        return false;
    }

    contacts.append(set_contact_tpl(contact_id, contact_title));
    contacts.find('li').last().addClass('active').siblings().removeClass('active');
    contacts.animate({
        scrollTop: contacts.prop('scrollHeight')
    }, 'slow');

    var ls_contacts = ls_get('contacts');

    ls_contacts.push({
        id: contact_id,
        title: contact_title
    });

    ls_set('contacts', ls_contacts);
    ls_set('active_contact_id', contact_id);

    $$$('#chat-modal .messages ul').empty();

    // Проверить контакт на наличие старых сообщений. Если есть, то подгружаем историю.
    ajax_request({
        type: 'check_guest_contact_messages',
        params: {
            user_id: ls_get('active_contact_id'),
            guest_hash: ls_get('guest_hash'),
            sender_type: 'guest',
            text: comp_title
        }
    }, function(result) {

        if (result.success == true)
        {
            load_messages();
        }

        ws_client.emit('new_contact', {
            user_id: ls_get('active_contact_id'),
            guest_hash: ls_get('guest_hash'),
            guest_title: result.guest_title,
            sender_type: 'guest'
        });

        show_chat();
        maximize_chat();
    });
});

// Выбрать контакт
$$$(document).on('click', '#chat-modal .contacts a.title', function(e)
{
    e.preventDefault();

    var el = $$$(this).parent('li');

    ls_set('active_contact_id', el.data('contact-id'));

    el.addClass('active').siblings().removeClass('active');

    hide_alert();
    reset_form();
    load_messages();
});

// Удалить контакт (открыть окно подтверждения)
$$$(document).on('click', '#chat-modal .contacts img.delete-contact', function(e)
{
    e.preventDefault();

    var contact = $$$(this).parent('li');
    var contact_id = contact.data('contact-id');

    contact.addClass('active').siblings().removeClass('active');

    ls_set('active_contact_id', contact_id);

    show_alert();
    load_messages();
});

// Удалить контакт (закрыть окно подтверждения)
$$$(document).on('click', '#chat-modal .alert a.btn-no', function(e)
{
    e.preventDefault();

    hide_alert();
});

// Удалить контакт (закрыть окно подтверждения)
$$$(document).on('click', '#chat-modal .alert a.close', function(e)
{
    e.preventDefault();

    hide_alert();
});

// Удалить контакт
$$$(document).on('click', '#chat-modal .alert a.btn-yes', function(e)
{
    e.preventDefault();

    var contact = $$$('#chat-modal .contacts ul').find('li.active');
    var deleted_contact_id = contact.data('contact-id');
    var params = {
        user_id: ls_get('active_contact_id'),
        guest_hash: ls_get('guest_hash'),
        sender_type: 'guest',
        all: false
    };

    ajax_request({
        type: 'close_messages',
        params: params
    }, function(result) {

        contact.remove();
        $$$('#chat-modal .messages ul').empty();

        ws_client.emit('close_messages', params);

        var contacts = $$$('#chat-modal .contacts ul').find('li');
        var ls_contacts = ls_get('contacts');

        if (contacts.length == 0)
        {
            // Обновить токен
            ajax_request({
                type: 'init_token',
                params: {
                    owner_type: 'guest',
                    owner_value: ls_get('guest_hash'),
                    update: true
                }
            }, function(result) {

                ls_set('csrf_token', result.token);
                ls_set('fcm_token', null);
                ls_set('active_contact_id', null);
                ls_set('contacts', []);
                hide_chat();

                clearInterval(last_vist_timer);
                last_vist_timer = null;
                ws_client.disconnect();
            });
        }
        else
        {
            $$$(contacts[0]).addClass('active');

            ls_set('active_contact_id', $$$(contacts[0]).data('contact-id'));
            
            if (ls_contacts.length > 0)
            {
                for (var i = 0; i < ls_contacts.length; i++)
                {
                    if (ls_contacts[i].id == deleted_contact_id)
                    {
                        ls_contacts.splice(i, 1);

                        ls_set('contacts', ls_contacts);
                    }
                }
            }

            load_contacts();
            load_messages();
        }

        hide_alert();
        reset_form();
    });
});

// Обработка формы и отправка сообщения
$$$(document).on('click', '#chat-modal .form a.btn-send', function(e)
{
    e.preventDefault();

    var form = $$$(this).parent('.form');
    var text = form.find('textarea').val();
    var messages = $$$('#chat-modal .messages ul');

    if (text.replace(/\s/g, '') == '' || text === '')
    {
        return false;
    }

    var params = {
        user_id: ls_get('active_contact_id'),
        guest_hash: ls_get('guest_hash'),
        sender_type: 'guest',
        text: strip_tags(text),
        search_position: 0,
        user_weak_activity: 0
    };

    ajax_request({
        type: 'add_message',
        params: params
    }, function(result) {

        ws_client.emit('new_message', params);

        messages.append('<li class="ms-right">' + nl2br(params.text) + '</li>');
        messages.animate({
            scrollTop: messages.prop('scrollHeight')
        }, 'slow');

        reset_form();
    });
});

// Проверка клавиши ENTER и эмуляция клика по кнопке
$$$(document).on('keydown', '#chat-modal .form textarea', function(e)
{
    if (e.keyCode == 13 && !e.shiftKey)
    {
        e.preventDefault();
        $$$('#chat-modal .form a.btn-send').click();
    }
});

// Сернуть/развернуть окно чата
$$$(document).on('click', '#chat-modal .header', function(e)
{
    e.preventDefault();

    if ($$$(e.target).hasClass('close-modal') || $$$(e.target).attr('alt') == 'close-modal')
    {
        return false;
    }

    var modal = $$$('#chat-modal');

    modal.hasClass('minimized') ? maximize_chat() : minimize_chat();
});

// Закрыть окно чата
$$$(document).on('click', '#chat-modal .header li.close-modal', function(e)
{
    e.preventDefault();

    ajax_request({
        type: 'close_messages',
        params: {
            user_id: ls_get('active_contact_id'),
            guest_hash: ls_get('guest_hash'),
            sender_type: 'guest',
            all: true
        }
    }, function(result) {

        // Обновить токен
        ajax_request({
            type: 'init_token',
            params: {
                owner_type: 'guest',
                owner_value: ls_get('guest_hash'),
                update: true
            }
        }, function(result) {

            var contacts_items = $$$('#chat-modal .contacts ul').find('li');

            contacts_items.each(function()
            {
                ws_client.emit('close_messages', {
                    user_id: $$$(this).data('contact-id'),
                    guest_hash: ls_get('guest_hash'),
                    sender_type: 'guest',
                    all: true
                });
            });

            $$$('#chat-modal .contacts ul').empty();

            ls_set('csrf_token', result.token);
            ls_set('fcm_token', null);
            ls_set('active_contact_id', null);
            ls_set('contacts', []);
            hide_alert();
            hide_chat();

            clearInterval(last_vist_timer);
            last_vist_timer = null;
            ws_client.disconnect();
        });
    });
});

// Проверить старт набора текста в форме
$$$(document).on('keyup', '#chat-modal .form textarea', function(e)
{
    var typing_params = {
        user_id: ls_get('active_contact_id'),
        guest_hash: ls_get('guest_hash'),
        sender_type: 'guest'
    };

    if (e.keyCode != 13)
    {
        ws_client.emit('start_typing', typing_params);
    }

    clearTimeout(ws_typing_timer);

    ws_typing_timer = setTimeout(function() {
        ws_client.emit('end_typing', typing_params);
    }, ws_typing_interval);
});

// Проверить стоп набора текста в форме
$$$(document).on('keydown', '#chat-modal .form textarea', function()
{
    clearTimeout(ws_typing_timer);
}); 

// ---------- Функции многоразоваого использования ----------

// Открыть чат
function show_chat()
{
    $$$('#chat-modal').removeClass('hidden');
}

// Закрыть чат
function hide_chat()
{
    $$$('#chat-modal').addClass('hidden');
    ls_set('is_window_minimized', 0);
}

// Свернуть окно чата
function minimize_chat()
{
    var modal = $$$('#chat-modal');

    modal.addClass('minimized');
    modal.find('.header li.toggle-modal img').attr('src', host + '/static/icons/maximize-modal.svg');

    ls_set('is_window_minimized', 1);
    set_header_unread_messages();
    set_favico_unread_messages();
}

// Развернуть окно чата
function maximize_chat()
{
    var modal = $$$('#chat-modal');
    var contact = $$$('#chat-modal .contacts ul').find('li.active');

    contact.find('.total-unread').addClass('hidden').text(0);

    modal.removeClass('minimized');
    modal.find('.header li.toggle-modal img').attr('src', host + '/static/icons/minimize-modal.svg');
    
    $$$('#chat-modal .header').find('span.unread').addClass('hidden');

    ls_set('is_window_minimized', 0);
    read_messages();
    set_favico_unread_messages();
}

// Открыть окно уведомления
function show_alert()
{
    var messages = $$$('#chat-modal .messages');

    messages.find('.alert-overlay').addClass('active');
    messages.find('.alert').addClass('active');
}

// Скрыть окно уведомления
function hide_alert()
{
    var messages = $$$('#chat-modal .messages');

    messages.find('.alert-overlay').removeClass('active');
    messages.find('.alert').removeClass('active');
}

// Очистить поле ввода
function reset_form()
{
    var form = $$$('#chat-modal .form');
    
    form.find('textarea').val('');
}

// Заблокировать форму ввода
function block_form()
{
    $$$('#chat-modal .form-blocked').removeClass('hidden');
    $$$('#chat-modal .form').addClass('hidden');
}

// Разблокировать форму ввода
function unblock_form()
{
    $$$('#chat-modal .form-blocked').addClass('hidden');
    $$$('#chat-modal .form').removeClass('hidden');
}

// Шаблон контакта
function set_contact_tpl(id, title)
{
    return '<li data-contact-id="'+id+'">' +
    '<a href="#" class="title">' +
    ' <span class="online" title="Поставщик он-лайн"></span>' +
    '<span title="' + title+ '">' + title+ '</span>' +
    '</a>' +
    '<a href="#" class="total-unread hidden">0</a>' +
    '<img class="delete-contact" src="'+host+'/static/icons/delete-contact.svg" alt="delete-contact">' +
    '</li>';
}

// Загрузить контакты
function load_contacts()
{
    var ls_contacts = ls_get('contacts');
    var contacts = $$$('#chat-modal .contacts ul');

    contacts.empty();

    if (ls_contacts.length > 0)
    {
        for (var i = 0; i < ls_contacts.length; i++)
        {
            contacts.append(set_contact_tpl(ls_contacts[i].id, ls_contacts[i].title));
        }

        var contacts_items = contacts.find('li');
    
        if (ls_get('active_contact_id') == null)
        {
            contacts_items.first().addClass('active');
            ls_set('active_contact_id', contacts_items.first().data('contact-id'));
        }
        else
        {
            contacts_items.each(function()
            {
                if ($$$(this).data('contact-id') == ls_get('active_contact_id'))
                {
                    $$$(this).addClass('active').siblings().removeClass('active');
                }
            });
        }
    }
}

// Загрузить сообщения
function load_messages()
{
    ajax_request({
        type: 'get_messages',
        params: {
            user_id: ls_get('active_contact_id'),
            guest_hash: ls_get('guest_hash')
        }
    }, function(result) {

        var closed_by_user = false;
        var messages = $$$('#chat-modal .messages ul');
        var contact = $$$('#chat-modal .contacts ul').find('li.active');

        messages.empty();

        if (!$$$('#chat-modal').hasClass('minimized'))
        {
            contact.find('.total-unread').addClass('hidden').text(0);
            read_messages();
        }

        set_header_unread_messages();
        set_favico_unread_messages();

        if (result.messages.length > 0)
        {
            for (var i = 0; i < result.messages.length; i++)
            {
                if (result.messages[i].sender_type == 'guest')
                {
                    if (result.messages[i].search_position == 1)
                    {
                        var text = result.messages[i].text.split(': ');
                        var href = '//' + host_frontend + '/store/' + text[1] + '/';

                        messages.append('<li class="ms-right srv">' + text[0] + ': <a href="'+ href +'" target="_blank">' + text[1] + '</li>');
                    }
                    else
                    {
                        messages.append('<li class="ms-right">' + nl2br(result.messages[i].text) + '</li>');
                    }
                }
                else if (result.messages[i].sender_type == 'user')
                {
                    if (result.messages[i].user_weak_activity == 1)
                    {
                        messages.append('<li class="ms-left srv">' + nl2br(result.messages[i].text) + '</li>');
                    }
                    else
                    {
                        messages.append('<li class="ms-left">' + nl2br(result.messages[i].text) + '</li>');
                    }
                }

                if (result.messages[i].closed_by_user == 1)
                {
                    closed_by_user = true;
                }
            }
        }

        if (closed_by_user == true)
        {
            block_form();
        }
        else
        {
            unblock_form();
        }

        messages.animate({
            scrollTop: messages.prop('scrollHeight')
        }, 'slow');
    });
}

// Прочитать сообщения
function read_messages()
{
    ajax_request({
        type: 'read_messages',
        params: {
            user_id: ls_get('active_contact_id'),
            guest_hash: ls_get('guest_hash'),
            sender_type: 'user'
        }
    }, function(result) {
        return true;
    });
}

// Проверить статус "онлайн" для контактов
function check_contacts_last_visit()
{
    var ls_contacts = ls_get('contacts');

    if (ls_contacts.length > 0)
    {
        ajax_request({
            type: 'check_guest_contacts_last_visit',
            params: {
                guest_hash: ls_get('guest_hash'),
                contacts: ls_get('contacts')
            }
        }, function(result) {
    
            var online_contacts = [];
            var contacts_items = $$$('#chat-modal .contacts ul').find('li');

            for (var i = 0; i < result.contacts_info.length; i++)
            {
                if (result.contacts_info[i].is_online == true)
                {
                    online_contacts.push(Number(result.contacts_info[i].user_id));
                }
            }
    
            for (var i = 0; i < contacts_items.length; i++)
            {
                var id = $$$(contacts_items[i]).data('contact-id');

                if (online_contacts.indexOf(id) != -1)
                {
                    $$$(contacts_items[i]).find('span.online')
                        .removeClass('no')
                        .attr('title', 'Поставщик он-лайн');
                }
                else
                {
                    $$$(contacts_items[i]).find('span.online')
                        .addClass('no')
                        .attr('title', 'Поставщик временно недоступен');
                }
            }
        });
    }
}

// Показать наличие непрочитанных сообщений для header
function set_header_unread_messages()
{
    if (!$$$('#chat-modal').hasClass('minimized'))
    {
        return false;
    }

    var has_unread = false;
    var contacts_items = $$$('#chat-modal .contacts ul').find('li');

    for (var i = 0; i < contacts_items.length; i++)
    {
        if (!$$$(contacts_items[i]).find('a.total-unread').hasClass('hidden'))
        {
            has_unread = true;
        }
    }

    if (has_unread)
    {
        $$$('#chat-modal .header').find('span.unread').removeClass('hidden');
    }
    else
    {
        $$$('#chat-modal .header').find('span.unread').addClass('hidden');
    }
}

// Показать наличие непрочитанных сообщений для favicon
function set_favico_unread_messages()
{
    var total = 0;
    var contacts_items = $$$('#chat-modal .contacts ul').find('li');

    for (var i = 0; i < contacts_items.length; i++)
    {
        if (!$$$(contacts_items[i]).find('a.total-unread').hasClass('hidden'))
        {
            total += Number($$$(contacts_items[i]).find('a.total-unread').text());
        }
    }

    if (total > 99)
    {
        total = 99;
    }

    favicon.badge(total);
}

// Показываем окно подписки на уведомления
function fb_show_popup_subscribe(content, states)
{
    if (states)
    {
        $$$('.js_subscribe_content').html(content);
        $$$('.js_popup_subcribe').removeClass('hidden');

        setTimeout(function () {
            $$$('.js_popup_subcribe').addClass('opacity');
        }, 200);
    }
    else
    {
        $$$('.js_popup_subcribe').removeClass('opacity');

        setTimeout(function () {
            $$$('.js_popup_subcribe').addClass('hidden');
        }, 200);
    }
}

// ---------- Функции хелперы ----------

// Шаблон для AJAX запроса
function ajax_request(data, success, error)
{
    data.csrf_token = ls_get('csrf_token');
    
    return $$$.ajax({
        url: host + '/index.php',
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function(data)
        {
            if (success)
            {
                success.call(this, data);
            }
            // else console.log(data);
        },
        error: function(jqXHR)
        {
            console.error(jqXHR);
        }
    });
}

// Получить параметр из Local Storage
function ls_get(key)
{
    var ls_key = 'chat_frontend:' + key;
    var param = JSON.parse(localStorage.getItem(ls_key));

    return (param !== null ) ? param : null;
}

// Установить параметр в Local Storage
function ls_set(key, value)
{
    var ls_key = 'chat_frontend:' + key;
    localStorage.setItem(ls_key, JSON.stringify(value));
}

// Обработка перевода строка в теги <br>
function nl2br(text)
{
    return text.replace(/\r?\n/g, '<br>');
}

// Удаление html тегов
function strip_tags(text)
{
    return text.replace(/<(?:.|\n)*?>/gm, '');
}