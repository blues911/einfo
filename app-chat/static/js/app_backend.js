/**
 * Функционал чата для app-backend.
 * 
 * Основные разделы файла:
 * - Настройки
 * - Функции начальной инициализации
 * - Вебсокеты
 * - События в окне чата
 * - Функции многоразоваого использования
 * - Функции хелперы
 */

// ---------- Настройки ----------

var $$$ = jQuery;
// meta[name="chat:xxx"]
var host = '//' + $$$('meta[name="chat:host"]').attr('content');
var host_ws = $$$('meta[name="chat:host_ws"]').attr('content');
var host_frontend = $$$('meta[name="chat:host_frontend"]').attr('content');
var last_vist_interval = $$$('meta[name="chat:last_visit_interval"]').attr('content');
// websocket
var ws_client = io.connect(host_ws, {query: {token: ls_get('csrf_token')}});
var ws_typing_timer;
var ws_typing_interval = 1500;
// firebase
var fb_config = {
    apiKey: "AIzaSyCPJ7aQj-ZQhdHb4QGvy6sfiYZtmVNNwFM",
    authDomain: "einfo-notification.firebaseapp.com",
    databaseURL: "https://einfo-notification.firebaseio.com",
    projectId: "einfo-notification",
    storageBucket: "einfo-notification.appspot.com",
    messagingSenderId: "593124678630",
    appId: "1:593124678630:web:9cd8ea94f997136f5c1427"
};
var fb_messaging = {};
// разное
var ns_config = {
    cursorcolor: '#b3b3b3',
    cursorborder: '1px solid #b3b3b3',
    background: '#e6e6e6',
    cursoropacitymin: 1,
    cursorwidth: '4px',
    cursorborderradius: '0px',
    horizrailenabled: false
};
var favicon = new Favico({
    animation: 'none'
});
var user_activity_interval = 60;
var last_vist_timer = null;
var user_activity_timer = null;

// ---------- Функции начальной инициализации ----------

// Из-за конфликта с prototype.js удаляем все prototype.toJSON
if (window.Prototype != undefined)
{
    delete Object.prototype.toJSON;
    delete Array.prototype.toJSON;
    delete Hash.prototype.toJSON;
    delete String.prototype.toJSON;
}

// Установка скроллбара
$$$('#chat-modal .contacts').niceScroll(ns_config);
$$$('#chat-modal .messages ul').niceScroll(ns_config);
$$$('#chat-modal .messages .form textarea').niceScroll(ns_config);

// Установка заголовков
$$$('#chat-modal .header strong').text('Онлайн-чат с клиентами');
$$$('#chat-modal .alert p').text('Хотите скрыть беседу?');
$$$('#chat-modal .form-blocked').text('Клиент завершил беседу');

// Инициализация firebase для push уведомлений
firebase.initializeApp(fb_config);
fb_messaging = firebase.messaging();

ajax_request({
    type: 'get_fcm_token',
    params: {
        user_id: ls_get('user_id'),
        sender_type: 'user',
        device_hash: ls_get('device_hash')
    }
}, function(result) {

    if (!result.fcm_token)
    {
        ls_set('fcm_token', null);
        init_fb_subscribe_content();
    }
    else
    {
        if (result.update_fcm_token == true)
        {
            fb_get_token();
        }
        else
        {
            ls_set('fcm_token', result.fcm_token);
            init_fb_subscribe_content();

            // Проверка состояния окна
            if (ls_get('is_window_minimized') == 1)
            {
                minimize_chat();
            }
        }
    }
});

// ---------- Вебсокеты ----------

// Новое сообщение от клиента
ws_client.on('new_message', function(response)
{
    var messages = $$$('#chat-modal .messages ul');
    var contacts = $$$('#chat-modal .contacts ul');
    var contacts_items = contacts.find('li');

    if (response.sender_type == 'guest'
        && response.user_id == ls_get('user_id')
        && response.guest_hash == ls_get('active_contact_hash'))
    {
        if ($$$('#chat-modal').hasClass('minimized'))
        {
            contacts_items.each(function()
            {
                var contact_hash = $$$(this).data('contact-hash');

                if (response.guest_hash == contact_hash)
                {
                    var el = $$$(this).find('.total-unread');
                    var total = Number(el.text()) + 1;
    
                    if (total > 99)
                    {
                        total = 99;
                    }
    
                    el.text(total);
                    el.removeClass('hidden');
    
                    $$$('#message-sound')[0].play();
                }
            });
    
            set_header_unread_messages();
        }
        else
        {
            read_messages();
        }

        messages.find('li.ms-left.dummy').remove();
        messages.append('<li class="ms-left">' + nl2br(response.text) + '</li>');
        messages.animate({
            scrollTop: messages.prop('scrollHeight')
        }, 'slow');
    }
    else if (response.sender_type == 'guest'
        && response.user_id == ls_get('user_id')
        && response.guest_hash != ls_get('active_contact_hash'))
    {
        contacts_items.each(function()
        {
            var contact_hash = $$$(this).data('contact-hash');

            if (response.guest_hash == contact_hash)
            {
                var el = $$$(this).find('.total-unread');
                var total = Number(el.text()) + 1;

                if (total > 99)
                {
                    total = 99;
                }

                el.text(total);
                el.removeClass('hidden');

                $$$('#message-sound')[0].play();
            }
        });

        set_header_unread_messages();
    }

    set_favico_unread_messages();
});

// Клиент начал печатать
ws_client.on('start_typing', function(response)
{
    var messages = $$$('#chat-modal .messages ul');

    if (response.sender_type == 'guest'
        && response.user_id == ls_get('user_id')
        && response.guest_hash == ls_get('active_contact_hash'))
    {
        if (messages.find('li.ms-left.dummy').length == 0)
        {
            messages.append('<li class="ms-left dummy"><span class="loading">печатает</span></li>');
            messages.animate({
                scrollTop: messages.prop('scrollHeight')
            }, 'slow');
        }
    }
});

// Клиент закончил печатать
ws_client.on('end_typing', function(response)
{
    var messages = $$$('#chat-modal .messages ul');

    if (response.sender_type == 'guest'
        && response.user_id == ls_get('user_id')
        && response.guest_hash == ls_get('active_contact_hash'))
    {
        messages.find('li.ms-left.dummy').remove();
        messages.animate({
            scrollTop: messages.prop("scrollHeight")
        }, "slow");
    }
});

// Клиент закрыл беседу
ws_client.on('close_messages', function(response)
{
    if (response.sender_type == 'guest'
        && response.user_id == ls_get('user_id')
        && response.guest_hash == ls_get('active_contact_hash'))
    {
        var contacts_items = $$$('#chat-modal .contacts ul').find('li');

        contacts_items.each(function()
        {
            var contact_hash = $$$(this).data('contact-hash');

            if (ls_get('active_contact_hash') == contact_hash)
            {
                block_form();
            }
        });
    }
});

// Появился новый контакт
ws_client.on('new_contact', function(response)
{
    if (response.sender_type == 'guest'
        && response.user_id == ls_get('user_id'))
    {
        var contacts = $$$('#chat-modal .contacts ul');

        if (ls_get('active_contact_hash') == null)
        {
            ls_set('active_contact_hash', response.guest_hash);

            contacts.append(set_contact_tpl(
                response.guest_hash,
                response.guest_title,
                'active'
            ));

            load_messages();
            show_chat();
        }
        else if (ls_get('active_contact_hash') == response.guest_hash)
        {
            load_messages();
        }
        else
        {
            if (contacts.find('li[data-contact-hash="' + response.guest_hash + '"]').length == 0)
            {
                contacts.append(set_contact_tpl(
                    response.guest_hash,
                    response.guest_title,
                    ''
                ));
            }

            $$$('#message-sound')[0].play();
            check_unread_messages();
        }
    }
});

// ---------- События в окне чата ----------

$$$(document).on('click', '.js_subscribe', function ()
{
    fb_messaging.requestPermission().then(function() {
        $$$('.js_subscribe_content').html('<p>Загрузка...</p>');
        fb_get_token();
    }).catch(function(err) {
        init_fb_subscribe_content();
        console.log(err);
    });
});

$$$(document).on('click', '.js_unsubscribe', function ()
{
    ajax_request({
        type: 'save_fcm_token',
        params: {
            fcm_token: '',
            owner_type: 'user',
            owner_value: ls_get('user_id')
        }
    }, function(result) {

        ls_set('fcm_token', null);
        $$$('#chat-modal .header li.close-modal').click();
    });
});

// Выбрать контакт
$$$(document).on('click', '#chat-modal .contacts a.title', function(e)
{
    e.preventDefault();

    var el = $$$(this).parent('li');

    ls_set('active_contact_hash', el.data('contact-hash'));

    el.addClass('active').siblings().removeClass('active');

    hide_alert();
    reset_form();
    load_messages();
});

// Удалить контакт (открыть окно подтверждения)
$$$(document).on('click', '#chat-modal .contacts img.delete-contact', function(e)
{
    e.preventDefault();

    var contact = $$$(this).parent('li');
    var contact_hash = contact.data('contact-hash');

    contact.addClass('active').siblings().removeClass('active');

    ls_set('active_contact_hash', contact_hash);

    show_alert();
    load_messages();
});

// Удалить контакт (закрыть окно подтверждения)
$$$(document).on('click', '#chat-modal .alert a.btn-no', function(e)
{
    e.preventDefault();

    hide_alert();
});

// Удалить контакт (закрыть окно подтверждения)
$$$(document).on('click', '#chat-modal .alert a.close', function(e)
{
    e.preventDefault();

    hide_alert();
});

// Удалить контакт
$$$(document).on('click', '#chat-modal .alert a.btn-yes', function(e)
{
    e.preventDefault();

    var contact = $$$('#chat-modal .contacts ul').find('li.active');
    var params = {
        user_id: ls_get('user_id'),
        guest_hash: ls_get('active_contact_hash'),
        sender_type: 'user',
        all: false
    };

    ajax_request({
        type: 'close_messages',
        params: params
    }, function(result) {

        contact.remove();
        $$$('#chat-modal .messages ul').empty();

        ws_client.emit('close_messages', params);

        var contacts_items = $$$('#chat-modal .contacts ul').find('li');

        if (contacts_items.length == 0)
        {
            ls_set('active_contact_hash', null);
            hide_chat();
        }
        else
        {
            $$$(contacts_items[0]).addClass('active');

            ls_set('active_contact_hash', $$$(contacts_items[0]).data('contact-hash'));
            load_messages();
        }

        hide_alert();
        reset_form();
    });
});

// Обработка формы и отправка сообщения
$$$(document).on('click', '#chat-modal .form a.btn-send', function(e)
{
    e.preventDefault();

    var form = $$$(this).parent('.form');
    var text = form.find('textarea').val();
    var messages = $$$('#chat-modal .messages ul');
    
    if (text.replace(/\s/g, '') == '' || text === '')
    {
        return false;
    }

    var params = {
        user_id: ls_get('user_id'),
        guest_hash: ls_get('active_contact_hash'),
        sender_type: 'user',
        text: strip_tags(text),
        search_position: 0,
        user_weak_activity: 0
    };

    ajax_request({
        type: 'add_message',
        params: params
    }, function(result) {

        ws_client.emit('new_message', params);

        messages.append('<li class="ms-right">' + nl2br(params.text) + '</li>');
        messages.animate({
            scrollTop: messages.prop('scrollHeight')
        }, 'slow');

        reset_form();
    });
});

// Проверка клавиши ENTER и эмуляция клика по кнопке
$$$(document).on('keydown', '#chat-modal .form textarea', function(e)
{
    if (e.keyCode == 13 && !e.shiftKey)
    {
        e.preventDefault();
        $$$('#chat-modal .form a.btn-send').click();
    }
});

// Сернуть/развернуть окно чата
$$$(document).on('click', '#chat-modal .header', function(e)
{
    e.preventDefault();
    
    if ($$$(e.target).hasClass('close-modal') || $$$(e.target).attr('alt') == 'close-modal')
    {
        return false;
    }

    var modal = $$$('#chat-modal');

    modal.hasClass('minimized') ? maximize_chat() : minimize_chat();
});

// Закрыть окно чата
$$$(document).on('click', '#chat-modal .header li.close-modal', function(e)
{
    e.preventDefault();

    ajax_request({
        type: 'close_messages',
        params: {
            user_id: ls_get('user_id'),
            guest_hash: ls_get('active_contact_hash'),
            sender_type: 'user',
            all: true
        }
    }, function(result) {

        var contacts_items = $$$('#chat-modal .contacts ul').find('li');

        contacts_items.each(function()
        {
            ws_client.emit('close_messages', {
                user_id: ls_get('user_id'),
                guest_hash: $$$(this).data('contact-hash'),
                sender_type: 'user',
                all: true
            });
        });

        $$$('#chat-modal .contacts ul').empty();

        ls_set('active_contact_hash', null);
        hide_alert();
        hide_chat();

        if (ls_get('fcm_token') == null)
        {
            ws_client.disconnect();
            clear_interval_params();

            if (window.location.search == '?module=users_chat&action=list')
            {
                init_fb_subscribe_content();
            }
        }
    });
});

// Проверить старт набора текста в форме
$$$(document).on('keyup', '#chat-modal .form textarea', function(e)
{
    var typing_params = {
        user_id: ls_get('user_id'),
        guest_hash: ls_get('active_contact_hash'),
        sender_type: 'user',
    };

    if (e.keyCode != 13)
    {
        ws_client.emit('start_typing', typing_params);
    }

    clearTimeout(ws_typing_timer);

    ws_typing_timer = setTimeout(function() {
        ws_client.emit('end_typing', typing_params);
    }, ws_typing_interval);
});

// Проверить стоп набора текста в форме
$$$(document).on('keydown', '#chat-modal .form textarea', function()
{
    clearTimeout(ws_typing_timer);
}); 

// ---------- Функции многоразоваого использования ----------

// Открыть чат
function show_chat()
{
    $$$('#chat-modal').removeClass('hidden');
}

// Закрыть чат
function hide_chat()
{
    $$$('#chat-modal').addClass('hidden');
    ls_set('is_window_minimized', 0);
    set_favico_unread_messages();
}

// Свернуть окно чата
function minimize_chat()
{
    var modal = $$$('#chat-modal');

    modal.addClass('minimized');
    modal.find('.header li.toggle-modal img').attr('src', host + '/static/icons/maximize-modal.svg');

    ls_set('is_window_minimized', 1);
    set_header_unread_messages();
    set_favico_unread_messages();
}

// Развернуть окно чата
function maximize_chat()
{
    var modal = $$$('#chat-modal');
    var contact = $$$('#chat-modal .contacts ul').find('li.active');

    contact.find('.total-unread').addClass('hidden').text(0);
    modal.removeClass('minimized');
    modal.find('.header li.toggle-modal img').attr('src', host + '/static/icons/minimize-modal.svg');

    $$$('#chat-modal .header').find('span.unread').addClass('hidden');

    ls_set('is_window_minimized', 0);
    read_messages();
    set_favico_unread_messages();
}

// Открыть окно уведомления
function show_alert()
{
    var messages = $$$('#chat-modal .messages');

    messages.find('.alert-overlay').addClass('active');
    messages.find('.alert').addClass('active');
}

// Скрыть окно уведомления
function hide_alert()
{
    var messages = $$$('#chat-modal .messages');

    messages.find('.alert-overlay').removeClass('active');
    messages.find('.alert').removeClass('active');
}

// Очистить поле ввода
function reset_form()
{
    var form = $$$('#chat-modal .form');
    
    form.find('textarea').val('');
}

// Заблокировать форму ввода
function block_form()
{
    $$$('#chat-modal .form-blocked').removeClass('hidden');
    $$$('#chat-modal .form').addClass('hidden');
}

// Разблокировать форму ввода
function unblock_form()
{
    $$$('#chat-modal .form-blocked').addClass('hidden');
    $$$('#chat-modal .form').removeClass('hidden');
}

// Шаблон контакта
function set_contact_tpl(hash, title, is_active)
{
    return '<li class="'+is_active+'" data-contact-hash="'+hash+'">' +
        '<a href="#" class="title">' +
            '<span class="title-common">Клиент с сайта</span>' +
            '<span class="title-num">'+title+'</span>' +
            '</a>' +
        '<a href="#" class="total-unread hidden">0</a>' +
        '<img class="delete-contact" src="'+host+'/static/icons/delete-contact.svg" alt="delete-contact">' +
        '</li>';
}

// Загрузить сообщения
function load_messages()
{
    ajax_request({
        type: 'get_messages',
        params: {
            user_id: ls_get('user_id'),
            guest_hash: ls_get('active_contact_hash')
        }
    }, function(result) {
        var closed_by_guest = false;
        var messages = $$$('#chat-modal .messages ul');
        var contact = $$$('#chat-modal .contacts ul').find('li.active');

        messages.empty();

        if (!$$$('#chat-modal').hasClass('minimized'))
        {
            contact.find('.total-unread').addClass('hidden').text(0);
            read_messages();
        }

        set_header_unread_messages();
        set_favico_unread_messages();
    
        if (result.messages.length > 0)
        {
            for (var i = 0; i < result.messages.length; i++)
            {
                if (result.messages[i].sender_type == 'user' && result.messages[i].user_weak_activity == 0)
                {
                    messages.append('<li class="ms-right">' + nl2br(result.messages[i].text) + '</li>');
                }
                if (result.messages[i].sender_type == 'guest')
                {
                    if (result.messages[i].search_position == 1)
                    {
                        var text = result.messages[i].text.split(': ');
                        var href = '//' + host_frontend + '/store/' + text[1] + '/';

                        messages.append('<li class="ms-left srv">' + text[0] + ': <a href="'+ href +'" target="_blank">' + text[1] + '</a></li>');
                    }
                    else
                    {
                        messages.append('<li class="ms-left">' + nl2br(result.messages[i].text) + '</li>');
                    }
                    
                    if (result.messages[i].closed_by_guest == 1)
                    {
                        closed_by_guest = true;
                    }
                }
            }
        }

        (closed_by_guest == true) ? block_form() : unblock_form();

        messages.animate({
            scrollTop: messages.prop('scrollHeight')
        }, 'slow');
    });
}

// Прочитать сообщения
function read_messages()
{
    ajax_request({
        type: 'read_messages',
        params: {
            user_id: ls_get('user_id'),
            guest_hash: ls_get('active_contact_hash'),
            sender_type: 'guest'
        }
    }, function(result) {
        return true;
    });
}

// Проверка непрочитанных сообщений
function check_unread_messages()
{
    ajax_request({
        type: 'count_unread_messages',
        params: {
            user_id: ls_get('user_id'),
            sender_type: 'guest'
        }
    }, function(result) {
    
        var contacts_items = $$$('#chat-modal .contacts ul').find('li');
    
        if (contacts_items.length > 0 && result.messages_info.length > 0)
        {
            for (var i = 0; i < contacts_items.length; i++)
            {
                for (var j = 0; j < result.messages_info.length; j++)
                {
                    if (!$$$('#chat-modal').hasClass('minimized'))
                    {
                        if (!$$$(contacts_items[i]).hasClass('active')
                            && $$$(contacts_items[i]).data('contact-hash') == result.messages_info[j].guest_hash
                            && result.messages_info[j].total_unread > 0)
                        {
                            var total = result.messages_info[j].total_unread;
        
                            if (total > 99)
                            {
                                total = 99;
                            }
        
                            $$$(contacts_items[i]).find('.total-unread')
                                .removeClass('hidden')
                                .text(total);
                        }
                    }
                    else
                    {
                        if ($$$(contacts_items[i]).data('contact-hash') == result.messages_info[j].guest_hash
                            && result.messages_info[j].total_unread > 0)
                        {
                            var total = result.messages_info[j].total_unread;
        
                            if (total > 99)
                            {
                                total = 99;
                            }
        
                            $$$(contacts_items[i]).find('.total-unread')
                                .removeClass('hidden')
                                .text(total);
                        }
                    }
                }
            }

            set_header_unread_messages();
            set_favico_unread_messages();
        }
    });
}

// Обновление онлайн статуса
function update_last_visit()
{
    ajax_request({
        type: 'update_user_last_visit',
        params: {
            user_id: ls_get('user_id')
        }
    }, function(result) {
        return true;
    });
}

// Показать наличие непрочитанных сообщений для header
function set_header_unread_messages()
{
    if (!$$$('#chat-modal').hasClass('minimized'))
    {
        return false;
    }

    var has_unread = false;
    var contacts_items = $$$('#chat-modal .contacts ul').find('li');

    for (var i = 0; i < contacts_items.length; i++)
    {
        if (!$$$(contacts_items[i]).find('a.total-unread').hasClass('hidden'))
        {
            has_unread = true;
        }
    }

    if (has_unread)
    {
        $$$('#chat-modal .header').find('span.unread').removeClass('hidden');
    }
    else
    {
        $$$('#chat-modal .header').find('span.unread').addClass('hidden');
    }
}

// Показать наличие непрочитанных сообщений для favicon
function set_favico_unread_messages()
{
    var total = 0;
    var contacts_items = $$$('#chat-modal .contacts ul').find('li');

    for (var i = 0; i < contacts_items.length; i++)
    {
        if (!$$$(contacts_items[i]).find('a.total-unread').hasClass('hidden'))
        {
            total += Number($$$(contacts_items[i]).find('a.total-unread').text());
        }
    }

    if (total > 99)
    {
        total = 99;
    }

    favicon.badge(total);
}

// Проверка контактов и загрузка сообщений
function check_contacts()
{
    ajax_request({
        type: 'get_user_contacts',
        params: {
            user_id: ls_get('user_id')
        }
    }, function(result) {

        var contacts = $$$('#chat-modal .contacts ul');
        contacts.empty();

        if (result.contacts.length > 0)
        {
            for (var i = 0; i < result.contacts.length; i++)
            {
                contacts.append(set_contact_tpl(
                    result.contacts[i].guest_hash,
                    result.contacts[i].guest_title,
                    (ls_get('active_contact_hash') != null && ls_get('active_contact_hash') == result.contacts[i].guest_hash) ? 'active' : ''
                ));
            }

            if (ls_get('active_contact_hash') == null)
            {
                contacts.find('li').first().addClass('active');

                ls_set('active_contact_hash', contacts.find('li').first().data('contact-hash'));
            }

            load_messages();
            check_unread_messages();
            show_chat();
        }
    });
}

// Проверка активности поставщика в чате
function chat_activity()
{
    if (ls_get('active_contact_hash') == null)
    {
        return false;
    }

    ajax_request({
        type: 'check_user_activity',
        params: {
            user_id: ls_get('user_id'),
            sender_type: 'user'
        }
    }, function(result) {

        // Деактивация чата если поставщик не отвечает на сообщения согласно лимиту
        if (result.weak_activity == true)
        {
            for (var i = 0; i < result.guest_hashes.length; i++)
            {
                ws_client.emit('new_message', {
                    user_id: ls_get('user_id'),
                    guest_hash: result.guest_hashes[i],
                    sender_type: 'user',
                    text: result.service_message,
                    search_position: 0,
                    user_weak_activity: 1
                });
            }

            ajax_request({
                type: 'save_fcm_token',
                params: {
                    fcm_token: '',
                    owner_type: 'user',
                    owner_value: ls_get('user_id')
                }
            }, function(result) {

                ls_set('fcm_token', null);
                $$$('#chat-modal .header li.close-modal').click();
            });
        }
    });
}

// Получение FCM токена
function fb_get_token()
{
    var retry = 0;
    var get_token = function()
    {
        fb_messaging.getToken()
        .then(function(token) {
            if (token)
            {
                ajax_request({
                    type: 'save_fcm_token',
                    params: {
                        fcm_token: token,
                        owner_type: 'user',
                        owner_value: ls_get('user_id')
                    }
                }, function(result) {

                    ls_set('fcm_token', token);
                    init_fb_subscribe_content();
                });
            }
            else 
            {
                ls_set('fcm_token', null);
                init_fb_subscribe_content();

                console.log('Не удалось получить токен.');
            }
        }).catch(function (err) {
            if (retry == 0)
            {
                // Ошибка с переключением разрешения на уведомления
                // https://github.com/firebase/firebase-js-sdk/issues/2364
                retry = 1;
                get_token();
            }
            else
            {
                ls_set('fcm_token', null);
                init_fb_subscribe_content();

                console.log(err);
            }
        });
    };

    get_token();
}

// Отображение контента в разделе "Чат" с учетом Notification.permission
function init_fb_subscribe_content()
{
    var subscribe_content = $$$('.js_subscribe_content');
    var chat_table = $$$('.js_chat_table');

    if (Notification.permission === 'granted' && ls_get('fcm_token') != null)
    {
        ws_client.connect();

        // Обновление статуса "онлайн"
        update_last_visit();
        last_vist_timer = setInterval(function() { 
            update_last_visit();
        }, last_vist_interval * 1000);

        // Проверка активности поставщика в чате
        user_activity_timer = setInterval(function() {
            chat_activity();
        }, user_activity_interval * 1000);

        subscribe_content.html('<p class="green">Вам доступен чат к использованию.</p>' +
            '<button class="js_unsubscribe" type="button">Отключить чат</button>');

        chat_table.show();

        // Загрузка активных диалогов
        check_contacts();
    }
    else if (Notification.permission == 'denied')
    {
        ws_client.disconnect();

        ls_set('fcm_token', null);
        clear_interval_params();

        subscribe_content.html('' +
            '<p class="red"><b>Внимание!</b> Вы заблокировали получение уведомление. Для использования чата необходимо дать разрешение на получение уведомлений.</p>' +
            '<p>Чтобы разрешить получение уведомлений, откройте <b>сведения о сайте</b> <img src="/i/notification.png">, а затем выберите <b>уведомление</b> <img src="/i/notification_icon.png"> и <b>разрешите</b> их получение.</b></p>'
        );
    }
    else
    {
        ws_client.disconnect();

        ls_set('fcm_token', null);
        clear_interval_params();

        subscribe_content.html('' +
            '<p>Для использования чата необходимо дать разрешение на получение уведомлений.</p>' +
            '<button class="js_subscribe" type="button">Подписаться на уведомления</button>'
        );

        chat_table.hide();
    }
}

// Очистить функции и параметры в инетрвале
function clear_interval_params()
{
    clearInterval(last_vist_timer);
    last_vist_timer = null;
    clearInterval(user_activity_timer);
    user_activity_timer = null;
}

// ---------- Функции хелперы ----------

// Шаблон для AJAX запроса
function ajax_request(data, success, error)
{
    data.csrf_token = ls_get('csrf_token');

    return $$$.ajax({
        url: host + '/index.php',
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function(data)
        {
            if (success)
            {
                success.call(this, data);
            }
            // else console.log(data);
        },
        error: function(jqXHR)
        {
            console.error(jqXHR);
        }
    });
}

// Получить параметр из Local Storage
function ls_get(key)
{
    var ls_key = 'chat_backend:' + key;
    var param = JSON.parse(localStorage.getItem(ls_key));

    return (param !== null ) ? param : null;
}

// Установить параметр в Local Storage
function ls_set(key, value)
{
    var ls_key = 'chat_backend:' + key;
    localStorage.setItem(ls_key, JSON.stringify(value));
}

// Обработка перевода строка в теги <br>
function nl2br(text)
{
    return text.replace(/\r?\n/g, '<br>');
}

// Удаление html тегов
function strip_tags(text)
{
    return text.replace(/<(?:.|\n)*?>/gm, '');
}