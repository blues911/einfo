-----------------------
Чат
-----------------------

Настройки
-------------------------
Корень проекта /app-chat.

Настройки NodeJS:
1) Установка npm пакетов: npm install
2) Создать config.js, скопировать в него конфиги из config.js.dist и указать (host, port, ssl_key, ssl_cert) для веб-сокетов.
3) Запуск сокет сервера: node server.js

Настройки PHP:
1) Поднять хост (Apache или Nginx), корневой файл index.php.
2) Указать адреса хостов (HOST_CHAT, HOST_CHAT_WS) в config.php (который для всех приложений).

Структура и логика работы чата
-------------------------

Основные части:
1) клиент
  - /app-chat/static/js/libs/jquery-3.3.1.min.js
  - /app-chat/static/js/libs/jquery.nicescroll.min.js
  - /app-chat/static/js/libs/socket.io.js
  - /app-chat/static/js/libs/firebase-7.2.0.min.js
  - /app-chat/static/js/init.js
  - /app-chat/static/js/app_frontend.js
  - /app-chat/static/js/app_backend.js
2) сервер
  - /app-chat/server.js
  - /app-chat/index.php

* в config.php есть параметры для настроек чата (пример: CHAT_)

socket.io.js - для работы вебсокетов
firebase-7.2.0.min.js - для push уведомлений в браузере
init.js - необходим для подгрузки всех библиотек чата, !тут важно соблюдение порядка
  загрузки js файлов для исключения возможных ошибок, также происходит инициализация
  Local Storage параметров и создание csrf_token

:[клиент]
Клиентская часть делится на:
  - app_backend.js (для поставщика)
  - app_frontend.js (для гостей и пользователей сайта)

Клиентская часть работает на основе Local Storage (LS) параметров.
app_backend.js LS параметры:
  - chat_backend:user_id
  - chat_backend:active_contact_hash
  - chat_backend:is_window_minimized
  - chat_backend:csrf_token
  - chat_backend:fcm_token
app_frontend.js LS параметры:
  - chat_frontend:guest_hash
  - chat_frontend:contacts
  - chat_frontend:active_contact_id
  - chat_frontend:is_window_minimized
  - chat_frontend:csrf_token
  - chat_frontend:fcm_token

csrf_token - токен для доступа к серверной части и вебсокет серверу
fcm_token - токен для push уведомлений (firebase)

:[сервер]
Серверная часть состоит из:
  - server.js (вебсокет сервер на NodeJS)
  - index.php (для обработки запросов со стороны клиента и работы с MySQL)

Запуск чата:
* гость или пользователь сайта при клике на "Онлайн-чат", должен разрешить работу
push уведомлений в браузере, после чего запускается чат с выбранным поставщиком.
* поставщик должен в админке в разделе "Чат" кликнуть "Подписаться на уведомления"
чтобы начал работать функционал чата.

[!] Для востановления чата:
1) Добавить в config.php
  - define('HOST_CHAT', 'einfo-chat.ru');
  - define('HOST_CHAT_WS', 'einfo-chat.ru:6001');
  - define('PATH_CHAT', PATH_ROOT . '/app-chat');
  - define('CHAT_LAST_VISIT_INTERVAL', 20);
  - define('CHAT_USER_WEAK_ACTIVITY', 300);
  - define('CHAT_IGNORED_GUESTS_LIMIT', 5);
  - define('CHAT_FCM_API_KEY', 'AAAAihj8W-Y:APA91bFYRMgDOfO0DIHqmpmrBSqzMYlKq4NUD9UfJiddMBcGYIjhDR_o_yt8WMOFelCdSY3_KL8lmiYR2Bh8vwdTZN9H9VSBV9iBqZUOZ37pORWTXTT7lV7YQc1S8U5RBjoi99oHIaIL');
2) Добавить таблицы в einfo_main
CREATE TABLE `users_chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `guest_hash` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `sender_type` enum('user','guest') NOT NULL DEFAULT 'guest',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `closed_by_guest` tinyint(1) NOT NULL DEFAULT '0',
  `closed_by_user` tinyint(1) NOT NULL DEFAULT '0',
  `search_position` tinyint(1) NOT NULL DEFAULT '0',
  `user_weak_activity` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=cp1251;
CREATE TABLE `users_chat_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner_type` enum('user','guest') NOT NULL DEFAULT 'guest',
  `owner_value` varchar(50) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `token` varchar(255) NOT NULL,
  `fcm_token` varchar(255) NOT NULL,
  `fcm_last_update` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  `device_hash` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2640 DEFAULT CHARSET=cp1251;
CREATE TABLE `users_last_visit` (
  `user_id` int(10) unsigned NOT NULL,
  `last_visit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `chat_last_banned` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;
3) Создать firebase проект

-----------------------
FCM - Firebase Cloud Messaging (PUSH уведомления)
-----------------------

1. Создать проект - https://console.firebase.google.com/.
2. Создать приложение, а затем получить параметр "messagingSenderId" из Firebase SDK snippet, и заменить на текущий в firebase-messaging-sw.js (в каждом модуле!)
3. Получить ключ сервера в FCM (Настроеки проекта -> Cloud Messaging) и подставить его в конфиг (CHAT_FCM_API_KEY).