<?php

function dd($var)
{
		echo '<pre/>';
    print_r($var);
    die;
}

function plural($num, $one, $three, $five)
{
    $n = (int)$num;
    $forms = array($one, $three, $five);
    if (count($forms) == 3)
    {
        $plural = (($n % 10 == 1 && $n % 100 != 11) ? 0 : (($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)) ? 1 : 2));
        return $forms[$plural];
    }
    else
    {
        return null;
    }
}
