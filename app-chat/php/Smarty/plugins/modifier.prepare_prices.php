<?php
/*
 * Smarty plugin
 * -----------------------------
 * Файл:	modifier.prepare_prices.php
 * Тип:		modifier
 * Имя:		prepare_prices
 * Назначение: преобразует цены из json
 * ------------------------------
 */

require_once $smarty->_get_plugin_filepath('modifier','format_price');

function smarty_modifier_prepare_prices($data)
{
    $result = '';
    $prices = json_decode($data, true);

    if (!empty($prices))
    {
        $prices_total = count($prices);
        $i = 1;

        foreach ($prices as $price)
        {
            if ($prices_total == 1 && $price['p'] > 0)
            {
                $pc = smarty_modifier_format_price($price['p']) . ' руб.';

                $result .= $pc;
            }
            else if ($prices_total > 1 && $price['p'] > 0)
            {
                $qty = ($i == $prices_total) ? $price['n'] . '+ шт: ' : $price['n'] . ' шт: ';
                $pc = smarty_modifier_format_price($price['p']) . ' руб.';

                $result .= $qty . $pc . '<br/>';
                $i++;
            }
        }
    }

    if (empty($result)) $result = '&mdash;';

    return $result;
}

?>
