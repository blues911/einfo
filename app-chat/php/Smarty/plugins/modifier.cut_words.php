<?php

/*
 * Smarty plugin
 * -----------------------------
 * ����:    modifier.cut_words.php
 * ���:        modifier
 * ���:        cut_words
 * ����������:    ������� ������� ������� ����
 * ------------------------------
 */

function smarty_modifier_cut_words($string, $max)
{
    $words = explode(" ", $string);
    foreach ($words as $k => $value)
    {
        if (strlen($value) > $max)
        {
            $value = preg_replace("/[.,;]/", " ", $value);
            $value = preg_replace("/(\S{".$max."})/", "$1 ", $value);
            $words[$k] = $value;
        }
    }

    return implode(" ", $words);
}

?>
