<?php

include_once("../config.php");
include_once(PATH_CHAT . "/php/function.php");
include_once(PATH_CHAT . "/php/adodb/adodb.inc.php");
include_once(PATH_CHAT . "/php/classes/class.phpmailer.php");
include_once(PATH_CHAT . "/php/Smarty/Smarty.class.php");

$result = array();
$result['success'] = false;

if ($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($_POST['type']))
{
    $result['message'] = 'Method Not Allowed';

    json_response($result, 405);
    die();
}

// Подключение к БД
$DB = ADONewConnection(DB_MAIN_DSN);
$DB->SetFetchMode(ADODB_FETCH_ASSOC);
$DB->Execute("SET NAMES utf8");
$DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
$DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
$DB->cacheSecs = DB_CACHE_TIME;
// $DB->debug = true;

// шаблонизатор
$smarty = new Smarty;
$smarty->template_dir = PATH_CHAT . "/templates";
$smarty->compile_dir = PATH_TMP . "/tpl-user";

// Инициализировать токен
if ($_POST['type'] == 'init_token')
{
    $token = md5(mt_rand());
    $date = date("Y-m-d H:i:s");
    $params = $_POST['params'];

    if (isset($params['update']) && $params['update'] == true)
    {
        $csrf_token = $_POST['csrf_token'];

        $DB->Execute('UPDATE users_chat_tokens
                        SET is_active = 0
                        WHERE owner_type = ?
                            AND owner_value = ?
                            AND token = ?', array(
                                                $params['owner_type'],
                                                $params['owner_value'],
                                                $csrf_token));
    }

    $data = $DB->GetRow('SELECT *
                            FROM users_chat_tokens
                            WHERE owner_type = ?
                                AND owner_value = ?
                                AND is_active = 1', array(
                                                        $params['owner_type'],
                                                        $params['owner_value']));

    if (!empty($data))
    {
        $token = $data['token'];
    }
    else
    {
        $DB->Execute('INSERT INTO users_chat_tokens
                        SET
                            owner_type = ?,
                            owner_value = ?,
                            ip = ?,
                            token = ?,
                            date = ?', array(
                $params['owner_type'],
                $params['owner_value'],
                $_SERVER['REMOTE_ADDR'],
                $token,
                $date
            )
        );
    }

    $result['token'] = $token;
    $result['success'] = true;

    json_response($result, 200);
}

// Сохранить fcm токен
else if ($_POST['type'] == 'save_fcm_token')
{
    $csrf_token = $_POST['csrf_token'];
    $params = $_POST['params'];

    check_token($DB, $csrf_token);

    $date = date('Y-m-d H:i:s');

    $DB->Execute('UPDATE users_chat_tokens
                        SET fcm_token = ?,
                        	fcm_last_update = "' . $date . '"
                        WHERE owner_type = ?
                            AND owner_value = ?
                            AND token = ?', array(
            $params['fcm_token'],
            $params['owner_type'],
            $params['owner_value'],
            $csrf_token
        )
    );

    $result['success'] = true;
    json_response($result, 200);
}

// Получить fcm токен
else if ($_POST['type'] == 'get_fcm_token')
{
    $csrf_token = $_POST['csrf_token'];
    $params = $_POST['params'];

    check_token($DB, $csrf_token);

    $fcm_token_id = get_fcm_token_by_owner(
        $DB,
        $params['sender_type'] == 'user' ? 'user' : 'guest',
        $params['sender_type'] == 'user' ? $params['user_id'] : $params['guest_hash']
    );

    if ($params['sender_type'] == 'user')
    {
        $result['update_fcm_token'] = false;

        $device_hash = $DB->GetOne('SELECT device_hash
                                            FROM users_chat_tokens
                                            WHERE owner_type = ?
                                                AND owner_value = ?
                                                AND is_active = 1', array('user', $params['user_id']));

        if (!$device_hash || $device_hash != $params['device_hash'])
        {
            $DB->Execute('UPDATE users_chat_tokens
                                  SET device_hash = ?
                                  WHERE owner_type = ?
                                      AND owner_value = ?
                                      AND is_active = 1', array(
                                                          $params['device_hash'],
                                                          'user',
                                                          $params['user_id']));

            $result['update_fcm_token'] = true;
        }
    }
    
    $result['success'] = true;
    $result['fcm_token'] = $fcm_token_id;

    json_response($result, 200);
}

// Добавить новое сообщение
else if ($_POST['type'] == 'add_message')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];
    $date = date("Y-m-d H:i:s");
    $text = strip_tags($params['text']);

    $DB->Execute('INSERT INTO users_chat
                    SET
                        user_id = ?,
                        guest_hash = ?,
                        text = ?,
                        sender_type = ?,
                        date = ?', array(
            $params['user_id'],
            $params['guest_hash'],
            $text,
            $params['sender_type'],
            $date
        )
    );

    // Проверка FCM token и отправка PUSH-уведомления
    $fcm_token_id = get_fcm_token_by_owner(
        $DB,
        $params['sender_type'] == 'user' ? 'guest' : 'user',
        $params['sender_type'] == 'user' ? $params['guest_hash'] : $params['user_id']
    );

    if (!empty($fcm_token_id))
    {
        $title = $params['sender_type'] == 'user' ? 'Сообщение от поставщика' : 'Новое сообщение от клиента';

        if ($params['sender_type'] == 'user')
        {
            $user_title = $DB->GetOne('SELECT title FROM users WHERE id = ?', array($params['user_id']));
            $title .= ' ' . $user_title;
        }

        $result['response'] = send_notification($fcm_token_id, $title, $text);
    }
    else 
    {
        $result['response'] = 'Token is null';
    }

    $result['success'] = true;

    json_response($result, 200);
}

// Получить список собщений
else if ($_POST['type'] == 'get_messages')
{
    $csrf_token = $_POST['csrf_token'];
    $params = $_POST['params'];

    check_token($DB, $csrf_token);

    $messages = $DB->GetAll('SELECT *
                                FROM users_chat
                                WHERE user_id = ?
                                    AND guest_hash = ?', array(
                                                            $params['user_id'],
                                                            $params['guest_hash']));

    $result['messages'] = $messages;
    $result['success'] = true;

    json_response($result, 200);
}

// Подсчет непрочитанных сообщений для активного контакта
else if ($_POST['type'] == 'count_unread_messages')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];
    $messages_info = array();

    if ($params['sender_type'] == 'guest')
    {
        $messages_info = $DB->GetAll('SELECT
                                            guest_hash,
                                            COUNT(*) as total_unread
                                        FROM users_chat
                                        WHERE user_id = ?
                                            AND sender_type = "guest"
                                            AND is_read = 0
                                        GROUP BY guest_hash', array($params['user_id']));
    }
    else if ($params['sender_type'] == 'user')
    {
        $messages_info = $DB->GetAll('SELECT
                                            user_id,
                                            COUNT(*) as total_unread
                                        FROM users_chat
                                        WHERE guest_hash = ?
                                            AND sender_type = "user"
                                            AND is_read = 0
                                        GROUP BY user_id', array($params['guest_hash']));

    }

    $result['messages_info'] = $messages_info;
    $result['success'] = true;

    json_response($result, 200);
}

// Отметить сообщения как прочитанные для активного контакта
else if ($_POST['type'] == 'read_messages')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];

    $DB->Execute('UPDATE users_chat
                    SET is_read = 1
                    WHERE user_id = ?
                        AND guest_hash = ?
                        AND sender_type = ?', array(
                                                $params['user_id'],
                                                $params['guest_hash'],
                                                $params['sender_type']));

    $result['success'] = true;

    json_response($result, 200);
}

// Завершить историю сообщений
else if ($_POST['type'] == 'close_messages')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];
    $sql_closed_by = ($params['sender_type'] == 'user') ? 'closed_by_user' : 'closed_by_guest';

    if ($params['all'] == true)
    {
        $user_or_guest = ($params['sender_type'] == 'user') ? 'user_id' : 'guest_hash';
        $id_or_hash = ($params['sender_type'] == 'user') ? $params['user_id'] : $params['guest_hash'];

        // Для всех контактов
        $DB->Execute("UPDATE users_chat SET $sql_closed_by = 1 WHERE $user_or_guest = ?", array($id_or_hash));
    }
    else
    {
        // Для одного контакта
        $DB->Execute("UPDATE users_chat
                        SET $sql_closed_by = 1
                        WHERE user_id = ?
                            AND guest_hash = ?", array(
                                                    $params['user_id'],
                                                    $params['guest_hash']));
    }

    $result['success'] = true;

    json_response($result, 200);
}

// Получить список контактов для поставщика
else if ($_POST['type'] == 'get_user_contacts')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];
    $contacts = array();

    $items = $DB->GetAll('SELECT users_chat.guest_hash, users_chat_tokens.ip AS guest_ip
                            FROM users_chat
                            LEFT JOIN users_chat_tokens
                                ON users_chat_tokens.owner_value = users_chat.guest_hash
                            WHERE users_chat.user_id = ?
                                AND users_chat.closed_by_user = 0
                                AND users_chat_tokens.is_active = 1
                            GROUP BY users_chat.guest_hash
                            ORDER BY users_chat_tokens.fcm_last_update ASC', array($params['user_id']));

    if (!empty($items))
    {
        foreach ($items as $item)
        {
            $city_name = $DB->GetOne('SELECT ip2ruscity_cities.city AS city_name
                                        FROM ip2ruscity_ip_compact
                                        LEFT JOIN ip2ruscity_cities
                                            ON ip2ruscity_cities.city_id = ip2ruscity_ip_compact.city_id
                                        WHERE INET_ATON(?) BETWEEN ip2ruscity_ip_compact.num_ip_start
                                            AND ip2ruscity_ip_compact.num_ip_end', array($item['guest_ip']));

            $contacts[] = array(
                'guest_hash' => $item['guest_hash'],
                'guest_title' => !empty($city_name) ? $item['guest_ip'] . ' ('. $city_name .')' : $item['guest_ip']
            );
        }
    }

    $result['contacts'] = $contacts;
    $result['success'] = true;

    json_response($result, 200);
}

// Проверить наличие сообщений для клиента
else if ($_POST['type'] == 'check_guest_contact_messages')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];

    $date = date("Y-m-d H:i:s");

    // Добавить сообщение типа "Запрос на позицию"
    $DB->Execute('INSERT INTO users_chat
                    SET
                        user_id = ?,
                        guest_hash = ?,
                        text = ?,
                        sender_type = ?,
                        search_position = 1,
                        date = ?', array(
                                        $params['user_id'],
                                        $params['guest_hash'],
                                        $params['text'],
                                        $params['sender_type'],
                                        $date));

    // Проверка FCM token и отправка PUSH-уведомления
    $fcm_token_id = get_fcm_token_by_owner($DB, 'user', $params['user_id']);

    if (!empty($fcm_token_id))
    {
        $result['response'] = send_notification($fcm_token_id, 'Новое сообщение от клиента', $params['text']);
    }
    else 
    {
        $result['response'] = 'Token is null';
    }

    // Выборка истории сообщений
    $messages = $DB->GetAll('SELECT *
                            FROM users_chat
                            WHERE user_id = ?
                                AND guest_hash = ?', array(
                                                        $params['user_id'],
                                                        $params['guest_hash']));

    // Сформировать название клиента
    $guest_ip = $DB->GetOne('SELECT users_chat_tokens.ip AS guest_ip
                                FROM users_chat_tokens
                                WHERE users_chat_tokens.owner_value = ?
                                    AND users_chat_tokens.is_active = 1', array($params['guest_hash']));

    $city_name = $DB->GetOne('SELECT ip2ruscity_cities.city AS city_name
                                  FROM ip2ruscity_ip_compact
                                  LEFT JOIN ip2ruscity_cities
                                      ON ip2ruscity_cities.city_id = ip2ruscity_ip_compact.city_id
                                  WHERE INET_ATON(?) BETWEEN ip2ruscity_ip_compact.num_ip_start
                                      AND ip2ruscity_ip_compact.num_ip_end', array($guest_ip));

    $result['guest_title'] = !empty($city_name) ? $guest_ip . ' ('. $city_name .')' : $guest_ip;

    if (!empty($messages))
    {
        $DB->Execute('UPDATE users_chat
                        SET
                            closed_by_guest = 0,
                            closed_by_user = 0
                        WHERE user_id = ?
                            AND guest_hash = ?', array(
                                                    $params['user_id'],
                                                    $params['guest_hash']));

        $result['success'] = true;
    }

    json_response($result, 200);
}

// Проверить "онлайн" статус контактов (поставщиков) для клиента
else if ($_POST['type'] == 'check_guest_contacts_last_visit')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];

    $ids = array();
    $contacts_info = array();

    foreach ($params['contacts'] as $contact)
    {
        $ids[] = (int)$contact['id'];
    }

    $users_last_visit = $DB->GetAll('SELECT * FROM users_last_visit WHERE user_id IN (' . implode(', ', $ids) . ')');

    foreach ($users_last_visit as $user_last_visit)
    {
        $now = strtotime(date('Y-m-d H:i:s'));
        $last_visit = strtotime($user_last_visit['last_visit']);

        $contacts_info[] = array(
            'user_id' => $user_last_visit['user_id'],
            'is_online' => (($now - $last_visit) <= 20) ? true : false
        );
    }

    $result['contacts_info'] = $contacts_info;
    $result['success'] = true;

    json_response($result, 200);
}

// Обновить "онлайн" статус поставщика
else if ($_POST['type'] == 'update_user_last_visit')
{
    $csrf_token = isset($_POST['csrf_token']) ? $_POST['csrf_token'] : null;

    check_token($DB, $csrf_token);

    $params = $_POST['params'];
    $date = date("Y-m-d H:i:s");

    $DB->Execute("UPDATE users_last_visit SET last_visit = '$date' WHERE user_id = ?", array($params['user_id']));
    
    // Актуализация FCM-токена
    $DB->Execute('UPDATE users_chat_tokens
                        SET fcm_last_update = "' . $date . '"
                        WHERE owner_type = "user"
                            AND owner_value = ?
                            AND token = ?', array(
            $params['user_id'],
            $csrf_token
        )
    );

    $result['success'] = true;

    json_response($result, 200);
}

// Проверка активности поставщика в чате
else if ($_POST['type'] == 'check_user_activity')
{
    $params = $_POST['params'];

    $guest_hashes = array();
    $service_message = 'Из-за отсутствия оперативного ответа на ваш запрос в чате, поставщик был заблокирован.';
    $is_weak_activity = false;
    $date = date('Y-m-d') . ' 00:00:00';
    $user = $DB->GetRow('SELECT users.id,
                                users.email_order_notification,
                                users_last_visit.chat_last_banned
                            FROM users
                            LEFT JOIN users_last_visit
                                ON users_last_visit.user_id = users.id
                            WHERE users.id = ?', array($params['user_id']));

    if (!empty($user['chat_last_banned'])
        && $user['chat_last_banned'] != '0000-00-00 00:00:00'
        && $user['chat_last_banned'] > $date)
    {
        $date = $user['chat_last_banned'];
    }

    // Подготовка контактов
    $contacts = $DB->GetAll('SELECT users_chat.guest_hash
                            FROM users_chat
                            LEFT JOIN users_chat_tokens
                                ON users_chat_tokens.owner_value = users_chat.guest_hash
                            WHERE users_chat.user_id = ?
                                AND users_chat.closed_by_user = 0
                                AND users_chat_tokens.is_active = 1
                            GROUP BY users_chat.guest_hash
                            ORDER BY users_chat_tokens.fcm_last_update DESC', array($params['user_id']));

    // Подготовка истории сообщений с учетом текущего дня или последней даты блокировки
    $messages = $DB->GetAll('SELECT *
                              FROM users_chat
                              WHERE user_id = ?
                                  AND closed_by_guest = 0
                                  AND closed_by_user = 0
                                  AND date >= ?
                              ORDER BY date DESC', array($params['user_id'], $date));

    if ($messages)
    {
        $rows = array();

        // Подготовка диалогов чата
        $chat = array();
        foreach ($contacts as $contact)
        {
            $guest_hashes[] = $contact['guest_hash'];

            foreach ($messages as $message)
            {
                if ($contact['guest_hash'] == $message['guest_hash'])
                {
                    $chat[$contact['guest_hash']][] = $message;
                }
            }
        }

        $chat_keys = array_keys($chat);
        foreach ($chat_keys as $chat_key)
        {
            // Проверка ситуации в которой все последние сообщения
            // (до запроса на позицию) только от клиента.
            $stat = array();
            $stat['ignored_total'] = 0;
            $stat['last_msg_date'] = null;
            $stat['has_position_request'] = false;
            foreach ($chat[$chat_key] as $item)
            {
                if (($item['search_position'] == 0 || $item['search_position'] == 1) && $item['sender_type'] == 'guest')
                {
                    if ($stat['ignored_total'] == 0) $stat['last_msg_date'] = $item['date'];
                    $stat['ignored_total']++;
                }

                if ($item['search_position'] == 1 && $item['sender_type'] == 'guest')
                {
                    if ($stat['ignored_total'] > 0) $stat['has_position_request'] = true;
                    break;
                }

                if ($item['search_position'] == 0 && $item['sender_type'] == 'user')
                {
                    $stat['ignored_total'] = 0;
                    $stat['has_position_request'] = false;
                    break;
                }
            }

            if ($stat['ignored_total'] > 0 && $stat['has_position_request'] == true)
            {
                $rows[$chat_key]['ignored_total'] = $stat['ignored_total'];
                $rows[$chat_key]['last_msg_date'] = $stat['last_msg_date'];
            }
            else
            {
                $rows[$chat_key]['ignored_total'] = 0;
                $rows[$chat_key]['last_msg_date'] = false;
            }
        }

        // $result['rows'] = $rows;

        // Проверка кол-ва непрочитанных диалогов с учетом лимита
        // и допустимого времени для отсутствия ответа поставщика
        if (count($rows) >= CHAT_IGNORED_GUESTS_LIMIT)
        {
            $t_diff = date('Y-m-d H:i:s', strtotime('-'. CHAT_USER_WEAK_ACTIVITY / 60 .' minutes'));
            $i = 0;
            foreach ($rows as $r)
            {
                if ($r['ignored_total'] > 0 && $r['last_msg_date'] <= $t_diff) $i++;
                else break;
            }

            if ($i >= CHAT_IGNORED_GUESTS_LIMIT)
            {
                $is_weak_activity = true;
                $DB->Execute("UPDATE users_last_visit SET chat_last_banned = ? WHERE user_id = ?", array(date('Y-m-d H:i:s'), $params['user_id']));
            }
        }
    }

    if ($is_weak_activity)
    {
        // Добавить служебные сообщения для клиентов о блокировке поставщика
        foreach ($guest_hashes as $guest_hash)
        {
            $DB->Execute('INSERT INTO users_chat
                            SET
                                user_id = ?,
                                guest_hash = ?,
                                text = ?,
                                sender_type = ?,
                                user_weak_activity = 1,
                                date = ?', array(
                                    $params['user_id'],
                                    $guest_hash,
                                    $service_message,
                                    'user',
                                    date('Y-m-d H:i:s')
                                ));

            // Проверка FCM token и отправка PUSH-уведомления
            $fcm_token_id = get_fcm_token_by_owner($DB, 'guest', $guest_hash);

            if (!empty($fcm_token_id))
            {
                $result['response'] = send_notification($fcm_token_id, 'Сообщение от поставщика', $service_message);
            }
            else 
            {
                $result['response'] = 'Token is null';
            }
        }

        // Отправка почты
        $mail = new PHPMailer();
        $mail->SetFrom('admin@einfo.ru', 'Einfo');
        $mail->Subject = '[einfo.ru] Чат отключен за неактивность';

        $ignored_guests_limit = CHAT_IGNORED_GUESTS_LIMIT . ' ' . plural(CHAT_IGNORED_GUESTS_LIMIT, 'раз', 'раза', 'раз');
        $user_activity_interval = (CHAT_USER_WEAK_ACTIVITY / 60) . ' ' . plural((CHAT_USER_WEAK_ACTIVITY / 60), 'минуты', 'минут', 'минуты');

        $smarty->assign('limit', $ignored_guests_limit);
        $smarty->assign('interval', $user_activity_interval);

        $mail->Body = $smarty->fetch('mail_user_activity.tpl');

        $email_to = preg_split("/[,;\s]/", $user['email_order_notification'], -1, PREG_SPLIT_NO_EMPTY);
        foreach ($email_to as $value)
        {
            $mail->AddAddress(trim($value));
        }

        $mail->Send();
    }

    $result['weak_activity'] = $is_weak_activity;
    $result['guest_hashes'] = $guest_hashes;
    $result['service_message'] = $service_message;
    $result['success'] = true;

    json_response($result, 200);
}

/**
 * Ответ json
 *
 * @param $data
 * @param $code
 */
function json_response($data, $code)
{
    $http_text = array(
        200 => 'OK',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        500 => 'Internal Server Error'
    );

    $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

    header($protocol . ' ' . $code . ' ' . $http_text[$code]);
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 604800');
    header('Content-type: application/json');

    echo json_encode($data);
}

/**
 * Проверка csrf токена
 *
 * @param $DB
 * @param $csrf_token
 */
function check_token(&$DB, $csrf_token)
{
    $data = $DB->GetRow('SELECT *
                            FROM users_chat_tokens
                            WHERE token = ?
                                AND is_active = 1', array($csrf_token));

    if (!$data)
    {
        $result['message'] = 'Forbidden';

        json_response($result, 403);
        die();
    }
}

/**
 * Получить firebase токен
 *
 * @param $owner_type
 * @param $owner_value
 * @return mixed
 */
function get_fcm_token_by_owner(&$DB, $owner_type, $owner_value)
{
    $data = $DB->GetOne('SELECT fcm_token
                        FROM users_chat_tokens
                        WHERE owner_type = ?
                            AND owner_value = ?
                            AND is_active = 1', array(
            $owner_type,
            $owner_value
        )
    );

    return $data;
}

/**
 * Отправить уведомление
 *
 * @param $host
 * @param $token_id
 * @param $title
 * @param $body
 * @return $response
 */
function send_notification($token_id, $title, $body)
{
    $url = 'https://fcm.googleapis.com/fcm/send';

    $request_body = array(
        'to' => $token_id,
        'notification' => array(
            'title' => $title,
            'icon' => '//' . HOST_FRONTEND . '//media/i/favicon.ico',
            'body' => $body,
            'click_action' => '/'
        )
    );

    $fields = json_encode($request_body);

    $request_headers = array(
        'Content-Type: application/json',
        'Authorization: key=' . CHAT_FCM_API_KEY,
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}