var config = require('./config');
var https = require('https');

var server = https.createServer({
        key: config.ssl_key,
        cert: config.ssl_cert
    }, function(req, res) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('It works!');
        res.end();
    }).listen(config.port, config.host, function(error) {
        console.log('Server is running on: ' + config.host + ':' + config.port);
    });

var ws = require('socket.io')(server, { origins: '*:*'});

ws.on('connection', function(socket) {

    // Проверка токена
    var token = socket.handshake.query.token;

    if (token == undefined || token.length != 32)
    {
        socket.disconnect(true);
    }

    // Новое сообщение
    socket.on('new_message', function(response) {
        socket.broadcast.emit('new_message', response);
    });

    // Старт набора текста
    socket.on('start_typing', function(response) {
        socket.broadcast.emit('start_typing', response);
    });

    // Стоп набора текста
    socket.on('end_typing', function(response) {
        socket.broadcast.emit('end_typing', response);
    });

    // Закрыть сообщения
    socket.on('close_messages', function(response) {
        socket.broadcast.emit('close_messages', response);
    });

    // Новый контакт
    socket.on('new_contact', function(response) {
        socket.broadcast.emit('new_contact', response);
    });
});