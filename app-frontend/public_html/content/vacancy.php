<?php

if (isset($_GET['id']))
{
    $id = (int)$_GET['id'];
    $vacancy = $DB->GetRow("SELECT vacancy.*, users.title as user_title
                         FROM vacancy
                            INNER JOIN users ON users.id=vacancy.user_id
                         WHERE users.status=1 AND vacancy.id=?", array($id));

    if ($vacancy)
    {
        $vacancy['date'] = $date->GetRusDate($vacancy['date'], 3);

        $smarty->assign("vacancy", $vacancy);
        $smarty->display("vacancy_view.tpl");
        $smarty->clearAllAssign();

        $container['page_title'] = "Вакансия: ".$vacancy['speciality'];
        $container['breadcrumbs'] = array('/vacancy/' => 'Вакансии');
    }
    else throw new Exception("Error 404", 404);
}
else
{
    if (isset($_GET['user']))
    {
        $user_id = (int)$_GET['user'];
        $user = $DB->GetRow("SELECT id, title FROM users WHERE id=? AND status=1", array($user_id));
        if ($user)
        {
        	$user_sql = "AND vacancy.user_id=".$user_id;

            $container['breadcrumbs'] = array('/vacancy/' => 'Вакансии');

        	$container['page_title'] = "Все вакансии от предприятия ".$user['title'];
            $smarty->assign('from_user', $container['page_title']);
            $smarty->assign("user", $user);
        }
        else throw new Exception("Error 404", 404);
    }
    else $user_sql = "";


    $all_items = $DB->GetOne("SELECT COUNT(*)
                              FROM vacancy
                                 INNER JOIN users ON users.id=vacancy.user_id
                              WHERE users.status=1 ".$user_sql);

    $pager = new Pager($all_items, $SETTINGS['vacancy']['max_on_page'], "static");

    $list = $DB->GetAll("SELECT vacancy.id,
                                vacancy.user_id,
                                vacancy.date,
                                vacancy.speciality,
                                vacancy.experience,
                                vacancy.city,
                                vacancy.pay,
                                vacancy.info,
                                users.title as user_title
                         FROM vacancy
                            INNER JOIN users ON users.id=vacancy.user_id
                         WHERE users.status=1 ".$user_sql."
                         ORDER BY vacancy.date DESC
                         LIMIT ".$pager->get_limit_start().", ".$pager->get_limit_end());
    foreach ($list as $k => $value)
    {
        $list[$k]['date'] = $date->GetRusDate($value['date'], 3);
    }

    $container['prevnext'] = $pager->get_prev_next();

    $smarty->assign('pager', $pager->get_pages());
    $smarty->assign('vacancy', $list);
    $smarty->display('vacancy_list.tpl');
    $smarty->clearAllAssign();
    unset($list);
}


?>