<?php

function menu_selected(&$item, $link)
{
    static $navcounter = 1;
    GLOBAL $navlinks, $container;

    foreach ($item as $k => $value)
    {
        if ($value['link'] == $link)
        {
            $item[$k]['selected'] = 1;
            $container['page_title'] = $value['title'];

            if (isset($item[$k]['submenu']) && isset($navlinks[$navcounter]))
            {
                menu_selected($item[$k]['submenu']['item'], $navlinks[$navcounter++]);
            }
        }

        if (isset($value['@attributes']['hide']))
        {
            $item[$k]['hide'] = 1;

            if (isset($value['submenu']))
            {
                $container['submenu'][$item[$k]['link']] = $item[$k]['submenu']['item'];
            }

            //unset($item[$k]);
        }
    }
}

// ----------------------------------------------------

// разборка URL и пострение цепочки папок (массив $nav_links)
$uri = $_SERVER['REQUEST_URI'];

if (preg_match('/\/(.*[^\/])\/?/i', preg_replace('/\.html$/i', '', $uri), $matches))
{
    $navlinks = explode("/", $matches[1]);
    if (end($navlinks) == "") array_pop($navlinks);
}
else
{
    $navlinks = array();
}

// загрузка данных их xml-файла
$menu = array();
$submenu = array();


$xml = simplexml_load_file("templates/menu.xml");

$menu = simplexml2array($xml);


menu_selected($menu['item'], count($navlinks) ? $navlinks[0] : '');

foreach ($menu['item'] as $value)
{
    if (isset($value['selected']))
    {
        $container['current_page'] = $value['link'];
    }
}

$container['menu_items'] = $menu['item'];
$container['nav_links'] = $navlinks;
