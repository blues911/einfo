<?php

if ($container['is_mobile'])
{
    $container['use_mobile_template'] = true;

    $smarty->assign('year', date('Y'));
    $smarty->display('mobile/index_info.tpl');
}
else
{
    // анонсы новостей на главной странице
    $news = $DB->GetAll("SELECT news.id, news.user_id, news.date, news.title, users.title as user_title, img
                     FROM news
                        INNER JOIN users ON users.id=news.user_id
                     WHERE users.status=1 AND news.status=1
                     ORDER BY news.date DESC
                     LIMIT 0, " . $SETTINGS['news']['max_on_indexpage']);
    foreach ($news as $k => $value)
    {
        $news[$k]['date'] = $date->GetRusDate($news[$k]['date'], 3);
    }
    $smarty->assign("news", $news);

    $smarty->display("index_info.tpl");
    $smarty->clearAllAssign();

    $container['page_keywords'] = 'einfo электронные компоненты купить поиск заказ';
    $container['page_description'] = 'Электронные компоненты - поиск, цены, наличие, заказ.';
}