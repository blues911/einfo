<?php

// Проверка допустимых GET параметров
$modules_params = array(
    'news'      => array('id', 'user', 'page', 'new', 'archive', 'yclid'),
    'vacancy'   => array('id', 'user', 'page', 'new', 'yclid'),
    'orders'    => array('confirm', 'confirm_user', 'view', 'page', 'new', 'user', 'success', 'yclid'),
    'users'     => array('id', 'price', 'cat', 'filter', 'page', 'new', 'yclid'),
    'search'    => array('yclid')
);

if (!empty($include_file))
{
    if ($get_module = substr($include_file, 0, -4))
    {
        if (array_key_exists($get_module, $modules_params))
        {
            if (count($_GET) > 0)
            {
                foreach ($_GET as $k => $val)
                {
                    if (!in_array($k, $modules_params[$get_module]))
                    {
                        throw new Exception("Error 404", 404);
                    }
                }
            }
        }
    }
}


// Выборка кол-ва предприятий и компонетов в БД
$container['count_users'] = $DB->GetOne("SELECT COUNT(*) FROM users WHERE status=1 AND id NOT IN (?, ?)", array(USER_ADMIN_ID, USER_PARTNER_ID));
$container['count_comp'] = $DB->CacheGetOne("SELECT `value` FROM settings
                                             WHERE `var` = 'catalog:main_cnt'");

// Пользовательские баннеры
$users_banners_sizes = explode(",", $SETTINGS['users_banners']['allow_sizes']);
if ($users_banners_sizes)
{
    foreach ($users_banners_sizes as $size)
    {
        $size_xy = explode("x", $size);
        $container['users_banners'][$size] = $DB->GetAll("SELECT *
                                                          FROM (SELECT banner.*
                                                              FROM users_banners banner
                                                                INNER JOIN users ON users.id=banner.user_id
                                                              WHERE users.status=1
                                                                  AND size_x=?
                                                                  AND size_y=?
                                                              ORDER BY RAND()) res
                                                          GROUP BY user_id ORDER BY RAND()", array($size_xy[0], $size_xy[1]));
    }
}

// Текущий год
$container['year'] = date('Y');
$container['site_host'] = HOST_FRONTEND;

// Соц. опрос
if (!isset($_COOKIE['social_poll']))
{
    $cookie = array('views' => 0, 'ids' => array());
    setcookie('social_poll', serialize($cookie), time() + 3600 * 24 * 30, '/');
}
else
{
    $social_poll = $DB->GetRow("SELECT * FROM social_polls WHERE status = 1");
    $cookie = unserialize($_COOKIE['social_poll']);

    if (!empty($social_poll) && !in_array($social_poll['id'], $cookie['ids']))
    {
        // Обновление кол-ва просмотров
        $cookie['views'] += 1;
        setcookie('social_poll', serialize($cookie), time() + 3600 * 24 * 30, '/');
    }
}

?>