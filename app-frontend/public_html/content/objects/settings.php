<?php

// загрузка настроек из БД
$SETTINGS = array();
$USER_SETTINGS = array();
$setn     = $DB->GetAll('SELECT * FROM settings');

if (!empty($setn))
{
    foreach ($setn as $value)
    {
        @list($section, $param) = explode(':', $value['var']);
        if (isset($param))
        {
            $SETTINGS[$section][$param] = $value['value'];
        }
        else
        {
            $SETTINGS[$section] = $value['value'];
        }
    }
}

// Корзина пользователя
if (!empty($_COOKIE['basket_key']))
{
    $basket_key = $SETTINGS['basket_key'] = $_COOKIE['basket_key'];
}
else
{
    $basket_key = $SETTINGS['basket_key'] = null;
}

// Количество компонентов
$container['basket_count'] = $DB->GetOne('SELECT COUNT(*) FROM basket_tmp WHERE basket_key = ? AND confirm = 1', array($basket_key));

// Компоненты
$basket_list = $DB->GetAll('SELECT basket_tmp.main_id AS id, cnt FROM basket_tmp WHERE basket_key = ? AND confirm = 1 ORDER BY basket_tmp.main_id ASC', array($basket_key));

$SETTINGS['basket_comps'] = array();

if (!empty($basket_list))
{
    foreach ($basket_list as $item)
    {
        $SETTINGS['basket_comps'][$item['id']] = $item['cnt'];
    }
}

// ---------------------------------------------

// загрузка пользовательских настроек из базы данных
if (!empty($client['auth']))
{
    if (!empty($client['settings'])) $USER_SETTINGS = unserialize($client['settings']);

    // определение города и страны пользователя
    $USER_SETTINGS['country_id'] = $DB->CacheGetOne("SELECT country_id FROM ip2ruscity_ip2country WHERE INET_ATON(?) BETWEEN num_ip_start AND num_ip_end", array(get_ip()));
    $USER_SETTINGS['city_id']    = $DB->CacheGetOne("SELECT city_id FROM ip2ruscity_ip_compact WHERE INET_ATON(?) BETWEEN num_ip_start AND num_ip_end", array(get_ip()));

    if (!empty($USER_SETTINGS['interface']))
    {
        $USER_SETTINGS['interface'] = (int)$USER_SETTINGS['interface'] > 1 ? 0 : (int)$USER_SETTINGS['interface'];
    }
}

// -----------------------------------------------------

// Тип поиска
if (!empty($_COOKIE['type_search']))
{
    $container['multiple_search'] = $_COOKIE['type_search'] == 'multiple' ? true : false;
}

// -----------------------------------------------------

// Обновление js и css файлов из кеша браузера пользователя при изменении
// Если файл подключается в ajaxer.php проверку не производим

if(isset($is_mobile))
{
    $media = $is_mobile ? 'mobile' : 'desktop';

    $asset = array(
        'js' => PATH_TMP . '/assets/assets.' . $media . '.js',
        'css' => PATH_TMP . '/assets/assets.' . $media . '.css'
    );

    $assets_config_file_path = PATH_FRONTEND . '/media/assets.json';
    $assets_config = json_decode(file_get_contents($assets_config_file_path), true);
    $assets_file_list = $assets_config[$media];

    foreach ($assets_file_list as $type => $media_set)
    {
        $asset_date = 1;

        foreach ($media_set as $file_path)
        {
            $file_path = PATH_FRONTEND . '/media/' . $file_path;

            if (is_file($file_path) && filemtime($file_path) > filemtime($asset[$type]))
            {
                $asset_date = filemtime($file_path);
                break;
            }
            else if (is_file($file_path) && filemtime($assets_config_file_path) > filemtime($asset[$type]))
            {
                $asset_date = filemtime($assets_config_file_path);
                break;
            }

            $asset_date = filemtime($file_path) > $asset_date ? filemtime($file_path) : $asset_date;
        }

        $container[$type . '_version'] = md5($asset_date);
    }
}

// -----------------------------------------------------
