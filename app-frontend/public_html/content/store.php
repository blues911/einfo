<?php

$result = false;

include_once (PATH_FRONTEND . '/php/classes/class.Search.php');
include_once (PATH_FRONTEND . '/php/classes/class.SearchSQL.php');
include_once (PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

$search = new SearchSphinxQL();

// Очищаем корзину пользователя для базового интерфейса
$_interface = isset($USER_SETTINGS['interface']) ? $USER_SETTINGS['interface'] : DEFAULT_INTERFACE;
if ($_interface > 1) $_interface = 0;

$q = '';
$multiple_search = false;
$search_url = '';
$_strong_search = false;

$is_index = !preg_match('/^search/', $path_url);

// Парсинг поискогового запроса
if (!empty($_POST['search_select']) && $_POST['search_select'] == 'multiple')
{
    $multiple_search = true;
    $search_query = explode("\n", $_POST['multiple_q']);
    $prepared_q = '';
    $prepared_q_arr = array();

    foreach ($search_query as $key => $value)
    {

        $value = trim($value, " \t\n\r\0\x0B");
        if ($value == '')
        {
            unset($search_query[$key]);
        }
        else
        {
            // Убираем * если они есть в начале и конце запроса
            if (preg_match('/^\*(.*)\*$/', $value))
            {
                $value = trim($value, '*');
            }

            $search_query[$key] = Search::prepare_query_multiple($value);

            $prepared_q .= $search_query[$key]['query'] . ', ';
            $prepared_q_arr[] = $search_query[$key]['query'];
        }
    }

    $prepared_q = rtrim($prepared_q, ', ');
}
else
{
    $search_url = preg_replace('/^(search)/', '', $path_url);
    $search_url = trim($search_url, '/');

    $search_query = Search::prepare_query($search_url);

    // Убираем * если они есть в начале и конце запроса
    if (preg_match('/^\*(.*)\*$/', $search_query))
    {
        $search_query = trim($search_query, '*');
    }
}

$result['query'] = false;

// Поисковый запрос
if (!empty($search_query))
{
    $result['query'] = true;

    // Редирект для страниц с кириллицей, fix
    if (!preg_match('/\/$/', $_SERVER['REQUEST_URI']))
    {
        header('Location: ' . rtrim($_SERVER['REQUEST_URI'], '/') . '/', true, 301);
        exit();
    }

    // Редирект для страниц с обычным названием компонента из поиска
    if (!$multiple_search && preg_match('/^\/(search)\/([^\/?*!]+)\/.*/sui', $_SERVER['REQUEST_URI'], $match)
        && !preg_match('/\?/i', $search_query)
        && !(preg_match('/^\'(.+)\'$/', $search_query) || preg_match('/^"(.+)"$/', $search_query))
        && !$container['is_mobile']) {
        header('Location: /' . rtrim($match[2], '/') . '/', true, 301);
        exit();
    }

    // Редирект для страниц с применением спецсимволов
    if (!$multiple_search && !preg_match('/^\/search\/([^\/]+)\//sui', $_SERVER['REQUEST_URI'])
        && (preg_match('/^\'(.+)\'$|[?*]|(%3F)/sui', $search_query) || preg_match('/^"(.+)"$|[?*]|(%3F)/sui', $search_query))) {
        preg_match('/^([^\/]+)/sui', trim($_SERVER['REQUEST_URI'], '/'), $match);
        header('Location: /search/' . rtrim($match[1], '/') . '/', true, 301);
        exit();
    }

    $_query = $search_query;

    $q = $_query;

    if ($multiple_search)
    {
        // Мета теги
        $container['page_keywords'] = mb_strtoupper($prepared_q);
        $container['page_description'] = 'Электронные компоненты '. mb_strtoupper($prepared_q) . ' - цена, наличие, заказ';

        $container['multiple_search'] = true;

        $container['query'] = $_POST['multiple_q'];

        $_special_search = (bool)preg_match('/[\*\?]/', $_POST['multiple_q']);
    }
    else
    {
        // Мета теги
        $container['page_title'] = (($container['is_mobile']) ? mb_strtoupper(ltrim($q, '!')) : mb_strtoupper($q)) . ' купить';
        $container['page_keywords'] = mb_strtoupper($q);
        $container['page_description'] = 'Электронный компонент ' . (($container['is_mobile']) ? mb_strtoupper(ltrim($q, '!')) : mb_strtoupper($q)) . ' - цена, наличие, заказ';

        if (!preg_match('/^search/', ltrim($_SERVER['REQUEST_URI'], '/')))
        {
            $canonical = $q;
            if (preg_match('/[^0-9a-zа-я\s-\/]+/sui', $canonical))
            {
                $canonical = preg_replace('/[^0-9a-zа-я\s-\/]+/sui', ' ', $canonical);
                $canonical = trim($canonical);
            }

            // Избавляемся от множественных пробелов
            $canonical = preg_replace('/\s{2,}/sui', ' ', $canonical);

            $host = $container['is_mobile'] ? HOST_FRONTEND_MOBILE : HOST_FRONTEND;

            $container['page_canonical'] = 'https://' . $host . '/' . encodeURIComponent(mb_strtoupper($canonical, 'utf-8')) . '/';
        }

        if (preg_match('/^[\'\"](.*)[\'\"]$/', $_query) || $is_index && $container['is_mobile'])
        {
            $_query = preg_replace('/^!/', '', $_query);
            $_query = preg_replace('/^[\'"]|[\'"]$/', '', $_query);
            $_strong_search = true;
        }

        $container['strong_search'] = $_strong_search;

        $_special_search = (bool)preg_match('/[\*\?]/', $q);

        $container['query'] = $q;
        $prepared_q = $q;
        $q = $_query;
    }

    // Устанавливаем флаг поиска со спецсимволами
    $smarty->assign('special_search', $_special_search);

    // Проверка на длину поискового запроса
    if (mb_strlen($prepared_q) >= 3)
    {
        $result['query_short'] = false;

        // Передача поисковой фразы и разборка на атомы
        if ($search->set($q, $_strong_search, $multiple_search, $_special_search))
        {
            $_timer = array();
            $list = $search->search($_timer);

            // Если компоненты найдены
            if($list)
            {
                $result['search_result'] = true;

                // Список уникальных компонентов для фильтра
                $filter_components = array();

                // Список поставщиков
                $users = array();

                // Список id поставщиков
                $users_ids = array();

                // Список вариантов наличия товаров
                $filter_availability = $filter_countries = $filter_cities = array();
                $filter_availability['in_stock'] = $filter_availability['unknown'] = 0;
                $filter_availability['order_quickly'] = $filter_availability['order_long'] = 0;

                foreach($list as $k => $item)
                {
                    // Строим comp_hash
                    $list[$k]['comp_title'] = $item['comp_title'] = trim($item['comp_title']);
                    $list[$k]['comp_hash'] = $item['comp_hash'] = md5(mb_strtoupper($item['comp_title']));

                    // Дата обновления прайса
                    $list[$k]['price_date'] = $date->GetRusDate($item['price_date'], 4);

                    // Список компонентов для фильтра
                    if (!empty($filter_components[$item['comp_hash']]))
                    {
                        $filter_components[$item['comp_hash']]['bids']++;
                    }
                    else
                    {
                        $filter_components[$item['comp_hash']] = array(
                            'comp_hash' => $item['comp_hash'],
                            'title' => $item['comp_title'],
                            'bids' => 1
                        );
                    }

                    // Фильтр по варианту наличия
                    if ($item['stock'] == -1 || $item['stock'] > 0)
                    {
                        // В наличии
                        $filter_availability['in_stock']++;
                        $filter_components[$item['comp_hash']]['availability'] = $list[$k]['availability'] = 1;
                    }
                    else if ($item['stock'] == 0 && $item['delivery'] > 0 &&  $item['delivery'] < $SETTINGS['search']['delivery_weeks'])
                    {
                        // На заказ до n-недель
                        $filter_availability['order_quickly']++;
                        $filter_components[$item['comp_hash']]['availability'] = $list[$k]['availability'] = 2;
                    }
                    else if ($item['stock'] == 0 && ($item['delivery'] == -1 ||  $item['delivery'] >= $SETTINGS['search']['delivery_weeks']))
                    {
                        // На заказ от n-недель
                        $filter_availability['order_long']++;
                        $filter_components[$item['comp_hash']]['availability'] = $list[$k]['availability'] = 3;
                    }
                    else
                    {
                        // Наличие неизвестно
                        $filter_availability['unknown']++;
                        $filter_components[$item['comp_hash']]['availability'] = $list[$k]['availability'] = 4;
                    }

                    // Количество на удалённом складе
                    // TODO: поправить когда введут поле в БД
                    $list[$k]['stock_remote'] = !empty($item['stock_remote']) ? (int)$item['stock_remote'] : 0;

                    // Список стран исключая Россию
                    if ($item['user_country_id'] != 186)
                    {
                        $filter_countries[$item['user_country_id']] = array('id' => $item['user_country_id'], 'title' => $item['user_country']);
                    }

                    // Список городов исключая неустановленные
                    if ($item['user_city_id'] != 0)
                    {
                        $filter_cities[$item['user_city_id']] = array('id' => $item['user_city_id'], 'title' => $item['user_city']);
                    }

                    $prices = json_decode($item['prices'], true);
                    $first_price = array_shift($prices);

                    $list[$k]['with_price'] = (!empty($first_price) && $first_price['p'] > 0) ? 1 : 0;
                    $list[$k]['prices'] = $item['prices'];

                    $users[$item['user_id']] = array('id' => $item['user_id'], 'title' => $item['user_title'], 'city' => $item['user_city'], 'city_id' => $item['user_city_id'], 'phone' => $item['user_phone']);

                    $users_ids[] = $item['user_id'];

                    // Компонент в корзине
                    if (array_key_exists($item['id'], $SETTINGS['basket_comps']))
                    {
                        $list[$k]['checked'] = true;
                    }
                    else
                    {
                        $list[$k]['checked'] = false;
                    }

                    $phones = explode(',', $item['user_phone']);
                    $list[$k]['user_phone_main'] = trim($phones[0]);
                }

                $filter_components = array_values($filter_components);
                $filter_countries = array_values($filter_countries);
                $filter_cities = array_values($filter_cities);

                $users_ids = array_unique($users_ids);
                $users_ids = json_encode($users_ids);

                $users = array_values($users);

                // Сортировка компонентов по кол-ву предложений
                foreach ($filter_components as $key => $row)
                {
                    $filter_bids[$key] = $row['bids'];
                }
                array_multisort($filter_bids, SORT_DESC, $filter_components);

                // Фильтр компонентов
                $smarty->assign('query', $q);
                $smarty->assign('list', $filter_components);
                $result['html']['filter'] = $smarty->fetch('search_filter.tpl');
                $smarty->clearAllAssign();

                // Всего результатов
                $all_items = count($list);

                // Занесение статистики запросов в БД
                $search_source = !empty($container['is_mobile']) ? 'mobile' : 'main';
                $search->search_log($all_items, $search_source);

                // Поиск datasheet
                if (!$multiple_search)
                {
                    $datasheet = $DB->GetRow('
                    SELECT
                      comp_canon,
                      `local` AS pdf_filename,
                      `img` AS img_filename,
                      `size` AS pdf_size
                    FROM
                      datasheet
                    WHERE
                      comp_canon LIKE ?
                      AND `local`
                    LIMIT 1
                ', array($q . '%'));

                    if (!empty($datasheet))
                    {
                        if(!empty($datasheet['img_filename']))
                        {
                            $datasheet['have_img'] = true;
                            $datasheet['img_preview'] = 'https://' . DATASHEET_URL . '/img/view/' .
                                substr($datasheet['img_filename'], 0, 2) .
                                '/' . $datasheet['img_filename'];
                            $datasheet['img_fullsize'] = 'https://' . DATASHEET_URL . '/img/full/' .
                                substr($datasheet['img_filename'], 0, 2) .
                                '/' . $datasheet['img_filename'];
                        }
                        else
                        {
                            $datasheet['have_img'] = false;
                        }

                        $datasheet['pdf_href'] = '/datasheet/' . urlencode($datasheet['comp_canon']) . '.pdf';
                        $datasheet['pdf_size'] = filesize_human($datasheet['pdf_size']);
                    }
                }

                // Тип интерфейса
                $_interface = isset($USER_SETTINGS['interface']) ? $USER_SETTINGS['interface'] : DEFAULT_INTERFACE;
                if ($_interface > 1) $_interface = 0;
                $smarty->assign('interface', $_interface);

                // Семантическая сортировка выдачи
                //$list = Search::sort_semantic($list, $q);
                
                // Результаты поиска
                $smarty->assign('multiple_search', $multiple_search ? true : false);
                $smarty->assign('multiple_comp', $prepared_q);
                $smarty->assign('list', $list);
                $smarty->assign('settings', $SETTINGS);
                $smarty->assign('user_settings', $GLOBALS['USER_SETTINGS']);

                $result['html']['search_result'] = $smarty->fetch('search_result.tpl');

                $smarty->assign('all_items', $all_items);
                $smarty->assign('filter_availability', $filter_availability);
                $smarty->assign('city_list', $filter_cities);
                $smarty->assign('country_list', $filter_countries);

                $smarty->assign('datasheet', !empty($datasheet) ? $datasheet : '');

                $smarty->assign('users', $users);
                $smarty->assign('users_ids', $users_ids);

                $search_param = $multiple_search ? 'multiple_max_on_page' : 'max_on_page';
                $smarty->assign('show_warning', $all_items >= $SETTINGS['search'][$search_param]);

                // Если поиск запущен первый раз спрашиваем про физлиц
                if (!isset($USER_SETTINGS['individual_buyers']) && !empty($_SERVER['HTTP_REFERER']) && !empty($client['auth']))
                {
                    $smarty->assign('show_individual_buyers_option', true);

                    $USER_SETTINGS['individual_buyers'] = false;

                    $_user_settings = serialize($USER_SETTINGS);
                    if (!empty($client['auth']))
                    {
                        $DB->Execute('UPDATE clients SET settings = ? WHERE id = ?', array($_user_settings,$client['id']));
                    }
                }
            }
            else
            {
                $result['search_result'] = false;
                $result['query_variant'] = false;

                //занесение статистики запросов в БД
                $search_source = !empty($container['is_mobile']) ? 'mobile' : 'main';
                $search->search_log(0, $search_source);

                // Обрезка кавычек
                $q_variant = Search::slice_quotes($prepared_q);

                // Проверка на существование атомов
                if ($search->set($q_variant))
                {
                    // Подсчет количества предложений
                    $cnt = $search->count();

                    if ($cnt)
                    {
                        $result['query_variant'] = true;

                        $smarty->assign('query_variant', $q_variant);
                        $smarty->assign('query_variant_encode', encodeURIComponent($q_variant));
                        $smarty->assign('query_variant_cnt', $cnt);
                    }
                    else
                    {
                        // Подсчет количества атомов в слове
                        $atoms_cnt = count(get_request_atoms($q_variant));

                        if ($atoms_cnt > 3)
                        {
                            while ($atoms_cnt > 3)
                            {
                                // Обрезка последнего атома
                                $q_variant = Search::slice_last_atom($q_variant);

                                $atoms_cnt = count(get_request_atoms($q_variant));

                                // Проверка на существование атомов
                                if ($search->set($q_variant))
                                {
                                    // Подсчет количества предложений
                                    $cnt = $search->count();

                                    if ($cnt)
                                    {
                                        $result['query_variant'] = true;

                                        $smarty->assign('query_variant', $q_variant);
                                        $smarty->assign('query_variant_encode', encodeURIComponent($q_variant));
                                        $smarty->assign('query_variant_url', '/' . encodeURIComponent($q_variant) . '/');
                                        $smarty->assign('query_variant_cnt', $cnt);

                                        break;
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // Собираем цепочку ссылок
            if (!$multiple_search)
            {
                $chain = array();
                $atoms = get_request_atoms($prepared_q);
                $atoms = array_reverse($atoms);
                $__title = $prepared_q;

                foreach ($atoms as $k => $atom)
                {
                    if ($k >= 5) continue;

                    $__title = preg_replace('/([^a-zа-я0-9]+)?' . $atom . '([^a-zа-я0-9]+)?$/sui', '', $__title);

                    if (mb_strlen($__title) >= 3) $chain[$k] = $__title;
                }

                unset($atoms);
                unset($atom);
                unset($k);

                $chain = array_reverse($chain);

                $smarty->assign('chain', $chain);
            }
        }
    }
    else
    {
        $result['query_short'] = true;
    }

    if ($multiple_search)
    {
        $q_cp = urlencode(@iconv("utf-8", "windows-1251", $prepared_q));
        $smarty->assign('comp_cp', $q_cp);
        $smarty->assign('comp', $prepared_q);
        $smarty->assign('count_comp', count($q));

    }
    else
    {
        $q_cp = urlencode(@iconv("utf-8", "windows-1251", $q));
        $smarty->assign('comp_cp', $q_cp);
        $smarty->assign('comp', $q);
    }

}
// Если в поисковой строке были заданы только пробелы или удаляемые символы
else
{
    preg_match('/\/(search)(\/\/)$/', $_SERVER['REQUEST_URI'], $matches);

    // Делаем редирект на корневую страницу поиска
    if (!empty($matches[2]))
    {
        header('Location: /' . $matches[1] . '/', true, 301);
        exit();
    }
}

// Таймеры в комментариях HTML
if (DEBUG_TIMERS_IN_HTML && !empty($_timer))
{
    $container['search_timer'] = $_timer;
}

// Тип интерфейса
$_interface = isset($USER_SETTINGS['interface']) ? $USER_SETTINGS['interface'] : DEFAULT_INTERFACE;
if ($_interface > 1) $_interface = 0;
$smarty->assign('interface', $_interface);

if ($container['is_mobile'])
{
    $container['no_breadcrumbs'] = true;

    $counts = $search->count_components();

    $result['list'] = array();
    unset($result['html']);

    $list = !empty($list) ? $list : array();

    foreach (array_keys($list) as $k)
    {
        $key = strtolower($list[$k]['comp_title']);
        $result['list'][$key]['list'][] = $list[$k];
        unset($list[$k]);
    }

    $max_count = !empty($counts['components']) ? max($counts['components']) : 0;
    foreach (array_keys($result['list']) as $key)
    {
        if (!empty($counts['components'][$key]))
        {
            $result['list'][$key]['count'] = $counts['components'][$key];
            $result['list'][$key]['color_code'] = ceil($counts['components'][$key] / $max_count / 2 * 10);
        }
    }

    if (!empty($_POST['sort']) && $_POST['sort'] == 'title')
    {
        uasort($result['list'], function($a, $b)
        {
            return strcasecmp($a['list'][0]['comp_title'], $b['list'][0]['comp_title']);
        });
    }
    else
    {
        uasort($result['list'], function($a, $b)
        {
            $cnt_a = !empty($a['count']) ? $a['count'] : 0;
            $cnt_b = !empty($b['count']) ? $b['count'] : 0;

            if ($cnt_a == $cnt_b)
            {
                return strcasecmp($a['list'][0]['comp_title'], $b['list'][0]['comp_title']);
            }
            elseif ($cnt_a < $cnt_b)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        });
    }

    $smarty->assign('sort', !empty($_POST['sort']) ? $_POST['sort'] : false);
    $smarty->assign('count_users', $counts['users']);
}

$smarty->assign('in_basket', $container['basket_count']);
$smarty->assign('result', $result);
$smarty->assign('delivery_weeks', $SETTINGS['search']['delivery_weeks']);
$smarty->assign('spoiler_num', SEARCH_SPOILER_NUM);
$smarty->assign('client', $client);
$smarty->assign('user_settings', $USER_SETTINGS);

$container['no_breadcrumbs'] = true;
$container['no_robots'] = true;

if ($container['is_mobile'])
{
    $container['use_mobile_template'] = true;

    $smarty->assign('strong_search', $_strong_search);
    $smarty->display('mobile/search.tpl');
}
else
{
    if (isset($result['search_result']) && empty($result['search_result']) || !empty($result['query_short']))
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    }

    $smarty->display('search.tpl');
}

$smarty->clearAllAssign();
