<?php

$response = array();
$response['success'] = false;

if (!empty($_POST['name']) && !empty($_POST['email']))
{
    // Подготовка имени и email
    $email = trim($_POST['email']);
    $name = trim($_POST['name']);

    if (filter_var($email, FILTER_VALIDATE_EMAIL) !== false)
    {
        // Проверяем, существует ли пользователь с таким e-mail
        $client_id = $DB->GetOne('SELECT id FROM clients WHERE email = ?', array($email));

        if (empty($client_id))
        {
            // Подготавливаем остальные данные
            $phone = !empty($_POST['phone']) ? trim($_POST['phone']) : '';
            $date_register = date('Y-m-d H:i:s');
            $auth_hash = md5(uniqid());

            // Генерируем пароль из 6 цифр
            $password = '';
            for($i = 1; $i <= 6; $i++)
            {
                $password .= strval(rand(0,9));
            }
            $password_hash = md5(iconv('utf-8', 'windows-1251', $password . CLIENT_PASSWORD_PEPPER));

            if ($DB->Execute('
                INSERT INTO
                  clients
                SET
                  email = ?,
                  name = ?,
                  phone = ?,
                  date_register = ?,
                  password = ?,
                  auth_hash = ?,
                  status = -1
            ', array(
                $email,
                $name,
                $phone,
                $date_register,
                $password_hash,
                $auth_hash
            ))
            )
            {
                // Отправляем письмо
                $mail = new PHPMailer();
                $mail->Sender = 'admin@einfo.ru';
                $mail->setFrom('admin@einfo.ru', 'Einfo');
                $mail->Subject = '[einfo.ru] Регистрация в системе';
                $mail->IsHTML();
                $mail->AddAddress($email);

                $client_id = $DB->_connectionID->insert_id;
                $url = 'https://' . HOST_FRONTEND . '/account/profile/?client_id=' . $client_id . '&auth_hash=' . $auth_hash;

                $smarty->assign('email', $email);
                $smarty->assign('password', $password);
                $smarty->assign('url', $url);

                $mail->Body = $smarty->fetch('account/mail_reg.tpl');

                if ($mail->Send())
                {
                    $mail->ClearAddresses();
                    $response['info'] = 'Вы успешно зарегистрировались. Инструкции по активации аккаунта отправлены на электронную почту <strong>' . $email . '</strong>.';
                    $response['success'] = true;
                }
                else
                {
                    $DB->Execute('DELETE FROM clients WHERE id = ?', array($client_id));
                    $response['error'] = 'Ошибка отправки письма';
                }
            }
        }
        else
        {
            $response['error'] = 'Данный адрес электронной почты зарегистрирован ранее';
        }
    }
    else
    {
        $response['error'] = 'Некорректный адрес электронной почты';
    }

}

echo(json_encode($response));