<?php

$response = array();
$response['success'] = false;
$response['not_registered'] = false;
$response['method'] = !empty($_POST['method']) ? $_POST['method'] : 'pass';

if (!empty($_POST['email']))
{
    if (filter_var(trim($_POST['email']), FILTER_VALIDATE_EMAIL) !== false)
    {
        if ($response['method'] == 'email')
        {
            $client = $DB->GetRow('SELECT id, auth_hash, email FROM clients WHERE email = ?', array($_POST['email']));

            if (!empty($client))
            {
                $mail = new PHPMailer();
                $mail->Sender = 'admin@einfo.ru';
                $mail->setFrom('admin@einfo.ru', 'Einfo');
                $mail->Subject = '[einfo.ru] Вход в личный кабинет';
                $mail->IsHTML();
                $mail->AddAddress($client['email']);

                $url = 'https://' . HOST_FRONTEND . '/account/profile/?client_id=' . $client['id'] . '&auth_hash=' . $client['auth_hash'];
                $smarty->assign('url', $url);

                $mail->Body = $smarty->fetch('account/mail_remember.tpl');

                $mail->Send();
                $mail->ClearAddresses();

                $response['info'] = 'Письмо с инструкциями отправлено на электронную почту';
                $response['success'] = true;
            }
            else
            {
                $response['error'] = 'Электронная почта не зарегестрирована в системе';
                $response['not_registered'] = true;
            }
        }
        else
        {
            $client = $DB->GetRow(
                'SELECT id, auth_hash FROM clients WHERE email = ? AND password = ? AND status = 1',
                array(
                    $_POST['email'],
                    md5(iconv('utf-8', 'windows-1251', $_POST['password'] . CLIENT_PASSWORD_PEPPER))
                )
            );

            if (!empty($client))
            {
                setcookie('client_id', $client['id'], time() + 365*24*60*60, '/');
                setcookie('auth_hash', $client['auth_hash'], time() + 365*24*60*60, '/');
                setcookie('auth_with_login', true, time() + 60, '/');

                $response['success'] = true;
            }
            else
            {
                $response['error'] = 'Ошибка авторизации';
            }
        }
    }
    else
    {
        $response['error'] = 'Некорректный адрес электронной почты';
    }
}

echo(json_encode($response));