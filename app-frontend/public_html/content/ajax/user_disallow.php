<?php

$response = array();
$response['success'] = false;

if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['user_id']))
{
    // Добавление предприятия в список исключенных
    $ip_address = get_ip();

    $user_id = (int)$_POST['user_id'];

    $DB_master->Execute('INSERT INTO clients_list_log VALUES(?, ?, ?, ?, ?, NOW())', array($ip_address, $user_id, $client['id'], 'black', 'add'));

    $USER_SETTINGS['search_users_disallow'][] = $user_id;

    // Сохранение настроек
    $_user_settings = serialize($USER_SETTINGS);
    if (!empty($client['auth']))
    {
        $DB->Execute('UPDATE clients SET settings = ? WHERE id = ?', array($_user_settings,$client['id']));
    }

    $response['success'] = true;

}

print json_encode($response);
