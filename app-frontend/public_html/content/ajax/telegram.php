<?php

include_once (PATH_FRONTEND . '/php/classes/class.Search.php');
include_once (PATH_FRONTEND . '/php/classes/class.SearchSQL.php');
include_once (PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');
include_once (PATH_FRONTEND . '/php/classes/class.Telegram.php');

if (!empty($_GET['phone']))
{
    $smarty->assign('phone', preg_replace('/^\s/uis', '+', $_GET['phone']));
    $smarty->display('phone_redirect.tpl');
}
elseif (!empty($_GET['token']) && $_GET['token'] == TELEGRAM_HOOK_TOKEN)
{
    $telegram = new Telegram();

    require 'php/libs/Requests/library/Requests.php';
    Requests::register_autoloader();

    $request_body = file_get_contents('php://input');

    $request_data = json_decode($request_body, true);

    $headers = array(
        'Content-Type' => 'application/json; charset=utf-8'
    );

    if (!empty($request_data['message']['text']) && !empty($request_data['message']['chat']['id']))
    {
        $message = $request_data['message'];

        $response = array(
            'chat_id' => $message['chat']['id'],
            'text' => 'Неизвестный запрос.',
            'parse_mode' => 'html',
            'disable_web_page_preview' => true
        );

        if (preg_match('/^(\/start|\/help)/uis', $message['text']))
        {
            $response['text'] = 'Для поиска просто отправьте боту название компонента.';
        }
        else
        {
            // Убираем * если они есть в начале и конце запроса
            if (preg_match('/^\*(.*)\*$/', $message['text']))
            {
                $message['text'] = trim($message['text'], '*');
            }

            $result = $telegram->search($message['text'], TELEGRAM_SEARCH_LIMIT);

            if (!empty($result['list']))
            {
                $response['text'] = $telegram->result_format($result['list'], $message['text'], $result['cnt'] - count($result['list']));
            }
            else
            {
                $response['text'] = 'Ничего не найдено.';
            }
        }
        
        Requests::post(TELEGRAM_API_URL . 'sendMessage', $headers, json_encode($response));
    }
}
else
{
    throw new Exception('Error 403', 403);
}