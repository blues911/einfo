<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['user_hash']))
{
    $hash = trim($_POST['user_hash']);
    $api_allowed_ip = get_ip();

    // Проверяем доступ пользователя
    $access_granted = false;
    $user = $GLOBALS['DB']->GetRow(
        'SELECT u.id, u.api_allowed_ip
                            FROM (
                                 SELECT id, MD5(CONCAT(login,password)) AS user_hash, api_allowed_ip
                                 FROM users
                                 WHERE status = 1) AS u
                            WHERE user_hash = ?',
        array($hash)
    );

    if ($user)
    {
        $api_allowed_ip = $user['api_allowed_ip'];

        // Проверяем список разрешенных IP адресов
        if (!empty($api_allowed_ip))
        {
            $api_allowed_ip = explode(',', $api_allowed_ip);

            // если задан один IP, приводим его к массиву
            if (!is_array($api_allowed_ip))
            {
                $api_allowed_ip = array($api_allowed_ip);
            }

            foreach ($api_allowed_ip as $k => $ip)
            {
                $api_allowed_ip[$k] = trim($ip);
            }
        }

        $access_granted = (empty($api_allowed_ip) || in_array($current_user_ip, $api_allowed_ip));
    }

    $user_id = $access_granted ? $user['id'] : false;

    // Если доступ разрешен
    if ($user_id)
    {
        $date_from = false;

        // Если задана начальная дата, задаем фильтр по ней
        if (isset($_POST['date_from']) && !empty($_POST['date_from']))
        {
            $date = strtotime(trim($_POST['date_from']));

            if ($date)
            {
                $date_from = date('Y-m-d H:i:s', $date);
            }
            else throw new Exception('Error 403', 403);
        }

        $date_filter = !empty($date_from) ? 'AND order.date >= "' . $date_from . '"' : '';

        // Получаем все заказы пользователя
        $orders_temp = $GLOBALS['DB']->GetAll(
            'SELECT
                                    order.id,
                                    order.date,
                                    client.name AS client_name,
                                    client.email AS client_email,
                                    client.phone AS client_phone,
                                    order.comments,
                                    item.rcount AS count,
                                    item.title,
                                    item.manuf AS manufacturer,
                                    item.descr AS description,
                                    item.prices,
                                    item.mfgdate AS mfgdate
                                FROM orders AS `order`
                                    LEFT JOIN clients AS `client` ON order.client_id = client.id
                                    LEFT JOIN orders_items AS item ON order.id = item.order_id
                                WHERE item.user_id = ? AND order.valid = 1 ' . $date_filter . '
                                ORDER BY order.date DESC',
            array($user_id)
        );

        // Если заказы найдены
        if ($orders_temp)
        {
            $result = array();

            // Собираем многомерный массив
            foreach ($orders_temp as $key => $order)
            {
                $result[$order['id']]['id'] = $order['id'];
                $result[$order['id']]['date'] = $order['date'];
                $result[$order['id']]['client_name'] = $order['client_name'];
                $result[$order['id']]['client_email'] = $order['client_email'];
                $result[$order['id']]['client_phone'] = $order['client_phone'];
                $result[$order['id']]['client_comment'] = $order['comments'];

                $prices = json_decode($order['prices'], true);
                $price = array();

                if (!empty($prices))
                {
                    $price = array_shift($prices);
                }

                $result[$order['id']]['items'][] = array(
                    'count'           => $order['count'],
                    'title'           => $order['title'],
                    'manufacturer'    => $order['manufacturer'],
                    'description'     => $order['description'],
                    'retail_price'    => isset($price['p']) ? $price['p'] : '',
                    'retail_currency' => isset($price['c']) ? $price['c'] : '',
                    'trade_price'     => isset($price['p']) ? $price['p'] : '',
                    'trade_currency'  => isset($price['c']) ? $price['c'] : '',
                    'mfgdate'         => $order['mfgdate']
                );

                unset($orders_temp[$key]);
            }
            $result = array_values($result);

            $orders = $result;
        }
        else
        {
            $orders = false;
        }

        $orders_to_string = json_encode($orders);

        header('Content-type: text/json; charset=utf-8');
        header('Content-length: ' . strlen($orders_to_string));

        echo $orders_to_string;
    }
    else throw new Exception('Error 403', 403);
}
else throw new Exception('Error 403', 403);
