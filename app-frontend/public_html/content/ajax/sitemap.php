<?php

// количество урлов в одном файле
define('URL_IN_FILE', 10000);

// --------------------------------------

// $DB->debug = true;

$host = $_SERVER['HTTP_HOST'];

if (empty($_GET['n']))
{
    $template = '<?xml version="1.0" encoding="UTF-8"?>
    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {sitemap}
    </sitemapindex>';

    $total = $DB_master->CacheGetOne('SELECT COUNT(*) FROM canonical_comp');
    $limit = URL_IN_FILE;
    $i = 1;

    $sitemap = '';
    while ($limit <= $total)
    {
        $sitemap .= '<sitemap><loc>https://' . $host . '/sitemap' . $i . '.xml</loc></sitemap>';
        $limit = $limit + URL_IN_FILE;
        $i++;
    }

    $sitemap_data = str_replace('{sitemap}', $sitemap, $template);

    header("Content-type: text/xml; charset=utf-8");
    print iconv('windows-1251', 'utf-8', $sitemap_data);
}
else
{
    $template = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {urlset}
    </urlset>';

    $n = (int)$_GET['n'];
    $limit = $n * URL_IN_FILE;
    $offset = URL_IN_FILE;

    $comps = $DB_master->CacheGetCol('SELECT comp_title
                                        FROM canonical_comp
                                        GROUP BY comp_title
                                        LIMIT ?, ?', array($limit, $offset));

    if ($comps)
    {
        $urlset = '';
        foreach ($comps as $name)
        {
            $urlset .= "  <url><loc>https://" . $host . "/".urlencode($name)."/</loc><changefreq>weekly</changefreq><priority>1</priority></url>\r\n";
        }

        $urlset_data = str_replace('{urlset}', $urlset, $template);

        header("Content-type: text/xml; charset=utf-8");
        print iconv('windows-1251', 'utf-8', $urlset_data);
    }
    else
    {
        throw new Exception('Error 404', 404);
    }
}

?>
