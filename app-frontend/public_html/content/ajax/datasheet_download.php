<?php

if (!empty($_GET['file']))
{
    $filename = urldecode(trim($_GET['file'], '/'));
    $filename = preg_replace('/\.pdf$/', '', $filename);

    $datasheet = $DB->GetRow('SELECT `local` FROM datasheet WHERE comp_canon = ?', $filename);

    if ($datasheet && !empty($datasheet['local']))
    {
        $file_path = DATASHEET_PATH . '/pdf/' . substr($datasheet['local'], 0, 2) . '/' . $datasheet['local'];

        if (is_file($file_path))
        {
            header('Content-Type: application/pdf');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_path));
            ob_clean();
            flush();
            readfile($file_path);
            exit;
        }
        else throw new Exception('Error 404', 404);
    }
    else throw new Exception('Error 404', 404);
}
else throw new Exception('Error 404', 404);