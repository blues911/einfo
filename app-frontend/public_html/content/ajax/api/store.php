<?php

include_once(PATH_FRONTEND . '/php/classes/class.Search.php');
include_once(PATH_FRONTEND . '/php/classes/class.SearchSQL.php');
include_once(PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

$search = new SearchSphinxQL();
$api_request = $api->get_request();

// Общее кол-во компонентов
if ($api_request['method'] == 'get_total') {
    $result = $api->get_total_info();
    $api->response($result);
}
// Поиск комплектующих
elseif ($api_request['method'] == 'find')
{
    // Парсинг поискогового запроса
    $search_query = Search::prepare_query($api_request['params']['query']);

    // Проверка на длину поискового запроса
    if (mb_strlen($search_query) >= 3)
    {
        // Убираем * если они есть в начале и конце запроса
        if (preg_match('/^\*(.*)\*$/', $search_query))
        {
            $search_query = trim($search_query, '*');
        }

        // Проверяем необходимость поиска со спецсимволами
        $special_search = (bool)preg_match('/[\*\?]/', $search_query);

        $search->set($search_query, false, false, $special_search);

        $list = $search->search();

        // Если компоненты найдены
        if ($list)
        {
            $all_items = count($list);
            $result = $api->count_components($list);
            $count_users = $result['users'];
            unset($result['users']);

            $max_count = !empty($result['components']) ? max($result['components']) : 0;

            foreach($result['components'] as $key => $item)
            {
                $result[]= array(
                    'title' => $key,
                    'count' => $item,
                    'color_code' => ceil($item / $max_count / 2 * 10)
                );
            }
            unset($result['components']);

            $orders_to_string = array('result' => $result,
                                 'total'  => array('offers'    => $all_items,
                                                   'suppliers' => $count_users),);

            $api->response($orders_to_string);
        }
        else throw new Exception('', 105);
    }
    else throw new Exception('', 106);

}
// Поиск предложений по строгому совпадению в названии
elseif ($api_request['method'] == 'get_offers')
{
    // Парсинг поискогового запроса
    $search_query = Search::prepare_query($api_request['params']['query']);

    // Строгий поиск
    $search->set($search_query, true);
    $list = $search->search();

    $result = array();
    foreach($list as $key => $item)
    {
        $prices = json_decode($item['prices'], true);
        $fixed_prices = array();

        if (!empty($prices))
        {
            foreach ($prices as $k => $v)
            {
                if ($v['p'] > 0) $fixed_prices[] = $prices[$k];
            }
        }

        $result[] = array('id'           => $item['id'],
                          'user_id'      => $item['user_id'],
                          'user_title'   => $item['user_title'],
                          'user_country' => $item['user_country'],
                          'user_city'    => $item['user_city'],
                          'user_email'   => $item['user_email'],
                          'user_www'     => $item['user_www'],
                          'user_phone'   => $item['user_phone1'],
                          'price_date'   => $item['price_date'],
                          'price_status' => $item['price_status'],
                          'comp_title'   => $item['comp_title'],
                          'descr'        => $item['descr'],
                          'stock'        => $item['stock'],
                          'stock_remote' => $item['stock_remote'],
                          'delivery'     => $item['delivery'],
                          'prices'       => $fixed_prices
        );
    }

    $api->response($result);
}
// Информация о поставщике
elseif ($api_request['method'] == 'get_supplier')
{
    $result = $api->get_user_info((int)$api_request['params']['id']);
    $api->response($result);
}
else throw new Exception('', 107);
