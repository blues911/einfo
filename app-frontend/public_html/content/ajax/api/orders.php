<?php

$api_request = $api->get_request();

// Оформление заявки
if ($api_request['method'] == 'send_inquiry')
{
    if (filter_var(trim($api_request['params']['email']), FILTER_VALIDATE_EMAIL) !== false)
    {
        // Подготовка данных
        $id                         = (int)$api_request['params']['id'];
        $item_cnt                   = (int)$api_request['params']['item_cnt'];
        $ins_basket['date']         = date('Y-m-d H:i:s');
        $ins_basket['valid']        = 0;
        $user_info['contact_name']  = $api_request['params']['name'];
        $user_info['contact_email'] = $api_request['params']['email'];
        $user_info['contact_phone'] = $api_request['params']['phone'];
        $ins_basket['comments']     = $api_request['params']['message'];
        $ins_basket['send']         = 0;
        $phone                      = !empty($user_info['contact_phone']) ? trim($user_info['contact_phone']) : '';

        // Проверяем существование пользователя с указанным email
        $client = $DB->GetRow('SELECT * FROM clients WHERE email = ?', array($user_info['contact_email']));
        if (empty($client))
        {
            // Если пользователя не существует - регистрируем
            $date_register = date('Y-m-d H:i:s');
            $auth_hash     = md5(uniqid());

            // Генерируем пароль из 6 цифр
            $password = '';
            for ($i = 1; $i <= 6; $i++)
            {
                $password .= strval(rand(0, 9));
            }
            $password_hash = md5(iconv('utf-8', 'windows-1251', $password . CLIENT_PASSWORD_PEPPER));

            if ($DB->Execute('
                INSERT INTO
                  clients
                SET
                  email = ?,
                  name = ?,
                  phone = ?,
                  date_register = ?,
                  password = ?,
                  auth_hash = ?,
                  status = -1
            ', array(
                    $user_info['contact_email'],
                    $user_info['contact_name'],
                    $phone,
                    $date_register,
                    $password_hash,
                    $auth_hash
                )
            )
            )
            {
                $client_id           = $DB->_connectionID->insert_id;
                $client              = $DB->GetRow('SELECT * FROM clients WHERE id = ?', array($client_id));
                $client['password']  = $password;
                $client['url']       = 'https://' . HOST_FRONTEND . '/account/profile/?client_id=' . $client['id'] . '&auth_hash=' . $client['auth_hash'];
                $client['mail_type'] = 'new';
            }
        }
        else
        {

            $client['url']       = 'https://' . HOST_FRONTEND . '/account/profile/?client_id=' . $client['id'] . '&auth_hash=' . $client['auth_hash'];
            $client['mail_type'] = 'existing';
        }

        // Если необходимая информация о клиенте получена то оформляем заявку
        if (!empty($client))
        {
            // Вставляем изменения телефона
            if (!empty($phone))
            {
                if ($DB->Execute('UPDATE clients SET phone = ? WHERE id = ?', array($phone, $client['id'])))
                {
                    $client['phone'] = $phone;
                }
            }

            $user_info['mail_type'] = $client['mail_type'];
            $user_info['success']   = true;


            // Сохранение заявки
            $ins_basket['client_id'] = $client['id'];

            if ($DB_master->AutoExecute('orders', $ins_basket, 'INSERT'))
            {
                $order_id = $DB_master->Insert_ID();

                if ($order_id)
                {
                    $DB_master->Execute('
                        INSERT INTO orders_items
                        (
                          order_id,
                          user_id,
                          rcount,
                          title,
                          manuf,
                          descr,
                          prices,
                          mfgdate
                        )
                        SELECT
                            ' . $order_id . ',
                            main.user_id,
                            ' . $item_cnt . ',
                            main.comp_title,
                            main.manuf_title as manuf,
                            main.descr,
                            main.prices,
                            main.mfgdate
                        FROM
                            base_main main
                            INNER JOIN users ON main.user_id = users.id
                        WHERE
                            users.status = 1
                            AND main.id = ?
                    ', array($id));
                }

                // отправка письма с кодом активации
                // $mail         = new PHPMailer();
                // $mail->Sender = 'admin@einfo.ru';
                // $mail->SetFrom('admin@einfo.ru', 'Автомат формирования заявок Einfo.ru');
                // $mail->Subject = 'Заявка #' . $order_id;
                // $mail->AddAddress($user_info['contact_email']);
                // $mail->IsHTML();

                $basket_items = $DB_master->GetAll('SELECT orders_items.*, users.title AS user_title
                                            FROM orders_items
                                                INNER JOIN users ON orders_items.user_id=users.id
                                            WHERE orders_items.order_id=?', array($order_id));

                $smarty->assign('site_host', HOST_FRONTEND);
                $smarty->assign('contact_name', $user_info['contact_name']);
                $smarty->assign('order_id', $order_id);
                $smarty->assign('basket_items', $basket_items);
                $smarty->assign('client', $client);
                $smarty->assign('comments', $ins_basket['comments']);
                $smarty->assign('settings', $SETTINGS);

                $mail->Body     = $smarty->fetch('mail_basket_valid.tpl');
                // $result['send'] = $mail->Send();
                $result['send'] = true;

                $result['order_num'] = $order_id;
            }
            else
            {
                $result['send'] = false;
                $result['error'] = 'Сбой сохранения заявки и отправки уведомления';
            }
        }
    }
    else
    {
        $result['send'] = false;
        $result['error'] = 'Некорректный адрес электронной почты';
    }
    $api->response($result);
}
else throw new Exception('', 107);
