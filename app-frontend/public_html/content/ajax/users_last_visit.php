<?php

$response = array(
    'success' => false
);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $ids = (!empty($_POST['users_ids'])) ? implode(', ', $_POST['users_ids']) : array();

    $query = sprintf("SELECT
                          users.id AS user_id,
                          IF (users_last_visit.last_visit >= NOW() - INTERVAL %1\$s SECOND, 1, 0) AS user_online,
                          IF (users_chat_tokens.fcm_last_update >= NOW() - INTERVAL %1\$s SECOND, 
                              IF (IFNULL(users_chat_tokens.fcm_token, '') = '', 0, 1), 0
                          ) AS user_allow_chat
                      FROM users
                      LEFT JOIN users_last_visit ON users.id = users_last_visit.user_id
                      LEFT JOIN users_chat_tokens ON users.id = users_chat_tokens.owner_value
                          AND users_chat_tokens.owner_type = 'user'
                          AND users_chat_tokens.is_active = 1
                      WHERE users.id IN (%2\$s)",
                      CHAT_LAST_VISIT_INTERVAL,
                      $ids
    );

    $users_info = $DB->GetAll($query);

    if (!$users_info)
    {
        $users_info = array();
    }

    $users_info_diff = true;

    if (isset($_COOKIE['search_users_chat_info'])
        && $_COOKIE['search_users_chat_info'] == serialize($users_info))
    {
        $users_info_diff = false;
    }
    else
    {
        setcookie('search_users_chat_info', serialize($users_info), time() + (86400 * 30), "/");
        $users_info_diff = true;
    }

    $response['users_info'] = $users_info;
    $response['users_info_diff'] = $users_info_diff;
    $response['success'] = true;
}

echo json_encode($response);