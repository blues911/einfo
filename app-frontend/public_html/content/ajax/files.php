<?php

$file_types = array(
    'jpg' => 'image/jpeg',
    'png' => 'image/png',
    'gif' => 'image/gif',
    'pdf' => 'application/pdf',
    'doc' => 'application/msword',
    'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'txt' => 'text/plain'
);

if (!empty($_GET['type']) && !empty($_GET['filename']))
{
    $type = $_GET['type'];
    $filename = $_GET['filename'];
    $extension = preg_replace('/^.*\./', '', $filename);

    if (array_key_exists($extension, $file_types))
    {
        $file_type = $file_types[$extension];

        if ($type == 'news')
        {
            $blob = $DB->GetOne(
                'SELECT
                  IF(LENGTH(news.img) > 0, news.img_blob, news_images.img_blob) AS img_blob
                FROM news, news_images
                WHERE
                  news.img = ?
                  OR news_images.img = ?
                LIMIT 1',
                array($filename, $filename)
            );
        }
        elseif ($type == 'users_docs')
        {
            $size = !empty($_GET['size']) ? $_GET['size'] : false;

            if ($size == 'small')
            {
                $blob = $DB->GetOne('SELECT file_small_blob FROM users_docs WHERE `file` = ?', array($filename));
            }
            else
            {
                $blob = $DB->GetOne('SELECT file_blob FROM users_docs WHERE `file` = ?', array($filename));
            }
        }
        elseif ($type == 'banners')
        {
            $blob = $DB->GetOne('SELECT banner_blob FROM users_banners WHERE banner = ?', array($filename));
        }

        if (empty($blob))
        {
            throw new Exception('Error 404', 404);
        }

        header('Content-Type: ' . $file_type);
        echo($blob);
        exit();
    }
    else
    {
        throw new Exception('Error 404', 404);
    }
}
else
{
    throw new Exception('Error 404', 404);
}