<?php

$result['success'] = false;
$result['action'] = $_POST['action'];

if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['action']))
{
    // Корзина пользователя
    if (!empty($_COOKIE['basket_key']))
    {
        $basket_key = $SETTINGS['basket_key'] = $_COOKIE['basket_key'];
    }
    else
    {
        $basket_key = $SETTINGS['basket_key'] = md5(uniqid());

        // Установка cookie на 30 дней
        setcookie('basket_key', $basket_key, time() + 3600 * 24 * 30, '/');
    }

    // Добавление в корзину
    if($_POST['action'] == 'add' && !empty($_POST['id']))
    {
        if (is_array($_POST['id']))
        {
            $ids = $_POST['id'];
        }
        else
        {
            if (!empty($_POST['oneclick']))
            {
                $DB_master->Execute('DELETE FROM basket_tmp WHERE basket_key = ?', array($basket_key));
            }

            $component = $DB->GetRow(
                'SELECT base_main.id, base_main.comp_title, base_main.user_id FROM base_main WHERE id = ?',
                array((int)$_POST['id'])
            );

            $similar = array(
                'all' => array(
                    'comp_ids' => array(),
                    'user_ids' => array(),
                    'comp_cnt' => 0,
                    'user_cnt' => 0
                ),
                'individual' => array(
                    'comp_ids' => array(),
                    'user_ids' => array(),
                    'comp_cnt' => 0,
                    'user_cnt' => 0
                )
            );

            $similar_items = $DB->GetAll(
                'SELECT base_main.id, users.id AS user_id, users.individual_buyers
                FROM base_main
                INNER JOIN users ON users.id = base_main.user_id
                WHERE base_main.comp_title = ? AND users.status = 1',
                array($component['comp_title'])
            );

            foreach ($similar_items as $similar_item)
            {
                $_user_id = (int)$similar_item['user_id'];

                $similar['all']['comp_ids'][] = (int)$similar_item['id'];
                $similar['all']['user_ids'][$_user_id] = $_user_id;

                if (!empty($similar_item['individual_buyers']))
                {
                    $similar['individual']['comp_ids'][] = (int)$similar_item['id'];
                    $similar['individual']['user_ids'][$_user_id] = $_user_id;
                }
            }

            $similar['all']['comp_cnt'] = in_array($component['id'], $similar['all']['comp_ids']) ? count($similar['all']['comp_ids']) - 1 : count($similar['all']['comp_ids']);
            $similar['all']['user_cnt'] = in_array($component['user_id'], $similar['all']['user_ids']) ? count($similar['all']['user_ids']) - 1 : count($similar['all']['user_ids']);
            $similar['individual']['comp_cnt'] = in_array($component['id'], $similar['individual']['comp_ids']) ? count($similar['individual']['comp_ids']) - 1 : count($similar['individual']['comp_ids']);
            $similar['individual']['user_cnt'] = in_array($component['user_id'], $similar['individual']['user_ids']) ? count($similar['individual']['user_ids']) - 1 : count($similar['individual']['user_ids']);

            if (empty($_POST['all_offers']))
            {
                $ids = (array)$component['id'];
            }
            else
            {
                if (empty($_POST['is_individual_buyer']))
                {
                    $ids = $similar['all']['comp_ids'];
                }
                else
                {
                    $ids = $similar['individual']['comp_ids'];
                }
            }

            $result['similar'] = $similar;
        }

        $DB_master->Execute('DELETE FROM basket_tmp WHERE basket_key = ? AND confirm = 0', array($basket_key));

        foreach ($ids as $id)
        {
            $comp = $DB_master->GetRow('
              SELECT
                main.id
              FROM
                base_main main
                INNER JOIN users ON main.user_id = users.id
              WHERE
                users.status = 1
                AND main.id = ?
            ', array($id));

            if (!empty($comp))
            {
                $DB_master->Execute('INSERT IGNORE INTO basket_tmp VALUES (?, NOW(), ?, 1, 1)', array($basket_key, $id));
            }
        }

        $result['count'] = $DB_master->GetOne('SELECT COUNT(*) FROM basket_tmp WHERE basket_key = ? AND confirm = 1', array($basket_key));

        $list = $DB_master->GetAll('
            SELECT
              main.id,
              main.manuf_title,
              main.comp_title,
              users.title,
              main.prices
            FROM
              basket_tmp AS basket
              INNER JOIN base_main AS main ON main.id = basket.main_id
              INNER JOIN users ON users.id = main.user_id
            WHERE
              basket.basket_key = ?
        ', array($basket_key));

        $result['success'] = true;
        $result['list'] = $list;
    }

    else if ($_POST['action'] == 'get_list')
    {
        $list = $DB_master->GetAll('
            SELECT
              main.id,
              main.manuf_title,
              main.comp_title,
              users.title,
              main.prices
            FROM
              basket_tmp AS basket
              INNER JOIN base_main AS main ON main.id = basket.main_id
              INNER JOIN users ON users.id = main.user_id
            WHERE
              basket.basket_key = ?
        ', array($basket_key));

        $result['success'] = true;
        $result['list'] = $list;
    }

    // Удаление из корзины
    else if ($_POST['action'] == 'remove' && !empty($_POST['id']))
    {
        $ids = (array)$_POST['id'];

        $DB_master->Execute('DELETE FROM basket_tmp WHERE basket_key = ? AND main_id IN (' . implode(',', $ids) . ')', array($basket_key));

        $result['success'] = true;
        $result['count'] = $DB_master->GetOne('SELECT COUNT(*) FROM basket_tmp WHERE basket_key = ? AND confirm = 1', array($basket_key));
    }

    // Изменение количетсва товаров
    else if ($_POST['action'] == 'update' && !empty($_POST['id']) && !empty($_POST['cnt']))
    {
        $id = (float)$_POST['id'];
        $cnt = (int)$_POST['cnt'];

        $DB_master->Execute('UPDATE basket_tmp SET cnt = ? WHERE basket_key = ? AND main_id = ?', array($cnt, $basket_key, $id));

        $result['count'] = $DB_master->GetOne('SELECT COUNT(*) FROM basket_tmp WHERE basket_key = ? AND confirm = 1', array($basket_key));

        $result['success'] = true;
    }

    // Групповое обновление количества
    else if($_POST['action'] == 'update_all' && !empty($_POST['cnt']))
    {
        $cnt = (int)$_POST['cnt'];

        $DB_master->Execute('UPDATE basket_tmp SET cnt = ? WHERE basket_key = ?', array($cnt, $basket_key));

        $result['success'] = true;
    }

    // Очистка корзины
    else if ($_POST['action'] == 'clear')
    {
        $DB_master->Execute('DELETE FROM basket_tmp WHERE basket_key = ?', array($basket_key));

        $result['count'] = 0;
    }

    // Добавить все на странице
    else if ($_POST['action'] == 'add_all' && !empty($_POST['ids']))
    {
        $DB_master->Execute('DELETE FROM basket_tmp WHERE basket_key = ? AND confirm = 0', array($basket_key));

        $ins = $val = array();

        foreach ($_POST['ids'] as $k => $item)
        {
            $_POST['ids'][$k] = (float)$item;

            array_push($val, $basket_key, (float)$item);
            array_push($ins, ' (?, NOW(), ?, 1, 0)');
        }

        $ins_str = implode(', ', $ins);

        $DB_master->Execute('INSERT IGNORE INTO basket_tmp VALUES ' . $ins_str, $val);

        $comp_cnt = $DB_master->GetAll('
            SELECT
                basket_tmp.main_id,
                basket_tmp.cnt,
                COUNT(base_main.id) AS offer_cnt,
                base_main.comp_title
            FROM
                basket_tmp
                INNER JOIN base_main ON base_main.id = basket_tmp.main_id
                INNER JOIN users ON base_main.user_id = users.id
            WHERE
                basket_key = ?
                AND base_main.id IN (' . implode(', ', $_POST['ids']) . ')
                AND users.status = 1
            GROUP BY
                base_main.comp_title
            ORDER BY
                base_main.comp_title ASC,
                basket_tmp.cnt DESC
        ', array($basket_key));

        // Сортировка компонентов по алфавиту
        foreach ($comp_cnt as $key => $row)
        {
            $comp_title[$key]  = mb_strtolower($row['comp_title']);
        }
        array_multisort($comp_title, SORT_ASC, $comp_cnt);

        // Добавление хеша названия
        foreach ($comp_cnt as $key => $row)
        {
            $comp_cnt[$key]['comp_hash'] = md5($row['comp_title']);
        }

        $smarty->assign('comp_cnt', $comp_cnt);
        
        $result['in_basket'] = $smarty->fetch('basket_upd_cnt.tpl');

        $result['comps_added'] = count($comp_cnt);

        $result['count'] = $DB_master->GetOne('SELECT COUNT(*) FROM basket_tmp WHERE basket_key = ? AND confirm = 1', array($basket_key));
    }

    // Групповое изменение количества
    else if ($_POST['action'] == 'group_upd' && !empty($_POST['basket_cnt_upd']))
    {
        foreach ($_POST['basket_cnt_upd'] as $k => $item)
        {
            $DB_master->execute('UPDATE basket_tmp
                                INNER JOIN base_main ON base_main.id = basket_tmp.main_id
                                SET cnt = ?, confirm = 1
                                WHERE basket_key = ? AND base_main.comp_title = ?',
                                array($item, $basket_key, $k));
        }
        $result['count'] = $DB_master->GetOne('SELECT COUNT(*) FROM basket_tmp WHERE basket_key = ? AND confirm = 1', array($basket_key));
    }

    // Удалить все на странице
    else if ($_POST['action'] == 'remove_all' && !empty($_POST['ids']))
    {
        $ins = $val = array();

        foreach ($_POST['ids'] as $k => $item)
        {
            array_push($val, $basket_key, $item);
            array_push($ins, ' (basket_key = ? AND main_id = ?)');
        }

        $ins_str = implode(' OR ', $ins);


        $DB_master->Execute('DELETE FROM basket_tmp WHERE ' . $ins_str, $val);

        $result['count'] = $DB_master->GetOne('SELECT COUNT(*) FROM basket_tmp WHERE basket_key = ? AND confirm = 1', array($basket_key));
    }

    // Оформление заявки
    else if ($_POST['action'] == 'order' && !empty($_POST['basket_item_cnt']))
    {
        if (filter_var(trim($_POST['basket_email']), FILTER_VALIDATE_EMAIL) !== false)
        {
            // Подготовка данных
            $ids = array_keys($_POST['basket_item_cnt']);
            $ins_basket['date'] = date('Y-m-d H:i:s');
            $ins_basket['valid'] = 0;
            $user_info['contact_name'] = $_POST['basket_name'];
            $user_info['contact_email'] = $_POST['basket_email'];
            $user_info['contact_phone'] = $_POST['basket_phone'];
            $ins_basket['comments'] = $_POST['basket_comments'];
            $ins_basket['send'] = 0;
            $phone = !empty($user_info['contact_phone']) ? trim($user_info['contact_phone']) : '';

            // Если пользователь авторизован
            $auth = false;
            if (!empty($_POST['client_id']) && !empty($_POST['auth_hash']))
            {
                // Проверяем авторизацию
                $client = $DB->GetRow('SELECT * FROM clients WHERE id = ? AND auth_hash = ?', array((int)$_POST['client_id'], $_POST['auth_hash']));
                if (!empty($client))
                {
                    $auth = true;
                    $client['mail_type'] = 'auth';
                    $ins_basket['valid'] = 1;
                }
            }

            // Если пользователь не авторизован, то проверяем существование пользователя с указанным email
            if (!$auth)
            {
                $client = $DB->GetRow('SELECT * FROM clients WHERE email = ?', array($user_info['contact_email']));
                if (empty($client))
                {
                    // Если пользователя не существует - регистрируем
                    $date_register = date('Y-m-d H:i:s');
                    $auth_hash = md5(uniqid());

                    // Генерируем пароль из 6 цифр
                    $password = '';
                    for($i = 1; $i <= 6; $i++)
                    {
                        $password .= strval(rand(0,9));
                    }
                    $password_hash = md5(iconv('utf-8', 'windows-1251', $password . CLIENT_PASSWORD_PEPPER));

                    if ($DB->Execute('
                        INSERT INTO
                          clients
                        SET
                          email = ?,
                          name = ?,
                          phone = ?,
                          date_register = ?,
                          password = ?,
                          auth_hash = ?,
                          status = -1
                    ', array(
                            $user_info['contact_email'],
                            $user_info['contact_name'],
                            $phone,
                            $date_register,
                            $password_hash,
                            $auth_hash
                        )
                    ))
                    {
                        $client_id = $DB->_connectionID->insert_id;
                        $client = $DB->GetRow('SELECT * FROM clients WHERE id = ?', array($client_id));
                        $client['password'] = $password;
                        $client['url'] = 'https://' . HOST_FRONTEND . '/account/profile/?client_id=' . $client['id'] . '&auth_hash=' . $client['auth_hash'];
                        $client['mail_type'] = 'new';
                    }
                }
                else
                {

                    $client['url'] = 'https://' . HOST_FRONTEND . '/account/profile/?client_id=' . $client['id'] . '&auth_hash=' . $client['auth_hash'];
                    $client['mail_type'] = 'existing';
                }
            }

            // Если необходимая информация о клиенте получена то оформляем заявку
            if (!empty($client))
            {
                // Вставляем изменения телефона
                if (!empty($phone))
                {
                    if ($DB->Execute('UPDATE clients SET phone = ? WHERE id = ?', array($phone, $client['id'])))
                    {
                        $client['phone'] = $phone;
                    }
                }

                $user_info['mail_type'] = $client['mail_type'];
                $user_info['success'] = true;

                // Сохранение данных в cookie
                setcookie('user_info', serialize($user_info), time() + 3600 * 24 * 180, '/');

                // Сохранение заявки
                $ins_basket['client_id'] = $client['id'];

                if ($DB_master->AutoExecute('orders', $ins_basket, 'INSERT'))
                {
                    $order_id = $DB_master->Insert_ID();

                    if ($order_id)
                    {
                        foreach ($ids as $comp_hash)
                        {
                            $DB_master->Execute('
                                INSERT INTO orders_items
                                (
                                    order_id,
                                    user_id,
                                    rcount,
                                    title,
                                    manuf,
                                    descr,
                                    prices,
                                    mfgdate
                                )
                                SELECT
                                    ' . $order_id . ',
                                    main.user_id,
                                    ' . (int)$_POST['basket_item_cnt'][$comp_hash] . ',
                                    main.comp_title as title,
                                    main.manuf_title as manuf,
                                    main.descr,
                                    main.prices,
                                    main.mfgdate
                                FROM
                                    base_main main
                                    INNER JOIN users ON main.user_id = users.id
                                WHERE
                                    users.status = 1
                                    AND main.id = ?
                            ', array((int)$comp_hash));
                        }
                    }

                    // отправка письма с кодом активации
                    $mail = new PHPMailer();
                    $mail->Sender = 'admin@einfo.ru';
                    $mail->SetFrom('admin@einfo.ru', 'Автомат формирования заявок Einfo.ru');
                    $mail->Subject = 'Заявка #' . $order_id;
                    $mail->AddAddress($user_info['contact_email']);
                    $mail->IsHTML();

                    $basket_items = $DB_master->GetAll('SELECT orders_items.*, users.title AS user_title
                                                FROM orders_items
                                                    INNER JOIN users ON orders_items.user_id=users.id
                                                WHERE orders_items.order_id=?', array($order_id));

                    $smarty->assign('site_host', HOST_FRONTEND);
                    $smarty->assign('contact_name', $user_info['contact_name']);
                    $smarty->assign('order_id', $order_id);
                    $smarty->assign('basket_items', $basket_items);
                    $smarty->assign('client', $client);
                    $smarty->assign('comments', $ins_basket['comments']);
                    $smarty->assign('settings', $SETTINGS);

                    $mail->Body = $smarty->fetch('mail_basket_valid.tpl');
                    $result['send'] = $mail->Send();

                    $DB_master->Execute('DELETE FROM basket_tmp WHERE basket_key = ?', array($basket_key));
                    $result['count'] = 0;

                    $result['success'] = 1;
                }
                else
                {
                    $result['send'] = false;
                }
            }
        }
        else
        {
            $result['count'] = 0;
            $result['send'] = false;
            $result['error'] = 'Некорректный адрес электронной почты';
        }
    }
}

print json_encode($result);