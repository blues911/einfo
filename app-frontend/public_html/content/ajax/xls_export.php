<?php

// ----------------------------------------------------------------------

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['items_id']))
{
    include_once (PATH_FRONTEND . '/php/classes/class.Search.php');
    include_once (PATH_FRONTEND . '/php/classes/class.SearchSQL.php');
    include_once (PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

    $search = new SearchSphinxQL();

    $_strong_search = false;

    if (!empty($_POST['xls_multiple_q']))
    {
        $query = explode("\n", $_POST['xls_multiple_q']);
        $multiple_search = true;

        foreach ($query as $key => $value)
        {
            $value = trim($value, " \t\n\r\0\x0B");
            if ($value == '')
            {
                unset($query[$key]);
            }
            else
            {
                // Убираем * если они есть в начале и конце запроса
                if (preg_match('/^\*(.*)\*$/', $value))
                {
                    $value = trim($value, '*');
                }

                $query[$key] = Search::prepare_query_multiple($value);
            }
        }

        $_special_search = (bool)preg_match('/[\*\?]/', $_POST['xls_multiple_q']);
    }
    else
    {
        $query = trim($_POST['query']);
        $multiple_search = false;

        if (preg_match('/^!/', $query))
        {
            $query = preg_replace('/^!/', '', $query);
            $_strong_search = true;
        }

        // Убираем * если они есть в начале и конце запроса
        if (preg_match('/^\*(.*)\*$/', $query))
        {
            $query = trim($query, '*');
        }

        $_special_search = (bool)preg_match('/[\*\?]/', $query);
    }

    $search->set($query, $_strong_search, $multiple_search, $_special_search);

    $search->search_exec($limit = 1000000);
    $filter = $search->get_sql_filter();
    $filter = $filter['where'];

    $items_id = array_keys($search->get_result_exec());
    $list = array_flip($items_id);

    $search_query = sprintf("SELECT
                              main.id,
                              main.comp_title,
                              main.descr,
                              prepare_prices(main.prices, IF(users.price_with_nds, 1, %3\$s), %1\$s, %2\$s) AS prices,
                              main.stock,
                              main.stock_remote,
                              main.delivery,
                              main.comp_url,
                              users.title AS user_title,
                              country.country AS user_country,
                              city.city AS user_city,
                              CONCAT(
                                users.phone1,
                                IF(LENGTH(users.phone1) AND LENGTH(users.phone1_ext), CONCAT(' доб. ', users.phone1_ext), ''),
                                IF(LENGTH(users.phone1) AND LENGTH(users.phone2), ', ', ''),
                                users.phone2,
                                IF(LENGTH(users.phone2) AND LENGTH(users.phone2_ext), CONCAT(' доб. ', users.phone2_ext), '')
                              ) AS user_phone,
                              main.manuf_title,
                              users.www AS user_www,
                              users.email AS user_email,
                              users.fax AS user_fax,
                              users.address AS user_address
                            FROM
                              base_main main
                              INNER JOIN users ON main.user_id = users.id
                              INNER JOIN ip2ruscity_countries AS country ON country.country_id = users.country_id
                              INNER JOIN ip2ruscity_cities AS city ON city.city_id = users.city_id
                            WHERE users.status = 1 AND main.id IN (" . implode(", ", $items_id) . ") %4\$s AND users.id NOT IN (%5\$s, %6\$s)
                            ORDER BY users.rating DESC, main.user_id ASC, main.comp_title ASC
                            LIMIT 65000",
                            $SETTINGS['cur_rates']['usd'],
                            $SETTINGS['cur_rates']['eur'],
                            $SETTINGS['nds']['value'],
                            $filter,
                            USER_ADMIN_ID,
                            USER_PARTNER_ID
    );

    $rs = $DB->Execute($search_query);

    if (is_object($rs))
    {
        include_once(PATH_FRONTEND . '/php/libs/PHPExcel/PHPExcel.php');

        // Генерируем excel файл с результатами
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator('User');

        $objPHPExcel->setActiveSheetIndex(0);

        $active_sheet = $objPHPExcel->getActiveSheet();

        if (is_array($query))
        {
            $_query = array();
            foreach ($query as $_q)
            {
                $_query[] = $_q['query'];
            }
            $query = implode(', ', $_query);
        }

        $active_sheet->mergeCells('A1:L2')
            ->setCellValue('A1', 'Результат поискового запроса ' . ($query !== '' ? '«' . $query . '»' : '') . ' на einfo.ru');

        $active_sheet->setCellValue('A3', 'Компонент');
        $active_sheet->setCellValue('B3', 'Описание');
        $active_sheet->setCellValue('C3', 'На складе');
        $active_sheet->setCellValue('D3', 'Под заказ');
        $active_sheet->setCellValue('E3', 'Цена');
        $active_sheet->setCellValue('F3', 'Поставщик');
        $active_sheet->setCellValue('G3', 'Страна, город');
        $active_sheet->setCellValue('H3', 'Адрес');
        $active_sheet->setCellValue('I3', 'Телефон');
        $active_sheet->setCellValue('J3', 'Факс');
        $active_sheet->setCellValue('K3', 'Адрес сайта');
        $active_sheet->setCellValue('L3', 'Электронная почта');

        $num = 4;

        while (!$rs->EOF)
        {
            // Вариант наличия
            $stock = '-';

            if ($rs->fields('stock') > 0)
            {
                $stock = $rs->fields('stock') . ' шт.';
            }
            elseif ($rs->fields('stock') < 0)
            {
                $stock = 'На складе';
            }

            $stock_remote = '';

            // Вариант наличия на удалённом складе
            if ($rs->fields('stock_remote') != 0)
            {
                $stock_remote .= $rs->fields('stock_remote') . ' шт.';

                if ($rs->fields('delivery') != 0)
                {
                    $stock_remote .= ', ';
                }
            }

            if ($rs->fields('delivery') > 0)
            {
                $stock_remote .= $rs->fields('delivery') . ' ' . plural($rs->fields('delivery'), 'неделя', 'недели', 'недель');
            }
            elseif ($rs->fields('delivery') < 0)
            {
                $stock_remote .= 'Заказ';
            }

            if ($rs->fields('stock_remote') == 0 && $rs->fields('delivery') == 0)
            {
                $stock_remote = '-';
            }

            $description = '';

            $manuf_title = preg_replace('/^[=]+/', '', $rs->fields('manuf_title'));
            $descr = preg_replace('/^[=]+/', '', $rs->fields('descr'));

            $user_fax = $rs->fields('user_fax');
            $user_www = $rs->fields('user_www');
            $user_email = $rs->fields('user_email');

            if (!empty($manuf_title)) $description .= $manuf_title;
            if (!empty($manuf_title) && !empty($descr)) $description .= ', ';
            if (!empty($descr)) $description .= $descr;

            $active_sheet->setCellValue('A' . $num, $rs->fields('comp_title'));
            $active_sheet->setCellValue('B' . $num, $description);
            $active_sheet->setCellValue('C' . $num, $stock);
            $active_sheet->setCellValue('D' . $num, $stock_remote);

            $prices = json_decode($rs->fields('prices'), true);
            $p_res = ' - ';

            if (!empty($prices))
            {
                $prices_total = count($prices);
                $p_res = '';
                $i = 1;

                foreach($prices as $price)
                {
                    if ($prices_total == 1 && $price['p'] > 0)
                    {
                        $p_res .= format_price($price['p']) . ' руб.';
                    }
                    else if ($prices_total > 1 && $price['p'] > 0)
                    {
                        $qty = ($i == $prices_total) ? $price['n'] . '+ шт: ' : $price['n'] . ' шт: ';
                        $pc = format_price($price['p']) . ' руб.';
        
                        $p_res .= $qty . $pc . "\n";
                        $i++;
                    }
                }
            }

            $active_sheet->setCellValue('E' . $num, $p_res);
            $active_sheet->setCellValue('F' . $num, $rs->fields('user_title'));
            $active_sheet->setCellValue('G' . $num, $rs->fields('user_country') . ', ' . $rs->fields('user_city'));
            $active_sheet->setCellValue('H' . $num, $rs->fields('user_address'));
            $active_sheet->setCellValue('I' . $num, $rs->fields('user_phone'));
            $active_sheet->setCellValue('J' . $num, !empty($user_fax) ? $user_fax : ' - ');
            $active_sheet->setCellValue('K' . $num, !empty($user_www) ? $user_www : ' - ');
            $active_sheet->setCellValue('L' . $num, !empty($user_email) ? $user_email : ' - ');
            $num++;

            $rs->MoveNext();
        }

        // ------ Обработка внешнего вида ячеек -------

        $align_right = array(
            'alignment' => array(
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
            ),
        );

        $align_center = array(
            'alignment' => array(
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
            ),
        );

        $search_title = array(
            'alignment' => array(
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
                'vertical'   => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'size' => 14,
                'name' => 'Colibri'
            ),
        );

        $cell = array(
            'alignment' => array(
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_LEFT,
                'vertical'   => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
            ),
            'font' => array(
                'size' => 10,
            ),
        );

        $table_head = array(
            'alignment' => array(
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'size' => 12,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array(
                        'rgb' => '696969'
                    )
                )
            )
        );

        $borders = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array(
                        'rgb' => '696969'
                    )
                ),
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        'rgb' => 'A0A0A0'
                    )
                )
            )
        );

        $border_bottom = array(
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => array(
                        'rgb' => '696969'
                    )
                )
            )
        );

        $currency = array(
            'alignment' => array(
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_RIGHT,
                'vertical'   => PHPExcel_STYLE_ALIGNMENT::VERTICAL_TOP,
            ),
            'numberformat' => array(
                'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
            )
        );

        foreach (range('C', 'L') as $column)
        {
            $active_sheet->getColumnDimension($column)->setAutoSize(true);
        }

        for ($l = 4; $l <= $num - 1; $l++)
        {
            $active_sheet->getStyle('B' . $l)
                ->getAlignment()
                ->setWrapText(true);
        }

        $active_sheet->getColumnDimension('A')->setWidth(30);
        $active_sheet->getColumnDimension('B')->setWidth(60);

        $active_sheet->getStyle('A1')->applyFromArray($search_title);
        $active_sheet->getStyle('A1:L' . ($num - 1))->applyFromArray($borders);
        $active_sheet->getStyle('A2:L2')->applyFromArray($border_bottom);
        $active_sheet->getStyle('A3:L3')->applyFromArray($table_head);
        $active_sheet->getStyle('C4:C' . ($num - 1))->applyFromArray($align_center);
        $active_sheet->getStyle('A4:L' . ($num - 1))->applyFromArray($cell);
        $active_sheet->getStyle('D4:E' . ($num - 1))->applyFromArray($currency);
        $active_sheet->getStyle('A' . $num)->applyFromArray(array());


        // ------ Подготовка к выводу содержимого -------

        // Выводим HTTP-заголовки
        header('Expires: Mon, 1 Apr 1974 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D,d M YH:i:s') . ' GMT');
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=export_' . date('Y-m-d_H-i-s') . '.xls');

        // Выводим содержимое файла
        @$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        @$objWriter->save('php://output');

        die();
    }
}
