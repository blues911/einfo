<?php

$response = array();
$response['success'] = false;

if (!empty($_POST['email']))
{

    if (filter_var(trim($_POST['email']), FILTER_VALIDATE_EMAIL) !== false)
    {
        $client = $DB->GetRow('SELECT id, auth_hash, email FROM clients WHERE email = ?', array($_POST['email']));

        if (!empty($client))
        {

            $mail = new PHPMailer();
            $mail->Sender = 'admin@einfo.ru';
            $mail->setFrom('admin@einfo.ru', 'Einfo');
            $mail->Subject = '[einfo.ru] Восстановление доступа к личному кабинету';
            $mail->IsHTML();
            $mail->AddAddress($client['email']);

            $url = 'https://' . HOST_FRONTEND . '/account/profile/?client_id=' . $client['id'] . '&auth_hash=' . $client['auth_hash'];
            $smarty->assign('url', $url);

            $mail->Body = $smarty->fetch('account/mail_remember.tpl');

            $mail->Send();
            $mail->ClearAddresses();

            $response['info'] = 'Письмо с инструкциями отправлено на электронную почту';
            $response['success'] = true;
        }
        else
        {
            $response['error'] = 'Электронная почта не зарегестрирована в системе';
        }
    }
    else
    {
        $response['error'] = 'Некорректный адрес электронной почты';
    }
}

echo(json_encode($response));