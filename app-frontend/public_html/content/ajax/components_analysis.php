<?php

$result['success'] = false;
$action = !empty($_POST['action']) ? $_POST['action'] : false;
$file_id = !empty($_POST['file_id']) ? (int) $_POST['file_id'] : false;

if ($action)
{
    session_write_close();

    include_once('php/classes/class.ComponentsEffectAnalysis.php');

    $analysis = new ComponentsEffectAnalysis();

    // Анализ файла. Если в процессе анализа не встретилось ошибки,
    // то возвращается id записи о файле, скрипт продолжает работать в фоне
    if (!empty($_FILES['file']['name']) && $action == 'analysis')
    {
        $result = $analysis->analyze_file($_FILES['file']);
    }

    // Отслеживание статуса обработки файла
    else if ($action == 'watch_analysis' && !empty($file_id))
    {
        $result = $analysis->watch_analysis($file_id);
    }
}

echo json_encode($result);