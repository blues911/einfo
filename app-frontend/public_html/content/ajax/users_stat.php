<?php

if (!empty($_POST['user_id']) && !empty($_POST['action']))
{
    $user = $DB->GetOne('SELECT id FROM users WHERE id = ? AND status = 1', array((int)$_POST['user_id']));

    if (!empty($user))
    {
        $referer = $_SERVER['HTTP_REFERER'];
        $component_title_atoms = '';

        if (preg_match('/https?:\/\/([^\/]+)\/(search\/)?(.*?)(\/|$|\?|#)/', $referer, $matches))
        {
            if (isset($matches[3]))
            {
                $component_title = mb_strtolower(urldecode($matches[3]));
                $component_title_atoms = implode(' ', get_request_atoms($component_title));
            }
        }

        $DB->Execute('
            INSERT INTO
              users_stat
            SET
              user_id = ?,
              date = NOW(),
              ip = ?,
              referer = ?,
              action = ?,
              search_atoms = ?
        ', array(
                (int)$_POST['user_id'],
                get_ip(),
                $referer,
                $_POST['action'],
                $component_title_atoms
            )
        );
    }

}