<?php

if (!empty($_GET['user_id']))
{
    // Заголовок прайс-листа
    $user = $DB->GetRow('
        SELECT
          users.*,
          country.country,
          city.city,
          CONCAT(
            users.phone1,
            IF(LENGTH(users.phone1) AND LENGTH(users.phone1_ext), CONCAT(" доб. ", users.phone1_ext), ""),
            IF(LENGTH(users.phone1) AND LENGTH(users.phone2), ", ", ""),
            users.phone2,
            IF(LENGTH(users.phone2) AND LENGTH(users.phone2_ext), CONCAT(" доб. ", users.phone2_ext), "")
          ) AS phone
        FROM
          users
          INNER JOIN ip2ruscity_countries AS country ON country.country_id = users.country_id
          INNER JOIN ip2ruscity_cities AS city ON city.city_id = users.city_id
        WHERE
          users.id = ?
    ', array($_GET['user_id']));

    // Выбрасываем 404 если пользователь не активен
    if ($user['status'] == 1 && $user['allow_download_price'] == 1)
    {
        if (empty($user['country']))
        {
            $user['country'] = '-';
        }

        if (empty($user['city']))
        {
            $user['city'] = '-';
        }

        if ($user['individual_buyers'])
        {
            $user['individual_buyers'] = 'Да';
        }
        else
        {
            $user['individual_buyers'] = 'Нет';
        }

        $price_header = html_entity_decode($user['title']) . "\t\t\r\n" .
            "Описание:\t" . html_entity_decode($user['about']) . "\t\r\n" .
            "Сайт:\t" . $user['www'] . "\t\r\n" .
            "Страна:\t" . $user['country'] . "\t\r\n" .
            "Город:\t" . $user['city'] . "\t\r\n" .
            "Адрес:\t" . $user['address'] . "\t\r\n" .
            "Телефон:\t" . $user['phone'] . "\t\r\n" .
            "Факс:\t" . $user['fax'] . "\t\r\n" .
            "Эл. почта:\t" . $user['email'] . "\t\r\n" .
            "Работает с физ. лицами:\t" . $user['individual_buyers'] . "\t\r\n" .
            "Мин. сумма заказа:\t" . $user['min_order_amount'] . "\t\r\n";

        // Тело прайс-листа
        $row = $DB->Execute('
            SELECT
              main.comp_title,
              main.prices
            FROM
              base_main AS main
            WHERE
              main.user_id = ?
            ORDER BY
              main.comp_title ASC
            ', array($_GET['user_id'])
        );
        
        // Цена руб.
        // Только первую цену

        $price_body = "\r\n\r\nНаименование\tЦена руб.\r\n";
        while (!$row->EOF)
        {
            $prices_list = json_decode($row->fields['prices'], true);
            $p_res = '-';

            if (!empty($prices_list))
            {
                $price = array_shift($prices_list);

                if ($price['c'] == 'rur' && $price['p'] > 0)
                {
                    $p_res = format_price($price['p']);
                }
                else if ($price['c'] == 'usd' && $price['p'] > 0)
                {
                    $price['p'] = $price['p'] * $usd;
                    $p_res = format_price($price['p']);
                }
                else if ($price['c'] == 'eur' && $price['p'] > 0)
                {
                    $price['p'] = $price['p'] * $eur;
                    $p_res = format_price($price['p']);
                }
                else
                {
                    $p_res = '-';
                }
            }

            $row->fields['prices'] = $p_res;
            $price_body .= implode("\t", $row->fields) . "\r\n";
            $row->MoveNext();
        }

        // Отдаем на скачивание
        $price_list = iconv('utf-8', 'windows-1251', $price_header . $price_body);
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: text/plain');

        $filename = str_replace('"', '\'', iconv('utf-8', 'windows-1251', html_entity_decode($user['title'] . '.txt', ENT_QUOTES, 'UTF-8')));

        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Length: ' . mb_strlen($price_list, 'windows-1251'));
        print $price_list;
        exit();
    }
    else
    {
        include_once ('content/objects/settings.php');
        include_once ('content/objects/common.php');
        include_once ('content/objects/menu.php');

        $container['content'] = file_get_contents('content/error_404.html');

        $container['robots'] = 'noindex, nofollow';

        $smarty->assign('container', $container);
        $smarty->display('_main.tpl');
        exit;
    }

}