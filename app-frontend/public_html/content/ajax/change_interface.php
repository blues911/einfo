<?php

$response = array(
    'success' => 0
);

$USER_SETTINGS = !empty($USER_SETTINGS) ? $USER_SETTINGS : array();

$_interface = isset($_POST['interface']) ? (int)$_POST['interface'] : DEFAULT_INTERFACE;
$USER_SETTINGS['interface'] = $_interface > 1 ? 0 : $_interface;

// Сохранение настроек
$_user_settings = serialize($USER_SETTINGS);
if (!empty($client['auth']))
{
    $DB->Execute('UPDATE clients SET settings = ? WHERE id = ?', array($_user_settings, $client['id']));
}

$response['success'] = 1;

echo(json_encode($response));