<?php

$response = array();
$response['success'] = 0;

if (!empty($client['auth']))
{

    if (!empty($_POST['var']) && !empty($_POST['value']))
    {
        $USER_SETTINGS[$_POST['var']] = $_POST['value'];
        $_user_settings = serialize($USER_SETTINGS);
        $DB->Execute('UPDATE clients SET settings = ? WHERE id = ?', array($_user_settings, $client['id']));
        $response['success'] = 1;
    }

}

echo(json_encode($response));
