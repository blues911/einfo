<?php

header('Access-Control-Allow-Origin: *'); 
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');

include_once(PATH_FRONTEND . '/php/classes/class.API.php');

try
{
    $api = new API();

    $api->check_request_schema();
    $api->check_authorization();
    $api->check_service_exist();

    include_once($api->get_service_path());
}
catch (Exception $e)
{
    $api->handle_error($e->getCode());
}
