<?php

$result = false;

if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['set_cookie']))
{
    $response = array(
        'success' => setcookie('is_search', '1', time() + 30, '/')
    );

    echo json_encode($response);
}
elseif($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['action']))
{
    // Autocomplete подсказка поиска
    if ($_POST['action'] == 'autocomplete' && !empty($_POST['term']))
    {
        $term = urldecode(strip_tags(trim($_POST['term'])));

        if (!empty($term))
        {
            $result['rows'] = $DB->GetAll(
                'SELECT cc.comp_title, COUNT(DISTINCT(bm.user_id)) AS cnt
                 FROM canonical_comp AS cc
                      LEFT JOIN base_main AS bm ON bm.comp_title = cc.comp_title AND bm.stock > 0
                 WHERE cc.comp_title LIKE ?
                 GROUP BY cc.comp_title
                 ORDER BY cnt DESC
                 LIMIT 20',
                array($term . '%')
            );
        }
    }

    // Информация о поставщике
    else if ($_POST['action'] == 'user_info' && !empty($_POST['id']))
    {
        $id = (int)$_POST['id'];

        $user = $DB->GetRow("
            SELECT
                users.*, country.country, city.city,
                IF(users.price_date < NOW() - INTERVAL 7 DAY, IF(users.price_date < NOW() - INTERVAL 14 DAY, 3, 2), 1) AS price_status
            FROM users
                INNER JOIN ip2ruscity_countries country ON country.country_id=users.country_id
                INNER JOIN ip2ruscity_cities city ON city.city_id=users.city_id
            WHERE users.id=? AND users.status=1
        ", array($id));

        $user['phone'] = !empty($user['phone1']) ? $user['phone1'] : '';
        $user['phone'] .= !empty($user['phone1']) && !empty($user['phone1_ext']) ? ' доб. ' . $user['phone1_ext'] : '';
        $user['phone'] .= !empty($user['phone1']) && !empty($user['phone2']) ? ', ' : '';
        $user['phone'] .= !empty($user['phone2']) ? $user['phone2'] : '';
        $user['phone'] .= !empty($user['phone2']) && !empty($user['phone2_ext']) ? ' доб. ' . $user['phone2_ext'] : '';

        // Прогон ссылки на сайт через регулярку
        $user['url_text'] = preg_replace('/http(s)?:\/\//', '', $user['www']);

        // Прогон телефонов через регулярку
        $user['phone'] = preg_replace('/\s*[,;]\s*/', "\n", $user['phone']);
        $user['fax'] = preg_replace('/\s*[,;]\s*/', "\n", $user['fax']);

        // Вариант вывода даты
        $user['price_date'] = $date->GetRusDate($user['price_date'], 1);

        $smarty->assign('user', $user);
        $smarty->assign('user_settings', $GLOBALS['USER_SETTINGS']);
        $result['html'] = $smarty->fetch('search_user_info.tpl');
    }
}

print json_encode($result);