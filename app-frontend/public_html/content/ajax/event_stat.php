<?php
/*
    Запись расширенной статистики по пользователям сайта в таблицу event_stat
*/
if (
    $_SERVER['REQUEST_METHOD'] !== 'POST'
    || empty($_POST['event_type'])
    || empty($_POST['event_label'])
    || empty($_SERVER['HTTP_REFERER'])
    || (!empty($_SERVER['HTTP_REFERER'])
        && !preg_match("/^https?:\/\/[a-z0-9-.]*einfo.ru\//i", urldecode($_SERVER['HTTP_REFERER']))
       )
    )
{
    exit();
}

// формируем объект статистических данных на основе данных, пришедших с клиента
$ins_stat = array();

$ins_stat['date']        = date('Y-m-d H:i:s');
$ins_stat['ip']          = get_ip();
$ins_stat['useragent']   = $_SERVER['HTTP_USER_AGENT'];
$ins_stat['referer']     = isset($_SERVER['HTTP_REFERER']) ? urldecode($_SERVER['HTTP_REFERER']) : '';
$ins_stat['screen']      = !empty($_POST['screen']) ? $_POST['screen'] : '';
$ins_stat['hardware']    = !empty($_POST['hardware']) ? $_POST['hardware'] : '';
$ins_stat['event_type']  = $_POST['event_type'];
$ins_stat['event_label'] = !empty($_POST['event_label']) ? $_POST['event_label'] : '';

// пользователь с такой кукой уже существует => не первый заход
if (!empty($_POST['stat_id']))
{
    $stat_id = $DB->GetOne('SELECT stat_id FROM event_stat WHERE stat_id = ?', array($_POST['stat_id']));
    $ins_stat['stat_id'] = ($stat_id) ? $stat_id : md5(uniqid());
}
// у пользователя нет куки => 1-ый заход
else
{
    $ins_stat['stat_id'] = md5(uniqid());
}

$sql = 'SELECT
          IF (COUNT(stat_id) < 200, 1, 0)
        FROM
          event_stat
        WHERE
          stat_id = ?
          AND DATE > SUBDATE(NOW(), INTERVAL 5 MINUTE)';

$allow_add = $DB->GetOne($sql, array($ins_stat['stat_id']));

if ($allow_add)
{
    $DB->AutoExecute('event_stat', $ins_stat, 'INSERT');
    echo json_encode(array('stat_id' => $ins_stat['stat_id']));
}
