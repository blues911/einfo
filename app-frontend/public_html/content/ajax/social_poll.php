<?php

$response = array();
$response['success'] = false;
$social_poll = $DB->GetRow('SELECT * FROM social_polls WHERE status = 1');

if ($_SERVER['REQUEST_METHOD'] == 'POST'
    && isset($_POST['type'])
    && !empty($social_poll))
{
    $cookie = unserialize($_COOKIE['social_poll']);

    // Проверка лимита просмотров
    if ($_POST['type'] == 'check_limit')
    {
        $response['form'] = '';

        // Если количество просмотренных страниц в интервале мин и макс количества просмотров
        // а также текущее голосование ещё не пройдено
        if (in_array((int)$cookie['views'], range((int)$social_poll['views_min'], (int)$social_poll['views_max']))
            && !in_array($social_poll['id'], $cookie['ids']))
        {
            // Отправка формы
            $smarty->assign('id', $social_poll['id']);
            $smarty->assign('questions', json_decode($social_poll['questions'], true));

            $response['form'] = $smarty->fetch('social_poll_form.tpl');
        }

        $response['success'] = true;
    }

    // Закрытие формы
    elseif ($_POST['type'] == 'close_form')
    {
        $data = array();
        $data['ip'] = get_ip();
        $data['type'] = 'close';
        $data['text'] = 'Отказ от ответа';
        $data['page'] = trim($_POST['page']);
        $data['cookies'] = !empty($_COOKIE) ? $_COOKIE : null;

        if (isset($_COOKIE['social_poll_like']))
        {
            if ($_COOKIE['social_poll_like'] == 'yes')
            {
                $data['text'] = 'Сайт понравился.';
            }
            else if ($_COOKIE['social_poll_like'] == 'no')
            {
                $data['text'] = 'Сайт не понравился.';
            }
        }

        $smarty->assign('data', $data);

        $mail = new PHPMailer();
        $mail->SetFrom('admin@einfo.ru', 'Einfo');
        $mail->Subject = 'Опрос einfo - ' . $social_poll['title'];
        $mail->IsHTML(true);
        $mail->Body = $smarty->fetch('mail_social_poll.tpl');
        $mail->AddAddress('admin@einfo.ru');
        $mail->Send();

        $cookie['views'] = 0;

        if (!in_array($social_poll['id'], $cookie['ids']))
            array_push($cookie['ids'], $social_poll['id']);

        setcookie('social_poll', serialize($cookie), time() + 3600 * 24 * 30, '/');
        setcookie('social_poll_like', '', time() - 3600);

        $response['success'] = true;
    }

    // Обработка данных формы
    elseif ($_POST['type'] == 'send_form')
    {
        $data = array();
        $data['ip'] = get_ip();
        $data['type'] = 'send';
        $data['text'] = 'Отказ от ответа';
        $data['page'] = trim($_POST['page']);
        $data['cookies'] = !empty($_COOKIE) ? $_COOKIE : null;

        if (isset($_COOKIE['social_poll_like']))
        {
            if ($_COOKIE['social_poll_like'] == 'yes')
            {
                $data['text'] = 'Сайт понравился.';
            }
            else if ($_COOKIE['social_poll_like'] == 'no')
            {
                $data['text'] = 'Сайт не понравился.';
            }
        }

        foreach ($_POST['question'] as $k => $v)
        {
            $data['content'][$k]['question'] = $v;
        }
    
        foreach ($_POST['answer'] as $k => $v)
        {
            $data['content'][$k]['answer'] = strip_tags($v);
        }
        
        $smarty->assign('data', $data);
        
        $mail = new PHPMailer();
        $mail->SetFrom('admin@einfo.ru', 'Einfo');
        $mail->Subject = 'Опрос einfo - ' . $social_poll['title'];
        $mail->IsHTML(true);
        $mail->Body = $smarty->fetch('mail_social_poll.tpl');
        $mail->AddAddress('admin@einfo.ru');
        $mail->Send();

        $cookie['views'] = 0;

        if (!in_array($social_poll['id'], $cookie['ids']))
            array_push($cookie['ids'], $social_poll['id']);

        setcookie('social_poll', serialize($cookie), time() + 3600 * 24 * 30, '/');
        setcookie('social_poll_like', '', time() - 3600);

        $response['success'] = true;
    }
}
 
echo json_encode($response);