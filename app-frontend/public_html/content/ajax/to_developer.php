<?php

$response = array(
    'success' => false
);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['text']))
{
    $mail = new PHPMailer();
    $mail->Sender = 'admin@einfo.ru';
    $mail->SetFrom('admin@einfo.ru', 'Einfo.ru');
    $mail->Subject = 'Форма обратной связи для разработчика';
    $mail->AddAddress('admin@einfo.ru');
    $mail->IsHTML();

    $smarty->assign('site_host', HOST_FRONTEND);
    $smarty->assign('text', trim($_POST['text']));
    $smarty->assign('page', trim($_POST['page']));
    $smarty->assign('ip', get_ip());
    $smarty->assign('cookies', !empty($_COOKIE) ? $_COOKIE : null);

    $mail->Body = $smarty->fetch('mail_to_developer.tpl');
    $response['success'] = $mail->Send();
}

echo json_encode($response);