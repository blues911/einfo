<?php

// Запись настроек в cookie
if (!empty($_POST['form_settings']))
{
    $USER_SETTINGS = !empty($USER_SETTINGS) ? $USER_SETTINGS : array();


    // Предприятия, исключенные из результатов поиска
    $ip_address = get_ip();

    $previous_condition = !empty($_POST['previous_condition']) ? array_keys($_POST['previous_condition']) : array();

    $USER_SETTINGS['search_users_disallow'] = array();

    if (isset($_POST['search_users_disallow']))
    {
        $USER_SETTINGS['search_users_disallow'] = array_keys($_POST['search_users_disallow']);

        foreach ($USER_SETTINGS['search_users_disallow'] as $value)
        {
            // Если предыдушее состояние отличается пишем в лог на добавление
            // и удаляем id из массива предыдущего состояния
            if(in_array($value, $previous_condition))
            {
                $key = array_search($value, $previous_condition);
                unset($previous_condition[$key]);
            }
            else
            {
                $DB_master->Execute('INSERT INTO clients_list_log VALUES(?, ?, ?, ?, ?, NOW())',
                    array($ip_address, $value, $client['id'], 'black', 'add'));
            }
        }
    }

    // Если массив предыдущего состояния не пуст, значит было удаление из списка
    if (!empty($previous_condition))
    {
        foreach ($previous_condition as $value)
        {
            $DB_master->Execute('INSERT INTO clients_list_log VALUES(?, ?, ?, ?, ?, NOW())',
                array($ip_address, $value, $client['id'], 'black', 'delete'));
        }
    }

    // Сохранение настроек
    $_user_settings = serialize($USER_SETTINGS);
    if (!empty($client['auth']))
    {
        $DB->Execute('UPDATE clients SET settings = ? WHERE id = ?', array($_user_settings, $client['id']));
    }

    $smarty->assign('save_succes', true);
}

$users_all = $DB->GetAll('SELECT id, title FROM users WHERE status=1 AND id NOT IN (?, ?) ORDER BY title ASC', array(USER_ADMIN_ID, USER_PARTNER_ID));

if (!empty($USER_SETTINGS['search_users_disallow']))
{

    foreach ($users_all as $k => $value)
    {
        if (in_array($value['id'], $USER_SETTINGS['search_users_disallow']))
        {
            $users_all[$k]['checked'] = true;
        }
    }
}

$users_all = sort_by_columns($users_all, 3, 2);

$smarty->assign('users_all', $users_all);
$smarty->assign('users_white_list', $USER_SETTINGS['search_users_white_list']);
$smarty->display('account/black_list.tpl');
