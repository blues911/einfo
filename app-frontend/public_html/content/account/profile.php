<?php

$info = '';
$error = '';

if (!empty($_POST['action']) && $client['auth'])
{
    if ($_POST['action'] == 'edit')
    {

        $upd = array();
        if (!empty($_POST['name']))
        {
            $upd['name'] = trim($_POST['name']);
        }
        if (!empty($_POST['password']))
        {
            $upd['password'] = md5(iconv('utf-8', 'windows-1251', $_POST['password'] . CLIENT_PASSWORD_PEPPER));
        }
        if (!empty($_POST['details']))
        {
            $upd['details'] = $_POST['details'];
        }
        $upd['phone'] = !empty($_POST['phone']) ? trim($_POST['phone']) : '';
        $upd['details_in_order'] = !empty($_POST['details_in_order']) ? 1 : 0;

        if ($DB->AutoExecute("clients", $upd, "UPDATE", "id=" . $client['id']))
        {
            $info = 'Изменения сохранены.';

            // Обновление данных пользователя из БД
            $_client = $DB->GetRow('SELECT * FROM clients WHERE id = ?', array($client['id']));
            $client = array_merge($client, $_client);

        }
        else
        {
            $error = 'Ошибка сохранения.';
        }

    }
}

$smarty->assign('client', $client);
$smarty->assign('profile_info', $info);
$smarty->assign('profile_error', $error);
$smarty->display('account/profile.tpl');