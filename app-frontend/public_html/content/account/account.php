<?php

$container['no_breadcrumbs'] = true;

// Раздел по умолчанию
preg_match('/^account\/?([-a-z0-9_]*)/i', $path_url, $matches);
$module = !empty($matches[1]) ? $matches[1] : 'orders';

// Если пользователь не авторизован, то модуль по умолчанию auth
if (empty($client['auth']))
{
    $module = 'auth';
}
elseif ($module == 'auth' && !empty($client['auth']))
{
    $module = 'orders';
}

if ($module == 'auth')
{
    $smarty->display('account/auth.tpl');
}
else
{
    // Проверяем существование файла
    $file_name = 'content/account/' . $module . '.php';
    if (file_exists($file_name))
    {

        // Парсим шаблон
        ob_start();
        include($file_name);
        $account_content = ob_get_contents();
        ob_end_clean();

        $smarty->assign('account_content', $account_content);
        $smarty->assign('module', $module);
        $smarty->display('account/_main.tpl');

    }
    else
    {
        throw new Exception('Error 404', 404);
    }
}


