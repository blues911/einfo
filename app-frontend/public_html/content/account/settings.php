<?php

// Запись настроек в cookie
if (!empty($_POST['form_settings']))
{
    $USER_SETTINGS = !empty($USER_SETTINGS) ? $USER_SETTINGS : array();

    if (isset($_POST['individual_buyers']))
    {
        $USER_SETTINGS['individual_buyers'] = true;
    }
    else
    {
        $USER_SETTINGS['individual_buyers'] = false;
    }

    $_interface = isset($_POST['interface']) ? (int)$_POST['interface'] : DEFAULT_INTERFACE;
    $USER_SETTINGS['interface'] = $_interface > 1 ? 0 : $_interface;

    // Сохранение настроек
    $_user_settings = serialize($USER_SETTINGS);
    if (!empty($client['auth']))
    {
        $DB->Execute('UPDATE clients SET settings = ? WHERE id = ?', array($_user_settings, $client['id']));
    }

    $smarty->assign('save_success', true);
}

$individual_buyers = false;
if (!empty($USER_SETTINGS['individual_buyers']))
{
    $individual_buyers = true;
}

$_interface = isset($USER_SETTINGS['interface']) ? (int)$USER_SETTINGS['interface'] : DEFAULT_INTERFACE;
if ($_interface > 1) $_interface = 0;

$smarty->assign('interface', $_interface);
$smarty->assign('individual_buyers', $individual_buyers);
$smarty->display('account/settings.tpl');