<?php

// валидация заявки и отправка поставщикам писем с кодом подтверждения
if (!empty($_GET['order_id']) && !empty($_GET['client_id']) && !empty($_GET['auth_hash']))
{
    $order = $DB_master->GetRow('
        SELECT
          orders.*,
          clients.email AS contact_email
        FROM
          orders
          INNER JOIN clients ON orders.client_id = clients.id
        WHERE
          orders.valid = 0
          AND orders.id = ?
    ', array((int)$_GET['order_id']));

    if ($order)
    {
        // валидация
        $DB_master->Execute('UPDATE orders SET valid=1 WHERE id=?', array($order['id']));

        $smarty->assign('order_id', $order['id']);
        $smarty->assign('confirm', 'yes');
    }
    else
    {
        $smarty->assign('confirm', 'no');
    }
}

// просмотр заявки
else if (!empty($_GET['view']))
{
    $smarty->assign('view', true);

    $basket = $DB_master->GetRow('
        SELECT
          orders.*,
          clients.email AS contact_email
        FROM
          orders
          INNER JOIN clients ON orders.client_id = clients.id
        WHERE
          orders.valid = 1
          AND orders.id = ?
    ', array($_GET['view']));

    if ($basket)
    {
        $user_hash = md5($basket['contact_email'] . ORDERS_EMAIL_PEPPER);
        $container['breadcrumbs'] = array('/account/orders/?user=' . $user_hash => 'Мои заявки');

        $basket['date'] = $date->GetRusDate($basket['date'], 4);

        $basket_items = $DB_master->GetAll('SELECT
                                                item.*,
                                                users.id AS user_id,
                                                users.title AS user_title,
                                                CONCAT(
                                                    users.phone1,
                                                    IF(LENGTH(users.phone1) AND LENGTH(users.phone1_ext), CONCAT(" доб. ", users.phone1_ext), ""),
                                                    IF(LENGTH(users.phone1) AND LENGTH(users.phone2), ", ", ""),
                                                    users.phone2,
                                                    IF(LENGTH(users.phone2) AND LENGTH(users.phone2_ext), CONCAT(" доб. ", users.phone2_ext), "")
                                                ) AS user_phone
                                            FROM orders_items AS item
                                                INNER JOIN users ON users.id=item.user_id
                                            WHERE item.order_id = ?
                                            ORDER BY item.user_id ASC', array($basket['id']));
        
        $smarty->assign('basket', $basket);
        $smarty->assign('basket_items', $basket_items);
    }
}
else if (isset($_GET['success']))
{
    $smarty->assign('success', true);
}
// Список всех заявок
else
{
    $orders = $DB_master->GetAll('SELECT
                                      orders.*,
                                      COUNT(orders_items.id) AS comp_all
                                  FROM orders
                                      INNER JOIN orders_items ON orders_items.order_id = orders.id
                                  WHERE
                                      client_id = ?
                                      AND orders.valid = 1
                                  GROUP BY orders.id
                                  ORDER BY orders.id DESC', array($client['id']));
    if($orders)
    {
        foreach ($orders as $k => $item)
        {
            $orders[$k]['date'] =  $date->GetRusDate($item['date'], 4);
        }

        $smarty->assign('orders', $orders);
    }
}

$smarty->display('account/orders.tpl');