<?php

if (isset($_GET['id']))
{
    $container['top']['news'] = true;

    $id = (int)$_GET['id'];
    $news = $DB->GetRow('SELECT news.*, users.title as user_title
                         FROM news
                            INNER JOIN users ON users.id=news.user_id
                         WHERE users.status=1 AND news.status=1 AND news.id=?', array($id));

    if ($news)
    {
        $news['date'] = $date->GetRusDate($news['date'], 3);

        // Количество разделов архива
        $archive_list = $DB->GetAll('SELECT DATE_FORMAT(news.date,"%Y") AS news_date
                                 FROM news
                                    INNER JOIN users ON users.id=news.user_id
                                 WHERE users.status=1 AND news.status=1
                                 GROUP BY news_date
                                 ORDER BY news_date DESC');

        $smarty->assign('archive_list', $archive_list);
        $smarty->assign('news', $news);
        $smarty->display('news_view.tpl');
        $smarty->clearAllAssign();

        $container['page_title'] = $news['title'];
        $container['breadcrumbs'] = array('/news/' => 'Новости');
    }
    else throw new Exception('Error 404', 404);
}
else
{
    $h1_title = false;
    $all_news = true;
    $user_sql = '';

    // Новости предприятия
    if (isset($_GET['user']))
    {
        $user_id = (int)$_GET['user'];
        $user = $DB->GetRow('SELECT id, title FROM users WHERE id=? AND status=1', array($user_id));
        if ($user)
        {
        	$user_sql = 'AND news.user_id=' . $user_id;

        	$h1_title = $container['page_title'] = 'Новости поставщика ' . $user['title'];
        	$smarty->assign('user', $user);
        }
        else throw new Exception('Error 404', 404);
    }

    // Количество разделов архива
    $archive_list = $DB->GetAll('SELECT DATE_FORMAT(news.date,"%Y") AS news_date
                                 FROM news
                                    INNER JOIN users ON users.id=news.user_id
                                 WHERE users.status=1 AND news.status=1 ' . $user_sql . '
                                 GROUP BY news_date
                                 ORDER BY news_date DESC');

    // Архив новостей
    if (isset($_GET['archive']))
    {
        $err = true;
        $arch_year =  (int)$_GET['archive'];

        foreach ($archive_list as $k => $item)
        {
            if($item['news_date'] == $arch_year)
            {
                $user_sql .= ' AND DATE_FORMAT(news.date,"%Y") = ' . $arch_year;

                if(!empty($user['title']))
                {
                    $user_title = $user['title'].' ';
                }
                else $user_title = '';

                $h1_title = $container['page_title'] = 'Архив новостей ' . $user_title . 'за ' . $arch_year . ' год';


                $archive_list[$k]['selected'] = true;

                $all_news = $err = false;
                break;
            }
        }

        if($err) throw new Exception('Error 404', 404);

    }




    $all_items = $DB->GetOne('SELECT COUNT(*)
                              FROM news
                                 INNER JOIN users ON users.id=news.user_id
                              WHERE users.status=1 AND news.status=1 '.$user_sql);

    $pager = new Pager($all_items, $SETTINGS['news']['max_on_page'], 'static');

    $list = $DB->GetAll('SELECT news.id, news.user_id, news.date, news.title, news.img, users.title as user_title
                         FROM news
                            INNER JOIN users ON users.id=news.user_id
                         WHERE users.status=1 AND news.status=1 ' . $user_sql . '
                         ORDER BY news.date DESC
                         LIMIT ' . $pager->get_limit_start() . ', ' . $pager->get_limit_end());
    foreach ($list as $k => $value)
    {
        $list[$k]['date'] = $date->GetRusDate($value['date'], 3);
    }

    $container['prevnext'] = $pager->get_prev_next();

    $smarty->assign('pager', $pager->get_pages());
    $smarty->assign('news', $list);
    $smarty->assign('all_news', $all_news);
    $smarty->assign('archive_list', $archive_list);
    $smarty->assign('h1_title', $h1_title);
    $smarty->display('news_list.tpl');
    $smarty->clearAllAssign();
    unset($list);
}


?>