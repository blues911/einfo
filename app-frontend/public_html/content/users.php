<?php

// Путь к файлам пользователей
$user_upload_path = $_SERVER['DOCUMENT_ROOT'].'/uploads/users_docs/';


if (isset($_GET['id']))
{
    $id = (int)$_GET['id'];
    $user = $DB->GetRow("SELECT users.*, country.country, city.city, users.cnt
                         FROM users
                            INNER JOIN ip2ruscity_countries country ON country.country_id=users.country_id
                            INNER JOIN ip2ruscity_cities city ON city.city_id=users.city_id
                         WHERE users.id=? AND users.status=1 AND users.id NOT IN (?, ?)", array($id, USER_ADMIN_ID, USER_PARTNER_ID));

    if ($user)
    {
        if (!empty($user['register_date']) && $user['register_date'] != '0000-00-00 00:00:00')
        {
            $register_date = $date->GetRusDate($user['register_date'], 6);
            $user['register_date'] = mb_convert_case($register_date, MB_CASE_TITLE, 'UTF-8') . ' г.';
        }
        else
        {
            $user['register_date'] = '';
        }

        // визитка предприятия
        $user['price_date'] = $date->GetRusDate($user['price_date'], 3);

        $last_news = $DB->GetAll("SELECT id, title, date, img
                                 FROM news
                                 WHERE status=1 AND user_id=? AND user_id NOT IN (?, ?)
                                 ORDER BY date DESC
                                 LIMIT 0, ".$SETTINGS['users']['last_news'], array($id, USER_ADMIN_ID, USER_PARTNER_ID));
        foreach ($last_news as $k => $value)
        {
            $last_news[$k]['date'] = $date->GetRusDate($value['date'], 3);
        }

        $last_vacancy = $DB->GetAll("SELECT id, speciality
                                    FROM vacancy
                                    WHERE user_id=?
                                    ORDER BY date DESC
                                    LIMIT 0, ".$SETTINGS['users']['last_vacancy'], array($id));

        $user_docs_list = $DB->GetAll("SELECT title, file, fshow FROM users_docs WHERE fshow=1 AND user_id=? ORDER BY id ASC", array($id));
        $user_docs = array();

        // Информация о файле
        foreach ($user_docs_list as $k => $item)
        {
            if (file_exists($user_upload_path . $item['file']))
            {
                // Расширение
                $file_info = pathinfo($user_upload_path . $item['file']);
                $user_docs_list[$k]['extension'] = $file_info['extension'];

                // Размер
                $file_size = filesize($user_upload_path . $item['file']);
                $user_docs_list[$k]['file_size'] = filesize_human($file_size);
            }
            else
            {
                unset($user_docs_list[$k]);
            }
        }


        foreach ($user_docs_list as $k => $value)
        {
            if (in_array(strtolower($value['extension']), array('jpg', 'jpeg', 'gif', 'png', 'bmp')))
            {
                $user_docs['imgs'][] = $value;
            }
            else
            {
                $user_docs['files'][] = $value;
            }
        }

        $smarty->assign("user", $user);
        $smarty->assign("last_news", $last_news);
        $smarty->assign("last_vacancy", $last_vacancy);
        $smarty->assign("user_docs", $user_docs);
        $smarty->display("user_view.tpl");
        $smarty->clearAllAssign();

        $container['page_title'] = $user['title'];
        $container['breadcrumbs'] = array('/users/' => 'Поставщики');
    }
    else throw new Exception("Error 404", 404);
}
else
{    //список предприятий
    $all_items = $DB->GetOne("SELECT COUNT(*) FROM users WHERE status=1 AND id NOT IN (?, ?)", array(USER_ADMIN_ID, USER_PARTNER_ID));
    $pager = new Pager($all_items, $SETTINGS['users']['max_on_page'], "static");

    $list = $DB->CacheGetAll("SELECT
                                  users.id,
                                  users.title,
                                  users.about,
                                  CONCAT(
                                    users.phone1,
                                    IF(LENGTH(users.phone1) AND LENGTH(users.phone1_ext), CONCAT(' доб. ', users.phone1_ext), ''),
                                    IF(LENGTH(users.phone1) AND LENGTH(users.phone2), ', ', ''),
                                    users.phone2,
                                    IF(LENGTH(users.phone2) AND LENGTH(users.phone2_ext), CONCAT(' доб. ', users.phone2_ext), '')
                                  ) AS phone,
                                  users.phone1,
                                  users.phone1_ext,
                                  users.phone2,
                                  users.phone2_ext,
                                  users.price_date,
                                  IF (users.city_id != 0, ip2ruscity_cities.city, '&mdash;') AS city,
                                  users.cnt,
                                  users.allow_download_price
                                FROM users
                                  LEFT JOIN ip2ruscity_cities ON ip2ruscity_cities.city_id = users.city_id
                                WHERE users.status=1 AND users.id NOT IN (?, ?)
                                GROUP BY users.id
                                ORDER BY users.rating DESC
                                LIMIT ".$pager->get_limit_start().", ".$pager->get_limit_end()
    , array(USER_ADMIN_ID, USER_PARTNER_ID));

    foreach ($list as $k => $value)
    {
        $list[$k]['price_date'] = $date->GetRusDate($value['price_date'], 4);
    }

    $container['prevnext'] = $pager->get_prev_next();

    $smarty->assign('pager', $pager->get_pages());
    $smarty->assign('users', $list);
    $smarty->assign('all_users', $all_items);
    $smarty->display('users_list.tpl');
    $smarty->clearAllAssign();
    unset($list);
}

?>
