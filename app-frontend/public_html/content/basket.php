<?php

if (!empty($_COOKIE['user_info']))
{
    $user_info = unserialize($_COOKIE['user_info']);

    $smarty->assign('user_info', unserialize($_COOKIE['user_info']));
}


// Очистить корзину
if (isset($_GET['clear']))
{
    $DB->Execute('DELETE FROM basket_tmp WHERE basket_key = ?', array($SETTINGS['basket_key']));

    header('Location: /info/buyers/');
    exit;
}
else if (isset($_GET['success']) && !empty($user_info['success']))
{
    $smarty->assign('success', true);
    $smarty->assign('user_email', $user_info['contact_email']);
    $smarty->assign('mail_type', $user_info['mail_type']);

    unset($user_info['success']);

    setcookie('user_info', serialize($user_info), time() + 3600 * 24 * 180, '/');
}
else
{
    // Список товаров для корзины
    $components_search = sprintf("SELECT main.id,
                                        main.comp_title,
                                        main.manuf_title,
                                        main.descr,
                                        prepare_prices(main.prices, IF(users.price_with_nds, 1, %2\$s), %3\$s, %4\$s) AS prices,
                                        main.stock,
                                        main.delivery,
                                        users.title AS user_title,
                                        CONCAT(
                                            users.phone1,
                                            IF(LENGTH(users.phone1) AND LENGTH(users.phone1_ext), CONCAT(' доб. ', users.phone1_ext), ''),
                                            IF(LENGTH(users.phone1) AND LENGTH(users.phone2), ', ', ''),
                                            users.phone2,
                                            IF(LENGTH(users.phone2) AND LENGTH(users.phone2_ext), CONCAT(' доб. ', users.phone2_ext), '')
                                        ) AS user_phone,
                                        users.city_id AS user_city_id,
                                        city.city AS user_city,
                                        main.user_id,
                                        basket_tmp.cnt as basket_cnt
                                    FROM base_main main
                                        INNER JOIN users ON users.id=main.user_id
                                        INNER JOIN basket_tmp ON basket_tmp.main_id = main.id AND basket_tmp.confirm = 1
                                        LEFT JOIN ip2ruscity_cities city ON city.city_id = users.city_id
                                    WHERE users.status = 1 AND basket_key = '%1\$s'
                                    ORDER BY main.comp_title ASC",
                                        $SETTINGS['basket_key'],
                                        $SETTINGS['nds']['value'],
                                        $SETTINGS['cur_rates']['usd'],
                                        $SETTINGS['cur_rates']['eur']
    );

    $list = $DB_master->GetAll($components_search);

    foreach($list as $k => $item)
    {
        $users[$item['user_id']] = array('id' => $item['user_id'], 'title' => $item['user_title'], 'city' => $item['user_city'], 'city_id' => $item['user_city_id'], 'phone' => $item['user_phone']);
    }

    if (!empty($users))
    {
        $smarty->assign('users', $users);
    }

    if (empty($list))
    {
        header('Location: /info/buyers/');
        exit;
    }

    $smarty->assign('basket', $list);
}

$smarty->display("basket.tpl");
