<?php

/**
 * Проверяем каноничность строки
 * @param $comp_title
 * @param $DB
 * @return bool
 */
function check_canonical($comp_title, &$DB = null)
{
    $cnt = 0;

    if ($DB)
    {
        // Проверяем кастомную каноничность компонента
        $is_canonical = (bool)$DB->GetOne('SELECT 1 FROM store_canonical WHERE comp_title = ?', array($comp_title));

        // Если компонент каноничен, останавливаем проверку
        if ($is_canonical) return true;

        // Проверяем по таблице каноничных позиций
        $canon_cnt = (int)$DB->GetOne('SELECT offer_cnt FROM catalog_comp_canon WHERE comp_canon = ?', array($comp_title));

        // Если количество больше или равно минимальному считаем каноничным
        if ($canon_cnt >= CATALOG_MIN_SUPPLY) return true;

        $cnt = $DB->GetOne('SELECT COUNT(*) FROM base_main WHERE comp_title = ?', array($comp_title));
    }

    return (preg_match('/^[-a-zа-я0-9\/\.]{4,20}$/sui', $comp_title)
    	&& preg_match('/^[a-zа-я0-9]/sui', $comp_title)
    	&& preg_match('/[a-zа-я0-9]$/sui', $comp_title)
        && preg_match('/[a-zа-я]+/sui', $comp_title)
        && preg_match('/[0-9]+/sui', $comp_title)
        && !preg_match('/\-nd$/sui', $comp_title)
        && !preg_match('/nopb$/sui', $comp_title)
        && !preg_match('/pbf$/sui', $comp_title)
        && !preg_match('/0402/sui', $comp_title)	#\
        && !preg_match('/0603/sui', $comp_title)	# |
        && !preg_match('/0805/sui', $comp_title)	# |
        && !preg_match('/1206/sui', $comp_title)	# | чип резисторы
        && !preg_match('/1210/sui', $comp_title)	# |
        && !preg_match('/1812/sui', $comp_title)	# |
        && !preg_match('/2220/sui', $comp_title)	# |
        && !preg_match('/2225/sui', $comp_title)	#/
        && !preg_match('/[0-9]{6,}/sui', $comp_title) # длинные цифровые партномера
        && !preg_match('/^(rs|rd|rid|rt|rq|nes|ned|net|lrs|se|sp|psp|tp|qp|hsp|hsn|nel|erp|hdp|spv|usp|rsp|rst|hrp|hrpg|msp|hep|gs|EPS|EPP|PS|PPS|ASP|LPS|LPP|ELP|PD|RPD|RPT|PPT|PQ|PPQ|PM|NFM|MPS|RPS|MPD|RPT|MPQ|PB|RPB|RCB|ENP|ENC|ESC|ESP|GC|PA|PB|RCP|RKP|EPP|EPS|IRM|SGA|HSP|UHP|OFM|SCP)-/sui', $comp_title)
        && !preg_match('/^(dr-[0-9]|dr[aht]-|drp-[24]|[emnstwh]dr)/sui', $comp_title)
        && !preg_match('/^(AD-|ADD|DRC|PSC|DR-UPS)/sui', $comp_title)
        && !preg_match('/^(HLG-[0-9]{1,3}H-C|ELG-[0-9]{1,3}-C|APC|FDL|HVGC|LPC|LPHC|PCD|PLD|LCM)|(APV|LPV|LPH-)|(CEN|CLG|ELG-[0-9]{1,3}-[^C]|HLG-[0-9]{1,3}H-[^C]|ELN|HBG|HLN|HLP|HVG-|LPF|NPF|PLN|GSC|HBG|LPLC|LPHC|PLM|IDLC|ODLC|PLP|HLP|LCM|PWM|IDPC|LDC)/sui', $comp_title)
        && !preg_match('/^(srs|sus|spr|smu|sma|sbt|sft|spu|spa|spb|scw|slw|ske|ska|skm|sdm|det|dcw|dlw|dke|dka|tka|mhb|nsd|nid|psd|sd-|rsd|LDB|LDH)/sui', $comp_title)
        && !preg_match('/^ddr-/sui', $comp_title)
        && !preg_match('/^(ADS-|MPD-|PD-|PID-|RD-|RID-|NED-|RPD-|NET-|PT-|MPT-|RPT-|RPTG-|RT-|TP-|RQ-|MPQ-|PPQ-|PQ-|QP-)/sui', $comp_title)
        && !preg_match('/^(a301|a302|ts|tn|irc|isi)/sui', $comp_title)
        && $cnt >= 2);
}
