<?php

if (!isset($page_id)) $page_id = md5($_SERVER["REQUEST_URI"]);

$page_hash = md5($container['content']); // Вычисляем хэш "нужной" части страницы
#$time_expires = time()+60*10;
$time_expires = 0;

// Достаем из базы данных информацию о текущей странице.
$DB->SetFetchMode(ADODB_FETCH_NUM);
$FILE_INFO = $DB->GetRow("SELECT cm_id, cm_md5, UNIX_TIMESTAMP(cm_modified) as modified FROM content_monitoring WHERE cm_id = ?", array($page_id));
$DB->SetFetchMode(ADODB_FETCH_ASSOC);

if (empty($FILE_INFO))
{
    // Запись для файла не найдена.
    $DB_master->Execute("INSERT INTO content_monitoring VALUES (?, ?, NULL)",array($page_id, $page_hash)); // Создаем запись о текущем файле.
    $FILE_INFO[2] = time(); // Модификация - сейчас.
    $last_modified = gmdate("D, d M Y H:i:s", $FILE_INFO[2]);
}
else
{
    // Запись для файла найдена.
    if ($page_hash != $FILE_INFO[1])
    {
        $DB_master->Execute("UPDATE content_monitoring SET cm_md5=? WHERE cm_id=?", array($page_hash, $page_id)); // Обновляем запись о текущем файле, если он изменился.
        $FILE_INFO[2] = time(); // Модификация - сейчас.
    }

    $last_modified = gmdate("D, d M Y H:i:s", $FILE_INFO[2]);

    // Дальше делаем обработку Conditional GET'а:
    if (!isset($_SERVER['HTTP_IF_NONE_MATCH']) && !isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    {
        // Conditional Get не задан - просто отдаем файл.
        header('ETag: "'.$page_hash.'" '); // присваеваем метку
        header("Last-Modified: $last_modified GMT"); // последнее изменение - сейчас
        if($time_expires) header('Expires: '.gmdate("D, d M Y H:i:s", $time_expires).' GMT'); // страница остается неизменной 10 минут
    }

    elseif (!isset($_SERVER['HTTP_IF_NONE_MATCH']) && isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    {
        // Случай первый - Conditional GET задан, проверка только по If-Modified-Since:
        $unix_ims = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']); // значение If-Modified-Since в UNIX формате
        if ($unix_ims > time() || !is_int($unix_ims))
        {
            // Ошибка Conditional GET - просто отдаем файл.
            header('ETag: "'.$page_hash.'" '); // присваеваем метку
            header("Last-Modified: $last_modified GMT"); // последнее изменение - сейчас
            if($time_expires) header('Expires: '.gmdate("D, d M Y H:i:s", $time_expires).' GMT'); // страница остается неизменной 10 минут
        }
        else
        {
            // Conditional GET корректен.
            if ($unix_ims >= $FILE_INFO[2])
            {
                // Копия файла в кеше клиента не устарела - сообщаем ему об этом...
                header("HTTP/1.1 304 Not Modified"); // не модифицировано
                header('ETag: "'.$page_hash.'" '); // присваеваем метку
                // ...и заканчиваем выполнение скрипта, не отсылая сам файл.
                while(ob_get_level()) ob_end_clean();
                exit;
            }
            else
            {
                // Похоже, что копия клиента устарела.
                header('ETag: "'.$page_hash.'" '); // присваеваем метку
                header("Last-Modified: $last_modified GMT"); // последнее изменение - сейчас
                if($time_expires) header('Expires: '.gmdate("D, d M Y H:i:s", $time_expires).' GMT'); // страница остается неизменной 10 минут
            }
        }
    }

    elseif (isset($_SERVER['HTTP_IF_NONE_MATCH']) && !isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    {
        // Случай второй - Conditional GET задан, проверка только по If-None-Match:
        $INM = preg_split('/[,][ ]?/', $_SERVER['HTTP_IF_NONE_MATCH']); // массив значений If-None-Match
        foreach($INM as $enity)
        {
            if ($enity == "\"$page_hash\"")
            {
                // Копия файла в кеше клиента не устарела - сообщаем ему об этом...
                header("HTTP/1.1 304 Not Modified"); // не модифицировано
                header('ETag: "'.$page_hash.'" '); // присваеваем метку
                // ...и заканчиваем выполнение скрипта, не отсылая сам файл.
                while(ob_get_level()) ob_end_clean();
                exit;
            }
            // Если дошло до этой линии, копия клиента устарела. Отдаем файл.
            header('ETag: "'.$page_hash.'" '); // присваеваем метку
            header("Last-Modified: $last_modified GMT"); // последнее изменение - сейчас
            if($time_expires) header('Expires: '.gmdate("D, d M Y H:i:s", $time_expires).' GMT'); // страница остается неизменной 10 минут
        }
    }

    else
    {
        // Случай третий - проверка и по If-Modified-Since, и по If-None-Match:
        $unix_ims = strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']); // значение If-Modified-Since в UNIX формате
        $INM = preg_split('/[,][ ]?/', $_SERVER['HTTP_IF_NONE_MATCH']); // массив значений If-None-Match
        if ($unix_ims > time() || !is_int($unix_ims))
        {
            // Ошибка Conditional Get - просто отдаем файл.
            header('ETag: "'.$page_hash.'" '); // присваеваем метку
            header("Last-Modified: $last_modified GMT"); // последнее изменение - сейчас
            if($time_expires) header('Expires: '.gmdate("D, d M Y H:i:s", $time_expires).' GMT'); // страница остается неизменной 10 минут
        }
        else
        {
            // Conditional GET корректен.
            foreach($INM as $enity)
            {
                if ($enity == "\"$page_hash\"" && $unix_ims >= $FILE_INFO[2])
                {
                    // Копия файла в кеше клиента не устарела - сообщаем ему об этом...
                    header("HTTP/1.1 304 Not Modified"); // не модифицировано
                    header('ETag: "'.$page_hash.'" '); // присваеваем метку
                    // ...и заканчиваем выполнение скрипта, не отсылая сам файл.
                    while(ob_get_level()) ob_end_clean();
                    exit;
                }
                // Если дошло до этой линии, копия клиента устарела. Отдаем файл.
                header('ETag: "'.$page_hash.'" '); // присваеваем метку
                header("Last-Modified: $last_modified GMT"); // последнее изменение - сейчас
                if($time_expires) header('Expires: '.gmdate("D, d M Y H:i:s", $time_expires).' GMT'); // страница остается неизменной 10 минут
            }
        }
    }
}

?>
