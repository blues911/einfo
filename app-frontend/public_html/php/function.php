<?php

function printr($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die();
}

// Разбиение списка на колонки
function sort_by_columns($list, $columns_max = 4, $rows_min = 4)
{
    $sorted_list = array();
    $column_size = count($list) / $rows_min;

    if ($column_size <= $columns_max)
    {
        $column_size = $rows_min;
    }
    else
    {
        $column_size = ceil(count($list) / $columns_max);
    }

    $num = 0;
    $column = 0;
    foreach ($list as $item)
    {
        if ($num % $column_size == 0 && $column < $columns_max) $column++;
        $sorted_list[$column][] = $item;
        $num++;
    }

    return $sorted_list;
}

// микросекунды
function timer()
{
     $a = explode(" ",microtime());
     return (float)$a[0] + (float)$a[1];
}

// Корректный размер файла (bytes, Kb, Mb, Gb)
function file_size($size)
{
    if ($size < 1024)
    {
        $new_size = $size . ' байт';
    }
    else if ($size <= 1048576)
    {
        $new_size = round($size / 1024, 0) . ' Кб';
    }
    else if ($size <= 1073741824)
    {
        $new_size = round($size / 1048576, 2) . ' Мб';
    }
    else
    {
        $new_size = round($size / 1073741824, 3) . ' Гб';
    }

    return $new_size;
}


// переименование существующего файла
function rename_existfile ($path, $prefix = null)
{
    if (file_exists($path))
    {
        $pinfo = pathinfo($path);
        $pinfo['basename'] = preg_replace("/^".$prefix."_/", "", $pinfo['basename']);
        $prefix = ($prefix) ? ++$prefix : 1;
        $new_path = $pinfo['dirname']."/".$prefix."_".$pinfo['basename'];

        return rename_existfile($new_path, $prefix);
    }
    else
    {
        return basename($path);
    }
}


// Image Magick
function image_magick($cmd)
{
    if (!preg_match("/;/", $cmd))
    {
        exec(IMAGE_MAGICK.$cmd);
        return 1;
    }
    else return 0;
}


// unicode escape
function unicode_escape($str)
{
	return preg_replace('/%u([0-9A-F]{4})/se', 'iconv("UTF-16BE", "Windows-1251", pack("H4", "$1"))', $str);
}


// convert utf8
function utf_convert($str)
{
	return iconv("utf-8", "windows-1251", $str);
}


// prepare string
function pstr($str, $mode = "text")
{
    $str = trim($str);
    switch ($mode)
    {
        case "text":
        	$str = htmlspecialchars($str, ENT_QUOTES);
        	break;

    }

    return $str;
}


// xml2array
function simplexml2array($xml, $attribsAsElements = 0)
{
    if (is_object($xml))
    {
   if (get_class($xml) == 'SimpleXMLElement')
   {
       $attributes = $xml->attributes();

            foreach ($attributes as $k => $v)
       {
                if ($v) $a[$k] = $v;
       }

       $x = $xml;
       $xml = get_object_vars($xml);
   }
    }

   if (is_array($xml))
   {
       if (count($xml) == 0) return (string) $x; // for CDATA

        foreach ($xml as $key => $value)
       {
           $r[$key] = simplexml2array($value, $attribsAsElements);
       }

       if (isset($a))
       {
            if ($attribsAsElements)
           {
               $r = array_merge($a, $r);
           }
           else
           {
               $r['@'] = $a; // Attributes
           }
       }

       return $r;
   }
   return (string) $xml;
}



// ----------------------------------
function stripslashes_for_array(&$arr)
{
   foreach($arr as $k=>$v)
   {
       if (is_array($v))
       {
           stripslashes_for_array($v);
           $arr[$k] = $v;
       }
       else
       {
           $arr[$k] = stripslashes($v);
       }
   }
}


function fix_magic_quotes_gpc()
{
   if (get_magic_quotes_gpc())
   {
       stripslashes_for_array($_POST);
       stripslashes_for_array($_GET);
       stripslashes_for_array($_COOKIE);
   }
}

// деактивирует при необходииости директиву "magic_quotes_gpc"
fix_magic_quotes_gpc();
// ----------------------------------


// Разбиение строки на атомы для поиска
function get_request_atoms($str)
{
    $atoms = array();
    if (preg_match_all('/([A-Z]+|[А-ЯЁ]+|[0-9]+)/uis', $str, $matches))
    {
        $atoms = $matches[0];
    }

    return $atoms;
}

// Разбиение строки на атомы для поиска тестовый алгоритм
function get_request_atoms_2($str)
{
    $atoms = array();
    $str = mb_ereg_replace('\W', '', $str);
    $steps = mb_strlen($str) - 2;

    for ($_s = 0; $_s < $steps; $_s++)
    {
        $atoms[] = mb_substr($str, $_s, 3);
    }

    return $atoms;
}

// encode для mod_rewrite
function my_urlencode($url)
{
    $url = urlencode($url);
    $url = str_replace("%2F", "%7Cr", $url);
    $url = str_replace("%5C", "%7Cl", $url);

    return  $url;
}


// decode для mod_rewrite
function my_urldecode($url)
{
    $url = str_replace("%7Cr", "%2F", $url);
    $url = str_replace("%7Cl", "%5C", $url);
    $url = urldecode($url);

    return  $url;
}


// получение реально IP-адреса
function get_ip()
{
/*    if (!empty($_SERVER['HTTP_CLIENT_IP']))  // check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) // to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
*/
#	return $ip;
#	return '91.201.55.2'; # ebugr
#	return '79.142.86.161'; #spb

	return !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
}


// получение referer query из поисковиков
function se_referer_query()
{
    $se_query = false;
    if (!empty($_SERVER['HTTP_REFERER']))
    {
        $ref_url = parse_url($_SERVER['HTTP_REFERER']);

        if (isset($ref_url['host'], $ref_url['query']))
        {
            parse_str($ref_url['query'], $output);
            if (preg_match ("/yandex/i",$ref_url['host']) && !empty($output['text']))
            {
                $se_query = $output['text'];
            }
            elseif (preg_match ("/google/i",$ref_url['host'] ) && !empty($output['q']))
            {
                $se_query = $output['q'];
            }
            elseif (preg_match ("/rambler/i",$ref_url['host'] ) && (!empty($output['query']) || !empty($output['words'])) )
            {
                if (!empty($output['query'])) $se_query = $output['query'];
                else $se_query = $output['words'];
            }
            elseif (preg_match ("/mail/i",$ref_url['host'] ) && !empty($output['q']))
            {
                $se_query = $output['q'];
            }
        }
    }

    return $se_query;
}


// Проверка статуса SphinxSearch
function sphinx_status()
{
    include_once(PATH_FRONTEND . '/php/classes/class.Search.php');
    include_once(PATH_FRONTEND . '/php/classes/class.SearchSphinxQL.php');

    $sphinx = new SearchSphinxQL();

    if ($sphinx->sphinx_status() == false)
    {
        header("HTTP/1.1 503 Service Unavailable");
        include("content/error_db_load.html");
        exit();
    }
}

// подключение к БД (с балансировкой)
function database_connect($DB_CONNECT)
{
    if (isset($DB_CONNECT['master']))
    {
        $DB_master = ADONewConnection($DB_CONNECT['master']);
        if (!$DB_master)
        {
			header("HTTP/1.1 503 Service Unavailable");
            include("content/error_db_load.html");
            exit();
        }

        $DB_master->SetFetchMode(ADODB_FETCH_ASSOC);
        $DB_master->Execute("SET NAMES utf8");
        $DB_master->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
        $DB_master->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
        $DB_master->cacheSecs = DB_CACHE_TIME;

        // выборка slave-серверов
        $serv_arr = array();
        foreach ($DB_CONNECT as $k => $value)
        {
            if ($k != "master")
            {
                $serv_arr[] = $k;
            }
        }

        if ($serv_arr)
        {
            $DB = ADONewConnection($DB_CONNECT[$serv_arr[array_rand($serv_arr)]]);
            if (!$DB)
            {
				header("HTTP/1.1 503 Service Unavailable");
                include("content/error_db_load.html");
                exit();
            }

            $DB->SetFetchMode(ADODB_FETCH_ASSOC);
            $DB->Execute("SET NAMES utf8");
            $DB->Execute('SET TIME_ZONE = "' . DEFAULT_TIMEZONE . '"');
            $DB->Execute("SET SESSION query_cache_type = ?", array(MYSQL_CACHE_TYPE));
            $DB->cacheSecs = DB_CACHE_TIME;

			$slavestat = $DB->GetRow("SHOW SLAVE STATUS");
			if($slavestat['Seconds_Behind_Master'] > 0 && $slavestat['Seconds_Behind_Master'] != null)
			{
				$DB = &$DB_master;
			}
        }
        else
        {
            $DB = &$DB_master;
        }

        return array($DB_master, $DB);
    }
    else
    {
        exit("Отсутствует master-сервер БД");
    }
}

function is_se_bot()
{
	if (!empty($_SERVER['HTTP_USER_AGENT']))
	{
		if (preg_match ("/yandexbot/i",$_SERVER['HTTP_USER_AGENT'])) return 1;
		if (preg_match ("/googlebot/i",$_SERVER['HTTP_USER_AGENT'])) return 1;
	}
	return 0;
}

// Склонение числовых значений.
function plural($num, $one, $three, $five)
{
    $n = (int)$num;
    $forms = array($one, $three, $five);
    if (count($forms) == 3)
    {
        $plural = (($n % 10 == 1 && $n % 100 != 11) ? 0 : (($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)) ? 1 : 2));
        return $forms[$plural];
    }
    else
    {
        return null;
    }
}

/**
 * Аналог js функции encodeURIComponent.
 * @param $str
 * @return string
 */
function encodeURIComponent($str) {
    $revert = array('%21' => '!', '%2A' => '*', '%27' => "'", '%28' => '(', '%29' => ')');
    return strtr(rawurlencode($str), $revert);
}


/**
 * Аналог js функции decodeURIComponent.
 * @param $str
 * @return string
 */
function decodeURIComponent($str) {
    $revert = array('%21' => '!', '%2A' => '*', '%27' => "'", '%28' => '(', '%29' => ')');
    return rawurldecode(strtr($str, $revert));
}

/**
 * Приводим цену к нужному формату
 * @param $price
 * @return float
 */
function prepare_price($price)
{
    return round((float)str_replace(',', '.', trim($price)), 2);
}

/**
 * Приводим цену к нужному формату с удалением .00 на конце
 * @param $num
 * @return float
 */
function format_price($num)
{
    $price = number_format($num, 2, '.', ' ');
    $price = preg_replace('/\.00$/', '', $price);

    return $price;
}

/**
 * Подключаем функции общие для frontend и backend
 */
include_once(PATH_FRONTEND . '/php/ext_function.php');