<?php

/**
 * Бот Telegram.
 */
class Telegram
{
    /**
     * Возвращает результаты поиска.
     * @param $term
     * @param int $limit
     * @return array
     */
    public function search($term, $limit = 10)
    {
        global $SETTINGS;

        $result = array(
            'cnt' => 0,
            'list' => array()
        );

        $special_search = (bool)preg_match('/[\*\?]/', $term);

        $search = new SearchSphinxQL();
        $search->set($term, false, false, $special_search);

        $list = $search->search();
        $search->search_log(count($list), 'telegram');

        $term_atoms = get_request_atoms(mb_strtoupper($term, 'utf-8'));
        
        // Сортировка выдачи
        usort($list, function($a, $b) use ($term_atoms, $SETTINGS)
        {
            // Подготавливаем различные значения для сортировки.
            $atoms_a = get_request_atoms(mb_strtoupper($a['comp_title']));
            $atoms_b = get_request_atoms(mb_strtoupper($b['comp_title']));

            $atoms_cnt_a = count($atoms_a);
            $atoms_cnt_b = count($atoms_b);

            $same_a = implode(' ', $atoms_a) === implode(' ', $term_atoms);
            $same_b = implode(' ', $atoms_b) === implode(' ', $term_atoms);

            $atoms_intersect_a = array_intersect_assoc($term_atoms, $atoms_a);
            $atoms_intersect_b = array_intersect_assoc($term_atoms, $atoms_b);

            $atoms_intersect_cnt_a = count($atoms_intersect_a);
            $atoms_intersect_cnt_b = count($atoms_intersect_b);

            $atoms_intersect_sum_a = array_sum(array_keys($atoms_intersect_a));
            $atoms_intersect_sum_b = array_sum(array_keys($atoms_intersect_b));

            $atoms_same_intersect_a = array_intersect($term_atoms, $atoms_a);
            $atoms_same_intersect_b = array_intersect($term_atoms, $atoms_b);

            $atoms_same_intersect_cnt_a = count($atoms_same_intersect_a);
            $atoms_same_intersect_cnt_b = count($atoms_same_intersect_b);

            $atoms_same_intersect_sum_a = 0;
            $_atoms_keys_a = array_flip($atoms_a);
            foreach ($atoms_same_intersect_a as $_intersect)
            {
                if (array_key_exists($_intersect, $_atoms_keys_a))
                {
                    $atoms_same_intersect_sum_a += (int)$_atoms_keys_a[$_intersect];
                }
            }

            $atoms_same_intersect_sum_b = 0;
            $_atoms_keys_b = array_flip($atoms_b);
            foreach ($atoms_same_intersect_b as $_intersect)
            {
                if (array_key_exists($_intersect, $_atoms_keys_b))
                {
                    $atoms_same_intersect_sum_b += (int)$_atoms_keys_b[$_intersect];
                }
            }

            $cnt_a = (int)$a['stock'];
            $cnt_b = (int)$b['stock'];

            $all_prices_from_a = json_decode($a['prices'], true);
            $all_prices_from_b = json_decode($b['prices'], true);

            $first_price_from_a = array_shift($all_prices_from_a);
            $first_price_from_b = array_shift($all_prices_from_b);

            $price_cur_a = !empty($SETTINGS['cur_rates'][$first_price_from_a['c']]) ? $SETTINGS['cur_rates'][$first_price_from_a['c']] : 1;
            $price_cur_b = !empty($SETTINGS['cur_rates'][$first_price_from_b['c']]) ? $SETTINGS['cur_rates'][$first_price_from_b['c']] : 1;

            $price_a = (float)$first_price_from_a['p'] * $price_cur_a;
            $price_b = (float)$first_price_from_b['p'] * $price_cur_b;


            // Проверка на точное вхождение по атомам.
            if ($same_a != $same_b)
            {
                if ($same_a)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }

            // Проверка: сколько атомов находятся в нужных местах.
            if ($atoms_intersect_cnt_a != $atoms_intersect_cnt_b)
            {
                return $atoms_intersect_cnt_b - $atoms_intersect_cnt_a;
            }

            // Проверка: сколько атомов встречается (не зависимо от положения).
            if ($atoms_same_intersect_cnt_a != $atoms_same_intersect_cnt_b)
            {
                return $atoms_same_intersect_cnt_b - $atoms_same_intersect_cnt_a;
            }

            // Проверка: расстояние между совпавшими атомами и их положение ближе к левому краю.
            if ($atoms_same_intersect_sum_a != $atoms_same_intersect_sum_b)
            {
                return $atoms_same_intersect_sum_a - $atoms_same_intersect_sum_b;
            }

            // Проверка: у кого хвосты с лишними атомами короче.
            if ($atoms_cnt_a != $atoms_cnt_b)
            {
                return $atoms_cnt_a - $atoms_cnt_b;
            }

            // Проверка: расстояние между совпавшими (с учетом положения) атомами и их положение ближе к левому краю.
            if ($atoms_intersect_sum_a != $atoms_intersect_sum_b)
            {
                return $atoms_intersect_sum_a - $atoms_intersect_sum_b;
            }

            // Проверка: в наличии или нет.
            if (($cnt_a > 0) != ($cnt_b > 0))
            {
                if ($cnt_a > 0)
                {
                    return -1;
                }

                if ($cnt_b > 0)
                {
                    return 1;
                }
            }

            // Проверка: у кого ненулевая цена ниже.
            if (!empty($price_a) || !empty($price_b))
            {
                if (empty($price_a))
                {
                    return 1;
                }

                if (empty($price_b))
                {
                    return -1;
                }

                if ($price_a < $price_b)
                {
                    return -1;
                }

                if ($price_a > $price_b)
                {
                    return 1;
                }
            }

            return 0;
        });

        $telegram_result = array();

        // Схлопываем до одного предложения на поставщика.
        foreach (array_reverse($list) as $row)
        {
            $telegram_result[$row['user_id']] = $row;
        }

        // Сортируем по рейтингу поставщика
        usort($telegram_result, function($a, $b)
        {
            $user_rating_a = (float)$a['user_rating'];
            $user_rating_b = (float)$b['user_rating'];

            if ($user_rating_a > $user_rating_b)
            {
                return -1;
            }
            elseif ($user_rating_a < $user_rating_b)
            {
                return 1;
            }

            return 0;
        });

        $result['cnt'] = count($list);
        $result['list'] = array_slice($telegram_result, 0, $limit);

        return $result;
    }

    /**
     * Формирует сообщение из результатов поиска.
     * @param array $list Результат поиска.
     * @param string $term Поисковый запрос.
     * @param int $cnt Ещё доступно компонентов.
     * @return string
     */
    function result_format($list, $term, $cnt = 0)
    {
        $currencies = array(
            'rur' => 'руб.',
            'usd' => '$',
            'eur' => '€'
        );

        // Переворачиваем выдачу, чтобы самый релевантный ответ был снизу.
        $list = array_reverse($list);

        $str = array();

        foreach($list as $row)
        {
            $_str = '<strong>' . $row['comp_title'] . '</strong>';

            $_stock = array();
            if ($row['stock'] == -1)
            {
                $_stock[0] = 'на складе';
            }
            elseif ($row['stock'] > 0)
            {
                $_stock[0] = 'в наличии ' . $row['stock'] . ' шт.';
            }

            if ($row['stock_remote'] > 0)
            {
                $_stock[1] = 'под заказ ' . $row['stock_remote'] . ' шт.' . ($row['delivery'] != 0 ? ' / ' : '');
            }
            else
            {
                $_stock[1] = '';
            }

            if ($row['delivery'] < 1)
            {
                $_stock[1] .= 'на заказ';
            }
            elseif ($row['delivery'] > 0)
            {
                $_stock[1] .= $row['delivery'] . ' ' . plural($row['delivery'], 'неделя', 'недели', 'недель');
            }

            $_str .= '<i> — ' . implode(', ', $_stock) . '</i>' . "\n";

            $_price = array();

            $prices = json_decode($row['prices'], true);

            if (!empty($prices))
            {
                $prices_total = count($prices);
                $i = 1;

                foreach ($prices as $price)
                {
                    $currency = 'rur';
                    if (strtolower($price['c']) == 'usd') $currency = 'usd';
                    if (strtolower($price['c']) == 'eur') $currency = 'eur';

                    if ($prices_total == 1 && $price['p'] > 0)
                    {
                        $_price[] = format_price($price['p']) . ' ' . $currencies[$currency];
                    }
                    else if ($prices_total > 1 && $price['p'] > 0)
                    {
                        $qty = ($i == $prices_total) ? $price['n'] . '+ шт: ' : $price['n'] . ' шт: ';
                        $pc = format_price($price['p']) . ' ' . $currencies[$currency];
        
                        $_price[] = $qty . $pc;
                        $i++;
                    }
                }
            }

            if (!empty($_price))
            {
                $_str .= implode("\n", $_price) . "\n";
            }

            $_str .= $row['user_title'] . "\n";

            $_phone_href = preg_replace('/[^0-9\+]/uis', '', $row['user_phone1']);
            $_phone_href = preg_replace('/\+/uis', ' ', $_phone_href);

            $_str .= '<a href="https://' . HOST_FRONTEND . '/call/' . urlencode($_phone_href) . '/">' . $row['user_phone1'] . '</a>';

            if (!empty($row['user_phone1_ext']))
            {
                $_str .= ' доб. ' . $row['user_phone1_ext'];
            }

            $_str .= "\n";

            $str[] = $_str;
        }

        $result = implode("\n", $str);

        if (!empty($cnt) && $cnt > 0)
        {
            $result .= "\n"
                . '<a href="https://' . HOST_FRONTEND . '/' . urlencode($term) . '/">'
                . 'Еще ' . $cnt . ' ' . plural($cnt, 'позиция', 'позиции', 'позиций')
                . '</a>';
        }

        return $result;
    }
}