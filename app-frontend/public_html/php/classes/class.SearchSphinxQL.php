<?php

/**
 * Class SearchSphinxQL
 *
 * Поиск реализованный на SphinxSearch
 * посредством SphinxQL.
 */
class SearchSphinxQL extends Search
{
    /** @var null|PDO $sphinx_pdo */
    protected $sphinx_pdo = null;
    protected $sphinx_limit;
    protected $ranker = '10000*SUM(exact_hit) + (10000 - SUM(100*min_hit_pos)) + (100 - SUM(10*min_gaps))';

    public function __construct()
    {
        parent::__construct();

        try
        {
            $this->sphinx_pdo = new PDO(
                'mysql:host=' . SPHINXQL_HOST . ';port=' . SPHINXQL_PORT . ';charset=utf8',
                '',
                '',
                array(
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false
                )
            );
        }
        catch (PDOException $e)
        {
            $this->sphinx_pdo = null;
        }

        $this->sphinx_limit = (int)$this->SETTINGS['search']['sphinx_limit'];
    }

    /**
     * Возвращает ID из результатов поиска.
     *
     * @param void.
     * @return null|array
     */
    public function get_result_exec()
    {
        return $this->result_exec;
    }

    /**
     * Возвращает фильтр для SQL запроса
     * согласно пользовательским настройкам.
     *
     * @param void.
     * @return null|array
     */
    public function get_sql_filter()
    {
        return $this->sql_filter();
    }

    /**
     * Установить поисковый запрос текущего поиска.
     *
     * @param string $query Поисковый запрос.
     * @param bool $strong Признак строгого поиска.
     * @param bool $multiple
     * @param bool $special
     * @return bool|void
     */
    public function set($query, $strong = false, $multiple = false, $special = false)
    {
        $this->result_exec = null;
        $this->result = null;

        $this->query['q'] = $query;
        $this->query['strong'] = $strong;
        $this->query['multiple'] = $multiple;
        $this->query['special'] = $special;

        if ($strong)
        {
            $this->query['qlike'] = $this->query['q'];
            $this->query['qlike_flag'] = true;
        }
        else
        {
            if ($this->query['multiple'] && !$this->query['special'])
            {
                $this->query['q_phrase'] = array();
                $this->query['q_phrase']['no_quantity_restriction'] = '';

                foreach ($this->query['q'] as $key => $value)
                {
                    if (!empty($value['quantity']))
                    {
                        $this->query['q_phrase']['quantity_restriction'][$key]['phrase'] = implode(' << ', get_request_atoms_2($value['query']));
                        $this->query['q_phrase']['quantity_restriction'][$key]['qt'] = $value['quantity'];
                    }
                    else
                    {
                        $this->query['q_phrase']['no_quantity_restriction'] .= '(' . implode(' << ', get_request_atoms_2($value['query'])) . ')|';
                    }
                }

                if(!empty($this->query['q_phrase']['no_quantity_restriction']))
                {
                    $this->query['q_phrase']['no_quantity_restriction'] = rtrim($this->query['q_phrase']['no_quantity_restriction'], '|');
                }
            }
            elseif ($this->query['multiple'] && $this->query['special'])
            {
                $this->query['qlike'] = array();
                $this->query['q_phrase'] = array();
                $this->query['q_phrase']['no_quantity_restriction'] = '';

                foreach ($this->query['q'] as $value)
                {
                    $this->query['qlike'][] = rtrim(str_replace(array('*', '?'), array('%', '_'), $value['query']), '%') . '%';

                    $this->query['q_phrase']['no_quantity_restriction'] .= rtrim(str_replace(array('*', '?'), array('%', '_'), $value['query']), '%') . '%|';
                }

                if (!empty($this->query['q_phrase']['no_quantity_restriction']))
                {
                    $this->query['q_phrase']['no_quantity_restriction'] = rtrim($this->query['q_phrase']['no_quantity_restriction'], '|');
                }
            }
            elseif ($this->query['special'])
            {
                $this->query['qlike'] = '';
                $this->query['q_phrase'] = '';

                $this->query['qlike'] = rtrim(str_replace(array('*', '?'), array('%', '_'), $this->query['q']), '%') . '%';

                if (preg_match('/^[*]/', $this->query['q']))
                {
                    $q = preg_replace('/(\?|\*)([a-zA-Z]+|[0-9]+)|([a-zA-Z]+|[0-9]+)(\?|\*)([a-zA-Z]+|[0-9]+)|([a-zA-Z]+|[0-9]+)(\?|\*)/', ' ', $this->query['q']);
                    $q = preg_replace('/[^a-zA-Z0-9]/', ' ', $q);
                    $q = trim($q);

                    if ($q != '')
                    {
                        $rows = explode(' ', $q);
                        $q_str_len = 0;
                        $q_str = '';

                        foreach ($rows as $row)
                        {
                            if (strlen($row) > 3 && strlen($row) > $q_str_len)
                            {
                                $q_str_len = strlen($row);
                                $q_str = $row;
                            }
                        }

                        if ($q_str != '')
                        {
                            $this->query['q_phrase'] = '("' . implode(' << ', get_request_atoms_2($q_str)) . '")';
                        }
                    }

                }
            }
            else
            {
                $this->query['qlike'] = '%' . implode(' ', get_request_atoms_2($this->query['q'])) . '%';
                $this->query['q_phrase'] =  '("' . implode(' << ', get_request_atoms_2($this->query['q'])) . '")';
            }
        }

        return true;
    }

    /**
     * Выполнить поиск и получить id и релевантность (вес)
     * найденных компонентов. Формат ответа:
     * [{id: <id>, weight: <релевантность>}, ...].
     *
     * @param int $limit Лимит поисковой выдачи.
     * @return bool|void
     */
    public function search_exec($limit = 2000)
    {
        if (!empty($this->query['strong']))
        {
            $result = $this->DB->CacheGetCol(
                'SELECT base_main.id FROM base_main WHERE base_main.comp_title = ?',
                array($this->query['q'])
            );

            if ($result)
            {
                foreach ($result as $_id)
                {
                    $this->result_exec[$_id] = array(
                        'id' => $_id,
                        'weight' => 0
                    );
                }

                return true;
            }
            else
            {
                $this->result_exec = array();
                return false;
            }
        }
        elseif (!empty($this->query['special']))
        {
            if ($this->query['multiple'])
            {
                foreach ($this->query['qlike'] as $_key => $_value)
                {
                    $this->query['qlike'][$_key] = $this->DB->qstr($_value);
                }

                $result = $this->DB->CacheGetCol(
                   'SELECT base_main.id FROM base_main WHERE base_main.comp_title LIKE ' . implode (' OR base_main.comp_title LIKE ', $this->query['qlike'])
                );
            }
            else
            {
                if (preg_match('/^[*]/', $this->query['q']) && !empty($this->query['q_phrase']) && $this->sphinx_status())
                {
                    $query = $this->sphinx_pdo->prepare(
                        'SELECT id, user_id, WEIGHT() AS weight FROM ' . SPHINX_INDEX . ' WHERE MATCH(?) ' .
                        'LIMIT ' . $this->sphinx_limit . ' OPTION ranker=expr(?), max_matches=' . $this->sphinx_limit
                    );

                    $query->execute(array($this->query['q_phrase'], $this->ranker));

                    $user_matches = array();

                    while($row = $query->fetch())
                    {
                        $user_matches[$row['user_id']][] = array(
                            'id' => $row['id'],
                            'weight' => $row['weight']
                        );
                    }

                    if (!empty($user_matches) && !preg_match('/[*]/', trim($this->query['q'], '*')))
                    {
                        $result = self::sort_result_by_user($user_matches, $limit);
                        $this->result_exec = $result;

                        return true;
                    }
                    else
                    {
                        $result = $this->DB->CacheGetCol(
                            'SELECT base_main.id FROM base_main WHERE base_main.comp_title LIKE ?',
                            array($this->query['qlike'])
                        );
                    }
                }
                else
                {
                    $result = $this->DB->CacheGetCol(
                        'SELECT base_main.id FROM base_main WHERE base_main.comp_title LIKE ?',
                        array($this->query['qlike'])
                    );
                }
            }

            if ($result)
            {
                foreach ($result as $_id)
                {
                    $this->result_exec[$_id] = array(
                        'id' => $_id,
                        'weight' => 0
                    );
                }

                return true;
            }
            else
            {
                $this->result_exec = array();
                return false;
            }
        }
        else
        {
            if ($this->sphinx_status() && !empty($this->query['q_phrase']))
            {
                $query = $this->sphinx_pdo->prepare(
                    'SELECT id, user_id, WEIGHT() AS weight FROM ' . SPHINX_INDEX . ' WHERE MATCH(?) ' .
                    'LIMIT ' . $this->sphinx_limit . ' OPTION ranker=expr(?), max_matches=' . $this->sphinx_limit
                );

                $user_matches = array();

                if ($this->query['multiple'])
                {
                    $_no_quantity_restriction = array();
                    $_quantity_restriction = array();

                    if (!empty($this->query['q_phrase']['no_quantity_restriction']))
                    {
                        $query->execute(array($this->query['q_phrase']['no_quantity_restriction'], $this->ranker));

                        while($row = $query->fetch())
                        {
                            $_no_quantity_restriction[$row['user_id']][] = array(
                                'id' => $row['id'],
                                'weight' => $row['weight']
                            );
                        }
                    }

                    if (!empty($this->query['q_phrase']['quantity_restriction']))
                    {
                        $_where = '';

                        foreach ($this->query['q_phrase']['quantity_restriction'] as $q)
                        {
                            $query->execute(array($q['phrase'], $this->ranker));
                            $_id_from_sphinx = array();

                            while($row = $query->fetch())
                            {
                                $_quantity_restriction[$row['user_id']][] = array(
                                    'id' => $row['id'],
                                    'weight' => $row['weight']
                                );
                                $_id_from_sphinx[] = $row['id'];
                            }

                            if (!empty($_id_from_sphinx))
                            {
                                $_where .= '(id IN (' . implode(',', $_id_from_sphinx) . ') AND stock >= ' . $q['qt'] . ') OR';
                            }
                        }

                        if (!empty($_quantity_restriction))
                        {
                            $_where = rtrim($_where, 'OR');
                            $_id = $this->DB->GetCol('SELECT id FROM base_main WHERE ' . $_where);
                        }

                        if (!empty($_id))
                        {
                            foreach ($_quantity_restriction as $user_key => $user)
                            {
                                foreach ($user as $key => $item)
                                {
                                    if (!in_array($item['id'], $_id))
                                    {
                                        unset($_quantity_restriction[$user_key][$key]);
                                    }
                                }

                                if (empty($_quantity_restriction[$user_key]))
                                {
                                    unset($_quantity_restriction[$user_key]);
                                }
                            }
                        }
                    }

                    foreach ($_quantity_restriction as $user => $item)
                    {
                        if (!empty($_no_quantity_restriction[$user]))
                        {
                            foreach ($item as $value)
                            {
                                array_push($_no_quantity_restriction[$user], $value);
                            }
                        }
                        else
                        {
                            $_no_quantity_restriction[$user] = $item;
                        }
                    }

                    $user_matches = $_no_quantity_restriction;
                }
                else
                {
                    $query->execute(array($this->query['q_phrase'], $this->ranker));

                    while($row = $query->fetch())
                    {
                        $user_matches[$row['user_id']][] = array(
                            'id' => $row['id'],
                            'weight' => $row['weight']
                        );
                    }
                }

                if (!empty($user_matches))
                {
                    $result = self::sort_result_by_user($user_matches, $limit);
                    $this->result_exec = $result;

                    return true;

                }
            }

            $this->result_exec = array();

            return false;
        }
    }

    /**
     * Из всех полученных id компонентов равномерно
     * выбираем результаты всех поставщиков.
     *
     * @param array $user_matches
     * @param int $limit
     * @return array
     */
    static protected function sort_result_by_user(array &$user_matches, $limit = 2000)
    {
        $result = array();

        $i = 0;

        while($i < $limit)
        {
            if (empty($user_matches))
            {
                break;
            }

            foreach (array_keys($user_matches) as $user_id)
            {
                if (empty($user_matches[$user_id]))
                {
                    unset($user_matches[$user_id]);
                }
                elseif ($i < $limit)
                {
                    $row = array_shift($user_matches[$user_id]);
                    $result[$row['id']] = $row;
                    $i++;
                }
            }
        }

        // Обнуляем массив для освобождения памяти
        $user_matches = array();

        return $result;
    }

    /**
     * Проверить доступность поиска через SphinxSearch.
     *
     * @return bool
     */
    public function sphinx_status()
    {
        if (!empty($this->sphinx_pdo))
        {
            $query = $this->sphinx_pdo->query('SHOW TABLES');

            while ($table = $query->fetchColumn(0))
            {
                if (SPHINX_INDEX == $table)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Добавляет RT индекс в sphinx
     *
     * @param int $id ID элемента.
     * @param int $user_id ID пользователя.
     * @param string $atoms Атомы.
     * @return void
     */
    public function add_rt_index($id, $user_id, $atoms)
    {
        $query = $this->sphinx_pdo->prepare('INSERT INTO ' . SPHINX_RT_INDEX . ' (id, user_id, atoms) VALUES (?, ?, ?)');
        $query->execute(array($id, $user_id, $atoms));
    }

    /**
     * Обновляет RT индекс в sphinx
     *
     * @param int $id ID элемента.
     * @param int $user_id ID пользователя.
     * @param string $atoms Атомы.
     * @return void
     */
    public function edit_rt_index($id, $user_id, $atoms)
    {
        $query = $this->sphinx_pdo->prepare('REPLACE INTO ' . SPHINX_RT_INDEX . ' (id, user_id, atoms) VALUES (?, ?, ?)');
        $query->execute(array($id, $user_id, $atoms));
    }

    /**
     * Удаляет RT индекс из sphinx
     *
     * @param int $id ID элемента.
     * @return void
     */
    public function delete_rt_index($id)
    {
        $this->sphinx_pdo->query('DELETE FROM ' . SPHINX_RT_INDEX . ' WHERE id = ' . (int)$id);
    }

    /**
     * Удаляет RT индексы из sphinx по user_id
     *
     * @param int $user_id ID пользователя.
     * @return void
     */
    public function delete_rt_index_by_user($user_id)
    {
        $this->sphinx_pdo->query('DELETE FROM ' . SPHINX_RT_INDEX . ' WHERE user_id = ' . (int)$user_id);
    }

    /**
     * Очищает RT индексы
     *
     * @return void
     */
    public function truncate_rt_index()
    {
        $this->sphinx_pdo->query('TRUNCATE RTINDEX ' . SPHINX_RT_INDEX);
    }
}
