<?php

/*
 * Класс: Pager
 * Назначение: генерирование постраничной навигации
 *
 */

class Pager
{
    private $all_items;            // всего элементов
    private $num_page_items;		// кол-во элементов на одной странице
    private $mode;				    // режим "статичный", "динамичный"
    private $display_pages = 7;	// кол-во выводимых страниц (с учётом первой и последней)
    
    private $pages = array();
    private $limit_start;
    private $limit_end;
    private $current_page;
    
    function __construct($all_items, $num_items, $mode, $display_pages = false, $current_page = false)
    {
        $this->all_items = (int)$all_items;
        $this->num_page_items = (int)$num_items;
        $this->mode = $mode;
        
        if ($display_pages !== false)
        {
            $this->display_pages = (int)$display_pages;
        }
        
        if ($current_page === false)
        {
            $this->define_current_page();
        }
        else
        {
            $this->current_page = (int)$current_page;
        }
        
        $this->generate_pages();
        $this->limits();
    }
    
    // Определение текущей страницы
    private function define_current_page()
    {
        if (preg_match("/page[-=](\d+)/i", $_SERVER['REQUEST_URI'], $matches))
        {
            $this->current_page = $matches[1];
        }
        else
        {
            $this->current_page = 1;
        }
    }
    
    // LIMIT для sql-запроса
    private function limits()
    {
        $this->limit_start = ($this->current_page - 1) * $this->num_page_items;
        $this->limit_end = $this->num_page_items;
    }
    
    // Определение ссылки для страницы
    private function page_link($numpage)
    {
        $result = '';

        if ($this->mode == 'dynamic')
        {
            if (preg_match("/page=\d+/i", $_SERVER['REQUEST_URI']))
            {
                $result = preg_replace("/page=\d+/i", 'page=' . $numpage, $_SERVER['REQUEST_URI']);
            }
            else if (preg_match("/\?/i", $_SERVER['REQUEST_URI']))
            {
                $result = $_SERVER['REQUEST_URI'] . '&page=' . $numpage;
            }
            else
            {
                $result = $_SERVER['REQUEST_URI'] . '?page=' . $numpage;
            }
        }
        else if ($this->mode == 'static')
        {
            if (preg_match("/page-\d+/i", $_SERVER['REQUEST_URI']))
            {
                $replace = $numpage == 1 ? '' : 'page-' . $numpage;
                $result = preg_replace("/page-\d+/i", $replace, $_SERVER['REQUEST_URI']);
            }
            else if (preg_match("/-[^\/]+$/i", $_SERVER['REQUEST_URI']))
            {
                $replace = $numpage == 1 ? '' : 'page-' . $numpage . '.html';
                $result = preg_replace("/\.html$/i", $replace, $_SERVER['REQUEST_URI']);
            }
            else if (preg_match("/\.html$/i", $_SERVER['REQUEST_URI']))
            {
                $replace = $numpage == 1 ? '' : '_page-' . $numpage . '.html';
                $result = preg_replace("/\.html$/i", $replace, $_SERVER['REQUEST_URI']);
            }
            else
            {
                $replace = $numpage == 1 ? '' : 'page-' . $numpage . '.html';
                $result = $_SERVER['REQUEST_URI'] . $replace;
            }
        }
        else if ($this->mode == 'hash')
        {
            $result = '#' . $numpage;
        }

        if ($numpage == 1)
        {
            $result = preg_replace('/\.html$/i', '', $result);
        }

        return $result;
    }
    
    // Генерация постраничной навигации
    private function generate_pages()
    {
        $all_pages = ceil($this->all_items / $this->num_page_items);
        
        $offset = floor(($this->display_pages - 2) / 2);

        $this->pages['total'] = $all_pages;
        $this->pages['prev'] = ($this->current_page > 1) ? $this->page_link($this->current_page - 1) : false;
        $this->pages['next'] = ($this->current_page < $all_pages) ? $this->page_link($this->current_page + 1) : false;
        
        // -------------------------------
        
        if ($all_pages > $this->display_pages)
        {
            $start_p = $this->current_page - $offset;
            $end_p = $this->current_page + $offset;
            
            // Предыдущая и первая страницы
            if ($this->current_page > 1)
            {
                if ($this->current_page - $offset > 2)
                {
                    $this->pages['first'] = $this->page_link(1);
                }
                else
                {
                    $start_p = 1;
                    $end_p = $this->display_pages - 1;
                }
            }
            else
            {
                $start_p = 1;
                $end_p = $this->display_pages - 1;
            }
            
            // Следующая и последняя страницы
            if ($this->current_page < $all_pages)
            {
                if ($all_pages - $this->current_page > $offset + 1)
                {
                    $this->pages['last'] = $this->page_link($all_pages);
                }
                else
                {
                    $start_p = $all_pages - 1 - $offset * 2;
                    $end_p = $all_pages;
                }
            }
            else
            {
                $start_p = $all_pages - 1 - $offset * 2;
                $end_p = $all_pages;
            }
        }
        else
        {
            $start_p = 1;
            $end_p = $all_pages;
        }
        
        // -------------------------------
        
        $n = 0;
        for ($i = $start_p; $i <= $end_p && $i <= $all_pages; $i++)
        {
            $this->pages['pages'][$n]['num'] = $i;
            $this->pages['pages'][$n]['link'] = $this->page_link($i);

            $n++;
        }
        
        $this->pages['current'] = $this->current_page;
        
        $this->pages['show_pages'] = ($this->all_items > $this->num_page_items);
    }
    
    public function get_pages()
    {
        return $this->pages;
    }
    
    public function get_prev_next()
    {
        $prev = false;
        $next = false;
        
        if ($this->current_page > 1)
        {
            $prev = $this->page_link($this->current_page - 1);
        }
        
        if ($this->current_page < $this->pages['total'])
        {
            $next = $this->page_link($this->current_page + 1);
        }
        
        return array('prev' => $prev, 'next' => $next);
    }
    
    public function get_limit_start()
    {
        return $this->limit_start;
    }
    
    public function get_limit_end()
    {
        return $this->limit_end;
    }
    
    public function get_current_page()
    {
        return $this->current_page;
    }
}

?>