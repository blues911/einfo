<?php

class SearchSQL extends Search
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Установить поисковый запрос текущего поиска.
     *
     * @param string $query Поисковый запрос.
     * @param bool $strong Признак строгого поиска.
     * @return bool
     */
    public function set($query, $strong = false)
    {
        $this->result_exec = null;
        $this->result = null;

        // Текущая поисковая фраза
        $this->query['q'] = $query;
        $this->query['strong'] = $strong;

        if ($strong)
        {
            $this->query['qatoms'] = get_request_atoms_quotes($this->query['q']);
            $this->query['qlike'] = $this->query['q'];
            $this->query['qlike_flag'] = true;
        }
        else
        {
            $this->query['qatoms'] = get_request_atoms_quotes($this->query['q']);
            $this->query['qlike'] = '%' . implode('%', $this->query['qatoms']) . '%';
            $this->query['qlike_flag'] = false;
        }

        return true;
    }

    /**
     * Выполнить поиск и получить id и релевантность (вес)
     * найденных компонентов. Формат ответа:
     * [{id: <id>, weight: <релевантность>}, ...].
     *
     * В реализации SQL релевантность всегда равна нулю.
     * Необходимо для сохранения совместимости.
     *
     * @param int $limit Лимит поисковой выдачи.
     * @return bool
     */
    public function search_exec($limit = 2000)
    {
        if ($this->query['strong'])
        {
            if ($result = $this->DB->CacheGetCol(
                'SELECT base_main.id FROM base_main WHERE base_main.comp_title = ?',
                array($this->query['q'])
            ))
            {
                foreach ($result as $_id)
                {
                    $this->result_exec[$_id] = array(
                        'id' => $_id,
                        'weight' => 0
                    );
                }

                return true;
            }
            else
            {
                $this->result_exec = array();
                return false;
            }
        }
        else
        {
            if ($result = $this->DB->CacheGetCol('
            SELECT
              base_main.id
            FROM
              base_main
            WHERE
              base_main.comp_title LIKE ?',
                array($this->query['qlike'])))
            {
                foreach ($result as $_id)
                {
                    $this->result_exec[$_id] = array(
                        'id' => $_id,
                        'weight' => 0
                    );
                }

                return true;
            }
            else
            {
                $this->result_exec = array();
                return false;
            }
        }
    }

} 