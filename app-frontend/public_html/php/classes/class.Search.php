<?php

/**
 * Class Search
 *
 * Абстрактный класс поиска.
 */
abstract class Search
{
    /** @var null|array Массив с Id элементов удовлетворяющих строке запроса. */
    protected $result_exec = null;

    /** @var null|array Массив с результатами с учетом фильтров. */
    protected $result = null;

    protected $query;
    protected $source;
    protected $DB;
    protected $DB_master;
    protected $max_atoms;
    public $USER_SETTINGS;
    public $SETTINGS;

    public function __construct()
    {
        $this->max_atoms = 20;
        $this->DB = &$GLOBALS['DB'];
        $this->DB_master = &$GLOBALS['DB_master'];
        $this->USER_SETTINGS = &$GLOBALS['USER_SETTINGS'];
        $this->SETTINGS = &$GLOBALS['SETTINGS'];
        $this->query = array();
    }

    /**
     * Установить поисковый запрос текущего поиска.
     *
     * @param string $query Поисковый запрос.
     * @param bool $strong Признак строгого поиска.
     * @return bool
     */
    public function set($query, $strong = false)
    {
        return false;
    }

    /**
     * Выполнить поиск и получить id и релевантность (вес)
     * найденных компонентов. Формат ответа:
     * [{id: <id>, weight: <релевантность>}, ...].
     *
     * @param int $limit Лимит поисковой выдачи.
     * @return bool
     */
    public function search_exec($limit = 2000) {
        return false;
    }

    /**
     * Формируем фильтр для SQL-запроса.
     *
     * @return array
     */
    protected function sql_filter()
    {
        $filter = array('where' => array(), 'order' => '');

        // Есть ли массив с Id
        if (is_null($this->result_exec))
        {
            $this->search_exec();
        }

        // Строгая последовательность атомов для однострочного поиска
        if (!$this->query['multiple'] && !$this->query['strong'] && !$this->query['special'])
        {
            $filter['where'][] = ' main.atoms2 LIKE ' . $this->DB->qstr($this->query['qlike']) . ' ';
        }

        // Если в поиске используются спецсимволы
        if (!empty($this->query['special']))
        {
            if (is_array($this->query['qlike']))
            {
                $q_like = implode(' OR main.comp_title LIKE ', $this->query['qlike']);
                $filter['order'] = ' main.comp_title ASC, ';
            }
            else $q_like = $this->DB->qstr($this->query['qlike']);

            $filter['where'][] = ' (main.comp_title LIKE ' . $q_like . ') ';
        }

        // Исключить работающих только с юрлицами
        if (!empty($this->USER_SETTINGS['individual_buyers']))
        {
            $filter['where'][] = ' users.individual_buyers = 1 ';
        }

        // Исключить заблокированных поставщиков
        if (!empty($this->USER_SETTINGS['search_users_disallow']))
        {
            $filter['where'][] = ' main.user_id NOT IN(' . implode(',', $this->USER_SETTINGS['search_users_disallow']) . ') ';
        }

        // Геосортировка страна
        if (!empty($this->USER_SETTINGS['country_id']))
        {
            $filter['order'] = ' users.country_id = ' . $this->USER_SETTINGS['country_id'] . ' DESC, ';
        }

        // Геосортировка город
        if (!empty($this->USER_SETTINGS['city_id']))
        {
            $filter['order'] = ' users.city_id = ' . $this->USER_SETTINGS['city_id'] . ' DESC, ';
        }

        // Объединение всех условий
        if (!empty($filter['where']))
        {
            $filter['where'] = ' AND ' .  implode(' AND ', $filter['where']);
        }
        else
        {
            $filter['where'] = '';
        }

        return $filter;
    }

    /**
     * Произвести поиск (если не произведен ранее) и
     * вернуть результаты в конечном формате.
     *
     * @param null|array $timer Сюда записываются таймеры каждого этапа поиска.
     * @return array|null Массив найденных компонентов.
     */
    public function search(&$timer = null)
    {
        $timer = array('start_sphinx' => 0, 'start_tmp' => 0, 'start_select' => 0, 'finish' => 0);

        if (is_null($this->result))
        {
            $timer['start_sphinx'] = microtime(1);

            if (is_null($this->result_exec))
            {
                if (!empty($this->query['multiple']))
                {
                    $this->search_exec($this->SETTINGS['search']['multiple_max_on_page']);
                }
                else
                {
                    $this->search_exec((int)$this->SETTINGS['search']['max_on_page']);
                }
            }

            if (count($this->result_exec))
            {
                $filter = $this->sql_filter();

                $timer['start_tmp'] = microtime(1);

                if (!empty($this->query['multiple']))
                {
                    if (!empty($this->query['q_phrase']['quantity_restriction']))
                    {
                        $_temp = end($this->query['q_phrase']['quantity_restriction']);
                        $search_hash = md5($_temp['phrase']);
                    }
                    else
                    {
                        $search_hash = md5($this->query['q_phrase']['no_quantity_restriction']);
                    }
                }
                else
                {
                    $search_hash = md5($this->query['q']);
                }

                $this->DB->Execute('DROP TEMPORARY TABLE IF EXISTS search_' . $search_hash);
                $this->DB->Execute('
                    CREATE TEMPORARY TABLE search_' . $search_hash . ' (
                        `id` INT(11) unsigned NOT NULL,
                        `weight` INT(11) unsigned,
                        PRIMARY KEY (`id`),
                        KEY `weight` (`weight`)
                    ) ENGINE=MEMORY DEFAULT CHARSET=cp1251');

                $_values = array();
                foreach ($this->result_exec as $_id => $_match)
                {
                    $_values[] = '(' . $_id . ',' . $_match['weight'] . ')';
                }

                $values = implode(',', $_values);
                $this->DB->Execute('INSERT IGNORE INTO search_' . $search_hash . ' (id, weight) VALUES ' . $values);

                $timer['start_select'] = microtime(1);

                //Если пользователь не авторизован
                $users_white_list = 1;
                if (!empty($this->USER_SETTINGS['search_users_white_list']))
                {
                    $users_white_list = implode(',', $this->USER_SETTINGS['search_users_white_list']);
                }

                $components_search = sprintf("SELECT
                                      main.id,
                                      main.comp_title,
                                      LOWER(main.comp_title) AS comp_title_lower,
                                      main.descr,
                                      prepare_prices(main.prices, IF(users.price_with_nds, 1, %9\$s), %1\$s, %2\$s) AS prices,
                                      main.stock,
                                      main.stock_remote,
                                      main.delivery,
                                      main.mfgdate,
                                      main.comp_url,
                                      main.user_id,
                                      users.title AS user_title,
                                      users.country_id AS user_country_id,
                                      country.country AS user_country,
                                      users.city_id AS user_city_id,
                                      city.city AS user_city,
                                      CONCAT(
                                        users.phone1,
                                        IF(LENGTH(users.phone1) AND LENGTH(users.phone1_ext), CONCAT(' доб. ', users.phone1_ext), ''),
                                        IF(LENGTH(users.phone1) AND LENGTH(users.phone2), ', ', ''),
                                        users.phone2,
                                        IF(LENGTH(users.phone2) AND LENGTH(users.phone2_ext), CONCAT(' доб. ', users.phone2_ext), '')
                                      ) AS user_phone,
                                      users.phone1 AS user_phone1,
                                      users.phone1_ext AS user_phone1_ext,
                                      users.phone2 AS user_phone2,
                                      users.phone2_ext AS user_phone2_ext,
                                      main.manuf_title,
                                      MD5(main.comp_title) AS cname_id,
                                      main.comp_title AS cname_title,
                                      users.price_date AS price_date,
                                      IF(users.price_date < NOW() - INTERVAL 7 DAY, IF(users.price_date < NOW() - INTERVAL 14 DAY, 3, 2), 1) AS price_status,
                                      users.individual_buyers AS individual_buyers,
                                      users.www AS user_www,
                                      users.email AS user_email,
                                      IF (users.id IN (" . $users_white_list . "), users.rating + 10, users.rating)  AS user_rating,
                                      IF (users_last_visit.last_visit >= NOW() - INTERVAL %3\$s SECOND, 1, 0) AS user_online,
                                      IF (token.fcm_last_update >= NOW() - INTERVAL %3\$s SECOND, 
                                      	IF (IFNULL(token.fcm_token, '') = '', 0, 1), 
                                      	0
                                      ) AS user_allow_chat
                                    FROM
                                      base_main main
                                      INNER JOIN search_" . $search_hash . " AS search_tmp ON main.id = search_tmp.id
                                      INNER JOIN users ON main.user_id = users.id
                                      INNER JOIN ip2ruscity_countries AS country ON country.country_id = users.country_id
                                      INNER JOIN ip2ruscity_cities AS city ON city.city_id = users.city_id
                                      LEFT JOIN users_last_visit ON main.user_id = users_last_visit.user_id
                                      LEFT JOIN users_chat_tokens AS token ON token.owner_value = users.id AND token.owner_type = 'user' AND token.is_active = 1
                                    WHERE users.status = 1 %4\$s AND users.id NOT IN (%5\$s, %6\$s)
                                    ORDER BY %7\$s user_online DESC, user_rating DESC, main.user_id ASC, search_tmp.weight DESC, main.comp_title ASC
                                    LIMIT 0, %8\$s",
                                            $this->SETTINGS['cur_rates']['usd'],
                                            $this->SETTINGS['cur_rates']['eur'],
                                            CHAT_LAST_VISIT_INTERVAL,
                                            $filter['where'],
                                            USER_ADMIN_ID,
                                            USER_PARTNER_ID,
                                            $filter['order'],
                    (!empty($this->query['multiple']) ? $this->SETTINGS['search']['multiple_max_on_page'] : $this->SETTINGS['search']['max_on_page']),
                                            $this->SETTINGS['nds']['value']
                );

                $this->result = $this->DB->GetAll($components_search);

                $this->DB->Execute('DROP TEMPORARY TABLE IF EXISTS search_' . $search_hash);

                $timer['finish'] = microtime(1);

                $timer['sphinx'] = round($timer['start_tmp'] - $timer['start_sphinx'], 3);
                $timer['tmp'] = round($timer['start_select'] - $timer['start_tmp'], 3);
                $timer['select'] = round($timer['finish'] - $timer['start_select'], 3);
                $timer['total'] = $timer['sphinx'] + $timer['tmp'] + $timer['select'];
            }
            else
            {
                $this->result = array();
            }

        }
        
        return $this->result;
    }

    /**
     * Получить количество уникальных компонентов
     * и пользователей в текущей поисковой выдаче.
     *
     * @return array
     */
    public function count_components()
    {
        $result = array(
            'components' => array(),
            'users' => 0
        );

        if (is_null($this->result))
        {
            $this->search();
        }

        $components_count = array();
        foreach ($this->result as $component)
        {
            $comp_title = mb_strtolower($component['comp_title']);
            if (!empty($components_count[$comp_title]))
            {
                $components_count[$comp_title]++;
            }
            else
            {
                $components_count[$comp_title] = 1;
            }
        }

        $components = array();
        foreach (array_keys($components_count) as $component)
        {
            $components[] = $this->DB->qstr($component);
        }
        unset($components_count);

        if (!empty($components))
        {
            $_result = $this->DB->GetAll(
                'SELECT base_main.comp_title, COUNT(DISTINCT(base_main.id)) AS cnt
                FROM base_main INNER JOIN users ON users.id = base_main.user_id
                WHERE users.status = 1 AND base_main.comp_title IN(' . implode(',', $components) . ')
                GROUP BY base_main.comp_title'
            );

            if (!empty($_result))
            {
                foreach ($_result as $k => $row)
                {
                    $result['components'][strtolower($row['comp_title'])] = $row['cnt'];
                    unset($_result[$k]);
                }
            }

            $result['users'] = (int)$this->DB->GetOne(
                'SELECT COUNT(DISTINCT(base_main.user_id))
                FROM base_main INNER JOIN users ON users.id = base_main.user_id
                WHERE users.status = 1 AND base_main.comp_title IN(' . implode(',', $components) . ')'
            );
        }

        return $result;
    }

    /**
     * Количество компонентов в текущей
     * поисковой выдаче.
     *
     * @return int
     */
    public function count()
    {
        if (is_null($this->result))
        {
            $this->search();
        }

        return count($this->result);
    }

    /**
     * Занесение статистики запросов в БД.
     *
     * @param int $cnt Кол-во найденных компонентов.
     * @param null|string $source Источник поиска.
     * @return bool
     */
    public function search_log($cnt, $source = null)
    {
        $has_referer = !empty($_SERVER['HTTP_REFERER']);
        $has_cookie = !empty($_COOKIE['is_search']);

        if ($has_cookie)
        {
            setcookie('is_search', '0', time() - 1, '/');
        }

        $search_stat['date'] = date('Y-m-d H:i:s');
        $search_stat['ip'] = ($source == 'telegram') ? null : get_ip();

        if ($this->query['multiple'])
        {
            $search_stat['query_user'] = 'multiple_search: ' . count($this->query['q']);
            $search_stat['query_regex'] = '';
        }
        else
        {
            $search_stat['query_user'] = $this->query['q'];
            $search_stat['query_regex'] = isset($this->query['qlike']) ? $this->query['qlike'] : '';
        }

        $search_stat['cnt_result'] = $cnt;
        $search_stat['weight'] = $cnt ? (1 / $cnt) : 0;
        $search_stat['search_source'] = $source;

        if (($has_referer && $has_cookie) || $source == 'telegram')
        {
            $this->DB_master->AutoExecute('search_stat', $search_stat, 'INSERT');

            return true;
        }

        return false;
    }

    // -------------------------------------------------------------------------------------------------------------
    // СТАТИЧНЫЕ ФУНКЦИИ
    // -------------------------------------------------------------------------------------------------------------


    /**
     * Подготовка поисковой фразы для множественного поиска.
     *
     * @param string $query
     * @return array
     */
    static public function prepare_query_multiple($query)
    {
        $query = stripslashes(strip_tags(trim($query)));
        $query = @iconv('UTF-8', 'UTF-8//IGNORE', $query);

        preg_match('/[\t|;]+([0-9]+[\s]*[0-9]*).*/uis', $query, $matches);

        return array(
            'query' => !empty($matches[0]) ? str_replace($matches[0], '', $query) : $query,
            'quantity' => str_replace(' ', '', array_pop($matches))
        );
    }

    /**
     * Подготовка поисковой фразы из URL.
     *
     * @param string $query
     * @return string
     */
    static public function prepare_query($query)
    {
        $query = stripslashes(strip_tags(trim(decodeURIComponent($query))));

        $_query = @iconv('UTF-8', 'UTF-8//IGNORE', $query);

        if (empty($_query))
        {
            $_query = @iconv('cp1251', 'UTF-8//IGNORE', $query);
        }

        return $_query;
    }

    /**
     * Иключть кавычки из поисковой фразы.
     *
     * @param string $query
     * @return string
     */
    static public function slice_quotes ($query)
    {
        return trim(preg_replace("/\"|\'/iu", ' ', $query));
    }

    /**
     * Обрезать последний атом.
     *
     * @param string $query
     * @return string
     */
    static public function slice_last_atom ($query)
    {
        return trim(preg_replace("/([^a-zA-Zа-яА-Я0-9]*)([a-zA-Zа-яА-Я]+|[0-9]+)([^a-zA-Zа-яА-Я0-9]*)$/iu", ' ', $query));
    }

    /**
     * Семантическая сортировка результатов поиска.
     *
     * @param array $list
     * @param string $term
     * @return array
     */
    static public function sort_semantic(array $list, $term)
    {
        $result = array();
        $user_groups = array();
        $term_atoms = get_request_atoms($term);

        foreach ($list as $row)
        {
            $user_groups[$row['user_id']][] = $row;
        }

        foreach (array_keys($user_groups) as $k)
        {
            usort($user_groups[$k], function($a, $b) use ($term_atoms)
                {
                    // Подготавливаем различные значения для сортировки.
                    $atoms_a = get_request_atoms(mb_strtoupper($a['comp_title']));
                    $atoms_b = get_request_atoms(mb_strtoupper($b['comp_title']));

                    $atoms_cnt_a = count($atoms_a);
                    $atoms_cnt_b = count($atoms_b);

                    $same_a = implode(' ', $atoms_a) === implode(' ', $term_atoms);
                    $same_b = implode(' ', $atoms_b) === implode(' ', $term_atoms);

                    $atoms_intersect_a = array_intersect_assoc($term_atoms, $atoms_a);
                    $atoms_intersect_b = array_intersect_assoc($term_atoms, $atoms_b);

                    $atoms_intersect_cnt_a = count($atoms_intersect_a);
                    $atoms_intersect_cnt_b = count($atoms_intersect_b);

                    $atoms_intersect_sum_a = array_sum(array_keys($atoms_intersect_a));
                    $atoms_intersect_sum_b = array_sum(array_keys($atoms_intersect_b));

                    $atoms_same_intersect_a = array_intersect($term_atoms, $atoms_a);
                    $atoms_same_intersect_b = array_intersect($term_atoms, $atoms_b);

                    $atoms_same_intersect_cnt_a = count($atoms_same_intersect_a);
                    $atoms_same_intersect_cnt_b = count($atoms_same_intersect_b);

                    $atoms_same_intersect_sum_a = 0;
                    $_atoms_keys_a = array_flip($atoms_a);
                    foreach ($atoms_same_intersect_a as $_intersect)
                    {
                        if (array_key_exists($_intersect, $_atoms_keys_a))
                        {
                            $atoms_same_intersect_sum_a += (int)$_atoms_keys_a[$_intersect];
                        }
                    }

                    $atoms_same_intersect_sum_b = 0;
                    $_atoms_keys_b = array_flip($atoms_b);
                    foreach ($atoms_same_intersect_b as $_intersect)
                    {
                        if (array_key_exists($_intersect, $_atoms_keys_b))
                        {
                            $atoms_same_intersect_sum_b += (int)$_atoms_keys_b[$_intersect];
                        }
                    }

                    // Проверка на точное вхождение по атомам.
                    if ($same_a != $same_b)
                    {
                        if ($same_a)
                        {
                            return -1;
                        }
                        else
                        {
                            return 1;
                        }
                    }

                    // Проверка: сколько атомов находятся в нужных местах.
                    if ($atoms_intersect_cnt_a != $atoms_intersect_cnt_b)
                    {
                        return $atoms_intersect_cnt_b - $atoms_intersect_cnt_a;
                    }

                    // Проверка: сколько атомов встречается (не зависимо от положения).
                    if ($atoms_same_intersect_cnt_a != $atoms_same_intersect_cnt_b)
                    {
                        return $atoms_same_intersect_cnt_b - $atoms_same_intersect_cnt_a;
                    }

                    // Проверка: расстояние между совпавшими атомами и их положение ближе к левому краю.
                    if ($atoms_same_intersect_sum_a != $atoms_same_intersect_sum_b)
                    {
                        return $atoms_same_intersect_sum_a - $atoms_same_intersect_sum_b;
                    }

                    // Проверка: у кого хвосты с лишними атомами короче.
                    if ($atoms_cnt_a != $atoms_cnt_b)
                    {
                        return $atoms_cnt_a - $atoms_cnt_b;
                    }

                    // Проверка: расстояние между совпавшими (с учетом положения) атомами и их положение ближе к левому краю.
                    if ($atoms_intersect_sum_a != $atoms_intersect_sum_b)
                    {
                        return $atoms_intersect_sum_a - $atoms_intersect_sum_b;
                    }

                    // Иначе: сортировка по алфавиту.
                    return strcmp(mb_strtoupper($a['comp_title']), mb_strtoupper($b['comp_title']));
                });

            foreach ($user_groups[$k] as $row)
            {
                $result[] = $row;
            }

            unset($user_groups[$k]);
        }

        return $result;
    }
}
