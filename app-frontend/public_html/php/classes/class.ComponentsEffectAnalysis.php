<?php

include_once('php/libs/PHPExcel/PHPExcel.php');

/**
 * Class RequestsAnalysis
 *
 * Расчета эффекта от размещения прайс-листа
 */
class ComponentsEffectAnalysis
{
    /** @var integer Сколько строк из файла взять, чтобы найти колонку с компонентами. */
    private $_rows_depth_analysis = 500;

    /** @var integer Кол-во компоневтов для одного запроса в цикле при расчете эффекта. */
    private $_effect_analysis_limit = 500;

    /** @var integer За какой период оценивать эффективность (кол-во месяцев). */
    private $_period_in_months = 6;

    /** @var integer Максимальное кол-во строк из файла */
    private $_rows_max = 100000;

    protected $DB;

    public function __construct()
    {
        $this->DB = &$GLOBALS['DB'];
    }

    /**
     * Определение в какой колонке находятся компоненты.
     * Метод вызывает себя пока не найдет колонку или файл не закончится.
     *
     * @param object $sheet объект phpExcel листа.
     * @param int $highest_row кол-во строк phpExcel листа.
     * @param int $current_row текущая строка - показатель для повторных вызовов.
     * @return string|bool Буква колонки с компонентами
     */
    public function detect_component_column($sheet, $highest_row, $current_row = 1)
    {
        $columns = $matches_count = array();
        $max_matches_count = 0;
        $component_column = false;

        // Если прошли все строки файла, но колонка с компонентами так и не найдена
        if ($current_row >= $highest_row)
            return false;

        // В файле строк хватает, чтобы анализировать этапами
        if ($highest_row > $this->_rows_depth_analysis)
        {
            // Начальная строка для следующего вызова и ограничение чтения строк для этого
            $next_current_row = $current_row + $this->_rows_depth_analysis;

            if ($next_current_row > $highest_row)
                $next_current_row = $highest_row + 1;
        }
        else
        {
            $next_current_row = $highest_row + 1;
        }

        // Составление массива со значениями колонок
        foreach ($sheet->getRowIterator($current_row) as $row)
        {
            $row_index = $row->getRowIndex();

            if ($row_index < $next_current_row)
            {
                foreach ($row->getCellIterator() as $cell)
                {
                    $cell_index = $cell->getColumn();
                    $cell_value = trim(
                        mb_strtolower($cell->getValue())
                    );

                    // Название компонента должно содержать хотя бы 1 букву
                    if (!empty($cell_value) && preg_match('/[a-zа-я]/ui', $cell_value))
                    {
                        $columns[$cell_index][] = $this->DB->qstr($cell_value);
                    }
                }
            }
        }

        if (!empty($columns))
        {
            // Проверка каждой колонки на совпадения с названиями компонентов
            foreach ($columns as $col_num => $values)
            {
                $matches_count[$col_num] = $this->DB->GetOne('
                    SELECT
                        COUNT(DISTINCT comp_title)
                    FROM
                        base_main
                    WHERE
                        comp_title IN(' . implode(',', $values) . ')
                ');
            }

            // Определение колонки с наибольшим кол-ом совпадений
            foreach ($matches_count as $column => $count)
            {
                if ($count > $max_matches_count)
                {
                    $max_matches_count = $count;
                    $component_column = $column;
                }
            }

            // Если не найдена колонка, но еще остались строки, то ищем в следующей группе строк
            if ($next_current_row && !$component_column)
            {
                return $this->detect_component_column($sheet, $highest_row, $next_current_row);
            }
        }

        return $component_column;
    }

    /**
     * Получение названий компонентов из всех данных.
     *
     * @param object $sheet Объект phpExcel листа.
     * @param string $component_column Буква колонки с компонентами.
     * @param int $highest_row кол-во строк phpExcel листа.
     * @return array Массив компонентов
     */
    public function get_components($sheet, $component_column, $highest_row)
    {
        $components = array();

        $rows_max = ($highest_row > $this->_rows_max) ? $this->_rows_max : $highest_row;

        foreach ($sheet->getRowIterator(1) as $row)
        {
            $row_index = $row->getRowIndex();

            if ($row_index <= $rows_max)
            {
                foreach ($row->getCellIterator() as $cell)
                {
                    if ($cell->getColumn() == $component_column)
                    {
                        $cell_value = trim($cell->getValue());

                        if (!empty($cell_value))
                            $components[] = $cell_value;
                    }
                }
            }
            else
            {
                break;
            }
        }

        return $components;
    }

    /**
     * Сохранение анализируемых файлов с именем даты и времени
     *
     * @param array $file Данные о временном файле.
     * @return array|bool
     */
    public function save_file($file)
    {
        $file_name_parts = explode('.', $file['name']);
        $extension = end($file_name_parts);
        $file_name = date("Y-m-d_H-i-s") . '.' . $extension;
        $path = PATH_ANALYSIS . '/' . $file_name;

        if (move_uploaded_file($file['tmp_name'], $path))
            return array('path' => $path, 'name' => $file_name);
        else
            return false;
    }

    /**
     * Среднее значение за n месяцев
     *
     * @param int $count Кол-во кликов по разным элементам в статистике.
     * @return int
     */
    public function count_average_value($count)
    {
        if ($count > $this->_period_in_months)
            $average_value = ceil($count / $this->_period_in_months);
        else
            $average_value = 1;

        return $average_value;
    }

    /**
     * Анализ файла.
     *
     * @param array $file Данные о временном файле.
     * @return array
     */
    public function analyze_file($file)
    {
        $result = array();
        $count_profile_view = $count_profile_click = 0;

        if (function_exists('sys_getloadavg') && sys_getloadavg()[0] > MAX_SERVER_LOAD)
        {
            $result['error'] = 'Прайс-лист не может быть проанализирован из-за высокой загрузки сервера, попробуйте позже';
        }
        else
        {
            if (file_exists($file['tmp_name']))
            {
                $file = $this->save_file($file);

                if (!empty($file))
                {
                    $reader = PHPExcel_IOFactory::createReaderForFile($file['path']);
                    $reader->setReadDataOnly(true);

                    $excel = $reader->load($file['path']);

                    $excel->setActiveSheetIndex(0);
                    $sheet = $excel->getActiveSheet();
                    $highest_row = $sheet->getHighestRow();

                    $component_column = $this->detect_component_column($sheet, $highest_row);

                    if ($component_column)
                    {
                        // Записываем инфо о файле в таблицу статуса обработки
                        $this->DB->AutoExecute(
                            'components_effect_analysis',
                            array(
                                'rows_count' => $highest_row,
                                'file_name' => $file['name'],
                                'date' => date("Y-m-d H:i:s"),
                            ),
                            'INSERT'
                        );

                        $file_analysis_id = $this->DB->Insert_ID();

                        // Возвращаем id записи о файле, скрипт продолжает работу, обновляя данные в таблице file_analysis_status
                        ob_start();
                        echo json_encode(array('file_id' => $file_analysis_id));
                        header('Connection: close');
                        header('Content-Length: '.ob_get_length());
                        ob_end_flush();
                        ob_flush();
                        flush();

                        $components = $this->get_components($sheet, $component_column, $highest_row);

                        unset($excel, $sheet);

                        $components_count = 0;

                        foreach ($components as $k => $component)
                        {
                            $component = mb_strtolower(
                                implode(' ', get_request_atoms($component))
                            );

                            $components[$k] = $this->DB->qstr($component);

                            $components_count++;
                        }

                        if ($components_count <= $this->_effect_analysis_limit)
                        {
                            $component_chunks[0] = $components;
                        }
                        else
                        {
                            $component_chunks = array_chunk($components, $this->_effect_analysis_limit);
                        }

                        foreach ($component_chunks as $components)
                        {
                            $stat = $this->DB->GetRow('
                                SELECT
                                    SUM(IF(result.action = "profile_view", 1, 0)) AS profile_view_count,
                                    SUM(IF(result.action = "profile_click_site", 1, 0)) AS profile_click_count
                                FROM
                                (
                                    SELECT
                                        action
                                    FROM
                                        users_stat
                                    WHERE
                                        search_atoms IN(' . implode(',', $components) . ')
                                        AND TO_DAYS(NOW()) - TO_DAYS(date) <= ' . ($this->_period_in_months * 30) . '
                                    GROUP BY CONCAT(ip, DATE(date), action, search_atoms)
                                ) AS result
                            ');

                            $count_profile_view += $stat['profile_view_count'];
                            $count_profile_click += $stat['profile_click_count'];

                            // Обновление кол-ва обработанных строк в таблице статуса анализа
                            $this->DB->Execute('
                                UPDATE
                                    components_effect_analysis
                                SET
                                    current_row = current_row + ' . count($components) . '
                                WHERE
                                    id = ' . $file_analysis_id . '
                            ');
                        }

                        if ($count_profile_view > 0)
                            $count_profile_view = $this->count_average_value($count_profile_view);

                        if ($count_profile_click > 0)
                            $count_profile_click = $this->count_average_value($count_profile_click);

                        $this->DB->AutoExecute(
                            'components_effect_analysis',
                            array(
                                'count_profile_view' => $count_profile_view,
                                'count_profile_click' => $count_profile_click
                            ),
                            'UPDATE',
                            'id=' . $file_analysis_id
                        );
                    }
                    else
                    {
                        // Не найдена колонка с названиями товаров
                        $result['error'] = 'Данных из файла недостаточно для анализа';
                    }
                }
                else
                {
                    $result['error'] = 'Не удалось разобрать файл';
                }
            }
            else
            {
                $result['error'] = 'Файл не найден';
            }
        }

        return $result;
    }

    public function watch_analysis($file_id)
    {
        return $this->DB->GetRow('
            SELECT
                *
            FROM
                components_effect_analysis
            WHERE
                id = ?
        ', array($file_id));
    }
}