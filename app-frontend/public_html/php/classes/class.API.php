<?php

/**
 * API
 * Класс для внешних обращений к серверу
 *
 */


class API
{
    protected $DB;
    protected $token_id;
    protected $request;
    protected $request_method;
    protected $service;
    protected $service_path;

    public $errors = array(
        101 => 'Ошибка схемы данных.',
        102 => 'Не указан token.',
        103 => 'Доступ запрещен.',
        104 => 'Cервис не найден.',
        105 => 'Hичего не найдено.',
        106 => 'Cлишком короткий запрос.',
        107 => 'Не верно задан метод.',
        199 => 'Не известная ошибка.',
    );

    public function __construct()
    {
        $this->DB             = &$GLOBALS['DB'];
        $this->request        = json_decode(file_get_contents('php://input'), true);
        $this->request_method = $_SERVER['REQUEST_METHOD'];
        $this->set_service();
    }

    /**
     * Устанавливает название сервиса и путь до него
     * @return void
     */
    public function set_service()
    {
        if (preg_match("/api\/([^\/][a-z0-9_\-]+[^\/])+/i", $_SERVER['REQUEST_URI'], $matches))
        {
            $this->service_path = PATH_FRONTEND . '/content/ajax/api/' . $matches[1] . '.php';
            $this->service = $matches[1];
        }
        else
        {
            $this->service = false;
            $this->service_path = '';
        }
    }
    /**
     * Возвращает запрос
     * @return array
     */
    public function get_request()
    {
        return $this->request;
    }

    /**
     * Возвращает абсолютный путь сервиса
     * @return string
     */
    public function get_service_path()
    {
        return $this->service_path;
    }

    /**
     * Проверяет схему запроса, если есть ошибки выбрасывается исключение
     * @return void
     * @throws Выбрасывает 'Ошибка схемы данных.' если запрос не прошел проверку
     */
    public function check_request_schema()
    {
        $check = (
            $this->request_method == 'POST' &&
            isset($this->request['method']) &&
            isset($this->request['params']) &&
            is_array($this->request['params']) &&
            $this->service
        );

        if (!$check)
        {
            throw new Exception('', 101);
        }
    }

    /**
     * Проверка авторизации, если не регистрация пользователя
     * @return void
     * @throws Выбрасывает 'Не указан token.' или 'Доступ запрещен.'
     */
    public function check_authorization()
    {
        if ($this->service == 'auth')
        {
            return;
        }

        if (!isset($this->request['token']))
        {
            throw new Exception('', 102);
        }
        else
        {
            $this->token_id = $this->check_user_access($this->request['token']);
        }

        if (!$this->token_id)
        {
            throw new Exception('', 103);
        }

    }

    /**
     * Проверка наличия файла сервиса
     * @return void
     * @throws Выбрасывает 'Cервис не найден.'
     */
    public function check_service_exist()
    {
        if (!file_exists($this->service_path))
        {
            throw new Exception('', 104);
        }
    }

    /**
     * Обработчик ошибок
     * @param int $code
     * @return void
     */
    public function handle_error($code)
    {
        if (!in_array($code, array_keys($this->errors)))
        {
            $code = 199;
            echo json_encode(array('error' => $this->errors[$code], 'code' => $code)) ;
        }
        else
        {
            echo json_encode(array('error' => $this->errors[$code], 'code' => $code)) ;
        }
    }

    /**
     * Отдача результата, вызом метода логирования
     * @param array $response
     * @return void
     */
    public function response($response)
    {
        $this->log($response);
        echo json_encode($response);
    }

    /**
     * Возвращает id пользователя если доступ разрешен или null
     * @param string $token
     * @return bool|int
     */
    public function check_user_access($token)
    {
        $response = $this->DB->GetOne('SELECT id FROM api_users WHERE token = ? AND NOW() < date_valid_to', array($token));

        return $response ?: false;
    }

    /**
     * Возвращает токен и дату до которой он действует
     * @return array
     */
    public function user_registration()
    {
        $token = md5(uniqid());
        $this->DB->Execute('INSERT INTO api_users (id, token, date_register, date_valid_to) VALUES ("", ?, NOW(), NOW() + INTERVAL 5 YEAR)', array($token));

        return $this->DB->GetRow('SELECT token, date_valid_to AS date FROM api_users WHERE id = ?', array($this->DB->Insert_ID()));
    }

    /**
     * Логирование запросов к API
     * @param array $response
     * @return void
     */
    public function log($response)
    {
        if ($this->service == 'auth')
        {
            $this->token_id = null;
        }

        $this->DB->Execute('INSERT INTO api_log (id, token_id, date, ip, request, response, service) VALUES ("", ?, NOW(), ?, ?, ?, ?)', array($this->token_id, get_ip(), json_encode($this->request), json_encode($response), $this->service));

    }

    /**
     * Информация о поставщике
     * @param int $id
     * @return array
     */
    public function get_user_info($id)
    {
        return $this->DB->GetRow("
                SELECT
                  users.title,
                  users.address,
                  users.phone2 AS phone,
                  users.email,
                  users.address,
                  users.www,
                  users.cnt AS user_offers
                FROM
                  users
                WHERE users.id = ?
                  AND users.status = 1
                        ", array($id));
    }

    /**
     * Информация о поставщике
     * @param array $list
     * @return array
     */
    public function count_components($list)
    {
        $result = array(
            'components' => array(),
            'users' => 0
        );
        $components_count = array();

        foreach ($list as $component)
        {
            if (!empty($components_count[$component['comp_title']]))
            {
                $components_count[$component['comp_title']]++;
            }
            else
            {
                $components_count[$component['comp_title']] = 1;
            }
        }

        $components = array();
        foreach (array_keys($components_count) as $component)
        {
            $components[] = $this->DB->qstr($component);
        }
        unset($components_count);

        if (!empty($components))
        {
            $_result = $this->DB->GetAll(
                'SELECT base_main.comp_title, COUNT(DISTINCT(base_main.user_id)) AS cnt
                FROM base_main INNER JOIN users ON users.id = base_main.user_id
                WHERE users.status = 1 AND base_main.comp_title IN(' . implode(',', $components) . ')
            GROUP BY base_main.comp_title
            ORDER BY cnt DESC'
            );

            if (!empty($_result))
            {
                foreach ($_result as $k => $row)
                {
                    $result['components'][$row['comp_title']] = $row['cnt'];
                    unset($_result[$k]);
                }
                $result['users'] = (int)$this->DB->GetOne(
                    'SELECT COUNT(DISTINCT(base_main.user_id))
                    FROM base_main INNER JOIN users ON users.id = base_main.user_id
                    WHERE users.status = 1 AND base_main.comp_title IN(' . implode(',', $components) . ')'
                );
            }
        }

        return $result;
    }

    /**
     * Общее кол-во компонентов из настроек
     * @return array
     */
    public function get_total_info()
    {
        $result = $this->DB->GetRow("SELECT value FROM settings WHERE var = 'catalog:main_cnt'");

        return array(
            'total' => $result['value']
        );
    }
}
