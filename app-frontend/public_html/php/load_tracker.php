<?php
define('HITLIMIT', 30);
define('TIMELIMIT', 20);
define('BANTIME', 1800);
define('FLUSHTIME', 100);

$dos_deny = 0;

if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
{
    $addr = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
elseif (isset($_SERVER['REMOTE_ADDR']))
{
    $addr = $_SERVER['REMOTE_ADDR'];
}

if (isset($addr))
{
    $rs = $DB_master->GetRow('SELECT * FROM load_tracking WHERE ip = ?', array($addr));

    if ($rs)
    {
	    $cnt = $rs['cnt'];
	    $fhit = $rs['first_hit'];
    }
    else
    {
        $cnt = 0;
    }

    if ($cnt == 0)
    {
	    if ($DB_master->GetOne('SELECT COUNT(*) FROM load_tracking WHERE ip = ?', array($addr)))
	    {
    		$DB_master->Execute('DELETE FROM load_tracking WHERE ip = ?', array($addr));
    	}

	    $rec['ip'] = $addr;
	    $rec['first_hit'] = timer();
	    $rec['cnt'] = 1;
	    $DB_master->AutoExecute('load_tracking', $rec, 'INSERT');
    }
    elseif ($cnt > 0 && $cnt < HITLIMIT)
    {
	    $rec['cnt'] = $cnt + 1;
        $DB_master->AutoExecute('load_tracking', $rec, 'UPDATE', 'ip="' . $addr . '"');
    }
    elseif ($cnt >= HITLIMIT)
    {
	    if ((timer() - $fhit) < TIMELIMIT)
	    {
            if (preg_match('/googlebot\.com$/', gethostbyaddr($addr)) ||
            	preg_match('/yahoo\.net$/', gethostbyaddr($addr)) ||
                preg_match('/yandex\.com$/', gethostbyaddr($addr)) ||
                preg_match('/yandex\.ru$/', gethostbyaddr($addr)) ||
                preg_match('/yandex\.net$/', gethostbyaddr($addr)) ||
                preg_match('/msn\.com$/', gethostbyaddr($addr)) ||
                preg_match('/mail\.ru$/', gethostbyaddr($addr)) )
            {
                $DB_master->Execute('DELETE FROM load_tracking WHERE ip = ?', array($addr));
                $dos_deny = 0;
            }
            else
            {
                $rec['cnt'] = -1;
                $rec['first_hit'] = timer();
                $DB_master->AutoExecute('load_tracking', $rec, 'UPDATE', 'ip="' . $addr . '"');
                $dos_deny = 1;
            }
	    }
	    else
	    {
		    $DB_master->Execute('DELETE FROM load_tracking WHERE ip = ?', array($addr));
	    }
    }

    elseif($cnt == -1)
    {
	    if((timer() - $fhit) < BANTIME)
	    {
		    $rec['first_hit'] = timer();
		    $DB_master->AutoExecute('load_tracking', $rec, 'UPDATE', 'ip="' . $addr . '"');
		    $dos_deny = 1;
	    }
        else
        {
            $DB_master->Execute('DELETE FROM load_tracking WHERE ip = ?', array($addr));
        }
    }

    $t2 = time() - FLUSHTIME;
    $DB_master->Execute('DELETE FROM load_tracking WHERE cnt > ? AND first_hit < ?', array(0, $t2));

}