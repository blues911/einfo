<?php

/**
 * Аналог js функции encodeURIComponent.
 * @param $str
 * @return string
 */
function smarty_modifier_decodeURIComponent($str)
{
    return decodeURIComponent($str);
}