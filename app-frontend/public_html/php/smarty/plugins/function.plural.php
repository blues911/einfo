<?php

/**
 * Smarty {plural} function plugin
 *
 * Type:     function<br>
 * Name:     plural<br>
 * Purpose:  print plural word form
 * @param array parameters
 * @param Smarty
 * @return string|null
 */

function smarty_function_plural($params, &$smarty)
{
    $n = isset($params['num']) ? (int)$params['num'] : 0;
    $forms = explode("|", $params['words']);
    if (count($forms) == 3)
    {
        $plural = (($n % 10 == 1 && $n % 100 != 11) ? 0 : (($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)) ? 1 : 2));
        return $forms[$plural];
    }
    else
    {
        return null;
    }
}


?>
