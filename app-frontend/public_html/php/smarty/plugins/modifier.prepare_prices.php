<?php
/*
 * Smarty plugin
 * -----------------------------
 * Файл:	modifier.prepare_prices.php
 * Тип:		modifier
 * Имя:		prepare_prices
 * Назначение: преобразует цены из json
 * ------------------------------
 */

function smarty_modifier_prepare_prices($data)
{
    require_once(SMARTY_PLUGINS_DIR . 'modifier.format_price.php');

    $result = '';
    $prices = json_decode($data, true);

    if (!empty($prices))
    {
        $prices_total = count($prices);
        $i = 1;

        if ($prices_total == 1)
        {
            foreach ($prices as $price)
            {
                if ($price['p'] > 0)
                {
                    $p_cur = smarty_modifier_format_price($price['p']) . ' руб.';
    
                    $result = $p_cur;
                }
            }
        }
        else
        {
            foreach ($prices as $price)
            {
                if ($price['p'] > 0)
                {
                    $p_qty = ($i == $prices_total) ? $price['n'] . '+ шт: ' : $price['n'] . ' шт: ';
                    $p_cur = smarty_modifier_format_price($price['p']) . ' руб.';
    
                    $result .= '<div class="multiple-price"><span>' . $p_qty . '</span>' . $p_cur . '</div>';
                    $i++;
                }
            }
        }
    }

    if (empty($result)) $result = '&mdash;';

    return $result;
}

?>
