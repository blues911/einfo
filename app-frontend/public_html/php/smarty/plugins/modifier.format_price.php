<?php
/*
 * Smarty plugin
 * -----------------------------
 * ����:	modifier.format_price.php
 * ���:		modifier
 * ���:		format_price
 * ����������:	��������������� ����� ���
 * ------------------------------
 */

function smarty_modifier_format_price($num)
{
    $price = number_format($num, 2, '.', ' ');
    $price = preg_replace('/\.00$/', '', $price);

    return $price;
}

?>
