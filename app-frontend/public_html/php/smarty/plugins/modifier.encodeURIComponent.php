<?php

/**
 * Аналог js функции encodeURIComponent.
 * @param $str
 * @return string
 */
function smarty_modifier_encodeURIComponent($str)
{
    return encodeURIComponent($str);
}