<?php
/*
 * Smarty plugin
 * -----------------------------
 * Файл:	modifier.format_file_size.php
 * Тип:		modifier
 * Имя:		format_file_size
 * Назначение:	форматированный размера файла
 * ------------------------------
 */

function smarty_modifier_format_file_size($size)
{
    if ($size < 1024)
    {
        $new_size = $size . ' байт';
    }
    else if ($size <= 1048576)
    {
        $new_size = round($size / 1024, 0) . ' Кб';
    }
    else if ($size <= 1073741824)
    {
        $new_size = round($size / 1048576, 2) . ' Мб';
    }
    else
    {
        $new_size = round($size / 1073741824, 3) . ' Гб';
    }
    
    return $new_size;
}

?>