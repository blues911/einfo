{if $pager.show_pages}
<div class="pager_pages">
  {if !empty($pager.prev)}
    <a class="prev" href="{$pager.prev|escape}">&larr; предыдущая</a>
  {else}
      <span class="prev">&larr; предыдущая</span>
  {/if}
  
  {if !empty($pager.first)}
    <a class="item" href="{$pager.first|escape}">1</a>

    <span class="separator">...</span>
  {/if}

  {foreach $pager.pages as $item}
      {if $item.num == $pager.current}
          <span class="item selected">{$item.num|escape}</span>
      {else}
          {if $item.num == 1}
              <a class="item" href="{$item.link|escape}">{$item.num|escape}</a>
          {else}
              <a class="item" href="{$item.link|escape}">{$item.num|escape}</a>
          {/if}
      {/if}
  {/foreach}
  
  {if !empty($pager.last)}
    <span class="separator">...</span>
    
    <a class="item" href="{$pager.last|escape}">{$pager.total|escape}</a>
  {/if}
    
  {if !empty($pager.next)}
    <a class="next" href="{$pager.next|escape}">следующая &rarr;</a>
  {else}
      <span class="next">&larr; следующая</span>
  {/if}
    <div class="clear"></div>
</div>
{/if}