<div class="wrap">
    <h1>Поставщики</h1>
    <div class="all_info">Всего поставщиков в базе данных — {$all_users|escape}</div>
</div>
<div class="table_wrap">
    <table class="simple">
        <thead>
        <tr>
            <th>Поставщик</th>
            <th style="width: 12%">Город</th>
            <th style="width: 10%" class="count">Позиций в прайсе</th>
            <th style="width: 17%" class="last_upd">Обновление прайса</th>
            <th style="width: 190px;">Контакты</th>
        </tr>
        </thead>
        <tbody>

      {foreach $users as $item}
        <tr>
            <td>
                <a href="/users/id-{$item.id|escape}.html">{$item.title|escape}</a>
                <span class="about">{$item.about|truncate:120:"&#8230;"|escape}</span>
            </td>
            <td class="city">{$item.city|default:"&mdash;"|escape}</td>
            <td>
                {if $item.cnt != 0}
                    {if $item.allow_download_price}
                        <a href="/users/price_{$item.id|escape}.txt">{$item.cnt|escape|format_price}</a>
                    {else}
                        {$item.cnt|escape|format_price}
                    {/if}
                {else}
                    0
                {/if}
            </td>
            <td class="price_upd">{$item.price_date|escape}</td>
            <td class="contacts">
                {if !empty($item.phone1) || !empty($item.phone2)}
                    {if !empty($item.phone1)}
                        {$item.phone1|escape}
                        {if !empty($item.phone1_ext)}
                            доб. {$item.phone1_ext|escape}
                        {/if}
                    {/if}
                    {if !empty($item.phone1) && !empty($item.phone2)}<br>{/if}
                    {if !empty($item.phone2)}
                        {$item.phone2|escape}
                        {if !empty($item.phone2_ext)}
                            доб. {$item.phone2_ext|escape}
                        {/if}
                    {/if}
                {/if}
            </td>
        </tr>
      {/foreach}

        </tbody>
    </table>
</div>

{include file="pager.tpl"}
