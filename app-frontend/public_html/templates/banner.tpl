{if $banner}
    {if $banner.url}
        <a href="{$banner.url|escape}" target="_blank"><img src="https://www.einfo.ru/uploads/banners/{$banner.banner|escape}" width="{$banner.size_x|escape}" height="{$banner.size_y|escape}" border="0"></a>
    {else}
        <img src="https://www.einfo.ru/uploads/banners/{$banner.banner|escape}" width="{$banner.size_x|escape}" height="{$banner.size_y|escape}" border="0">
    {/if}
{/if}
