<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Телефон</title>
    <script src="/media/jquery/jquery.js" type="text/javascript"></script>
</head>
<body>
    <a id="phone" href="tel:{$phone|escape}">{$phone|escape}</a>

    <script>
        $(document).on('ready', function()
        {
            window.location.href = 'tel:{$phone|escape}';
        });
    </script>
</body>
</html>