{* Smarty *}
<script src="/media/js/settings.js" type="text/javascript"></script>

<div class="wrap">
    <h1>Черный список</h1>
    <div id="settings_company">

        {if $save_succes}
            <div id="save_sucess" class="system_message" style="width: 575px;">
                <p>
                    <strong>Настройки успешно сохранены</strong>
                    <br>Предложения от выбранных поставщиков будут исключены из вашего поиска.
                </p>
            </div>
        {/if}

        <div class="settings_items">

            <form action="/account/black_list/" method="post" id="settings_form">

                <input type="hidden" name="form_settings" value="1">


                <div class="buttons settings">
                    <a class="button settings_form_submit" href="/account/settings/"><span>Сохранить изменения</span></a>
                    <a class="with_icon dotted clr" href="#"><span class="link">Очистить форму</span><span class="icon"></span></a>

                    <div class="clear"></div>
                </div>

                <p class="settings_block">Исключить из результатов поиска:</p>
                {$counter = 1|escape}
                {foreach $users_all as $column}

                    <ul class="column{if $column@last} last{/if}">

                        {foreach $column as $item}
                            {if in_array($item.id, $users_white_list)}{$disable = true}{else}{$disable = false}{/if}

                            <li>
                                <label for="user_{$item.id|escape}" class="custom_label {if $item.checked}checked{/if}" {if $disable}style="opacity: 0.5" title="Поставщик уже находится в белом списке"{/if}>
                                    <span class="custom_checkbox"></span>
                                    <span class="company" {if $disable}title="Поставщик уже находится в белом списке"{/if}>{$item.title|escape}</span>
                                    <input type="checkbox" class="hidden_checkbox" id="user_{$item.id|escape}" name="search_users_disallow[{$item.id|escape}]" value="1" tabindex="{$counter|escape}" {if $item.checked}checked{/if} {if $disable}disabled{/if}/>
                                </label>
                                <input type="checkbox" name="previous_condition[{$item.id}]" value="1" {if $item.checked}checked{/if} style="display: none"/>
                            </li>
                            {$counter = $counter + 1|escape}

                        {/foreach}

                    </ul>

                {/foreach}
                <div class="clear"></div>
                <div class="buttons settings">
                    <a class="button settings_form_submit" href="/account/settings/"><span>Сохранить изменения</span></a>
                    <a class="with_icon dotted clr" href="#"><span class="link">Очистить форму</span><span class="icon"></span></a>

                    <div class="clear"></div>
                </div>

            </form>

        </div>
    </div>
</div>
