{* Smarty *}

<h1>Профиль</h1>

<p id="profile_error" style="color: red;">{$profile_error|escape}</p>
<p id="profile_info" style="color: green;">{$profile_info|escape}</p>

<div style="margin-top: 25px;">
    <form id="profile_form" class="form-horizontal" action="." style="width: 300px;" method="post">
        <input type="hidden" name="action" value="edit">
        <div class="control-group">
            <label class="control-label" for="profile_name">Имя</label>
            <div class="controls">
                <input type="text" id="profile_name" name="name" value="{$client.name|escape}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="profile_email">Электронная почта</label>
            <div class="controls">
                <input type="text" id="profile_email" name="email" value="{$client.email|escape}" disabled>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="profile_phone">Телефон</label>
            <div class="controls">
                <input type="text" id="profile_phone" name="phone" value="{$client.phone|escape}">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="profile_password">Пароль</label>
            <div class="controls">
                <input type="password" id="profile_password" name="password" style="display: none;">
                <a href="#" id="change_password" class="dotted"
                   style="position: relative; top: 4px;"
                   onclick="$('#change_password').hide(); $('#profile_password').show();">
                    Сменить пароль
                </a>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="profile_details">Реквизиты</label>
            <div class="controls">
                <textarea style="width: 600px; height: 150px;" id="profile_details" name="details">{$client.details|escape}</textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="profile_details_in_order"></label>
            <div class="controls" style="width: 600px;">
                <input type="checkbox" id="profile_details_in_order" name="details_in_order" value="1" {if $client.details_in_order}checked="checked"{/if}>
                <span style="position: relative; top: 3px;">Включать реквизиты в комментарий к заявке</span>
            </div>
        </div>
        <a class="button" style="margin-top: 0;" id="client_login_submit" href="#" onclick="profile_form.submit();"><span>Сохранить</span></a>
    </form>
</div>