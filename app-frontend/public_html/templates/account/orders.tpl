{if $view}
  {if $basket}

    <div class="wrap">
        <h1>Заявка #{$basket.id|escape} <span>от {$basket.date|escape}</span></h1>
        <div class="all_info">Всего компонентов — {if $basket_items}{$basket_items|@count|escape}{else}0{/if}</div>
    </div>
    <div class="table_wrap">
        <table class="simple">
            <thead>
            <tr>
                <th style="width: 40%;">Наименование</th>
                <th class="right_align">Кол-во</th>
                <th>Продавец</th>
                <th class="right_align nobr">Цена</th>
            </tr>
            </thead>

            {foreach $basket_items as $item}
                <tr>
                    <td class="comp">
                        {$item.title|escape}
                        <span>{$item.descr|escape}</span>
                        {if $item.manuf}<span>{$item.manuf|escape}</span>{/if}

                    </td>
                    <td>{$item.rcount|escape}</td>
                    <td class="conpany">
                        <a href="/users/id-{$item.user_id|escape}.html">{$item.user_title|escape}</a>
                        {if $item.user_phone}<span class="phone">{$item.user_phone|escape}</span>{/if}
                    </td>
                    <td class="nobr">
                        {$item.prices|prepare_prices|unescape:'html'}
                    </td>
                </tr>
            {/foreach}

            </tbody>
        </table>
    </div>

  {else}

    <div class="wrap">
        <h1 style="width: 800px;">Заявки</h1>
        <div class="column_right"></div>
        <div class="column_left">
            <div class="static_text" >

                <div class="system_message">
                    <h2>Код валидации неверен!</h2>
                </div>

            </div>
        </div>
    </div>

  {/if}

{elseif $confirm}

    <div class="wrap">
        <h1 style="width: 800px;">Заявки</h1>
        <div class="column_right"></div>
        <div class="column_left">
            <div class="static_text" >

                {if $confirm == "yes"}

                    <div class="system_message" style="width: 90%;">
                        <h2>Заявка подтверждена!</h2>
                        <p>Ваш запрос успешно отправлен поставщикам.<br>Для просмотра статуса заявки воспользуйтесь <a href="/account/orders/?view={$order_id|escape}">данной ссылкой</a>.</p>
                        <p>Историю ваших запросов доступна по этой <a href="/account/orders/">ссылке</a></p>
                    </div>

                    {else}

                    <div class="system_message" style="width: 60%;">
                        <p><strong>Заявка была подтверждена ранее</strong></p>
                    </div>

                {/if}

            </div>
        </div>
    </div>

{elseif $orders}

<div class="wrap">
    <h1>Мои заявки</h1>
    <div class="all_info">Всего заявок — {$orders|@count|escape}</div>
</div>
<div class="table_wrap">
    <table class="simple">
        <thead>
        <tr>
            <th>Наименование</th>
            <th class="right_align">Дата заявки</th>
            <th class="right_align">Кол-во компонентов</th>
        </tr>
        </thead>
        <tbody>

{foreach $orders as $item}
        <tr>
            <td><a href="/account/orders/?view={$item.id|escape}">#{$item.id|escape}</a></td>
            <td><span class="date">{$item.date|escape}</span></td>
            <td>{$item.comp_all|escape}</td>
        </tr>
{/foreach}


        </tbody>
    </table>
</div>


{else}

    <div class="wrap">
        <h1 style="width: 800px;">Заявки</h1>
        <div class="column_right"></div>
        <div class="column_left">
            <div class="static_text" >
                <div class="system_message">
                    <h2>Заявок не найдено</h2>
                </div>
            </div>
        </div>
    </div>

{/if}
