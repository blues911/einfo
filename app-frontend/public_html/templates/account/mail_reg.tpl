{* Smarty *}

<html>

<p>Вы успешно зарегестрировались в системе Einfo.</p>

<p>
    <strong>Ваш логин:</strong> {$email|escape}<br />
    <strong>Ваш пароль:</strong> {$password|escape}
</p>

<p>
    Перейдите по этой ссылке, чтобы активировать свой аккаунт:<br />
    <a href="{$url|escape}">{$url|escape}</a><br />
    <br />
    После активации рекомендуем вам сменить пароль.
</p>

</html>