<script src="/media/js/settings.js" type="text/javascript"></script>

<div class="wrap">
    <h1>Личные настройки</h1>
    <div id="settings_company">

{if $save_success}
    <div id="save_sucess" class="system_message" style="width: 575px;">
        <p><strong>Настройки успешно сохранены</strong></p>
    </div>
{/if}

        <div class="settings_items">
            <form action="/account/settings/" method="post" id="settings_form">

                <input type="hidden" name="form_settings" value="1">


                <ul class="column" style="width: auto;">
                    <li>
                        <h2 style="margin-left: 15px;">Результаты поиска</h2>
                    </li>
                    <li>
                        <label for="individual_buyers" class="custom_label {if $individual_buyers}checked{/if}">
                            <span class="custom_checkbox"></span>
                            <span class="company">Исключить из&nbsp;поиска поставщиков, работающих только с&nbsp;юрлицами</span>
                            <input type="checkbox" class="hidden_checkbox" id="individual_buyers" name="individual_buyers" value="1" {if $individual_buyers}checked{/if}/>
                        </label>
                    </li>
                    <li style="margin-top: 30px;">
                        <h2 style="margin-left: 15px;">Интерфейс поисковой выдачи</h2>
                    </li>
                    <li>
                        <div>
                            <label>
                                <input type="radio" class="hidden_checkbox" name="interface" value="1" {if $interface == 1}checked{/if} />
                                &nbsp;<strong>Как интернет-магазин</strong> &mdash; наполнение корзины компонентами от разных поставщиков и отправка запросов в рамках единой заявки
                            </label>
                            <label>
                                <input type="radio" class="hidden_checkbox" name="interface" value="0" {if $interface == 0}checked{/if} />
                                &nbsp;<strong>Запрос в один клик</strong> &mdash; отправка запроса конкретному поставщику на один компонент
                            </label>
                        </div>
                    </li>
                </ul>
                <div class="clear"></div>

                <div class="buttons settings">
                    <a class="button settings_form_submit" href="/account/settings/"><span>Сохранить изменения</span></a>
                    <div class="clear"></div>
                </div>

            </form>

        </div>
    </div>
</div>