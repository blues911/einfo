{* Smarty *}

<table style="width: 100%;">
    <tr>
        <td style="width: 200px;">

            <!-- Menu -->

            <div class="account__menu{if $module == 'orders'} account__menu--selected{/if}">
                <a href="/account/orders/"><span>Мои заявки</span></a>
            </div>

            <div class="account__menu{if $module == 'settings'} account__menu--selected{/if}">
                <a href="/account/settings/"><span>Настройки</span></a>
            </div>

            <div class="account__menu{if $module == 'black_list'} account__menu--selected{/if}">
                <a href="/account/black_list/"><span>Черный список</span></a>
            </div>

            <div class="account__menu{if $module == 'white_list'} account__menu--selected{/if}">
                <a href="/account/white_list/"><span>Белый список</span></a>
            </div>

            <div class="account__menu{if $module == 'profile'} account__menu--selected{/if}">
                <a href="/account/profile/"><span>Профиль</span></a>
            </div>

            <!---->

        </td>
        <td style="padding-top: 13px;">

            <!-- Content -->

            {$account_content nofilter}

            <!---->

        </td>
    </tr>
</table>
