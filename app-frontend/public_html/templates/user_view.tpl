<div class="wrap">
    <h1 style="width: 800px;">{$user.title|escape}</h1>
</div>


<div class="column_right">

{if $user.cnt}
    <div id="price_block">

        {if $user.allow_download_price}
            <a href="/users/price_{$user.id|escape}.txt" style="font-weight: bold;">
                Прайс-лист компании
            </a>
        {else}
            <span style="font-weight: bold;">
                Прайс-лист компании
            </span>
        {/if}

        <span class="update">от {$user.price_date|escape}</span>
        <span class="positions">&ndash; {$user.cnt|escape|format_price} {plural num=$user.cnt words="позиция|позиции|позиций"}</span>
    </div>
{/if}

{if $last_vacancy}
    <div id="vacancy_company">
        <a class="all_vacancy" href="/vacancy/user-{$user.id|escape}.html">Вакансии</a>
        <ul>

          {foreach $last_vacancy as $item}
            <li><a href="/vacancy/id-{$item.id|escape}.html">{$item.speciality|escape}</a></li>
          {/foreach}

        </ul>
    </div>
{/if}

</div>

<div class="column_left">
    <div id="company_card">
        <div class="company_info">

            <div class="line">
                <div class="title">Описание</div>
                <div class="val">{$user.about|escape|nl2br}</div>
            </div>

            {if $user.www != ''}
                <div class="line">
                    <div class="title">Сайт</div>
                    <div class="val"><a href="{$user.www|escape}" target="_blank">{$user.www|escape}</a></div>
                </div>
            {/if}

            {if $user.country_id != 0}
                <div class="line">
                    <div class="title">Страна</div>
                    <div class="val">{$user.country|escape}</div>
                </div>
            {/if}

            {if $user.city_id != 0}
                <div class="line">
                    <div class="title">Город</div>
                    <div class="val">{$user.city|escape}</div>
                </div>
            {/if}

            {if $user.address != ''}
                <div class="line">
                    <div class="title">Адрес</div>
                    <div class="val">{$user.address|escape}</div>
                </div>
            {/if}

            {if !empty($user.phone1) || !empty($user.phone2)}
                <div class="line">
                    <div class="title">Телефон</div>
                    <div class="val">
                        {if !empty($user.phone1)}
                            {$user.phone1|escape}
                            {if !empty($user.phone1_ext)}
                                доб. {$user.phone1_ext|escape}
                            {/if}
                        {/if}
                        {if !empty($user.phone1) && !empty($user.phone2)}<br>{/if}
                        {if !empty($user.phone2)}
                            {$user.phone2|escape}
                            {if !empty($user.phone2_ext)}
                                доб. {$user.phone2_ext|escape}
                            {/if}
                        {/if}
                    </div>
                </div>
            {/if}

            {if $user.fax != ''}
                <div class="line">
                    <div class="title">Факс</div>
                    <div class="val">{$user.fax|escape}</div>
                </div>
            {/if}

            {if $user.email != ''}
                <div class="line">
                    <div class="title">Электронная почта</div>
                    <div class="val"><a href="mailto:{$user.email|escape}">{$user.email|escape}</a></div>
                </div>
            {/if}

            {if $user.icq != ''}
                <div class="line">
                    <div class="title">ICQ</div>
                    <div class="val">{$user.icq|escape}</div>
                </div>
            {/if}

            {if $user.skype != ''}
                <div class="line">
                    <div class="title">Skype</div>
                    <div class="val">{$user.skype|escape}</div>
                </div>
            {/if}

            {if $user.viber != ''}
                <div class="line">
                    <div class="title">Viber</div>
                    <div class="val">{$user.viber|escape}</div>
                </div>
            {/if}

            {if $user.whatsapp != ''}
                <div class="line">
                    <div class="title">WhatsApp</div>
                    <div class="val">
                        {if $user.whatsapp|strstr:"http"}
                            <a href="{$user.whatsapp|escape}" target="_blank">{$user.whatsapp|escape}</a>
                        {else}
                            {$user.whatsapp|escape}
                        {/if}
                    </div>
                </div>
            {/if}

            {if $user.telegram != ''}
                <div class="line">
                    <div class="title">Telegram</div>
                    <div class="val">
                        {if $user.telegram|strstr:"http"}
                            <a href="{$user.telegram|escape}" target="_blank">{$user.telegram|escape}</a>
                        {else}
                            {$user.telegram|escape}
                        {/if}
                    </div>
                </div>
            {/if}

            {if $user.facebook != ''}
                <div class="line">
                    <div class="title">Facebook</div>
                    <div class="val">
                        {if $user.facebook|strstr:"http"}
                            <a href="{$user.facebook|escape}" target="_blank">{$user.facebook|escape}</a>
                        {else}
                            <a href="https://facebook.com/{$user.facebook|escape}" target="_blank">https://facebook.com/{$user.facebook|escape}</a>
                        {/if}
                    </div>
                </div>
            {/if}

            {if $user.vk != ''}
                <div class="line">
                    <div class="title">ВК</div>
                    <div class="val">
                        {if $user.vk|strstr:"http"}
                            <a href="{$user.vk|escape}" target="_blank">{$user.vk|escape}</a>
                        {else}
                            <a href="https://vk.com/{$user.vk|escape}" target="_blank">https://vk.com/{$user.vk|escape}</a>
                        {/if}
                    </div>
                </div>
            {/if}

            {if $user.instagram != ''}
                <div class="line">
                    <div class="title">Instagram</div>
                    <div class="val">
                        {if $user.instagram|strstr:"http"}
                            <a href="{$user.instagram|escape}" target="_blank">{$user.instagram|escape}</a>
                        {else}
                            <a href="https://instagram.com/{$user.instagram|escape}" target="_blank">https://instagram.com/{$user.instagram|escape}</a>
                        {/if}
                    </div>
                </div>
            {/if}

                <div class="line">
                    <div class="title">Работает с физ. лицами</div>
                    <div class="val">{if $user.individual_buyers}Да{else}Нет{/if}</div>
                </div>

            {if $user.min_order_amount}
                <div class="line">
                    <div class="title">Мин. сумма заказа</div>
                    <div class="val">{$user.min_order_amount|escape}</div>
                </div>
            {/if}

            {if $user.register_date != ''}
                <div class="line">
                    <div class="title">Дата регистрации в системе</div>
                    <div class="val">{$user.register_date|escape}</div>
                </div>
            {/if}
        </div>
    </div>
</div>

{if $last_news}
<div id="news_feed">
    <a href="/news/user-{$user.id|escape}.html" class="news_company">Новости {$user.title|escape}</a>

    {foreach $last_news as $item}
    <div class="item{if $item.img == ""} no_img{/if}">
      {if $item.img != ""}
        <a href="/news/id-{$item.id|escape}.html"><img src="/uploads/news/{$item.img|escape}" alt="{$item.title|escape}"></a>
      {/if}

        <div class="text">
            <span class="date">{$item.date|escape}</span>
            <a href="/news/id-{$item.id|escape}.html" class="link">{$item.title|escape}</a>
        </div>
    </div>
    {/foreach}

</div>
{/if}

{if $user_docs}
<div id="user_docs">
    <div class="docs">Загруженные документы {$user.title|escape}</div>

    {if $user_docs.imgs}
        {foreach $user_docs.imgs as $item}
            <div class="item img">
                    <a href="/uploads/users_docs/{$item.file|escape}" target="_blank"><img src="/uploads/users_docs/{$item.file|escape}" width="70" alt="{$item.title|escape}"/></a>
                <div class="text">
                    <span class="file_type">{$item.extension|escape}, {$item.file_size|escape}</span>
                    <a href="/uploads/users_docs/small/{$item.file|escape}"  target="_blank" class="link">{$item.title|escape}</a>
                </div>
            </div>
        {/foreach}
    {/if}

    {if $user_docs.files}
        {foreach $user_docs.files as $item}
            <div class="item file">
                <a href="/uploads/users_docs/{$item.file|escape}" target="_blank"><img src="/media/i/file.png" alt="{$item.title|escape}"></a>
                <div class="text">
                    <span class="file_type">{$item.extension|escape}, {$item.file_size|escape}</span>
                    <a href="/uploads/users_docs/{$item.file|escape}" target="_blank" class="link">{$item.title|escape}</a>
                </div>
            </div>
        {/foreach}
    {/if}
</div>
{/if}