<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="{if !empty($container.robots)}{$container.robots|escape}{else}index, follow{/if}" />
		{if $container.strong_search && !empty($container.page_description)}<meta name="description" content="{$container.page_description|escape}" />{/if}

    {if !empty($container.page_canonical)}
        <link rel="canonical" href="{$container.page_canonical|escape}"/>
    {/if}

    <title>{if !empty($container.page_title)}{$container.page_title|escape} на Einfo.ru{else}Электронные компоненты, информационно-торговая система Einfo.ru{/if}</title>

    <link rel="stylesheet" type="text/css" href="/assets_m.css" />

</head>

<body>
    {$container.content nofilter}

    {literal}
        <script>
            document.addEventListener('DOMContentLoaded', function()
            {
                $(document).on('focus', '.search-input', function()
                {
                    var save_this = $(this);
                    window.setTimeout (function(){
                        save_this.select();
                    },100);
                });

                statistics_enable(this);
            });
        </script>
    {/literal}

    <div style="display: none" hidden>
        {include file='counters.tpl'}
    </div>

    <!-- JS -->
    <script src="/assets_m.js" type="text/javascript"></script>
</body>
</html>