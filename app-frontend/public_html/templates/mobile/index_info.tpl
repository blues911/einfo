<div date-role="page">

    <div role="main" class="ui-content">

        <!-- Лого -->
        <div id="logo"></div>

        <!-- Строка поиска -->
        <div id="mobile-index-search-container">

            <form id="mobile-index-search-form" action="javascript:;" method="get">
                <div id="mobile-index-search" class="input-group">
                    <input type="text" class="form-control search-input" placeholder="Поиск...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <span class="glyphicon glyphicon-search" style="color: #ffffff;"></span>
                        </button>
                    </span>
                </div>
            </form>

            <div id="mobile-index-search-description" style="">
                Поиск среди {floor($settings.catalog.main_cnt / 1000000)|default:1} млн компонентов
            </div>
        </div>

        <!-- Telegram -->
        <div id="mobile-telegram-promo">
            <h2>Бот в Telegram</h2>
            <a href="https://telegram.me/EinfoBot" target="_blank">telegram.me/EinfoBot</a>
        </div>

    </div>

    <!-- Футер -->
    <div id="footer">
        <a class="a-underline toggle-to-desktop" href="https://{$container.host_desktop|escape}/?force_version=true">Полная версия</a><br /><br />
        &copy; ИТС &laquo;Электронные компоненты&raquo;, 1999-{$year|escape}
    </div>

    {literal}
        <script>
            document.addEventListener('DOMContentLoaded', function()
            {
                $(document).on('submit', '#mobile-index-search-form', function()
                {
                    // Поисковый запрос
                    var search_str = $.trim($(this).find('input:first').val());
					
					          einfo_stat('search', search_str);

                    // Поисковый адрес
                    var address = '/search/' + encodeURIComponent(search_str) + '/';

                    $('.ui-loader').show();

                    // Редирект
                    $.ajax({
                        type: 'GET',
                        url: '/ajaxer.php?x=search&set_cookie=1',
                        complete: function()
                        {
                            document.location = address;
                        }
                    });
                });
            });
        </script>
    {/literal}

</div>
