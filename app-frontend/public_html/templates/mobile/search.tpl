<div>
    <div>

        <!-- Хедер -->
        <div id="mobile-header">
            <div id="header-wrap">

                <div id="header-btn-left" data-strong-search="{if !empty($strong_search)}1{else}0{/if}">
                    <a class="nav-button button" href="/">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                </div>

                <form id="mobile-index-search-form" action="javascript:;" method="get" data-comp="{$comp|escape}">

                    <div id="header-input">
                        <input type="text" class="search-input" placeholder="Поиск..." value="{$comp}" />
                    </div>

                    <div id="header-btn-right">
                        <button class="nav-button button" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </div>

                </form>

            </div>
        </div>

        <!-- Поисковая выдача -->
        <div id="content-mobile">

            {if !$result.query || $result.query && $result.query_short}

                <p><strong>Длина поискового запроса должна быть не меньше 3-х символов.</strong></p>

            {elseif $result.query && !$result.search_result && !$result.query_short && $result.query_variant}

                <p>
                    <strong>По результатам поиска ничего не найдено.</strong><br>
                    Попробуйтей другие варианты поиска. Например,
                    <strong><a href="/search/{$query_variant_encode}/">{$query_variant|escape}</a></strong>
                    <span style="color: #a4a4a1">({$query_variant_cnt|escape} {plural num=$query_variant_cnt words="предложение|предложения|предложений"} от поставщиков)</span>
                </p>

            {elseif $result.query && !$result.search_result && !$result.query_short && !$result.query_variant}

                <p>
                    <strong>По результатам поиска ничего не найдено.</strong><br>
                    Попробуйте еще раз, но с другими параметрами.
                </p>

            {else}

                {if $strong_search}
                    <p><strong>Актуальные предложения от поставщиков по компоненту &laquo;{$comp}&raquo;:</strong></p>
                    {foreach from=$result.list item=component name=components}
                        {foreach from=$component.list item=item}

                            <div>
                                <a href="javascript:;" class="mobile-search-user-item" data-users-stat="user_id:{$item.user_id|escape},action:'profile_view',event:'click'">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="user-title">
                                                            <span>{$item.user_title|escape}</span>
                                                        </td>
                                                        <td class="stock-delivery">
                                                            <div style="padding: 0; margin: 0;">
                                                                {if $item.stock_remote != 0 || $item.delivery != 0}
                                                                    <span class="delivery">
                                                                        {if $item.stock_remote > 0}
                                                                            {$item.stock_remote|escape}&nbsp;шт.
                                                                            {if $item.delivery != 0}/{/if}
                                                                        {/if}
                                                                        {if $item.delivery < 0}
                                                                            на&nbsp;заказ
                                                                        {elseif $item.delivery > 0}
                                                                            {$item.delivery|escape}&nbsp;{plural num=$item.delivery words="неделя|недели|недель"}
                                                                        {/if}
                                                                    </span>
                                                                {/if}
                                                                {if $item.stock == -1}
                                                                    <span class="stock">на&nbsp;складе</span>
                                                                {elseif $item.stock > 0}
                                                                    <span class="stock">{$item.stock|escape}&nbsp;шт.</span>
                                                                {/if}
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="description">
                                                            <span>{$item.descr|escape|nl2br}</span>
                                                        </td>
                                                        <td class="price">
                                                            {$item.prices|prepare_prices|unescape:'html'}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </a>

                                <div class="mobile-search-user-info" style="display: none;">

                                    <div class="user-info-header">
                                        <div class="comp-title">{$comp|escape}</div>
                                        <div class="price-date">
                                            <span class="price-status-icon status_{$item.price_status|escape}"></span>
                                            обновлено {$item.price_date|escape}
                                        </div>
                                        <div class="close-btn">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </div>
                                    </div>

                                    <div class="comp-info">
                                        <div class="delivery">
                                            {if $item.stock != 0}
                                                {if $item.stock == -1}
                                                    <div class="stock">НА&nbsp;СКЛАДЕ</div>
                                                {else}
                                                    <div class="stock">{$item.stock|escape}&nbsp;{plural num=$item.stock words="ШТУКА|ШТУКИ|ШТУК"}</div>
                                                {/if}
                                            {/if}
                                            {if $item.stock_remote > 0 || $item.delivery != 0}
                                                <div class="remote">
                                                    {if $item.stock_remote > 0}
                                                        {$item.stock_remote|escape}&nbsp;{plural num=$item.stock_remote words="ШТУКА|ШТУКИ|ШТУК"}
                                                        {if $item.delivery != 0} / {/if}
                                                    {/if}
                                                    {if $item.delivery < 1}
                                                        НА&nbsp;ЗАКАЗ
                                                    {elseif $item.delivery > 0}
                                                        {$item.delivery|escape}&nbsp;{plural num=$item.delivery words="НЕДЕЛЯ|НЕДЕЛИ|НЕДЕЛЬ"}
                                                    {/if}
                                                </div>
                                            {/if}
                                        </div>
                                        <div class="price">
                                            {$item.prices|prepare_prices|unescape:'html'}
                                        </div>
                                    </div>

                                    {if !empty($item.descr)}
                                        <div class="description">
                                            {$item.descr|nl2br|escape}
                                        </div>
                                    {/if}

                                    <div class="user-info">
                                        <div class="country-city">{$item.user_country|escape}, {$item.user_city|escape}</div>
                                        <div class="user-title">{$item.user_title|escape}</div>
                                        <div class="user-www">
                                            <span class="glyphicon glyphicon-home"></span>
                                            <a href="{$item.user_www|escape}" target="_blank" data-users-stat="user_id:{$item.user_id|escape},action:'profile_click_site',event:'click'">{$item.user_www|escape}</a>
                                        </div>
                                        <div class="user-email">
                                            <span class="glyphicon glyphicon-envelope"></span>
                                            <a href="mailto:{$item.user_email|escape}" data-users-stat="user_id:{$item.user_id|escape},action:'profile_click_email',event:'click'">{$item.user_email|escape}</a>
                                        </div>
                                    </div>

                                    <a href="tel:{$item.user_phone_main|escape}" class="user-phone" data-users-stat="user_id:{$item.user_id|escape},action:'profile_click_phone',event:'click'">
                                        <div>
                                            <div class="phone-number">
                                                <span class="glyphicon glyphicon-phone-alt"></span>
                                                <span class="number">
                                                    {if !empty($item.user_phone1)}
                                                        {$item.user_phone1|escape}
                                                        {if !empty($item.user_phone1_ext)}
                                                            доб. {$item.user_phone1_ext|escape}
                                                        {/if}
                                                    {elseif !empty($item.user_phone2)}
                                                        {$item.user_phone2|escape}
                                                        {if !empty($item.user_phone2_ext)}
                                                            доб. {$item.user_phone2_ext|escape}
                                                        {/if}
                                                    {/if}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="accept-btn">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </div>
                                    </a>

                                </div>
                            </div>

                        {/foreach}
                        {if $smarty.foreach.components.first}
                            {break}
                        {/if}
                    {/foreach}
                {else}
                    {foreach from=$result.list item=item}
                        <a class="search-item" href="/{$item.list[0]['comp_title']|encodeURIComponent|escape}/#{$comp|encodeURIComponent}">
                            <table class="mobile-search-item">
                                <tr>
                                    <td>
                                        {$item.list[0]['comp_title']|escape}
                                    </td>
                                    <td class="bg-{$item['color_code']|escape}">
                                        <span>{$item['count']|escape}</span>
                                        <div></div>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    {/foreach}
                {/if}

            {/if}

            <!-- Telegram -->
            <div id="mobile-telegram-promo" class="inner-pages">
                <h2>Бот в Telegram</h2>
                <a href="https://telegram.me/EinfoBot" target="_blank">telegram.me/EinfoBot</a>
            </div>

        </div>

        <!-- Футер -->
        {if !$strong_search}
            <div id="mobile-footer">
                <span>
                    {if $all_items > 0}
                        {$all_items|escape} {plural num=$all_items words="предложение|предложения|предложений"}
                        от {$count_users|escape} {plural num=$count_users words="поставщика|поставщиков|поставщиков"}
                    {else}
                        ничего не найдено
                    {/if}
                </span>
            </div>
        {/if}

    </div>
</div>

{literal}
    <script>
        document.addEventListener('DOMContentLoaded', function()
        {
            $(document).on('submit', '#mobile-index-search-form', function()
            {
                // Поисковый запрос
                var search_str = $.trim($(this).find('input:first').val());

                // Поисковый адрес
                var address = (search_str.match(/^["']|[?*]|["']$/g) ? '/search' : '' ) + '/' + encodeURIComponent(search_str) + '/';

                // Редирект
                $.ajax({
                    type: 'GET',
                    url: '/ajaxer.php?x=search&set_cookie=1',
                    complete: function()
                    {
                        document.location = address;
                    }
                });
            });

            $(document).on('click', '.mobile-search-user-item', function()
            {
                var item = $(this);
                var info = item.siblings('.mobile-search-user-info');
                var opened_info = $('.mobile-search-user-info.opened');
                var closed_item = $('.mobile-search-user-item.closed');

                var old_scroll_top = $(document).scrollTop();
                var offset = $('#mobile-header').height();

                var item_offset = item.offset().top;

                var hidden_height = 0;
                opened_info.each(function()
                {
                    if ($(this).offset().top < item_offset)
                    {
                        hidden_height += $(this).height() - $(this).siblings('.mobile-search-user-item.closed:first').height();
                    }
                });

                opened_info.slideUp(0, function()
                {
                    $(this).removeClass('opened');
                });

                closed_item.slideDown(0, function()
                {
                    $(this).removeClass('closed');
                    var new_scroll_top = old_scroll_top - hidden_height + 15;
                    $(document).scrollTop(new_scroll_top);
                });

                item.slideUp(function()
                {
                    $(this).addClass('closed');
                });

                info.slideDown(function()
                {
                    $(this).addClass('opened');
                    $.scrollTo(info, 400, { offset: { left: 0, top: -offset } } );
                });
            });

            $(document).on('click', '.mobile-search-user-info .close-btn span', function()
            {
                var elem = $(this).closest('.mobile-search-user-info');

                elem.siblings('.mobile-search-user-item').slideDown();
                elem.slideUp();
            });

            // Кнопка назад
            $(document).on('ready', function() {
                var strong_search = Number($('#header-btn-left').attr('data-strong-search'));
                var comp = window.location.hash ? window.location.hash.substr(1) : false;
                var button = $('#header-btn-left a.button:first');

                if (strong_search && comp)
                {
                    button.attr('href', '/search/' + comp + '/');
                }
            });
        });
    </script>
{/literal}