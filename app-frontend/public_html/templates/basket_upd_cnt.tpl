{if $comp_cnt|@count == 1}
    <div class="one_comp">
        <div class="drag_area one">
            <a class="close" href="#"></a>
            <h2>{$comp_cnt[0].comp_title|escape}</h2>
            <p>Требуемое количество для запроса поставщикам</p>
        </div>
        <form id="basket_cnt_upd_form" class="form-horizontal" action="/" style="width: 300px;" method="post">
            <input type="hidden" name="action" value="group_upd"/>

            <div class="control-group">
                <div class="controls">
                    <div class="cnt_line">
                        <a href="#" class="icon minus" data-id="{$comp_cnt[0].comp_hash|escape}" data-action="minus"></a>
                        <input type="text" class="upd_comp_val" data-id="{$comp_cnt[0].comp_hash|escape}" id="basket_cnt_upd_{$comp_cnt[0].comp_hash|escape}" name="basket_cnt_upd[{$comp_cnt[0].comp_title|escape}]" value="{$comp_cnt[0].cnt|default:'1'|escape}">
                        <a href="#" class="icon plus" data-id="{$comp_cnt[0].comp_hash|escape}" data-action="plus"></a>
                    </div>

                    <a class="button" id="upd_submit" href="#"><span>Сохранить</span></a>
                </div>
            </div>

            <div class="clear"></div>
        </form>
    </div>
{elseif $comp_cnt|@count > 1}
    <div class="some_comp">
        <div class="drag_area">
            <a class="close" href="#"></a>
            <h2>Требуемое количество<br> для запроса поставщикам</h2>
            <input type="text" class="all_comp_val custom_placeholder empty" data-placeholder="Кол-во" data-cur="" value="Кол-во">
        </div>
        <form id="basket_cnt_upd_form" class="form-horizontal" action="/" method="post">
            <input type="hidden" name="action" value="group_upd"/>
            <div class="shadow_box">
                <div id="list_container">
                    <div id="list_overflow">

                      {foreach $comp_cnt as $item}
                        <div class="control-group">
                            <label class="control-label" for="basket_cnt_upd_{$item.comp_hash|escape}" title="{$item.comp_title|escape}">{$item.comp_title|escape}</label>
                            <span class="offers">{$item.offer_cnt|escape} {plural num=$item.offer_cnt words="предложение|предложения|предложений"}</span>
                            <div class="controls">
                                <div class="cnt_line">
                                    <a href="#" class="icon minus" data-id="{$item.comp_hash|escape}" data-action="minus"></a>
                                    <input type="text" class="upd_comp_val" data-id="{$item.comp_hash|escape}" id="basket_cnt_upd_{$item.comp_hash|escape}" name="basket_cnt_upd[{$item.comp_title|escape}]" value="{$item.cnt|default:'1'|escape}">
                                    <a href="#" class="icon plus" data-id="{$item.comp_hash|escape}" data-action="plus"></a>
                                </div>
                            </div>
                            <span class="fadeout"></span>
                        </div>
                      {/foreach}

                    </div>
                </div>
                <div class="shadow top"></div>
                <div class="shadow bottom"></div>
            </div>


            <div class="btn_wrap" style="height: 45px; float: right;">
                <a class="button" id="upd_submit" href="#"><span>Сохранить</span></a>
            </div>

            <div class="clear"></div>
        </form>
    </div>
{/if}
