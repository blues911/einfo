<a id="show_social_poll" href="#social_poll_block" style="display: none;" hidden="hidden"></a>
<div id="social_poll_block" style="display: none;">
    <a title="Закрыть" class="close" href="javascript:;"></a>
    <div id="social_poll_body">
        <div id="social_poll_content_1">
            <p>Был ли сайт полезен?</p>
            <ul>
                <li><a href="#" class="button yes"><span>Да</span></a></li>
                <li><a href="#" class="button no"><span>Нет</span></a></li>
            </ul>
        </div>
        <div id="social_poll_content_2">
            <p>Готовы ответить на несколько вопросов?</p>
            <ul>
                <li><a href="#" class="button yes"><span>Да</span></a></li>
                <li><a href="#" class="button no"><span>Нет</span></a></li>
            </ul>
        </div>
        <div id="social_poll_content_3"></div>
    </div>
</div>