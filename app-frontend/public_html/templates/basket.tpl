{if $success}

<div class="wrap">
    <h1>Формирование запроса цены и наличия</h1>
    <div class="column_right"></div>
    <div class="column_left">
        <div class="static_text" >

            {if $mail_type == 'auth'}
                <div class="system_message" style="width: 100%">
                    <h2>Заявка успешно отправлена!</h2>
                    <p>На Вашу электронную почту <strong>{$user_email|escape}</strong> отправлено письмо с информацией по заявке.</p>
                </div>
            {else}
                <div class="system_message" style="width: 100%">
                    <h2>Заявка успешно обработана!</h2>
                    <p>На Вашу электронную почту <strong>{$user_email|escape}</strong> отправлено письмо с ссылкой для активации заявки.</p>
                    <p>Пожалуйста, проверьте свой почтовый ящик и подтвердите заявку, кликнув по присланной в письме ссылке. Без подтверждения заявки она <strong>НЕ БУДЕТ ОТПРАВЛЕНА</strong> поставщику.</p>
                </div>
            {/if}

        </div>
    </div>
</div>


{else}


{* <script src="/media/js/basket.js" type="text/javascript"></script> *}

<div class="wrap">
    <h1>Формирование запроса цены и наличия</h1>
</div>
<div class="table_wrap">
    <div class="buttons settings">
        <a class="button fancy_order" href="#order_block"><span>Отправить заявку</span></a>
        <a class="with_icon dotted clr basket_clr" href="/basket/?clear"><span class="link">Очистить заявку</span><span class="icon"></span></a>

        <div class="clear"></div>
    </div>
    <form id="basket_form" action="#" method="post">
    <table class="simple right basket_list">
        <thead>
        <tr>
            <th class="title">Наименование</th>
            <th style="width: 100px; text-align: center; padding-left: 0; padding-right: 0;">
                <input type="text" class="custom_placeholder" id="all_comp_val" data-placeholder="Кол-во" data-cur="">
            </th>
            <th style="width: 28%" class="company">Продавец</th>
            <th style="width: 12%" class="nobr">Цена</th>
            <th style="width: 5px" class="delete">&nbsp;</th>
        </tr>
        </thead>
        <tbody>


      {foreach $basket as $item}
        <tr data-user_id="{$item.user_id|escape}" data-id="{$item.id|escape}">
            <td class="title">
                {$item.comp_title|escape}
                <span>{$item.descr|escape}</span>
            </td>
            <td style="padding-left: 0; padding-right: 0;">
                <div class="cnt_line">
                    <a href="#" class="icon minus" data-id="{$item.id|escape}" data-action="minus"></a>
                    <input type="text" class="comp_val" data-id="{$item.id|escape}" id="basket_item_cnt_{$item.id|escape}" name="basket_item_cnt[{$item.id|escape}]" value="{$item.basket_cnt|default:'1'|escape}">
                    <a href="#" class="icon plus" data-id="{$item.id|escape}" data-action="plus"></a>
                </div>
            </td>
            <td class="conpany">
                {$item.user_title|escape}
                {if $item.user_phone}<span class="phone">{$item.user_phone|escape}</span>{/if}
            </td>
            <td class="nobr">
                {$item.prices|prepare_prices|unescape:'html'}
            </td>
            <td class="delete"><a href="#" class="icon delete" data-id="{$item.id|escape}" title="Удалить {$item.comp_title|escape} из заявки"></a></td>
        </tr>
      {/foreach}

        </tbody>
    </table>

    </form>

    <div class="buttons settings">
        <a class="button fancy_order" href="#order_block"><span>Отправить заявку</span></a>
        <a class="with_icon dotted clr basket_clr" href="/basket/?clear"><span class="link">Очистить заявку</span><span class="icon"></span></a>

        <div class="clear"></div>
    </div>

    {if $users}
        <div id="companies_block">
            <h2 class="del" style="margin-bottom: 15px;">Удалить предложения поставщиков из заявки</h2>

            <div class="table_wrap" style="padding: 0; border-radius: 8px; border: 2px dashed #e1e0dc; overflow: hidden;">
                <table class="simple delete user_delete" >
                    <tbody>

                    {foreach $users as $item}

                        <tr data-id="{$item.id|escape}">
                            <td style="width: 40%">
                                <a href="/users/id-{$item.id|escape}.html">{$item.title|escape}</a>
                            </td>
                            <td class="city">{if $item.city_id != 0}{$item.city|escape}{else}&mdash;{/if}</td>
                            <td style="width: 40%" class="contacts">{$item.phone|escape}</td>
                            <td style="width: 25px" class="delete"><a href="#" data-user_id="{$item.id|escape}" class="icon delete" title="Удалить все позиции {$item.title|escape} из заявки"></a></td>
                        </tr>

                    {/foreach}

                    </tbody>
                </table>
            </div>
        </div>
    {/if}

    <div id="order_block" style="display: none;">
        <a class="close" href="#"></a>
        <h2>Отправка заявки</h2>

        <div class="system_message" style="width: 415px; padding: 5px 15px 5px 15px; font-size: 13px; margin-top: 10px; line-height: 17px;">
            <p>Эта заявка не накладывает на Вас никаких обязательств. <br>
                Ожидайте предложений от поставщиков и выбирайте лучшие. <br>
                Понравился наш сервис? Возвращайтесь к нам снова!</p>
        </div>



        <form id="order_form" class="form-horizontal" action="#" method="post">

            {if $client.auth}
                <table style="margin-bottom: 25px;">
                    <tr>
                        <td style="padding-right: 20px; font-size: 13px;">
                            <input type="hidden" name="client_id" value="{$client.id|escape}" />
                            <input type="hidden" name="auth_hash" value="{$client.auth_hash|escape}" />

                            <input type="hidden" name="basket_name" value="{$client.name|escape}" />
                            <!-- <div class="control-group">
                                <label class="control-label" for="basket_name">Ваши ФИО <span style="color: #ff0000">*</span></label>
                                <div class="controls">
                                    <input type="text" id="basket_name" name="basket_name" value="{$client.name|escape}" disabled />
                                </div>
                            </div> -->

                            Ваши ФИО
                        </td>
                        <td><strong>{$client.name|escape}</strong></td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <input type="hidden" name="basket_email" value="{$client.email|escape}" />
                            <!-- <div class="control-group">
                                <label class="control-label" for="basket_email">Эл. почта <span style="color: #ff0000">*</span></label>
                                <div class="controls">
                                    <input type="text" id="basket_email" name="basket_email" value="{$client.email|escape}" disabled />
                                </div>
                            </div> -->

                            Эл. почта
                        </td>
                        <td><strong>{$client.email|escape}</strong></td>
                    </tr>

                    {* Если в профиле не указан телефон то показываем поле для ввода *}
                    {if empty($client.phone)}
                        </table>
                        <div class="control-group">
                            <label class="control-label" for="basket_phone">Телефон</label>
                            <div class="controls">
                                <input type="text" id="basket_phone" name="basket_phone" value="">
                            </div>
                        </div>
                    {else}
                        <tr>
                            <td style="font-size: 13px;">
                                <input type="hidden" name="basket_phone" value="{$client.phone|escape}" />
                                <!-- <div class="control-group">
                                    <label class="control-label" for="basket_email">Телефон</label>
                                    <div class="controls">
                                        <input type="text" id="basket_phone" name="basket_phone" value="{$client.phone|escape}" disabled />
                                    </div>
                                </div> -->

                                Телефон
                            </td>
                            <td><strong>{$client.phone|escape}</strong></td>
                        </tr>
                        </table>
                    {/if}
            {else}
                <div class="control-group required">
                    <label class="control-label" for="basket_name">Ваши ФИО <span style="color: #ff0000">*</span></label>
                    <div class="controls">
                        <input type="text" id="basket_name" name="basket_name" value="{$user_info.contact_name|escape}">
                    </div>
                </div>
                <div class="control-group required">
                    <label class="control-label" for="basket_email">Эл. почта <span style="color: #ff0000">*</span></label>
                    <div class="controls">
                        <input type="text" id="basket_email" name="basket_email" value="{$user_info.contact_email|escape}">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="basket_phone">Телефон</label>
                    <div class="controls">
                        <input type="text" id="basket_phone" name="basket_phone" value="{$user_info.contact_phone|escape}">
                    </div>
                </div>
            {/if}

            <div class="control-group new_line">
                <label class="control-label" for="basket_comments">Комментарии к заявке</label>
                <div class="controls">
                    <textarea id="basket_comments" name="basket_comments" rows="5">{if $client.auth and $client.details_in_order}{$client.details|escape}{/if}</textarea>
                </div>
            </div>

            <div class="personal">Отправляя данную форму, я соглашаюсь с <a href="/uploads/files/personal.pdf " target="_blank">политикой обработки персональных данных</a>.</div>

            <a class="button" id="order_submit" href="#" style="margin-right: 14px; margin-bottom: 20px;"><span>Отправить заявку</span></a>

        </form>
    </div>
</div>

{/if}