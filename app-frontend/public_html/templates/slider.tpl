<div id="block_slider">
    <div class="main_title">Используйте настройки и опции сайта для удобной работы</div>
    <div class="block-slider">
        <div class="block-slider_left">
            <p>Вы можете настроить отображение необходимых данных и использовать внедренные опции для удобной работы</p>
            <ul class="block-slider_menu">
                <li class="block-slider_menu-item active" data-target="variant-white_list"><span>Белый список</span></li>
                <li class="block-slider_menu-item" data-target="variant-black_list"><span>Черный список</span></li>
                <li class="block-slider_menu-item" data-target="variant-basket"><span>Интерфейс магазина (корзина)</span></li>
                <li class="block-slider_menu-item" data-target="variant-filters"><span>Фильтры по наличию и региону</span></li>
                <li class="block-slider_menu-item" data-target="variant-one_click"><span>Запрос в один клик</span></li>
                <li class="block-slider_menu-item" data-target="variant-excel"><span>Экспорт компонентов в excel</span></li>
                <li class="block-slider_menu-item" data-target="variant-search"><span>Поиск списком (многострочный поиск)</span></li>
                <li class="block-slider_menu-item" data-target="variant-send_all"><span>Веерные рассылки (чекбокс «послать всем»)</span></li>
                <li class="block-slider_menu-item" data-target="variant-telegram"><span>Бот телеграм</span></li>
                <li class="block-slider_menu-item" data-target="variant-mobile"><span>Мобильная версия сайта</span></li>
            </ul>
        </div>
        <div class="block-slider_right slider-tab">
            <div class="slider-tab_item active" id="variant-white_list">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/white_list.jpg" alt="Белый список"/>
                </div>
                <div class="slider-tab_text">
                    Войдите в личный кабинет. Выберите понравившихся вам поставщиков, теперь в списке предложения от них будут отображаться первыми.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-black_list">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/black_list.jpg" alt="Черный список"/>
                </div>
                <div class="slider-tab_text">
                    Войдите в личный кабинет. Выберите поставщиков. от которых не желаете получать предложения.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-basket">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/basket.png" alt="Интерфейс магазина (корзина)"/>
                </div>
                <div class="slider-tab_text">
                    Выберите этот параметр и добавляйте компоненты, указывайте количество и отправляйте заявки как в интернет-магазине.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-filters">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/filters.png" alt="Фильтры по наличию и региону"/>
                </div>
                <div class="slider-tab_text">
                    Фильтруйте поисковую выдачу как вам удобно. Выбирайте товары по наличию на складе и региону, где расположен поставщик.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-one_click">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/one_click.jpg" alt="Запрос в один клик"/>
                </div>
                <div class="slider-tab_text">
                    Выберите этот параметр и отправляйте запрос конкретному поставщику на один компонент.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-excel">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/excel.jpg" alt="Экспорт компонентов в excel"/>
                </div>
                <div class="slider-tab_text">
                    Нажмите на кнопку «Выгрузить в excel». Документ будет автоматически сформирован и загружен на ваш компьютер со всеми доступными позициями.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-search">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/search.jpg" alt="Поиск списком (многострочный поиск)"/>
                </div>
                <div class="slider-tab_text">
                    Переключитесь на кнопку «поиск по списку компонентов». Скопируйте из документа или наберите вручную несколько запросов. Результаты поиска будут выданы по всем введеным запросам.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-send_all">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/send_all.jpg" alt="Веерные рассылки (чекбокс «послать всем»)"/>
                </div>
                <div class="slider-tab_text">
                    Поставьте галочку и заявка будет отправлена всем поставщикам, предлагающим такой же компонент.
                </div>
            </div>
            <div class="slider-tab_item" id="variant-telegram">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/telegram.jpg" alt="Бот телеграм"/>
                </div>
                <div class="slider-tab_text">
                    Подключите бота по ссылке <a href="//telegram.me/EinfoBot" target="_blank">telegram.me/EinfoBot</a>, напишите в чате поисковый запрос и получите цены, статусы наличия и контакты поставщиков. Имя бота <a href="//telegram.me/EinfoBot" target="_blank">@EinfoBot</a>
                </div>
            </div>
            <div class="slider-tab_item" id="variant-mobile">
                <div class="slider-tab_img">
                    <img src="/media/i/slider/mobile.jpg" alt="Мобильная версия сайта"/>
                </div>
                <div class="slider-tab_text">
                    Искать и заказывать компоненты вы можете прямо с мобильного телефона.
                </div>
            </div>
        </div>
    </div>
</div>