<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ru">
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>

        <p>Здравствуйте, {$contact_name|escape}!</p>
        <p>Вы оформили заявку на сайте <a href="https://www.einfo.ru">www.einfo.ru</a> на следующие компоненты:</p>
        <p><strong>ЗАЯВКА #{$order_id|escape}</strong></p>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="font-size: 100%; margin-top: 5px;">
            <tr>
                <th style="background-color: #DFDFDF;" width="50%">Наименование</th>
                <th style="background-color: #DFDFDF;" width="100">Произв.</th>
                <th style="background-color: #DFDFDF;" width="200">Цена</th>
                <th style="background-color: #DFDFDF;" width="70">Кол-во</th>
				<th style="background-color: #DFDFDF;" width="70">Поставщик</th>
            </tr>
            {foreach from=$basket_items item=item}
            <tr>
                <td style="border-bottom: 1px solid gray">
                    <div><strong>{$item.title|escape}</strong>{if $item.mfgdate > 0} (г.в. {$item.mfgdate|escape}){/if}</div>
                    {if $item.descr}<div>{$item.descr|escape}</div>{/if}
                </td>
                <td style="border-bottom: 1px solid gray" align="center">{$item.manuf|default:"&mdash;"|escape}</td>
                <td style="border-bottom: 1px solid gray" align="center">
                    {$item.prices|prepare_prices|unescape:'html'}
                </td>
                <td style="border-bottom: 1px solid gray" align="center">{$item.rcount|escape}</td>
				<td style="border-bottom: 1px solid gray" align="center">{$item.user_title|escape}</td>
            </tr>
            {/foreach}
        </table>

        {if !empty($comments)}
            <p>
                <strong>Комментарий:</strong><br>
                {$comments|escape|nl2br}
            </p>
        {/if}

		<p>Письмо сформировано автоматической системой оформления заявок, пожалуйста, не отвечайте на него.</p>

        {if $client.mail_type == 'auth'}
            <p>
                Заявка отправлена поставщикам.<br />
                Просмотреть заявку вы можете по этой ссылке:<br />
                <a href="https://{$site_host|escape}/account/orders/?view={$order_id|escape}">https://{$site_host|escape}/account/orders/?view={$order_id|escape}</a>
            </p>
        {else}

            {if $client.mail_type == 'existing'}
                <p>
                    У вас уже есть аккаунт в системе Einfo.
                </p>
            {elseif $client.mail_type == 'new'}
                <p>
                    Теперь у вас есть аккаунт в системе Einfo.<br />
                    <strong>Ваш логин:</strong> {$client.email|escape}<br />
                    <strong>Ваш пароль:</strong> {$client.password|escape}<br />
                </p>
            {/if}

            <p>
                Для подтверждения заявки и отправки ее поставщикам, пожалуйста, перейдите по ссылке:<br />
                <a href="https://{$site_host|escape}/account/orders/?order_id={$order_id|escape}&client_id={$client.id|escape}&auth_hash={$client.auth_hash|escape}">https://{$site_host|escape}/account/orders/?order_id={$order_id|escape}&client_id={$client.id|escape}&auth_hash={$client.auth_hash|escape}</a>
            </p>
        {/if}

        <p style="color: gray">Просим прощения, если это письмо попало к Вам случайно, в этом случае оставьте его без внимания.</p>

        <p>--<br />
        C уважением,<br />
        Администрация www.einfo.ru</p>

    </body>
</html>
