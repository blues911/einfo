{* Smarty *}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ru">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
    Ваш прайс обработан и загружен в базу данных einfo.ru.<br />
    <br />
    Поставщик: {$user.title|escape}<br />
    Количество позиций в прайсе: {$cnt|escape}<br />
    Время обработки: {$date|escape}
</body>
</html>
