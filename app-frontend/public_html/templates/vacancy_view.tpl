<div class="wrap">
    <h1 style="width: 800px;">{$vacancy.speciality|escape} <a href="/vacancy/" class="back"><span class="icon"></span>Все вакансии</a></h1>
</div>
<div id="vacancy_card">
    <div class="vcancy_info">
        <div class="line date">
            <div class="title">Дата размещения</div>
            <div class="val">{$vacancy.date|escape}</div>
        </div>
        <div class="line">
            <div class="title">Описание</div>
            <div class="val">
                {$vacancy.info|escape|nl2br}
            </div>
        </div>
        <div class="line">
            <div class="title">Компания</div>
            <div class="val"><a href="/users/id-{$vacancy.user_id|escape}.html">{$vacancy.user_title|escape}</a> — <a class="all_vacancy" href="/vacancy/user-{$vacancy.user_id|escape}.html">все вакансии компании</a></div>
        </div>
        <div class="line">
            <div class="title">Опыт работы</div>
            <div class="val">

                {if $vacancy.experience == 1}
                    от 1 года
                    {elseif $vacancy.experience == 2}
                    от 2 лет
                    {elseif $vacancy.experience == 3}
                    от 3 лет
                    {elseif $vacancy.experience == 4}
                    от 4 лет
                    {elseif $vacancy.experience == 5}
                    от 5 лет
                {/if}

            </div>
        </div>

      {if $vacancy.age_start OR $vacancy.age_end}
        <div class="line">
            <div class="title">Возраст</div>
            <div class="val">{if $vacancy.age_start}от {$vacancy.age_start|escape}{/if} {if $vacancy.age_end}до {$vacancy.age_end|escape}{/if} лет</div>
        </div>
      {/if}

      {if $vacancy.city}
        <div class="line">
            <div class="title">Город</div>
            <div class="val">{$vacancy.city|escape}</div>
        </div>
      {/if}

        <div class="line">
            <div class="title">Место работы</div>
            <div class="val">

                {if $vacancy.workplace == 0}
                    не важно
                    {elseif $vacancy.workplace == 1}
                    работа в офисе
                    {elseif $vacancy.workplace == 2}
                    удаленная работа
                {/if}

            </div>
        </div>
        <div class="line">
            <div class="title">Тип работы</div>
            <div class="val">

                {if $vacancy.worktype == 0}
                    не важно
                    {elseif $vacancy.worktype == 1}
                    постоянная
                    {elseif $vacancy.worktype == 2}
                    разовая
                {/if}

            </div>
        </div>
        <div class="line">
            <div class="title">Тип занятости</div>
            <div class="val">

                {if $vacancy.workgraphic == 0}
                    не важно
                    {elseif $vacancy.workgraphic == 1}
                    полная
                    {elseif $vacancy.workgraphic == 2}
                    неполная
                    {elseif $vacancy.workgraphic == 3}
                    свободный график
                {/if}

            </div>
        </div>

      {if $vacancy.language}
        <div class="line">
            <div class="title">Иностранные языки</div>
            <div class="val">{$vacancy.language|escape}</div>
        </div>
      {/if}

      {if $vacancy.pay}
        <div class="line">
            <div class="title">Зарплата</div>
            <div class="val">{$vacancy.pay|escape} руб.</div>
        </div>
      {/if}

    </div>

  {if $vacancy.contact_person || $vacancy.contact_phone || $vacancy.contact_fax || $vacancy.contact_email}
    <div class="vcancy_contacts">

      {if $vacancy.contact_person}
        <div class="line">
            <div class="title">Контактное лицо</div>
            <div class="val">{$vacancy.contact_person|escape}</div>
        </div>
      {/if}

      {if $vacancy.contact_phone}
        <div class="line">
            <div class="title">Телефон</div>
            <div class="val">{$vacancy.contact_phone|escape}</div>
        </div>
      {/if}

      {if $vacancy.contact_fax}
        <div class="line">
            <div class="title">Факс</div>
            <div class="val">{$vacancy.contact_fax|escape}</div>
        </div>
      {/if}

      {if $vacancy.contact_email}
        <div class="line">
            <div class="title">Электронная почта</div>
            <div class="val"><a href="mailto:{$vacancy.contact_email|escape}">{$vacancy.contact_email|escape}</a></div>
        </div>
      {/if}
    </div>
  {/if}

</div>

