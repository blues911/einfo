<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ru">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
    {if !empty($data.ip)}
        IP: {$data.ip}<br>
    {/if}

    {if !empty($data.text)}
        {$data.text}<br>
    {/if}

    {if $data.type == 'send'}
        {foreach $data.content as $content}
            Вопрос: {$content.question}<br/>Ответ: {$content.answer}<br/><br/>
        {/foreach}
    {elseif $data.type == 'close'}
        Пользователь отказался от прохождения опроса.<br>
    {/if}

    <p>Страница: {$data.page|escape}</p>
    <p>COOKIE:</p>
    {foreach $data.cookies as $cookie => $value}
        <div><b>{$cookie}</b>: {$value|json_encode}</div>
        {foreachelse}
        <div>Пусты</div>
    {/foreach}
</body>
</html>
