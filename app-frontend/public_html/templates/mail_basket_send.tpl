<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ru">
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>

        <p>Вниманию сотрудников {$user_title|escape}!</p>
        <p>В адрес вашей организации с сайта <a href="https://www.einfo.ru">www.einfo.ru</a> поступила заявка #{$basket.id|escape} на компоненты, присутствующие в вашем прайс-листе:</p>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="font-size: 100%; margin-top: 5px;">
            <tr style="background-color: #DFDFDF;">
                <th width="50%">Наименование</th>
                <th width="100">Произв.</th>
                <th width="200">Цена</th>
                <th width="70">Кол-во</th>
            </tr>
            {foreach from=$basket_items item=item}
            <tr>
                <td style="border-bottom: 1px solid gray">
                    <div><strong>{$item.title|escape}</strong>{if $item.mfgdate > 0} (г.в. {$item.mfgdate|escape}){/if}</div>
                    {if $item.descr}<div>{$item.descr|escape}</div>{/if}
                </td>
                <td style="border-bottom: 1px solid gray" align="center">{$item.manuf|default:"&mdash;"|escape}</td>
                <td style="border-bottom: 1px solid gray" align="center">
                    {$item.prices|prepare_prices|unescape:'html'}
                </td>
                <td style="border-bottom: 1px solid gray" align="center">{$item.rcount|escape}</td>
            </tr>
            {/foreach}
        </table>
        <p style="margin-top: 25px">
            <div><strong>Контактные данные заказчика:</strong></div>
            -------------------------------<br />
            <strong>Имя</strong>: {$basket.contact_name|escape}<br />
            <strong>E-mail:</strong> <a href="mailto:{$basket.contact_email|escape}">{$basket.contact_email|escape}</a><br />
            <strong>Телефон:</strong> {$basket.contact_phone|escape}<br />
            {if $basket.comments}<strong>Комментарии:</strong> {$basket.comments|escape|nl2br}<br />{/if}
        </p>
		Валидность E-mail адреса заказчика подтверждена.<br />
		Пожалуйста, свяжитесь с ним в кратчайшие сроки для уточнения деталей.

		<p>--<br />
        C уважением, администратор проекта einfo.ru
        </p>

    </body>
</html>
