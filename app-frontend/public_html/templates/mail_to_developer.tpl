<p>Заполнена форма обратной связи для разработчика на сайте {$site_host}</p>
<p>Текст</p>
<div>{$text|escape}</div>
<p>IP: {$ip}</p>
<p>Страница: {$page|escape}</p>
<p>COOKIE:</p>
{foreach $cookies as $cookie => $value}
    <div><b>{$cookie}</b>: {$value|json_encode}</div>
{foreachelse}
    <div>Пусты</div>
{/foreach}