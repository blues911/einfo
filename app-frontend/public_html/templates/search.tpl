{$limit = 300|escape}

{* <script src="/media/js/search.js" type="text/javascript"></script> *}

<table style="display: none;">
    <tr class="spoiler">
        <td colspan="2"></td>
        <td colspan="5"><a href="javascript:;">Еще предложения</a></td>
    </tr>
</table>

{* Однокликовый интерфейс *}

<div id="config" data-oneclick="{if $interface != 1}1{else}0{/if}" data-auth="{if $client.auth}1{else}0{/if}" style="display: none;" hidden="hidden"></div>

{if $interface == 0}

    <a id="show_order" href="#order_block" style="display: none;" hidden="hidden"></a>

    <form id="basket_form" action="javascript:void(null);" style="display: none;">
        <input id="basket_item" class="basket_item" data-id="" type="hidden" />
    </form>

    <div id="order_block" data-individual-buyer="{if $user_settings.individual_buyers}1{else}0{/if}" style="display: none;">
        <a title="Закрыть" class="close" href="javascript:;"></a>

        <div id="order_body" data-individual-buyers="" data-individual-users-cnt="">

            <h3>Запросить коммерческое предложение на </h3>
            <h1 id="part_number" style="max-width: 400px; word-break: break-all;"></h1>

            <div class="individual_buyer_radio" {if $user_settings.individual_buyers}style="display: none;"{/if}>
                Вы заказываете как <span style="color: #ff0000">*</span>
                <label>
                    <input id="individual_buyer_yes" type="radio" name="individual_buyer" value="yes" />
                    физлицо
                </label>
                <label>
                    <input id="individual_buyer_no" type="radio" name="individual_buyer" value="no" />
                    юрлицо
                </label>
            </div>

            <div id="no_individual_buyers_msg" style="">
                <div class="this">
                    Выбранный вами поставщик работает только с юридическими лицами,
                    если вы хотите оформить заказ как физлицо, то отправьте заявку другим поставщикам.
                </div>
                <div class="all">
                    Выбранный вами поставщик работает только с юридическими лицами.
                </div>
            </div>

            <table class="basket_list form-element" style="margin: 10px 0 0 -2px; font-size: 13px; max-width: 400px;">
                <tr id="part_user_row">
                    <td style="vertical-align: top; width: 82px;">
                        Поставщик
                    </td>
                    <td id="part_user" style="font-weight: bold; word-break: break-all;"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;">
                        Количество
                    </td>
                    <td style="vertical-align: middle;">
                        <div class="cnt_line">
                            <a href="javascript:;" class="icon minus" data-action="minus" style="display: inline;"></a>
                            <input type="text" class="comp_val" id="basket_item_cnt" name="basket_item_cnt" value="1" style="width: 37px; text-align: right;">
                            <a href="javascript:;" class="icon plus" data-action="plus" style="display: inline;"></a>
                        </div>
                    </td>
                </tr>
            </table>

            <form id="order_form" class="form-horizontal" action="javascript:;" method="post">

                {if $client.auth}
                    <table class="form-element" style="margin: 0 0 25px -3px;">
                    <tr>
                        <td style="width: 82px; font-size: 13px;">
                            <input type="hidden" name="client_id" value="{$client.id|escape}" />
                            <input type="hidden" name="auth_hash" value="{$client.auth_hash|escape}" />
                            <input type="hidden" name="basket_name" value="{$client.name|escape}" />

                            Ваши ФИО
                        </td>
                        <td><strong>{$client.name|escape}</strong></td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <input type="hidden" name="basket_email" value="{$client.email|escape}" />

                            Эл. почта
                        </td>
                        <td><strong>{$client.email|escape}</strong></td>
                    </tr>

                    {* Если в профиле не указан телефон то показываем поле для ввода *}
                    {if empty($client.phone)}
                        </table>
                        <div class="control-group">
                            <label class="control-label" for="basket_phone">Телефон</label>
                            <div class="controls">
                                <input type="text" id="basket_phone" name="basket_phone" value="">
                            </div>
                        </div>
                    {else}
                        <tr>
                            <td style="font-size: 13px;">
                                <input type="hidden" name="basket_phone" value="{$client.phone|escape}" />

                                Телефон
                            </td>
                            <td><strong>{$client.phone|escape}</strong></td>
                        </tr>
                        </table>
                    {/if}
                {else}
                    <div class="control-group required form-element">
                        <label class="control-label" for="basket_name">Ваши ФИО <span style="color: #ff0000">*</span></label>
                        <div class="controls">
                            <input type="text" id="basket_name" name="basket_name" value="{$user_info.contact_name|escape}">
                        </div>
                    </div>
                    <div class="control-group required form-element">
                        <label class="control-label" for="basket_email">Эл. почта <span style="color: #ff0000">*</span></label>
                        <div class="controls">
                            <input type="text" id="basket_email" name="basket_email" value="{$user_info.contact_email|escape}">
                        </div>
                    </div>
                    <div class="control-group form-element">
                        <label class="control-label" for="basket_phone">Телефон</label>
                        <div class="controls">
                            <input type="text" id="basket_phone" name="basket_phone" value="{$user_info.contact_phone|escape}">
                        </div>
                    </div>

                {/if}

                <div class="control-group new_line form-element">
                    <label class="control-label" for="basket_comments">Комментарии к заявке</label>
                    <div class="controls">
                        <textarea id="basket_comments" name="basket_comments" rows="5" style="width: 415px;">{if $client.auth and $client.details_in_order}{$client.details|escape}{/if}</textarea>
                    </div>
                </div>

                <div id="order_all_offers_block">
                    <label class="custom_label" for="order_all_offers">
                        <span class="custom_checkbox" style="display: inline-block;"></span>
                        <span class="caption" style="position: relative; top: -3px; font-weight: bold;" data-text="" data-individual-text="">Отправить заявку остальным поставщикам</span>
                        <input id="order_all_offers" type="checkbox" style="display: none;" data-id="" data-comp-ids="" data-individual-comp-ids="" />
                        <div style="font-size: 11px; padding-left: 20px; margin-top: -7px;">(заявка будет отправлена поставщикам, предлагающим такой же компонент)</div>
                    </label>
                </div>

                <div id="order_all_individual_offers_block" style="display: none;">
                    <label class="custom_label" for="order_all_individual_offers">
                        <span class="custom_checkbox" style="display: inline-block;"></span>
                        <span class="caption" style="position: relative; top: -3px; font-weight: bold;" data-text="" data-individual-text="">Отправить заявку остальным поставщикам</span>
                        <input id="order_all_individual_offers" type="checkbox" style="display: none;" data-id="" data-comp-ids="" data-individual-comp-ids="" />
                        <div style="font-size: 11px; padding-left: 20px; margin-top: -7px;">(заявка будет отправлена поставщикам, предлагающим такой же компонент)</div>
                    </label>
                </div>

                <div class="system_message" style="width: 400px; padding: 5px 15px 5px 15px; font-size: 13px; margin: 10px 10px 15px 0px; line-height: 17px;">
                    <p>Эта заявка не накладывает на Вас никаких обязательств. <br>
                        Ожидайте предложения от поставщика. <br>
                        Понравился наш сервис? Возвращайтесь к нам снова!</p>
                </div>

                <div class="personal">Отправляя данную форму, я соглашаюсь с <a href="/uploads/files/personal.pdf " target="_blank">политикой обработки персональных данных</a>.</div>

                <a class="button" id="order_submit" href="#" style="margin-right: 10px; margin-bottom: 20px;"><span>Отправить заявку</span></a>

            </form>

        </div>

        {if $client.auth == false}
            <div id="order_msg" style="display: none;" hidden="hidden">
                <h2>Заявка успешно обработана!</h2>
                <p>На Вашу электронную почту <strong>{$user_email|escape}</strong> отправлено письмо с ссылкой для активации заявки.</p>
                <p>Пожалуйста, проверьте свой почтовый ящик и подтвердите заявку, кликнув по присланной в письме ссылке. Без подтверждения заявки она <strong>НЕ БУДЕТ ОТПРАВЛЕНА</strong> поставщику.</p>
                <div style="float: right;">
                    <a class="button close-button" href="javascript:;" style="margin-right: 10px; margin-bottom: 20px;"><span>Закрыть</span></a>
                </div>
            </div>
        {/if}

    </div>

{/if}

{* ------------------ *}

{if !$result.query || $result.query && $result.query_short}

<div id="search_content">
    <div class="wrap">
        <h1>Информация о наличии компонентов у поставщиков</h1>
        <div class="column_right"></div>
        <div class="column_left">
            <div class="static_text" >

            {if $result.query_short}
                <div class="system_message" style="width: 520px;">
                    <h2>Длина поискового запроса должна быть не меньше 3-х символов.</h2>
                </div>
            {/if}

                {include file = "howtosearch.tpl"}

            </div>
        </div>

    </div>
</div>

{elseif $result.query && !$result.search_result && !$result.query_short && $result.query_variant}

<div id="search_content">
    <div class="wrap">
        <div class="system_message" style="width: 70%;">
            <p><strong>По результатам поиска ничего не найдено.</strong>
            <br>Попробуйте другие варианты поиска.
            {if !$special_search }
                Например, <strong><a href="{$query_variant_url}">{$query_variant|escape}</a></strong> <span style="color: #a4a4a1">({$query_variant_cnt|escape} {plural num=$query_variant_cnt words="предложение|предложения|предложений"} от поставщиков)</span></p>
            {/if}
        </div>
    </div>
</div>

{elseif $result.query && !$result.search_result && !$result.query_short && !$result.query_variant}

<div id="search_content">
    <div class="wrap">
        <div class="system_message">
            <p><strong>По результатам поиска ничего не найдено.</strong>
            <br>Попробуйте еще раз, но с другими параметрами.</p>
        </div>
    </div>
</div>

{else}

<div id="search_content" data-comp="{$comp|escape}">
    <div class="wrap {if !empty($chain)}with_chain{/if}">
        {if $multiple_search}
            <h1>Результат поиска по <span class="multiple_count">{$count_comp} комплектующим</span></h1>
        {else}
            <h1>Купить {$comp|escape} у поставщиков</h1>
        {/if}

        {if !empty($chain)}
            <div class="chain_block">
              {foreach $chain as $item}
                <a href="/{$item|encodeURIComponent}/">{$item|escape}</a>
                {if $item@iteration < $chain|count}&nbsp;|&nbsp;{/if}
              {/foreach}
            </div>
        {/if}

  {if $show_individual_buyers_option && $client.auth}
        <div class="system_message" style="width: 70%">
            <div class="button_block" style="margin: 14px 30px 0 0; float: left;">
                <a class="button light yes" data-redirect_link="/{$comp|escape}/" href="#" style="margin-right: 15px;">Да</a>
                <a class="button light no" href="#">Нет</a>
                <div class="clear"></div>
            </div>

            <p><strong>Исключить из поиска поставщиков, работающих только с юрлицами?</strong>
            <br>Изменить значение этого параметра можно в разделе &ndash; <span class="icon_settings"><span></span></span><a href="/account/settings/">настройки</a></p>
        </div>
  {/if}

        <div id="search_options_wrap">
            <div id="search_options">
                <div class="wrap">

                    {if $interface == 1}
                        <a class="button light order" href="/basket/">Оформить запросы</a>
                        <div id="smile"></div>
                        <div id="in_basket_fixed"{if empty($in_basket)} class="hide"{/if}><span>{$in_basket|escape}</span></div>
                        <a class="button light select_all_components {if !($all_items < $limit && $all_items > 0)} hide{/if}" href="#" data-page="search"><span class="empty">Выбрать все</span><span class="checked">Очистить все</span></a>
                    {/if}

                    <a class="button light to_top" href="#">Наверх</a>

                    <div class="select_wrap">
                        <a class="button white region filter_button disabled" data-filter="region" data-value="all" data-type="country" href="#"><span class="title">Всe регионы</span><span class="icon"></span></a>
                        <div class="popup region" style="display: none;">
                            <table>
                                <tr>
                                    <td {if $country_list && $city_list}colspan="2"{/if}>
                                        <ul class="column" style="float: left;">
                                            <li>
                                                <a class="dotted dark" data-title="Всe регионы" data-val="all" data-type="country" href="#">Всe регионы</a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>

                              {if $country_list || $city_list}
                                <tr>

                                  {if $city_list}
                                    <td>
                                        <ul class="column" style="float: left;">

                                            <li class="country">
                                                <a class="dotted dark" data-title="Россия" data-val="186" data-type="country" href="#">Россия</a>
                                            </li>


                                          {foreach $city_list as $item}

                                            <li class="city">
                                                <a class="dotted dark" data-title="{$item.title|escape}" data-val="{$item.id|escape}" data-type="city" href="#">{$item.title|escape}</a>
                                            </li>

                                          {/foreach}

                                        </ul>
                                    </td>
                                  {/if}

                                  {if $country_list}
                                    <td>
                                        <ul class="column" style="float: left;">

                                          {foreach $country_list as $item}
                                            <li class="country">
                                                <a class="dotted dark" data-title="{$item.title|escape}" data-val="{$item.id|escape}" data-type="country" href="#">{$item.title|escape}</a>
                                            </li>
                                          {/foreach}

                                        </ul>
                                    </td>
                                  {/if}

                                </tr>
                              {/if}

                            </table>
                        </div>
                    </div>

                    <div class="select_wrap">
                        <a class="button white availability filter_button disabled" data-filter="availability" data-value="all" data-type="availability"  href="#"><span class="title">Наличие не важно</span><span class="icon"></span></a>
                        <div class="popup availability" style="display: none;">
                            <table>
                                <tr>
                                    <td>
                                        <ul class="column" style="float: left;">
                                            <li>
                                                <a class="dotted dark" data-val="all" data-type="availability" data-title="Наличие не важно" href="#">Наличие не важно</a>
                                            </li>
                                            <li>
                                                <a class="dotted dark{if !$filter_availability.in_stock} lock{/if}" data-val="1" data-type="availability" data-title="На складе" href="#">На складе</a>
                                            </li>
                                            <li>
                                                <a class="dotted dark{if !$filter_availability.order_quickly} lock{/if}" data-val="2" data-type="availability" data-title="На заказ до {$delivery_weeks|escape} недель" href="#">На заказ до {$delivery_weeks|escape} недель</a>
                                            </li>
                                            <li>
                                                <a class="dotted dark{if !$filter_availability.order_long} lock{/if}" data-val="3" data-type="availability" data-title="На заказ от {$delivery_weeks|escape} недель" href="#">На заказ от {$delivery_weeks|escape} недель</a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <form class="xls_export top" action="/ajaxer.php?x=xls_export" method="post">
                        <input type="hidden" name="query"/>
                        <input type="hidden" name="items_id"/>
                        <textarea name="xls_multiple_q" style="display: none;"></textarea>
                        <button type="submit">Выгрузить в Excel</button>
                    </form>

                    {if $interface == 0}

                        {if !$client.auth || ($client.auth && !$user_settings.individual_buyers)}
                            <label class="custom_label" style="display: block; float: right; cursor: pointer; margin: 0 10px 0 15px;">
                                <span id="price_filter" class="custom_checkbox filter_button" data-type="individualBuyers" style="display: inline-block; margin: 0; vertical-align: middle;"></span>
                                <span class="text" style="font-size: 13px; vertical-align: middle; line-height: 30px;">Работают с физическими лицами</span>
                            </label>
                        {/if}

                        <label class="custom_label" style="display: block; float: right; cursor: pointer; margin: 0 10px;">
                            <span id="price_filter" class="custom_checkbox filter_button" data-type="with_price" style="display: inline-block; margin: 0; vertical-align: middle;"></span>
                            <span class="text" style="font-size: 13px; vertical-align: middle; line-height: 30px;">Только с ценой</span>
                        </label>
                    {/if}

                    {if $multiple_search}
                        <div class="select_wrap">
                            <a class="button white clear hide multiple_search_clean" href="#"><span class="title">Очистить</span><span class="icon"></span></a>
                        </div>
                    {else}
                        <div class="select_wrap">
                            <a class="button white clear hide" href="/{$comp|urlencode|escape}/"><span class="title">Очистить</span><span class="icon"></span></a>
                        </div>
                    {/if}


                    <div class="clear"></div>
                </div>
                <div class="shadow"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div style="position: relative" class="result_block">
        <div class="result_column" >

            {if $show_warning}
                <div id="info_large_result" class="system_message" style="width: 100%; margin: 0 0 10px; padding: 0;">
                    <p style="padding: 13px;"><strong>По вашему запросу показаны только первые {$all_items|escape} {plural num=$all_items words="предложение|предложения|предложений"} от поставщиков.</strong><br>Пожалуйста, уточните запрос.</p>
                </div>
            {/if}

            <div id="search_result" data-spoiler-num="{$spoiler_num|escape}" data-users-ids='{$users_ids}'>
                <table class="search offer tmp_table" style="table-layout: fixed;">
                    <thead>
                    <tr>
                        <th class="company">Продавец</th>

                        {if $interface == 1}
                            <th class="check{if !($all_items < $limit && $all_items > 0)} hide{/if}">
                                <span class="check_wrap">
                                    <span class="tip_check"></span>
                                    <span class="custom_checkbox select_all_components" data-page="search"></span>
                                </span>
                            </th>
                        {else}
                            <th class="check"></th>
                        {/if}
                        <th class="product">Наименование</th>
                        <th class="header availability">Склад</th>
                        <th class="header remote">Под заказ</th>
                        <th class="header">Цена</th>
                    </tr>
                    </thead>
                    <tfoot>
                        <tr id="cross_filter_empty" style="display: none;">
                            <td colspan="6" style="height: 150px; text-align: center; vertical-align: middle;">
                                <div class="system_message" style="margin: 25px auto;">
                                    <h2>Для данной комбинации фильтров ничего не найдено.</h2>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {$result.html.search_result nofilter}

                {if $show_warning}
                    <div id="info_large_result" class="system_message" style="width: 100%; margin: 0 0 10px; padding: 0;">
                        <p style="padding: 13px;"><strong>По вашему запросу показаны только первые {$all_items|escape} {plural num=$all_items words="предложение|предложения|предложений"} от поставщиков.</strong><br> Все результаты вы можете получить, выгрузив файл Excel</p>
                    </div>
                {/if}
                <form class="xls_export bottom" action="/ajaxer.php?x=xls_export" method="post">
                    <input type="hidden" name="query"/>
                    <input type="hidden" name="items_id"/>
                    <textarea name="xls_multiple_q" style="display: none;"></textarea>
                    <button type="submit">Выгрузить в Excel</button>
                </form>
            </div>
            <div id="log" style="height: 50px; width: 50px; margin: 0 auto;">
                <div id="searching" style="padding: 5px 0 0; display: none;"><img src="/media/i/load.gif" alt="Сортировка"></div>
            </div>

            {if !$multiple_search}
                <div id="search_link" style="padding: 25px 0 0;" data-q="{$comp|escape}" data-q_cp="{$comp_cp|escape}">
                    Поискать <strong>{$comp|escape}</strong> в других поисковых системах:
                    <a class="hide_link" target="_blank" data-link="efind" href="#">eFind.ru</a> -
                    <a class="hide_link" target="_blank" data-link="chipfind" href="#">chipFind.ru</a> -
                    <a class="hide_link" target="_blank" data-link="yandex" href="#">Yandex.ru</a> -
                    <a class="hide_link" target="_blank" data-link="google" href="#">Google.ru</a>
                </div>
            {/if}

        </div>

        <div class="filter_column">

            {* Datasheet *}
            {if !empty($datasheet)}

                {if !$multiple_search}
                    <div class="datasheet_preview">

                        <div id="pdf" style="display: none;" data-href="{$datasheet.pdf_href|escape}" data-size="{$datasheet.pdf_size|escape}"></div>

                        {* IMG *}
                        {if $datasheet.have_img}
                            <a href="{$datasheet.img_fullsize|escape}" class="fancybox">
                                <img src="{$datasheet.img_preview|escape}">
                            </a>
                        {/if}

                        {* PDF *}
                        <div class="datasheet_icon"></div>
                        <div class="datasheet_link">
                            <a href="{$datasheet.pdf_href|escape}" target="_blank">
                                <span style="font-size: 12px;">Документация на</span><br />
                                <strong>{$datasheet.comp_canon|escape}</strong>
                            </a>
                            <div class="datasheet_size">{$datasheet.pdf_size|escape}</div>
                        </div>

                    </div>
                {/if}

            {/if}

            {if $multiple_search}
                <div id="filter_block" class="filter_block">
                    <span class="title">Компоненты</span>
                    <ul>
                        <li class="main"><p class="comp_link">ВСЕ КОМПОНЕНТЫ</p> <div class="cnt">{$all_items|escape}</div></li>
                        {$result.html.filter nofilter}
                    </ul>
                    <div class="clear"></div>
                </div>
            {else}
                <div id="filter_block" class="filter_block">
                    <ul>
                        <li class="main"><p class="comp_link">ВСЕ КОМПОНЕНТЫ</p> <div class="cnt">{$all_items|escape}</div></li>

                        {$result.html.filter nofilter}
                    </ul>
                    <div class="clear"></div>
                </div>
            {/if}
        </div>
    </div>

    <div class="clear"></div>
</div>

{/if}

<a id="show_messag_for_unregistered" href="#message_unregistered" style="display: none;" hidden="hidden"></a>

<div id="message_unregistered" style="display: none;">
    <a href="#" class="close"></a>
        <div>
            Это действие доступно только зарегистрированным пользователям.
            <br>
            <a href="#" class="show_reg_form">Зарегистрироваться</a>
        </div>
</div>

{literal}
    <script type="text/javascript">
        function change_individual_buyer(is_individual_buyer)
        {
            var submit = $('#order_submit');
            var form_elements = $('#order_body .form-element');
            var form_message = $('#order_form .system_message:first');
            var part_user_row = $('#part_user_row');

            var checkbox = $('#order_all_offers').closest('div');
            var individual_checkbox_input = $('#order_all_individual_offers');
            var individual_checkbox = individual_checkbox_input.closest('div');
            var error_msg = $('#no_individual_buyers_msg');

            var individual_buyers_flag = Number($('#order_body').attr('data-individual-buyers'));
            var individual_users_cnt = Number($('#order_body').attr('data-individual-users-cnt'));

            checkbox.hide();
            individual_checkbox.hide();
            individual_checkbox.off('change.individual_buyers');
            error_msg.hide();
            form_message.show();
            submit.show();
            form_elements.show();
            part_user_row.show();

            if (is_individual_buyer)
            {
                if (individual_users_cnt)
                {
                    individual_checkbox.show();
                }

                if (!individual_buyers_flag)
                {
                    form_elements.hide();

                    if(individual_users_cnt)
                    {
                        error_msg.show();
                        error_msg.find('div').hide();
                        error_msg.find('.this').show();

                        var _toggle_all_offers = function(checkbox)
                        {
                            var is_checked = $(checkbox).prop('checked');

                            if (is_checked)
                            {
                                error_msg.hide();
                                submit.removeClass('disabled');
                                form_elements.show();
                                part_user_row.hide();
                            }
                            else
                            {
                                error_msg.show();
                                submit.addClass('disabled');
                                form_elements.hide();
                            }
                        };

                        _toggle_all_offers(individual_checkbox_input);

                        individual_checkbox_input.on('change.individual_buyers', function()
                        {
                            _toggle_all_offers(this);
                        });
                    }
                    else
                    {
                        error_msg.show();
                        error_msg.find('div').hide();
                        error_msg.find('.all').show();
                        form_message.hide();
                        submit.addClass('disabled').hide();
                    }
                }
            }
            else
            {
                checkbox.show();
                submit.removeClass('disabled');
            }
        }

        document.addEventListener('DOMContentLoaded', function()
        {
            $(document).on('click', 'a.multiple_search_clean', function(e)
            {
                $('#search_form').submit();
                e.preventDefault();

            });

            $('#order_all_offers, #order_all_individual_offers').on('change', function()
            {
                if ($(this).attr('checked'))
                {
                    $(this).siblings('.custom_checkbox').addClass('checked');
                }
                else
                {
                    $(this).siblings('.custom_checkbox').removeClass('checked');
                }
            });

            $('#individual_buyer_yes').on('change', function()
            {
                $('#order_submit').removeClass('disabled');
                var is_checked = $(this).prop('checked');
                change_individual_buyer(is_checked);
            });

            $('#individual_buyer_no').on('change', function()
            {
                $('#order_submit').removeClass('disabled');
                var is_checked = $(this).prop('checked');
                change_individual_buyer(!is_checked);
            });

            xls_export_set_values();

            $('.xls_export').on('submit', function(e){
                if ($(this).find('input[name="items_id"]').val() == '')
                {
                    return false;
                }

                var cnt = $('#search_result').find('tr.show').length;

                einfo_stat('search_export_xls', cnt);
            });
        });

    </script>
{/literal}
