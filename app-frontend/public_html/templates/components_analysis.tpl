<form class="components_analysis">
    <div class="main_title">
        Рассчитайте примерный эффект от размещения вашего прайс-листа на нашей площадке.
        <a class="tooltip price_analysis question" title="Эффект рассчитывается на основе данных за последние пол года, используются до 100 тыс. строк из загруженного файла"></a>
    </div>

    <p class="ordinary">Загрузите прайс-лист до 100 тыс. строк (форматы: XLS, XLSX)</p>

    <div class="file_input_wrap">
        <div class="before">Выберите файл</div>
        <div class="after">Файл не выбран</div>
        <input type="file"/>
    </div>

    <div class="file_info">
        <div class="title"></div>
        <a class="cancel_file" href="#"></a>
    </div>

    <div class="process_status">
        <div class="message"></div>
        <div class="progress_bar"><div class="inner"></div></div>
    </div>

    <div class="file_format_error">Выбран файл неверного формата</div>

    <div class="result">
        <div class="ready_title">Готово!</div>

        <div class="messages">
            <div class="profile_view item">
                <div class="digit"></div>
                <div class="text"></div>
            </div>

            <div class="profile_click item">
                <div class="digit"></div>
                <div class="text"></div>
            </div>
        </div>

        <a class="more_analysis" href="#">Проанализировать еще</a>
    </div>

    <div class="clear"></div>
</form>