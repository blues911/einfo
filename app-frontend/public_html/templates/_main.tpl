{if !empty($container.page_timer)}
<!-- Страница: {$container.page_timer.total|escape} сек. (подготовка - {$container.page_timer.prepare|escape} сек.; проверка - {$container.page_timer.verify|escape} сек.; парсинг - {$container.page_timer.parser|escape} сек.) -->
{/if}
{if !empty($container.search_timer)}
<!-- Sph: {$container.search_timer.sphinx|escape}; Tmp: {$container.search_timer.tmp|escape}; Slc: {$container.search_timer.select|escape}; Ttl: {$container.search_timer.total|escape} -->
{/if}

<!DOCTYPE html>
<html>
    <head>
    <title>{if !empty($container.page_title)}{$container.page_title|escape} на Einfo.ru{else}Радиодетали и электронные компоненты, поисковая система Einfo.ru{/if}</title>
    <meta name="chat:type" content="frontend" />
    <meta name="chat:host" content="{$chat_host|escape}" />
    <meta name="chat:host_ws" content="{$chat_host_ws|escape}" />
    <meta name="chat:host_frontend" content="{$chat_host_frontend|escape}" />
    <meta name="chat:last_visit_interval" content="{$chat_last_visit_interval|escape}" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    {if !empty($container.page_description)}<meta name="description" content="{$container.page_description|escape}" />{/if}

    {if !empty($container.page_keywords)}<meta name="keywords" content="{$container.page_keywords|escape}" />{/if}

    {if empty($container.no_robots)}<meta name="robots" content="{if !empty($container.robots)}{$container.robots|escape}{else}index, follow{/if}" />{/if}
    <meta name="googlebot" content="noodp" />
    <meta name="referrer" content="always" />

    <meta name="viewport" content="width=1024" />
    <meta name="format-detection" content="telephone=no">

    {if !empty($container.page_canonical)}
    <link rel="canonical" href="{$container.page_canonical|escape}"/>
    {/if}

    {if !empty($container.page_url_canonical)}
    <link rel="alternate" media="only screen and (max-width: 640px)" href="{$container.page_url_canonical|escape}">
    {/if}

    <link href="/assets.css?ver={$container.css_version}" type="text/css" rel="stylesheet" />

    <!--[if lte IE 8]>
    <link href="/media/css/ie8-.css" rel="stylesheet" type="text/css" />
    <![endif]-->

    <link href="/media/i/favicon.ico" type="image/x-icon" rel="icon" />

    {if isset($container.prevnext)}
        {if $container.prevnext.prev}<link rel="prev" href="{$container.prevnext.prev|escape}" />{/if}
        {if $container.prevnext.next}<link rel="next" href="{$container.prevnext.next|escape}" />{/if}
    {/if}
    
    </head>
<body>

<div id="main">
    <div class="wrap" id="main_wrap">
        <div id="header">
            <div class="navigation">
                <ul id="menu">
                    <li class="home"><a href="/" class="home_icon"></a></li>
                  {foreach $container.menu_items as $item}
                    {if !isset($item.hide)}
                      <li {if $item.link == 'telegram'}class="telegram"{/if}>
                          <a href="/{$item.link|escape}/">{$item.title|escape}</a>
                      </li>
                    {/if}
                  {/foreach}

                </ul>

                {if $interface == 1}
                    <div id="in_basket"{if $container.basket_count == 0} class="empty"{/if}>
                        <span class="custom_checkbox" title="Воспользуйтесь поиском для добавления компонента в заявку"></span>
                        <span class="text">Компоненты пока не выбраны</span>
                        <a class="link" href="/basket/">{plural num=$container.basket_count words="Выбран|Выбрано|Выбрано"} {$container.basket_count|escape} {plural num=$container.basket_count words="компонент|компонента|компонентов"}</a>
                    </div>
                {/if}

                {* Проверяем авторизацию пользователя *}
                {if $client.auth_with_hash or $client.auth_with_login}
                    <div id="auth_info">
                        Вы вошли
                    </div>
                {/if}

                <div id="auth_div">
                    {if $client.auth}
                        <a class="settings_icon" href="/account/settings/"></a>&nbsp;
                        <a id="client_menu" href="#" class="dotted">{$client.name|escape}</a>
                    {else}
                        <img src="/media/i/login.png">&nbsp;
                        <a id="client_login" href="#login" class="dotted dark">Вход</a>&nbsp;/&nbsp;<a id="client_reg" href="#reg" class="dotted dark">Регистрация</a>
                    {/if}
                </div>

                <!-- Меню клиента -->

                <div id="client_menu_block" class="popup availability" style="padding: 10px 20px; display: none;">
                    <table>
                        <tr>
                            <td>
                                <ul class="column" style="float: left;">
                                    <li>
                                        <a class="dark" href="/account/orders/">Мои заявки</a>
                                    </li>
                                    <li>
                                        <a class="dark" href="/account/settings/">Настройки</a>
                                    </li>
                                    <li>
                                        <a class="dark" href="/account/black_list/">Черный список</a>
                                    </li>
                                    <li>
                                        <a class="dark" href="/account/white_list/">Белый список</a>
                                    </li>
                                    <li>
                                        <a class="dark" href="/account/profile/">Профиль</a>
                                    </li>
                                    <li style="margin-top: 15px;">
                                        <a class="client_exit" href="#">Выйти</a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- Вход для клиентов -->
                <div id="client_login_block" style="display: none;">
                    <a class="close" href="#"></a>
                    <img src="/media/i/ajax_loader.gif" class="block_ajax_loader">

                    <div style="position: relative;">
                        <h2>Авторизация</h2>
                        <div class="login_method">
                            <div id="login_by_pass" class="active">
                                <span>с паролем</span>
                            </div>
                            <div id="login_by_email">
                                <span>только по email</span>
                            </div>
                        </div>
                    </div>

                    <p id="client_login_error" class="block_error"></p>
                    <p id="client_login_msg" class="block_info"></p>
                    <form id="client_login_form" class="form-horizontal" action="javascript:void(null);" style="width: 300px;" method="post">
                        <input name="action" type="hidden" value="login" />
                        <input id="client_login_method" name="method" type="hidden" value="pass" />
                        <div class="control-group">
                            <label class="control-label" for="login">Эл. почта</label>
                            <div class="controls">
                                <input type="text" id="client_login_email" name="email" class="required" />
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: -12px;">
                            <label class="control-label" for="password">Пароль</label>
                            <div class="controls">
                                <input type="password" id="client_login_password" name="password" class="required" />
                                <a class="remember" id="client_remember" href="#" style="font-size: 11px;">Забыли пароль?</a>
                            </div>
                        </div>
                        <div id="client_login_regmsg">
                            Ваш аккаунт не найден. <a href="javascript:;" onclick="$('#client_reg').click();">Зарегистрироваться?</a>
                        </div>
                        <input type="submit" class="button" id="client_login_submit" value="Войти" />
                    </form>
                </div>

                <!-- Форма напоминания пароля -->
                <div id="client_remember_block" style="display: none;">
                    <a class="close" href="#"></a>
                    <img src="/media/i/ajax_loader.gif" class="block_ajax_loader">
                    <h2>Восстановить доступ</h2>
                    <p id="client_remember_error" class="block_error"></p>
                    <p id="client_remember_info" class="block_info"></p>
                    <form id="client_remember_form" class="form-horizontal" action="javascript:void(null);" style="width: 300px;" method="post">
                        <input name="action" type="hidden" value="remember" />
                        <div class="control-group">
                            <label class="control-label" for="email">Эл. почта</label>
                            <div class="controls">
                                <input type="text" id="client_remember_email" name="email" class="required">
                            </div>
                        </div>
                        <input type="submit" class="button" id="client_remember_submit" value="Напомнить" />
                    </form>
                </div>

                <!-- Форма регистрации -->
                <div id="client_reg_block" style="display: none;">
                    <a class="close" href="#"></a>
                    <img src="/media/i/ajax_loader.gif" class="block_ajax_loader">
                    <h2>Регистрация</h2>
                    <p id="client_reg_error" class="block_error"></p>
                    <p id="client_reg_info" class="block_info"></p>
                    <form id="client_reg_form" class="form-horizontal" action="javascript:void(null);" style="width: 300px;" method="post">
                        <input name="action" type="hidden" value="reg" />
                        <div class="control-group">
                            <label class="control-label" for="name">Имя</label>
                            <div class="controls">
                                <input type="text" id="client_reg_name" name="name" class="required">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="email">Эл. почта</label>
                            <div class="controls">
                                <input type="text" id="client_reg_email" name="email" class="required">
                            </div>
                        </div>
                        <input type="submit" class="button" id="client_reg_submit" value="Регистрация" />
                    </form>
                </div>

            </div>

            {if $container.error && !$client.auth}
                <div id="global_error">
                    {$container.error|escape}
                    <img src="/media/i/cross.png" style="float: right; cursor: pointer;" onclick="$('#global_error').hide('medium');">
                </div>f
            {/if}

            <div id="search_block" {if $container.multiple_search}style="height: 197px;" {/if}>
                <div class="bg" {if $container.multiple_search}style="height: 178px;" {/if}>
                    <a id="logo" href="/"></a>
                    <form action="/search/" id="search_form" method="{if $container.multiple_search}post{else}get{/if}">
                        <div class="wrap">
                            <input type="text" name="q" id="search_input" class="custom_placeholder" tabindex="1" data-placeholder="Поиск среди {$container.count_comp|escape} комплектующих от {$container.count_users|escape} поставщиков" {if !empty($container.query) && !$container.multiple_search} value="{$container.query|escape}"{/if} {if $container.multiple_search}style="display: none;"{/if}>

                            <textarea rows="4" id="multiple_input" name="multiple_q" {if $container.multiple_search}style="display: inline-block;"{/if}
                                      placeholder="Скопируйте сюда список необходимых комплектующих"
                                    >{if !empty($container.query) && $container.multiple_search}{trim($container.query)|escape}{/if}</textarea>

                            <span class="clear_search"{if !empty($container.query)} style="display:block;"{/if}></span>
                            <img class="img hide" src="/media/i/load.gif" alt="Идет поиск">

                            <div class="search_select">
                                <input id="single_search" type="radio" name="search_select" value="single" {if !$container.multiple_search}checked{/if}>
                                <label for="single_search">по одному компоненту</label>
                                <input id="multiple_search" type="radio" name="search_select" value="multiple" {if $container.multiple_search}checked{/if}>
                                <label for="multiple_search">по списку компонентов</label>
                            </div>

                        </div>
                        <a class="button" tabindex="2" id="go_search" href="#"><span>Найти</span></a>
                    </form>
                </div>
            </div>
        </div>
        <div id="container">

        {if $container.current == 'index'}

            {$container.content nofilter}

        {else}

        {if $container.no_breadcrumbs != true}
            <div class="wrap">
                {include file = "breadcrumbs.tpl"}
            </div>
        {/if}

          {if $container.current == 'static'}
              <div class="wrap">
                  <h1 style="width: 800px;">{$container.page_title|escape}</h1>
                  <div class="column_right">

                      {if !empty($container.nav_links[0])}
                          {if !empty($container.submenu[$container.nav_links[0]])}
                              <div class="heading">
                                  <ul>

                                      {foreach $container.submenu[$container.nav_links[0]] as $item}
                                          <li><a href="/{$container.nav_links[0]|escape}/{$item.link|escape}/"{if !empty($item.selected)} class="selected"{/if} title="{$item.title|escape}">{$item.title_short|escape}</a></li>
                                      {/foreach}

                                  </ul>
                              </div>
                          {elseif !empty($container.submenu.info)}
                              <div class="heading">
                                  <ul>

                                      {foreach $container.submenu.info as $item}
                                          <li><a href="/info/{$item.link|escape}/"{if !empty($item.selected)} class="selected"{/if} title="{$item.title|escape}">{$item.title_short|escape}</a></li>
                                      {/foreach}

                                  </ul>
                              </div>
                          {/if}
                      {/if}


                  </div>
                  <div class="column_left">
                      <div class="static_text" >

                          {$container.content nofilter}

                      </div>
                  </div>
              </div>
          {else}

              {$container.content nofilter}

          {/if}

        {/if}

        </div>
        <div id="footer">
            <p class="copyright">&copy;&nbsp;ИТС &laquo;Электронные компоненты&raquo;, <nobr>1999&ndash;{$container.year|escape}.</nobr> Информация для <a href="/info/buyers/">покупателей</a>, <a href="/info/sellers/">поставщиков</a> и <a href="/info/advertisers/">рекламодателей</a></p>
            <p class="not_offer">
                Все материалы сайта носят исключительно информационный характер и&nbsp;ни&nbsp;при каких условиях не&nbsp;являются публичной офертой, определяемой положениями Статьи 437 (2) Гражданского кодекса&nbsp;РФ.
                <a href="/legal/">Пользовательское соглашение</a>
            </p>
            <div class="counters">
                <a href="https://{$container.host_mobile|escape}{$container.request_uri|escape}" id="toggle-mobile-version">
                    <img src="/media/i/mobile_version.png">Мобильная версия
                </a>
                {include file="counters.tpl"}
                <div class="to_developer">
                    <a href="#form_developer" id="to_developer">Обратная связь с разработчиками сайта</a>
                </div>
            </div>
        </div>

        <a href="#" id="to_top"><img src="/media/i/to_top.png" alt="To top"/></a>
    </div>

    <div id="basket_cnt_upd" style="position: absolute; left: 300px; top: 100px; display: none; overflow: hidden;"></div>
    
    {include file="social_poll.tpl"}

    {include file="to_developer.tpl"}

</div>

<div class="popup_subscribe js_popup_subcribe hidden">
    <div class="popup_subscribe_content">
        <div class="popup_subscribe_close js_subscribe_close">×</div>
        <div class="js_subscribe_content" data-contact-id="">
            Загрузка...
        </div>
    </div>
</div>

<!-- JS -->
<script src="/assets.js?ver={$container.js_version}" type="text/javascript"></script>
<script src="//{$chat_host|escape}/static/js/init.js?v=6" type="text/javascript"></script>
</body>
</html>
