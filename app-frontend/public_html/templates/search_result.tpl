{if $list}

    {$prev = -1|escape}

    <div id="label_order_send" style="display: none;" hidden="hidden">
        <span class="label">Заявка&nbsp;отправлена</span>
    </div>

    {foreach from=$list key=k item=item name=products}

        {if $smarty.foreach.products.first}
            <table class="search offer tmp_table"><tbody>
        {else}
            {if $item.user_id != $list.$prev.user_id}
                </tbody></table><table class="search offer tmp_table"><tbody>
            {/if}
        {/if}

        <tr data-id="{$item.id|escape}"
            data-comp_hash="{$item.comp_hash|escape}"
            data-availability="{$item.availability|escape}"
            data-city="{$item.user_city_id|escape}"
            data-country="{$item.user_country_id|escape}"
            data-user_id="{$item.user_id|escape}"
            class="show{if $item.checked && $interface == 1} checked{/if} {if $item.user_id == $list.$prev.user_id}following_item{/if}"
            data-individual-buyers="{$item.individual_buyers|escape}"
            data-price-status="{$item.price_status|escape}"
            data-with_price="{$item.with_price|escape}"
        >
            <td class="company empty {if $item.user_id == $list.$prev.user_id}hide{else}show{/if}{$for_rating = $prev|escape}{$prev = $k|escape}">
                <div class="info">
                    <div class="line">
                        <ul>
                            <li>
                                <div class="icon-phone"></div>
                                <a class="show_user_info main dotted" data-id="{$item.user_id|escape}" data-users-stat="user_id:{$item.user_id|escape},action:'profile_view',event:'click'" href="#">
                                    <strong>{$item.user_title|escape}</strong>
                                </a>
                            </li>
                            <li>
                                <a class="show_user_info phone dotted" data-id="{$item.user_id|escape}" data-users-stat="user_id:{$item.user_id|escape},action:'profile_view',event:'click'" href="#">
                                    <span>{$item.user_phone1|escape|substr:0:16}...</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                        <span class="user_rating {if $user_settings != null}registered{/if}">

                            {if $user_settings != null}

                                {if $user_settings.search_users_white_list && in_array($item.user_id, $user_settings.search_users_white_list)}
                                    <span class="thumb-up-filled" title="Поставщик в белом списке"></span>
                                {else}
                                    <span class="thumb-up {if $item.user_id !== $list.$for_rating.user_id}hide_rating{/if}" title="Занести поставщика в белый список"></span>
                                    <span class="thumb-down {if $item.user_id !== $list.$for_rating.user_id}hide_rating{/if}" title="Убрать поставщика из моей выдачи"></span>

                                    <span class="thumb-up-filled" title="Поставщик в белом списке" style="display: none"></span>
                                {/if}

                                {else}
                                <span class="thumb-up unregistered {if $item.user_id !== $list.$for_rating.user_id}hide_rating{/if}" title="Занести поставщика в белый список">
                                </span>
                                <span class="thumb-down unregistered {if $item.user_id !== $list.$for_rating.user_id}hide_rating{/if}" title="Убрать поставщика из моей выдачи">
                                </span>

                            {/if}
                        </span>
                    <span class="price_status status_{$item.price_status|escape}"></span>
                    <span class="user_type">{if $item.individual_buyers}ЮЛ+ФЛ{else}ЮЛ{/if}</span>
                </div>
            </td>

            {if $interface == 1}
                <td class="check workspace">
                    <span class="check_wrap">
                        <span class="custom_checkbox">&nbsp;</span>
                    </span>
                </td>
            {else}
                <td class="check workspace">
                    <div class="order_loading hidden"></div>
                    <div class="order_icon" title="Запросить коммерческое предложение"></div>
                </td>
            {/if}
            <td class="product {if $interface == 1}workspace{/if}">
                <div>
                    <span class="comp_search"></span>
                    <a href="/{$item.comp_title|encodeURIComponent}/" class="comp_title">{$item.comp_title|escape}</a>
                </div>
                {if $item.manuf_title}<span>({$item.manuf_title|escape})</span>{/if}
                {if $item.mfgdate != "0000"}<span>({$item.mfgdate|escape}&nbsp;г.)</span>{/if}
                <span>{$item.descr|escape}</span>
            </td>
            <td class="availability nobr {if $item.user_online == 1 && $item.user_allow_chat == 1}chat_online{/if}">
                {if $item.stock > 0}
                    <span class="avail-info">{$item.stock|escape} шт</span>
                {elseif $item.stock < 0}
                    <span class="avail-info">на&nbsp;складе</span>
                {else}
                    <span class="avail-info">&mdash;</span>
                {/if}
                <a href="#"
                    class="start-chat"
                    data-id="{$item.id|escape}"
                    data-contact-id="{$item.user_id|escape}"
                    data-contact-title="{$item.user_title|escape}"
                    data-comp-title="{$item.comp_title|escape}">Онлайн-чат</a>
            </td>
            <td class="remote nobr">
                {if $item.stock_remote != 0 || $item.delivery != 0}
                    <span class="remote-info">
                        {if $item.stock_remote > 0}
                            <span>{$item.stock_remote} шт</span>{if $item.delivery != 0}, {/if}
                        {/if}
                        {if $item.delivery > 0}
                            <span {if $item.stock_remote > 0}title="Срок поставки с удаленного склада"{/if}>
                                {$item.delivery|escape} {plural num=$item.delivery words="неделя|недели|недель"}
                            </span>
                        {elseif $item.delivery < 0}
                            <span>заказ</span>
                        {/if}
                    </span>
                {else}
                    &mdash;
                {/if}
            </td>
            <td class="nobr {if $interface == 1}workspace{/if}">
                {$item.prices|prepare_prices|unescape:'html'}
            </td>
        </tr>

        {if $smarty.foreach.products.last}
            </tbody></table>
        {/if}

    {/foreach}

{else}

    <tr>
        <td class="empty" colspan="6"></td>
    </tr>

{/if}
