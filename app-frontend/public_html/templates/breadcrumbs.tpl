<div id="breadcrumbs">

    {if !empty($container.breadcrumbs)}

        {foreach $container.breadcrumbs as $link => $title}

            <a href="{$link|escape}" title="{$title|escape}">{$title|escape}</a> /

        {/foreach}

    {/if}

</div>