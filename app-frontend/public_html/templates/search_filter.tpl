{if $list}
    {foreach $list as $item}
        <li class="line" data-id="{$item.comp_hash|escape}">
            <a href="#" class="comp_link dotted">{$item.title|escape}</a><span></span>
        </li>
    {/foreach}
{/if}