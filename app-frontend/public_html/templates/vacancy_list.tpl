<div class="wrap">
    <h1>{if !empty($from_user)}{$from_user|escape}{else}Вакансии{/if}</h1>
</div>
<div id="vacancy_list">

  {foreach $vacancy as $item}
    <div class="item">
        <div class="date">Дата размещения&nbsp;&mdash; {$item.date|escape}</div>
        <a class="vacancy_title" href="/vacancy/id-{$item.id|escape}.html">{$item.speciality|escape}</a>
        <div class="line">
            <div class="title">Описание</div>
            <div class="val">
                {$item.info|truncate:200:"&#8230;"|escape}
            </div>
        </div>
        <div class="line">
            <div class="title">Компания</div>
            <div class="val"><a href="/users/id-{$item.user_id|escape}.html">{$item.user_title|escape}</a></div>
        </div>

      {if $item.city}
        <div class="line">
            <div class="title">Город</div>
            <div class="val">{$item.city|escape}</div>
        </div>
      {/if}

        <div class="line">
            <div class="title">Опыт работы</div>
            <div class="val">
                {if $item.experience == 1}
                    от 1 года
                    {elseif $item.experience == 2}
                    от 2 лет
                    {elseif $item.experience == 3}
                    от 3 лет
                    {elseif $item.experience == 4}
                    от 4 лет
                    {elseif $item.experience == 5}
                    от 5 лет
                {/if}
            </div>
        </div>

      {if $item.pay}
        <div class="line">
            <div class="title">Зарплата</div>
            <div class="val">{$item.pay|escape} руб.</div>
        </div>
      {/if}

    </div>
  {/foreach}

</div>

{include file="pager.tpl"}