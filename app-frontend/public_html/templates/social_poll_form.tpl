<form id="social_poll_form" method="post">
    <input type="hidden" name="id" value="{$id}" />
    {foreach from=$questions key=key item=value}
        <div class="form-item">
            {if $value.type == "text"}
                <p>{$value.text}</p>
                <input type="hidden" name="question[{$key}]" value="{$value.text}" />
                <textarea name="answer[{$key}]" style="width: 96%;"></textarea>
            {/if}
            {if $value.type == "radio"}
                <p>{$value.text}</p>
                <input type="hidden" name="question[{$key}]" value="{$value.text}" />
                {foreach from=$value.answers key=k item=v}
                    <label><input name="answer[{$key}]" type="radio" value="{$v}"> {$v}</label>
                {/foreach}
            {/if}
        </div>
    {/foreach}
    <div class="form-item">
        <a href="#" class="button" style="margin-bottom: 20px;"><span>Отправить</span></a>
        <div style="clear:both"></div>
    </div>

</form>