<div class="wrap">
    <h1>{if $h1_title}{$h1_title|escape}{else}Новости{/if}</h1>
</div>
{if $archive_list}
<div class="heading">
    <ul>
        <li><a{if !empty($all_news)} class="selected"{/if} href="/news/{if !empty($user)}user-{$user.id|escape}.html{/if}">Все новости</a></li>

      {foreach $archive_list as $item}
        <li><a{if !empty($item.selected)} class="selected"{/if} href="/news/archive-{$item.news_date|escape}{if !empty($user)}_user-{$user.id|escape}{/if}.html">Архив за {$item.news_date|escape}</a></li>
      {/foreach}

    </ul>
</div>
{/if}
<div id="news_list">
{foreach $news as $item}

    <div class="item">

      {if $item.img != ""}
        <div class="img"><a href="/news/id-{$item.id|escape}.html" title="{$item.title|escape}"><img src="/uploads/news/{$item.img|escape}" alt="{$item.title|escape}"></a></div>
      {/if}

        <div class="text">
          {if empty($user)}
            <div class="company">{$item.user_title|escape}</div>
          {/if}
            <div class="date">{$item.date|escape}</div>
            <a class="link" href="/news/id-{$item.id|escape}.html">{$item.title|escape}</a>
        </div>
    </div>

{/foreach}
</div>

{include file="pager.tpl"}