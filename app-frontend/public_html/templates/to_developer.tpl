<div style="display: none !important;">
    <form id="form_developer" method="post">
        <h3 style="margin: 0 0 16px;">Сообщение для разработчиков</h3>
        <div class="form-item">
            <textarea name="text" id="text" rows="3" style="width: 96%;" placeholder="Ваши пожелания команде разработчиков или сообщение об ошибке на сайте"></textarea>
        </div>
        <div class="form-item">
            <a href="#" class="button"><span>Отправить</span></a>
            <div style="clear:both"></div>
        </div>

        <div id="to_developer_success">
            Сообщение успешно отправлено. Спасибо за обратную связь.
        </div>
    </form>
</div>