
{if $user}
    <div id="company_card_popup">
        <div class="padding_wrap">
            <div class="title_block">
                <div class="line">
                    <a data-id="{$user.id|escape}" class="hide_user_info dotted" href="#"><strong>{$user.title|escape}</strong></a>
                </div>

                <span class="user_rating {if $user_settings != null}registered{/if}">

                            {if $user_settings != null}

                                {if $user_settings.search_users_white_list && in_array($user.id, $user_settings.search_users_white_list)}
                                    <span class="thumb-up-filled" title="Поставщик в белом списке"></span>
                                {else}
                                    <span class="thumb-up" title="Занести поставщика в белый список"></span>
                                    <span class="thumb-down" title="Убрать поставщика из моей выдачи"></span>
                                {/if}

                                {else}
                                <span class="thumb-up unregistered" title="Занести поставщика в белый список"></span>
                                <span class="thumb-down unregistered" title="Убрать поставщика из моей выдачи"></span>

                            {/if}

                        </span>
                <span class="price_status status_{$user.price_status|escape}" title="Прайс обновлен {if $user.price_status == 1}менее недели назад{elseif $user.price_status == 2}более недели назад{elseif $user.price_status == 3}более двух недель назад{/if}"></span>
                <span class="user_type" title="Поставщик работает {if $user.individual_buyers}с юридическими и физическими{else}только с юридическими{/if} лицами">{if $user.individual_buyers}ЮЛ+ФЛ{else}ЮЛ{/if}</span>
            </div>

            <div class="company_info">

                {if $user.country_id != 0}
                    <div class="line">
                        <div class="val"><strong>{$user.country|escape}{if $user.city_id != 0}, {$user.city|escape}{/if}</strong></div>
                    </div>
                {/if}

                {if $user.address != ''}
                    <div class="line">
                        <div class="title">{$user.address|escape}</div>
                    </div>
                {/if}

                {if $user.phone != ''}
                    <div class="line">
                        <div class="title">Телефон</div>
                        <div class="val"><strong>{$user.phone|escape|nl2br}</strong></div>
                    </div>
                {/if}

                {if $user.fax != ''}
                    <div class="line">
                        <div class="title">Факс</div>
                        <div class="val"><strong>{$user.fax|escape|nl2br}</strong></div>
                    </div>
                {/if}

                {if $user.www != ''}
                    <div class="line">
                        <div class="title">Адрес сайта</div>
                        <div class="val"><a href="{$user.www|escape}" data-users-stat="user_id:{$user.id|escape},action:'profile_click_site',event:'click'" target="_blank">{$user.url_text|escape}</a></div>
                    </div>
                {/if}

                {if $user.skype != ''}
                    <div class="line">
                        <div class="title">Skype</div>
                        <div class="val"><a href="callto:{$user.skype|escape}">{$user.skype|escape}</a></div>
                    </div>
                {/if}

                {if $user.email != ''}
                    <div class="line">
                        <div class="title">Электронная почта</div>
                        <div class="val"><a href="mailto:{$user.email|escape}" data-users-stat="user_id:{$user.id|escape},action:'profile_click_email',event:'click'">{$user.email|escape}</a></div>
                    </div>
                {/if}

                {if $user.icq != ''}
                    <div class="line">
                        <div class="title">ICQ</div>
                        <div class="val"><strong>{$user.icq|escape}</strong></div>
                    </div>
                {/if}

                {if $user.viber != ''}
                    <div class="line">
                        <div class="title">Viber</div>
                        <div class="val"><strong>{$user.viber|escape}</strong></div>
                    </div>
                {/if}

                {if $user.whatsapp != ''}
                    <div class="line">
                        <div class="title">WhatsApp</div>
                        <div class="val">
                            {if $user.whatsapp|strstr:"http"}
                                <a href="{$user.whatsapp|escape}" target="_blank">{$user.whatsapp}</a>
                            {else}
                                <strong>{$user.whatsapp|escape}</strong>
                            {/if}
                        </div>
                    </div>
                {/if}

                {if $user.telegram != ''}
                    <div class="line">
                        <div class="title">Telegram</div>
                        <div class="val">
                            {if $user.telegram|strstr:"http"}
                                <a href="{$user.telegram|escape}" target="_blank">{$user.telegram}</a>
                            {else}
                                <strong>{$user.telegram|escape}</strong>
                            {/if}
                        </div>
                    </div>
                {/if}

                {if $user.facebook != ''}
                    <div class="line">
                        <div class="title">Facebook</div>
                        <div class="val">
                            {if $user.facebook|strstr:"http"}
                                <a href="{$user.facebook|escape}" target="_blank">{$user.facebook|escape}</a>
                            {else}
                                <a href="https://facebook.com/{$user.facebook|escape}" target="_blank">https://facebook.com/{$user.facebook|escape}</a>
                            {/if}
                        </div>
                    </div>
                {/if}

                {if $user.vk != ''}
                    <div class="line">
                        <div class="title">ВК</div>
                        <div class="val">
                            {if $user.vk|strstr:"http"}
                                <a href="{$user.vk|escape}" target="_blank">{$user.vk|escape}</a>
                            {else}
                                <a href="https://vk.com/{$user.vk|escape}" target="_blank">https://vk.com/{$user.vk|escape}</a>
                            {/if}
                        </div>
                    </div>
                {/if}

                {if $user.instagram != ''}
                    <div class="line">
                        <div class="title">Instagram</div>
                        <div class="val">
                            {if $user.instagram|strstr:"http"}
                                <a href="{$user.instagram|escape}" target="_blank">{$user.instagram|escape}</a>
                            {else}
                                <a href="https://instagram.com/{$user.instagram|escape}" target="_blank">https://instagram.com/{$user.instagram|escape}</a>
                            {/if}
                        </div>
                    </div>
                {/if}

                {if $user.min_order_amount != ''}
                    <div class="line">
                        <div class="title">Мин. сумма заказа</div>
                        <div class="val">{$user.min_order_amount|escape}</div>
                    </div>
                {/if}
            </div>
        </div>
        <div class="price_info">
            <strong>Прайс-лист</strong> от {$user.price_date|escape}
        </div>
    </div>
{/if}
