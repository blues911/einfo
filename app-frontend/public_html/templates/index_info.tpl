{include file = 'slider.tpl'}

<div id="index_components_analysis">
    <div class="main_title">Оцените приблизительный эффект от подключения своего склада к поиску</div>

    <div class="inner">

        <div class="steps">
            <div class="step upload">
                <div class="icon_number">
                    <div class="number">1</div>
                    <div class="icon"></div>
                </div>

                <div class="title">Загрузите прайс-лист</div>
                <div class="anot">Загрузите прайс-лист (форматы: XLS, XLSX, до 100 тыс. строк, партномера должны быть на первом листе)</div>
            </div>

            <div class="step analyze">
                <div class="icon_number">
                    <div class="number">2</div>
                    <div class="icon"></div>
                </div>

                <div class="title">После загрузки прайс-лист анализируется</div>
                <div class="anot">Результат рассчитывается на основе данных за последние полгода.</div>
            </div>

            <div class="step result">
                <div class="icon_number">
                    <div class="number">3</div>
                    <div class="icon"></div>
                </div>

                <div class="title">Результат: два показателя</div>
                <div class="anot">Первый - показывает среднее количество просмотров профиля компании, второй - среднее количество переходов на ваш сайт за месяц.</div>
            </div>

            <div class="arrow"></div>
        </div>

        <div class="excel_background_block">
            <form class="components_analysis">
                <div class="main_title">Проверьте свой склад</div>

                <div class="file_input_wrap">
                    <div class="before">Выберите файл</div>
                    <div class="after">Файл не выбран</div>
                    <input type="file"/>
                </div>

                <div class="file_info">
                    <div class="title"></div>
                    <a class="cancel_file" href="#"></a>
                </div>

                <div class="process_status">
                    <div class="message"></div>
                    <div class="progress_bar"><div class="inner"></div></div>
                </div>

                <div class="file_format_error">Выбран файл неверного формата</div>

                <div class="result">
                    <div class="ready_title">Готово!</div>

                    <div class="messages">
                        <div class="profile_view item">
                            <div class="digit"></div>
                            <div class="text"></div>
                        </div>

                        <div class="profile_click item">
                            <div class="digit"></div>
                            <div class="text"></div>
                        </div>
                    </div>

                    <a class="more_analysis" href="#">Проанализировать еще</a>
                </div>

                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>

{*
<!--
<div id="ui-promo">
    <h2>ТРИ СПОСОБА ПОИСКА И ЗАКАЗА ЭЛЕКТРОННЫХ КОМПОНЕНТОВ</h2>
    <a href="/interface/eshop/" class="ui pro">
        <h3>КАК ИНТЕРНЕТ-МАГАЗИН</h3>
        <div class="zoom" data-zoom-image="/media/i/ui_pro_screen.png"></div>
        <p>Наполняйте корзину нужными компонентами, как в обычном интернет-магазине</p>
    </a>
    <a href="/interface/tender/" class="ui oneclick">
        <h3>ОБЪЯВИТЕ ТЕНДЕР</h3>
        <div class="zoom" data-zoom-image="/media/i/ui_oneclick_screen.png"></div>
        <p>Отправка запросов сразу всем поставщикам, предлагающим нужный компонент</p>
    </a>
    <a href="/interface/oneclick/" class="ui simple">
        <h3>ЗАПРОС В ОДИН КЛИК</h3>
        <div class="zoom" data-zoom-image="/media/i/ui_simple_screen.png"></div>
        <p>Быстрая отправка запроса на одну позицию в один клик, без лишних задержек</p>
    </a>
</div>
-->
*}

<div id="news_header">Новости рынка электронных компонентов</div>
<div id="news_feed" CLASS="resizee">

{foreach $news as $item}
    <div class="item{if $item.img == ""} no_img{/if}">
      {if $item.img != ""}
        <a href="/news/id-{$item.id|escape}.html"><img src="/uploads/news/{$item.img|escape}" alt="{$item.title|escape}"></a>
      {/if}
        <div class="text">
            <span class="title">{$item.user_title|escape}</span>
            <span class="date">{$item.date|escape}</span>
            <a href="/news/id-{$item.id|escape}.html" class="link">{$item.title|escape}</a>
        </div>
    </div>
{/foreach}

</div>
<div class="sticker info" style="margin-left: auto; margin-right: auto;">
    <div class="shadow"></div>
    <span class="icon"></span>
    <span class="text">Смотрите также — <a href="/search/">инструкция по расширенному поиску на сайте</a></span>
</div>
{literal}
<script type="text/javascript">

    function zoom(elem)
    {
        var offset_over = 15;
        var elem = $(elem);
        var zoom = $('<div class="lens"></div>');
        zoom.css({
            'background-image': 'url("' + elem.attr('data-zoom-image') + '")',
            'background-repeat': 'no-repeat'
        });
        zoom.appendTo(elem);

        elem.on('mousemove', function(e)
        {
            var x = e.pageX;
            var y = e.pageY;

            var elem_position = $(this).offset();
            var elem_width = $(this).width();
            var elem_height = $(this).height();
            var elem_left = elem_position.left;
            var elem_top = elem_position.top;
            var elem_right = elem_left + elem_width;
            var elem_bottom = elem_top + elem_height;

            var elem_x = x - elem_left;
            var elem_y = y - elem_top;

            var offset_x = elem_x / elem_width * (offset_over * 2 + 100) - offset_over;
            var offset_y = elem_y / elem_height * (offset_over * 2 + 100) - offset_over;

            if (x > elem_left && x < elem_right && y > elem_top && y < elem_bottom)
            {
                zoom.css({
                    'left': elem_x + 'px',
                    'top': elem_y + 'px',
                    'background-position': offset_x + '% ' + offset_y + '%'
                }).show();
            }
            else
            {
                zoom.hide();
            }
        });
        elem.on('mouseleave', function()
        {
            zoom.hide();
        });
    }

    document.addEventListener('DOMContentLoaded', function()
    {
        $('.ui > .zoom').each(function()
        {
            zoom(this);
        });
    });


</script>
{/literal}
