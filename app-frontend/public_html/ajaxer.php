<?php

header('Content-type: text/html; charset=utf-8');

if (!empty($_COOKIE[session_name()]) || !empty($_COOKIE['basket_key']) || !empty($_COOKIE['user_settings']) || $_SERVER['REQUEST_METHOD'] == 'POST')
{
    session_start();
}
elseif ($_GET['x'] != 'assets')
{
    header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
    header("Pragma: no-cache");
}

// подключение основных модулей
include_once('../../config.php');
include_once(PATH_FRONTEND . '/php/function.php');
include_once(PATH_FRONTEND . '/php/adodb/adodb.inc.php');
include_once(PATH_FRONTEND . '/php/smarty/Smarty.class.php');
include_once(PATH_FRONTEND . '/php/classes/class.Pager.php');
include_once(PATH_FRONTEND . '/php/classes/class.MyDate.php');
include_once(PATH_FRONTEND . '/php/classes/class.phpmailer.php');

// подключение к БД
$DB_CONNECT = array(
    'master' => DB_MAIN_DSN
);
list($DB_master, $DB) = database_connect($DB_CONNECT);
$ADODB_CACHE_DIR = PATH_TMP . '/adodb_cache';

if ($_SERVER['REMOTE_ADDR'] == '109.195.7.194' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1')
{
    //$DB->debug = true;
}

//include_once("php/load_tracker.php");

// дата
$date = new MyDate();

// шаблонизатор
$smarty = new Smarty;
$smarty->template_dir = PATH_FRONTEND . '/templates';
$smarty->compile_dir = PATH_TMP . '/tpl-einfo';
$smarty->error_reporting = 6135;
//$smarty->escape_html = true;

// Проверяем авторизацию
$client = array();
$container['auth'] = false;
if (!empty($_COOKIE['client_id']) && !empty($_COOKIE['auth_hash']))
{
    // Проверяем пару client_id и auth_hash
    $client = $DB->GetRow('
        SELECT * FROM clients WHERE id = ? AND auth_hash = ?
      ', array(
            $_COOKIE['client_id'],
            $_COOKIE['auth_hash']
        )
    );

    if (!empty($client))
    {
        // Обновляем дату последнего входа клиента
        $DB->Execute('
            UPDATE clients SET date_last_login = ? WHERE id = ?
        ', array(
            date('Y-m-d H:i:s'),
            $client['id']
        ));

        $container['auth'] = true;
    }
}

$client['auth'] = $container['auth'];

// Настройки
include_once('content/objects/settings.php');


// -----------------------------------------------------
try
{
    if (empty($_SERVER['HTTP_REFERER'])
        && !preg_match("/^\/(sitemap)|(users\/price_[0-9]+)|(uploads\/)|(telegram\/)|(api\/)|(assets\.(css|js))/",$_SERVER["REQUEST_URI"])
        && !(!empty($_GET['x']) && in_array($_GET['x'], array('telegram', '_clear_templates')))
    ) {
        throw new Exception('Error 403', 403);
    }

    if (isset($_GET['x']) && preg_match('/^([-a-z0-9_]+)(\.([-a-z0-9_]+))?$/i', $_GET['x'], $matches))
    {
        if (isset($matches[1], $matches[3]) && file_exists('content/ajax/' . $matches[1] . '.' . $matches[3] . '.php'))
        {
            include_once ('content/ajax/' . $matches[1] . '.' . $matches[3] . '.php');
        }
        elseif (isset($matches[1]) && file_exists('content/ajax/' . $matches[1] . '.php'))
        {
            include_once ('content/ajax/' . $matches[1] . '.php');
        }
        else throw new Exception('Error 404', 404);
    }
    else throw new Exception('Error 404', 404);
}
catch (Exception $e)
{
    if ($e->getCode() == 404)
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
        exit;
    }
    elseif ($e->getCode() == 403)
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
        exit;
    }
    elseif ($e->getCode() == 500)
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
        exit;
    }    
}