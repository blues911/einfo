$(document).ready(function()
{
    var change_timeout = {};

    // +1/-1 компонент
    $('table.basket_list').on('click', '.icon.plus, .icon.minus', function(e)
    {
        e.preventDefault();

        var id = $(this).attr('data-id');
        var action = $(this).data('action');
        var current_cnt = parseInt($('#basket_item_cnt_' + id).val());

        clearTimeout(change_timeout[id]);

        if (isNaN(current_cnt) || current_cnt <= 0)
        {
            current_cnt = 1;
        }

        $('#basket_item_cnt_' + id).val(current_cnt);
        $('#basket_form').find('[data-id="' + id + '"]').val(current_cnt);

        change_timeout[id] = setTimeout(function()
        {
            $.ajax({
                url: '/ajaxer.php?x=basket',
                dataType: 'json',
                data: { 'action': 'update', id: id, 'cnt': current_cnt },
                type: 'POST',
                success: function(data)
                {
                    if (!data)
                    {
                        alert('Произошла ошибка, попробуйте еще раз');
                    }
                }
            });
        }, 2000);
    });

    // Групповое изменение компонентов
    $('#all_comp_val').on('keyup change', function()
    {
        var cur = $(this).val();

        if (cur.toString() !== $(this).data('cur').toString() && cur != '')
        {
            clearTimeout(change_timeout['all']);

            cur = parseInt($(this).val());

            if (isNaN(cur) || cur <= 0)
            {
                cur = 1;
            }

            $(this).val(cur).data('cur', cur);
            $('table.basket_list').find('.comp_val').val(cur);

            change_timeout['all'] = setTimeout(function()
            {
                $.ajax({
                    url: '/ajaxer.php?x=basket',
                    dataType: 'json',
                    data: { 'action': 'update_all', 'cnt':cur },
                    type: 'POST',
                    success: function(data)
                    {
                        return data;
                    }
                });
            }, 2000);
        }
    });

    // Изменение количества компонентов
    $('.comp_val').on('change', function()
    {
        var input = $(this);
        var id = input.data('id');

        var cnt = parseInt($(this).val());
        if (isNaN(cnt) || cnt <= 0)
        {
            cnt = 1;
        }

        input.val(cnt);
        $('#basket_form').find('[data-id="' + id + '"]').val(cnt);

        $.ajax({
            url: '/ajaxer.php?x=basket',
            dataType: 'json',
            data: { 'action': 'update', id: id, 'cnt': cnt },
            type: 'POST',
            success: function(data)
            {
                if (data)
                {
                    print_basket_count(data.count);
                }
                else
                {
                    alert('Произошла ошибка, попробуйте еще раз');
                }
            }
        });
    });

    // Удаление компонента
    $('table.basket_list').on('click', '.icon.delete', function(e)
    {
        var id = $(this).data('id');
        var row = $(this).closest('tr');
        var user_id = row.data('user_id');

        $.ajax({
            url: '/ajaxer.php?x=basket',
            dataType: 'json',
            data: { 'action': 'remove', id: id },
            type: 'POST',
            success: function(data)
            {
                if (data)
                {
                    if (data.count > 0)
                    {
                        row.hide().remove();

                        // Удаление поставщика из списка, если компонент был удален
                        var rows = $('table.basket_list').find('tbody tr');
                        var user_remove = true;

                        rows.each(function()
                        {
                            if ($(this).data('user_id') == user_id)
                            {
                                user_remove = false;
                            }
                        });

                        if(user_remove)
                        {
                            $('#companies_block').find('tr[data-id="' + user_id + '"]').hide().remove();
                        }
                    }
                    else
                    {
                        window.location.href = '/info/buyers/';
                    }

                    print_basket_count(data.count);
                }
                else
                {
                    alert('Произошла ошибка, попробуйте еще раз');
                }
            }
        });

        e.preventDefault();
    });

    // Удаление поставщика
    $('#companies_block').on('click', '.icon.delete', function(e)
    {
        var user_id = $(this).data('user_id');
        var row = $(this).closest('tr');
        var rows = $('table.basket_list').find('tbody tr');
        var list = [];

        rows.each(function()
        {
            if ($(this).data('user_id') == user_id)
            {
                list.push($(this).data('id'));
            }
        });

        $.ajax({
            url: '/ajaxer.php?x=basket',
            dataType: 'json',
            data: { 'action': 'remove_all', 'ids': list },
            type: 'POST',
            success: function(data)
            {
                if (data)
                {
                    print_basket_count(data.count);

                    if (data.count > 0)
                    {
                        row.hide().remove();

                        rows.each(function()
                        {
                            if ($(this).data('user_id') == user_id)
                            {
                                $(this).hide().remove();
                            }
                        });
                    }
                    else
                    {
                        window.location.href = '/info/buyers/';
                    }
                }
                else
                {
                    alert('Произошла ошибка, попробуйте еще раз');
                }
            }
        });

        e.preventDefault();
    });

    // Подсветка строчки удалить при наведении
    $('table.delete .icon.delete').hover(
        function ()
        {
            $(this).closest('tr').addClass('del');
        },
        function ()
        {
            $(this).closest('tr').removeClass('del');
        }
    );

    // Форма оформления заявки
    $('.fancy_order').fancybox({
        padding : 10,
        wrapCSS: 'rounded',
        closeBtn: false,
        beforeShow: function() { }
    });

    // Fancybox кнопка закрыть
    $('#order_block').find('.close').on('click', function(e)
    {
        $.fancybox.close();

        e.preventDefault();
    });

    // Отправка заявки
    $('#order_submit').on('click', function(e)
    {
        var button = $(this);
        var form = $('#order_form');

        if (!button.hasClass('disabled'))
        {
            button.addClass('disabled');

            form.find('.required input').each(function()
            {
                if ($.trim($(this).val()) == '')
                {
                    $(this).closest('.control-group').addClass('error');
                }
                else
                {
                    $(this).closest('.control-group').removeClass('error');
                }
            });

            if(form.find('.required.error').length)
            {
                button.removeClass('disabled');
            }
            else
            {
                $.ajax({
                    url: '/ajaxer.php?x=basket',
                    dataType: 'json',
                    data: $('#basket_form').serialize() + '&action=order&' + form.serialize(),
                    type: 'POST',
                    success: function(data)
                    {
                        if (data)
                        {
                            if (data.send)
                            {
                                form.find(':input').attr('disabled', true);
                                document.location.href = '/basket/?success';
                            }
                            else
                            {
                                var error;
                                if (data.error)
                                {
                                    error = data.error;
                                }
                                else
                                {
                                    error = 'Произошла ошибка, попробуйте еще раз';
                                }
                                alert(error);
                                button.removeClass('disabled');
                            }

                        }
                        else
                        {
                            alert('Произошла ошибка, попробуйте еще раз');
                            button.removeClass('disabled');
                        }
                    },
                    error: function()
                    {
                        alert('Произошла ошибка, попробуйте еще раз');
                        button.removeClass('disabled');
                    }
                });
            }
        }

        e.preventDefault();
    });
});