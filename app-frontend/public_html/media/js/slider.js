$(function(){
    var button_list = $(document).find('.block-slider_menu-item');
    var tab_list = $(document).find('.slider-tab_item');
    var interval;

    $(document).ready(function(){
        $(document).on('click', '.block-slider_menu-item', function(){
            change_tab(
                $(this)
            );
        });

        $(document).on('mouseover', '#block_slider', stop_slider);

        $(document).on('mouseout', '#block_slider', start_slider);

        start_slider();
    });

    function change_tab(button)
    {
        var target = button.data('target');

        // Если элемент активен делаем выход
        if (button.hasClass('active')) return false;

        $('.block-slider_menu-item').removeClass('active');
        $('.slider-tab_item').removeClass('active');

        button.addClass('active');

        let id = $('#' + button.data('target'));
        id.addClass('active');
    }

    function start_slider()
    {
        // Если нет элементов на странице, делаем выход
        if (button_list.length == 0) return false;

        interval = setInterval(function(){
            var current = $(document).find('.block-slider_menu-item.active'),
                index = button_list.index(current),
                total = button_list.length,
                next = (index + 1 == total) ? 0 : index + 1;

            change_tab(
                button_list.eq(next)
            );
        }, 8000);
    }

    function stop_slider()
    {
        clearInterval(interval);
    }
});