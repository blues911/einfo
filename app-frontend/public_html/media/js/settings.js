document.addEventListener('DOMContentLoaded', function()
{
    $('#settings_company .hidden_checkbox').on('change', function()
    {
        var input = $(this);
        var label = input.closest('.custom_label');

        label.toggleClass('checked', input.prop('checked'));
    });


    // Очистить форму
    $('#settings_company .clr').on('click', function(e)
    {
        $('#settings_company .hidden_checkbox').removeAttr('checked').closest('.custom_label').removeClass('checked');

        e.preventDefault();
    });

    // Отправить форму
    $('.settings_form_submit').on('click', function(e)
    {
        $('#settings_form').submit();

        e.preventDefault();
    });

    $(window).load(function()
        {
            $('#save_sucess').fadeIn();
        }
    );
});
