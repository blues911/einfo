$(window).load(function()
{
    $('#search_options').find('.filter_button').removeClass('disabled');

    var links = {efind: 'http://efind.ru/icsearch/?search=', chipfind: 'http://www.chipfind.ru/search/?part=', yandex: 'http://yandex.ru/yandsearch?site=www.einfo.ru&text=', google: 'http://www.google.ru/search?q=site:www.einfo.ru+'};

    // Простановка ссылок поиска в других поисковых системах
    $('#search_link').find('.hide_link').each(function()
    {
        var q = $('#search_link').data('q');
        var q_cp = $('#search_link').data('q_cp');

        var link = $(this).data('link');

        if (link == 'efind')
        {
            $(this).attr('href', links[link] + q_cp);
        }
        else
        {
            $(this).attr('href', links[link] + q);
        }
    });
});

$(document).on('keyup', '#search_input, #multiple_input', function(){
    var parent = $(this).parent();
    if ($(this).val().length > 0)
    {
        parent.find('.clear_search').show()
    }
    else
    {
        parent.find('.clear_search').hide();
    }
});

$(document).on('click', '.clear_search', function(){
    var search_choice = $('.search_select > input:checked').val();

    if (search_choice == 'multiple_search')
    {
        var input = $(this).parent().find('#multiple_input');
    }
    else
    {
        var input = $(this).parent().find('#search_input');
    }

    input.val(input.attr('data-placeholder')).addClass('empty');
    $(this).hide();
});

var cache = { };
var ref = { };

function render_elements()
{
    $('#searching').show();

    var prev = false;

    // Скрытие ячеек поставщиков
    for (var i in ref.rows)
    {
        cache.rows[ref.rows[i]].marked = false;

        if (prev && cache.rows[prev].visible && !cache.rows[prev].remove && cache.rows[ref.rows[i]].visible && !cache.rows[ref.rows[i]].remove)
        {
            if (cache.rows[prev].data.user_id == cache.rows[ref.rows[i]].data.user_id)
            {
                cache.rows[ref.rows[i]].marked = true;
            }
        }

        prev = ref.rows[i];
    }

    // Показ/скрытие кнопки отметить все
    var limit = 300;
    var checkbox = $('#search_result').find('thead .check');
    var button = $('#search_options').find('.button.select_all_components');

    var size = 0;
    for (var key in cache.rows)
    {
        if (cache.rows.hasOwnProperty(key) && cache.rows[key].remove == false && cache.rows[key].visible == true) size++;
    }

    if (size < limit && size > 0)
    {
        checkbox.removeClass('hide');
        button.removeClass('hide');
    }
    else
    {
        checkbox.addClass('hide');
        button.addClass('hide');
    }

    var render = { };

    $('#search_result').find('tbody tr').addClass('hide').removeClass('show').removeClass('last');

    for (var i in cache)
    {
        render[i] = { };

        // Количество элементов
        render[i].len = ref[i].length;
        render[i].rest = ref[i].length;
        render[i].count = 0;
        render[i].cursor = 0;
        render[i].step = 200;
        render[i].time_int = 100;
        render[i].visible = false;

        render[i].step_func = function(link)
        {
            var data = link;

            // Проверка количества оставшихся элементов больше шага
            if (render[data].rest - render[data].step < 0)
            {
                render[data].step = render[data].len - render[data].cursor;
            }

            while(render[data].count < (render[data].len - render[data].rest))
            {

                if(cache[data][ref[data][render[data].count]].remove == false && cache[data][ref[data][render[data].count]].visible == true)
                {
                    $(cache[data][ref[data][render[data].count]].link).removeClass('hide').addClass('show');
                    $(cache[data][ref[data][render[data].count]].link).find('.company').addClass('hide').removeClass('show');

                    render[data].visible = true;
                }
                else
                {
                    $(cache[data][ref[data][render[data].count]].link).addClass('hide').removeClass('show');
                }

                render[data].count++;

                if (render[data].len == render[data].count) {
                    custom_scroll.get_height();
                    custom_scroll.scrolling($(window).scrollTop());

                    if(data == 'rows')
                    {
                        $('#searching').hide();

                        if(render[data].visible)
                        {
                            // Показать блок поставщиков
                            $('#companies_block').show();
                            $('#search_result tbody').each(function()
                            {
                                $(this).find('tr.show:last').addClass('last');
                            });
                        }
                    }
                }
            }

            $('.search.offer').find('tr.show:first').find('.company:first').removeClass('hide').addClass('show');

            if (render[data].rest > 0)
            {
                // Осталось
                render[data].rest -= render[data].step;

                // Пройдено
                render[data].cursor += render[data].step;

                // Запуск
                render[data].interval = setTimeout(function()
                {
                    render[data].step_func(data);
                }, render[data].time_int);
            }
            else
            {
                if (data == 'rows')
                {
                    $(document).trigger('render-finish');
                }
            }
        };

        render[i].step_func(i);
    }

    setTimeout(xls_export_set_values, 500);
}

function xls_export_set_values()
{
    var form = $('.xls_export'),
        items = $('#search_result').find('tr.show'),
        query = $('#search_input').val(),
        search_choice = $('.search_select > input:checked').val(),
        items_id = [];

    if (search_choice == 'multiple')
    {
        query = '';
        var multiple = $('#multiple_input').val();
        form.find('[name="xls_multiple_q"]').val(multiple);
    }

    $.each(items, function(){
        if (!isNaN(Number($(this).data('id'))))
        {
            items_id.push(Number($(this).data('id')));
        }
    });

    form.find('[name="query"]').val(query).end()
        .find('[name="items_id"]').val(items_id);
}

function get_filter_values()
{
    // Массив значения фильтров
    var _filter = [];

    var search_options = $('#search_options');

    // Получение значения фильтров
    search_options.find('.filter_button').each(function()
    {
        if ($(this).hasClass('custom_checkbox'))
        {
            if ($(this).hasClass('checked'))
                _filter.push({title: $(this).data('type'), val: 1});
        }
        else
        {
            if ($(this).data('value') != 'all')
            {
                _filter.push({title: $(this).data('type'), val: $(this).data('value')});
            }
        }
    });

    return _filter;
}

// Функция скрывающая позиции под спойлер
function hide_positions(num)
{
    if (num == undefined)
    {
        num = 5;
    }

    $('#search_result .spoiler').remove();
    var spoiler = $('.spoiler:first');

    var last_user = {
        id: null,
        cnt: 0,
        rows: []
    };

    var rows = $('#search_result tr:not(.spoiler)');
    rows.removeClass('spoil');

    var process = function(last_user)
    {
        if (last_user.rows.length)
        {
            for (var i in last_user.rows)
            {
                last_user.rows[i].addClass('spoil');
            }

            var _spoiler = spoiler.clone();
            _spoiler.find('a:first').text('Еще ' + last_user.rows.length + ' ' + plural(last_user.rows.length, ['предложение', 'предложения', 'предложений']));
            _spoiler.attr('data-user-id', last_user.id).insertBefore(last_user.rows[0]);
            _spoiler.on('click', function()
            {
                var spoil_rows = $('#search_result tr[data-user_id=' + $(this).attr('data-user-id') + ']');
                spoil_rows.show(1000).removeClass('spoil').css('display', '');
                $(this).remove();
            });
        }
    };

    rows.each(function()
    {
        var row = $(this);

        row.css({'background-color': 'transparent'});

        if (row.hasClass('hide'))
        {
            return true;
        }

        if (last_user.id == Number(row.attr('data-user_id')))
        {
            last_user.cnt++;

            if (last_user.cnt > num)
            {
                last_user.rows.push(row);
            }
        }
        else
        {
            process(last_user);

            last_user.id = Number(row.attr('data-user_id'));
            last_user.cnt = 1;
            last_user.rows = [];
        }

        return true;
    });

    process(last_user);
}

// Сохранение настроек для белого/черного списка пользователя
function change_user_lists(user_id, url, action) {

    return new Promise(function(resolve, reject) {
        $.ajax({
            url: url,
            dataType: 'json',
            data: {user_id: user_id, action: action},
            type: 'POST',
            success: function(data)
            {
                data.success ? resolve() : reject(data);
            }
        });
    });

};

$(document).on('render-finish', function()
{
    var num = Number($('#search_result').attr('data-spoiler-num'));
    hide_positions(num);
});

$(document).ready(function()
{
    // Fancybox
    $('.fancybox').fancybox({
        overlayOpacity: 0.5,
        padding: 20,
        fitToView: false,
        helpers		: {
            title	: { type: 'inside', position: 'top' }
        },
        beforeLoad : function()
        {
            var pdf = $('#pdf');
            if (pdf.data('href'))
            {
                this.title = '<div class="fancy_link_wrap"><a target="_blank" href="' + pdf.data('href') + '" class="fancy_link">Полная техническая документация</a> <span class="size">(PDF, ' + pdf.data('size') + ')</span></div>';
            }
        }
    });

    // Исходные данные
    var tmp = [
        {title: 'rows', link: $('#search_result').find('tbody tr') },
        {title: 'comps', link: $('#filter_block').find('ul .line') },
        {title: 'users', link: $('table.delete tbody tr') }
    ];

    // Кеширование
    //var cache = { };
    var i = 0;
    while(i < tmp.length)
    {
        cache[tmp[i].title] = { };
        ref[tmp[i].title] = [];

        var j = 0;
        while(j < tmp[i].link.length)
        {
            cache[tmp[i].title][$(tmp[i].link[j]).attr('data-id')] = {
                link: $(tmp[i].link[j]),
                data: $(tmp[i].link[j]).data(),
                visible: true,
                remove: false,
                marked: false
            };

            ref[tmp[i].title].push(
                $(tmp[i].link[j]).attr('data-id')
            );

            j++;
        }

        i++;
    }

    // Фиксированная панель и плавающий блок
    custom_scroll = new function()
    {
        var _this = this,
            block_offset = 331,
            h_container,
            h_filter,
            h_table,
            h_window,
            h_footer,
            h_main;

        // Функция получения высот блоков
        _this.get_height = function()
        {
            h_container = $('#container').height();
            h_filter = $('#filter_block').height();
            h_table = $('#search_result').height();
            h_window = $(window).height();
            h_footer = $('#footer').height() + 50;
            h_main = $('#main').height();

        };

        // Функция фиксирования панели фильтров и плавующего фильтра
        _this.scrolling = function(current_scroll)
        {
            if (current_scroll >= 268)
            {
                if (h_table > h_filter )
                {
                    if (h_window > h_filter + 66 + h_footer)
                    {
                        $('#filter_block').css('position', 'fixed').css('top', '66px');
                    }
                    else
                    {
                        if (current_scroll + h_window  >= h_main - h_footer)
                        {
                            $('#filter_block').css('position', 'absolute').css('bottom', '50px');
                        }
                        else if (current_scroll + h_window >= h_filter + block_offset + 52)
                        {
                            $('#filter_block').css('position', 'fixed').css('bottom', '30px');
                        }
                        else
                        {
                            $('#filter_block').css('position', 'relative').css('top', 'auto').css('bottom', 'auto');
                        }
                    }
                }

                $('#search_options').addClass('fixed');
                var width = $('#search_options_wrap').width() + 50;
                var margin = -width / 2;
                _this.block_pos(width, margin);
            }
            else {
                $('#filter_block').css('position', 'relative').css('top', 'auto').css('bottom', 'auto');
                $('#search_options').removeClass('fixed');
                _this.block_pos('100%', 0);
            }
        };

        // Функция позиционирования панели фильтров
        _this.block_pos = function(width, margin)
        {
            $('#search_options').width(width).css('margin-left', margin);
        };

        // Инициализация
        $(window).load(function()
        {
            _this.get_height();

            $(window).scroll(function()
            {
                _this.scrolling($(window).scrollTop());
            });
        });

        // Пересчет параметров при ресайзе
        $(window).resize(function()
        {
            _this.get_height();
            _this.scrolling($(window).scrollTop());
        });
    };

    // Выбор фильтра
    function filter_action()
    {
        var cross_filter_empty = $('#cross_filter_empty');
        var search_options = $('#search_options');
        var options_clear_button = $('#search_options').find('.button.clear');

        var without_component_size = 0;

        // Выбранный компонент в левой колонке
        var selected_comp = $('#filter_block').find('li a.selected');
        var comp_selected_hash = selected_comp.closest('li').data('id');

        // Массив значений фильтров
        var _filter = get_filter_values();

        // Фильтрация элементов
        if (_filter.length) {
            // Показать кнопку сбросить
            options_clear_button.removeClass('hide');

            $('.select_all_components').removeClass('checked');

            // Скрытие всех поставщиков
            for (var i in cache.users) {
                cache.users[i].visible = false;
            }

            // Скрытие всех компонентов, кроме выбранного
            for (var i in cache.comps) {
                if (comp_selected_hash && i == comp_selected_hash)
                    cache.comps[i].visible = true;
                else
                    cache.comps[i].visible = false;
            }

            // Показ всех предложений
            for (var i in cache.rows) {
                if (cache.rows[i].remove == false) {
                    cache.rows[i].visible = true;
                }
            }

            // Фильтрация предложений
            for (var i in cache.rows) {
                // Фильтры сверху
                if (cache.rows[i].remove == false) {
                    var j = 0;
                    while (j < _filter.length) {

                        if (_filter[j].title == 'comp_hash')
                        {
                            cache.rows[i].comp_filter_visible = true;
                        }

                        if (cache.rows[i].visible == true
                            && cache.rows[i].data[_filter[j].title] == _filter[j].val)
                        {
                            cache.rows[i].visible = true;
                        }
                        else {
                            cache.rows[i].visible = false;
                        }

                        j++;
                    }

                    // Показ поставщиков
                    if (cache.rows[i].visible != undefined && cache.rows[i].visible) {

                        // Здесь строки, прошедшие основную фильтрацию.
                        without_component_size++;

                        // Если выбран компонент слева, то проверяем их на соответствие */
                        if (!comp_selected_hash ||
                            (comp_selected_hash && cache.rows[i].data.comp_hash == comp_selected_hash))
                        {
                            if (cache.rows[i].data.user_id != undefined) {
                                if (cache.users[cache.rows[i].data.user_id] != undefined) {
                                    cache.users[cache.rows[i].data.user_id].visible = true;
                                }
                            }
                        }
                        else
                        {
                            cache.rows[i].visible = false;
                        }

                        // Компоненты в левой колонке
                        cache.comps[cache.rows[i].data.comp_hash].visible = true;
                    }
                }
            }

            render_elements();
        }
        else
        {
            // Показать кнопку сбросить
            options_clear_button.addClass('hide');

            // Показ всех поставщиков
            for (var i in cache.users)
            {
                if (cache.users[i].remove == false)
                {
                    cache.users[i].visible = true;
                }
            }

            // Показ всех компонентов
            for (var i in cache.comps)
            {
                if (cache.comps[i].remove == false)
                {
                    cache.comps[i].visible = true;
                }
            }

            // Показ всех предложений
            for (var i in cache.rows)
            {
                if (cache.rows[i].remove == false)
                {
                    cache.rows[i].visible = true;
                    without_component_size++;
                }
            }

            // Фильтров нет, но мог остаться выбор компонента
            if (comp_selected_hash)
            {
                selected_comp.click();
            }
            else
            {
                render_elements();
            }
        }

        var size = 0;

        for (var key in cache.rows)
        {
            if (cache.rows.hasOwnProperty(key)
                && cache.rows[key].remove == false
                && cache.rows[key].visible == true)
            {
                size++;
            }
        }

        $('#filter_block').find('.main .cnt').text(without_component_size);

        if (!size)
        {
            $('#cross_filter_empty').fadeIn();
            $('#companies_block').hide();
        }
        else
        {
            cross_filter_empty.fadeOut();
            $('#companies_block').show();
        }

        custom_scroll.get_height();
        custom_scroll.scrolling($(window).scrollTop());
    }

    // Клик на "Все компоненты"
    $(document).on('click', '#filter_block .main', function(){
        var link = $(document).find('#search_options .button.clear');
        if (link.is(':visible')) window.location = link.attr('href');
    });

    $('#search_options .custom_label').find('.text, .custom_checkbox').on('click', function(){
        var custom_checkbox = $(this).closest('.custom_label').find('.custom_checkbox');

        custom_checkbox.toggleClass('checked');

        $('#cross_filter_empty').hide();

        filter_action();
    });

    var search_options = $('#search_options');

    search_options.find('.custom_label').on('click', function(){
        var checkbox = $(this).find('.custom_checkbox');
        var value = checkbox.hasClass('checked') ? 'Да' : 'Не важно';
        var title = checkbox.data('type');
    });

    search_options.find('.custom_checkbox').on('click', function(){
        var value = $(this).hasClass('checked') ? 'Да' : 'Не важно';
        var title = $(this).data('type');
    });

    search_options.find('.filter_button').on('click', function(e)
    {
        if (!$(this).hasClass('disabled'))
        {
            $('#search_options').find('.popup').off('click.button').hide();
            $('#main').off('click.filter');

            var button = $(this);
            var filter = button.data('filter');
            var popup = $('#search_options').find('.popup.' + filter).show();

            // Выбор значения фильтра
            popup.on('click.button', 'a.dotted', function(e)
            {
                $('#info_large_result').hide();

                if (!$(this).hasClass('lock'))
                {
                    // Смена значения фильтра
                    button.data('value', $(this).data('val')).data('type', $(this).data('type')).find('.title').html($(this).data('title'));
                    popup.off('click.button').hide();

                    filter_action();
                }
                e.stopPropagation();
                e.preventDefault();
            });

            $('#main').on('click.filter', function(e)
            {
                popup.off('click.button').hide();
                $('#main').off('click.filter');
            });

        }

        e.stopPropagation();
        e.preventDefault();
    });

    $('.popup').on('click', function(e){  e.stopPropagation(); } );

    // Фильтр по компонентам
    $('#filter_block').on('click', 'li a', function(e)
    {
        e.preventDefault();
        var comp = $(this);

        if (comp.hasClass('selected')) return;

        einfo_stat('search_comp_select', comp.text());

        var search_options = $('#search_options');
        var clear_search_options = search_options.find('.button.clear');

        $('#info_large_result').hide();
        $('#cross_filter_empty').hide();

        // Скрыть блок поставщиков
        $('#companies_block').hide();

        // Снятие галочки выделеть все компоненты
        $('.select_all_components').removeClass('checked');

        var comp_hash = $(this).closest('li').data('id'), link = $(this);

        // Выделение пункта в списке
        $('#filter_block').find('li a.selected').removeClass('selected');

        link.addClass('selected');

        // Масив значений фильтров
        var _filter = get_filter_values();

        // Добавление компонента в фильтр
        if (comp_hash)
        {
            _filter.push({title: 'comp_hash', val: comp_hash});
        }

        // Фильтрация элементов
        if (_filter.length)
        {
            // Показать кнопку сброса фильтров
            clear_search_options.removeClass('hide');

            // Скрытие всех поставщиков
            for (var i in cache.users)
            {
                cache.users[i].visible = false;
            }

            // Показ всех предложений
            for (var i in cache.rows)
            {
                if (cache.rows[i].remove == false)
                {
                    cache.rows[i].visible = true;
                }
            }

            // Фильтрация предложений
            for (var i in cache.rows)
            {
                if (cache.rows[i].remove == false)
                {
                    var j = 0;

                    while(j < _filter.length)
                    {
                        if (cache.rows[i].visible == true && cache.rows[i].data[_filter[j].title] == _filter[j].val)
                        {
                            cache.rows[i].visible = true;
                        }
                        else
                        {
                            cache.rows[i].visible = false;
                        }

                        j++;
                    }

                    // Показ поставщиков
                    if(cache.rows[i].visible)
                    {
                        if (cache.users[cache.rows[i].data.user_id] != undefined)
                        {
                            cache.users[cache.rows[i].data.user_id].visible = true;
                        }
                    }
                }
            }

            render_elements();
        }
        else
        {
            // Скрыть кнопку сброса фильтров
            clear_search_options.addClass('hide');

            // Показ всех поставщиков
            for (var i in cache.users)
            {
                if (cache.users[i].remove == false)
                {
                    cache.users[i].visible = true;
                }
            }

            // Показ всех предложений
            for (var i in cache.rows)
            {
                if (cache.rows[i].remove == false)
                {
                    cache.rows[i].visible = true;
                }
            }

            render_elements();
        }

        var size = 0;

        for (var key in cache.rows)
        {
            if (cache.rows.hasOwnProperty(key)
                && cache.rows[key].remove == false
                && cache.rows[key].visible == true)
            {
                size++;
            }
        }

        if (!size)
        {
            $('#cross_filter_empty').fadeIn();
            $('#companies_block').hide();
        }

        custom_scroll.get_height();
        custom_scroll.scrolling($(window).scrollTop());

        $('body,html').animate({ scrollTop: $('#search_result').offset().top - 110 }, 'fast');
    });

    // Подсветка строчки удалить поставщика
    $('table.delete .icon.delete').hover(
        function ()
        {
            $(this).closest('tr').addClass('del');
        },
        function ()
        {
            $(this).closest('tr').removeClass('del');
        }
    );

    /*-------------------------------------------------------------------------------------*/
    // Показать/скрыть иконки для добавления в список для первой записи поставщика
    $('tr.show').hover(
        function(){
            $(this).find('.hide_rating').css( 'display', 'inline-block');
        },
        function(){
            $(this).find('.hide_rating').css( 'display', 'none');
    });

    // Прозрачность для первой записи поставщика при ховере на последующие записи
    $('tr.following_item').hover(
        function(){
            $(this).siblings(':first').find('td.company').css('transition', 'opacity 0.5s ease-out').css('opacity', '0.4');
        },
        function(){
            $(this).siblings(':first').find('td.company').css('opacity', '1');
    });

    // Открытие формы для не зарегестрированных пользователей
    $('#show_messag_for_unregistered').fancybox({
        padding : 10,
        wrapCSS: 'rounded',
        closeBtn: false
    });

    // Прокрутка до шапки и показ формы регистрации
    $('.show_reg_form').on('click', function(){

        $.fancybox.close();

        setTimeout(function ()
        {
            $('html,body').stop().animate({ scrollTop: $('#client_reg').offset().top - 110 }, 1000);
            $('#client_reg').click();
        }, 1000);

        return false;
    });

    // Прокрутка до шапки и показ формы регистрации
    $('#message_unregistered .close').on('click', function(){

        $.fancybox.close();
        return false;
    });


    // Открытие белого списка при клике на значек
    $('#main').on('click', '.thumb-up-filled',  function(){
        document.location.href = '/account/white_list/';
        return false;
    });

    // Добавление в белый список
    $('.user_rating .thumb-up').on('click',  function(){

        if ($(this).hasClass('unregistered'))
        {
            $('#show_messag_for_unregistered').click();
            return false;
        }

        var user_id = $(this).closest("tr").data('user_id');

        change_user_lists(user_id, '/ajaxer.php?x=user_white_list', 'add')
            .then(
                function(){
                    $('[data-user_id="'+user_id+'"]').find('.thumb-up').hide().removeClass('hide_rating')
                        .end()
                        .find('.thumb-down').hide().removeClass('hide_rating')
                        .end()
                        .find('.thumb-up-filled').show();
                },
                function(err) {
                    console.log(err);
                }
            );
    });

    // Добавление в черный списка
    $('.user_rating .thumb-down').on('click',  function(){

        if ($(this).hasClass('unregistered'))
        {
            $('#show_messag_for_unregistered').click();
            return false;
        }

        var user_id = $(this).closest("tr").data('user_id');

        change_user_lists(user_id, '/ajaxer.php?x=user_disallow')
            .then(
            function(){
                $('[data-user-id="'+user_id+'"]').fadeOut();
                $('[data-user_id="'+user_id+'"]').fadeOut();
            },
            function(err) {
                console.log(err);
            }
        );
    });
    /* ------------------------------------------------- */

    // Поиск с учетом поставщиков работающих с физ лицами
    $('.button_block .yes').on('click',function(e)
    {
        e.preventDefault();

        var href = $(this).data('redirect_link');

        $(this).addClass('checked');

        $.ajax({
            url: '/ajaxer.php?x=user_settings',
            dataType: 'json',
            data: {
                'var': 'individual_buyers',
                'value': 1
            },
            type: 'POST',
            success: function(response)
            {
                document.location.href = href;
            },
            error: function()
            {
                document.location.href = href;
            }
        });
    });

    $('.button_block .no').on('click',function(e)
    {
        e.preventDefault();

        $(this).closest('.system_message').hide().remove();
    });

    // Однокликовый интерфейс
    $('table.offer tbody tr').removeClass('checked');

    $('#show_order').fancybox({
        padding : 10,
        wrapCSS: 'rounded',
        closeBtn: false,
        beforeShow: function()
        {
            $('#basket_item_cnt').val(1);
            $('#order_body').show();
            $('#order_msg').hide();
        }
    });

    $('#order_block .close').click(function()
    {
        $.fancybox.close();
    });
    $('.close-button').click(function()
    {
        $.fancybox.close();
    });

    $('.icon[data-action]').click(function()
    {
        var action = $(this).data('action');
        var input = $(this).siblings('input');
        var value = Number(input.val());

        if (action == 'plus')
        {
            value = value + 1;
        }
        else if (action == 'minus')
        {
            value = value - 1;
        }

        if (value < 1)
        {
            value = 1;
        }

        input.val(value);
    });

    $('.icon[data-action]').siblings('input').change(function()
    {
        var input = $(this);
        var value = Number(input.val());

        if (value < 1)
        {
            value = 1;
        }

        input.val(value);
    });

    // Отправка заявки
    $('#order_submit').on('click', function(e)
    {
        var button = $(this);
        var form = $('#order_form');
        var auth = $('#config').data('auth');

        var all_offers = $('#order_all_offers');
        var all_individual_offers = $('#order_all_individual_offers');

        var all_offers_flag = all_offers.attr('checked');
        var all_individual_offers_flag = all_individual_offers.attr('checked');
        var is_individual_buyer = $('#individual_buyer_yes').attr('checked');

        if (!button.hasClass('disabled'))
        {
            button.addClass('disabled');

            form.find('.required input').each(function()
            {
                if ($.trim($(this).val()) == '')
                {
                    $(this).closest('.control-group').addClass('error');
                }
                else
                {
                    $(this).closest('.control-group').removeClass('error');
                }
            });



            if(form.find('.required.error').length)
            {
                button.removeClass('disabled');
            }
            else
            {
                var comp_id = eval(all_offers.attr('data-id'));
                var basket_item_cnt = $('#basket_item_cnt').val();

                var success_handler = function(response)
                {
                    var basket_form = $('#basket_form');
                    var basket_item = $('#basket_item');

                    basket_form.find('input:not(#basket_item)').remove();

                    // заполняем форму заказа
                    for (var i in response.list)
                    {
                        if (i == 0)
                        {
                            basket_item.attr('name', 'basket_item_cnt[' + String(response.list[i].id + ']'));
                            basket_item.attr('data-id', response.list[i].id);
                            basket_item.val(basket_item_cnt);
                        }
                        else
                        {
                            var new_basket_item = basket_item.clone();
                            new_basket_item.removeAttr('id');
                            new_basket_item.attr('name', 'basket_item_cnt[' + String(response.list[i].id + ']'));
                            new_basket_item.attr('data-id', response.list[i].id);
                            new_basket_item.val(basket_item_cnt);
                            new_basket_item.appendTo(basket_form);
                        }
                    }
                };

                if (comp_id)
                {
                    $.ajax({
                        type: 'POST',
                        url: '/ajaxer.php?x=basket',
                        async: false,
                        dataType: 'json',
                        data: {
                            action: 'add',
                            id: comp_id,
                            oneclick: 1,
                            all_offers: (all_offers_flag || all_individual_offers_flag) ? 1 : 0,
                            is_individual_buyer: is_individual_buyer ? 1 : 0
                        },
                        success: success_handler
                    });
                }
                else
                {
                    $.ajax({
                        type: 'POST',
                        url: '/ajaxer.php?x=basket',
                        async: false,
                        dataType: 'json',
                        data: {
                            action: 'get_list'
                        },
                        success: success_handler
                    });
                }

                $('#basket_item').val($('#basket_item_cnt').val());

                $.ajax({
                    url: '/ajaxer.php?x=basket',
                    dataType: 'json',
                    data: $('#basket_form').serialize() + '&action=order&' + form.serialize(),
                    type: 'POST',
                    success: function(data)
                    {
                        if (data)
                        {
                            if (data.send)
                            {
                                var ids = [];
                                $('.basket_item').each(function()
                                {
                                    ids.push(Number($(this).attr('data-id')));
                                });

                                for (var i in ids)
                                {
                                    var id = ids[i];
                                    var title = $('table.search tbody tr[data-id=' + String(id) + '] td.product p.comp_title');
                                    var label = title.find('.label');
                                    var label_html = $('#label_order_send').html();

                                    label.remove();
                                    title.append(' ' + label_html);
                                }

                                button.removeClass('disabled');
                                if (auth)
                                {
                                    $.fancybox.close();
                                }
                                else
                                {
                                    $('#order_body').hide();
                                    $('#order_msg').show();
                                }

                                // Сохраняем информацию о заказанных товарах в куках
                                var session_orders = [];
                                try
                                {
                                    if ($.cookie('session_orders'))
                                    {
                                        session_orders = JSON.parse($.cookie('session_orders'));
                                    }
                                }
                                finally
                                {
                                    if (!session_orders)
                                    {
                                        session_orders = [];
                                    }
                                }

                                $('table.search.basic-interface tbody tr.checked').each(function()
                                {
                                    session_orders.push($(this).data('title'));
                                    $(this).addClass('muted');
                                });

                                $.cookie('session_orders', JSON.stringify(session_orders), { 'path': '/' });

                                $('#basket_form.basic-interface').empty();
                                $('table.search.basic-interface tbody tr').removeClass('checked');
                                $('.show-order').attr('disabled', 'disabled').addClass('disabled');
                            }
                            else
                            {
                                var error;
                                if (data.error)
                                {
                                    error = data.error;
                                }
                                else
                                {
                                    error = 'Произошла ошибка, попробуйте еще раз';
                                }
                                alert(error);
                                button.removeClass('disabled');
                            }

                        }
                        else
                        {
                            alert('Произошла ошибка, попробуйте еще раз');
                            button.removeClass('disabled');
                        }
                    },
                    error: function()
                    {
                        alert('Произошла ошибка, попробуйте еще раз');
                        button.removeClass('disabled');
                    }
                });
            }
        }

        e.preventDefault();
    });

    $(this).trigger('render-finish');

    // title="Прайс обновлен {if $item.price_status == 1}менее недели назад{elseif $item.price_status == 2}более недели назад{elseif $item.price_status == 3}более двух недель назад{/if}"
    $('.price_status').on('hover', function() {
        var elem = $(this);
        var price_status = elem.closest('tr').attr('data-price-status');

        if (!elem.attr('title'))
        {
            var title = 'Прайс обновлен ';

            if (price_status == 1)
            {
                title += 'менее недели назад';
            }
            else if (price_status == 2)
            {
                title += 'более недели назад';
            }
            else if (price_status == 3)
            {
                title += 'более двух недель назад';
            }

            elem.attr('title', title);
        }
    });

    // title="Поставщик работает {if $item.individual_buyers}с юридическими и физическими{else}только с юридическими{/if} лицами"
    $('.user_type').on('hover', function() {
        var elem = $(this);
        var individual_buyers = Number(elem.closest('tr').attr('data-individual-buyers'));

        if (!elem.attr('title'))
        {
            var title = 'Поставщик работает ';

            if (individual_buyers)
            {
                title += 'с юридическими и физическими лицами';
            }
            else
            {
                title += 'только с юридическими лицами';
            }

            elem.attr('title', title);
        }
    });

    // title="Все предложения по {$item.title|escape}"
    $('#filter_block .line a').on('hover', function() {
        var elem = $(this);

        if (!elem.attr('title'))
        {
            elem.attr('title', 'Все предложения по ' + elem.text());
        }
    });

    // Регулирование "онлайн" статуса
    if ($('#search_content .result_column').length == 1)
    {
        var users_ids = $('#search_result').data('users-ids');

        setInterval(function()
        {
            $.ajax({
                url: '/ajaxer.php?x=users_last_visit',
                data: { users_ids: users_ids },
                dataType: 'json',
                type: 'POST',
                success: function(data)
                {
                    if (data.users_info.length == 0 || data.users_info_diff == false)
                    {
                        return false;
                    }

                    for (var i = 0; i < data.users_info.length; i++)
                    {
                        if ($('table.search tr[data-user_id="'+data.users_info[i].user_id+'"]').length > 0)
                        {
                            $('table.search tr[data-user_id="'+data.users_info[i].user_id+'"]').each(function()
                            {
                                if (data.users_info[i].user_online == 1 && data.users_info[i].user_allow_chat == 1)
                                {
                                    $(this).find('td:nth-child(4)').addClass('chat_online');
                                }
                                else
                                {
                                    $(this).find('td:nth-child(4)').removeClass('chat_online');
                                }
                            });
                        }
                    }
                }
            });
        }, 10000);
    }
});

// Склонение для чисел. Пример: plural(13, ['год', 'года', 'лет']);
function plural(n,f){n%=100;if(n>10&&n<20)return f[2];n%=10;return f[n>1&&n<5?1:n==1?0:2]}
