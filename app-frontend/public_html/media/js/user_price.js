$(document).ready(function()
{
    // Фиксированная панель и плавающий блок
    var custom_scroll = new function()
    {
        var _this = this,
            block_offset = 331,
            h_container,
            h_filter,
            h_table,
            h_window,
            h_footer,
            h_main;

        // Функция получения высот блоков
        _this.get_height = function()
        {
            h_container = $('#container').height();
            h_filter = $('#company_info').height();
            h_table = $('#price_list').height();
            h_window = $(window).height();
            h_footer = $('#footer').height() + 50;
            h_main = $('#main').height();
        };

        // Функция фиксирования панели фильтров и плавующего фильтра
        _this.scrolling = function(current_scroll)
        {
            if (current_scroll >= 268)
            {
                if (h_table > h_filter )
                {
                    if (h_window > h_filter + 66 + h_footer)
                    {
                        $('#company_info').css('position', 'fixed').css('top', '66px');
                    }
                    else
                    {
                        if (current_scroll + h_window  >= h_main - h_footer) {
                            $('#company_info').css('position', 'absolute').css('bottom', '50px');
                        }
                        else if (current_scroll + h_window >= h_filter + block_offset + 52)
                        {
                            $('#company_info').css('position', 'fixed').css('bottom', '30px');
                        }
                        else
                        {
                            $('#company_info').css('position', 'relative').css('top', 'auto').css('bottom', 'auto');
                        }
                    }
                }

                $('#search_options').addClass('fixed');
                var width = $('#search_options_wrap').width() + 50;
                var margin = -width / 2;
                _this.block_pos(width, margin);
            }
            else {
                $('#company_info').css('position', 'relative').css('top', 'auto').css('bottom', 'auto');


                $('#search_options').removeClass('fixed');
                _this.block_pos('100%', 0);
            }
        };

        // Функция позиционирования панели фильтров
        _this.block_pos = function(width, margin)
        {
            $('#search_options').width(width).css('margin-left', margin);
        };

        // Инициализация
        $(window).load(function()
        {
            _this.get_height();

            $(window).scroll(function()
            {
                _this.scrolling($(window).scrollTop());
            });
        });

        // Пересчет параметров при ресайзе
        $(window).resize(function() {
            _this.get_height();
            _this.scrolling($(window).scrollTop());
        });
    };


});