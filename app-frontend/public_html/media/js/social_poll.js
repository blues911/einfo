$(document).ready(function()
{
    var simple_close = false;

    $('#show_social_poll').fancybox({
        padding: 10,
        wrapCSS: 'rounded',
        closeBtn: false,
        topRatio: 0,
        beforeShow: function() {
            simple_close = false;
        },
        afterClose: function() {
            if (!simple_close) close_modal();
        }
    });

    $(document).mouseleave(function()
    {
        if ($('body').hasClass('fancybox-lock'))
        {
            return false;
        }

        $.ajax({
            url: '/ajaxer.php?x=social_poll',
            dataType: 'json',
            data: {
                type: 'check_limit'
            },
            type: 'POST',
            success: function(response)
            {
                if (!response.success)
                {
                    return false;
                }

                if (response.form !== '')
                {
                    $('#show_social_poll').click();

                    $('#social_poll_block').show();
                    $('#social_poll_content_3').html(response.form);

                    var textarea = $(document).find('#social_poll_form textarea');

                    if (textarea.length == 0)
                    {
                        $(document).find('#social_poll_form a.button').removeClass('disabled');
                    }
                }
            },
            error: function()
            {
                alert('Ошибка сервера.');
            }
        });
    });

    // Закрытие окна
    $('#social_poll_block .close').click(function(e)
    {
        e.preventDefault();
        close_modal();
    });

    // Обработка окна 1
    $('#social_poll_block').on('click', '#social_poll_content_1 a.yes', function(e)
    {
        e.preventDefault();
        $('#social_poll_content_1').hide();
        $('#social_poll_content_2').show();

        $.cookie('social_poll_like', 'yes', { expires: 7, path: '/' });
    });

    $('#social_poll_block').on('click', '#social_poll_content_1 a.no', function(e)
    {
        e.preventDefault();
        $('#social_poll_content_1').hide();
        $('#social_poll_content_2').show();

        $.cookie('social_poll_like', 'no', { expires: 7, path: '/' });
    });

    // Обработка окна 2
    $('#social_poll_block').on('click', '#social_poll_content_2 a.yes', function(e)
    {
        e.preventDefault();
        $('#social_poll_content_2').hide();
        $('#social_poll_content_3').show();
    });

    $('#social_poll_block').on('click', '#social_poll_content_2 a.no', function(e)
    {
        e.preventDefault();
        close_modal();
    });

    // Обработка окна 3
    $(document).on('click', '#social_poll_form a.button', function(e)
    {
        e.preventDefault();

        var from_data = $('#social_poll_form').serialize() + '&type=send_form&page=' + location.pathname;

        $.ajax({
            url: '/ajaxer.php?x=social_poll',
            dataType: 'json',
            data: from_data,
            type: 'POST',
            success: function(response)
            {
                close_fancybox();
            },
            error: function()
            {
                alert('Ошибка сервера.');
            }
        });
    });

    function close_modal()
    {
        $.ajax({
            url: '/ajaxer.php?x=social_poll',
            dataType: 'json',
            data: {
                type: 'close_form',
                page: location.pathname
            },
            type: 'POST',
            success: function(response)
            {
                close_fancybox();
            },
            error: function()
            {
                alert('Ошибка сервера.');
            }
        });
    }

    function close_fancybox()
    {
        simple_close = true;

        $.fancybox.close();

        setTimeout(function() {
            $('#social_poll_content_1').show();
            $('#social_poll_content_2').hide();
            $('#social_poll_content_3').hide();
            $('#social_poll_content_3').html('');
        }, 2000);
    }
});