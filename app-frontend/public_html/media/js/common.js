// Проверка формы на заполненность обязательных полей
function CheckFieldsForm(fields)
{
    var result = true;
    var check_fields = new Array();
	
	$.each(fields, function(i, selector)
	{
        var length = $(selector).length;
        
        if (length == 1)
        {
            check_fields.push($(selector));
        }
        else if (length > 1)
        {
            $(selector).each(function(){ check_fields.push($(this)); });
        }
    });
    
    $.each(check_fields, function(i, item)
    {
        item.removeClass('f-incorrect');
        
        if ($.trim(item.val()) == '' || item.val() == -1)
        {
            item.addClass('f-incorrect');
            result = false;
        }
    });
	
	return result;
}

// Правильные падежи
function Plural(num, words)
{
    var wforms = words.split('|');
    
    if (wforms.length == 3)
    {
        var plural = ((num % 10 == 1 && num % 100 != 11) ? 0 : ((num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)) ? 1 : 2));
        return wforms[plural];
    }
    else return '';
}

// Форматированная сумма
function FormatPrice(num)
{
    var result = '';

    if (matches = /^(-?)(\d+)(\.(\d+))?$/.exec(num))
    {
        sign = matches[1] ? matches[1] : '';
        whole = matches[2];
        psub = matches[4] ? matches[4] : '';

        while (/\d+\d{3}/.test(whole))
        {
            whole = whole.replace(/(\d+)(\d{3})/, '$1 $2');
        }

        psub = psub.replace(/^(0+)$/, '');
        psub = (psub.length == 1) ? psub + '0' : psub;

        result = sign + whole + ((psub != '') ? '.' + psub : '');
    }

    return result;
}