/*
 * Получение расширенной статистики о посетителях сайта
 */
function einfo_stat(event_type, event_label)
{
    if (!event_type) return;

    var arr_stat_data = {};

    arr_stat_data.screen = screen.width + 'x' + screen.height;
    arr_stat_data.event_type = event_type;
    arr_stat_data.event_label = event_label ? event_label : '';
    arr_stat_data.hardware = ($('#main_wrap').length) ? 'desktop' : 'mobile';

    arr_stat_data.stat_id = $.cookie('stat_id') ? $.cookie('stat_id') : '';


    $.ajax({
        url: "/ajaxer.php?x=event_stat",
        type: "POST",
        data: arr_stat_data,
        dataType: "json",
        success: function(response)
        {
            if (response && response.stat_id)
            {
                $.cookie('stat_id', response.stat_id, { expires: 365, path: '/', domain: '.einfo.ru' });
            }
        }
    });
}

// сбор расширенной статистики по посетителям: логирование посещенных страниц
einfo_stat('visit_page', decodeURIComponent(window.location.toString().replace(/\+/g,  ' ')));
