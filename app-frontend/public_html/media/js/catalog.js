$(document).ready(function()
{
    // Фиксированная панель
    var custom_scroll = new function()
    {
        var _this = this;

        // Функция фиксирования панели фильтров
        _this.scrolling = function(current_scroll)
        {
            if (current_scroll >= 268)
            {
                $('#search_options').addClass('fixed');
                var width = $('#search_options_wrap').width() + 50;
                var margin = -width / 2;
                _this.block_pos(width, margin);
            }
            else {
                $('#search_options').removeClass('fixed');
                _this.block_pos('100%', 0);
            }
        };

        // Функция позиционирования панели фильтров
        _this.block_pos = function(width, margin)
        {
            $('#search_options').width(width).css('margin-left', margin);
        };

        // Инициализация
        $(window).load(function()
        {
            $(window).scroll(function()
            {
                _this.scrolling($(window).scrollTop());
            });
        });

        // Пересчет параметров при ресайзе
        $(window).resize(function() {
            _this.scrolling($(window).scrollTop());
        });
    };

    $('#comp_list tbody tr').on('click', function(e)
    {
        var row = $(this);
        var id = $(this).data('id');

        if (row.hasClass('checked'))
        {
            $.ajax({
                url: '/ajaxer.php?x=basket',
                dataType: 'json',
                data: { 'action': 'remove', id: id },
                type: 'POST',
                success: function(data)
                {
                    if (data)
                    {
                        row.removeClass('checked');
                        print_basket_count(data.count);
                    }
                    else
                    {
                        alert('Произошла ошибка, попробуйте еще раз');
                    }
                }
            });

        }
        else
        {
            $.ajax({
                url: '/ajaxer.php?x=basket',
                dataType: 'json',
                data: { 'action': 'add', id: id },
                type: 'POST',
                success: function(data)
                {
                    if (data)
                    {
                        row.addClass('checked');
                        print_basket_count(data.count);
                    }
                    else
                    {
                        alert('Произошла ошибка, попробуйте еще раз');
                    }
                }
            });
        }
    });

    // Отключение выделения при переходе по ссылке
    $('#comp_list tbody .company a').on('click', function(e)
    {
        e.stopPropagation();
    });
});