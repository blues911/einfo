function watch_file_analysis_status(file_id, percent, prev_current_row_val)
{
    var progress_bar = $('.components_analysis .progress_bar');
    var progress_bar_width = progress_bar.width();
    var progress_inner = progress_bar.find('.inner');

    $.ajax({
        url: '/ajaxer.php?x=components_analysis',
        data: {
            action: 'watch_analysis',
            file_id: file_id
        },
        dataType: 'json',
        type: 'POST',
        success: function(result)
        {
            // Обработка файла еще не закончилась
            if (result.count_profile_click === null && result.count_profile_view === null)
            {
                var current_row = parseInt(result.current_row);
                var rows_count = parseInt(result.rows_count);

                // Игнорируем случай, когда номер строки анализа = 0, это и есть статус загрузки файла
                if (current_row > 0 && (prev_current_row_val && prev_current_row_val != current_row))
                {
                    // Загрузке всего файла отведено 30%, остальные 70% рассчитываются здесь в зависимости от того
                    // сколько строк из общего кол-ва обработано, т.е считаем процент обработанных строк, переносим его на ширину
                    var new_percent = percent + parseInt((current_row * 70 / rows_count));

                    if (percent < 100)
                        progress_inner.animate({width: (progress_bar_width * new_percent / 100) + 'px'}, 500);
                    else
                        progress_inner.animate({width: '100%'}, 500);
                }

                setTimeout(function () {
                    watch_file_analysis_status(file_id, percent, current_row);
                }, 700);
            }
            else
            {
                progress_inner.animate({width: '100%'}, 1000, function(){
                    var form = $('.components_analysis');

                    var profile_view = form.find('.profile_view');
                    var profile_view_digit = profile_view.find('.digit');
                    var profile_view_text = profile_view.find('.text');

                    var profile_click = form.find('.profile_click');
                    var profile_click_digit = profile_click.find('.digit');
                    var profile_click_text = profile_click.find('.text');

                    form.find('.analysis_button').removeClass('disabled').hide();
                    form.find('.result').show();

                    if (result.count_profile_click > 0 || result.count_profile_view > 0)
                    {
                        form.find('.ready_title').show();

                        if (result.count_profile_view > 0)
                        {
                            profile_view.show();
                            profile_view_digit.text(result.count_profile_view);
                            profile_view_text.html('Прогнозируемое количество просмотров профиля компании в результатах поиска в месяц');
                        }

                        if (result.count_profile_click > 0)
                        {
                            profile_click.show();
                            profile_click_digit.text(result.count_profile_click);
                            profile_click_text.html('Прогнозируемое количество переходов<br/> на ваш сайт в месяц');
                        }
                    }
                    else
                    {
                        form.find('.process_status .message').text('Данных из файла недостаточно для анализа');
                    }

                    form.find('.progress_bar').hide();
                    form.find('.cancel_file').removeClass('disabled');
                });
            }
        }
    });
}

$(document).ready(function()
{
    /* ------------------------------------------------- */

    var components_analysis = $('.components_analysis');

    // Выбор файла
    $('.components_analysis input[type="file"]').change(function(event) {
        var self = $(this);
        var file_path = self.val();
        var file_name;
        var file_format_error = components_analysis.find('.file_format_error');

        $('#index_components_analysis .arrow').hide();

        if (file_path != '')
        {
            if (/\S+.xls(x)?$/.exec(file_path))
            {
                file_format_error.hide();

                self.parent('.file_input_wrap').hide();

                file_name = file_path.replace(/(.*(\/|\\))(\S+.xls(x)?)$/, '$3');

                $('.components_analysis .file_info')
                    .find('.title').text(file_name)
                    .end()
                    .show();

                // Начало анализа
                var progress_bar = $('.components_analysis .progress_bar');
                var progress_bar_width = progress_bar.width();
                var progress_inner = progress_bar.find('.inner');
                var percent = 30;

                var formData = new FormData();

                formData.append('action', 'analysis');
                formData.append('file', event.target.files[0]);

                $.ajax({
                    url: '/ajaxer.php?x=components_analysis',
                    processData: false,
                    contentType: false,
                    data: formData,
                    dataType: 'json',
                    type: 'POST',
                    beforeSend: function()
                    {
                        $('.components_analysis .process_status')
                            .find('.message').html('')
                            .end()
                            .show();

                        $('.components_analysis').find('.cancel_file, .analysis_button').addClass('disabled');

                        progress_inner.width(0);
                        progress_bar.show();

                        progress_inner.animate({width: Math.round(progress_bar_width * percent / 100) + 'px'}, 10000);
                    },
                    success: function(result)
                    {
                        // Остановка анимации
                        progress_inner.stop(false, true);

                        // Процесс анализа файла закончился одной из ошибок
                        if (result.error)
                        {
                            progress_inner.animate({width: '100%'}, 500, function(){
                                $('.components_analysis .process_status')
                                    .find('.message').html(result.error)
                                    .end()
                                    .find('.ready_title').hide();

                                $('.components_analysis').find('.cancel_file, .analysis_button').removeClass('disabled');

                                progress_bar.hide();
                            });
                        }

                        // Анализ файла идет
                        else if (result.file_id)
                        {
                            // Теперь параллельно с работой анализа следим за ее статусом
                            watch_file_analysis_status(result.file_id, percent);
                        }
                    }
                });
            }
            else
            {
                file_format_error.show();
            }
        }
    });

    // Кнопка отмены, возврат к начальному состоянию
    components_analysis.find('.cancel_file, .more_analysis').click(function(){
        if (!$(this).hasClass('disabled')) {
            components_analysis
                .find('.file_info, .process_status, .result').hide()
                .end()
                .find('input[type="file"]').val('')
                .end()
                .find('.file_input_wrap').show();

            components_analysis.find('.result .digit, .result .text').text('');
        }

        return false;
    });

    /* ------------------------------------------------- */

    // Перетаскивание всплывающего окна
    $('#basket_cnt_upd').draggable({ handle: '.drag_area' });

    // Autocomplete поисковая фраза
    $('#search_input').autocomplete({
        source: function( request, response ) {
            var parent = $(this.element).parent();

            $.ajax({
                url: '/ajaxer.php?x=search',
                dataType: 'json',
                data: { action: 'autocomplete', term: request.term },
                type: 'POST',
                beforeSend: function()
                {
                    parent.find('.clear_search').addClass('_is_loading');
                },
                success: function( data )
                {
                    parent.find('._is_loading').removeClass('_is_loading');

                    response( $.map( data.rows, function(item) {
                        return {
                            label: item.comp_title,
                            value: item.comp_title
                        }
                    }));
                }
            });
        },
        minLength: 4,
        select: function( event, ui )
        {
            $('#search_input').val(ui.item.value);
            $('#search_form').submit();
        }
    });


    // Добавление класса
    $(window).resize(function() {
        var width = $(this).width();

        if (width < 1130) {
            $('body').removeClass().addClass('small');
        }
        else {
            $('body').removeClass();
        }

    });

    $(window).resize();


    // Placeholder
    $('.custom_placeholder').each(function()
    {
        var input = $(this);

        if (input.val() == '')
        {
            $(this).val($(this).attr('data-placeholder')).addClass('empty');
        }
    });

    $(document).on('focus', '.custom_placeholder', function()
    {
        var input = $(this);

        if (input.hasClass('empty'))
        {
            input.val('').removeClass('empty');
        }
    });

    $(document).on('blur', '.custom_placeholder', function()
    {
        var input = $(this);

        if (input.val() == '')
        {
            input.val(input.attr('data-placeholder')).addClass('empty');
        }
    });
    // ---------

    // Добавить/удалить предложение из корзины
    $('table.offer tbody tr').on('click', '.workspace', function(e)
    {
        var oneclick = $('#config').data('oneclick');
        var row = $(this).closest('tr');
        var id = row.data('id');
        var users_cnt = 0;
        var individual_users_cnt = 0;
        var individual_buyers = Number(row.attr('data-individual-buyers'));

        // для однокликового интерфейса
        if (oneclick)
        {
            $('table.offer tbody tr').removeClass('checked');

            var loading = row.find('.workspace .order_loading');
            var icon = row.find('.workspace .order_icon');
            var order_block = $('#order_block');
            var basket_form = $('#basket_form');
            var basket_item = $('#basket_item');

            basket_form.find('input:not(#basket_item)').remove();

            var add_comp_error = false;

            loading.show();
            icon.hide();

            // добавляем элемент в корзину
            $.ajax({
                url: '/ajaxer.php?x=basket',
                dataType: 'json',
                data: {
                    'action': 'add',
                    id: id,
                    oneclick: 1
                },
                type: 'POST',
                success: function(response)
                {
                    if (response)
                    {
                        // сбор расширенной статистики по посетителям: открытие поисковой формы
                        einfo_stat('open_request_form', response.list[0].comp_title + ' (' + response.list[0].title + ')');

                        users_cnt = Number(response['similar']['all']['user_cnt']);
                        individual_users_cnt = Number(response['similar']['individual']['user_cnt']);

                        loading.hide();
                        icon.show();

                        // заполняем форму заказа
                        basket_item.attr('name', 'basket_item_cnt[' + String(response.list[0].id + ']'));
                        basket_item.attr('data-id', response.list[0].id);
                        basket_item.val(1);

                        order_block.find('#part_user').html(response.list[0].title);
                        order_block.find('#part_number').html(response.list[0].comp_title);
                        order_block.find('#order_all_offers').attr('data-id', id);
                        order_block.find('.cnt_line').children().attr('data-id', id);
                        order_block.find('[id^="basket_item_cnt"]').attr('id', 'basket_item_cnt_' + id).val(1);
                        order_block.find('#order_body')
                            .attr('data-individual-buyers', individual_buyers)
                            .attr('data-individual-users-cnt', individual_users_cnt);

                        order_block.find('.required').removeClass('error');

                        var checkbox = order_block.find('#order_all_offers').closest('label');
                        var checkbox_caption = checkbox.find('.caption:first');

                        var individual_checkbox = order_block.find('#order_all_individual_offers').closest('label');
                        var individual_checkbox_caption = individual_checkbox.find('.caption:first');

                        var individual_buyer_yes = order_block.find('#individual_buyer_yes');
                        var individual_buyer_no = order_block.find('#individual_buyer_no');

                        checkbox.find('input').prop('checked', false);
                        checkbox.find('input').triggerHandler('change');
                        individual_checkbox.find('input').prop('checked', false);
                        individual_checkbox.find('input').triggerHandler('change');

                        individual_buyer_yes.prop('checked', false);
                        individual_buyer_no.prop('checked', false);

                        if (users_cnt)
                        {
                            var text = 'Отправить заявку ' +
                                plural(users_cnt, ['остальному', 'остальным', 'остальным']) +
                                ' ' + users_cnt + ' ' +
                                plural(users_cnt, ['поставщику', 'поставщикам', 'поставщикам']);
                            checkbox_caption.text(text);
                            checkbox.show();

                            if (individual_users_cnt)
                            {
                                var text = 'Отправить заявку ' +
                                    plural(individual_users_cnt, ['остальному', 'остальным', 'остальным']) +
                                    ' ' + individual_users_cnt + ' ' +
                                    plural(individual_users_cnt, ['поставщику', 'поставщикам', 'поставщикам']);

                                individual_checkbox_caption.text(text);
                                individual_checkbox.show();
                            }
                            else
                            {
                                individual_checkbox.hide();
                            }
                        }
                        else
                        {
                            checkbox.hide();
                            individual_checkbox.hide();
                        }

                        // показываем форму
                        if (Number(order_block.attr('data-individual-buyer')))
                        {
                            individual_buyer_yes.prop('checked', true);
                            change_individual_buyer(1);
                            $('#order_submit').removeClass('disabled');
                        }
                        else
                        {
                            change_individual_buyer(0);
                            $('#order_submit').addClass('disabled');
                        }

                        $('#show_order').click();
                    }
                    else
                    {
                        add_comp_error = true;
                    }
                },
                error: function()
                {
                    add_comp_error = true;
                }
            });

            // обработка ошибки
            if (add_comp_error)
            {
                loading.hide();
                icon.show();
            }

        }
        // для обычного интерфейса
        else
        {

            if (row.hasClass('checked'))
            {
                row.removeClass('checked');

                $.ajax({
                    url: '/ajaxer.php?x=basket',
                    dataType: 'json',
                    data: { 'action': 'remove', id: id },
                    type: 'POST',
                    success: function(data)
                    {
                        if (data)
                        {
                            print_basket_count(data.count);
                            show_smile('sad');
                        }
                        else
                        {
                            row.addClass('checked');
                            alert('Произошла ошибка, попробуйте еще раз');
                        }
                    },
                    error: function()
                    {
                        row.addClass('checked');
                        alert('Произошла ошибка, попробуйте еще раз');
                    }
                });
            }
            else
            {
                row.addClass('checked');
                $.ajax({
                    url: '/ajaxer.php?x=basket',
                    dataType: 'json',
                    data: { 'action': 'add', id: id },
                    type: 'POST',
                    success: function(data)
                    {
                        if (data)
                        {
                            print_basket_count(data.count);
                            show_smile('smile');
                        }
                        else
                        {
                            row.removeClass('checked');
                            alert('Произошла ошибка, попробуйте еще раз');
                        }
                    },
                    error: function()
                    {
                        row.removeClass('checked');
                        alert('Произошла ошибка, попробуйте еще раз');
                    }
                });
            }

        }

    });

    // Отключение выделения при переходе по ссылке
    $('table.offer tbody .company a').on('click', function(e)
    {
        e.stopPropagation();
    });

    // Уточнение поиска по названию компонента.
    $('table.search tbody .comp_search').on('click', function(e)
    {
        encodeURIComponent()
        e.stopPropagation();
    });


    $('#basket_cnt_upd').on('click', function(e) { e.stopPropagation(); });

    // Добавить в корзину все компоненты
    $('.select_all_components').on('click', function(e)
    {
        var checkbox = $('.select_all_components');
        var list = [];
        var rows = $('table.offer').find('tbody tr');
        var page =  checkbox.data('page');

        var action = '';

        // Выбор действия в зависимости от значения чекбокса
        if (checkbox.hasClass('checked'))
        {
            checkbox.removeClass('checked');
            action = 'remove_all';
        }
        else
        {
            checkbox.addClass('checked');
            action = 'add_all';
        }

        // Выделение/снятие выделения со строчек
        var i = 0;
        while(i < rows.length)
        {
            if(!$(rows[i]).hasClass('hide'))
            {
                list.push($(rows[i]).data('id'));

                $(rows[i]).removeClass('mark');

                if (action == 'add_all')
                {
                    if (!$(rows[i]).hasClass('checked'))
                    {
                        $(rows[i]).addClass('mark');
                    }
                    else
                    {
                        $(rows[i]).addClass('checked');
                    }
                }
                else if (action == 'remove_all')
                {
                    $(rows[i]).removeClass('checked');
                }
            }
            i++;
        }

        // Отправка временных данных
        $.ajax({
            url: '/ajaxer.php?x=basket',
            dataType: 'json',
            data: { 'action': action, 'ids': list },
            type: 'POST',
            success: function(data)
            {
                if (data)
                {
                    // Выделение всех предложений
                    if (action == 'add_all')
                    {
                        var i = 0;
                        while(i < rows.length)
                        {
                            if(!$(rows[i]).hasClass('hide'))
                            {
                                $(rows[i]).addClass('checked');
                            }

                            i++;
                        }

                        // Всплывающее окно
                        $('#basket_cnt_upd').css({top: e.pageY, left: e.pageX + 20}).html(data.in_basket).show();

                        update_scroll();

                        show_smile('smile');

                        // Кнопка закрыть
                        $('#basket_cnt_upd').on('click', 'a.close', function(e)
                        {
                            e.stopPropagation();
                            e.preventDefault();

                            // Отменяем маркировку
                            var i = 0;
                            while(i < rows.length)
                            {
                                if($(rows[i]).hasClass('mark'))
                                {
                                    $(rows[i]).removeClass('checked');
                                }

                                i++;
                            }

                            checkbox.removeClass('checked');

                            $('#basket_cnt_upd').html('').hide();
                            $('#main').off('click.basket_cnt_upd');
                            $('#basket_cnt_upd').off('a.close click.upd_count change.upd_count keyup.group_upd change.group_upd');

                        });

                        // Скрытие всплывающего окна по клику за его пределами
                        $('#main').on('click.basket_cnt_upd', function(e)
                        {
                            // Отменяем маркировку
                            var i = 0;
                            while(i < rows.length)
                            {
                                if($(rows[i]).hasClass('mark'))
                                {
                                    $(rows[i]).removeClass('checked');
                                }

                                i++;
                            }

                            checkbox.removeClass('checked');

                            $('#basket_cnt_upd').html('').hide();
                            $('#main').off('click.basket_cnt_upd');
                            $('#basket_cnt_upd').off('a.close click.upd_count change.upd_count keyup.group_upd change.group_upd');
                        });

                        // +1/-1 компонент
                        $('#basket_cnt_upd').on('click.upd_count', '.icon.plus, .icon.minus', function(e)
                        {
                            e.preventDefault();

                            var id = $(this).data('id');
                            var action = $(this).data('action');
                            var current_cnt = parseInt($('#basket_cnt_upd_' + id).val());

                            if (isNaN(current_cnt) || current_cnt <= 0)
                            {
                                current_cnt = 1;
                            }

                            if (action == 'plus')
                            {
                                current_cnt++;
                            }
                            else if (action == 'minus')
                            {
                                if (current_cnt - 1 >= 1)
                                {
                                    current_cnt--;
                                }
                            }

                            $('#basket_cnt_upd_' + id).val(current_cnt);

                        });

                        // Изменение количества компонентов
                        $('#basket_cnt_upd').on('change.upd_count', '.upd_comp_val', function()
                        {
                            e.preventDefault();
                            //e.stopPropagation();

                            var input = $(this);
                            var id = input.data('id');

                            var cnt = parseInt($(this).val());
                            if (isNaN(cnt) || cnt <= 0)
                            {
                                cnt = 1;
                            }

                            input.val(cnt);
                        });

                        // Групповое изменение компонентов
                        $('#basket_cnt_upd').on('keyup.group_upd change.group_upd','.all_comp_val', function()
                        {
                            var cur = $(this).val();

                            if (cur.toString() !== $(this).data('cur').toString() && cur != '')
                            {
                                cur = parseInt($(this).val());

                                if (isNaN(cur) || cur <= 0)
                                {
                                    cur = 1;
                                }

                                $(this).val(cur).data('cur', cur);
                                $('#basket_cnt_upd').find('.upd_comp_val').val(cur);
                            }
                        });


                        // Клик на сохранение результата
                        $('#basket_cnt_upd').on('submit', '#basket_cnt_upd_form', function(e)
                        {
                            e.preventDefault();
                            $(this).find('#upd_submit').click();
                        });

                        // Сохранение группового результата
                        $('#basket_cnt_upd').on('click.upd_count', '#upd_submit', function(e)
                        {
                            var button = $(this);
                            if (!button.hasClass('disabled'))
                            {
                                button.addClass('disabled');

                                $('#basket_cnt_upd').find('.upd_comp_val').change();

                                // Подтверждение временных данных
                                $.ajax({
                                    url: '/ajaxer.php?x=basket',
                                    dataType: 'json',
                                    data: $('#basket_cnt_upd_form').serialize(),
                                    type: 'POST',
                                    success: function(data)
                                    {
                                        if (data)
                                        {
                                            print_basket_count(data.count);

                                            $('#basket_cnt_upd').fadeOut('fast', function(){$(this).html('');});
                                            $('#main').off('click.basket_cnt_upd');
                                            $('#basket_cnt_upd').off('a.close click.upd_count change.upd_count keyup.group_upd change.group_upd');
                                        }
                                        else
                                        {
                                            button.removeClass('disabled');
                                            alert('Произошла ошибка, попробуйте еще раз');
                                        }
                                    },
                                    error: function()
                                    {
                                        button.removeClass('disabled');
                                        alert('Произошла ошибка, попробуйте еще раз');
                                    }
                                });
                            }
                            e.preventDefault();
                        });

                    }

                    // Снятие выделения всех предложений
                    else if (action == 'remove_all')
                    {
                        var i = 0;
                        while(i < rows.length)
                        {
                            if(!$(rows[i]).hasClass('hide'))
                            {
                                $(rows[i]).removeClass('checked');
                            }

                            i++;
                        }

                        show_smile('sad');
                    }
                    else
                    {
                        // Непредвиденная ошибка
                        alert('Произошла ошибка, попробуйте еще раз');

                        var i = 0;
                        while(i < rows.length)
                        {
                            if(!$(rows[i]).hasClass('hide'))
                            {
                                if (action == 'add_all')
                                {
                                    $(rows[i]).removeClass('checked');
                                }
                                else if (action == 'remove_all')
                                {
                                    $(rows[i]).addClass('checked');
                                }
                            }

                            i++;
                        }

                        if (checkbox.hasClass('checked'))
                        {
                            checkbox.removeClass('checked');
                        }
                        else
                        {
                            checkbox.addClass('checked');
                        }
                    }


                    print_basket_count(data.count);
                }
                else
                {
                    alert('Произошла ошибка, попробуйте еще раз');
                }

            },
            error: function()
            {
                // Непредвиденная ошибка
                alert('Произошла ошибка, попробуйте еще раз');

                var i = 0;
                while(i < rows.length)
                {
                    if(!$(rows[i]).hasClass('hide'))
                    {
                        if (action == 'add_all')
                        {
                            $(rows[i]).removeClass('checked');
                        }
                        else if (action == 'remove_all')
                        {
                            $(rows[i]).addClass('checked');
                        }
                    }

                    i++;
                }

                if (checkbox.hasClass('checked'))
                {
                    checkbox.removeClass('checked');
                }
                else
                {
                    checkbox.addClass('checked');
                }
            }

        });

        e.preventDefault();
    });



    // Форма входа пользователей
    $('#for_seller').on('click', function(e)
    {
        $('#main').off('click.filter');
        $('#main').click();
        $('#login_block').show();
        $('#login').focus();

        $('#login_block').on('click', 'a.close', function(e)
        {
            $(this).off('click');
            $('#login_block').hide();
            $('#main').off('click.login_form');

            e.stopPropagation();
            e.preventDefault();
        });

        $('#main').on('click.login_form', function(e)
        {
            $('#login_block').hide();
            $('#main').off('click.login_form');
            $('#login_block').off('click', 'a.close');
        });

        e.stopPropagation();
        e.preventDefault();
    });

    $('#login_block').on('click', function(e) { e.stopPropagation(); });

    // Вход для поставщиков
    $('#login_submit').on('click', function(e)
    {
        var button = $(this);

        if(!button.hasClass('disabled'))
        {
            button.addClass('disabled');
            $('#login_form').find('.required').each(function()
            {
                if ($(this).val() == '')
                {
                    $(this).closest('.control-group').addClass('error');
                }
                else
                {
                    $(this).closest('.control-group').removeClass('error');
                }
            });

            if (!$('#login_form').find('.control-group.error').length)
            {
                $('#login_form').submit();
            }
            else
            {
                button.removeClass('disabled');
            }
        }

        e.preventDefault();
    });

    $('#login_form').find('input').keypress(function(e)
    {
        if (e.which == 13)
        {
            e.preventDefault();
            $('#login_submit').click();
        }
    });

    // Меню клиента
    $('#client_menu').on('click', function(e)
    {
        $('#main').off('click.filter');
        $('#main').click();
        $('#client_menu_block').show();

        $('#main').on('click', function(e)
        {
            $('#client_menu_block').hide();
        });

        e.stopPropagation();
        e.preventDefault();
    });

    $('.client_exit').on('click', function(e)
    {
        $.removeCookie('client_id', { path: '/' });
        $.removeCookie('auth_hash', { path: '/' });

        window.location = '.';

        e.stopPropagation();
        e.preventDefault();
    });

    // Форма входа для клиентов
    $('#client_login').on('click', function(e)
    {
        $('#main').off('click.filter');
        $('#main').click();
        $('#client_login_block').show();
        $('#client_login_email').focus();

        $('#client_login_block').on('click', 'a.close', function(e)
        {
            $(this).off('click');
            $('#client_login_block').hide();
            $('#main').off('click.client_login_form');

            e.stopPropagation();
            e.preventDefault();
        });

        $('#main').on('click.client_login_form', function(e)
        {
            $('#client_login_block').hide();
            $('#main').off('click.client_login_form');
            $('#client_login_block').off('click', 'a.close');
        });

        e.stopPropagation();
        e.preventDefault();
    });

    $('#client_login_block').on('click', function(e) { e.stopPropagation(); });

    $('#login_by_email').on('click', function()
    {
        var login_by_email = $(this);
        var login_by_pass = $('#login_by_pass');
        var input_pass = $('#client_login_password');
        var input_method = $('#client_login_method');
        var reg_msg = $('#client_login_regmsg');

        reg_msg.hide();
        login_by_email.addClass('active');
        login_by_pass.removeClass('active');

        input_pass.prop('disabled', true);
        input_pass.closest('.control-group').hide();
        input_method.val('email');
    });

    $('#login_by_pass').on('click', function()
    {
        var login_by_email = $('#login_by_email');
        var login_by_pass = $(this);
        var input_pass = $('#client_login_password');
        var input_method = $('#client_login_method');
        var reg_msg = $('#client_login_regmsg');

        reg_msg.hide();
        login_by_email.removeClass('active');
        login_by_pass.addClass('active');

        input_pass.prop('disabled', false);
        input_pass.closest('.control-group').show();
        input_method.val('pass');
    });

    $('#client_login_form').submit(function(e)
    {
        var button = $(this);
        var loading = $('#client_login_block > .block_ajax_loader');
        var msg = $('#client_login_msg');
        var error = $('#client_login_error');
        var reg_msg = $('#client_login_regmsg');

        msg.hide();
        error.hide();
        reg_msg.hide();

        if(!button.hasClass('disabled'))
        {
            button.addClass('disabled');
            $('#client_login_form').find('.required').each(function()
            {
                if ($(this).val() == '' && !$(this).prop('disabled'))
                {
                    $(this).closest('.control-group').addClass('error');
                }
                else
                {
                    $(this).closest('.control-group').removeClass('error');
                }
            });

            if (!$('#client_login_form').find('.control-group.error').length)
            {

                loading.show();

                $.ajax({
                    url: '/ajaxer.php?x=client_login',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        email: $('#client_login_email').val(),
                        password: $('#client_login_password').val(),
                        method: $('#client_login_method').val()
                    },
                    success: function(response)
                    {
                        if (response['method'] == 'email')
                        {
                            if (response.success)
                            {
                                msg.text(response.info).show();
                            }
                            else
                            {
                                if (response['not_registered'])
                                {
                                    reg_msg.show();
                                }
                                else
                                {
                                    error.text(response.error).show();
                                }
                            }
                        }
                        else
                        {
                            if (response.success)
                            {
                                window.location = '.';
                            }
                            else
                            {
                                error.text(response.error).show();
                            }
                        }

                        loading.hide();
                        button.removeClass('disabled');
                    },
                    error: function()
                    {
                        loading.hide();
                        button.removeClass('disabled');
                        error.text('Непредвиденная ошибка. Повторите позже.').show();
                    }
                });
            }
            else
            {
                button.removeClass('disabled');
                error.text('Заполните обязательные поля').show();
            }
        }

        e.preventDefault();
    });

    // Форма напоминания пароля
    $('#client_remember').on('click', function(e)
    {
        $('#main').off('click.filter');
        $('#main').click();
        $('#client_remember_block').show();
        $('#client_remember_email').val($('#client_login_email').val()).focus();

        $('#client_remember_block').on('click', 'a.close', function(e)
        {
            $(this).off('click');
            $('#client_remember_block').hide();
            $('#main').off('click.client_remember_form');

            e.stopPropagation();
            e.preventDefault();
        });

        $('#main').on('click.client_remember_form', function(e)
        {
            $('#client_remember_block').hide();
            $('#main').off('click.client_remember_form');
            $('#client_remember_block').off('click', 'a.close');
        });

        e.stopPropagation();
        e.preventDefault();
    });

    $('#client_remember_block').on('click', function(e) { e.stopPropagation(); });

    $('#client_remember_form').submit(function(e)
    {
        var button = $(this);
        var loading = $('#client_remember_block > .block_ajax_loader');
        var error = $('#client_remember_error');
        var info = $('#client_remember_info');

        error.hide();
        info.hide();

        if(!button.hasClass('disabled'))
        {
            button.addClass('disabled');
            $('#client_remember_form').find('.required').each(function()
            {
                if ($(this).val() == '')
                {
                    $(this).closest('.control-group').addClass('error');
                }
                else
                {
                    $(this).closest('.control-group').removeClass('error');
                }
            });

            if (!$('#client_remember_form').find('.control-group.error').length)
            {
                loading.show;

                $.ajax({
                    url: '/ajaxer.php?x=client_remember',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        email: $('#client_remember_email').val()
                    },
                    success: function(response)
                    {
                        if (response.success)
                        {
                            info.text(response.info).show();
                        }else{
                            error.text(response.error).show();
                        }

                        loading.hide();
                        button.removeClass('disabled');
                    },
                    error: function()
                    {
                        loading.hide();
                        button.removeClass('disabled');
                        error.text('Непредвиденная ошибка. Повторите позже.').show();
                    }
                });
            }
            else
            {
                button.removeClass('disabled');
                error.text('Введите адрес электронной почты').show();
            }
        }

        e.preventDefault();
    });

    // Форма регистрации
    $('#client_reg').on('click', function(e)
    {
        $('#main').off('click.filter');
        $('#main').click();
        $('#client_reg_form').show();
        $('#client_reg_block').show();
        $('#client_reg_name').focus();

        $('#client_reg_block').on('click', 'a.close', function(e)
        {
            $(this).off('click');
            $('#client_reg_block').hide();
            $('#main').off('click.client_reg_form');

            e.stopPropagation();
            e.preventDefault();
        });

        $('#main').on('click.client_reg_form', function(e)
        {
            $('#client_reg_block').hide();
            $('#main').off('click.client_reg_form');
            $('#client_reg_block').off('click', 'a.close');
        });

        e.stopPropagation();
        e.preventDefault();
    });

    $('#client_reg_block').on('click', function(e) { e.stopPropagation(); });

    $('#client_reg_form').submit(function(e)
    {
        var button = $(this);
        var loading = $('#client_reg_block > .block_ajax_loader');
        var error = $('#client_reg_error');
        var info = $('#client_reg_info');

        error.hide();
        info.hide();

        if(!button.hasClass('disabled'))
        {
            button.addClass('disabled');
            $('#client_reg_form').find('.required').each(function()
            {
                if ($(this).val() == '')
                {
                    $(this).closest('.control-group').addClass('error');
                }
                else
                {
                    $(this).closest('.control-group').removeClass('error');
                }
            });

            if (!$('#client_reg_form').find('.control-group.error').length)
            {
                loading.show();

                $.ajax({
                    url: '/ajaxer.php?x=client_reg',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        name: $('#client_reg_name').val(),
                        email: $('#client_reg_email').val(),
                        phone: $('#client_reg_phone').val()
                    },
                    success: function(response)
                    {
                        if (response.success)
                        {
                            info.html(response.info).show();
                            $('#client_reg_form').hide();
                        }
                        else
                        {
                            error.text(response.error).show();
                        }

                        loading.hide();
                        button.removeClass('disabled');
                    },
                    error: function()
                    {
                        loading.hide();
                        button.removeClass('disabled');
                        error.text('Непредвиденная ошибка. Повторите позже.').show();
                    }
                });
            }
            else
            {
                button.removeClass('disabled');
                error.text('Заполните обязательные поля').show();
            }
        }

        e.preventDefault();
    });

    // Отправка формы поиска
    $('#go_search').on('click', function(e)
    {
        if(!$('#go_search').hasClass('disabled'))
        {
            $('#search_form').submit();
        }

        e.preventDefault();
    });

    $('#search_form').on('submit', function(e)
    {
        var search_str = '';

        $('#go_search').addClass('disabled');

        $('#search_form').find('.wrap .img').removeClass('hide');

        // Запоминаем тип поиска множественный или обычный
        var type_of_search = $('.search_select > input:checked').val();
        $.cookie('type_search', type_of_search, {expires: 30, path: '/'});

        if (type_of_search == 'single')
        {
            e.preventDefault();

            if (!$('#search_input').hasClass('empty'))
            {
                // Поисковый запрос
                search_str = $.trim($('#search_input').val());

                einfo_stat('search', search_str);


                // Поисковый адрес
                var address = (search_str.match(/^["'](.+)["']$|[?*]/g) ? '/search' : '' ) + '/' + encodeURIComponent(search_str) + '/';

                // Редирект
                $.ajax({
                    type: 'GET',
                    url: '/ajaxer.php?x=search&set_cookie=1',
                    complete: function()
                    {
                        // Поисковый адрес
                        document.location.href = address;
                    }
                });
            }
            else
            {
                // Редирект
                document.location.href = '/search/';
            }
        }
        else
        {
            // Поисковый запрос
            search_str = $.trim($('#multiple_input').val());
            if (search_str !== '')
            {
                var search_arr = search_str.split('\n');
                einfo_stat('multiple_search', search_arr.length);

                $.cookie('is_search', '1', {expires: 1, path: '/'});
            }
        }
    });


    // Информация о поставщике
    $('.info .line .icon-phone').on('click', function()
    {
        var link = $(this).next('.show_user_info.main');

        show_user_info(link);
    });

    $('.show_user_info').on('click', function(e)
    {
        e.preventDefault();
        
        var link = $(this).hasClass('main') ? $(this) : $(this).parent().siblings('li').find('.show_user_info.main');

        show_user_info(link);
    });

    // Включаем статистику
    statistics_enable(document);

    // ------ Подсказки ------
    $('.tooltip').qtip({
        style: {
            classes: 'qtip-light qtip-shadow qtip-rounded'
        }
    });
    // -----------------------

    // Переключение на множественный поиск и обратно
    $(document).on('click', '.search_select > input', function(event)
    {
        var search_choice = $(this).val(),
            single_search = $('#search_input'),
            multiple_search = $('#multiple_input'),
            search_form = $('#search_form');

        if (search_choice === 'multiple')
        {
            single_search.hide();
            multiple_search.show();
            var text = multiple_search.val();
            multiple_search.val(text.trim());

            if (multiple_search.val() !== '')
            {
                $('#search_form span.clear_search').show();
            }
            else
            {
                $('#search_form span.clear_search').hide();
            }

            search_form.attr('method', 'post');

            $('#search_block').css('height', '197px');
            $('#search_block > div.bg').css('height', '178px');
        }
        else
        {
            single_search.show();
            single_search.focus();
            var search_text = multiple_search.val();

            if (search_text !== '')
            {
                var search_arr = search_text.split('\n');
                single_search.val(search_arr[0].trim());
                $('#search_form span.clear_search').show();
            }
            else
            {
                single_search.val('');
            }

            multiple_search.hide();
            search_form.attr('method', 'get');

            $('#search_block').css('height', '105px');
            $('#search_block > div.bg').css('height', '87px');
        }
    });

    // -----------------------

    $('#to_developer').fancybox({
        padding: 10,
        wrapCSS: 'rounded',
        closeBtn: true,
        beforeShow: function() {

        },
        afterClose: function() {

        }
    });

    // Отправка формы
    $('#form_developer .button').click(function(e)
    {
        e.preventDefault();

        var btn = $(this);
        var input = $('#form_developer').find('#text');
        var text = input.val();

        if (text.trim() == '')
        {
            input.addClass('f-incorrect');
            return false;
        }

        $.ajax({
            url: '/ajaxer.php?x=to_developer',
            dataType: 'json',
            data: {
                text: text,
                page: location.pathname
            },
            type: 'POST',
            beforeSend: function() {
                input.removeClass('f-incorrect').addClass('disabled').prop('disabled', true);
                btn.addClass('disabled');
            },
            success: function(response)
            {
                if (!response.success)
                {
                    alert('Ошибка сервера.');
                    input.removeClass('disabled').prop('disabled', false);
                    return false;
                }

                $(document).find('#to_developer_success').show();
            },
            error: function()
            {
                $.fancybox.close();
                input.removeClass('disabled').prop('disabled', false);
                alert('Ошибка сервера.');
            }
        });
    });
});

// Функция просмотра информации о поставщике.
// link - jQuery элемент нажатой ссылки.
function show_user_info(link)
{
    if (!link.hasClass('.lock'))
    {
        $('.show_user_info').removeClass('.lock');
        link.addClass('.lock');

        var user_id = link.data('id');

        // Правый верхний край
        var width = link.width();
        var top = link.offset().top - 20;
        var left = link.offset().left + width - 195;

        $.ajax({
            url: '/ajaxer.php?x=search',
            dataType: 'json',
            data: { action: 'user_info', id: user_id },
            type: 'POST',
            success: function(data)
            {
                if (data)
                {
                    $('#main').append($.trim(data.html));
                    var popup = $('#company_card_popup');
                    var popup_height = popup.height();
                    var main_height = $('#main').height();
                    var window_height = $(window).height();

                    // Поднять вверх, если за пределами экрана
                    if (top + popup_height >= main_height)
                    {
                        top = main_height - 50 - popup_height;
                    }

                    popup.css({top: top, left: left});

                    if( popup_height + top > window_height + $(window).scrollTop() - 50)
                    {
                        $('body,html').animate({ scrollTop: popup_height + top + 50 - window_height }, 'normal');
                    }

                    popup.on('click', function(e) {e.stopPropagation(); });

                    /******************* Белый черный список *********************************/

                    // Открытие белого списка при клике
                    $('#company_card_popup .thumb-up-filled').on('click', function(){
                        document.location.href = '/account/white_list/';
                        return false;
                    });

                    // Добавление в белый список
                    $('#company_card_popup .thumb-up').on('click',  function(){

                        if ($(this).hasClass('unregistered'))
                        {
                            $('#show_messag_for_unregistered').click();
                            return false;
                        }

                        var user_id = $('.hide_user_info').data('id');

                        change_user_lists(user_id, '/ajaxer.php?x=user_white_list', 'add')
                            .then(
                                function(){
                                    popup.find('.title_block .line').click();

                                    $('[data-user_id="'+user_id+'"]').find('.thumb-up').hide().removeClass('hide_rating')
                                        .end()
                                        .find('.thumb-down').hide().removeClass('hide_rating')
                                        .end()
                                        .find('.thumb-up-filled').show();
                                },
                                function(err) {
                                    console.log(err);
                                }
                            );
                    });

                    // Добавление в черный списка
                    $('.thumb-down').on('click',  function(){

                        if ($(this).hasClass('unregistered'))
                        {
                            $('#show_messag_for_unregistered').click();
                            return false;
                        }

                        var user_id = $('.hide_user_info').data('id');

                        change_user_lists(user_id, '/ajaxer.php?x=user_disallow')
                            .then(
                                function(){
                                    popup.find('.title_block .line').click();

                                    $('[data-user-id="'+user_id+'"]').fadeOut();
                                    $('[data-user_id="'+user_id+'"]').fadeOut();
                                },
                                function(err) {
                                    console.log(err);
                                }
                            );
                    });

                    // Скрыть информацию по клику
                    popup.find('.title_block .line').on('click', function(e)
                    {
                        e.preventDefault();

                        popup.find('.thumb-up-filled').off('click');
                        popup.find('.thumb-up').off('click');
                        popup.find('.thumb-down').off('click');

                        popup.fadeOut('fast', function()
                        {
                            link.removeClass('.lock');
                            popup.find('.title_block .line').off('click');
                            popup.off('mouseenter mouseleave click');
                            popup.remove();

                            $('#main').off('click.company_card_popup');
                        });
                    });

                    $('#main').on('click.company_card_popup', function()
                    {
                        link.removeClass('.lock');
                        popup.find('.title_block .line').off('click');
                        popup.off('mouseenter mouseleave click');
                        popup.remove();

                        $('#main').off('click.company_card_popup');
                    });

                    // Скрыть если курсор за областью блока
                    popup.hover(
                        function(){},
                        function()
                        {
                            popup.fadeOut('fast', function()
                            {
                                link.removeClass('.lock');
                                popup.find('.title_block .line').off('click');
                                popup.off('mouseenter mouseleave click');
                                popup.remove();

                                $('#main').off('click.company_card_popup');
                            });
                        }
                    );

                    statistics_enable('#company_card_popup');

                }
            }
        });
    }
}

var timeout;

function show_smile(type)
{
    if (type == 'smile' || type == 'sad')
    {


        var smile = $('#smile');

        clearTimeout(timeout);

        smile.removeClass().addClass(type);

        timeout = setTimeout(function()
        {
            smile.fadeOut('slow', function() {
                smile.removeClass(type).css('display', 'block');
            });
        }, 900);
    }
}

function print_basket_count(cnt)
{
    if(cnt > 0)
    {
        var variant_1 = 'Выбран ' + cnt + ' компонент';
        var variant_2 = 'Выбрано ' + cnt + ' компонента';
        var variant_3 = 'Выбрано ' + cnt + ' компонентов';

        var str = Plural(cnt, variant_1 + '|' + variant_2 + '|' + variant_3);
        $('#in_basket').removeClass('empty').find('.link').html(str);
        $('#in_basket_fixed').removeClass('hide').find('span').html(cnt);
    }
    else
    {
        $('#in_basket').addClass('empty').find('.link').html('');
        $('#in_basket_fixed').addClass('hide').find('span').html('');
    }
}

function update_scroll()
{
    var list_container = $('#basket_cnt_upd').find('#list_container');
    var right_list = $('#basket_cnt_upd').find('.right_list');
    var list_overflow = $('#basket_cnt_upd').find('#list_overflow');

    if (!!('ontouchstart' in window) ? 1 : 0)
    {
        if (!list_container.hasClass('scroll'))
        {
            list_container.addClass('scroll');
        }
    }
    else
    {
        if (list_overflow.height() >= right_list.height() && !list_container.hasClass('scrollable'))
        {
            list_container.addClass('scrollable');

            list_container.mCustomScrollbar({
                scrollInertia: 0,
                horizontalScroll: false,
                mouseWheel: 10,
                advanced:{
                    autoScrollOnFocus: false
                },
                scrollButtons: {
                    enable: false,
                    scrollType: 'continuous'
                }
            });
        }
        else
        {
            list_container.mCustomScrollbar('update');
        }
    }
}

// Кнопка прокрутки страницы
$(window).scroll(function() {
    var top = $(document).scrollTop();
    if (top > 60) $('#to_top').fadeIn(400);
    else $('#to_top').fadeOut(400);
});

$(document).on('click', '#to_top', function () {
    $('html, body').animate({ scrollTop: 0 });
    return false;
});