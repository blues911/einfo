//------ Cтатистика ---------

function statistics_enable(container)
{
    $(container).find('[data-users-stat]').each(function(index, item)
    {
        // Получаем объект item
        var item = $(item);

        // Получаем данные необходимые для обработчика статистики
        eval('var data = {' + item.data('users-stat') + '}');

        // Вешаем обработчик на событие event
        item.one(data.event, function(event)
        {
            users_stat(data.user_id, data.action);
        });
    });
}

function users_stat(user_id, action)
{
    $.post('/ajaxer.php?x=users_stat', { user_id: user_id, action: action });
}