<?php

error_reporting(E_ERROR);

/**************************************************
 *               Н А С Т Р О Й К И                *
 **************************************************/

/* Хеш пользователя. */
$user_hash = 'userhash';

/* Сервер базы данных. */
$db_host = 'localhost';

/* Имя пользователя базы данных. */
$db_user = 'root';

/* Пароль пользователя базы данных. */
$db_pass = 'password';

/* Имя базы данных. */
$db_name = 'dbname';

/*
 * Текст запроса к базе данных.
 *
 * Важно сохранить алиасы полей, так как они далее будут использоваться
 * в скрипте. Также запрос должен возвращать все поля, но значения этих
 * полей могут быть пустыми.
 *
 * Ниже приведен список необходимых полей и их значения.
 *
 * title            Название компонента.
 * category         Категория.
 * description      Описание компонента.
 * manufacture      Производитель (бренд).
 * prices           Массив объектов, где каждый объект имеет ключи:
 *                  "n" - кол-во (number);
 *                  "с" - валюта (currency);
 *                  "p" - цена (price).
 * mfgdate          Год производства (напр., 1997).
 * docs             Ссылка на документацию (напр., "http://mysite.ru/doc.pdf").
 * comp_url         Ссылка на страницу компонента (напр., "http://mysite.ru/comp.html").
 * stock            Если N=-1, то "скрыть склад";
 *                  если N>0, то это кол-во на складе;
 *                  если N=0, то позиция не складская.
 * stock_remote     Количество на удалённом складе;
 * delivery         Если N=-1, то "скрыть кол-во недель для поставки";
 *                  если N>0, то это кол-во недель для поставки;
 *                  если N=0, то позиция не на заказ.
 *
 * Пример файла выгрузки: https://einfo.ru/media/export_example.xml
 */

$query_string = '
    SELECT
      tbl.title AS title,
      tbl.category AS category,
      tbl.text AS description,
      brand.title AS manufacture,
      tbl.prices AS prices,
      "" AS mfgdate,
      tbl.pdf_remote AS docs,
      "" AS comp_url,
      tbl.stock AS stock,
      tbl.stock_remote AS stock_remote,
      -1 AS delivery
    FROM
      catalog_component AS tbl
      INNER JOIN catalog_brand AS brand ON tbl.brand_id = brand.id
';


// ****************************************************************************

/*
 * Проверка загруженности сервера
 */
sleep_overload(1.0);

/*
 * Подключение к базе данных
 */
$connection = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (!$connection)
{
    die('Ошибка подключения к базе данных: ' . mysqli_connect_error());
}

mysqli_query($connection, 'SET NAMES utf8');


/*
 * Выполнение запроса к базе данных
 */
$query = mysqli_real_query($connection, $query_string);

if (!$query)
{
    die('Ошибка запроса к базе данных: ' . mysqli_error($connection));
}

$result = mysqli_use_result($connection);


/*
 * Формирование заголовка XML файла
 */
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Cache-Control: private', false);
header('Content-Type: text/xml');
header('Content-Disposition: attachment; filename=' . $user_hash . '.xml');

echo '<?xml version="1.0" encoding="utf-8" standalone="yes"?>';

?>

<price>
    <header>
        <hash><?php echo $user_hash; ?></hash>
        <action>sale</action>
        <status>all</status>
        <uploadtype>replace</uploadtype>
    </header>
    <data>
        <?php


        /*
         * Обработка результатов запроса
         */
        $_cnt = 0;
        while ($row = mysqli_fetch_assoc($result))
        {
            $_cnt++;
            if ($_cnt % 1000 == 0)
            {
                sleep_overload(1.0);
            }
            ?>

            <item>
                <title><![CDATA[<?php echo $row['title']; ?>]]></title>
                <category><![CDATA[<?php echo $row['category']; ?>]]></category>
                <description><![CDATA[<?php echo $row['description']; ?>]]></description>
                <manufacture><![CDATA[<?php echo $row['manufacture']; ?>]]></manufacture>
                <prices>
                    <?php foreach($row['prices'] as $price): ?>
                        <price n="<?php echo $price['n']; ?>" c="<?php echo $price['c']; ?>"><?php echo $price['p']; ?></price>
                    <?php endforeach; ?>
                </prices>
                <mfgdate><?php echo $row['mfgdate']; ?></mfgdate>
                <docs><![CDATA[<?php echo $row['docs']; ?>]]></docs>
                <comp_url><![CDATA[<?php echo $row['comp_url']; ?>]]></comp_url>
                <stock><?php echo (int)$row['stock']; ?></stock>
                <stock_remote><?php echo (int)$row['stock_remote']; ?></stock_remote>
                <delivery><?php echo (int)$row['delivery']; ?></delivery>
            </item>
        <?php
        }


        /*
         * Окончание файла
         */
        ?>

    </data>
</price>

<?php

function sleep_overload($max_load = 1.0)
{
    while(true)
    {
        $load = sys_getloadavg();

        if ($load[0] >= $max_load)
        {
            sleep(1);
        }
        else
        {
            break;
        }
    }

    return true;
}
