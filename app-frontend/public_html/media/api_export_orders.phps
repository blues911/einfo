<?php

error_reporting(E_ERROR);

/**************************************************
 *               Н А С Т Р О Й К И                *
 **************************************************/

/* Хеш пользователя. */
$user_hash = 'userhash';

/**
 * Дата отсечки результатов.
 *
 * Для списка за весь период указывать как пустое значение ('')
 */
$date_from = '2012-01-01 12:10:10';

if ($curl = curl_init())
{
    curl_setopt($curl, CURLOPT_URL, 'https://www.einfo.ru/api/export_orders/');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array('user_hash' => $user_hash, 'date_from' => $date_from)));
    $out = curl_exec($curl);

    /**
     * Ниже приведен список получаемых полей и их значения.
     *
     * id               Номер заказа.
     * date             Дата заказа.
     * client_name      Имя клиента.
     * client_email     Электронная почта клиента.
     * client_phone     Номер телефона клиента.
     * client_comment   Коментарий к заказу.
     * items            Позиции заказа.
     *  - count            Количество заказанных изделий (число).
     *  - title            Название изделия.
     *  - manufacturer     Производитель.
     *  - description      Описание изделия.
     *  - retail_price     Розничная цена (число).
     *  - retail_currency  Валюта розничной цены ("rur", "usd" или "eur").
     *  - trade_price      Оптовая цена (число).
     *  - trade_currency   Валюта оптовой цены ("rur", "usd" или "eur").
     *  - mfgdate          Год производства (напр., 1997);
     *
     */

    print_r(json_decode($out));

    curl_close($curl);
}
