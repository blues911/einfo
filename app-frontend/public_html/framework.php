<?php
$timer = array();
$timer['start'] = microtime(true);

if (!empty($_COOKIE[session_name()]) || !empty($_COOKIE['basket_key']) ||
    !empty($_COOKIE['user_settings']) || $_SERVER['REQUEST_METHOD'] == 'POST' ||
    !empty($_GET['order']))
{
    session_start();
}
else
{
    header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    header('Pragma: no-cache');
}

// подключение основных модулей
include_once('../../config.php');
include_once(PATH_FRONTEND . '/php/function.php');
include_once(PATH_FRONTEND . '/php/adodb/adodb.inc.php');
include_once(PATH_FRONTEND . '/php/smarty/Smarty.class.php');
include_once(PATH_FRONTEND . '/php/classes/class.Pager.php');
include_once(PATH_FRONTEND . '/php/classes/class.MyDate.php');
include_once(PATH_FRONTEND . '/php/classes/class.phpmailer.php');
include_once(PATH_FRONTEND . '/php/libs/Mobile_Detect.php');

// подключение к БД
$DB_CONNECT = array(
    'master' => DB_MAIN_DSN
);
list($DB_master, $DB) = database_connect($DB_CONNECT);

$ADODB_CACHE_DIR = PATH_TMP . '/adodb_cache';

if ($_SERVER['REMOTE_ADDR'] == '109.195.7.194' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == '10.0.2.2')
{
    // $DB->debug = true;
}

// Проверка статуса SphinxSearch
sphinx_status();

// Load Tracker
include_once('php/load_tracker.php');

// дата
$date = new MyDate();


// шаблонизатор
$smarty = new Smarty();
$smarty->template_dir = PATH_FRONTEND . '/templates';
$smarty->compile_dir = PATH_TMP . '/tpl-einfo';
$smarty->error_reporting = 6135;
//$smarty->escape_html = true;

// -----------------------------------------------------

// Детектинг мобильного устройства
$container['host_desktop'] = HOST_FRONTEND;
$container['host_mobile'] = HOST_FRONTEND_MOBILE;
$container['request_uri'] = $_SERVER['REQUEST_URI'];

$is_mobile = (bool)preg_match('/' . HOST_FRONTEND_MOBILE . '/ui', $_SERVER['HTTP_HOST']);

if (!empty($_GET['force_version']))
{
    setcookie('force_desktop_version', '1', 0, '/');
    header('Location: https://' . HOST_FRONTEND);
    exit();
}

if (preg_match('/' . HOST_FRONTEND_MOBILE . '/ui', $_SERVER['HTTP_HOST']))
{
    $container['is_mobile'] = true;
}
elseif (!empty($_COOKIE['force_desktop_version']))
{
    $container['is_mobile'] = false;
}
else
{
    $mobile_detect = new Mobile_Detect();
    $container['is_mobile'] = ($mobile_detect->isMobile() && !$mobile_detect->isTablet());
}

if ($is_mobile != $container['is_mobile'])
{
    $host = !empty($container['is_mobile']) ? HOST_FRONTEND_MOBILE : HOST_FRONTEND;
    $uri = !empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
    $redirect_uri = preg_match('/(^$|^\/$)/ui', $uri) ? $uri : null;

    // Если адрес ведет на главную страницу или store, то насильный редирект.
    if (!empty($container['is_mobile']) && isset($redirect_uri))
    {
        header('Location: https://' . $host . $redirect_uri);
        exit();
    }
}

$container['use_mobile_template'] = false;

// Авторизация по хешу
if (!empty($_GET['client_id']) && !empty($_GET['auth_hash']))
{
    $client = $DB->GetRow('SELECT * FROM clients WHERE id = ? AND auth_hash = ? AND (status = 1 OR status = -1)', array((int)$_GET['client_id'], $_GET['auth_hash']));
    if (!empty($client))
    {
        // Меняем хеш на новый и статус подтвержден
        $auth_hash = md5(uniqid());
        $DB->Execute('UPDATE clients SET status = 1, auth_hash = ? WHERE id = ?', array($auth_hash, $client['id']));

        // Записываем авторизацию в куки
        setcookie('client_id', $client['id'], time() + 365*24*60*60, '/');
        setcookie('auth_hash', $auth_hash, time() + 365*24*60*60, '/');

        $_COOKIE['client_id'] = $client['id'];
        $_COOKIE['auth_hash'] = $auth_hash;

        // Устанавливаем признак того, что авторизация произведена по ссылке
        $_auth_with_hash = true;

    }
    else
    {
        $container['error'] = 'Ссылка для авторизации устарела. Для получения новой ссылки воспользуйтесь меню «Вход» → «Забыли пароль?»';
    }
}

// Проверяем авторизацию
$client = array();
$container['auth'] = false;
if (!empty($_COOKIE['client_id']) && !empty($_COOKIE['auth_hash']))
{
    // Проверяем пару client_id и auth_hash
    $client = $DB->GetRow('
        SELECT * FROM clients WHERE id = ? AND auth_hash = ?
      ', array(
            $_COOKIE['client_id'],
            $_COOKIE['auth_hash']
        )
    );

    if (!empty($client))
    {
        // Обновляем дату последнего входа клиента
        $DB->Execute('
            UPDATE clients SET date_last_login = ? WHERE id = ?
        ', array(
            date('Y-m-d H:i:s'),
            $client['id']
        ));

        // Проверяем наличие настроек в куках
        if (empty($client['settings']) && !empty($_COOKIE['user_settings']))
        {
            $client['settings'] = $_COOKIE['user_settings'];
            $DB->Execute('UPDATE clients SET settings = ? WHERE id = ?', array($client['settings'], $client['id']));
        }

        $container['auth'] = true;

        // Признак авторизации по логину
        if (!empty($_COOKIE['auth_with_login']))
        {
            $client['auth_with_login'] = true;
            setcookie('auth_with_login', '', -1, '/');
            unset($_COOKIE['auth_with_login']);
        }
        else if (!empty($_auth_with_hash))
        {
            $client['auth_with_hash'] = true;
        }
    }
}

$client['auth'] = $container['auth'];

$smarty->assign('client', $client);

// -----------------------------------------------------
$timer['step1'] = microtime(true);

$container['current'] = 'internal';
$container['template_inc'] = $container['top'] = false;

// получение http-запроса и его предварительная разборка
$request_uri = str_replace(':', urlencode(':'), $_SERVER['REQUEST_URI']);
$parse_url = parse_url($request_uri);
$path_url = trim($parse_url['path'], "/");

//------------------------------------------------

try
{
    // Load Tracker
    if ($dos_deny) throw new Exception('Error 403', 403);

    if ($path_url == '')
    {
        $container['current'] = 'index';
    }
    // Загрузка одного из разделов аккаунта
    else if (preg_match('/^account\/?/i', $path_url, $matches))
    {
        $include_file = 'account/account.php';
    }
    else if (preg_match("/^(users|about|info|news|legal|basket)(\/([a-z0-9-_*!?&+=()\/<>~'\"]+)(\.html)?)?$/i", $path_url, $matches))
    {
        // pазборка динамического урла
        if (isset($matches[4]) && preg_match('/.html$/', $matches[4]))
        {
            $dinamic_url = str_replace(array('_', '-'), array('&', '='), $matches[3]);
            parse_str($dinamic_url . (isset($parse_url['query']) ? '&'. $parse_url['query'] : ''), $_GET);
        }

        if ($matches[1] == '')
        {
            $container['current'] = 'index';
            if (preg_match('/^\/\?/i', $_SERVER['REQUEST_URI'], $matches))
            {
                $allowed_get = array('yclid');

                if (!empty($_GET))
                {
                    foreach (array_keys($_GET) as $k)
                    {
                        if (!in_array($k, $allowed_get))
                        {
                            throw new Exception('Error 404', 404);
                        }
                    }
                }
                else
                {
                    throw new Exception('Error 404', 404);
                }
            }
        }
        else
        {
            $include_file = $matches[1] . (empty($matches[4]) && !empty($matches[2]) ? $matches[2] : '');
            $include_file = str_replace('/', '.', $include_file);

            // проверка на существование файла для отображения
            if (file_exists('content/' . $include_file . '.html'))
            {
                $container['current'] = 'static';
                $include_file = $include_file . '.html';
            }
            else if (file_exists('content/' . $include_file . '.php'))
            {
                $include_file = $include_file . '.php';
            }
            else throw new Exception('Error 404', 404);
        }
    }
    else if (preg_match('/^search/i', $path_url) && $_SERVER['HTTP_HOST'] == $container['is_mobile'] ? HOST_FRONTEND_MOBILE : HOST_FRONTEND)
    {
        if (!empty($_GET['q']))
        {
            $q = stripslashes(strip_tags(trim(urldecode($_GET['q']))));

            $q = @iconv('windows-1251', 'utf-8', $q);

            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /search/' . urlencode($q) . '/');

            exit;
        }
        else
        {

            $include_file = 'store.php';
        }
    }
    else if (preg_match("/^([-a-zа-я0-9_.\?\"\*'!\(\)%~]+)\/?([^\/.]*)$/i", $path_url, $matches))
    {
        $include_file = 'store.php';
    }
    else throw new Exception('Error 404', 404);

    $timer['step2'] = microtime(true);

    // парсинг глобальных шаблонов
    ob_start();
        include_once ('content/objects/settings.php');
        include_once ('content/objects/common.php');
        include_once ('content/objects/menu.php');

        // Тип интерфейса
        $_interface = isset($USER_SETTINGS['interface']) ? $USER_SETTINGS['interface'] : DEFAULT_INTERFACE;
        if ($_interface > 1) $_interface = 0;
        $smarty->assign('interface', $_interface);

        $smarty->assign('settings', $SETTINGS);
        $smarty->assign('container', $container);

        switch ($container['current'])
        {
            case 'index':
            	include_once ('content/objects/index_info.php');
            	break;

            default:
                include_once ('content/' . $include_file);
            	break;
        }

        $container['content'] = ob_get_contents();
    ob_end_clean();

    $timer['step3'] = microtime(true);
}
catch (Exception $e)
{
    if ($e->getCode() == 404)
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');

        include_once ('content/objects/settings.php');
        include_once ('content/objects/common.php');
        include_once ('content/objects/menu.php');

        $container['content'] = file_get_contents('content/error_404.html');

		$container['robots'] = 'noindex, nofollow';

        $smarty->assign('container', $container);
        $smarty->assign("chat_host", HOST_CHAT);
        $smarty->assign("chat_host_ws", HOST_CHAT_WS);
        $smarty->assign("chat_host_frontend", HOST_FRONTEND);
        $smarty->assign("chat_last_visit_interval", CHAT_LAST_VISIT_INTERVAL);
		$smarty->display('_main.tpl');
		exit;
    }
	elseif ($e->getCode() == 403)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
		exit;
	}
	elseif ($e->getCode() == 500)
	{
		header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
		exit;
	}

}

include_once('php/ims.php');

// Общее время
$timer['prepare'] = number_format($timer['step1'] - $timer['start'], 5, '.', '');
$timer['verify'] = number_format($timer['step2'] - $timer['step1'], 5, '.', '');
$timer['parser'] = number_format($timer['step3'] - $timer['step2'], 5, '.', '');
$timer['total'] = number_format(microtime(true) - $timer['start'], 5, '.', '');
if (DEBUG_TIMERS_IN_HTML)
{
    $container['page_timer'] = $timer;
}

// Тип интерфейса
$_interface = isset($USER_SETTINGS['interface']) ? $USER_SETTINGS['interface'] : DEFAULT_INTERFACE;
if ($_interface > 1) $_interface = 0;
$smarty->assign('interface', $_interface);

$smarty->assign('client', $client);
$smarty->assign('container', $container);
$smarty->assign("chat_host", HOST_CHAT);
$smarty->assign("chat_host_ws", HOST_CHAT_WS);
$smarty->assign("chat_host_frontend", HOST_FRONTEND);
$smarty->assign("chat_last_visit_interval", CHAT_LAST_VISIT_INTERVAL);

if ($container['use_mobile_template'])
{
    $smarty->display('mobile/_main.tpl');
}
else
{
    $smarty->display('_main.tpl');
}
