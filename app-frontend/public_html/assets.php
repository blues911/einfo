<?php

/*
 * Скрипт минификации CSS и JS.
 *
 * Список файлов для минификации хранится
 * в файле /media/assets.json.
 *
 * Скрипт проверяет время последнего изменения
 * перечисленных файлов и файла /media/assets.json.
 * Если один из файлов моложе минифицированной
 * версии, то файл снова компилируется.
 *
 * Компилированные файлы хранятся в папке {PATH_TMP}/assets
 * с именами assets.css и assets.js.
 */

use Assetic\Filter\CssMinFilter;
use Assetic\Filter\JSqueezeFilter;
use Assetic\Filter\CssRewriteFilter;
use Assetic\Asset\FileAsset;
use Assetic\Asset\AssetCollection;

require(__DIR__ . '/../../config.php');

if (empty($_GET['type']) || !in_array($_GET['type'], array('css', 'js')))
{
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
    die('404 Not Found');
}

$type = $_GET['type'];

$media = !empty($_GET['media']) ? $_GET['media'] : 'desktop';
$media = in_array($media, array('desktop', 'mobile')) ? $media : 'desktop';

// Читаем список файлов для текущего типа assets
$assets_config_file_path = __DIR__ . '/media/assets.json';
$assets_config_last_modified = filemtime($assets_config_file_path);
$assets_config = json_decode(file_get_contents($assets_config_file_path), true);
$assets_file_list = $assets_config[$media][$type];

// Проверяем временную папку
$assets_tmp_path = PATH_TMP . '/assets/';

if (!is_dir($assets_tmp_path))
{
    mkdir($assets_tmp_path);
}

$tmp_file_path = $assets_tmp_path . 'assets.' . $media . '.' . $type;
$tmp_file_last_modified = is_file($tmp_file_path) ? filemtime($tmp_file_path) : 0;

// Проверяем наличие изменений
$assets_is_changed = $assets_config_last_modified > $tmp_file_last_modified;

if (!$assets_is_changed)
{
    foreach ($assets_file_list as $file_path)
    {
        $file_path = __DIR__ . '/media/' . $file_path;

        if (is_file($file_path) && filemtime($file_path) > $tmp_file_last_modified)
        {
            $assets_is_changed = true;
            break;
        }
    }
}

// Минифицируем файлы
if ($assets_is_changed)
{
    require(__DIR__ . '/vendor/autoload.php');

    $assets_filters = array();

    $assets_filters['css'] = function()
    {
        return array(
            new CssRewriteFilter(),
            new CssMinFilter()
        );
    };

    $assets_filters['js'] = function()
    {
        return array(
            new JSqueezeFilter()
        );
    };

    $asset_collection_filters_func = $assets_filters[$type];
    $asset_collection_files = array();

    foreach ($assets_file_list as $file_relative_path)
    {
        $file_path = __DIR__ . '/media/' . $file_relative_path;
        if (is_file($file_path))
        {
            $asset_collection_files[] = new FileAsset($file_path, array(), '/media/', '/media/' . $file_relative_path);
        }
    }

    $asset_collection = new AssetCollection($asset_collection_files, $asset_collection_filters_func());

    file_put_contents($tmp_file_path, $asset_collection->dump());
}

// Отправляем ответ
$headers = array(
    'css' => array(
        'Content-Type: text/css',
        'Pragma: public',
        'Cache-Control: max-age=2592000, must-revalidate'
    ),
    'js' => array(
        'Content-Type: application/javascript',
        'Pragma: public',
        'Cache-Control: max-age=2592000, must-revalidate'
    )
);

foreach ($headers[$type] as $header)
{
    header($header);
}

echo(file_get_contents($tmp_file_path));